﻿<?php
class Backup{
	//php -d safe_mode=Off -d disable_functions=. /var/www/html/backups/backup.php
	
	private $domain_list = array(
         'geralda.horasoee.eu',
         'kaunobaldai.horasoee.eu',
         'stronglasas.horasoee.eu',
         'vici.horasoee.eu',
         'vpaukstynas.horasoee.eu',
         'adax.horasoee.eu',
         'aumeta.horasoee.eu',
         'lrytas.horasoee.eu',
         'selteka.horasoee.eu',
         'zemaitijosspauda.horasoee.eu',
         'ump.horasoee.eu',
         'vitabaltic.horasoee.eu',
         'omniteksas.horasoee.eu',
         'axioma.horasoee.eu',
         'hydro.horasoee.eu',      
		'kggroup.horasoee.eu',
		'kitron.horasoee.eu',
		'kitronsp.horasoee.eu',
		'bfc.horasoee.eu',
		'balticfilter.horasoee.eu',
		'baltijosbrasta.horasoee.eu',
		'biok.horasoee.eu',
		'dotpack.horasoee.eu',
		'fpi.horasoee.eu',
		'grafija.horasoee.eu',
		'histeel.horasoee.eu',
		'krekenava.horasoee.eu',
		'oho.horasoee.eu',
		'technogaja.horasoee.eu',
		'ut.horasoee.eu',
		'haas.horasoee.eu',
		'perapack.horasoee.eu',
		'asistemos.horasoee.eu',
		'fasa.horasoee.eu',
		'duguva.horasoee.eu',
		'amalva.horasoee.eu',
	);
	private static $rootPath = __DIR__.'/';
	private static $currentProjectAppPath = '';
	private static $dbLink = null;
	private static $dbConfigs = array();
	
	function __construct(){
		$this->deleteOldBackups(); 
		$this->createBackup(); 
	}

	public function createBackup(){
		foreach($this->domain_list as $domain) {
			self::$currentProjectAppPath = '/var/www/html/'.$domain.'/app/'; 
			self::$dbConfigs = self::getDbConfigs($domain);
			list($host, $prefix, $login, $password, $database) = self::$dbConfigs;
			self::removeSessions();
			if(empty($login) || empty($password) || empty($database)) {
				continue;
			}
			//$this->checkProjectActivity($domain, $login, $password, $database, $prefix);				
			//echo $domain ."\r\n".$login."\r\n".$password."\r\n".$database."\r\n-----";		
			$date = date("Y-m-dH:i:s");
			if(!file_exists(self::$rootPath.$domain.'/')) {
				umask(0);
				mkdir(self::$rootPath.$domain.'/',0777);
			}
			set_time_limit(30000);
			exec('mysqldump -h '.$host.' --user='.$login.' --password='.$password.' '.$database.' > '.self::$rootPath.$domain.'/'.$database.'_'.$date.'.sql');
			$dbCredentials = array($login, $password, $database);
			$this->createPeriodicalBackup($domain, date('Y-m-d',strtotime("first day of this month"))); 
			self::dicconnectFromDb();
		}
		gc_collect_cycles();
	}
	
	public function deleteOldBackups(){ // delete old backups
		$delete_if_older = (60 * 60 * 24) * 2 - 7200; // (60 * 60 * 24) = 1 day . Result = 15 days
		foreach($this->domain_list as $domain) {
			$dir = self::$rootPath.$domain.'/';
			$files = scandir($dir);
			foreach($files as $f) {
				if($f == '..' || $f == '.' || is_dir($dir.$f)) {
					continue;
				}
				//echo "Full filename: ".$f."\r\n";
				$file_date = explode("_20",$f);
				$file_date = "20".$file_date[1];
				$file_date = explode(".",$file_date);
				$file_date = $file_date[0];
				//echo "File date: " . $file_date."\r\n";
				//echo "Delete if older than: " . date("Y-m-d H:i:s",(time() - $delete_if_older)) ."\r\n";
				if(strtotime($file_date) < (time() - $delete_if_older)) {
					echo "deleting: ".$dir.$f."\r\n";
					unlink($dir.$f);
				}
			}
		}
	}
	
	public static function getDbConfigs($domain, $connectionType = 'default'){
		$database_config_path = self::$currentProjectAppPath.'Config/database.php';
		if(!file_exists($database_config_path)) {
			die('Neegzistuoja database.php failas : '.$domain);
		}
		$file = fopen($database_config_path,'r');
		$content = fread($file,filesize($database_config_path));
		fclose($file);
		$host = '';
		$prefix = '';
		$login = '';
		$password = '';
		$database = '';
		preg_match("/\'host\' => \'(.+?)\'/", $content,$host);
		if(sizeof($host) == 2) {
			$host = $host[1];
		}
		preg_match("/\'prefix\' => \'(.+?)\'/", $content,$prefix);
		if(sizeof($prefix) == 2) {
			$prefix = $prefix[1];
		}
		preg_match("/\'login\' => \'(.+?)\'/", $content,$login);
		if(sizeof($login) == 2) {
			$login = $login[1];
		}
		preg_match("/\'password\' => \'(.+?)\'/", $content,$password);
		if(sizeof($password) == 2) {
			$password = $password[1];
		}
		preg_match("/\'database\' => \'(.+?)\'/", $content,$database);
		if(sizeof($database) == 2) {
			$database = $database[1];
		}
		return array($host, $prefix, $login, $password, $database);
	}
	
	function checkProjectActivity($domain, $login, $password, $database, $prefix){
		if(preg_match('/horasmpm\.eu|imperatum\.lt$/i', $domain)){
			self::connectToDb();
			$result = mysqli_query(self::$dbLink, 'SELECT * FROM '.$prefix.'events ORDER BY id DESC LIMIT 1');
			if($result->num_rows){
				$row = $result->fetch_object();
				if(strtotime($row->created) < time() - 5*24*3600 && strtotime($row->created) > time() - 20*24*3600 ){
					echo "$domain\n";
					$to      = 'v.tutkus@horasmpm.eu';
					//$to      = 'd.kreismontas@horasmpm.eu';
					$subject = $domain.' klientas nustojo naudotis HorasMPM sistema ilgiau kaip 5 dienas';
					$message = $domain."<br />".'Reikia kažką daryti su šiuo klientu, nes jis nebesinaudojama Horu. Galimas daiktas, kad jam kažkas nebepatinka šioje sistemoje ir reikėtų iš naujo jį sudominti. Jei nieko nedarysim, galim nebegauti aptarnavimo ir visas darbas nueis šuniui ant uodegos.<br />'.
						'Paskutinis įvykis sistemoje fiksuotas: '.$row->created.' '.$row->action;
					;
					$headers = 'From: noreply@horasmpm.eu' . "\r\n" .
						'Content-Type: text/html; charset=UTF-8' . "\r\n" .
						'Reply-To: noreply@horasmpm.eu' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();
					mail($to, $subject, $message, $headers); 
				}
				
			}
		}
	}
	
	public static function connectToDb(){
		if(self::$dbLink !== null || empty(self::$dbConfigs)){ return null; }
		list($host, $prefix, $login, $password, $database) = self::$dbConfigs;
		self::$dbLink = mysqli_connect($host, $login, $password, $database);
		if (!self::$dbLink) {
			die('Could not connect: ' . mysqli_error(self::$dbLink));
		}
	}
	public static function dicconnectFromDb(){
		if(self::$dbLink == null){ return null; }
		mysqli_close(self::$dbLink);
		self::$dbLink = null;
	}
	
	private static function removeSessions(){
		$files = glob(self::$currentProjectAppPath.'tmp/sessions/*'); //get all file names
		foreach($files as $file){
			if(is_file($file))
			unlink($file); //delete file
		}
	}
	
	private function createPeriodicalBackup(String $domain, String $periodDate){
		$year = date('Y',strtotime($periodDate));
		$path = self::$rootPath.'rackraysmb/'.$domain.'/'.$year.'/';
		if(!file_exists($path)) {
			umask(0);
			mkdir($path,0777,true);
		}
		$path .= $periodDate.'.sql';
		if(file_exists($path)){ return null; }
		set_time_limit(30000);
		list($host, $prefix, $login, $password, $database) = self::$dbConfigs;
		exec('mysqldump -h '.$host.' --user='.$login.' --password='.$password.' '.$database.' > '.$path);
		self::connectToDb();
		$result = mysqli_query(self::$dbLink, 'TRUNCATE TABLE `records_archive`');
	}

}
new Backup;


?>