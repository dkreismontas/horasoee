-- phpMyAdmin SQL Dump
-- version 3.2.0.1
-- http://www.phpmyadmin.net
--
-- Darbinė stotis: localhost
-- Atlikimo laikas:  2015 m. Sausio 09 d.  08:10
-- Serverio versija: 5.1.36
-- PHP versija: 5.3.29

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Duombazė: `vd_process_db`
--

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `dashboards_calculations`
--

DROP TABLE IF EXISTS `dashboards_calculations`;
CREATE TABLE IF NOT EXISTS `dashboards_calculations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theory_prod_time` float NOT NULL COMMENT 'Teorinis gamybos laikas (min.)',
  `fact_prod_time` int(11) NOT NULL COMMENT 'Faktinis gamybos laikas (min.)',
  `total_quantity` int(11) NOT NULL COMMENT 'Pagamintos produkcijos kiekis (vnt.)',
  `shift_id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `oee` float NOT NULL,
  `exploitation_factor` float NOT NULL COMMENT 'Išnaudojimo koefic',
  `operational_factor` float NOT NULL COMMENT 'Veiklos efektyvumo koefic',
  `quality_factor` float NOT NULL COMMENT 'Kokybės koefic',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `dashboards_calculation_mos`
--

DROP TABLE IF EXISTS `dashboards_calculation_mos`;
CREATE TABLE IF NOT EXISTS `dashboards_calculation_mos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `theory_prod_time` float NOT NULL COMMENT 'Teorinis gamybos laikas (min.)',
  `fact_prod_time` int(11) NOT NULL COMMENT 'Faktinis gamybos laikas (min.)',
  `total_quantity` int(11) NOT NULL COMMENT 'Pagamintos produkcijos kiekis (vnt.)',
  `shift_id` int(11) NOT NULL,
  `mo_number` int(11) NOT NULL,
  `oee` float NOT NULL,
  `exploitation_factor` float NOT NULL COMMENT 'Išnaudojimo koefic',
  `operational_factor` float NOT NULL COMMENT 'Veiklos efektyvumo koefic',
  `quality_factor` float NOT NULL COMMENT 'Kokybės koefic',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=64 ;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `dashboards_mailsends`
--

DROP TABLE IF EXISTS `dashboards_mailsends`;
CREATE TABLE IF NOT EXISTS `dashboards_mailsends` (
  `user_id` int(11) NOT NULL,
  `graph_id` int(11) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Sukurta duomenų struktūra lentelei `dashboards_settings`
--

DROP TABLE IF EXISTS `dashboards_settings`;
CREATE TABLE IF NOT EXISTS `dashboards_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `position` int(11) NOT NULL,
  `data` text CHARACTER SET utf8 NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subscribed_users` text CHARACTER SET utf8 NOT NULL COMMENT 'vartotoju id sarasas, kurie registruoja si grafika kaip prenumerata',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=53 ;
