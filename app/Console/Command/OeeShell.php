<?php

class OeeShell extends AppShell {

    public $uses = array('Shift', 'Sensor', 'Plan', 'Record', 'DashboardsCalculation', 'DashboardsCalculationMo', 'ApprovedOrder', 'FoundProblem', 'OrderCalculation','Problem','QualityFactor');
    public $shift;
    public $workWithMo = true;
    private $records = array('sensors' => array(), 'orders' => array());
    private $currentRecordValue;
    private $currentRecordType;
    private $cutSensorsLines = array();//[linija][] = sensorius surasoma kuriose linijose yra pjaustymo sensoriu
    private $settings=null;
    public $daysBack = 0;
    public $sensorsLines = array();
    public $searchFromShift = true;
	
    public $sensorsIds = array();	//skaiciavimai atliekami siems sensoriams
    private $completeOrders = array(); 

    const RecordDuration = 20; // Kas kiek laiko sukuriamas įrašas, sekundėmis
    
    public function __construct(){
        $this->sensorsLines = $this->Sensor->find('list', array('fields'=>array('id','line_id'), 'order'=>array('Sensor.id ASC'), 'group'=>array('Sensor.id')));
    }

    public function main() {
        // $this->shift = $this->Shift->findById(4694);
        // $workSensors = array(26);
        // $this->start_sensors_calculation($workSensors, false);
        // //$res = $this->calculateOrderStats(139560);
        // //pr($res);
        // die();
        ini_set('memory_limit', '-1');
        set_time_limit(2000);
//        $this->Record->setSource('records_archive');
        $this->daysBack = $this->daysBack > 0?$this->daysBack:1;
        //$this->removeRecords();
        $startPoint = strtotime('-'.($this->daysBack).' DAY');
        echo 'Perskaiciuota nuo '.date('z', strtotime($startPoint)).' dienos, '.date('W', strtotime($startPoint)).' savaites,'.date('m', strtotime($startPoint)).' menesio';
        $this->daysBack = min($this->daysBack, 13);
        //$this->Shift->bindModel(array('hasMany' => array('DashboardsCalculation')));
        $sensorsSearchParams = array(
            'fields' => array('Sensor.branch_id'),
            'order' => array('Sensor.id'),
            'conditions'=>array(
                'Sensor.pin_name <>'=>'',
                //'Sensor.id'=>2
            )
        );
		if(!empty($this->sensorsIds)){
			$sensorsSearchParams['conditions']['Sensor.id'] = $this->sensorsIds;
		}
        $params = array(&$sensorsSearchParams);
        $this->callPluginFunction('Oeeshell_beforeMain_Hook', $params, Configure::read('companyTitle'));
        $fullShifts = $this->Shift->find('all', array(//'fields'=>array('Shift.start','Shift.end','Shift.id'),
            'conditions' => array(
                'Shift.start >=' => date('Y-m-d', strtotime('-'.$this->daysBack.' day')),
                'Shift.end <'   => date('Y-m-d H:i'),
                //'Shift.id'=>8214
            ),
            'order'      => array('Shift.start')));
        //sensoriu sarasas
        $allSensorsTemp = $this->Sensor->find('list', $sensorsSearchParams);
        $allSensors = array();
        array_walk($allSensorsTemp, function ($branch, $sensor_id) use (&$allSensors) {
            $allSensors[$branch][] = $sensor_id;
        });
//        $this->log($allSensors);
        unset($allSensorsTemp);
        //apskaiciuotu darbo centru sarasas ismetamas is skaiciuojamojo saraso
        foreach ($fullShifts as $shift) {
            //$savedSensors = Set::extract('/sensor_id', $shift['DashboardsCalculation']); 
            //$savedSensors = array();
            //$workSensors = isset($allSensors[$shift['Shift']['branch_id']])?array_diff($allSensors[$shift['Shift']['branch_id']], $savedSensors):array();
            $workSensors = isset($allSensors[$shift['Shift']['branch_id']])?$allSensors[$shift['Shift']['branch_id']]:array();
            if(empty($workSensors)) continue;
            //unset($shift['DashboardsCalculation']);
            $this->shift = $shift;
            $this->fixFoundProblemsTimesByRecords($workSensors);//tvarko tik jei ner ADD_FULL_RECORD
            $this->start_sensors_calculation($workSensors, false);
        }
        for($i = $this->daysBack; $i > 0; $i--){
            $this->startDurationCalculation(date('Y-m-d',strtotime('-'.$i.' day')), date('Y-m-d',strtotime('-'.($i-1).' day')), 'day');
        }
        for($i = ceil($this->daysBack/7); $i > 0; $i--){
            $this->startDurationCalculation(date('Y-m-d',strtotime('-'.$i.' week',strtotime("monday this week"))), date('Y-m-d',strtotime('-'.($i-1).' week',strtotime("monday this week"))), 'week');
        }
        $this->startDurationCalculation(date('Y-m-d',strtotime("first day of last month")), date('Y-m-d',strtotime("first day of this month")), 'month');
        
        $this->Shift->query('DELETE FROM dashboards_calculations WHERE shift_id IS NULL');
        if(!isset($this->settings)) $this->settings = ClassRegistry::init('Settings');
        $sysName = $this->settings->getOne(Settings::S_REMOVE_EMPTY_SHIFTS_FROM_OEE);
        if($sysName != null && $sysName == '1') {
            $this->Shift->query('DELETE FROM dashboards_calculations WHERE fact_prod_time = 0 AND shift_length = 0');
        };
        $this->Shift->query('DELETE FROM dashboards_calculations WHERE fact_prod_time = 0 AND (oee != 0 OR operational_factor != 0) AND true_problem_time = 0 AND shift_length = 0');

        //die();
    }

    //$returnData -- bool grazinti duomenis ar ne
    public function start_sensors_calculation($sensors, $returnData) {
        $this->settings = ClassRegistry::init('Settings');
        //if(!$returnData) $this->updateSensorsRecords($sensors);
        $this->getTheoryTime($sensors);
        $this->getFactTime($sensors, $returnData);
        //$this->getQualityFactor($sensors);
        //$this->getMTTFR($sensors);
        //$this->calculateOEE();
        if ($returnData) return $this->records['sensors'];
        //issaugomi duomenys i db
        $this->DashboardsCalculation->create();
        //pr($this->records['sensors']);die();
        if (!empty($this->records['sensors'])) {
            foreach($this->records['sensors'] as $sensorData){
            	if($sensorData['true_problem_time'] == 0 && $sensorData['fact_prod_time']==0 && $sensorData['shift_length'] == 0){
					continue; //jei nera irasu uz pamainos laikotarpi, skaiciavimu neperrasome
				}
                $this->DashboardsCalculation->create();
                if($sensorData['shift_id'] > 0){
                    $this->DashboardsCalculation->deleteAll(array(
                        'DashboardsCalculation.sensor_id'=>$sensorData['sensor_id'], 
                        'DashboardsCalculation.shift_id'=>$sensorData['shift_id'], 
                    ));
                }
                $this->DashboardsCalculation->save($sensorData);
            }
        }
        $this->saveOrderData($this->records['orders']);

        // $mo_records = array(); //sumesime dvimati masyva i vienmati tam, kad galetume viska issaugoti per saveAll
        // array_walk($this->records['mo'], function ($val) use (&$mo_records) {
            // foreach ($val as $val_inner) {
                // $mo_records[] = $val_inner;
            // }
        // });


        // $shift = $this->shift;
        // $branch_id = $shift['Shift']['branch_id'];
        // $shift_st = $shift['Shift']['start'];
        // $shift_end = $shift['Shift']['end'];
        //$this->ApprovedOrder->bindModel(array('belongsTo' => array('Plan')));
        //$this->ApprovedOrder->bindModel(array('hasOne' => array('Line' => array('foreignKey' => false, 'conditions' => array('Plan.line_id = Line.id')))));
        // $orders_in_shift = $this->ApprovedOrder->find('all', array(
// //            'fields'     => array('id'),
            // 'conditions' => array(
                // 'ApprovedOrder.created <=' => $shift_end,
                // 'ApprovedOrder.end >='     => $shift_st,
               // // 'Line.branch_id'           => $branch_id
            // )
        // ));
        // foreach ($orders_in_shift as $ao) {
// 
// //            var_dump($shift_st = $shift['Shift']['start'],'shift start');
            // if (!in_array($ao['ApprovedOrder']['id'] . '_' . $ao['ApprovedOrder']['sensor_id'] . '_' . $shift['Shift']['id'], $this->completeOrders)) {
// //                var_dump($ao['ApprovedOrder']['id']);
// //                echo ($ao);
                // $res = $this->calculateOrderStats($ao['ApprovedOrder']['id'], $shift);
                // $res['shift_id'] = $shift['Shift']['id'];
// 
                // try {
                    // $this->OrderCalculation->create();
                    // $this->OrderCalculation->save($res);
                // } catch (Exception $ex) {
                    // //throw $ex;
                    // //CakeLog::write('debug','Err: '.json_encode($ex));
                // }
                // $this->completeOrders[] = $ao['ApprovedOrder']['id'] . '_' . $ao['ApprovedOrder']['sensor_id'] . '_' . $shift['Shift']['id'];
            // }
        // }
        $this->records = array('sensors' => array(), 'orders' => array());
    }

    public function saveOrderData($orders){
        foreach($orders as $sensorId => $ordersData){
            foreach($ordersData as $order){
                $approvedOrder = $this->ApprovedOrder->findById($order['Record']['approved_order_id']);
                if(!$approvedOrder) continue;
                $this->FoundProblem->bindModel(array('belongsTo'=>array('Problem')));
                $foundProblemsTmp = $this->FoundProblem->find('all', array(
                    'fields'=>array(
                        'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)) AS length',
                        'Problem.id',
                        'FoundProblem.transition_problem_id',
                    ),'conditions'=>array(
                        'FoundProblem.approved_order_id'=>$approvedOrder['ApprovedOrder']['id'],
                        'FoundProblem.shift_id'=>$this->shift['Shift']['id'],
                    ),
                    'group'=>array('Problem.id','FoundProblem.transition_problem_id')
                ));
                $foundProblems = array();
                foreach($foundProblemsTmp as $foundProblem){
                    $problemId = $foundProblem['FoundProblem']['transition_problem_id'] > 0?$foundProblem['FoundProblem']['transition_problem_id']:$foundProblem['Problem']['id'];
                    $foundProblems[$problemId] = $foundProblem[0];
                }
                unset($foundProblemsTmp);
                $oeePrimaryParameters = $order[0];
                $length = $order[0]['total_time'] - $order[0]['no_work_time'];
                $lengthExclusions = isset($order[0]['shift_time_exclusions'])?$order[0]['total_time'] - $order[0]['shift_time_exclusions']:$length;
                $oeePrimaryParameters['shift_length'] = $length;
                $oeePrimaryParameters['sensor_id'] = $sensorId;
                $oeePrimaryParameters['shift_length_with_exclusions'] = $lengthExclusions;
                $oeePrimaryParameters['fact_prod_time'] = $order[0]['fact_prod_time'];
                $oeePrimaryParameters['count_delay'] = $order['Plan']['step'] > 0 ? $order[0]['total_quantity'] / ($order['Plan']['step'] / 60) : 0;
                $oeeParams = $this->Record->calculateOee($oeePrimaryParameters);
                // MTTR MTTF
                //$mttf_mttr = $this->calculateOrderMTTRF($approved_order, $order_start, $order_end);
                $saveData = array(
                    'shift_id'=>$this->shift['Shift']['id'],
                    'sensor_id'=>$sensorId,
                    'mo_number'=>$order['Plan']['mo_number'],
                    'step'=>$order['Plan']['step'] > 0?$order['Plan']['step']:0,
                    'approved_order_id'=>$approvedOrder['ApprovedOrder']['id'],
                    'order_start'=>$order[0]['start'],
                    'order_end'=>$order[0]['end'],
                    'quantity'=>$order[0]['total_quantity'],
                    'confirmed_quantity'=>$approvedOrder['ApprovedOrder']['confirmed_quantity'],
                    'time_full'=>$length*60,
                    'time_full_with_exclusions'=>$lengthExclusions*60,
                    'time_green'=>$order[0]['fact_prod_time']*60,
                    'time_yellow'=>$order[0]['transition_time']*60,
                    'exceeded_time_yellow'=>$order[0]['exceeded_transition_time']*60,
                    'time_red'=>$order[0]['true_problem_time']*60,
                    'problems'=>json_encode($foundProblems),
                    'problem_count'=>is_array($order[0]['true_problems_ids']) && isset($order[0]['true_problems_ids'])?sizeof(array_unique($order[0]['true_problems_ids'])):sizeof(array_unique(explode(',',$order[0]['true_problems_ids']))),
                    'oee_oee'=>$oeeParams['oee'],
                    'oee_oee_with_exclusions'=>$oeeParams['oee_exclusions'],
                    'oee_avail'=>$oeeParams['exploitation_factor'],
                    'oee_avail_with_exclusions'=>$oeeParams['exploitation_factor_exclusions'],
                    'oee_perf'=>$oeeParams['operational_factor'],
                    'oee_quality'=>0,
                    'mttf'=>0,
                    'mttr'=>0,
                    'user_id'=>$order[0]['user_id'],
                );
                $params = array(&$saveData, &$order, &$approvedOrder);
                $this->callPluginFunction('Oeeshell_afterSaveOrderData_Hook', $params, Configure::read('companyTitle'));
                if($this->OrderCalculation->deleteAll(array('approved_order_id'=>$approvedOrder['ApprovedOrder']['id'], 'shift_id'=>$this->shift['Shift']['id'], 'sensor_id'=>$sensorId))) {
                    $this->OrderCalculation->create();
                    $this->OrderCalculation->save($saveData);
                }
            }
        }
    }

    //Teorinis gamybos laikas (min.)
    private function getTheoryTime($sensors) {
        $records = &$this->records;
        extract($this->shift['Shift']);
        $time = $this->Plan->find('all', array('fields' => array(
            'Plan.sensor_id',
            'Plan.mo_number',
            'Plan.start',
            'Plan.end',
            'SUM(TIMESTAMPDIFF(SECOND,
                CASE WHEN Plan.start > \'' . $start . '\' THEN Plan.start ELSE \'' . $start . '\' END,
                CASE WHEN Plan.end < \'' . $end . '\' THEN Plan.end ELSE \'' . $end . '\' END))/60 as sum'
            ), 'conditions' => array('Plan.end >=' => $start, 'Plan.start <=' => $end, 'Plan.sensor_id' => $sensors),
            'group'  => array('Plan.sensor_id', 'Plan.mo_number')));
//        $this->log(json_encode($time));
        array_walk($time, function ($val, $key, $data) use (&$records) {
            extract($data);
            $sensor_id = $val['Plan']['sensor_id'];
            $mo_number = $val['Plan']['mo_number'];
            if (!isset($records['sensors'][$sensor_id])) {
                $records['sensors'][$sensor_id] = $thisObj->fill_empty($sensor_id, $shift_id, $mo_number);
            }
            $records['sensors'][$sensor_id]['theory_prod_time'] += $val[0]['sum'];
            // if (!isset($records['mo'][$mo_number][$sensor_id]) && trim($mo_number) && $thisObj->workWithMo) {
                // $records['mo'][$mo_number][$sensor_id] = $thisObj->fill_empty($sensor_id, $shift_id, $mo_number);
            // }
            // if (trim($mo_number) && $thisObj->workWithMo) {
                // $records['mo'][$mo_number][$sensor_id]['theory_prod_time'] += round($val[0]['sum'], 4);
            // }
        }, array('shift_id' => $id, 'thisObj' => $this));
    }

    //Faktinis gamybos laikas (min.)
    private function getFactTime($sensors, $returnData=false) {
        $records = &$this->records;
        extract($this->shift['Shift']);
        $this->Record->bindModel(array('belongsTo' => array('FoundProblem', 'Plan', 'Sensor','Problem'=>array('foreignKey'=>false, 'conditions'=>array('Problem.id = FoundProblem.problem_id')))));
        $additional_fields = array();
        $shift_length = round((strtotime($this->shift['Shift']['end']) - strtotime($this->shift['Shift']['start'])) / 60);
        $recordBindModels = array('belongsTo' => array('FoundProblem', 'Plan', 'Sensor','Problem'=>array('foreignKey'=>false, 'conditions'=>array('Problem.id = FoundProblem.problem_id'))));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $plugin_model = 'ShiftTimeProblemExclusionsAppModel';
            $this->loadModel('ShiftTimeProblemExclusions.' . $plugin_model);
            $exclusionList = $plugin_model::getExclusionIdList();
            if (empty($exclusionList)) {$exclusionList = array(0);}
                $additional_fields[] = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . join(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS shift_time_exclusions';
                $additional_fields[] = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id >= '.FoundProblem::STATE_UNKNOWN.' AND FoundProblem.problem_id NOT IN (' . join(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS true_problem_time_exclusions';
                $additional_fields[] = 'GROUP_CONCAT(DISTINCT CASE WHEN FoundProblem.problem_id NOT IN (' . join(',', $exclusionList) . ') AND FoundProblem.problem_id >= '.FoundProblem::STATE_UNKNOWN.' AND FoundProblem.prev_shift_problem_id IS NULL THEN Record.found_problem_id END) AS problems_ids_without_exclusions';
        }else{ $exclusionList = array(0); }
        $params = array(&$additional_fields, &$recordBindModels);
        $this->callPluginFunction('Oeeshell_beforeSearchGetFactTime_Hook', $params, Configure::read('companyTitle'));
        $exceededTransitionId = $this->settings->getOne('exceeded_transition_id');
        $exceededTransitionId = $exceededTransitionId > 0?$exceededTransitionId:-1000;
        if($this->searchFromShift){
            $conditions = array(
                'Record.shift_id' => $id,
                'Record.sensor_id' => $sensors,
            );
        }else{
            $conditions = array(
                'Record.created >=' => $start,
                'Record.created <=' => $end,
                'Record.sensor_id' => $sensors,
            );
        }
        $this->Record->bindModel($recordBindModels);
        $time = $this->Record->find('all', array(
            'fields' => array_merge(array(
               // 'count(Record.id)',
                'Record.approved_order_id',
                'Record.shift_id',
                'Record.sensor_id',
                'Plan.mo_number',
                'Plan.box_quantity',
                'Plan.step',
                'Plan.id',
                'Sensor.type',
                'Sensor.calculate_quality',
                'TRIM(Record.user_id) AS user_id',
                'SUM('.Configure::read('recordsCycle').') AS total_time',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS fact_prod_time',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity),0)) as total_quantity',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id=1,' . (Configure::read('recordsCycle')) . ',0)) as transition_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id='.$exceededTransitionId.',' . (Configure::read('recordsCycle')) . ',0)) exceeded_transition_time',
                'GROUP_CONCAT( DISTINCT CASE WHEN FoundProblem.problem_id = 1 AND FoundProblem.prev_shift_problem_id IS NULL THEN FoundProblem.id END ) as transition_count',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id=2,' . (Configure::read('recordsCycle')) . ',0)) as no_work_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id=14,' . (Configure::read('recordsCycle')) . ',0)) as other_product_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id>=3,' . (Configure::read('recordsCycle')) . ',0)) as true_problem_time',
                'SUM(IF(Problem.mean_time_calculations = 1,' . (Configure::read('recordsCycle')) . ',0)) as mtt_problem_time',
                //'SUM(IF(Record.found_problem_id IS NULL AND Record.plan_id IS NOT NULL, IF(Sensor.plan_relate_quantity=\'quantity\', IF(Record.quantity < Plan.step/3600*'.Configure::read('recordsCycle').','.Configure::read('recordsCycle').',0), IF(Record.unit_quantity < Plan.step/3600*'.Configure::read('recordsCycle').','.Configure::read('recordsCycle').',0)),0)) AS slow_speed_duration',
                'MIN(Record.created) as start',
                'MAX(Record.created) as end',
                'GROUP_CONCAT(DISTINCT CASE WHEN FoundProblem.problem_id>=3 AND FoundProblem.prev_shift_problem_id IS NULL THEN Record.found_problem_id END) as true_problems_ids',
                'GROUP_CONCAT(DISTINCT CASE WHEN Problem.mean_time_calculations=1 AND FoundProblem.prev_shift_problem_id IS NULL THEN Record.found_problem_id END) as mtt_problems_ids'
            ), $additional_fields), 
             'conditions'=>$conditions,
             'group'  => array('Record.sensor_id', 'Record.approved_order_id'),
             'order'  => array('Record.sensor_id ASC')
        ));
        if(empty($time)) return;
        $params = array(&$time, &$this->shift, &$records, &$returnData, &$sensors, &$shift_length);
        $this->callPluginFunction('Oeeshell_filterGetFactTimeRecords_Hook', $params, Configure::read('companyTitle'));
        $this->Record->bindModel(array('belongsTo' => array('FoundProblem', 'Plan')));
        $slowSpeedSearch = $this->Record->find('all', array(
            'fields'=>array(
                'GROUP_CONCAT(IF(FoundProblem.problem_id IN ('.implode(',',$exclusionList).' OR Record.plan_id IS NULL),0,ROUND(Plan.step/60, 2))) AS purpose',
                'ROUND(SUM(IF(Sensor.plan_relate_quantity = \'quantity\',Record.quantity,Record.unit_quantity)),2) as quantity',
                'TRIM(Record.sensor_id) AS sensor_id',
                'TRIM(Record.plan_id) AS plan_id',
                'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\') AS groupper',
                'TRIM(Record.user_id) AS user_id'
            ),'conditions'=>array(  
                'Record.shift_id' => $id, 
                'Record.sensor_id' => $sensors,
                'Record.plan_id IS NOT NULL',
                'Record.found_problem_id IS NULL'
            ),'group'=>array('Record.sensor_id', 'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\')'),
            'order'=>array('Record.sensor_id','Record.created'),
        ));
        $usersCountByPlan = array();
        foreach($slowSpeedSearch as $slowSpeed){
            $planId = $slowSpeed[0]['plan_id'];
            $userId = $slowSpeed[0]['user_id'];
            if($userId > 0){
                if(!isset($usersCountByPlan[$planId][$userId])){ $usersCountByPlan[$planId][$userId] = 1; }else{ $usersCountByPlan[$planId][$userId]++; }
            }
            $purpose = max(explode(',', $slowSpeed[0]['purpose']));
            if($purpose > $slowSpeed[0]['quantity']){
                if(!isset($slowSpeedsBySensors[$slowSpeed[0]['sensor_id']])){ $slowSpeedsBySensors[$slowSpeed[0]['sensor_id']] = 0; }
                $slowSpeedsBySensors[$slowSpeed[0]['sensor_id']] += sizeof(explode(',', $slowSpeed[0]['purpose'])) * Configure::read('recordsCycle');
            }
        }
        $qualityQuantitiesInShift = $this->QualityFactor->calculateQualityInShift($time,$branch_id);
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $apprivedOrders = Hash::combine($this->ApprovedOrder->find('all', array('conditions'=>array('ApprovedOrder.id'=>Set::extract('/Record/approved_order_id', $time)))),'{n}.ApprovedOrder.id','{n}');
        foreach ($time as &$timeSingle) {
            $timeSingle[0]['total_time'] = $timeSingle[0]['total_time'] / 60;
            $timeSingle[0]['fact_prod_time'] = $timeSingle[0]['fact_prod_time'] / 60;
            $timeSingle[0]['transition_time'] = $timeSingle[0]['transition_time'] / 60;
            $timeSingle[0]['exceeded_transition_time'] = $timeSingle[0]['exceeded_transition_time'] / 60;
            $timeSingle[0]['no_work_time'] = $timeSingle[0]['no_work_time'] / 60;
            $timeSingle[0]['other_product_time'] = $timeSingle[0]['other_product_time'] / 60;
            $timeSingle[0]['true_problem_time'] = $timeSingle[0]['true_problem_time'] / 60;
            $timeSingle[0]['mtt_problem_time'] = $timeSingle[0]['mtt_problem_time'] / 60;
            $appOrder = isset($apprivedOrders[$timeSingle['Record']['approved_order_id']])?$apprivedOrders[$timeSingle['Record']['approved_order_id']]:array();
            if($appOrder){
                $timeSingle[0]['theorical_transition_time'] = min($timeSingle[0]['total_time'], ($appOrder['Plan']['preparation_time'] - min($appOrder['Plan']['preparation_time'], bcdiv(max(0, strtotime($this->shift['Shift']['start']) - strtotime($appOrder['ApprovedOrder']['created'])),60,6))));
            }else{
                $timeSingle[0]['theorical_transition_time'] = 0;
            }
            
            $planId = $timeSingle['Plan']['id'];
            if(isset($usersCountByPlan[$planId])){
                arsort($usersCountByPlan[$planId]);
                $timeSingle[0]['user_id'] = key($usersCountByPlan[$planId]);
            }else{
                //useris pareina pirmas pasitaikes is recordu, kai visoje pamainoje prie sio uzsakymo buvo tik probleminiai laikai. Ateity gali tekti pataisyti
            }
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                if (!empty($exclusionList)) {
                    $timeSingle[0]['shift_time_exclusions'] /= 60;
                    $timeSingle[0]['true_problem_time_exclusions'] /= 60;
                }
            }
        }

        $shift_id = $this->shift['Shift']['id'];
        $uniqueTransitions = array();     
        array_walk($time, function ($val, $key, $params) use (&$records, &$uniqueTransitions, $shift_length) {
            extract($params);
            $sensor_id = $val['Record']['sensor_id'];
            $mo_number = $val['Plan']['mo_number'];
            $line_id = isset($this->sensorsLines[$val['Record']['sensor_id']])?$this->sensorsLines[$val['Record']['sensor_id']]:0;
            $type = $val['Sensor']['type'];
            
            if (!isset($records['sensors'][$sensor_id])) {
                $records['sensors'][$sensor_id] = $thisObj->fill_empty($sensor_id, $shift_id, $mo_number);
            }
            // if (!isset($records['order'][$mo_number][$sensor_id]) && trim($mo_number) && $thisObj->workWithMo) {
                // $records['mo'][$mo_number][$sensor_id] = $thisObj->fill_empty($sensor_id, $shift_id, $mo_number);
            // }
            if($val['Record']['approved_order_id']){ $records['orders'][$sensor_id][] = $val; }
            $totalQuantity = $val[0]['total_quantity'];
            //gaunam kieki is pjaustymo sensoriu, jei tai pakavimo sensorius ir jis turi pjaustymo sensoriu, kuriu kiekiai didesni uz 0
            //$count = $thisObj->getCutSensorsCount($sortedData, $type, $mo_number, $line_id, $sensor_id);
            //$cutSensorCount = 0;
            // if ($previous_sensor == null) {
                // $previous_sensor = $sensor_id;
            // }
            if ($records['sensors'][$sensor_id]['shift_length_with_exclusions'] == 0) {
                $records['sensors'][$sensor_id]['shift_length_with_exclusions'] = $shift_length;
            }
            
            //skaiciuojame pagal sensorius
            $uniqueTransitions[$sensor_id] = !isset($uniqueTransitions[$sensor_id])?array():$uniqueTransitions[$sensor_id];
            if(trim($val[0]['transition_count'])){
                $uniqueTransitions[$sensor_id] = array_unique(array_merge($uniqueTransitions[$sensor_id], explode(',', $val[0]['transition_count'])));
            }
            $totalQuantity = (array_key_exists('total_quantity', $val[0])) ? $val[0]['total_quantity'] : 0;
            $records['sensors'][$sensor_id]['count_delay'] += $val['Plan']['step'] > 0?$totalQuantity / ($val['Plan']['step'] / 60):0;
            $records['sensors'][$sensor_id]['total_quantity'] += $totalQuantity;
            $records['sensors'][$sensor_id]['fact_prod_time'] += $val[0]['fact_prod_time'];
            $records['sensors'][$sensor_id]['transition_time'] += $val[0]['transition_time'];
            $records['sensors'][$sensor_id]['exceeded_transition_time'] += $val[0]['exceeded_transition_time'];
            $records['sensors'][$sensor_id]['transition_count'] = sizeof($uniqueTransitions[$sensor_id]);
            $records['sensors'][$sensor_id]['true_problem_time'] += $val[0]['true_problem_time']; //+ (0.3 * $val[0]['true_problem_count']);
            $records['sensors'][$sensor_id]['mtt_problem_time'] += $val[0]['mtt_problem_time'];
            //$records['sensors'][$sensor_id]['true_problem_count'] += $val[0]['true_problem_count'];
            $records['sensors'][$sensor_id]['no_work_time'] += $val[0]['no_work_time'];
            $records['sensors'][$sensor_id]['other_product_time'] += $val[0]['other_product_time'];
            $records['sensors'][$sensor_id]['shift_length'] = $shift_length;
            $records['sensors'][$sensor_id]['operational_factor'] = 0;
            $records['sensors'][$sensor_id]['exploitation_factor'] = 0;
            $records['sensors'][$sensor_id]['true_problems_ids'] = array_merge($records['sensors'][$sensor_id]['true_problems_ids'], array_filter(explode(',',$val[0]['true_problems_ids'])));
            $records['sensors'][$sensor_id]['mtt_problems_ids'] = array_merge($records['sensors'][$sensor_id]['mtt_problems_ids'], array_filter(explode(',',$val[0]['mtt_problems_ids'])));
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded()) && isset($val[0]['shift_time_exclusions'])) {
                $records['sensors'][$sensor_id]['shift_time_exclusions'] += $val[0]['shift_time_exclusions'];
                $records['sensors'][$sensor_id]['shift_length_with_exclusions'] -= $val[0]['shift_time_exclusions'];
                $records['sensors'][$sensor_id]['true_problem_time_exclusions'] += $val[0]['true_problem_time_exclusions'];
                $records['sensors'][$sensor_id]['problems_ids_without_exclusions'] = array_merge($records['sensors'][$sensor_id]['problems_ids_without_exclusions'], array_filter(explode(',',$val[0]['problems_ids_without_exclusions'])));
            }
            if(isset($val[0]['theory_prod_time']) && $val[0]['theory_prod_time'] > 0){
                $records['sensors'][$sensor_id]['theory_prod_time'] += $val[0]['theory_prod_time'];
            }
            $records['sensors'][$sensor_id]['theorical_transition_time'] += $val[0]['theorical_transition_time'];
            $params = array(&$records, &$val, $sensor_id);
            $this->callPluginFunction('Oeeshell_appendGetFactTimeLoop_Hook', $params, Configure::read('companyTitle'));
//            $records['sensors'][$sensor_id]['operational_factor'] += round($operational_factor_single_fraction, 4);
        }, array('shift_id' => $id, 'thisObj' => $this));
        $shift_length = $shift_length > 0?$shift_length:0;
        foreach($records['sensors'] as $sensor_id => &$record){
            $params = array(&$record, $sensor_id);
            $this->callPluginFunction('Oeeshell_beforeLastGetFactTimeLoop_Hook', $params, Configure::read('companyTitle'));
            $records['sensors'][$sensor_id]['true_problems_ids'] = isset($records['sensors'][$sensor_id]['true_problems_ids'])?$records['sensors'][$sensor_id]['true_problems_ids']:array();
            $records['sensors'][$sensor_id]['true_problem_count'] = sizeof(array_unique($records['sensors'][$sensor_id]['true_problems_ids']));
            $records['sensors'][$sensor_id]['mtt_problems_ids'] = isset($records['sensors'][$sensor_id]['mtt_problems_ids'])?$records['sensors'][$sensor_id]['mtt_problems_ids']:array();
            $records['sensors'][$sensor_id]['mtt_problem_count'] = sizeof(array_unique($records['sensors'][$sensor_id]['mtt_problems_ids']));
            unset($records['sensors'][$sensor_id]['true_problems_ids']);
            $record['sensor_id'] = $sensor_id;
            $record['count_delay'] = isset($record['count_delay'])?$record['count_delay']:0;
            $record['theory_prod_time'] = isset($record['theory_prod_time'])?$record['theory_prod_time']:0;
            $record['fact_prod_time'] = isset($record['fact_prod_time'])?$record['fact_prod_time']:0;
            $record['total_quantity'] = isset($record['total_quantity'])?$record['total_quantity']:0;
            $record['shift_length'] = isset($record['shift_length'])?$record['shift_length']:0;
            $record['shift_length_with_exclusions'] = isset($record['shift_length_with_exclusions'])?$record['shift_length_with_exclusions']:0;
            $record['transition_time'] = isset($record['transition_time'])?$record['transition_time']:0;
            $record['exceeded_transition_time'] = isset($record['exceeded_transition_time'])?$record['exceeded_transition_time']:0;
            $record['true_problem_time'] = isset($record['true_problem_time'])?$record['true_problem_time']:0;
            $record['mtt_problem_time'] = isset($record['mtt_problem_time'])?$record['mtt_problem_time']:0;
            $record['slow_speed_duration'] = isset($slowSpeedsBySensors[$sensor_id])?bcdiv($slowSpeedsBySensors[$sensor_id],60,6):0;
            $record['no_work_time'] = isset($record['no_work_time'])?$record['no_work_time']:0;
            $record['other_product_time'] = isset($record['other_product_time'])?$record['other_product_time']:0;
            $oeeParams = $this->Record->calculateOee($record);
            if($oeeParams['operational_factor'] == 1){ //reiskia per plugina imonei nurodytas efektyvumas = 1 arba neskaiciuojamas, t.y. OEE lygus prieinamumui
                $record['count_delay'] = $record['fact_prod_time']; //sulyginame sias reiksmes, kad skydeliuose dinamiskai skaiciuojant OEE efektyvumas taptu lygus 1 (pagal formule efektyvymas skaiciuoajsi count_delay / fact_prod_time)
            }
            $records['sensors'][$sensor_id]['operational_factor'] = $oeeParams['operational_factor'];//efektyvumas
            $records['sensors'][$sensor_id]['exploitation_factor'] = $oeeParams['exploitation_factor']; //prieininamumas arba availability
            if(isset($record['shift_length_with_exclusions']) && $record['shift_length_with_exclusions']){
                $records['sensors'][$sensor_id]['exploitation_factor_with_exclusions'] = $oeeParams['exploitation_factor_exclusions'];
            }
            $records['sensors'][$sensor_id]['oee'] = $oeeParams['oee'];
            $records['sensors'][$sensor_id]['oee_with_exclusions'] = $oeeParams['oee_exclusions'];
            if(isset($records['sensors'][$sensor_id]['true_problem_time_exclusions'])){
                $records['sensors'][$sensor_id]['true_problem_count_exclusions'] = isset($records['sensors'][$sensor_id]['problems_ids_without_exclusions'])?sizeof(array_unique($records['sensors'][$sensor_id]['problems_ids_without_exclusions'])):0;
                unset($records['sensors'][$sensor_id]['problems_ids_without_exclusions']);
                $records['sensors'][$sensor_id]['mttr'] = $records['sensors'][$sensor_id]['true_problem_count_exclusions'] > 0?$records['sensors'][$sensor_id]['true_problem_time_exclusions'] / $records['sensors'][$sensor_id]['true_problem_count_exclusions']:0;
                $records['sensors'][$sensor_id]['mttf'] = $records['sensors'][$sensor_id]['true_problem_count_exclusions'] > 0?$records['sensors'][$sensor_id]['fact_prod_time'] / $records['sensors'][$sensor_id]['true_problem_count_exclusions']:0;
            }
            $record['good_quantity'] = isset($qualityQuantitiesInShift[$sensor_id])?$qualityQuantitiesInShift[$sensor_id]['good_quantities']:0;
            $record['all_quantity'] = isset($qualityQuantitiesInShift[$sensor_id])?$qualityQuantitiesInShift[$sensor_id]['all_quantities']:0;
        }
    }

    //gaunmas pakavimo sensoriaus linijoje esancio pjaustymo sensoriaus kiekis
    public function getCutSensorsCount($sortedData, $type, $mo_number, $line_id, $sensor_id) {
        if (isset($this->cutSensorsLines[$line_id]) && $type == 3) { //jei egzistuoja esamam sensoriui pjaustymo sensorius ir pats sensorius yra pakavimo, t.y.type=3
            foreach ($this->cutSensorsLines[$line_id] as $cutSensorId) { //pjaustymo sensoriai gali buti keli vienam sensoriui
                if (isset($sortedData[$cutSensorId][$mo_number][0]['total_quantity']) && $sortedData[$cutSensorId][$mo_number][0]['total_quantity'] > 0) {
                    return $sortedData[$cutSensorId][$mo_number][0]['total_quantity']; //graziname pjaustymo sensoriaus kieki vietoje pakavimo sensoriaus
                }
            }
        }

        return 0;
    }

    //masyvas uzpildomas is anksto numatytomis tusciomis reiksmemis
    public function fill_empty($sensor_id, $shift_id, $mo_number) {
        $emptyList = array(
            'operational_factor'           => 0,
            'fact_prod_time'               => 0,
            'total_quantity'               => 0,
            'theory_prod_time'             => 0,
            'quality_factor'               => 0,
            'sensor_id'                    => $sensor_id,
            'shift_id'                     => $shift_id,
            'shift_length'                 => 0,
            'shift_length_with_exclusions' => 0,
            'mo_number'                    => $mo_number,
            'transition_time'              => 0,
            'exceeded_transition_time'     => 0,
            'transition_count'             => 0,
            'true_problem_time'            => 0,
            'mtt_problem_time'             => 0,
            'slow_speed_duration'          => 0,
            'true_problem_count'           => 0,
            'no_work_time'                 => 0,
            'other_product_time'           => 0,
            'type2_quantity'               => 0,
            'total_quantity'               =>0,
            'fact_prod_time'                =>0,
            'transition_time'               =>0,
            'exceeded_transition_time'      =>0,
            'transition_count'              =>0,
            'true_problem_time'             =>0,
            'true_problems_ids'             =>array(),
            'mtt_problems_ids'              =>array(),
            'no_work_time'                  =>0,
            'other_product_time'            =>0,
            'count_delay'                   =>0,
            'theorical_transition_time'    =>0,
        );
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $emptyList['shift_time_exclusions'] = 0;
            $emptyList['shift_length_with_exclusions'] = 0;
            $emptyList['exploitation_factor_with_exclusions'] = 0;
            $emptyList['true_problem_time_exclusions'] = 0;
            $emptyList['problems_ids_without_exclusions'] = array();
        }

        return $emptyList;
    }

    //apskaiciuojamas kokybes faktorius
    private function getQualityFactor($sensors) {
//        $this->log("getQualityFactor");
        $records = &$this->records;
        extract($this->shift['Shift']);
        $this->Record->bindModel(array('belongsTo' => array('Sensor', 'Plan')));
        $time = $this->Record->find('all', array('fields' => array(
            'Record.sensor_id',
            'Plan.mo_number',
            'SUM(IF(Sensor.type=2,Record.unit_quantity,0)) as type2',
            'SUM(IF(Sensor.type=3,Record.unit_quantity,0)) as type3',
//        ), 'conditions' => array('Record.shift_id' => $id, 'Record.sensor_id' => array(15,16), 'Sensor.type', ),
        ), 'conditions'                                   => array('Record.shift_id' => $id, 'Record.sensor_id' => $sensors, 'Sensor.type',),
                                                 'group'  => array('Record.sensor_id', 'Plan.mo_number')
        ));
//        $this->log("time: ".json_encode($time));
        $qualitySumsBySensor = array(); //nuo cia pasalinsime irasus kur nebuvo pjaustymo(type2) ir likusius irasus susumuosime pagal sensorius
//        $this->log(json_encode($time));

        $qualitySumsByLine = array();

        foreach ($time as $val) {
            $sensor_id = $val['Record']['sensor_id'];
            if ($val[0]['type2'] == 0 && $val[0]['type3'] == 0) {
                continue;
            }
            $mo_number = $val['Plan']['mo_number'];
            $line_id = $this->sensorsLines[$sensor_id];
            $emptyValues = array('type2' => 0, 'type3' => 0);
            if (!isset($qualitySumsBySensor['sensors'][$sensor_id])) $qualitySumsBySensor['sensors'][$sensor_id] = $emptyValues;
            if (!isset($qualitySumsBySensor['mo'][$mo_number][$sensor_id])) $qualitySumsBySensor['mo'][$mo_number][$sensor_id] = $emptyValues;
            if (!isset($qualitySumsByLine[$line_id])) $qualitySumsByLine[$line_id] = $emptyValues;
            //skaiciuojame pagal sensorius
            $qualitySumsBySensor['sensors'][$sensor_id]['type2'] += $val[0]['type2'];
            $qualitySumsBySensor['sensors'][$sensor_id]['type3'] += $val[0]['type3'];
            $qualitySumsByLine[$line_id]['type2'] += $val[0]['type2'];
            $qualitySumsByLine[$line_id]['type3'] += $val[0]['type3'];
            if ($val[0]['type3'] != 0) {
                $qualitySumsByLine[$line_id]['packing_sensor_id'] = $sensor_id;
            }
            //skaiciuojame pagal mo numerius
            if ($this->workWithMo) {
                $qualitySumsBySensor['mo'][$mo_number][$sensor_id]['type2'] += $val[0]['type2'];
                $qualitySumsBySensor['mo'][$mo_number][$sensor_id]['type3'] += $val[0]['type3'];
            }
        }

        foreach ($qualitySumsByLine as $line_results) {
            if (!array_key_exists('packing_sensor_id', $line_results)) {
                continue;
            }
            if (!array_key_exists($line_results['packing_sensor_id'], $records['sensors'])) {
                continue;
            }
            if ($line_results['type2'] != 0) {
                $records['sensors'][$line_results['packing_sensor_id']]['quality_factor'] = $line_results['type3'] / $line_results['type2'];
            } else {
                $records['sensors'][$line_results['packing_sensor_id']]['quality_factor'] = 0;
            }
        }

        if (isset($qualitySumsBySensor['mo']) && $this->workWithMo) {
            //echo "Calculating quality koefs";
            array_walk($qualitySumsBySensor['mo'], function ($val, $mo_number) use (&$records) { //cia rasime pati kokybes koeficineta kiekvienam sensoriui
                $type2_sum = 0;
                $type3_sum = 0;
                foreach ($val as $sensor_values) {
                    $type2_sum += $sensor_values['type2'];
                    $type3_sum += $sensor_values['type3'];
                }
                array_walk($val, function ($val, $sensor_id) use (&$records, &$mo_number, $type3_sum, $type2_sum) { //cia rasime pati kokybes koeficineta kiekvienam sensoriui
                    if (isset($mo_number) && $type2_sum != 0) {
                        $records['mo'][$mo_number][$sensor_id]['quality_factor'] = $type3_sum / $type2_sum;
                    } else {
                        $records['mo'][$mo_number][$sensor_id]['quality_factor'] = 1;
                    }
                }, array(compact('mo_number')));
            });
        }
    }


    private function getMTTFR($sensors) {
//        extract($this->shift['Shift']);
/*        foreach ($sensors as $sensor_id) {

            $tmpModel = "TmpTable";
            $tmpTable = "tmp_table";
            $this->Record->query('
                DROP TEMPORARY TABLE IF EXISTS `' . $tmpTable . '`;
                CREATE TEMPORARY TABLE `' . $tmpTable . '` ENGINE=INNODB AS (
                    SELECT
                            UNIX_TIMESTAMP(MIN(records.created)) as start,
                            UNIX_TIMESTAMP(MAX(records.created)) as end,
                            found_problem_id,
                            (TIMESTAMPDIFF(SECOND,MIN(records.created),MAX(records.created)))+38 as duration,
                            COUNT(*) record_count
                    FROM records
                    LEFT JOIN found_problems ON found_problems.id = records.found_problem_id
                    WHERE
                            records.sensor_id=' . $sensor_id . '
                            AND records.shift_id=' . $this->shift['Shift']['id'] . '
                            AND records.found_problem_id IS NOT NULL
                            AND found_problems.problem_id >= 3

                    GROUP BY records.found_problem_id
                    ORDER BY records.found_problem_id


                );
            ');

            $tmpModelObj = ClassRegistry::init($tmpModel);
            $tmpModelObj->useTable = $tmpTable;

            $results = $tmpModelObj->find('all', array(
                'order' => 'found_problem_id ASC'
            ));

            $durations = array();
            $differences_between_problems = array();
            $mttf = 0;
            $mttr = 0;
            $previous_end = 0;
            if (!empty($results)) {
                foreach ($results as $result) {
                    if ($previous_end == 0 && $result[$tmpModel]['start'] > strtotime($this->shift['Shift']['start'])) {
                        $previous_end = strtotime($this->shift['Shift']['start']) - 19;
                    }
                    if ($previous_end != 0) {
                        $diff = $result[$tmpModel]['start'] - $previous_end - 38;
                        $differences_between_problems[] = $diff;
                        $mttf += $diff;
                    }
                    $durations[] = $result[$tmpModel]['duration'];
                    $mttr += $result[$tmpModel]['duration'];
                    $previous_end = $result[$tmpModel]['end'];
                    if (end($results) === $result && $result[$tmpModel]['end'] < strtotime($this->shift['Shift']['end'])) { // If last, add difference till next shift
                        $mttf += strtotime($this->shift['Shift']['end']) - $result[$tmpModel]['end'];
                    }
                }
                if (count($differences_between_problems) != 0) {
                    $mttf /= (count($differences_between_problems));
                }
                if (count($durations) != 0) {
                    $mttr /= count($durations);
                }
            }

            $records = &$this->records;

            if($mttf) $records['sensors'][$sensor_id]['mttf'] = $mttf / 60;
            if($mttr) $records['sensors'][$sensor_id]['mttr'] = $mttr / 60;

        }*/
    }

    //paruosiamas OEE skaiciavimas atskiriant sensoriu ir mo skirsni
    private function calculateOEE() {
        foreach ($this->records as $type => &$records) {
            $this->currentRecordType = $type;
            if ($type == 'sensors') {
                $this->currentRecordValue = $records;
                array_walk($records, array($this, 'finalOEECalculation'), compact('type'));
                $records = $this->currentRecordValue;
            }
        }
    }

    //paskutinis oee skaiciavimo etapas
    private function finalOEECalculation(&$val, $identifier, $data) {
        if (!array_key_exists('exploitation_factor', $val)) return;
        if ($this->currentRecordType != "mo") {
            //$params = array(&$val);
            //$this->callPluginFunction('recalcOeeHook', $params, Configure::read('companyTitle'));
            $this->currentRecordValue[$identifier] = array_merge($this->currentRecordValue[$identifier], $val);
            $this->currentRecordValue[$identifier]['oee'] = 100 * $val['exploitation_factor'] * $val['operational_factor'];
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded()) && isset($val['exploitation_factor_with_exclusions'])) {
                $this->currentRecordValue[$identifier]['oee_with_exclusions'] = 100 * $val['exploitation_factor_with_exclusions'] * $val['operational_factor'];
            }
        }
    }

    public function callPluginFunction($methodTitle, &$parameters, $companyName){
        //kviesime plugino modelyje esancia funkcija
        $companyPluginRoot = ROOT.DS.'app'.DS.'Plugin'.DS.$companyName;
        if(file_exists($companyPluginRoot.DS.'Model')){
            $extensionFiles = scandir($companyPluginRoot.DS.'Model');
            $extensionFiles = array_filter($extensionFiles, function($val){return strpos($val, '.') && !strpos($val, 'AppModel');});
            foreach($extensionFiles as $modelFile){
                $companyMainPluginModel = ClassRegistry::init($companyName.'.'.str_replace('.php','',$modelFile));
                if(method_exists($companyMainPluginModel,$methodTitle)) {
                    return call_user_func_array(array($companyMainPluginModel, $methodTitle), $parameters);
                }
            }
        }
    }
    
    public function calculateOrderMTTRF($approved_order, $start, $end) {
        $tmpModel = "TmpTable";
        $tmpTable = "tmp_table";
        $this->Record->query('
                DROP TEMPORARY TABLE IF EXISTS `' . $tmpTable . '`;
                CREATE TEMPORARY TABLE `' . $tmpTable . '` ENGINE=INNODB AS (
                    SELECT
                            UNIX_TIMESTAMP(MIN(records.created)) as start,
                            UNIX_TIMESTAMP(MAX(records.created)) as end,
                            found_problem_id,
                            (TIMESTAMPDIFF(SECOND,MIN(records.created),MAX(records.created))) as duration,
                            COUNT(*) record_count
                    FROM records
                    LEFT JOIN found_problems ON found_problems.id = records.found_problem_id
                    WHERE
                            records.sensor_id=' . $approved_order['ApprovedOrder']['sensor_id'] . '
                            AND records.created >= "' . $start . '"
                            AND records.created <= "' . $end . '"
                            AND records.found_problem_id IS NOT NULL
                            AND found_problems.problem_id >= 3

                    GROUP BY records.found_problem_id
                    ORDER BY records.found_problem_id
                );
            ');

        $tmpModelObj = ClassRegistry::init($tmpModel);
        $tmpModelObj->useTable = $tmpTable;

        $results = $tmpModelObj->find('all', array(
            'order' => 'found_problem_id ASC'
        ));

        $durations = array();
        $differences_between_problems = array();
        $mttf = 0;
        $mttr = 0;
        $previous_end = 0;
        if (!empty($results)) {

            foreach ($results as $result) {
                if ($previous_end != 0) {
                    $diff = $result[$tmpModel]['start'] - $previous_end;
                    $differences_between_problems[] = $diff;
                    $mttf += $diff;
                }
                $durations[] = $result[$tmpModel]['duration'];
                $mttr += $result[$tmpModel]['duration'];
                $previous_end = $result[$tmpModel]['end'];
            }
            $mttf /= (count($differences_between_problems) + 1);
            $mttr /= count($durations);
        }

        return array('mttf' => ($mttf / 60), 'mttr' => ($mttr / 60));
    }

    // public function calculateOrderStats($approved_order_id = 0) {
        // $this->ApprovedOrder->bindModel(array('belongsTo' => array('Plan','Sensor')));
        // $approved_order = $this->ApprovedOrder->find('first', array(
            // 'conditions' => array(
                // 'ApprovedOrder.id' => $approved_order_id
            // )
        // ));
        // // $last_approved_order = $this->ApprovedOrder->find('first', array(
            // // 'conditions' => array('end <='    => $approved_order['ApprovedOrder']['created'],
                                  // // 'sensor_id' => $approved_order['ApprovedOrder']['sensor_id']),
            // // 'order'      => 'id DESC'
        // // ));
// 
        // $order_start = $approved_order['ApprovedOrder']['created'];
        // // if ($last_approved_order) {
            // // $order_start = $last_approved_order['ApprovedOrder']['end'];
        // // }
        // $order_end = $approved_order['ApprovedOrder']['end'];
// 
        // unset($this->Record->virtualFields['time']);
        // $this->Record->bindModel(array('belongsTo' => array('FoundProblem')));
        // $this->Record->bindModel(array('belongsTo' => array('Problem' => array('foreignKey' => false, 'conditions' => 'FoundProblem.problem_id = Problem.id'))));
        // $records_between = $this->Record->find('all', array(
            // 'conditions' => array(
                // //'Record.created >=' => $order_start,
                // //'Record.created <=' => $order_end,
                // 'Record.approved_order_id' => $approved_order['ApprovedOrder']['id'],
                // 'Record.sensor_id' => $approved_order['ApprovedOrder']['sensor_id']
            // ),
            // 'order'=>array('Record.created ASC'),
            // 'group'      => array()
        // ));
        // // $clean_record_max = 0;
        // // foreach ($records_between as $r) {
            // // if ($r['Record']['found_problem_id'] == null && $r['Record']['created'] > $clean_record_max) {
                // // $clean_record_max = $r['Record']['created'];
            // // }
        // // }
// 
        // // Template
        // $results = array(
            // 'sensor'            => $approved_order['Sensor']['id'],
            // 'sensor_type'       => $approved_order['Sensor']['type'],
            // 'approved_order_id' => $approved_order_id,
            // 'plan_id' => $approved_order['Plan']['id'],
            // 'total_quantity'     => 0,
            // 'sum'               => array(
                // 'full'   => 0,
                // 'green'  => 0,
                // 'yellow' => 0,
                // 'red'    => 0,
                // 'exclusion_time'    => 0,
            // ),
            // 'problems'          => array(),
            // 'problem_count'     => 0,
            // 'oee'               => array(
                // 'oee'   => 0,
                // 'avail' => 0,
                // 'perf'  => 0
            // )
        // );
        // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            // $results['sum']['full_with_exclusions'] = 0;
            // $results['sum']['exclusion_time'] = 0;
            // $results['oee']['avail_with_exclusions'] = 0;
            // $results['oee']['oee_with_exclusions'] = 0;
            // $plugin_model = 'ShiftTimeProblemExclusionsAppModel';
            // $this->loadModel('ShiftTimeProblemExclusions.' . $plugin_model);
            // $exclusionList = $plugin_model::getExclusionIdList();
        // }
        // $problems_added = array();
// 
        // /*$fp = array();
        // if ($last_approved_order) {
            // $fp = $this->FoundProblem->withRefs()->find('all', array(
                // 'conditions' => array(
                    // 'or'        => array(
                        // array('FoundProblem.start >= ' => $last_approved_order['ApprovedOrder']['created'], 'FoundProblem.problem_id' => array(1, 2)),
                        // array('FoundProblem.start >=' => $last_approved_order['ApprovedOrder']['end'], 'FoundProblem.problem_id !=' => array(1, 2))
                    // ),
                    // 'FoundProblem.end <='    => $approved_order['ApprovedOrder']['created'],
                    // 'FoundProblem.sensor_id' => $approved_order['ApprovedOrder']['sensor_id'],
// //                    'problem_id !=' => Problem::ID_NO_WORK
                // ),
                // 'order'      => 'FoundProblem.id ASC'
            // ));
        // }
// //        pr($fp);
        // if (!empty($fp)) {
            // foreach ($fp as &$fp_single) {
                // if ($fp_single['FoundProblem']['end'] < $last_approved_order['ApprovedOrder']['end'] && !in_array($fp_single['Problem']['id'], array(1, 2))) {
// //                    pr($fp_single,'skipping');
                    // continue;
                // }
                // if ($fp_single['FoundProblem']['start'] < $last_approved_order['ApprovedOrder']['end'] && !in_array($fp_single['Problem']['id'], array(1, 2))) {
                    // $fp_single['FoundProblem']['start'] = $last_approved_order['ApprovedOrder']['end'];
                // }
                // if (strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']) < 0 && !in_array($fp_single['Problem']['id'], array(1, 2))) {
                    // continue;
                // }
// 
                // if ($fp_single['FoundProblem']['start'] < $order_start) {
                    // $order_start = $fp_single['FoundProblem']['start'];
                // }
// 
                // $problem_id = $fp_single['FoundProblem']['problem_id'];
                // //$results['sum']['yellow'] += strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']);
                // $problems_added[] = $fp_single['FoundProblem']['id'];
                // if (!array_key_exists($problem_id, $results['problems'])) {
                    // $results['problems'][$problem_id] = array(
                        // 'name'   => $fp_single['Problem']['name'],
                        // 'length' => 0
                    // );
                // }
                // $results['problems'][$problem_id]['length'] += strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']);
                // //$results['sum']['full'] += strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']);
                // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                    // if (in_array($fp_single['FoundProblem']['problem_id'], $exclusionList)) {
                        // $results['sum']['exclusion_time'] += strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']);
                    // }
                // }
                // if ($fp_single['Problem']['id'] != 1) {
                    // $results['problem_count'] += 1;
                // }
            // }
        // }
        // // End tarpas tarp praeito užsakymo ir dabartinio
         // */
// 
        // $found_problems = array();
        // foreach ($records_between as $rec) {
            // // if ($rec['Record']['created'] > $clean_record_max) {
                // // if ($rec['Record']['found_problem_id'] != null && in_array($rec['FoundProblem']['problem_id'], array(1, 2))) {
                    // // if ($rec['FoundProblem']['start'] < $order_end) {
                        // // $order_end = $rec['FoundProblem']['start'];
                    // // }
                // // }
                // // continue;
            // // }
            // $order_start = strtotime($rec['Record']['created']) < strtotime($order_start)?$rec['Record']['created']:$order_start;
            // $order_end = strtotime($rec['Record']['created']) > strtotime($order_end)?$rec['Record']['created']:$order_end;
            // // Quantity
            // $results['total_quantity'] += $rec['Record']['unit_quantity'];
            // $problem_id = $rec['FoundProblem']['problem_id'];
            // if ($problem_id == Problem::ID_NO_WORK) {
                // continue;
            // }
            // $results['sum']['full'] += Configure::read('recordsCycle');
            // if ($rec['Record']['found_problem_id'] == null) {
                // $results['sum']['green'] += Configure::read('recordsCycle');
                // continue;
            // }
            // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                // if (in_array($rec['FoundProblem']['problem_id'], $exclusionList)) {
                    // $results['sum']['exclusion_time'] += Configure::read('recordsCycle');
                // }
            // }
            // if (in_array($rec['FoundProblem']['id'], $problems_added)) {
                // continue;
            // }
            // if ($rec['FoundProblem']['problem_id'] == 1) {
                // $results['sum']['yellow'] += Configure::read('recordsCycle');
            // } else if ($rec['FoundProblem']['problem_id'] > 2) {
                // $results['sum']['red'] += Configure::read('recordsCycle');
                // if (!in_array($rec['Record']['found_problem_id'], $found_problems)) {
                    // $results['problem_count'] += 1;
                    // $found_problems[] = $rec['Record']['found_problem_id'];
                // }
            // }
            // if (!array_key_exists($problem_id, $results['problems'])) {
                // $results['problems'][$problem_id] = array(
                    // 'name'   => $rec['Problem']['name'],
                    // 'length' => 0
                // );
            // }
            // $results['problems'][$problem_id]['length'] += Configure::read('recordsCycle');
        // }
        // $params = array(&$results, &$this->shift);
        // $this->callPluginFunction('Oeeshell_filterOrderStatsRecords_Hook', $params, Configure::read('companyTitle'));
        // $quantity = $approved_order['ApprovedOrder']['quantity'];
        // $confirmed_quantity = $approved_order['ApprovedOrder']['confirmed_quantity'];
        // // Koefs
        // $oeePrimaryParameters['shift_length'] = $results['sum']['full'];
        // $oeePrimaryParameters['shift_length_with_exclusions'] = $results['sum']['full'] - $results['sum']['exclusion_time'];
        // $oeePrimaryParameters['fact_prod_time'] = $results['sum']['green'];
        // $oeePrimaryParameters['count_delay'] = $results['total_quantity'] / ($approved_order['Plan']['step'] / 3600);
        // $oeePrimaryParameters['transition_time'] = $results['sum']['yellow'];
        // $oeeParams = $this->Record->calculateOee($oeePrimaryParameters);
//         
        // // if ($results['sum']['full'] != 0) {
            // // $results['oee']['avail'] = $results['sum']['green'] / $results['sum']['full'];
            // // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                // // $results['sum']['full_with_exclusions'] = ($results['sum']['full'] - $results['sum']['exclusion_time']);
                // // if($results['sum']['full_with_exclusions'] != 0) {
                    // // $results['oee']['avail_with_exclusions'] = $results['sum']['green'] / $results['sum']['full_with_exclusions'];
                // // }
            // // }
        // // }
        // // if ($results['sum']['green'] != 0 && $approved_order['Plan']['step'] != 0) {
            // // $results['oee']['perf'] = ($results['unit_quantity'] / $results['sum']['green']) / ($approved_order['Plan']['step'] / 3600);
        // // }
        // // $results['oee']['oee'] = $results['oee']['avail'] * $results['oee']['perf'];
        // // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            // // $results['oee']['oee_with_exclusions'] = $results['oee']['avail_with_exclusions'] * $results['oee']['perf'];
        // // }
// 
        // // MTTR MTTF
        // $results['mean_times'] = $this->calculateOrderMTTRF($approved_order, $order_start, $order_end);
// 
        // $data = array(
            // 'sensor_id'          => $approved_order['ApprovedOrder']['sensor_id'],
            // 'mo_number'          => $approved_order['Plan']['mo_number'],
            // 'step'               => $approved_order['Plan']['step'],
            // 'approved_order_id'  => $approved_order['ApprovedOrder']['id'],
            // 'order_start'        => $order_start,
            // 'order_end'          => $order_end,
            // 'quantity'           => $results['total_quantity'],
            // 'confirmed_quantity' => $confirmed_quantity,
            // 'time_full'          => $results['sum']['full'],
            // 'time_full_with_exclusions' => $oeePrimaryParameters['shift_length_with_exclusions'],
            // 'time_green'         => $results['sum']['green'],
            // 'time_yellow'        => $results['sum']['yellow'],
            // 'time_red'           => $results['sum']['red'],
            // 'problems'           => json_encode($results['problems']),
            // 'problem_count'      => $results['problem_count'],
            // 'oee_oee'            => $oeeParams['oee'],
            // 'oee_oee_with_exclusions'=>$oeeParams['oee_exclusions'],
            // 'oee_avail'          => $oeeParams['exploitation_factor'],
            // 'oee_avail_with_exclusions'=>$oeeParams['exploitation_factor_exclusions'],
            // 'oee_perf'           => $oeeParams['operational_factor'],
            // 'oee_quality'        => 0,
            // 'mttr'               => $results['mean_times']['mttr'],
            // 'mttf'               => $results['mean_times']['mttf'],
        // );
// 
        // // if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            // // $data['oee_oee_with_exclusions'] = $results['oee']['oee_with_exclusions'];
            // // $data['oee_avail_with_exclusions'] = $results['oee']['avail_with_exclusions'];
            // // $data['time_full_with_exclusions'] = $oeePrimaryParameters['shift_length_with_exclusions'];
        // // }
// 
        // return $data;
    // }

    //Prieš skaičiuojant OEE patikrinama ar toje pamainoje sensoriui yra įrašų kurių quantity > [0] ir approved_order_id = [null] ir found_problem_id=null. 
    //Jeigu tokių įrašų yra, sukuriamas approved_order įrašas, o jo ID priskiriamas visiems tos pamainos to sensoriaus neapdorotiems įrašams 
    private function updateSensorsRecords($sensors){
        //gaunamas sarasas sensoriu, kuriuose yra neapdorotu irasu su kiekiu (neturi approved orderio)
        $conditions = array(
            'Record.sensor_id' => $sensors,
            'Record.shift_id'  => $this->shift['Shift']['id'],
            'Record.quantity >' => 0,
            'Record.found_problem_id IS NULL',
            'Record.approved_order_id IS NULL',
        );
        $unPreparedSensors = $this->Record->find('all', array(
            'fields'=>array('Record.sensor_id', 'MIN(Record.created) AS min', 'MAX(Record.created) AS max', 'SUM(quantity) AS sum'),
            'recursive'=>-1, 'conditions'=>$conditions,'group'=>array('Record.sensor_id'))
        );
        foreach($unPreparedSensors as $sensor){
            if($sensor[0]['min'] == $sensor[0]['max']){
                continue;
            }
            $conditions['Record.sensor_id'] = $sensor['Record']['sensor_id'];
            $this->ApprovedOrder->create();
            $this->ApprovedOrder->save(array(
                'sensor_id' => $sensor['Record']['sensor_id'],
                'plan_id' => 1,
                'status_id' => 2,
                'created' => $sensor[0]['min'],
                'end' => $sensor[0]['max'],
                'quantity' => $sensor[0]['sum'],
                'box_quantity'=>$sensor[0]['sum'],
                'additional_data'=>json_encode(array('Sukurtas OEE skaiciavimo metu'))
            ));
            $this->Record->updateAll(
                array('Record.approved_order_id' => $this->ApprovedOrder->id, 'Record.plan_id'=>1),
                $conditions
            );
        }
    }

    private function startDurationCalculation($start, $end, $type){
        $types = array('week'=>'W', 'month'=>'m','day'=>'z');
        //$start = date('Y', strtotime($start)) != date('Y')?date('Y').'-01-01':$start;
        $dateValue = (int)date($types[$type], strtotime($end)-1);
        $this->DashboardsCalculation->deleteAll(array(
            $type => $dateValue,
            'year' => date('Y', strtotime($end))
        ));
//        $lastCalculcated = $this->DashboardsCalculation->find('first', array('conditions'=>array(
//            'DashboardsCalculation.year' => date('Y', strtotime($start)),
//            'DashboardsCalculation.'.$type => $dateValue
//        )));
//        if($lastCalculcated) return array();
        $this->Shift->bindModel(array('hasMany' => array('DashboardsCalculation'=>array('fields'=>array('id')))));
        $shiftsSearchParams = array(
            'fields'=>array('Shift.id'),
            'conditions'=>array('Shift.start >=' => $start, 'Shift.start <' => $end, 'Shift.disabled'=>0)
        );
        //hoksas gali buti naudojamas periodu skaiciavimuose kai reikia pamainas itraukti pagal ju pabaigas, o ne pagal pradzias
        $params = array(&$shiftsSearchParams, &$start, &$end);
        $this->callPluginFunction('Oeeshell_beforeStartDurationCalculation_Hook', $params, Configure::read('companyTitle'));
        $shifts = $this->Shift->find('all', $shiftsSearchParams);
        $shiftsList = array();
        foreach($shifts as $shift){
            if(empty($shift['DashboardsCalculation'])){
                 $hasQuantities = $this->Record->find('first', array(
                    'fields'=>array('COUNT(Record.id) as record_count'),
                    'conditions'=>array('Record.shift_id'=>$shift['Shift']['id'], 'Record.quantity >'=>0)));
                 if(isset($hasQuantities[0]['count']) && $hasQuantities[0]['count'] >= 20){ //reiskia pamaina dirbama, vadinasi turi ten buti irasu kieki. Ieskome bent 20, nes gali buti siaip keli trugdziai
                    return array(); //ne visos pamainos yra apskaiciuotos, lauksime kol apskaiciuos
                 }
            }
            $shiftsList[] = $shift['Shift']['id'];
        }
        $fields = array_keys($this->DashboardsCalculation->schema());
		unset($fields[array_search('id', $fields)]);
        array_walk($fields, function(&$col){ $col = 'SUM('.$col.') AS '.$col; });
        $params = array(&$fields);
        $this->callPluginFunction('Oeeshell_beforeStartDurationCalculation2_Hook', $params, Configure::read('companyTitle'));
        $results = $this->DashboardsCalculation->find('all',array(
            'fields'=>array_merge($fields, array(
                'TRIM(DashboardsCalculation.sensor_id) AS sensor_id',
                'AVG(DashboardsCalculation.mttf) AS mttf',
                'AVG(DashboardsCalculation.mttr) AS mttr',
            )),
            'conditions'=>array(
                'DashboardsCalculation.shift_id'=>$shiftsList
            ),'group'=>array('DashboardsCalculation.sensor_id')
        ));
        foreach($results as &$result){
            $oeeParams = $this->Record->calculateOee($result[0]);
            $result[0]['operational_factor'] = $oeeParams['operational_factor'];
            $result[0]['exploitation_factor'] = $oeeParams['exploitation_factor'];
            $result[0]['exploitation_factor_with_exclusions'] = $oeeParams['exploitation_factor_exclusions'];
            $result[0]['oee'] = $oeeParams['oee'];
            $result[0]['oee_with_exclusions'] = $oeeParams['oee_exclusions'];
            // if ($result[0]['fact_prod_time'] > 0) {
                // $result[0]['operational_factor'] = round($result[0]['count_delay'] / $result[0]['fact_prod_time'], 3);//efektyvumas
            // }else $result[0]['operational_factor'] = 0;
            // $result[0]['exploitation_factor'] = $result[0]['shift_length'] > 0?round($result[0]['fact_prod_time'] / $result[0]['shift_length'], 3):0;
            // $result[0]['exploitation_factor_with_exclusions'] = $result[0]['shift_length_with_exclusions'] > 0?round($result[0]['fact_prod_time'] / $result[0]['shift_length_with_exclusions'], 3):0;
            // $result[0]['oee'] = 100 * $result[0]['exploitation_factor'] * $result[0]['operational_factor'];
            // $result[0]['oee_with_exclusions'] = 100 * $result[0]['exploitation_factor_with_exclusions'] * $result[0]['operational_factor'];
            $result[0]['shift_id'] = 0;
            $result[0]['year'] = date('Y', strtotime($start));
            $result[0]['shift_included'] = implode(',', $shiftsList).'; pradzia: '.$start.'; pabaiga: '.$end;
            $result[0]['calc_for'] = date('Y-m-d', strtotime($start));
            $result[0][$type] = $dateValue;
            $result = $result[0];
        }
        if(!empty($results)){
            $this->DashboardsCalculation->saveAll($results);
        }
    }

    /*public function removeRecords(){
        $startPoint = strtotime('-'.($this->daysBack).' DAY');
        $startPoint = date('Y', $startPoint) != date('Y')?date('Y').'-01-01':date('Y-m-d',$startPoint);
        //$shiftsToRemove = $this->Shift->find('list', array('fields'=>array('id'), 'conditions'=>array('Shift.start >='=>$startPoint), 'order'=>array('Shift.id')));
        //$this->DashboardsCalculation->deleteAll(array('shift_id'=>$shiftsToRemove));
        //$this->DashboardsCalculation->deleteAll(array('day >='=>date('z', strtotime($startPoint)), 'year'=>date('Y', strtotime($startPoint)) ));
        $this->DashboardsCalculation->deleteAll(array('week >='=>(int)date('W', strtotime($startPoint)), 'year'=>date('Y', strtotime($startPoint)) ));
        //$this->DashboardsCalculation->deleteAll(array('month >='=>(int)date('m', strtotime($startPoint)), 'year'=>date('Y', strtotime($startPoint)) ));
        //echo 'Perskaiciuota nuo '.$startPoint.' dienos. Ieinancios pamainos: '.implode(',', $shiftsToRemove).'. <br />Perskaiciuota nuo '.date('z', strtotime($startPoint)).' dienos, '.date('W', strtotime($startPoint)).' savaites,'.date('m', strtotime($startPoint)).' menesio';
    }*/
    
    public function fixFoundProblemsTimesByRecords($workSensors){
        $params = array();
        //if($this->callPluginFunction('Oeeshell_skipFixFoundProblemsTimesByRecords_Hook', $params, Configure::read('companyTitle')) === true){
        if(Configure::read('ADD_FULL_RECORD')){
            return true;
        }
        $tmpModel = "TmpTable2";
        $tmpTable = "tmp_table2";
        foreach($workSensors as $sensorId){
             $sql = '
            SET @currJobNr := 0, @prevProblem := -1, @prevCreated := NULL; 
            DROP TEMPORARY TABLE IF EXISTS `' . $tmpTable . '`;
            CREATE TEMPORARY TABLE `' . $tmpTable . '` ENGINE=INNODB AS (
                SELECT 
                    plans.id AS plan_id, 
                    plans.mo_number, 
                    SUM(tbl1.time_diff) / 60 AS minutes, 
                    tbl1.created, 
                    found_problems.start AS problem_start,
                    found_problems.end AS problem_end,
                    tbl1.found_problem_id,
                    tbl1.job_nr,
                    tbl1.approved_order_id,
                    sum(tbl1.quantity) AS quantity,
                    sum(tbl1.unit_quantity) AS unit_quantity
                    FROM (
                        SELECT 
                            `records`.plan_id,
                            `records`.sensor_id,
                            `records`.approved_order_id,
                            `records`.quantity,
                            `records`.unit_quantity,
                            IF(@prevCreated IS NULL, 20, TIMESTAMPDIFF(SECOND,@prevCreated,`records`.created)) as time_diff,
                            CASE WHEN `records`.found_problem_id IS NULL AND IFNULL(`records`.found_problem_id,-1) != @prevProblem
                                    THEN @currJobNr := @currJobNr + 1 
                            END as exp1,
                            CASE WHEN `records`.found_problem_id IS NULL THEN CONCAT(\'order_\',@currJobNr) ELSE `records`.found_problem_id END AS job_nr,
                            @prevProblem := IFNULL(`records`.found_problem_id,-1) AS exp2,
                            @prevCreated := `records`.created AS exp3,
                            `records`.found_problem_id,
                            `records`.created 
                        FROM `records` 
                        WHERE 
                            `records`.shift_id = '.$this->shift['Shift']['id'].' AND `records`.sensor_id = '.$sensorId.' AND user_id > 0
                        ORDER BY `records`.created
                    ) AS tbl1 
                    LEFT JOIN plans ON(plans.id = tbl1.plan_id)
                    LEFT JOIN found_problems ON(found_problems.id = tbl1.found_problem_id)
                    GROUP BY tbl1.job_nr
                    ORDER BY tbl1.created
            )';
            $this->Record->query($sql);
            $tmpModelObj = ClassRegistry::init($tmpModel);
            $tmpModelObj->useTable = $tmpTable;
            $data = $tmpModelObj->find('all');
            if($data){
                $this->FoundProblem->deleteAll(array('FoundProblem.problem_id'=>-1, 'FoundProblem.shift_id'=>$this->shift['Shift']['id'], 'FoundProblem.sensor_id'=>$sensorId));
                $this->FoundProblem->deleteAll(array('FoundProblem.start > FoundProblem.end', 'FoundProblem.shift_id'=>$this->shift['Shift']['id'], 'FoundProblem.sensor_id'=>$sensorId));
                foreach($data as $event){
                    $end = strtotime($event[$tmpModel]['created']) + round($event[$tmpModel]['minutes']*60) - 1;
                    $saveData = array(
                            'start' => $event[$tmpModel]['created'],
                            'end' => date('Y-m-d H:i:s', $end),
                            'problem_id'=>-1,//darbas
                            'super_parent_problem_id'=>-1,
                            'sensor_id'=>$sensorId,
                            'shift_id'=>$this->shift['Shift']['id'],
                            'plan_id'=>$event[$tmpModel]['plan_id'],
                            'state'=>FoundProblem::STATE_COMPLETE,
                            'renew'=>1
                        );
                    if($event[$tmpModel]['found_problem_id']){ //kai dirbame su problema, tiksliname jos laikus
                        //jei nesutampa uzfiksuotos problemos pradzia/pabaiga su is records irasu gautais duomenimis
                        if(strtotime($event[$tmpModel]['created']) != strtotime($event[$tmpModel]['problem_start']) || $end != strtotime($event[$tmpModel]['problem_end']) ){
                            if(!trim($event[$tmpModel]['problem_start'])){//jei problema neturi starto found_problem lentelej, vadinas ten nera iraso visai, kazkaras negerai, sukursime rankiniu budu
                                $this->FoundProblem->create();
                                $saveData['problem_id'] = Problem::ID_NOT_DEFINED;
                                $this->FoundProblem->save($saveData);
                            }else{
                                $this->FoundProblem->id = $event[$tmpModel]['found_problem_id'];
                                $dataBefore = $this->FoundProblem->read();
                                $this->FoundProblem->save(array(
                                        'start' =>$event[$tmpModel]['created'],
                                        'end' => date('Y-m-d H:i:s', $end),
                                        'renew'=>json_encode($dataBefore)
                                ));
                            }
                        }
                    }elseif(trim($event[$tmpModel]['approved_order_id'])){//kai yra darbas vykdysime iterpima i found_problems lentele, kad gautume darba tenai, taciau jis tures problem_id = -1
                        $this->FoundProblem->create();
                        $this->FoundProblem->save($saveData);
                    }
                }
            }
        }
    }

}