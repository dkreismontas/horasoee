<?php
App::uses('SettingsController', 'Controller');
App::uses('ComponentCollection', 'Controller');
App::uses('HelpComponent', 'Controller/Component');
App::uses('CakeEmail','Network/Email');

class SubscriptionShell extends AppShell {
	public $uses = array('DashboardsSetting','DashboardsMailsend','Dashboard','User','Setting','Shift','Sensor','StatisticalReportsSubscription','StatisticalReportsMailsend','DashboardsCalculation');
	public $components = array('Help');
	private $dataContainer=array();		//duomenu masyvas, i kuri rasomi irasai apie busima siunciama laiska
	private $settings=array();		//duomenu bazeje settings lenteleje esantys parametrai
	private $usersMails=array();	//visu duomenu bazeje esanciu vartotoju sarasas [id]=[email]

	public function main(){
//$data = array(262=>array('2019-10-16 06:00:00','2019-10-17 05:59:59'));	    
		$this->settings = $this->Setting->find('list',array('fields'=>array('Setting.key','Setting.value')));
		$Collection = new ComponentCollection();
		$this->Help = new HelpComponent($Collection);
        $this->usersMails = Hash::combine($this->User->find('all',array('conditions'=>array('User.email <> \'\''))), '{n}.User.id', '{n}.User');
		$this->checkDashboardGraphs();
		$this->checkStatisticalReportsGraphs();
	}

	private function checkDashboardGraphs(){
        $graphs = $this->DashboardsSetting->find('all',array('conditions'=>array('DashboardsSetting.subscribed_users <> \'\'')));
        foreach($graphs as $graph){
            $data = unserialize($graph['DashboardsSetting']['data']);
            if(!in_array($data['Node']['time_pattern'], $this->Dashboard->timeGroupsForSubcribsions)) continue;
            $sensorsBranches = array_unique($this->Sensor->find('list', array('fields'=>array('id','branch_id'), 'conditions'=>array('Sensor.id'=>$data['Node']['sensors']))));
            //ieskome ar esama pamaina kertasi tarp vakarykstes ir dabartines dienos. Jei taip reikes palaukti kol ji uzsibaigs.
            if($data['Node']['time_group'] != 1){ //pamainu susikirtimo ieskome tik jei grafikas ne pamaininis
                $currTime = date('Y-m-d H:i:s');
                $currentShiftCrissCross = $this->Shift->find('first', array('conditions'=>array(
                    'Shift.start <=' => $currTime,
                    'Shift.end >=' => $currTime,
                    'Shift.branch_id'=>$sensorsBranches,
                    'Shift.disabled'=>0,
                    'DATE_FORMAT(Shift.start, \'%Y-%d\')'=>date('Y-d', strtotime('-1 day')),
                    'DATE_FORMAT(Shift.end, \'%Y-%d\')'=>date('Y-d'),
                )));
                if($currentShiftCrissCross) continue;
            }
            list($start,$end) = $this->Help->setStartEndByPattern($data['Node']);
            $users = explode(',',$graph['DashboardsSetting']['subscribed_users']);
            foreach($users as $user_id){
                $conditions = array(
                    'user_id'=> $user_id,
                    'graph_id'=> $graph['DashboardsSetting']['id'],
                    'start'=> $start,
                    'end'=> $end,
                );
                $this->adjustConditions($conditions, $data['Node'], $start, $end);
                $is_sended = $this->DashboardsMailsend->find('first',array('conditions'=>$conditions));
                if(!empty($is_sended)) continue;
                $this->DashboardsMailsend->create();
                $this->DashboardsMailsend->save($conditions);
                $this->dataContainer[$user_id][$graph['DashboardsSetting']['id']] = array('name'=>$data['Node']['graph_name'],'start'=>$start,'end'=>$end);
            }
        }
        $this->sendMails();
    }

	private function sendMails(){
		$pathArray = array_reverse(explode(DIRECTORY_SEPARATOR,dirname(__FILE__)));
		$currentProjectName = $pathArray[3];
        if(Configure::read('sendEmails') === false) {
            return;
        }
		foreach($this->dataContainer as $user_id => $graphData){
		    if(!isset($this->usersMails[$user_id])) continue;
            CakeSession::write("Config.language", $this->usersMails[$user_id]['language']);
			$mailText = '<h3 style="text-align:center;">'.__('Suformuoti grafikai %s OEE sistemoje', Configure::read('companyTitle')).'</h3><br />';
			foreach($graphData as $graph_id => $data){
				$infoCoded = array($graph_id=>array($data['start'],$data['end']));
				//$link = Router::url('/'.$currentProjectName.'/dashboards/inner/subscription:'.base64_encode(serialize($infoCoded)),true);
                $link = Router::url('/dashboards/inner/subscription:'.base64_encode(serialize($infoCoded)),true);
				$mailText .= '<p><label style="display: min-width: 150px;">'.__('Grafiko pavadinimas:').'</label> <strong>';
				if($data['name']) {
                    $mailText .= $data['name'];
                } else {
                    $mailText .= __("Be pavadinimo");
                }
				$mailText .= '</strong></p>';
				$mailText .= '<p><label style="min-width: 150px;">'.__('Laikotarpis:').'</label> <strong>'.$data['start'].' - '.$data['end'].'</strong></p>';
				$mailText .= '<p><label style="min-width: 150px;">'.__('Nuoroda:').'</label> <a target="_blank" href="'.$link.'">';
				if($data['name']) {
                    $mailText .= $data['name'];
                } else {
                    $mailText .= __("Grafikas");
                }
				$mailText .= '</a></p>';
			}
            //var_dump(json_encode($this->usersMails[$user_id]));
            require_once(__DIR__.'/../../Config/email.php');
            $emailsList = explode(',', $this->usersMails[$user_id]['email']);
            $emailsList = array_map(function($email){ return trim($email); }, $emailsList);
            $mailConfig = new EmailConfig();
			$mail = new CakeEmail('default');
			$mail->from(isset($mailConfig->default['from_email'])?$mailConfig->default['from_email']:'noreply@horasoee.eu');
			$mail->to($emailsList);
			$mail->subject(__('OEE sistemos grafikų prenumerata'));
			$mail->emailFormat('both');
			$mail->send($mailText);
		}
	}

    private function adjustConditions(&$conditions, $data, $start, $end){
        set_time_limit(5);
        if(!in_array($data['time_pattern'], array(11))) return false;
        $groupper = 'Y-m-d';//paru grupavimas
        $jumper = '+1 day';
        unset($conditions['end']);
        switch($data['time_group']){
            case 1: //pamaininis grupavimas
                $shiftsIncluded = $this->Shift->find('list', array('fields'=>array('id'), 'conditions'=>array(
                    'Shift.start >=' => $start,
                    'Shift.end <=' => $end,
                    'Shift.disabled' => 0,
                )));
                $conditions['time_groupper'] = implode(',', $shiftsIncluded);
                return;
            break;
            case 3: //savaitinis grupavimas
                $groupper = 'Y/W';
                $jumper = '+1 week';
            break;
            case 4: //menesinis grupavimas
                $groupper = 'Y-m';
                $jumper = '+1 month';
            break;
        }
        $collection = array();
        $start = strtotime($start);
        $end = strtotime($end);
        while($start <= $end){
            $collection[] = date($groupper, $start);
            $start = strtotime($jumper, $start);
        }
        $conditions['time_groupper'] = implode(',', $collection);
    }

    private function checkStatisticalReportsGraphs(){
        $this->StatisticalReportsSubscription->bindModel(array('belongsTo'=>array('User')));
        $graphs = $this->StatisticalReportsSubscription->find('all',array('conditions'=>array(
            'StatisticalReportsSubscription.subscribed'=>1,
            'User.email <> \'\''
        ),'order'=>array('StatisticalReportsSubscription.id')));
        $this->dataContainer = array();
        foreach($graphs as $graph){
            set_time_limit(600);
            $data = json_decode($graph['StatisticalReportsSubscription']['data'], true);
            list($start,$end) = $this->Help->setStartEndByPattern($data);
            $branchesIds = isset($data['sensors'])?$this->Sensor->find('list', array('fields'=>array('Sensor.branch_id','Sensor.branch_id'),'conditions'=>array('Sensor.id'=>$data['sensors']))):array();
            $shiftsEndedInCurrentDay = $this->Shift->find('list', array(
                'fields'=>array('Shift.id','Shift.branch_id'),
                'conditions'=>array('Shift.end BETWEEN \''.date('Y-m-d').'\' AND \''.date('Y-m-d H:i:s').'\' ', 'Shift.branch_id'=>$branchesIds)
            ));
			if(!empty(array_diff($branchesIds, $shiftsEndedInCurrentDay))){ continue; } //reiskia grafikas turi sensoriu is padalinio, kuriam dar nera uzsibaigusi pamaina esamoje dienoje, todel siusti dar negalima
            // //siusime ataskaita tik tada, jei intervale yra bent viena pasibaigusi pamaina
            // $atLeastOneCalculated = $this->DashboardsCalculation->find('first', array('conditions'=>array('DashboardsCalculation.shift_id'=>$shiftsInsideInterval)));
            // if(empty($atLeastOneCalculated)){ continue; }
            $conditions = array(
                'user_id'=> $graph['StatisticalReportsSubscription']['user_id'],
                'report_id'=> $graph['StatisticalReportsSubscription']['id'],
                'start'=> $start,
            );
            if(isset($data['time_pattern']) && !in_array($data['time_pattern'], array(6,7,8,9,10))){//jei laiko intervalo ribos yra aiskios, t.y nuo - iki zinoma, pabaiga pridedame
                $conditions['end'] = $end;
            }else{//jei intervalo pabaiga yra dabar, t.y. nuolat kintanti, grafika siunciame kas nurodyta periodiskuma
                $conditions['periodicity'] = $this->constructPeriodicity($graph['StatisticalReportsSubscription']);
            }
            $isSended = $this->StatisticalReportsMailsend->find('first',array('conditions'=>$conditions));
            if(!empty($isSended)) continue;
            $conditions['created'] = date('Y-m-d H:i:s');
            if($graph['StatisticalReportsSubscription']['periodicity'] == 0){//kai periodiskumas yra pamaininis, patikrinam ar praejusi pamaina nebuvo isjungta, tokiu atveju siusti nereikia
                $enabledShifts = $this->Shift->find('list', array(
                    'fields'=>array('Shift.id','Shift.branch_id'),
                    'conditions'=>array('Shift.branch_id'=>$branchesIds,'Shift.start'=>$start, 'Shift.end'=>$end,'Shift.disabled'=>0)
                ));
                if(empty($enabledShifts)){ continue; }
            }
            $this->requestAction(array('controller'=>'dashboards', 'action'=>'send_statistical_subscription_graph'), array('data'=>array('subscribtionId'=>$graph['StatisticalReportsSubscription']['id'])));
            $this->StatisticalReportsMailsend->create();
            $this->StatisticalReportsMailsend->save($conditions);
        }
    }

    private function constructPeriodicity($graph){
        switch($graph['periodicity']){
            case 0: //Pamaininis
                $conditions = array();
                $settings = json_decode($graph['data'],true);
                if(isset($settings['sensors'])){
                    $conditions['Sensor.id'] = $settings['sensors'];
                }
                $sensorData = $this->Sensor->find('first', array('conditions'=>$conditions, 'recursive'=>-1, 'order'=>array('Sensor.id')));
                if(empty($sensorData)){
                    $currentShift['Shift']['id'] = 0;
                }else{
                    $currentShift = $this->Shift->findCurrent($sensorData['Sensor']['branch_id']);
                }
                return 'Shift'.$currentShift['Shift']['id'];
            break;
            case 1: //Paromis
                return 'Y'.date('Y').'D'.date('d');
            break;
            case 2: //Savaitemis
                return 'Y'.date('Y').'W'.date('W');
            break;
            case 3: //Menesiais
                return 'Y'.date('Y').'M'.date('m');
            break;
            default: return '';
        }
    }

}
