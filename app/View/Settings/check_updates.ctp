<?php
if($user->id == 1){
    echo '<div class="row-fluid">';
    echo $this->Form->create();
    echo $this->Form->input('update_info', array('type'=>'textarea','label'=>__('Informacija apie atnaujinimą').$this->App->addLanguages($possibleLanguages),'class' => 'form-control', 'div' => array('class' => 'form-group col-md-6')));
    echo $this->Form->input('instruction', array('type'=>'textarea','label'=>__('Naudojimo instrukcija').$this->App->addLanguages($possibleLanguages),'class' => 'form-control', 'div' => array('class' => 'form-group col-md-6')));
    echo '<div class="row-fluid">';
    echo $this->Html->link(__('Kitas atnaujinimas'), array('controller'=>'settings','action'=>'check_updates'), array('class'=>'btn btn-primary','style'=>'font-size: 13px; float:right'));
    echo $this->Form->submit(__('Išsaugoti'),array('class'=>'btn btn-primary','div'=>false));
    echo '</div>';
    echo $this->Form->end();
    echo '<br /></div>';
} ?>
<table class="table table-bordered table-hover updates-table">
    <thead><tr>
        <th><?php echo __('Atnaujinimo data'); ?></th>
        <th><?php echo __('Informacija apie atnaujinimą'); ?></th>
        <th><?php echo __('Naudojimo instrukcija'); ?></th>
        <th></th>
    </tr></thead>
<?php
foreach($updatesList as $update){
    $class = '';
    $links = array();
    if(!in_array($update['created'],$confirmedUpdates) && $user->id != 1){
        $links[] = $this->Html->link(__('Susipažinau'), array('confirmed'=>$update['created']));
        $class = 'unread';
    }
    if($user->id == 1){
        $links[] = $this->Html->link(__('Redaguoti'), array('edit'=>$update['created']));
        $links[] = $this->Html->link(__('Pašalinti'), array('delete'=>$update['created']),array(),__('Ar tikrai norite pašalinti šį atnaujinimo aprašymą?'));
    }
    $html = '<tr class="'.$class.'"><td>%s</td><td>%s</td><td>%s</td><td>%s</td></tr>';
    echo sprintf($html, 
        date('Y-m-d H:i',$update['created']), 
        str_replace("\r\n",'<br />',Settings::translate($update['update_info'])), 
        str_replace("\r\n",'<br />',Settings::translate($update['instruction'])),
        implode(' | ',$links)
    );
}
?></table>