<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>

<form method="POST">
    <!--input type="file" id="qr-input-file" accept="image/*" capture-->
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th style="width: 1%;"><?php echo __('Pavadinimas'); ?></th>
				<th><?php echo __('Reikšmė'); ?></th>
				<th><?php echo __('Aprašymas'); ?></th>
			</tr>
		</thead>
		<tbody>
		    <tr>
                <td style="white-space: nowrap;"><label><?php echo __('OEE perskaičiavimai'); ?></label></td>
                <td>
                   <?php echo __('Už kiek dienų perskaičiuoti OEE duomenis?'); ?>
                </td>
                <td><?php
                    $m = $this->Session->flash('oeeRecalSussess'); echo trim($m)?'<p>'.$m.'</p>':'';
                    for($i = 1; $i < 7; $i++){
                        echo $this->Html->link($i, array('controller'=>'cron', 'action'=>'calcOee', $i, 1), array('class'=>'btn', 'style'=>'border: 1px solid #ccc;')).'&nbsp;';
                    }  
                ?></td>
            </tr>
			<?php foreach ($list as $li_): $li = (object) $li_[$model]; ?>
			<tr>
				<td width="30%" style="white-space: nowrap;"><label><?php echo ($li->name ? Settings::translate(__(trim($li->name))) : __($li->key)); ?></label></td>
				<td width="30%">
					<input type="hidden" name="<?php echo htmlspecialchars($li->key) ?>[id]" value="<?php echo htmlspecialchars($li->id); ?>" />
					<input type="hidden" name="<?php echo htmlspecialchars($li->key) ?>[key]" value="<?php echo htmlspecialchars($li->key); ?>" />
					<input type="text" class="form-control" name="<?php echo htmlspecialchars($li->key) ?>[value]" value="<?php echo htmlspecialchars($li->value); ?>" />
				</td>
				<td width="40%"><p class="help-block"><?php echo $li->description ? __(trim($li->description)) : '&nbsp;'; ?></p></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
</form>
