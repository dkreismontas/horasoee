<?php
$pageTitle = __('Įskiepiai');
$this->set('page_title', $pageTitle);
$this->Html->addCrumb($pageTitle);
echo $this->Session->flash();
?>
<div class="row-fluid">
	<div class="span12">
		<table class="table table-bordered table-hover">
			<thead>
			<tr class="">
				<th><?php echo __('Įskiepio pavadinimas'); ?></th>
				<th><?php echo __('Įskiepio aprašymas'); ?></th>
				<th><?php echo __('Sukūrimo data'); ?></th>
				<th><?php echo __('Įmonė'); ?></th>
				<th><?php echo __('Įjungti /išjungti'); ?></th>
			</tr>
			</thead>
			<?php foreach($plugins as $plugin){ ?>
				<tr class="<?php echo $plugin['enabled']?'green':'red'; ?>">
					<td><?php echo $plugin['name']; ?></td>
					<td><?php echo $plugin['description']; ?></td>
					<td><?php echo $plugin['created']; ?></td>
					<td><?php echo $plugin['company']; ?></td>
					<td width="20%">
						<a class="changePluginStatus btn <?php echo $plugin['enabled']?'btn-danger':'btn-primary'; ?>" data-name="<?php echo $plugin['name']; ?>" title="<?php echo $plugin['enabled']?__('Išjungti'):__('Įjungti'); ?>" style="cursor: pointer;">
							<i class="<?php echo $plugin['enabled']?'icon-ok':'icon-ban-circle'; ?>"></i> 
							<span class="status"><?php echo $plugin['enabled']?__('Išjungtas'):__('Įjungtas'); ?></span>
						</a>
					</td>
				</tr>
			<?php } ?>
		</table>
	</div>
</div>

<script type="text/javascript">
	jQuery(document).ready(function($){
		$('.changePluginStatus').bind('click', function(){
			$.ajax({
				url: '<?php echo $this->Html->url('/settings/controll_plugins',true) ?>',
				type: 'POST',
				data: {name: $(this).data('name')},
				context: $(this),
				success: function(){
					var iconStatusChange = $(this).find('i');
					$(this).find('.status').html( !iconStatusChange.hasClass('icon-ok')?'<?php echo __('Išjungtas') ?>':'<?php echo __('Įjungtas') ?>');
					iconStatusChange.toggleClass('icon-ban-circle icon-ok');
					$(this).toggleClass('btn-primary btn-danger');
					$(this).attr('title', iconStatusChange.hasClass('icon-ok')?'<?php echo __('Išjungti') ?>':'<?php echo __('Įjungti') ?>');
				}
			});
		});
	});
</script>
