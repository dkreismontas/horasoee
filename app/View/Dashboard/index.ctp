<div>
    <div class="row-fluid">
        <div class="col-md-2 col-sm-0"></div>
        <div class="col-md-8 col-sm-12">
            <div class="row-fluid">
                <div class="col-sm-10 col-xs-6"  style="padding-left: 0;">
                    <div id="mainButtons">
                        <?php echo $this->Html->link('<i class="iconfa-bar-chart"></i> '.__('Darbo centrai'), array('controller'=>'tv', 'action'=>'plugins'), array('escape'=>false, 'target'=>'_blank','class'=>'btn btn-info','style'=>'min-width: 180px;')); ?>
                        <?php echo $this->Html->link('<i class="iconfa-bar-chart"></i> '.__('Skaidrių rinkiniai'), array('controller'=>'slides', 'action'=>'display'), array('escape'=>false, 'class'=>'btn btn-info', 'style'=>'min-width: 180px;')); ?>
                        <?php echo $this->Html->link('<i class="iconfa-bar-chart"></i> '.__('6h DC peržiūra'), array('controller'=>'work_center_6h', 'action'=>'display'), array('escape'=>false, 'class'=>'btn btn-info', 'style'=>'min-width: 180px;')); ?>
                        <?php echo $this->Html->link('<i class="iconfa-bar-chart"></i> '.__('24h grafikai'), array('controller'=>'work_center_24h', 'action'=>'index'), array('escape'=>false, 'target'=>'_blank', 'class'=>'btn btn-info', 'style'=>'min-width: 180px;')); ?>
                    </div>
                </div>
                <div class="col-sm-2 col-xs-6" style="padding-right: 0;">
                    <?php if(is_array($factoriesList) && sizeof($factoriesList) > 1): ?>
                        <div class="btn-group pull-right">
                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Fabrikai') ?>:</strong>&nbsp;<?php echo isset($this->params['named']['factory_id']) ? $factoriesList[$this->params['named']['factory_id']] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
                            <ul class="dropdown-menu">
                                <li><a href="<?php echo Router::url('/'); ?>"><?php echo __('---'); ?></a></li>
                                <?php foreach ($factoriesList as $lid => $li): ?>
                                    <li><a href="<?php echo Router::url(array('factory_id'=>$lid)); ?>"><?php echo $li; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <div class="col-md-2 col-sm-0"></div>
    </div>
	<br class="clearfix" />
	<br />
    <div class="row-fluid">
        <div class="col-md-2 col-sm-0"></div>
        <div class="col-md-8 col-sm-12">
            <?php
            $factoryExist = array_filter($workCenters, function($sensor){
                return $sensor['Factory']['id'] > 0;
            })
            ?>
            <table id="activeWorkCentersListTable" class="table table-bordered table-condensed table-striped">
                <thead>
                <tr>
                    <th><?php echo __('Linija'); ?></th>
                    <th><?php echo __('Jutiklis'); ?></th>
                    <th><?php echo __('Padalinys'); ?></th>
                    <?php if(!empty($factoryExist)){ ?><th><?php echo __('Fabrikas'); ?></th><?php } ?>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach ($workCenters as $key => $workCenter):  ?>
                    <tr sensor_id="<?php echo $workCenter['Sensor']['id']; ?>">
                        <td><?php echo Line::buildName($workCenter, false); ?></td>
                        <td><a style="white-space: nowrap;" class="" href="<?php echo htmlspecialchars(sprintf($viewUrl, $key)) ?>" target="_blank"><?php echo Sensor::buildName($workCenter, false); ?></a></td>
                        <td><?php echo Branch::buildName($workCenter, false); ?></td>
                        <?php if(!empty($factoryExist)){ ?><td><?php echo Factory::buildName($workCenter, false); ?></td><?php } ?>
                        <td class="full_width">
                            <a style="white-space: nowrap;" class="btn btn-default" href="<?php echo htmlspecialchars(sprintf($viewUrl, $key)) ?>" target="_blank"><span class="iconfa-signin"></span>&nbsp;<?php echo __('Peržiūrėti'); ?></a>
                            <a style="white-space: nowrap;" class="btn btn-default" href="<?php echo htmlspecialchars(sprintf($view24HUrl, $workCenter['Sensor']['id'])) ?>" target="_blank"><span class="iconfa-signin"></span>&nbsp;<?php echo __('24h grafikas'); ?></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <div class="col-md-2 col-sm-0"></div>
    </div>
</div>
