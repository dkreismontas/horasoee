<?php echo $this->Form->create('Filter'); ?>
<div class="row-fluid">
	<div class="col-sm-8">
		<h3 class="widgettitle"><?php echo __('Vartotojų grupės'); ?></h3>
		<div class="widgetcontent">
		  <div class="row-fluid">
    		<?php
    			$options = array(0=>__(__('---')), 1=>__('Nepažymėti'),2=>__('Pažymėti'));
    			foreach($roles as $key => $role){
    			    echo $key > 0 && $key % 5 == 0?'</div><br style="clear: both;" /><div class="row-fluid">':'';
    				echo $this->Form->input($role['Group']['id'],array('type'=>'select', 'options'=>$options,'label'=>$role['Group']['name'].':&nbsp;','div'=>'col-sm-2','class'=>'form-control')).'&nbsp;';
    			}
    			//echo $this->Form->input('controller',array('label'=>__('Kontroleris'),'type'=>'select','div'=>'inline','class'=>'input-medium','options'=>$availableControllers,'empty'=>__('---')));
    			//echo $this->Form->input('action',array('label'=>__('Metodas'),'type'=>'select','div'=>'inline','class'=>'input-medium','options'=>$availableActions,'empty'=>__('---')));
    		 ?>
    		 </div>
    		 <br style="clear: both;" /><br /><div class="row-fluid"><div class="col-sm-2"><?php echo $this->Form->submit(__('Filtruoti'),array('class'=>'btn btn-primary')); ?></div></div>
		 </div>
	</div>
	<div class="col-sm-4">
		<h3 class="widgettitle"><?php echo __('Prieeiga'); ?></h3>
		<div class="widgetcontent row-fluid">
		<?php 
			echo $this->Form->input('plugin',array('label'=>__('Įskiepis'),'type'=>'select','div'=>'col-sm-4','class'=>'form-control','options'=>array('MainApp'=>'---')+$pluginsList));
			echo $this->Form->input('controller',array('label'=>__('Kontroleris'),'type'=>'select','div'=>'col-sm-4','class'=>'form-control','options'=>$availableControllers,'empty'=>__('---')));
			echo $this->Form->input('action',array('label'=>__('Metodas'),'type'=>'select','div'=>'col-sm-4','class'=>'form-control','options'=>$availableActions,'empty'=>__('---')));
		 ?>
		 </div>
	</div>
</div>
<?php echo $this->Form->end(); ?>
<?php if(isset($data)){ ?>
<table class="allFunctions table table-bordered" cellpadding="0" cellspacing="0">
	<thead>
	<tr>
		<th>ID</th>
		<th><?php echo __('Įskiepis'); ?></th>
		<th><?php echo __('Kontroleris'); ?></th>
		<th><?php echo __('Metodas'); ?></th>
		<th><?php echo __('Aprašymas'); ?></th>
		<th><?php echo __('Vartotojai'); ?> <br />
		    <?php foreach($roles as $role){ ?>
                <input  type="checkbox" class="checkAll" group_id="<?php echo $role['Group']['id']; ?>" />
                <span class="groupName_<?php echo $role['Group']['id']; ?>"><?php echo $role['Group']['name']; ?></span>
            <?php } ?>

		</th>
	</tr>
	</thead>
<?php
	foreach($data as $val){
	    if(strtolower($val['Permission']['controllers']) == 'users' && strtolower($val['Permission']['actions']) == 'login') continue;
?>
	<tr permission_id="<?php echo $val['Permission']['id']; ?>">
		<td><?php echo $val['Permission']['id']; ?></td>
		<td><?php echo $val['Permission']['plugin']; ?></td>
		<td><?php echo $val['Permission']['controllers']; ?></td>
		<td><?php echo $val['Permission']['actions']; ?></td>
		<td><?php echo $val['Permission']['description']; ?></td>
		<td>
			<?php foreach($roles as $role){
				$mas = unserialize($val['Permission']['users']); //fb($mas,$role['Group']['id']);
			?>
			<input class="donttouch" group_id="<?php echo $role['Group']['id']; ?>" type="checkbox" <?php echo in_array($role['Group']['id'],$mas)?'checked="checked"':'' ?> onclick="<?php echo $this->Ajax->remoteFunction(
		        array(
		            'url' => array( 'controller' => 'users', 'action' => 'give_permission',$val['Permission']['id'],$role['Group']['id'])
		        )
		    )?>" /> <?php echo $role['Group']['name']; } ?>
		</td>
	</tr>
<?php
	}
?>
</table>
<?php } ?>
<script type="text/javascript">
    var allFunctions = new Array();
    jQuery(document).ready(function($){
        //patikra ar pazymetos visos funkcijos atitinkamai grupei
        $('table thead input[type|="checkbox"]').each(function(){
            var group_id = $(this).attr('group_id');
            var emptyInputFound = false;
            $('table.allFunctions tbody tr').each(function(){
                if(!$(this).find('input[group_id|="'+group_id+'"]').is(':checked')){
                    emptyInputFound = true;
                    return false;
                }
            });
            if(!emptyInputFound){
                $(this).attr('checked','checked');
                $(this).parents('span').addClass('checked');
            }
        });

        //Vykdomas teisiu priskyrimas visu funkciju vienai grupei
        $('input.checkAll').bind('click',function(){
            var group_id = $(this).attr('group_id');
            var type = 0;
            var text = '<?php echo __('grupės vartotojams bus nuimamos teisės nuo visų funkcijų'); ?>';
            var text2 = '<?php echo __('Ar tikrai norite panaikinti prieigą prie visų funkcijų grupės'); ?>';
            if($(this).is(':checked')){
                type = 1;
                var text = '<?php echo __('grupės vartotojams priskiriamos teisės prie visų funkcijų'); ?>';
                var text2 = '<?php echo __('Ar tikrai norite suteikti prieigą prie visų funkcijų grupės'); ?>';
            }
            var currentGroupName = $('.groupName_'+$(this).attr('group_id')).html();
            if(!confirm(text2+' '+currentGroupName+' '+'<?php echo __('vartotojams?'); ?>')){
                //$(this).prop('checked', false);
                $(this).parent().addClass('checked')
                return false;
            }
            $('table.allFunctions tbody tr').each(function(){
                if(type == 1){
                    if(!$(this).find('input[group_id|="'+group_id+'"]').is(':checked')){
                        allFunctions.push($(this).attr('permission_id'));
                    }
                    $(this).find('input[group_id="'+group_id+'"]').prop('checked',true);
                }else{
                    if($(this).find('input[group_id|="'+group_id+'"]').is(':checked')){
                        allFunctions.push($(this).attr('permission_id'));
                    }
                    $(this).find('input[group_id="'+group_id+'"]').prop('checked',false);
                }
            });
   console.log(allFunctions)         
   console.log(type)         
            startChanges(0, $(this).attr('group_id'));
                    $('body').append('<div id="startPlaning"><div class="planWrapper"><div><h3 id="currentFunction"></h3><h3><?php echo __(''); ?> '+currentGroupName+' '+text+'</h3><img src="'+root+'images/loaders/loader18.gif" /></div></div><div class="planScreen"></div></div>');
                    var thisOne = $(this);
        });
        
        $('#FilterPlugin').live('change',function(){
            $('#FilterController').find('option[value=""]').prop('selected',true);
        	$('#FilterAction').find('option[value=""]').prop('selected',true);
        	$('#FilterController optgroup').not($('#FilterController option:selected').parent()).hide();
        	$('#FilterController optgroup[label="'+$(this).val()+' "]').show();
        });
        $('#FilterController').live('change',function(){
            var optGroupLabel = $(this.options[this.selectedIndex]).closest('optgroup').prop('label');
        	$('#FilterAction').find('option[value=""]').prop('selected',true);
        	$('#FilterAction optgroup').hide();
        	if(optGroupLabel){
        	   $('#FilterAction optgroup[label="'+optGroupLabel.replace(/MainApp|\s/g,'')+'-'+$(this).val()+'"]').show();
        	}
        });
        $('#FilterPlugin').trigger('change');
        $('#FilterController').trigger('change');
    });

     function startChanges(i,group_id){
         if(typeof allFunctions[i] != 'undefined') {
             var currentRow = jQuery('table tr[permission_id|="'+allFunctions[i]+'"]');
             jQuery('h3#currentFunction').html(currentRow.children('td').eq(1).html()+' / '+currentRow.children('td').eq(2).html())
             jQuery.ajax({
                url: '<?php echo $this->Html->url('/'.$this->params['controller'].'/give_permission/',true); ?>'+allFunctions[i]+'/'+group_id,
                complete: function(){
                    startChanges(++i,group_id);
                }
            });
         }else{
             jQuery('#startPlaning .planWrapper > div').html('<h3><?php echo __('Priskyrimas baigtas'); ?></h3>');
             var timeOut = setInterval(function(){
                 jQuery('#startPlaning').remove(); 
                 clearInterval(timeOut); 
                 //location.href="<?php echo $this->Html->url('/'.$this->params['controller'].'/check_permissions/',true); ?>"
             },2000);
         }
     }
</script>
