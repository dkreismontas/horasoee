<div class="commercial">
	<p><?php __('Į šį el. paštą bus nusiųsta nuoroda, kurią sekdami galėsite pasikeisti savo vartotojo vardą bei slaptažodį. Įsitikinkite, kad siunčiate teisingu paštu.')?></p>
	<?php
		echo $form->create('User', array('action' => 'remind'));
	    echo $form->input('email',array('label'=>__('El.Pašto Adresas:',true)));
		echo $form->end(array('class'=>'submit_button','label'=>__('Patvirtinti',true)));
	?>
</div>