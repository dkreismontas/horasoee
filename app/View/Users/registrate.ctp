<h1>Registracija</h1>
<div id="center">
<div class="brandProducts one" id="cartList">
	<div class="topRound"></div>
	<div class="bottomRound"></div>
	<div class="paymentTypes"><div id="messageHere">
		<?php
			echo $this->Form->create('User');
			echo $this->Form->input('username',array('label'=>'<span>*</span> '.__('Prisijungimo vardas:',true),'class'=>'validate[required,ajax[ajaxUser]]'));
			echo $this->Form->input('fname',array('label'=>'<span>*</span> '.__('Vardas:',true),'class'=>'validate[required],custom[onlyLetter]'));
			echo $this->Form->input('lname',array('label'=>'<span>*</span> '.__('Pavardė:',true),'class'=>'validate[required],custom[onlyLetter]'));
			echo $this->Form->input('email',array('label'=>'<span>*</span> '.__('El. paštas:',true),'class'=>'validate[required],custom[email]'));
			echo $this->Form->input('password',array('value'=>'','label'=>'<span>*</span> '.__('Slaptažodis:',true),'type'=>'password','class'=>'validate[required,length[6,20]]'));
			echo $this->Form->input('password_repeat',array('type'=>'password','value'=>'','label'=>'<span>*</span> '.__('Pakartoti slaptažodį:',true),'class'=>'validate[required,length[6,20],funcCall[validate2fields]]'));
			echo $this->Form->input('phone',array('label'=>__('Telefono numeris:',true),'class'=>'validate[custom[telephone]]'));
			//echo $this->Form->input('address',array('type'=>'textarea','rows'=>3,'label'=>'<span>*</span> '.__('Adresas:',true)));
			//echo $this->Form->input('hide',array('label'=>__('Noriu gauti naujienlaiškius.',true),'checked'=>'checked','type'=>'checkbox'));
			echo '<div id="code">
				'.$this->Html->image('indicator.gif',array('style'=>'display: none;','id'=>'loading')).'
				
				<div id="codeFrame"><iframe width="180" height="60" frameborder="0" src="'.$this->Html->url('/',true).'securimage/capcha.php" class="sidebarFrame"></iframe></div>'.
				$this->Ajax->link(
					'',
					array( 'controller' => 'users', 'action' => 'change_capcha'),
					array( 'update' => 'codeFrame', 'indicator' => 'loading', 'class'=>'changeCapcha', 'title'=>__('Pakeisti apsaugos kodą',true))
				).
			'</div><br class="clear" />';
			echo $this->Form->input('confirm',array('label'=>'<span>*</span> '.__('Įvesti patvirtinimo kodą:',true),'class'=>'validate[required,length[4,4]]'));
			echo $this->Form->end(array('class'=>'submit_button','label'=>__('Registruoti',true),'name'=>'send'));
		?>
	</div>
		
	</div>
</div>
<br class="clear">
</div>

<?php
	echo $this->Html->css('validationEngine.jquery');
	echo $this->Html->script('jquery.validationEngine-en');
	echo $this->Html->script('jquery.validationEngine');
	echo $this->Html->script(
		'<script type="text/javascript">
			$(document).ready(function(){
				$("#UserRegistrateForm").validationEngine();
			});
function validate2fields(){
	if($("#UserPassword").val() != $("#UserPasswordRepeat").val()){
		return true;
	}else{
		return false;
	}
}

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Šitas laukelis yra privalomas",true).'",
						"alertTextCheckboxe":"* '.__("Šitas parinkties laukelis būtinas",true).'"},
					"length":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"*'.__("Tarp",true).' ",
						"alertText2":" '.__("ir",true).' ",
						"alertText3": " '.__("leidžiami simboliai",true).'"},
					"maxCheckbox":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Parinkties laukelių skaičius viršytas",true).'"},	
					"minCheckbox":{
						"regex":"'.__("Šitas laukelis yra privalomas",true).'",
						"alertText":"* '.__("Prašom pasirinkti",true).' ",
						"alertText2":" '.__("nustatymai",true).'"},	
					"confirm":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Jūsų laukelis nesutampa",true).'"},		
					"telephone":{
						"regex":"/^[\+0-9\-\(\)\ ]+$/",
						"alertText":"* '.__("Neteisingas telefono numeris",true).'"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":"* '.__("Neteisingas el. paštas",true).'"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* '.__("Neteisingas datos formatas: turi būti metai-mėnuo-diena (yyyy-mm-dd)",true).'"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* '.__("Tik skaičiai",true).'"},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* '.__("Neleidžiamas nė vienas specialus simbolis",true).'"},	
					"ajaxUser":{
						"file":"'.$this->Html->url('/',true).'users/check_name'.'",
						"alertTextOk":"* '.__("Šitas vartotojo vardas leidžiamas",true).'",	
						"alertTextLoad":"* '.__("Kraunama, prašome palaukti...",true).'",
						"alertText":"* '.__("Šitas vartotojo vardas jau užimtas",true).'"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* '.__("Šitas vartotojo vardas jau užimtas",true).'",
						"alertTextOk":"* '.__("Šitas vardas leidžiamas",true).'",	
						"alertTextLoad":"* '.__("Kraunama, prašome palaukti...",true).'"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* '.__("Tik raidės",true).'"},
					"validate2fields":{
    					"nname":"validate2fields",
    					"alertText":"* '.__("Nesutampa įvesti slaptažodžiai",true).'"}	
					}	
					
					}
				}
			})(jQuery);</script>
		'
	);
?>


