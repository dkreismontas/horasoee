<?php
echo $this -> Form -> create('User', array('url'=>array('action' => 'login'))); ?>
<div class="inputwrapper animate1 bounceIn">
	<?php echo $this -> Form -> input('username', array('label' => false, 'div' => false, 'placeholder' => __('Vartotojas'))); ?>
</div>
<div class="inputwrapper animate2 bounceIn">
     <?php echo $this -> Form -> input('password', array('label' => false, 'div' => false, 'placeholder' => __('Slaptažodis'))); ?>
</div>
<div class="inputwrapper animate3 bounceIn">
    <button name="submit"><?php echo __('Prisijungti'); ?></button>
</div>
<?php  
	if(isset($this->request->params['pass'][0])){
		 echo $this -> Form -> input('sensor_id', array('type'=>'hidden', 'value'=>$this->request->params['pass'][0]));
	}
    echo $this -> Form -> hidden('controller', array('value' => 'admin_page'));
	echo $this -> Form -> hidden('action', array('value' => 'show'));
	echo $this -> Form -> end();
?>
<div style="position:relative;">
    <div style="position: absolute; color: #fff;"><?php echo $this->Session->flash(); ?></div>
</div>