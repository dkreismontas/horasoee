<h1>Registracija</h1>
<div id="center">
<div class="brandProducts one" id="cartList">
	<div class="topRound"></div>
	<div class="bottomRound"></div>
	<div class="paymentTypes"><div id="messageHere">
		<?php
			echo $form->create('User',array('id'=>'UserRegistrateForm'));
			echo $form->input('username',array('label'=>'<span>*</span> '.__('Prisijungimo vardas:',true),'class'=>'validate[required,ajax[ajaxUser]]'));
			echo $form->input('fname',array('label'=>'<span>*</span> '.__('Vardas:',true),'class'=>'validate[required],custom[onlyLetter]'));
			echo $form->input('lname',array('label'=>'<span>*</span> '.__('Pavardė:',true),'class'=>'validate[required],custom[onlyLetter]'));
			echo $form->input('email',array('label'=>'<span>*</span> '.__('El. paštas:',true),'class'=>'validate[required],custom[email]'));
			echo $form->input('password',array('value'=>'','label'=>'<span>*</span> '.__('Slaptažodis:',true),'type'=>'password',));
			echo $form->input('password_repeat',array('type'=>'password','value'=>'','label'=>'<span>*</span> '.__('Pakartoti slaptažodį:',true),'class'=>'validate[funcCall[validate2fields]]'));
			echo $form->input('phone',array('label'=>__('Telefono numeris:',true),'class'=>'validate[custom[telephone]]'));
			//echo $form->input('address',array('type'=>'textarea','rows'=>3,'label'=>'<span>*</span> '.__('Adresas:',true)));
			//echo $form->input('hide',array('label'=>__('Noriu gauti naujienlaiškius.',true),'checked'=>'checked','type'=>'checkbox'));
			echo $form->end(array('class'=>'submit_button','label'=>__('Išsaugoti',true),'name'=>'send'));
		?>
	</div>
		
	</div>
</div>
<br class="clear">
</div>

<?php
	echo $this->addScript($html->css('validationEngine.jquery'));
	echo $this->addScript($javascript->link('jquery.validationEngine-en'));
	echo $this->addScript($javascript->link('jquery.validationEngine'));
	echo $this->addScript(
		'<script type="text/javascript">
			$(document).ready(function(){
				$("#UserRegistrateForm").validationEngine();
			});
function validate2fields(){
	if($("#UserPassword").val() != $("#UserPasswordRepeat").val()){
		return true;
	}else{
		return false;
	}
}

(function($) {
	$.fn.validationEngineLanguage = function() {};
	$.validationEngineLanguage = {
		newLang: function() {
			$.validationEngineLanguage.allRules = 	{"required":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Šitas laukelis yra privalomas",true).'",
						"alertTextCheckboxe":"* '.__("Šitas parinkties laukelis būtinas",true).'"},
					"length":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"*'.__("Tarp",true).' ",
						"alertText2":" '.__("ir",true).' ",
						"alertText3": " '.__("leidžiami simboliai",true).'"},
					"maxCheckbox":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Parinkties laukelių skaičius viršytas",true).'"},	
					"minCheckbox":{
						"regex":"'.__("Šitas laukelis yra privalomas",true).'",
						"alertText":"* '.__("Prašom pasirinkti",true).' ",
						"alertText2":" '.__("nustatymai",true).'"},	
					"confirm":{
						"regex":"'.__("Nieko",true).'",
						"alertText":"* '.__("Jūsų laukelis nesutampa",true).'"},		
					"telephone":{
						"regex":"/^[\+0-9\-\(\)\ ]*$/",
						"alertText":"* '.__("Neteisingas telefono numeris",true).'"},	
					"email":{
						"regex":"/^[a-zA-Z0-9_\.\-]+\@([a-zA-Z0-9\-]+\.)+[a-zA-Z0-9]{2,4}$/",
						"alertText":"* '.__("Neteisingas el. paštas",true).'"},	
					"date":{
                         "regex":"/^[0-9]{4}\-\[0-9]{1,2}\-\[0-9]{1,2}$/",
                         "alertText":"* '.__("Neteisingas datos formatas: turi būti metai-mėnuo-diena (yyyy-mm-dd)",true).'"},
					"onlyNumber":{
						"regex":"/^[0-9\ ]+$/",
						"alertText":"* '.__("Tik skaičiai",true).'"},	
					"noSpecialCaracters":{
						"regex":"/^[0-9a-zA-Z]+$/",
						"alertText":"* '.__("Neleidžiamas nė vienas specialus simbolis",true).'"},	
					"ajaxUser":{
						"file":"'.$this->Html->url('/',true).'users/check_name'.'",
						"alertTextOk":"* '.__("Šitas vartotojo vardas leidžiamas",true).'",	
						"alertTextLoad":"* '.__("Kraunama, prašome palaukti...",true).'",
						"alertText":"* '.__("Šitas vartotojo vardas jau užimtas",true).'"},	
					"ajaxName":{
						"file":"validateUser.php",
						"alertText":"* '.__("Šitas vartotojo vardas jau užimtas",true).'",
						"alertTextOk":"* '.__("Šitas vardas leidžiamas",true).'",	
						"alertTextLoad":"* '.__("Kraunama, prašome palaukti...",true).'"},		
					"onlyLetter":{
						"regex":"/^[a-zA-Z\ \']+$/",
						"alertText":"* '.__("Tik raidės",true).'"},
					"validate2fields":{
    					"nname":"validate2fields",
    					"alertText":"* '.__("Nesutampa įvesti slaptažodžiai",true).'"}	
					}	
					
					}
				}
			})(jQuery);</script>
		'
	);
?>


