<i><?php echo __('Norėdami pakeisti vartotojo grupę, spustelkite pele ant atitinkamo vartotojo grupės pavadinimo'); ?></i>
<div class="row-fluid">
<div class="col-sm-3">
      <h4 class="widgettitle"><?php echo __('Grupės'); ?>
          <i class="iconfa-plus-sign h4_icons" id="addGroup" title="<?php echo __('Sukurti naują grupę'); ?>" style="float:right;"></i>
      </h4>
      <div class="widgetcontent">
            <?php 
                $groupsTitles = $groups;
                array_walk($groupsTitles, function(&$title,$id){
                    if($id==1) return; 
                    $title .= '&nbsp;<i class="glyphicon glyphicon-edit editGroup" data-group_id="'.$id.'" title="'.__('Pakeisti grupės pavadinimą').'"></i>';
                    if(!in_array($id, array(User::ID_SUPER_ADMIN, User::ID_MODERATOR, User::ID_GUEST, User::ID_OPERATOR))) {
                        $title .= '&nbsp;<i class="glyphicon glyphicon-remove removeGroup" data-group_id="' . $id . '" title="' . __('Pašalinti grupę') . '"></i>';
                    }
                });
                echo $this->Form->create('User',array('url'=>'giv_permissions'));
                echo $this->Form->input('groups', array('multiple' => 'checkbox', 'options' => $groupsTitles,'label'=>false, 'div'=>'floated_checkboxes', 'escape'=>false, 'selected'=>isset($_SESSION['user_filter'])?$_SESSION['user_filter']:array()));
                //echo $this->Form->input('',array_merge(array('name' => 'data[ResourceSpliting][tech_id][]','value' => $techProcess['id'], 'type'=>'checkbox', 'after' => $techProcess['process_name'], 'label'=>false, 'div'=>false))).'<br />';
                //echo $this->Form->input('in4',array('after' => __('Adminai').'<br />', 'label'=>false, 'div'=>false, 'type'=>'checkbox','value'=>4,'checked'=>isset($_SESSION['user_filter']) && strpos($_SESSION['user_filter'], '4')?'checked':''));
                //echo $this->Form->input('in3',array('after' => __('Meistrai').'<br />', 'label'=>false, 'div'=>false, 'type'=>'checkbox', 'value'=>3,'checked'=>isset($_SESSION['user_filter']) && strpos($_SESSION['user_filter'], '3')?'checked':''));
                //echo $this->Form->input('in2',array('after' => __('SuperAdminai').'<br />', 'label'=>false, 'div'=>false, 'type'=>'checkbox','value'=>1,'checked'=>isset($_SESSION['user_filter']) && strpos($_SESSION['user_filter'], '1')?'checked':''));
                //echo $this->Form->input('in1',array('after' => __('Moderatoriai').'<br />', 'label'=>false, 'div'=>false, 'type'=>'checkbox','value'=>2,'checked'=>isset($_SESSION['user_filter']) && strpos($_SESSION['user_filter'], '2')?'checked':''));
                echo $this->Form->hidden('filter',array('value'=>'1'));
                echo '<br class="clear" />'.$this->Form->end(array('class'=>'btn btn-primary','label'=>__('Filtruoti')));
            ?>
    </div>
</div>
<div class="col-sm-9">
      <!--THIS IS A PORTLET-->
		<div class="portlet">
			<div class="portlet-content">
				<div id="giv_permissions">
<?php
if($this->Paginator->numbers()){
    ?>
    <div class="pagination">
        <?php echo $this->Paginator->numbers(array('class'=>'btn btn-default', 'separator'=>'&nbsp;','currentTag'=>'a')); ?>
        <!-- Shows the next and previous links -->
        <?php
            echo $this->Paginator->prev(__('« Ankstesnis'),array('class' => 'btn btn-default prev', 'disabledTag'=>'a')).'&nbsp;';
            echo $this->Paginator->next(__(' Kitas »'), array('class' => 'btn btn-default forward', 'disabledTag'=>'a'));
        ?>
        <!-- prints X of Y, where X is current page and Y is number of pages -->
        <?php echo $this->Paginator->counter('{:page} / {:pages}');
        ?>
    </div>
<?php } ?>
<table class="table table-bordered">
    <thead>
    <tr>
        <th><?php echo $this->Paginator->sort('Title', __('Vartotojo vardas')); ?></th>
        <th><?php echo __('El. paštas'); ?></th>
        <th><?php echo __('Vardas'); ?></th>
        <th><?php echo __('Pavardė'); ?></th>
        <th><?php echo __('Skyrius'); ?></th>
        <th><?php echo __('Darbo centras'); ?></th>
        <th><?php echo __('Veiksmai'); ?></th>
        <th><?php echo __('Grupė'); ?></th>
        <th><?php echo __('Įjungtas'); ?></th>
    </tr>
    </thead>
       <?php foreach($data as $recipe): ?>
    <tr>
        <td><?php echo $this->Html->link($recipe['User']['username'],array('action'=>'edit',$recipe['User']['id'])); ?></td>
        <td><?php echo $recipe['User']['email']; ?> </td>
        <td><?php echo $recipe['User']['first_name']; ?> </td>
        <td><?php echo $recipe['User']['last_name']; ?> </td>
        <td><?php echo $recipe['User']['section']; ?> </td>
        <td><?php echo implode(', ',array_intersect_key($sensorsList, array_flip(explode(',',$recipe['User']['sensor_id'])))); ?> </td>
        <td>
            <?php echo $this->Html->link('<i class="glyphicon glyphicon-edit"></i>&nbsp;'.__('Redaguoti'),array('action'=>'edit',$recipe['User']['id']), array('escape'=>false,'style'=>'white-space:nowrap;')); ?><br />
            <?php if($recipe['User']['group_id'] != 1 || ($recipe['User']['group_id'] == 1 && !$last_superadmin && $recipe['User']['id'] > 1))  echo $this->Html->link('<i class="glyphicon glyphicon-trash"></i>&nbsp;'.__('Pašalinti'),array('action'=>'delete',$recipe['User']['id']),array('title'=>__('Ištrinti'),'class'=>'delete','escape'=>false,'style'=>'white-space:nowrap;'),__('Ar tikrai norite panaikinti šį vartotoją?')); ?>
        </td>
		<td width="200">
			<p class="edit" id="UserGroup<?php echo $recipe['User']['id']; ?>"><?php echo trim($recipe['Group']['name'])?$recipe['Group']['name']:'---'; ?></p>
			<?php
				echo $this->Ajax->editor(
				    "UserGroup".$recipe['User']['id'],
				    array(
				        'controller' => 'users',
				        'action' => 'change_user_group',
						$recipe['User']['id'],'group_id','Group','name'
				    ),
				    array(
				        'indicator' => '<img src="'.$this->Html->url('/',true).'css/ajax-loader.gif">',
				        'submit' => 'OK',
				        'style' => 'inherit',
				        'submitdata' => array(),
				        'tooltip'   => __('Spustelkite pele norėdami redaguoti...'),
						'data' => $groups,
						'type' => 'select',
						'update' => ''
				    )
				);
		?>
		</td>
		<td  align="center"><?php echo $this->Form->input('active',array('type'=>'checkbox','class'=>'active_controller'.$recipe['User']['id'],'label'=>false,'checked'=>$recipe['User']['active']?'checked':'',
			'onclick'=> $this->Ajax->remoteFunction(array(
		            'url' => array( 'controller' => 'users', 'action' => 'activate_user',$recipe['User']['id']),
		     		 'success'=>'
		     		 	try{
    		     		 	var obj = jQuery.parseJSON(request);
    		     		 	if(obj.string){
    		     		 	    jQuery(".maincontentinner").prepend(obj.string);
                            }else{
                                jQuery(".errorsList").remove();
                            }
    						var interval = setInterval(function(){ if(jQuery(".errorsList").length <= 0){window.clearInterval(interval); } jQuery(".errorsList").fadeOut("slow")},5000);
    		     		 	if(obj.state){
    		     		 		if(obj.state==1){
    		     		 			jQuery(".active_controller"+obj.id).attr("checked","checked");
    		     		 		}else{
    		     		 			jQuery(".active_controller"+obj.id).removeAttr("checked");
    		     		 		}
    						}
                        }catch(e){}
    				    '
			 )))); ?></td>
    </tr>
    <?php endforeach; ?>
</table>
<div style="display: none;"><div id="popupContainer"></div></div>
<?php
if($this->Paginator->numbers()){
    ?>
    <div class="pagination">
        <?php echo $this->Paginator->numbers(array('class'=>'btn btn-default', 'separator'=>'&nbsp;','currentTag'=>'a')); ?>
        <!-- Shows the next and previous links -->
        <?php
        echo $this->Paginator->prev(__('« Ankstesnis'),array('class' => 'btn btn-default prev', 'disabledTag'=>'a')).'&nbsp;';
        echo $this->Paginator->next(__(' Kitas »'), array('class' => 'btn btn-default forward', 'disabledTag'=>'a'));
        ?>
        <!-- prints X of Y, where X is current page and Y is number of pages -->
        <?php echo $this->Paginator->counter('{:page} / {:pages}');
        ?>
    </div>
<?php } ?>
</div>
	        </div>
       </div>
      <!--THIS IS A PORTLET-->
</div>

</div>
<br class="clear" />
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.maincontent').on('click','#addGroup',function(e, groupId){
            $.ajax({
                url: '<?php echo Router::url(array($this->params['controller'],'action'=>'add_change_group'),true); ?>',
                type: 'post',
                data: {group_id: groupId},
                context: $(this),
                success:function(data){
                    $('#popupContainer').html(data);
                    popupWindow('#popupContainer', 'auto', '1200','auto', "<?php echo __('Grupės duomenys'); ?> ", "<?php echo __('Uždaryti langą') ?>");
                }
            });
        });
        $('.maincontent').on('click','.removeGroup',function(e, groupId){
            var conf = confirm('<?php echo __('Ar tikrai norite pašalinti šią grupę kartu su visais jai priklausančiais vartotojais?'); ?>');
            if(!conf) return false;
            $.ajax({
                url: '<?php echo Router::url(array($this->params['controller'],'action'=>'remove_group'),true); ?>',
                type: 'post',
                data: {group_id: $(this).data('group_id')},
                success:function(data){
                    location.reload();
                }
            });
        });
        $('.maincontent').on('click','.editGroup',function(){
            $('#addGroup').trigger('click',[$(this).data('group_id')]);
        });
    });
</script>
