<div class="pageheader">
    <div class="pageicon">
        <span class="iconfa-table"></span>
    </div>
    <div class="pagetitle">
        <h1><?php echo __('Naujo vartotojo sukūrimas') ?></h1>
    </div>
</div>
<div class="row-fluid">
    <?php
    	$this->Session->flash();
		$langFolder = ROOT.'/app/Locale/';
		$files1 = scandir($langFolder);
		$possibleLanguages = array_filter($files1, function($value){ return !preg_match('/^\..*/',$value); });
		$possibleLanguages = array_combine($possibleLanguages, $possibleLanguages);
        $possibleLanguagesFixed = array();
        foreach($possibleLanguages as $key=>$lang) {
            $newName = $key;
            switch($lang) {
                case "eng":
                    $newName = "EN";
                    break;
                case "lit":
                    $newName = "LT";
                    break;
            }
            $possibleLanguagesFixed[$key] = $newName;
        }
        $possibleLanguages = $possibleLanguagesFixed;
    ?>
    <div id="UserChangePasswordForm">
    <?php
    	$branches[0] = ''; asort($branches);
    	echo $this->Form->create('User', array('action' => 'register'));
		echo '<div class="span4">';
        echo $this->Form->input('name',array('label'=>__('Vartotojo vardas:',true)));
        echo $this->Form->input('pass',array('label'=>__('Slaptažodis:',true),'type'=>'password'));
        echo $this->Form->input('password_repeat',array('type'=>'password','value'=>'','label'=>__('Pakartoti slaptažodį:',true)));
    	echo $this->Form->input('fname',array('label'=>__('Vardas:',true)));
    	echo $this->Form->input('lname',array('label'=>__('Pavardė:',true)));
    	echo $this->Form->input('group_id',array('options'=>$groups,'label'=>'Grupė:'));
        echo $this->Form->input('default_start_path',array('label'=>__('Pirminis puslapis po prisijungimo: (pvz. /scans/start_scan )',true),'type' => 'text'));
    	echo $this->Form->input('redirect_page',array('label'=>__('Nukreipiamasis puslapis kai neturi teisių: (pvz. /users/login )',true),'type' => 'text'));
    	echo $this->Form->input('language',array('label'=>__('Kalba'),'type' => 'select','options'=>$possibleLanguages, 'default'=>'lit'));
    	echo $this->Form->submit(__('Išsaugoti'),array('class'=>'btn btn-primary','label'=>'Išsaugoti'));
		echo '</div><div class="span4"><h4 class="widgettitle">'.__('Papildomi laukai užsakymų išvedimui').'</h4><div class="widgetcontent" style="margin-bottom: 0;">';
		echo $this->Form->input('additional', array('type'=>'select','options'=>$additionalFields, 'label'=>false,'empty'=>'---', 'id'=>'additional'));
		echo '<div id="additionalFields">'.$this->My->constructAdditionalField(array(), $view->Form, true).'</div></div>';
        echo '</div><div class="span4"><h4 class="widgettitle">'.__('Vartotojo priskyrimas prie skenavimo darbuotojo / atributo').'</h4><div class="widgetcontent" style="margin-bottom: 0;">';
        echo $this->Form->input('worker_id', array('type'=>'select','options'=>$workers, 'label'=>false,'empty'=>'---', 'label'=>__('Darbuotojas')));
        echo $this->Form->input('attribute_id', array('type'=>'select','options'=>$attributes, 'label'=>false,'empty'=>'---', 'label'=>__('Atributas')));
        echo '</div></div>';
        
        echo '<div class="span4"><h4 class="widgettitle">'.__('Vartotojas negali redaguoti užsakymų su požymiais:').'</h4><div class="widgetcontent" style="margin-bottom: 0;">';
        echo $this->Form->input('', array('name'=>'data[User][edit_restriction][order][disabled]','type'=>'radio','options'=>array(0=>__('Įjungtas'),1=>__('Išjungtas')), 'checked'=>'', 'legend'=>__('Užsakymo statusas'),'div'=>'simpleRadio'));
        echo '</div></div>';
		
		echo $this->Form->end();
    ?>
    </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		$('select#additional').bind('change',function(){
			if($(this).val() == ''){ return false;}
			$.ajax({
				url: "<?php echo Router::url('/users/get_additional_field_values',true); ?>",
				type: 'POST',
				data: {slug: $(this).val()},
				success: function(data){
					currentId = data.match(/id="([^"]+)"/i);
					if($('#additionalFields #'+currentId[1]).length > 0){ $('#additionalFields #'+currentId[1]).parent().remove(); }
					$('#additionalFields').append(data);
				}
			});
		});
	});
</script>

