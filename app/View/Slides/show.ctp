<?php //pr($slidesCollection); ?>
<iframe src="" id="frame" width="100%"></iframe>
<script type="text/javascript">
    jQuery(document).ready(function($){
        setInterval(function(){location.reload();},7200000);
        $('#frame').css('height', $(window).height()+'px');
        var links = [<?php echo $slidesCollection['SlidesCollection']['links']; ?>];
        var linkIndex = 0;
        if(links.length > 0){
            var slideShow = function(){
                if(!links[linkIndex]){ linkIndex = 0; }
                $('#frame').attr('src', links[linkIndex++]);
            }
            setInterval(slideShow,<?php echo $slidesCollection['SlidesCollection']['transition_interval']*1000; ?>);
            slideShow();
        }
    });
</script>