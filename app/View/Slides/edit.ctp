<div class="row-fluid">
    <div class="col-md-6">
        <h3><?php echo __('Skaidrių rinkinio nustatymai'); ?></h3>
        <?php
        echo $this->Form->create('SlidesCollection');
        echo $this->Form->input('title', array('class' => 'form-control', 'type'=>'text','label'=>__('Pavadinimas')));
        echo $this->Form->input('transition_interval', array('class' => 'form-control', 'type'=>'number', 'min'=>10, 'label'=>__('Skaidrių pasikeitimo intervalas sekundėmis (min 10s)')));
        echo $this->Form->submit(__('Išsaugoti'),array('class'=>'btn btn-info'));
        echo $this->Form->end();
        ?>
    </div>
    <div class="col-md-6">
        <h3><?php echo __('Skaidrių nustatymai'); ?><br /><small><?php echo __('Norėdami pakeisti skaidrių pozicijas, spustelkite pele ant skaidrės fono baltos vietos ekrane ir tempkite į norimą poziciją'); ?></small></h3>
        <br />
        <div id="slides">
            <ul style="list-style-type: none;" v-sortable>
                 <slide_settings v-dragable-for="(key,slide) in slidesList" :slide="slide" :key="key"></slide_settings>
            </ul>
            <div class="clearfix">
                <button id="addSlide" class="btn btn-info" v-on:click="addSlide()"><?php echo __('Pridėti skaidrę'); ?></button>
            </div>
        </div>
    </div>
</div>
<template id="slideSettingsTpl">
    <li class="row-fluid" style="cursor: move;">
        <div class="col-md-5">
            <?php echo $this->Form->input('',array('id'=>false,'type'=>'select', 'label'=>__('Atvaizdavimo tipas'), 'v-model'=>'type', 'class' => 'form-control', 'value'=>-1, 'options'=>$viewTypes, 'v-on:change'=>'changeValue()')); ?>
        </div>
        <div class="col-md-5">
            <?php echo $this->Form->input('',array('id'=>false,'type'=>'select', 'label'=>$sensorsLabel, 'v-model'=>'sensors', 'class' => 'form-control', 'options'=>$sensors, 'value'=>-1, 'multiple'=>true, 'v-on:change'=>'changeValue()')); ?>
        </div>
        <div class="col-md-2">
            <br />
            <button class="btn btn-info" v-on:click="removeSlide()"><?php echo __('Pašalinti'); ?></button>
        </div>
        <br class="clearfix" />
        <hr />
    </li>
</template>

<script type="text/javascript" src="<?php echo $baseUri; ?>js/vue.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/vuedraggable/Sortable.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/vuedraggable/lodash.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/vuedraggable/vuedragablefor.js"></script>
<script type="text/javascript">


var vm = new Vue({
    el: '#slides',
    data: {
        slidesList: [],
        appInitiated: false
    },
    methods: {
        loadData: function(){
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/slides/get_slides/'.$slidesCollectionId,true); ?>",
                type: 'post',
                context: this,
                dataType: 'JSON',
                async: true,
                success: function(request){
                    this.slidesList = request.slidesList;
                }
            })
        },
        addSlide: function(){
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/slides/add_slide/'.$slidesCollectionId,true); ?>",
                type: 'post',
                context: this,
                async: true,
                dataType: 'JSON',
                success: function(request){
                    this.slidesList = request;
                }
            })
        }
    },
    watch:{
        slidesList: function(){
            if(!this.appInitiated){
                this.appInitiated = true;
                return false;   
            }
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/slides/change_value/'.$slidesCollectionId,true); ?>",
                type: 'post',
                async: true,
                data: {'changePos': this.slidesList},
                success: function(request){}
            });
        }
    },
    beforeCompile: function(){ 
        this.loadData();
    },
    components: {
        slide_settings: {
            template: '#slideSettingsTpl',
            props: ['slide','key'],
            data: function(){return {type:0, sensors: []}},
            beforeCompile: function(){
                this.type = this.slide.type;
                this.sensors = this.slide.sensors;
            },
            methods: {
                removeSlide: function(){
                    this.$remove();
                    jQuery.ajax({
                        url: "<?php echo $this->Html->url('/slides/remove_slide/'.$slidesCollectionId,true); ?>",
                        type: 'post',
                        async: true,
                        data: {slideKey: this.key},
                        success: function(request){}
                    });
                },changeValue: function(){
                    jQuery.ajax({
                        url: "<?php echo $this->Html->url('/slides/change_value/'.$slidesCollectionId,true); ?>",
                        type: 'post',
                        async: true,
                        data: {slideKey: this.key, saveData: {type: this.type, sensors: this.sensors}},
                        success: function(request){}
                    });
                }
            }
        }
    }
});
</script>