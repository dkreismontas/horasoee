<div class="row-fluid">
    <div class="col-md-6">
        <?php
        if($user->group_id == 1){
            echo $this->Html->link(__('Sukurti naują rinkinį'), array('controller'=>'slides', 'action'=>'edit'), array('class'=>'btn btn-info'));
        }
        ?>
    </div>
</div>
<div class="row-fluid clearfix">
    <div class="col-md-8">
        <table class="table table-bordered"><thead>
            <tr>
                <th><?php echo __('Pavadinimas'); ?></th>
                <th><?php echo __('Skaidrių keitimosi intervalas'); ?></th>
                <th></th>
            </thead></tr>
            <tbody>
                <?php foreach($slidesCollections as $slidesCollection){ ?>
                      <tr>
                          <td><?php echo $slidesCollection['SlidesCollection']['title']; ?></td>
                          <td><?php echo $slidesCollection['SlidesCollection']['transition_interval']; ?> s.</td>
                          <td><?php 
                            echo $this->Html->link(__('Peržiūrėti'), array('action'=>'show', $slidesCollection['SlidesCollection']['id']), array('target'=>'_blank','class'=>'btn btn-default')).'&nbsp;';
                            if($hasAccessToEdit){
                                echo $this->Html->link(__('Redaguoti'), array('action'=>'edit', $slidesCollection['SlidesCollection']['id']), array('class'=>'btn btn-info')).'&nbsp;';
                            }
                            if($hasAccessToRemove){
                                echo $this->Html->link(__('Pašalinti'), array('action'=>'remove_collection', $slidesCollection['SlidesCollection']['id']), array('class'=>'btn btn-danger'),__('Ar tikrai norite pašalinti šį rinkinį'));
                            } ?></td>
                      </tr>
                <?php } ?>
            </tbody>
        </table>
    </div>
</div>
