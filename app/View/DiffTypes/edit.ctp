<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('parent_id', array('label' => __('Tėvinis'), 'options' => $parentOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('code', array('label' => __('Kodas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<div>
	<br />
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a href="<?php echo $listUrl; ?>" class="btn btn-default"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>

