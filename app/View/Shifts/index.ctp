<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Nauja pamaina'); ?></button>
<br /><br /> */ ?>
<div class="btn-group pull-right">
    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Padalinys') ?>:</strong>&nbsp;<?php echo isset($this->params['named']['branch_id'])?$branchOptions[$this->params['named']['branch_id']]:__('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
    <ul class="dropdown-menu">
        <?php foreach ($branchOptions as $branchId => $branch): ?>
        <li><a href="<?php echo Router::url(array('branch_id'=>$branchId)); ?>"><?php echo $branch; ?></a></li>
        <?php endforeach; ?>            
    </ul>
</div>
<table class="table table-bordered table-striped" id="shiftsTable">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Pavadinimas'); ?></th>
			<th><?php echo __('PS ID'); ?></th>
			<th><?php echo __('Tipas'); ?></th>
			<th><?php echo __('Padalinys'); ?></th>
			<th><?php echo __('Pradžios data ir laikas'); ?></th>
			<th><?php echo __('Pabaigos data ir laikas'); ?></th>
			<th></th>
			<?php /* <th>&nbsp;</th> */ ?>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="8"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; ?>
		<tr class="<?php echo $li->disabled?'disabled':'enabled'; ?>">
			<td><?php echo $li->id; ?></td>
			<td><?php echo $li->name; ?></td>
			<td><?php echo $li->ps_id; ?></td>
			<td><?php echo $li->type; ?></td>
			<td><?php echo isset($branchOptions[$li->branch_id]) ? $branchOptions[$li->branch_id] : $li->branch_id; ?></td>
			<td><?php echo $li->start; ?></td>
			<td><?php echo $li->end; ?></td>
			<td><?php echo $this->Html->link(($li->disabled?__('Įjungti'):__('Išjungti')),Router::url(array_merge(array('action'=>'disableEnableShift', $li->id), $this->request->params['named']),true), array('escape'=>false,'class'=>'btn btn-default')); ?></td>
			<?php /* <th>
				<a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				&nbsp;
				<a href="<?php printf($removeUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
			</th> */ ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(); ?></div>
