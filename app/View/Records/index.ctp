<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas įrašas'); ?></button>
<br /><br /> */ ?>

<div class="clearfix"></div>
<div class="row-fluid">
    <div class="col-xs-12">
        <?php if(isset($dataSum)){ ?>
            <div class="pull-left" style="padding-right: 20px;">
                <label><?php echo __('Kiekis'); ?>: </label><?php echo $dataSum[0]['totalQ']; ?><br />
                <label><?php echo __('Kiekis vnt.'); ?>: </label><?php echo $dataSum[0]['totalUQ']; ?>
            </div>
        <?php } ?>
        <div class="pull-left" style="padding-right: 20px;">
            <label><?php echo __('Nuo - iki'); ?>: </label>
            <input type="text" name="dateStart" class="form-control records_daterange" style="width: 250px;" value="<?php echo isset($date)?$date:''; ?>" />
        </div>
        <div class="pull-left">
            <label class="hidden-xs">&nbsp;</label><br class="hidden-xs" />
            <?php echo $this->Html->link(__('Paskutiniai įrašai'), array('last_records'=>'1'), array('class'=>'btn btn-primary')); ?>
        </div>
        <div class="pull-left" style="position: relative;">
            <label class="hidden-xs">&nbsp;</label><br class="hidden-xs" />
            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo $sensorsLabel ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu">
                <?php foreach ($sensorOptions as $lid => $li): ?>
                    <li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
<div class="clearfix"></div>
<table class="table table-bordered table-striped" style="margin-top: 10px;">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Data ir laikas'); ?></th>
			<th><?php echo __('Kiekis'); ?></th>
			<th><?php echo __('Kiekis (vnt)'); ?></th>
			<th><?php echo $sensorsLabel; ?></th>
			<th><?php echo __('Padalinys'); ?></th>
			<th><?php echo __('Planas'); ?></th>
			<th><?php echo __('Pamaina'); ?></th>
			<?php /* <th>&nbsp;</th> */ ?>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model];
			$liPlan = (isset($li_[Plan::NAME]) ? ((object) $li_[Plan::NAME]) : null);
			?>
		<tr>
			<td><?php echo $li->id;?> <?php echo "(Shift: ".$li->shift_id.")"; ?> <?php if(Configure::read('debug')>0 && $li->found_problem_id != null) echo "<b>(fProblem: ".$li->found_problem_id.")</b>"; ?></td>
			<td><?php $cdt = new DateTime($li->created); echo $cdt->format('Y-m-d H:i:s'); ?></td>
			<td><?php echo $li->quantity; ?></td>
			<td><?php echo $li->unit_quantity; ?></td>
			<td><?php echo Sensor::buildName($li_, false); ?></td>
			<td><?php echo Branch::buildName($li_); ?></td>
			<td><?php echo $li->plan_id ? Plan::buildName($li_) : '&mdash;'; ?> <?php echo ($li->approved_order_id) ? "(A.Order Id: ".$li->approved_order_id.")" : ""; ?></td>
			<td><?php echo $li->shift_id ? Shift::buildName($li_) : '&mdash;'; ?></td>
			<?php /* <td>
				<a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				&nbsp;
				<a href="<?php printf($removeUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
			</td> */ ?>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
<script type="text/javascript">
	jQuery(document).ready(function(){
		
		var updateLink = function(){
		    
		    if($('input[name="dateStart"]').val().length > 0){
		        dateT = $('input[name="dateStart"]').val();
		    }
		    // if($('input[name="dateEnd"]').val().length > 0){
		        // //console.log($('input[name="dateEnd"]').val())
		        // dateT['endD'] = $('input[name="dateEnd"]').val();
		    // }
		    $('ul.dropdown-menu li a').each(function(){
		    	var split = $(this).attr('href').split('?');
		    	$(this).attr('href', split[0] + (!$.isEmptyObject(dateT)?'?date='+ encodeURIComponent(dateT):'') );
		    });
		}
		
	$('input.records_daterange').daterangepicker({
        locale: {
            format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
            firstDay: 1, //to start week by Monday
            daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
            monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
            applyLabel: "<?php echo __('Išsaugoti') ?>",
            cancelLabel: "<?php echo __('Atšaukti') ?>",
            fromLabel: "<?php echo __('Nuo') ?>",
            toLabel: "<?php echo __('Iki') ?>",
            separator: " ~ " 
        },
        forceUpdate: true,
        timePicker: true,
        timePicker24Hour: true,
        timePickerIncrement: 1,
        opens: 'right',
      }, function(start, end, label) {
        //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
    }).on('hide.daterangepicker', function(ev, picker) {
          updateLink();
    });
		// jQuery(".datepicker").datepicker({ dateFormat: 'yy-mm-dd'}).on('change', function(ev){
		   // updateLink();
		// });
	});
</script>