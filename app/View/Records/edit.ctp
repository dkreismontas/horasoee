<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('created', array('label' => __('Data ir laikas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('quantity', array('label' => __('Kiekis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('sensor_id', array('label' => __('Jutiklis'), 'options' => $sensorOptions, 'empty' => '', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('plan_id', array('label' => __('Plano ID'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('shift_id', array('label' => __('Pamainos ID'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<div>
	<br />
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>

