<?php
$this->Html->script('Tv.loader',array('inline' => false));
$this->Html->script('gauge.min',array('inline' => false));
$this->Html->script('vue.js?v=1',array('inline' => false));
$this->Html->css('work-center-24h.css?v=1',null,array('inline'=>false));
if(file_exists(ROOT.'/app/Plugin/'.$companyTitle.'/webroot/css/tv.css'))
    $this->Html->css($companyTitle.'.tv',null,array('inline'=>false));
?>
<div id="mashines">
    <div class="row-fluid" v-for="(row_nr, row_sensors) in sensors" style="height: {{heightPerc}}%;">
        <sensor_comp v-for="(sensor_id, sensor) in row_sensors" :wc="sensor"></sensor_comp>
    </div>
</div>
<template id="sensorTpl">
    <div class="sensor sensor_{{wc.sensor.id}} col-md-12 col-lg-{{12/columns}}">
        <h1><span><i></i></span>{{wc.sensor.name|shortenString}}</h1>
        {{{wc.sensor.tv_legends}}}
        <div class="row-fluid">
            <div class="col-xs-9 hours">
                <div v-for="(hour_nr, hour) in wc.hours">
                    <div class="stack">
                        <!--div v-if="hour.unknown_time > 0" class="unknown" style="height: {{hour.unknown_time / 3600 * 100}}%;"></div-->
                        <div v-if="hour.grey_time > 0" class="grey" style="height: {{hour.grey_time / (3600-hour.unknown_time) * 100}}%;">{{Math.round(hour.grey_time / (3600-hour.unknown_time) * 100)}}</div>
                        <div v-if="hour.red_time > 0" class="red" style="height: {{hour.red_time / (3600-hour.unknown_time) * 100}}%;">{{Math.round(hour.red_time / (3600-hour.unknown_time) * 100)}}</div>
                        <div v-if="hour.yellow_time > 0" class="yellow" style="height: {{hour.yellow_time / (3600-hour.unknown_time) * 100}}%;">{{Math.round(hour.yellow_time / (3600-hour.unknown_time) * 100)}}</div>
                        <div v-if="hour.green_time > 0" class="green" style="height: {{hour.green_time / (3600-hour.unknown_time) * 100}}%;">{{Math.round(hour.green_time / (3600-hour.unknown_time) * 100)}}</div>
                    </div>
                    <br />
                    <label>{{hour_nr|extract_hour}}</label>
                </div>
            </div>
            <div class="col-xs-3 donut">
                <div class="graph">
                    <div id="graph{{wc.sensor.id}}"></div>
                    <table>
                        <tr>
                            <td class="donutCell">
                                <div id="donutchart1" class="donutDiv"></div>
                                <div class="centerLabel"><p>OEE</p><p>{{wc.oee_data.oee}} %</p></div>
                            </td>
                    </table>
                </div>
                <div class="factors">
                    <div v-if="wc.oee_data.exploitation_factor">
                        <p><?php echo __('Prieinamumas') ?> <b>{{(wc.oee_data.exploitation_factor).toFixed(0)}}%</b></p>
                        <div class="exploitationIndicator indicator"><div style="width: {{wc.oee_data.exploitation_factor}}%;"></div></div>
                    </div>
                    <div v-if="wc.oee_data.operational_factor">
                        <p><?php echo __('Efektyvumas') ?> <b>{{(wc.oee_data.operational_factor).toFixed(0)}}%</b></p>
                        <div class="operationalIndicator indicator"><div style="width: {{wc.oee_data.operational_factor}}%;"></div></div>
                    </div>
                    <div v-if="wc.oee_data.calculate_quality > 0 AND wc.oee_data.quality_factor">
                        <p><?php echo __('Kokybės koef.') ?> <b>{{(wc.oee_data.quality_factor).toFixed(2)}}%</b></p>
                        <div class="qualityIndicator indicator"><div style="width: {{wc.oee_data.quality_factor}}%;"></div></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>



<script type="text/javascript">
    setInterval(function(){location.reload();}, 1800000);
    var vm = new Vue({
        el: '#mashines',
        data: {
            sensors: {},
            columns: 1,
            heightPerc: 0,
        },
        methods: {
            loadData: function(){
                jQuery.ajax({
                    url: "<?php echo $this->Html->url('/work_center_24h/get_data/'.$workCenters,true); ?>",
                    type: 'post',
                    context: this,
                    dataType: 'JSON',
                    async: true,
                    success: function(request){
                        this.sensors = request.sensors;
                        this.columns = this.sensors.length > 0?this.sensors[0].length:1;
                        this.heightPerc = this.sensors.length > 1?50:100;
                    }
                })
            }
        },
        beforeCompile: function(){ 
            this.loadData();
            setInterval(this.loadData,60000);
        },
        components: {
            'sensor_comp': {
                template: '#sensorTpl',
                props: ['wc'],
                data: function(){return { columns:0}},
                beforeCompile: function(){
                    this.columns = this.$parent.columns;
                },
                ready: function(){
                    this.buildGraph(); 
                },
                methods: {
                    buildGraph: function(){
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(this.drawChart);
                    },
                    drawChart: function(){
                        var data = google.visualization.arrayToDataTable([
                          ['24H', 'Percentage'],
                          ['active', this.wc.oee_data.green_time],
                          ['downtime', this.wc.oee_data.red_time],
                          ['prep', this.wc.oee_data.yellow_time],
                          ['inactive', this.wc.oee_data.grey_time],
                        ]);
                        var options = {
                          pieHole: 0.5,
                          pieSliceTextStyle: {
                            color: 'black', fontSize: this.columns==1?'20':'14'
                          },
                          tooltip : {
                             trigger: 'none'
                          },
                          pieSliceText: this.wc.oee_data.single_type_exist==1?'none':'value',//'percentage',
                          legend: 'none',
                          'chartArea': {'width': '85%', 'height': '85%'},
                          'backgroundColor': '#fff',
                          slices: {
                            0: { color: '#18a418' },
                            1: { color: '#df0404' },
                            2: { color: '#ffd800' },
                            3: { color: '#b7b7b7' },
                          }
                        };
                        var chart = new google.visualization.PieChart(document.getElementById('graph'+this.wc.sensor.id));
                        google.visualization.events.addListener(chart, 'ready', this.myReadyHandler);
                        google.visualization.events.addListener(chart, 'error', function (googleError) {
                            google.visualization.errors.removeError(googleError.id);
                        });
                        chart.draw(data, options);
                        
                    }
                },
                filters: {
                    shortenString: function(string){
                        return string.length > 28?string.substring(0, 28)+'...':string;
                    },
                    extract_hour: function(hour_nr){
                        return hour_nr.split(' ')[1];
                    }
                }
            }
        }
    });
</script>
