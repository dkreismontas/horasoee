<?php
    ob_end_clean();
    error_reporting(0);
    $styleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman'
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
    );
    $headerStyleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman',
            'bold'=>true
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'wrap'=>true,
            'rotation'   => 0,
        ),
    );

//HEADERS
$currentWorkerId = $currentSheetNr = 0;
foreach($data as $rec){
    if($currentWorkerId != $rec['User']['id']){
        $activeSheet = $objPHPExcel->createSheet($currentSheetNr);
        $objPHPExcel->setActiveSheetIndex($currentSheetNr);
        $currentSheetNr++;
        $activeSheet->setTitle(substr($rec[0]['worker'],0,31));
        $colNr = 0; $rowNr = 1;
        $currentWorkerId = $rec['User']['id'];
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Įrenginys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamybos laikas (val.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbo laikas (val.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagamintas kiekis, vnt.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Planinis kiekis, vnt.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Formų greitis vnt/h.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Planinis derinimo laikas (min.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Faktinis derinimo laikas + viršijimo laikas (min.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
        $colLetter = 'A';
        for ($col = 0; $col <= $colNr; $col ++) {
            $activeSheet->getColumnDimension($colLetter++)->setAutoSize(true);
        }
    }
    //CONTENT
    $colNr = 0; $rowNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['Sensor']['name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['Plan']['production_name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, bcdiv($rec[0]['production_time'],3600,2));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, bcdiv($rec[0]['work_time'],3600,2));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec[0]['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, round(bcmul($rec['Plan']['step'],bcdiv($rec[0]['production_time'],3600,2),3)));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['Plan']['step']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['Plan']['preparation_time']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec[0]['total_transition_time']);
    for($i = 0; $i < $colNr; $i++){
        $activeSheet->getStyleByColumnAndRow($i, $rowNr)->applyFromArray($styleArray);
    }
}
// for ($col = 'A'; $col != 'G'; $col++){
    // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
// }
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . date('Y-m-d') . '_' . time() . '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}