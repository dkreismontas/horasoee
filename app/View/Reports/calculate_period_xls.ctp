<?php
$sheet_number = 0;
$currentSensorId = 0;
$weekNr = '';
$calculation[][] = array('sensor_id'=>-1, 'last'=>true, 'shift_length'=>0);//reikalingas kad issivestu paskutinio sheet apibendrintas stulpelis
$dateObj = new DateTime();
foreach($calculation as $sensorData){ $sensorData = current($sensorData);
    if(!isset($sensorData['shift_length'])) continue;
    if($currentSensorId != $sensorData['sensor_id']){
        $row = isset($sensors[$currentSensorId])?5:4;
        if(isset($sumData)){ //Apibendrintu duomenu stulpelis
            $sumData['sensor_id'] = $sensorData['sensor_id'];
            $workSheet->setCellValueByColumnAndRow($col, 2, __('Bendri'));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['shift_length'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['shift_length_with_exclusions'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['theory_prod_time'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['fact_prod_time'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['true_problem_time'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['true_problem_count']);
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['transition_time'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['transition_count']);
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['theorical_transition_time']);
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['no_work_time'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['fact_prod_time'] > 0?round(bcdiv($sumData['fact_prod_time'] - $sumData['slow_speed_duration'], $sumData['fact_prod_time'],4) * 100,2):0);
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['slow_speed_duration'],2));
            $oeeParams = $recordModel->calculateOee($sumData);
            // $explotationalFacor = $sumData['shift_length'] > 0?$sumData['fact_prod_time'] / $sumData['shift_length']:0; //prieinamumas
            // $explotationalFacorExclusion = $sumData['shift_length_with_exclusions'] > 0?$sumData['fact_prod_time'] / $sumData['shift_length_with_exclusions']:0;//prieinamumas su isimtimis
            // $operationalFacor = $sumData['fact_prod_time'] > 0?$sumData['count_delay'] / $sumData['fact_prod_time']:0; //efektyvumas
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['exploitation_factor'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['exploitation_factor_exclusions'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['operational_factor'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['quality_factor'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['oee_exclusions'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, round($oeeParams['oee'],2));
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['true_problem_count_exclusions'] > 0?round($sumData['true_problem_time_exclusions'] / $sumData['true_problem_count_exclusions'], 2):0);
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['mtt_problem_count'] > 0?round($sumData['mtt_problem_time'] / $sumData['mtt_problem_count'], 2):0);
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['true_problem_count_exclusions'] > 0?round(($sumData['fact_prod_time'] + $sumData['transition_time']) / $sumData['true_problem_count_exclusions'], 2):0);
            $workSheet->setCellValueByColumnAndRow($col, $row++, $sumData['mtt_problem_count'] > 0?round(($sumData['fact_prod_time'] + $sumData['transition_time']) / $sumData['mtt_problem_count'], 2):0);
            //$workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['exploitation_factor'] / ($col-1),2));
            //$workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['operational_factor'] / ($col-1),2));
            //$workSheet->setCellValueByColumnAndRow($col, $row++, round($sumData['oee'] / ($col-1),2));
        }
        if(isset($sensorData['last'])) break;
        $currentSensorId = $sensorData['sensor_id'];
        if ($sheet_number != 0) {
            $objPHPExcel->createSheet();
        }
        $workSheet = $objPHPExcel->setActiveSheetIndex($sheet_number++);
        for ($col = 'A'; $col !== 'P'; $col ++) {
            $workSheet->getColumnDimension($col)->setAutoSize(true);
        }

        $row = 1;
        //$workSheet->setTitle(substr(preg_replace('/[^\s\da-zA-Z]/','',!isset($sensors[$currentSensorId])?__('Visi jutikliai'):$sensors[$currentSensorId]),0,30));
        $workSheet->setTitle(preg_replace('/[^\s\da-zA-Z]/', '', mb_substr((!isset($sensors[$currentSensorId])?__('Visi jutikliai'):$sensors[$currentSensorId]),0,30)), false);
        if(isset($sensors[$currentSensorId])){
            $branchId = $sensorData['branch_id'];
            $workSheet->mergeCells('B1:D1');
            $workSheet->setCellValueByColumnAndRow(0, $row, __('Duomenų laikotarpis'));
            $workSheet->setCellValueByColumnAndRow(1, $row++, ($shiftsExtremums[$branchId]['first_shift_start']??'') . ' - ' . ($shiftsExtremums[$branchId]['last_shift_end']??''));
        }
        $workSheet->setCellValueByColumnAndRow(0, $row++, $periodDetails['title']);
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Data'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Rodikliai'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Pamainų trukmė (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Pamainų trukmė su išimtimis (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Teorinis gamybos laikas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Gamybos laikas faktas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Prastovų laikas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Prastovų kiekis (vnt)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Derinimų laikas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Derinimų kiekis (vnt)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Teorinis derinimų laikas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Nėra darbo laikas (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Gero greičio minučių kiekis (%)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Lėtesnių nei numatyta darbo minučių laikas'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Prieinamumo koef.'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Prieinamumo koef. su išimtimis'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Efektyvumo koef.'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('Kokybės koef.'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('OEE'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('OEE be išimčių'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('MTTR (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('MTTR (pažymėtoms prastovoms, min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('MTBF (min)'));
        $workSheet->setCellValueByColumnAndRow(0, $row++, __('MTBF (pažymėtoms prastovoms, min)'));
        $col = 1;
        $sumData = $sensorData;
        array_walk($sumData, function(&$data,$key){ if(is_numeric($data)){ $data = 0; }else{ $data=''; } });
        $sumData = array_filter($sumData, function($data){ return $data === 0; });
    }
    $row = isset($sensors[$currentSensorId])?2:1;
    if($viewAngle == 1){ //tik kai savaitinis grupavimas
        
    }
    switch($viewAngle){
        case 1: //savaitine
            preg_match('/(\d{4})-(\d{1,2})/i', $sensorData['nr'], $match); unset($match[0]);
            list($year,$week) = array_values($match);
            $dateCol = $dateObj->setISODate($year, $week, 1)->format('Y-m-d');
        break;
        case 3://para
            preg_match('/(\d{4})-(\d{1,3})/i', $sensorData['nr'], $match); unset($match[0]);
            list($year,$day) = array_values($match);
            $dateColObj = DateTime::createFromFormat('Y-z', $year.'-'.$day);
            $dateCol = $dateColObj->format('Y-m-d');
            $sensorData['nr'] = $year.'-'.($day+1).' '.__('para'); //didiname para per 1d, nes duombazej paros indeksuojasi nuo 0, o vartotojui reikia rodyti nuo 1
        break;
        default:
            $dateCol = $sensorData['nr'];
    }
    //pr(round($sensorData['slow_speed_duration'],2));die();
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['nr']);
    $workSheet->setCellValueByColumnAndRow($col, $row++, $dateCol);
    $workSheet->setCellValueByColumnAndRow($col, $row++, '');
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['shift_length'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['shift_length_with_exclusions'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['theory_prod_time'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['fact_prod_time'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['true_problem_time'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['true_problem_count']);
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['transition_time'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['transition_count']);
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['theorical_transition_time']);
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['no_work_time'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['fact_prod_time'] > 0?round(bcdiv(bcsub($sensorData['fact_prod_time'], $sensorData['slow_speed_duration'],6), $sensorData['fact_prod_time'],4) * 100,2):0);
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['slow_speed_duration'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['exploitation_factor'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['exploitation_factor_with_exclusions'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['operational_factor'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['quality_factor'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['oee_with_exclusions'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, round($sensorData['oee'],2));
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['true_problem_count_exclusions'] > 0?round($sensorData['true_problem_time_exclusions'] / $sensorData['true_problem_count_exclusions'], 2):0);
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['mtt_problem_count'] > 0?round($sensorData['mtt_problem_time'] / $sensorData['mtt_problem_count'], 2):0);
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['true_problem_count_exclusions'] > 0?round(($sensorData['fact_prod_time'] + $sensorData['transition_time']) / $sensorData['true_problem_count_exclusions'], 2):0);
    $workSheet->setCellValueByColumnAndRow($col, $row++, $sensorData['mtt_problem_count'] > 0?round(($sensorData['fact_prod_time'] + $sensorData['transition_time']) / $sensorData['mtt_problem_count'], 2):0);
    
    foreach($sensorData as $key => $value){
        if(isset($sumData[$key])){
            $sumData[$key] += $value;
        }
    }
    $col++;
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="' . __('Periodo ataskaita') . '.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter->save('php://output');
    exit();
}