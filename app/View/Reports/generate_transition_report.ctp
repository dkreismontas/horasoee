<?php
ob_end_clean();
$row = 0;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Produktas iš"));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Produktas į"));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Perėjimo pav."));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Pradžia"));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Pabaiga"));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Trukmė"));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($row++, 1, __("Darbuotojas"));
foreach($problems as $key => $problem){
    $startDate = new DateTime($problem['FoundProblem']['start']);
    $endDate = new DateTime($problem['FoundProblem']['end']);
    $worker = isset($problem['User']['first_name'])?$problem['User']['first_name'].' '.$problem['User']['last_name']:'';
    if(!empty($problem['ApprovedOrderPrev'])){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $key+2, $problem['ApprovedOrderPrev']['Plan']['production_code'].' '.$problem['ApprovedOrderPrev']['Plan']['production_name']);
    }
    if(!empty($problem['ApprovedOrderNext'])){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $key+2, $problem['ApprovedOrderNext']['Plan']['production_code'].' '.$problem['ApprovedOrderNext']['Plan']['production_name']);
    }
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $key+2, $problem['FoundProblem']['transition_problem_id'] > 0?$problem['TransitionProblem']['name']:Settings::translate($problem['Problem']['name']));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $key+2, $problem['FoundProblem']['start']);
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(3, $key+2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME2);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $key+2, $problem['FoundProblem']['end']);
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(4, $key+2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME2);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $key+2, $startDate->diff($endDate)->format("%H:%I:%S"));
    $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(5, $key+2)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_TIME4);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(6, $key+2,$worker);
}
for ($col = 'A'; $col != 'N'; $col++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$sensor['Sensor']['name'].' '.__('perėjimų ataskaita').'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter->save('php://output');
    exit();
}