<?php
$previous_date = array();
$mttr = array();
$mtbf = array();
$totalByProblemType = array();
$realProblemsCount = array();
$sheetsIndex = 0;
$sheetsParams = array();
foreach($shifts as $shift){
    $shiftId = $shift['Shift']['id'];
    if(!isset($problemsByShifts[$shiftId])){ continue; }
    $shiftsToWorkOn = array(
        0=>__('Visų pamainų įvykių žurnalas'),
        $shiftId => $shift['Shift']['type'].' '.str_replace(':', ' ', $shift['Shift']['start'])
    );
    foreach($shiftsToWorkOn as $shiftIdNr => $shiftTitle){
        if(!isset($sheetsParams[$shiftIdNr])) {
            $sheetsParams[$shiftIdNr]['sheet_index'] = $sheetsIndex;
            $workSheet = $objPHPExcel->createSheet($sheetsIndex++);
            $workSheet->setTitle($shiftTitle, false);
            $letterStart = 'A';
            $currentRow = $sheetsParams[$shiftIdNr]['current_row'] = 1;
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Darbo centras'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('MO'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio pavadinimas'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio kodas'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Paaiškinimas'));
            for ($i = 0; $i <= $maxDowntimeLevel; $i++) {
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas %d', $i + 1));
            }
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Pradžia'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Pabaiga'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė tarp prastovų (min)'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Komentaras'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Plano greitis (step) vnt/h'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis'));
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis pakuotėje/porcijoje'));
            $workSheet->getStyle('A' . ($currentRow - 1) . ':Q' . $currentRow)->getFont()->setBold(true);
            $sheetsParams[$shiftIdNr]['current_row']++;
            $mttr[$shiftIdNr] = 0;
            $mtbf[$shiftIdNr] = 0;
            $realProblemsCount[$shiftIdNr] = 0;
        }else{
            $workSheet = $objPHPExcel->setActiveSheetIndex($sheetsParams[$shiftIdNr]['sheet_index']);
        }
        foreach ($problemsByShifts[$shiftId] as $fp) {
            $currentRow = $sheetsParams[$shiftIdNr]['current_row'];
            try{
                $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
            }catch(Exeption $e){
                $approvedAddData = null;
            }
            $start_column = 'A';
            $fp['Sensor']['name'] = $fp['Sensor']['name'].' '.(isset($fp['Factory']['name'])?$fp['Factory']['name']:'');
            $workSheet->setCellValue($start_column . $currentRow, $fp['Sensor']['name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['DiffCard']['description']);
            $problemId = $fp['FoundProblem']['transition_problem_id'] > 0?$fp['FoundProblem']['transition_problem_id']:$fp['Problem']['id'];
            $parsedProblemsTree = $problemId > 0 && isset($problemsTreeList[$problemId])?array_reverse($problemsTreeList[$problemId]):array();
            for($i=0; $i <= $maxDowntimeLevel; $i++){
                if(isset($parsedProblemsTree[$i])){
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($parsedProblemsTree[$i]));
                }else{
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($fp['Problem']['name']));
                }
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['start']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['end']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($fp[0]['found_problem_duration'] / 60,2));
            if (!isset($previous_date[$shiftIdNr]) && strtotime($fp['FoundProblem']['start']) >= strtotime($shift['Shift']['start'])) {
                $previous_date[$shiftIdNr] = $shift['Shift']['start'];
            }
            $date_diff = 0;
            if($fp['FoundProblem']['problem_id'] >= Problem::ID_NOT_DEFINED && !in_array($fp['FoundProblem']['problem_id'], $downtimesExclusionsIds) && strtotime($fp['FoundProblem']['start']) > 1000 && strtotime($fp['FoundProblem']['end']) > 1000) {
                $realProblemsCount[$shiftIdNr]++;
                $mttr[$shiftIdNr] += $fp[0]['found_problem_duration'];
                if (isset($previous_date[$shiftIdNr])) {
                    $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date[$shiftIdNr]);
                    $date_diff = $date_diff > 0?$date_diff:0;
                    $mtbf[$shiftIdNr] += $date_diff;
                }
                $previous_date[$shiftIdNr] = $fp['FoundProblem']['end'];
            }else{ //kadangi mtbf yra tarpu tarp prastovu vidurkis, tai is esmes tai yra darbo, derinimo ir exlusion prastovu sumos dalyba is tikru prastovu kiekio
                $mtbf[$shiftIdNr] += $fp[0]['found_problem_duration'];
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, ($date_diff > 0) ? round($date_diff / 60, 2) : "");
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['comments']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['step']);
            $workSheet->setCellValue(++ $start_column . $currentRow, getIntervalQuantity($fp));
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['box_quantity']);
            //$start_column++;
            $backgroundColor = isset($colors[$fp['FoundProblem']['problem_id']])?$colors[$fp['FoundProblem']['problem_id']]:$colors[3];
            $backgroundColor = in_array($fp['FoundProblem']['problem_id'], $downtimesExclusionsIds)?$colors[2]:$backgroundColor;
            //do{
            $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => $backgroundColor)
                    )
                )
            );
            //}while($bgCol != $start_column);
            $sheetsParams[$shiftIdNr]['current_row']++;
            if(!isset($totalByProblemType[$shiftIdNr][$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']])){
                $totalByProblemType[$shiftIdNr][$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']] = array('problem_name'=>$fp['Problem']['name'], 'total'=>0,'sensor'=>$fp['Sensor']['name']);
            }
            if(strtotime($fp['FoundProblem']['start']) > 1000 && strtotime($fp['FoundProblem']['end']) > 1000) { //apsauga jei vienas is laiku bus 1970-01-01
                $totalByProblemType[$shiftIdNr][$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']]['total'] += strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start']);
            }
        }
        for ($col = 'A'; $col != $start_column; $col++){
            $workSheet->getColumnDimension($col)->setAutoSize(true);
        }
    }
}
foreach($totalByProblemType as $shiftIdNr => $problemTypes){
    $workSheet = $objPHPExcel->setActiveSheetIndex($sheetsParams[$shiftIdNr]['sheet_index']);
    $currentRow = ++$sheetsParams[$shiftIdNr]['current_row'];
    $letterStart = 'A';
    $workSheet->mergeCells('A'.$currentRow.':B'.$currentRow);
    $workSheet->setCellValue($letterStart . $currentRow, __('Visų pamainų susumuotas įvykių laikų žurnalas'));
    $workSheet->setCellValue($letterStart++ . ++ $currentRow, __('Darbo centras'));
    $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas'));
    $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
    $workSheet->getStyle('A' . ($currentRow - 1) . ':C' . $currentRow)->getFont()->setBold(true);
    foreach($problemTypes as $sensorDowntimes){
        foreach($sensorDowntimes as $sensorDowntime){
            $letterStart = 'A';
            $currentRow++;
            $workSheet->setCellValue($letterStart++ . $currentRow, $sensorDowntime['sensor']);
            $workSheet->setCellValue($letterStart++ . $currentRow, Settings::translate($sensorDowntime['problem_name']));
            $workSheet->setCellValue($letterStart++ . $currentRow, bcdiv($sensorDowntime['total'],60,2));
        }
    }
    if ($realProblemsCount[$shiftIdNr] > 0) {
        $mtbf[$shiftIdNr] /= $realProblemsCount[$shiftIdNr];
        $mttr[$shiftIdNr] /= $realProblemsCount[$shiftIdNr];
    } else {
        $mtbf[$shiftIdNr] = 0;
        $mttr[$shiftIdNr] = 0;
    }
    $currentRow++;
    $workSheet->setCellValue('A' . ++ $currentRow, __('MTTR (min)'));
    $workSheet->setCellValue('B' . $currentRow, round($mttr[$shiftIdNr]/60, 2));
    $workSheet->setCellValue('A' . ++ $currentRow, __('MTBF (min)'));
    $workSheet->setCellValue('B' . $currentRow, round($mtbf[$shiftIdNr]/60, 2));
    $sheetsParams[$shiftIdNr]['current_row'] = $currentRow;
}
$objPHPExcel->setActiveSheetIndex(0);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else{
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.$sensor['Sensor']['name'].' '.__('įvykių žurnalas').'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter->save('php://output');
    exit();
}

function getIntervalQuantity($data){
    if($data['Sensor']['plan_relate_quantity'] == 'quantity'){ return $data['FoundProblem']['quantity']; }
    $multiplier = 1;
    if(isset($data['ApprovedOrder']) && !empty($data['ApprovedOrder']) && $data['ApprovedOrder']['box_quantity'] > 0){
        $multiplier = bcdiv($data['ApprovedOrder']['quantity'],$data['ApprovedOrder']['box_quantity'],6);
    }
    return bcmul($data['FoundProblem']['quantity'],$multiplier,2);
}