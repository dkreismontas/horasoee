<div class="row-fluid" style="color: #fff;">
    <div class="col-xs-2"></div>
    <div class="col-xs-8" style="padding-top: 100px;">
        <h2 class="text-center"><?php echo __('Pasirinkite vieną iš galimų darbo centrų'); ?></h2><br /><br />
        <div class="row-fluid">
        <?php
        foreach($sensors as $sensor){ 
            $disabled = time() - strtotime($sensor['Sensor']['user_update_time']) < 60?true:false;
            $style='width: 100%; font-size: 20px; margin-bottom: 20px; padding: 10px;'.($disabled?'background:#aaa;':'');
        ?>
            <div class="col-xs-12">
                <a href="<?php echo $disabled?'#':Router::url(array($sensor['Sensor']['id']),true); ?>" class="btn btn-success" style="<?php echo $style; ?>">
                    <?php echo $sensor['Sensor']['name'].($disabled?' <small style="color: #fff;">'.__('Užimtas kito operatoriaus').'</small>':''); ?>
                </a>
            </div>
        <?php } ?>
        </div>
    </div>
    <div class="col-xs-2"></div>
</div>
<script type="text/javascript">
    setInterval(function(){location.reload()},20000);
</script>
