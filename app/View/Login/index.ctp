<div class="loginpanel">
	<div class="loginpanelinner">
		<div class="logo animate0 bounceIn"><img src="images/logo.png" alt="" /></div>
		<form id="login" action="<?php echo $formUrl; ?>" method="post">
			<input type="hidden" name="login" value="1" />
			<input type="hidden" name="return" value="<?php echo (isset($returnUrl) && $returnUrl) ? htmlspecialchars($returnUrl) : ''; ?>" />
			<?php if (($msg = $this->Session->flash('loginMessage'))): ?>
			<div class="inputwrapper"><div class="alert alert-error"><?php echo $msg; ?></div></div>
			<?php endif; ?>
			<div class="inputwrapper animate1 bounceIn">
				<input type="text" name="login_user" value="<?php echo htmlspecialchars($uname); ?>" id="username" placeholder="<?php echo htmlspecialchars(__('Įveskite vartotojo vardą')); ?>" />
			</div>
			<div class="inputwrapper animate2 bounceIn">
				<input type="password" name="login_pass" value="<?php echo htmlspecialchars($upass); ?>" id="password" placeholder="<?php echo htmlspecialchars(__('Įveskite slaptažodį')); ?>" />
			</div>
			<div class="inputwrapper animate3 bounceIn">
				<button name="submit"><?php echo __('Prisijungti'); ?></button>
			</div>
			<?php /* <div class="inputwrapper animate4 bounceIn">
				<label><input type="checkbox" class="remember" name="login_remember" /> <?php echo __('Prisijungti automatiškai'); ?></label>
			</div> */ ?>
		</form>
	</div><!--loginpanelinner-->
</div><!--loginpanel-->

<div class="loginfooter">
    <p><?php echo $copyRightMsg; ?></p>
</div>