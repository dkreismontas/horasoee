<?php
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<div class="clearfix row">
    <form action="<?php echo htmlspecialchars($excelUrl); ?>" method="get" class="pull-left">
        <div class="row">
            <div class="col-xs-12">
                <label><?php echo __('Nuo - iki'); ?>:</label>&nbsp;
                <input type="text" name="fromto" class="form-control daterange" value="" style="width: 300px;" />
            </div>
            <!--div class="col-xs-6">
            		<label><?php echo __('Iki'); ?>:</label>&nbsp;
            		<input type="text" name="to" class="form-control data-date-picker" value="" placeholder="yyyy-mm-dd" style="width: 100px;" />
        		</div-->
        </div>
        <div class="row">
            <div class="col-xs-12">
                <button class="btn btn-default form-control" type="submit"><span class="iconsweets-excel"></span>&nbsp;<?php echo __('Excel exportas'); ?></button>
            </div>
        </div>
        <script type="text/javascript">
            $(function() { $('.data-date-picker').datepicker(<?php echo json_encode($this->App->datePickerConfig()); ?>); });
        </script>
    </form>
    <button class="btn btn-default pull-right" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Nauja neatitikties kortelė'); ?></button>
</div>
<br />
<div class="row">
    <table class="table table-bordered" id="diffCardTable">
        <?php if (empty($list)): ?>
            <tr><td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
        <?php endif; ?>
        <?php foreach ($list as $key => $li_):
            $li = (object) $li_[$model];
            $additionalData = isset($li_['ApprovedOrder'])?json_decode($li_['ApprovedOrder']['additional_data'],true):array();
            $nr = isset($additionalData['add_order_number'])?' (nr.: '.$additionalData['add_order_number'].')':'';
            $date = new DateTime($li->date);
            $columns = array(
                'id'=>array('ID',$li->id),
                'date_time'=>array(__('Data ir laikas'), $date->format('Y-m-d H:i')),
                'dc'=>array(__('Darbo centras'),Sensor::buildName($li_, false)),
                'loss_type'=>array(__('Nuostolio tipas'),DiffType::buildName($li_, false)),
                'description'=>array(__('Aprašymas'),$li->description),
                'product_name'=>array(__('Gaminys'), Plan::buildName($li_, false)),
                'mo'=>array(__('Mo'),$li_['Plan']['mo_number'].$nr),
                'downtime_reason'=>array(__('Prastovos priežastis'),Problem::buildName($li_, false)),
                'respinsive_person'=>array(__('Atsakingas asmuo'), isset($usersList[$li->responsive_section_user_id])?$usersList[$li->responsive_section_user_id]:''),
                'state'=>array(__('Būsena'), isset($states[$li->state]) ? $states[$li->state] : '&mdash;'),
                'actions'=>array('', $this->Html->link('<span class="glyphicon glyphicon-pencil"></span>&nbsp;'.__('Redaguoti'), sprintf($editUrl, $li->id), array('escape'=>false, 'class'=>'btn btn-default', 'style'=>'white-space: nowrap;')))
            );
            $parameters = array(&$columns, $li_);
            $helpComponent->callPluginFunction('View_DiffCardsIndex_TableFormation_Hook', $parameters, Configure::read('companyTitle'));
            if($key == 0){
                echo '<thead><tr><th>'.implode('</th><th>', Hash::extract(array_values($columns),'{n}.0')).'</th></tr></thead><tbody>';
            }
            echo '<tr style="background: '.$colors[$li->state].'"><td>'.implode('</td><td>', Hash::extract(array_values($columns),'{n}.1')).'</td></tr>';
            ?>
        <?php endforeach;
        echo sizeof($list) > 0?'</tbody>':'';
        ?>
    </table>
</div>
<div><?php echo $this->App->properPaging(); ?></div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('input.daterange').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ "
            },
            forceUpdate: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            opens: 'right',
        }, function(start, end, label) {
            //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
        <?php if (!empty($list)): ?>
        jQuery('#diffCardTable').dataTable({
            "sPaginationType": "full_numbers",
            //"bScrollInfinite" : true,
            "aLengthMenu": [[10,50,100, 200, 500, -1], [10,50, 100, 200, 500, "<?php echo __('Visi'); ?>"]],
            "aaSorting": [],
            "bScrollCollapse" : true,
            "sScrollY" : "800px",
            "iDisplayLength": -1,
            "paging": true,
            "scrollY": 500,
            "oLanguage":{
                'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
                'sSearch':'<?php echo __('Paieška');?>',
                'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
                'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
                'sInfoEmpty':"",
                'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
                'oPaginate':{
                    'sFirst': '<?php echo __('Pirmas');?>',
                    'sPrevious': '<?php echo __('Ankstesnis');?>',
                    'sNext': '<?php echo __('Kitas');?>',
                    'sLast': '<?php echo __('Paskutinis');?>'
                }
            },
            "oSearch": {"sSearch": "<?php echo $this->Session->read('FilterText.'.$this->params['controller']); ?>"}
        });
        <?php endif; ?>
        $('.maincontentinner').on('keyup','#diffCardTable_filter input', function(){
            $.ajax({
                url: '<?php echo Router::url(array('controller'=>'settings','action'=>'set_filter_text'),true); ?>',
                type: 'POST',
                data: {filter: 'FilterText.<?php echo $this->params['controller']; ?>', value: $(this).val()}
            });
        });
    });
</script>
