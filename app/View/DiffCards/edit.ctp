<?php
/* @var $this View */

$this->Html->css('bootstrap-timepicker.min', array('inline' => false));

?>
<div id="saveMessage">
    <?php if (($msg = $this->Session->flash('saveMessage'))): ?>
        <div class="alert alert-error"><?php echo $msg; ?></div>
    <?php endif; ?>
</div>
<div data-ng-app="DiffCard" data-ng-controller="DiffCardCtrl">
	<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off', 'type'=>'file')); ?>
	<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
	<?php echo $this->Form->input('approved_order_id', array('div' => null, 'label' => null, 'type' => 'hidden', 'value'=>isset($currentPlan['ApprovedOrder'])?$currentPlan['ApprovedOrder']['id']:0)); ?>
	<?php $date = $item ? new DateTime($item[DiffCard::NAME]['date']) : new DateTime(); ?>
<div class="row-fluid">
    <div class="col-lg-2"></div>
    <div class="col-lg-8">
        <table class="table table-bordered table-condensed">
		<tbody>
			<tr>
                <td class="narrow_col"><label><?php echo __('Vartotojas'); ?>:</label></td>
                <td colspan="2"><?php
                echo ($item && $item[DiffCard::NAME]['shift_manager_user_id'])
                    ? User::buildName($item, false, DiffCard::PFX_SHIFT_MANAGER)
                    : User::buildName($userData, false);
                ?></td>
            </tr>
			<tr><td class="narrow_col"><label><?php echo __('Neatitikties data'); ?>:</label></td><td colspan="2"><?php $d = new DateTime(); echo $d->format('Y-m-d H:i'); ?></td></tr>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Darbo centras'); ?>:</label></td><td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php if ($item): ?>
						<?php echo Sensor::buildName($item, false); ?>
					<?php else: ?>
						<?php echo $this->Form->input('sensor_id', array('label' => false, 'options' => $sensorOptions, 'class' => 'form-control', 'div' => null)); ?>
					<?php endif; ?>
				</td>
			</tr>
			<?php if ($item): ?>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Padalinys'); ?>:</label></td><td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php echo Branch::buildName($item); ?>
					<?php //echo $this->Form->input('branch_id', array('label' => false, 'options' => $branchOptions, 'class' => 'form-control', 'div' => null)); ?>
				</td>
			</tr>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Linija'); ?>:</label></td><td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php echo Line::buildName($item, false); ?>
					<?php //echo $this->Form->input('line_id', array('label' => false, 'options' => $lineOptions, 'class' => 'form-control', 'div' => null)); ?>
				</td>
			</tr>
			<?php endif; ?>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Intervalas'); ?>:</label></td>
				<td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php if ($item): ?>
						<?php
							$ds = new DateTime($item[FoundProblem::NAME]['start']);
							$de = new DateTime($item[FoundProblem::NAME]['end']);
							echo $ds->format('Y-m-d H:i').' &mdash; '.$de->format('Y-m-d H:i');
						?>
					<?php else: ?>
						<?php echo $this->Form->input('start_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'data-date-picker' => '', 'placeholder' => 'yyyy-mm-dd', 'div' => array('class' => 'date-field-holder input-group'))); ?>
						<?php echo $this->Form->input('start_time', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'data-time-picker' => '', 'placeholder' => 'HH:MM', 'div' => array('class' => 'time-field-holder'))); ?>
						<div class="date-field-holder-sep">&mdash;</div>
						<?php echo $this->Form->input('end_date', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'data-date-picker' => '', 'placeholder' => 'yyyy-mm-dd', 'div' => array('class' => 'date-field-holder input-group'))); ?>
						<?php echo $this->Form->input('end_time', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'data-time-picker' => '', 'placeholder' => 'HH:MM', 'div' => array('class' => 'time-field-holder'))); ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Gaminys'); ?>:</label></td><td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php if ($item): ?>
						<?php echo Plan::buildName($item, true); ?>
					<?php else: ?>
						<?php echo $this->Form->input('plan_id', array('label' => false,  'options' => array('-- '.__('Jokio gaminio').' --') + $planOptions, 'class' => 'form-control', 'div' => null)); ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="narrow_col<?php if (!$item) echo ' require-input-label'; ?>"><label><?php echo __('Prastovos priežastis'); ?>:</label></td><td colspan="2"<?php if (!$item) echo ' class="require-input"'; ?>>
					<?php if ($item): ?>
						<?php echo Problem::buildName($item, false); ?>
					<?php else: ?>
						<?php echo $this->Form->input('problem_id', array('label' => false, 'options' => $problemOptions, 'class' => 'form-control', 'div' => null)); ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="narrow_col require-input-label"><label><?php echo __('Nuostolių kiekis'); ?>:</label></td>
				<td colspan="2" class="require-input">
					<?php if ((!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) && $canEdit): ?>
						<?php echo $this->Form->input('consequences', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'div' => null)); ?>
					<?php else: ?>
						<?php echo ($item && $item[DiffCard::NAME]['consequences']) ? htmlspecialchars($item[DiffCard::NAME]['consequences']) : '&mdash;'; ?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
				<td class="require-input-label"><label><?php echo __('Neatitikties aprašymas') ?>:</label></td>
                <td colspan="2" class="require-input">
                    <?php if ((!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) && $canEdit): ?>
                        <?php echo $this->Form->input('description', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
                    <?php else: ?>
                        <?php echo $item ? $this->App->multilineText($item[DiffCard::NAME]['description']) : '&mdash;'; ?>
                    <?php endif; ?>
                </td>
			</tr>
			<tr>
                <td class="require-input-label"><label><?php echo __('Neatitikties tipas') ?>:</label></td>
				<td colspan="2" class="require-input">
					<?php if ((!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) && $canEdit): ?>
						<?php echo $this->Form->input('diff_type_id', array('div' => null, 'label' => null, 'type' => 'hidden', 'data-ng-value' => 'diffOption.DiffType.id')); ?>
						<select data-ng-model="diffOption" data-ng-options="diffOpt.DiffType.label for diffOpt in diffOptions" class="form-control"></select>
					<?php else: ?>
						<?php echo ($item && ($v = htmlspecialchars(DiffType::buildName($item, false)))) ? $v : '&mdash;'; ?>
					<?php endif; ?>
				</td>
			</tr>
            <?php if(!empty($childDiffCardExist)): ?>
			<tr>
                <td class="require-input-label"><label><?php echo __('Vidinis neatitikties tipas') ?>:</label></td>
				<td colspan="2" class="require-input">
					<?php if ((!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) && $canEdit): ?>
						<?php echo $this->Form->input('diff_sub_type_id', array('div' => null, 'label' => null, 'type' => 'hidden', 'data-ng-value' => 'diffSubOption.DiffType.id')); ?>
						<select data-ng-hide="!diffOption" data-ng-model="diffSubOption" data-ng-options="diffSubOpt.DiffType.label for diffSubOpt in diffOption.DiffType.subs" class="form-control"></select>
						<span data-ng-hide="diffOption">&mdash;</span>
					<?php else: ?>
						<?php echo ($item && ($v = htmlspecialchars(DiffType::buildName($item, false, DiffCard::PFX_SUB_TYPE)))) ? $v : '&mdash;'; ?>
					<?php endif; ?>
				</td>
			</tr>
            <?php endif; ?>
			<tr>
				<td class="require-input-label">
					<?php if (!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW): ?>
					<label><?php echo __('Atsakingas asmuo'); ?></label>
					<?php else: ?>
					<label><?php echo __('Peradresavimas'); ?></label>
					<?php endif; ?>
				</td>
				<td colspan="2" class="require-input">
					<?php if ((!$item || $item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) && $canEdit): ?>
						<?php if (!$item || !$item[DiffCard::NAME]['responsive_section_user_id']): ?>
						<?php echo $this->Form->input('responsive_section_user_id', array('div' => null, 'label' => null, 'type' => 'hidden', 'data-ng-value' => 'responsiveUser.User.id')); ?>
						<div><select data-ng-model="responsiveUser" data-ng-options="responsiveUsr.User.label for responsiveUsr in responsiveUsers" class="form-control"><option value="">-- <?php echo __('Neparinktas'); ?> --</option></select></div>
                        <?php endif; ?>
						{{responsiveUser.User.section}}<br />
						<?php echo $date->format('Y-m-d'); ?>, {{responsiveUser.User.label}}
					<?php elseif (!$item): ?>
						&mdash;
					<?php else: ?>
						<?php //echo htmlspecialchars($item[User::NAME.DiffCard::PFX_RESPONSIVE_SECTION]['section']); ?>
						<?php //echo $date->format('Y-m-d'); ?>
                        <?php echo htmlspecialchars(User::buildName($item, false, DiffCard::PFX_RESPONSIVE_SECTION)); ?>
                        <?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit): ?>
                            <div>
                                <select name="otherResponsive" class="btn btn-default">
                                    <?php foreach($responsiveUsers as $u): ?>
                                        <option value="<?php echo $u['User']['id'];?>"><?php echo $u['User']['username'];?></option>
                                    <?php endforeach;?>
                                </select>
                            </div>
                            <input class="btn btn-default" type="submit" name="redirectToOtherResponsive" value="<?php echo __('Peradresuoti'); ?>" />
                        <?php endif;?>
					<?php endif; ?>
				</td>
			</tr>
			<tr>
			    <td class="require-input-label" style="vertical-align: bottom;"><label><?php echo empty($this->request->data)?__('Prisegti failą'):__('Prisegtas failas'); ?></label></td>
			    <td colspan="2"><?php //if(!$item){
			             echo $this->Form->input('',array('name'=>'data[DiffCard][attachment][]','type'=>'file', 'label'=>false, 'multiple'=>'multiple'));
			             if(!empty($item) && trim($item['DiffCard']['attachment'])){
                             foreach(array_filter(explode(',', $item['DiffCard']['attachment'])) as $attachment){
                                echo $this->Html->link('<i class="iconfa-file"> '.__('Prisegtas failas').' '.$attachment.'</i>', $this->Html->url('/files'.DS.'diff_cards_attachments'.DS.$attachment), array('escape'=>false,'target'=>'_blank')).'<br />';
                             }
                         }
			        ?>
			    </td>
			</tr>
			<?php if ($item && in_array($item[DiffCard::NAME]['state'], array(DiffCard::STATE_ACCEPTED, DiffCard::STATE_EXPLAINED, DiffCard::STATE_EXPLANATION_DENIED, DiffCard::STATE_COMPLETE))): ?>
                <tr>
                    <td class="require-input-label"><label><?php echo __('Paaiškinimas'); ?></label></td>
                    <td colspan="2" class="require-input">
                        <?php fb($canEdit?'t':'n');
                        if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit): ?>
                            <?php echo $this->Form->input('clarification', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
                        <?php else: ?>
                            <?php echo $this->App->multilineText($item[DiffCard::NAME]['clarification']); ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="require-input-label"><label><?php echo __('Kokie veiksmai atlikti nedelsiant?'); ?></label></td>
                    <td colspan="2" class="require-input">
                        <?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit): ?>
                            <?php echo $this->Form->input('measures_taken', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
                        <?php else: ?>
                            <?php echo $this->App->multilineText($item[DiffCard::NAME]['measures_taken']); ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="require-input-label"><label><?php echo __('Kokie daromi koregavimo veiksmai, kad to išvengti ateityje?'); ?></label></td>
                    <td colspan="2" class="require-input">
                        <?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit): ?>
                            <?php echo $this->Form->input('measures_for_future', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
                        <?php else: ?>
                            <?php echo $this->App->multilineText($item[DiffCard::NAME]['measures_for_future']); ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <tr>
                    <td class="require-input-label"><label><?php echo __('Koregavimo veiksmų atlikimo terminas'); ?></label></td>
                    <td colspan="2" class="require-input">
                        <?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit): ?>
                            <?php echo $this->Form->input('deadline', array('label' => false, 'type' => 'text', 'class' => 'form-control', 'data-date-picker' => '', 'placeholder' => 'yyyy-mm-dd', 'div' => null)); ?>
                        <?php else: ?>
                            <?php $ddt = new DateTime($item[DiffCard::NAME]['deadline']); echo $item[DiffCard::NAME]['deadline'] ? $ddt->format('Y-m-d') : '&mdash;'; ?>
                        <?php endif; ?>
                    </td>
                </tr>
                <!--tr>
                    <td colspan="2" class="require-input-label" style="vertical-align: bottom;">
                        <?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED): ?>
                        <label><?php echo __('Kokybės skyrius'); ?></label>
                        <?php else: ?>
                        <label>Koregavimo ir prevencinių  veiksmų efektyvumo įvertinimas</label>
                        <?php endif; ?>
                    </td>
                    <td>
                    </td>
                </tr-->
			<?php endif; ?>
			<?php if ($item && in_array($item[DiffCard::NAME]['state'], array(DiffCard::STATE_EXPLAINED, DiffCard::STATE_EXPLANATION_DENIED, DiffCard::STATE_COMPLETE))): ?>
			<tr>
                <td class="require-input-label"><label><?php echo __('Koregavimo ir prevencinių veiksmų efektyvumo įvertinimas'); ?></label></td>
				<td colspan="2" class="require-input">
					<?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED && $canEdit): ?>
						<?php echo $this->Form->input('measure_evaluation', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
					<?php else: ?>
						<?php echo $this->App->multilineText($item[DiffCard::NAME]['measure_evaluation']); ?>
					<?php endif; ?>
				</td>
			</tr>
			<?php endif; ?>
			<?php if ($item && in_array($item[DiffCard::NAME]['state'], array(DiffCard::STATE_EXPLANATION_DENIED, DiffCard::STATE_COMPLETE))): ?>
			<tr>
				<td colspan="3" class="require-input">
					<?php if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLANATION_DENIED && $canEdit): ?>
						<?php echo $this->Form->input('production_manager_comment', array('label' => false, 'type' => 'textarea', 'rows' => 2, 'class' => 'form-control ta-desc-vert', 'div' => null)); ?>
					<?php else: ?>
						<?php echo $this->App->multilineText($item[DiffCard::NAME]['production_manager_comment']); ?>
					<?php endif; ?>
				</td>
			</tr>
			<?php endif; ?>
		</tbody>
	</table>
        <div>
            <br />
            <?php  if ((!$item || $item[DiffCard::NAME]['state'] != DiffCard::STATE_COMPLETE) && $canEdit): ?>
                <button type="submit" class="btn btn-success"><?php echo __('Patvirtinti'); ?></button>
            <?php endif; ?>
            <?php if ($item && $item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED && $canEdit): ?>
                <input type="submit" class="btn btn-danger" name="explanation_denied" value="<?php echo htmlspecialchars(__('Nepatvirtinti')); ?>" />
            <?php endif; ?>
            <a href="<?php echo $listUrl; ?>" class="btn btn-link" onclick="window.close();"><?php echo __('Atšaukti'); ?></a>
        </div>
        <?php echo $this->Form->end(); ?>
    </div>
    </div>
    <div class="col-lg-2"></div>
</div>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/angular.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-timepicker.min.js"></script>
<script type="text/javascript">
	window.DiffCard = {
		baseUrl: '<?php echo $baseUri; ?>',
		diffTypes: <?php echo json_encode($diffTypes); ?>,
		diffTypeId: <?php echo ($item && $item[DiffCard::NAME]['diff_type_id']) ? $item[DiffCard::NAME]['diff_type_id'] : (($item && $this->request->data(DiffCard::NAME.'.diff_type_id')) ? $this->request->data(DiffCard::NAME.'.diff_type_id') : 'null'); ?>,
		diffSubTypeId: <?php echo ($item && $item[DiffCard::NAME]['diff_sub_type_id']) ? $item[DiffCard::NAME]['diff_sub_type_id'] : (($item && $this->request->data(DiffCard::NAME.'.diff_sub_type_id')) ? $this->request->data(DiffCard::NAME.'.diff_sub_type_id') : 'null'); ?>,
		responsiveUsers: <?php echo json_encode( $responsiveUsers); ?>,
		responsiveUserId: <?php echo ($item && $item[DiffCard::NAME]['responsive_section_user_id']) ? $item[DiffCard::NAME]['responsive_section_user_id'] : (($item && $this->request->data(DiffCard::NAME.'.responsive_section_user_id')) ? $this->request->data(DiffCard::NAME.'.responsive_section_user_id') : 'null'); ?>,
		datePickerConfig: <?php echo json_encode($this->App->datePickerConfig()); ?>,
		timePickerConfig: <?php echo json_encode($this->App->timePickerConfig()); ?>
	};
    var translations = {choose: '<?php echo __('Pasirinkite') ?>'};
    jQuery(document).ready(function($){
        <?php if(preg_match('/.*work-center\/*.*/i',$_SERVER['HTTP_REFERER']??'')){ ?>
        $('#DiffCardEditForm').bind('submit', function(e){
            e.preventDefault();
            $.ajax({
                url: '<?php echo Router::url($this->here, true); ?>',
                type: 'post',
                data: $(this).serialize(),
                success: function(data){
                    try{//jei grazina json, vadinasi tai klaidu pranesimai, lango neuzdarome, leidziame siusti uzklausa
                        let jsonData = $.parseJSON(data);
                        $('#saveMessage').html('<div class="alert alert-error"><ul><li>'+jsonData.join('</li><li>')+'</li></ul></ul>');
                        $('#saveMessage').get(0).scrollIntoView();
                    }catch (e) {//jei grizta html, vadinasi uzkalusa nuejo, langa uzdarome
                        window.close();
                    }
                }
            });
        });
        <?php } ?>
    });
</script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/DiffCard.js"></script>