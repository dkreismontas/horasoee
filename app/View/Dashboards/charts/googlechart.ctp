<div id="googlechartContainer<?php echo $ident; ?>" style="height: 100%;"></div>
<div id="googlechartBase64<?php echo $ident; ?>" style="display:none;"></div>
<script type="text/javascript">
google.charts.load('current', {packages: ['corechart']});
google.charts.setOnLoadCallback(get_data_<?php echo $ident; ?>);

function get_data_<?php echo $ident; ?>(returnImage=false){
    <?php echo current(json_decode($graphData,true)); ?>;
}

function startExport<?php echo $ident; ?> (imgBase64) {
    $('form#<?php echo $ident; ?>').find('input.base64_img_src').val(imgBase64);
    $('form#<?php echo $ident; ?>').submit(); //siunciame ne per ajax nes narsykle kitaip nepaduoda save popup lango
    $('form#<?php echo $ident; ?>').find('input.base64_img_src').val('');
}

</script>