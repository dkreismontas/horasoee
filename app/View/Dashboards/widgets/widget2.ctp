<?php
$uid = uniqid('uid');
?>
<div  class="span4 widget_block" id="b<?php echo $uid ?>">
	<?php
	$this->request->data = isset($widget_data['data'])?unserialize($widget_data['data']):array();
	echo $this->Form->create('Node', array(
			'url' => array('controller'=>'dashboards','action'=>'chart', $type),
			'id' => $uid,
			'autocomplete'=>'off'
		));
    $explane = array(
        __('Jei pageidaujate matyti kitokio tipo informaciją šiame grafike, spustelkite "Keisti" mygtuką, pakeiskite norimas reikšmes ir vėl išsaugokite.')
    );
		?>
	<h4 class="widgettitle"><?php echo __('Priklausomybės grafikas'); ?>
	    <span class="actionButtons">
	       <?php
                echo $this->Html->link('',array(),array('class'=>'exel-grayscale export','export_type'=>2,'title'=>__('Grafiko bei galutinių duomenų eksportavimas į excel failą')));
            ?>
	       <div class="explane"><ul><li><?php echo implode('</li><li>',$explane); ?></li></ul></div>
	       <i title="<?php echo __('Padidinti grafiką'); ?>" class="iconsweets-fullscreen" rel="<?php echo $uid ?>"></i>
	       <?php if(isset($widget_data)){ ?>
           <i title="<?php echo __('Uždaryti grafiką'); ?>" class="closeWidget" widget_id="<?php echo $widget_data['id']; ?>"></i>
           <?php } ?>
	    </span>
	</h4>
	<div class="widgetcontent">
		<!-- PAIESKOS ATVAIZDAVIMAS -->
		<div class="filtras" style="<?php echo (@$widget_data)?'display: none':''; ?>">
		<div  class="span6">




		<?php
		if(isset($widget_data)){
			$widget_settings = unserialize(@$widget_data['data']);
			$widget_settings = $widget_settings['Node'];
		//	fb($widget_settings,'$widget_settings');
		}
		echo $this->Form->input('id',array('value'=>''.@$widget_data['id'],'type'=>'hidden','id'=>false,'class'=>'widget_id'));
		echo $this->Form->input('type',array('value'=>$type,'type'=>'hidden','id'=>false,'class'=>'type'));

		echo $this->Form->input('node', array(
			'label' => __('Padaliniai'),
			'empty' => __('-Pasirinkite-'),
			'options' => @$node,
			'value' => @$widget_settings['node'],
			'class' => 'field_node',
			'multiple' => 'multiple'
		));
		echo $this->Form->input('indicator1', array(
			'label' => __('Rodiklis y1'),
			'empty' => __('-Pasirinkite rodiklį y1-'),
			'options' => Configure::read('INDICATORS'),
			'class' => ''
		));
		/*echo $this->Form->input('indicator2', array(
			'label' => __('Rodiklis y2'),
			'empty' => __('-Pasirinkite rodiklį y2-'),
			'options' => Configure::read('INDICATORS'),
			'class' => ''
		));*/
		echo $this->Form->input('angle', array(
			'label' => __('Rakursas'),
			'empty' => __('-Pasirinkite rakursą-'),
			'options' => array(0=>__('Procesai'),1=>__('Darbuotojai'),2=>__('Atributai'), 3=>__('Padaliniai')),
			//'value' => '',
			'class' => ''
		));
		echo $this->Form->input('group', array(
			'label' => __('Grupės'),
			'options' => @$groups,
			//'empty' => __('-Pasirinkite-'),
			'multiple' => 'multiple',
			'type' => 'select',
			'disabled' => 'disabled',
			'class' => 'field_group',
			'id'=>'group'.$uid
		));
		echo $this->Form->input('process', array(
			'label' => __('Procesai'),
			'options' => @$processes,
			//'empty' => __('-Pasirinkite-')
			'multiple' => 'multiple',
			'type' => 'select',
			'disabled' => 'disabled',
			'class' => 'field_process',
			'id'=>'process'.$uid
		));

		echo '<span style="display:none">'.$this->Form->checkbox('Show in groups',
			array(
				'value'=>'combined_',
				'class'=>'show_in_groups',
				'div'=>false,
				'id'=>'',
				'hiddenField' => false,
				'checked'=>'checked'
				)
		). __('Rodyti grupėse').' </span>';
		echo $this->Form->input('date_start', array(
			//'type' => 'date',
			'label' => __('Nuo'),
			//'value' => (isset($widget_settings['date_start']))?$widget_settings['date_start']:date('Y-m-d'),
			'class' => 'date_selector field_start',
			'id'=> $uid.'field_start'
		));
		echo $this->Form->input('date_end', array(
			//'type' => 'date',
			'label' => __('Iki'),
			//'value' => (isset($widget_settings['date_end']))?$widget_settings['date_end']:date('Y-m-d'),
			'class' => 'date_selector field_end',
			'id'=> $uid.'field_end'
		));
        echo $this->Form->input('time_group', array(
            'label' => __('Laiko grupavimas'),
            'empty' => __('-Pasirinkite laiko grupavimą-'),
            'options' => array(__('Diena'),__('Savaitė'),__('Mėnuo'),__('Metai')),
            'class' => ''
        ));
        echo $this->Form->input('export_type', array('type' => 'hidden','value'=>0,'class'=>'export_type'));
        echo $this->Form->input('ident', array('type' => 'hidden','value'=>isset($widget_settings['ident'])?$widget_settings['ident']:$ident,'class'=>'keyIdent'));
        echo $this->Form->input('file_name', array('type' => 'hidden','value'=>'','class'=>'file_name'));
				?>

			</div>
				<div  class="span6">
				<h4 class="widgettitle"><?php echo __('Darbuotojai'); ?></h4>
					<div class="widgetcontent">
						<div class="extraData"></div>
					</div>


			<h4 class="widgettitle"><?php echo __('Atributai'); ?></h4>
			<div class="widgetcontent">
				<div class="extraData2"></div>
			</div>

				</div>
				<div>
				<div class="span12">
					<?php
					echo $this->Form->button(__('Išsaugoti'), array(
						 'type' => 'button',
						 'class' => 'saveData'
					 )); ?>
				 </div></div>
				<div class="clr"></div>
			</div>
			<!-- GRAFIKO ATVAIZDAVIMAS -->
			<div class="grafikas" style="<?php echo (@$widget_data)?'':'display: none'; ?>">
				<div  class="span12">
					<div class="chart_holder" style="height: 300px;"><div class="widgetOpener" id="graph_<?php echo $uid ?>">
						<?php
						if(@$widget_data['data']){
							echo $this->requestAction('/dashboards/chart/'.@$widget_data['type'].'/',array('widget'=>@$widget_data['data'],'key'=>@$key));
						}
						?>
					</div></div>
					<?php
					echo $this->Form->button(__('Keisti'), array(
						 'type' => 'button',
						 'class' => 'changeData'
					 )); ?>
				</div>
				<!-- EOF GRAFIKO ATVAIZDAVIMAS -->
				<div class="clr"></div>
			</div>

		</div>
		<?php echo $this->Form->end() ?>

</div>
<script type="text/javascript" charset="utf-8">
    (function($, window) {
        $.fn.fillData = function(options) {
            var self, $option;
            self = this;
            $.each(options, function(index, option) {
                $option = $("<option></option>").attr("value", index).text(option);
                self.append($option);
                self.removeAttr('disabled');
            });
        };
    })(jQuery, window);

	jQuery(document).ready(function($) {

		$('.iconsweets-fullscreen').bind('click',function(){
            $('#graph_'+$(this).attr('rel')).extractor({
                autoOpen: false,
                show: {
                    effect: "blind",
                    duration: 200
                },
                hide: {
                    effect: "fade",
                    duration: 200
                },
                buttons: {
                    "<?php echo __('Uždaryti langą') ?>": function() {
                      $( this ).dialog( "close" );
                    }
                },
                width: 1200,
                height: 800,
                title: '<?php echo __('Grafikas'); ?>',
                close: function(){}
            });
            $('#graph_'+$(this).attr('rel')).dialog( "open" );
        });

		var pref_id = '#<?php echo $uid ?> ';
		var type =  $(pref_id+'.type').val();

		 $(pref_id+".saveData").click(function() {
		 	var mdata = $(pref_id).serialize();
		 	var node_id_nr = $('#nodeId').val();
		 	var temp_val = $(pref_id+'.saveData').text();
		 	$(pref_id+'.saveData').text('Saiving...');
		 	$.post('/dashboards/saveWidget/'+node_id_nr,mdata,function(data){
		 		$(pref_id+'.widget_id').val(data);
		 		$(pref_id+'.saveData').text(temp_val);
		 			$.post('/dashboards/chart/'+type,mdata,function(data){
		 					$(pref_id+'.chart_holder > div').html(data);
		 			});

		 		$(pref_id+'.filtras').slideUp();
		 		$(pref_id+'.grafikas').slideDown();
		 	});

        });
         $(pref_id+".changeData").click(function() {
         	$(pref_id+'.filtras').slideDown();
		 	$(pref_id+'.grafikas').slideUp();
         });


		 $(pref_id+".field_node").change(function() {
           fillData<?php echo $uid ?> (pref_id);
        });
		 $( pref_id+".show_in_groups").change(function() {
           fillData<?php echo $uid ?> (pref_id);
        });

		  $(pref_id+".field_group, "+pref_id+".field_process").change(function() {
            getWorkers<?php echo $uid ?>(pref_id);
            getAttributes<?php echo $uid ?>(pref_id);
        });
		fillData<?php echo $uid ?> (pref_id);


            $(pref_id+'.date_selector').datetimepicker({
				dateFormat : 'yy-mm-dd',
                firstDay : 1,
                closeText: 'Uždaryti',
                currentText: 'Dabar',
                timeText: 'Laikas',
                hourText: 'Valandos',
                minuteText: 'Minutės',
                alwaysSetTime: false
			});
		function fillData<?php echo $uid ?> (pref_id) {
			var grouped = '';
            if($(pref_id+'.show_in_groups').prop('checked')){
            	grouped = $(pref_id+'.show_in_groups').val();
            }

            var node_id = $(pref_id+".field_node").val();
            $.post("/visual_plans/ajax_get_groups/" + node_id, function(data) {
                $(pref_id+".field_group").replaceOptions(data);
            }, "json");
            $.post("/visual_plans/ajax_get_"+grouped+"processes/"  + node_id, function(data) {
                $(pref_id+".field_process").replaceOptions(data);
            }, "json");
            $.post("/visual_plans/ajax_get_shifts/" + node_id, function(data) {
                $(pref_id+".field_shift").replaceOptions(data);
            }, "json");
            //  console.log($("#NodeIndexForm").serialize());
          getWorkers<?php echo $uid ?>(pref_id);
          getAttributes<?php echo $uid ?>(pref_id);
        }
         function getWorkers<?php echo $uid ?> (pref_id) {
        	var node_id = $(pref_id+".field_node").val();
            var tech_process_id = $(pref_id+".field_process").val();
            var shift_id = null;
            var group_id = $(pref_id+".field_group").val();
        	  $.post("/visual_plans/ajax_get_workers/" + node_id+'/'+tech_process_id+'/'+shift_id+'/'+group_id, function(data) {
                $(pref_id+".extraData").setCheckboxes(data,'workers');
            },'json');

        }
         function getAttributes<?php echo $uid ?> (pref_id) {
         	var node_id = $(pref_id+".field_node").val();
            var tech_process_id = $(pref_id+".field_process").val();
            var shift_id = null;
            var group_id = $(pref_id+".field_group").val();
        	  $.post("/visual_plans/ajax_get_attributes/" + node_id+'/'+tech_process_id+'/'+shift_id+'/'+group_id, function(data) {
             // console.log(data);
                $(pref_id+".extraData2").setCheckboxes(data,'atributes');
            },'json');
         }
         function fillStartData<?php echo $uid ?>(pref_id) {

         	var workers = <?php echo @json_encode( $this->request->data['Node']['workers']) ?>;
         	if(workers){
         	$.each(workers, function(index, val) {
         		  $(pref_id+" .extraData input[type=checkbox][value="+val+"]").attr('checked', true);
			   console.log(index+ ' '+ val);
			 });
			 }
         var atributes = <?php echo @json_encode( $this->request->data['Node']['atributes']) ?>;
         if(atributes){
         	$.each(atributes, function(index, val) {
         		  $(pref_id+".extraData2 input[type=checkbox][value="+val+"]").attr('checked', true);
			   console.log(index+ ' '+ val);
			 });
          }


       var process = <?php echo @json_encode( $this->request->data['Node']['process']) ?>;

         if(process){
         	$.each(process, function(index, val) {
         		  $(pref_id+".field_process option[value="+val+"]").attr('selected', true);
			   console.log(index+ ' '+ val);
			 });
          }

       var group = <?php echo @json_encode( $this->request->data['Node']['group']) ?>;

         if(group){
         	$.each(group, function(index, val) {
         		  $(pref_id+".field_group option[value="+val+"]").attr('selected', true);
			   console.log(index+ ' '+ val);
			 });
          }

         }
         setTimeout( function(){fillStartData<?php echo $uid ?>(pref_id);}, 3000 );

	});

</script>