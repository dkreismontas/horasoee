<?php
if(isset($widget_data)){
    $widget_settings = unserialize(@$widget_data['data']);
    $widget_settings = $widget_settings['Node'];
}
$uid = isset($widget_settings['uid'])?$widget_settings['uid']:uniqid('uid');
?>
<div  class="col-md-6 col-xl-4 widget_block" id="b<?php echo $uid ?>" style="min-width: 300px;" data-widget_id="<?php echo isset($widget_data['id'])?$widget_data['id']:0; ?>">
    <?php
    $this->request->data = isset($widget_data['data'])?unserialize($widget_data['data']):array();
    echo $this->Form->create('Node', array(
            'url' => array('controller'=>'dashboards','action'=>'chart', $type),
            'id' => $uid,
            'class'=>'form-horizontal',
            'role'=>'form',
            'autocomplete'=>'off'
        ));
    echo $this->Form->input('uid', array('type'=>'hidden', 'value'=>$uid));
        ?>
    <div rel="<?php echo $uid ?>" class="widgettitle" style="padding-right: 0;">
        <span class="titleText"><?php echo isset($widget_data)?$indicators[unserialize($widget_data['data'])['Node']['indicator1']]:__('Kiekybinis grafikas'); ?></span>
        <span class="actionButtons popoversample" style="background: #ffffff; color: #000000;  padding: 10px; float: right; top: -10px; position: relative; margin-right: 2px;">
            <i data-original-title="" href="#" class="btn iconsweets-alert" rel="popover" data-placement="top" data-content="<?php echo __('Jei pageidaujate matyti kitokio tipo informaciją šiame grafike, spustelkite &quot;Keisti&quot; mygtuką, pakeiskite norimas reikšmes ir vėl išsaugokite.'); ?>" rel="popover"></i>
            <?php
                echo $this->Html->link('<i class="btn iconsweets-excel2"></i>',array(),array('class'=>'exel-grayscale export','export_type'=>2,'title'=>__('Grafiko bei galutinių duomenų eksportavimas į excel failą'),'escape'=>false));
            ?>
           <i title="<?php echo __('Spausdinti'); ?>" href="#" class="btn iconsweets-printer print" rel="popover" data-placement="top" rel="popover"></i>
           <i title="<?php echo __('Padidinti grafiką'); ?>" class="btn iconsweets-fullscreen" rel="<?php echo $uid ?>"></i>
           <?php if(isset($widget_data) && !trim($subscription)){$hideIcon = false;}else{$hideIcon = true;} ?>
           <i <?php if($hideIcon){echo 'style="display:none;"';} ?> title="<?php echo __('Perduoti kitam vartotojui'); ?>" class="btn iconsweets-shuffle shareWidget" widget_id="<?php echo isset($widget_data['id'])?$widget_data['id']:''; ?>"></i>
           <i <?php if($hideIcon){echo 'style="display:none;"';} ?> title="<?php echo __('Pašalinti grafiką'); ?>" class="btn iconsweets-trashcan closeWidget" widget_id="<?php echo isset($widget_data['id'])?$widget_data['id']:''; ?>"></i>
        </span>
    </div>
    <div class="widgetcontent" style="position: relative;">
        <!-- PAIESKOS ATVAIZDAVIMAS -->
        <div class="filtras col-xs-12" style="<?php echo isset($widget_data)?'display: none':''; ?>">
        <?php
        echo $this->Form->input('id',array('value'=>isset($widget_data['id'])?$widget_data['id']:'','type'=>'hidden','id'=>false,'class'=>'widget_id'));
        echo $this->Form->input('type',array('value'=>$type,'type'=>'hidden','id'=>false,'class'=>'type'));
        echo $this->Form->input('user_id',array('value'=>isset($widget_data['user_id'])?$widget_data['user_id']:$user->id,'type'=>'hidden','id'=>false,'class'=>'type'));
        echo $this->Form->input('graph_name', array(
            'label' => false,
            'type'=>'text',
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Grafiko pavadinimas').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group'
        ));
        asort($indicators);
        echo $this->Form->input('indicator1', array(
            'label' => false,
            'empty' => __('-Pasirinkite rodiklį y1-'),
            'options' => $indicators,
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Grafiko tipas').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group'
        ));
        /*echo $this->Form->input('indicator2', array(
            'label' => false,
            'empty' => __('-Pasirinkite rodiklį y2-'),
            'options' => $indicators,
            'class' => 'input-large',
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Rodiklis y2').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group'
        ));*/
        echo $this->Form->input('add_downtime_exclusions', array(
            'legend' => false,
            'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip')),
            'class' => '',
            'default'=>0,
            'before'=>'<label class="col-sm-5">'.__('Ar įtraukti planines prastovas?').'</label><div class="col-xs-7">',
            'after'=>'</div>',
            'div'=>'form-group hidden add_downtime_exclusions',
            //'separator' => '<br />',
            'type' => 'radio'
        ));
        echo $this->Form->input('add_disabled_shifts', array(
            'legend' => false,
            'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip')),
            'class' => '',
            'default'=>0,
            'before'=>'<label class="col-sm-5">'.__('Ar įtraukti išjungtas pamainas?').'</label><div class="col-xs-7">',
            'after'=>'</div>',
            'div'=>'form-group hidden add_disabled_shifts',
            //'separator' => '<br />',
            'type' => 'radio'
        ));
        echo $this->Form->input('downtime_level', array(
            'label' => false,
            'empty' => __('-Pasirinkite prastovos lygį-'),
            'options' => $downtimeLevelsList,
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Prastovų lygmuo').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group hidden downtime_level'
        ));
        echo $this->Form->input('downtimes', array(
            'label' => false,
            'options' => array(),
            'multiple' => 'multiple', 'size'=>6,
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Prastovos').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group hidden downtimes'
        ));
        echo $this->Form->input('graph_type', array(
            'label' => false,
            'empty' => __('-Pasirinkite tipą-'),
            'options' => array(0=>__('Linijinis'),1=>__('Stulpelinis'),2=>__('Skritulinis')),
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Grafiko tipas').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group'
        ));
        echo $this->Form->input('sensors', array(
            'label' => false,
            'options' => $sensorsList,
            //'value' => @$widget_settings['node'],
            'data-placeholder'=>__('Pasirinkite darbo centrą'),
            'class' => 'form-control chosen-select', 'before'=>'<label class="col-sm-3">'.__('Darbo centrai').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group',
            'multiple' => 'multiple', 'size'=>sizeof($sensorsList) >= 12?12:sizeof($sensorsList)
        ));
        echo $this->Form->input('shifts', array(
            'label' => false,
            'options' => array(-1=>__('Visos pamainos')),
            //'value' => @$widget_settings['node'],
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Pamainos').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group',
            'multiple' => 'multiple'
        ));
        echo $this->Form->input('date_start_end', array(
            //'type' => 'date',
            'label' => false, 'id' => 'datestart'.$uid,
            'value' => isset($widget_settings['date_start_end'])?$widget_settings['date_start_end']:date('Y-m-d'),
            //'value' => 'Pamaina ~ Pamaina',
            'class' => 'form-control daterange_selector',
            'before'=>'<label class="col-sm-3">'.__('Nuo - iki').'</label><div class="col-xs-7">',
            'after'=>'</div><div id="timePatterns" class="dropdown col-xs-1" style="padding-left: 0;">
            <span title="'.__('Laiko šablonai').'" class="btn glyphicon glyphicon-calendar" id="menu1" data-toggle="dropdown"></span>
            <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="0">'.__('Praėjusi pamaina').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="1">'.__('Praėjusi para').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="2">'.__('Praėjusi savaitė').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="10">'.__('Praėjusi savaitė iki dabar').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="11">'.__('Nuo ... iki dabar').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="3">'.__('Praėjęs mėnuo').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="9">'.__('Praėjęs mėnuo iki dabar').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="4">'.__('Praėję metai').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="5">'.__('Šiandien').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="6">'.__('Ši savaitė').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="7">'.__('Šis mėnuo').'</a></li>
              <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="8">'.__('Šie metai').'</a></li>
            </ul>
          </div>',
            'div'=>'form-group field_start_end',
        ));
        echo $this->Form->input('date_from', array(
            'label' => false,
            'id' => 'dateend'.$uid,
            'class' => 'form-control singledate_selector', 'before'=>'<label class="col-sm-3">'.__('Nuo...').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group field_from hidden',
        ));
       echo $this->Form->input('time_pattern',array('type'=>'hidden','id'=>'time_pattern'));
       echo $this->Form->input('time_group', array(
            'label' => false,
            'options' => array(0=>__('Suminis'),1=>__('Pamaina'),2=>__('Para'),3=>__('Savaitė'),4=>__('Mėnuo')),
            //'value' => @$widget_settings['node'],
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Laiko grupavimas').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group',
            'type' => 'select'
        ));
       echo '<div style="display: none;">'.$this->Form->input('action', array(
            'label' => false,
            'options' => array(
                 1=>__('Sumavimas'),
                 //2=>__('Vidurkinimas')
            ),
            //'value' => @$widget_settings['node'],
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Veiksmas').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group',
            'type' => 'select'
        )).'</div>';
        echo $this->Form->input('decimal_count', array(
            'label' => false,
            'type'=>'number',
            'min'=>0,
            'max'=>6,
            'class' => 'form-control', 'before'=>'<label class="col-sm-3">'.__('Skaitmenų po kablelio kiekis').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group'
        ));
        /*echo $this->Form->input('summarize', array(
            'legend' => false,
            'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip')),
            //'value' => @$widget_settings['node'],
            'class' => '', 'before'=>'<label class="col-sm-3">'.__('Apibendrintas stulpelis').'</label><div class="col-xs-7">','after'=>'</div>','div'=>'form-group',
            'type' => 'radio'
        ));*/
        ?><div class="row">
            <div class="col-xs-6" style="border-right: 1px solid #ccc;"><?php
                echo $this->Form->input('order_columns_desc', array(
                    'legend' => false,
                    'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip (Paretas)')),
                    'class' => '',
                    'before'=>'<label class="col-sm-6">'.__('Surikiuoti mažėjimo tvarka?').'</label><div class="col-xs-6">',
                    'after'=>'</div>',
                    'div'=>'form-group row',
                    'separator' => '<br />',
                    'type' => 'radio'
                ));
            ?></div>
            <div class="col-xs-6"><?php

            ?></div>
        </div>
        <div class="row">
            <div class="col-xs-6" style="border-right: 1px solid #ccc;"><?php
                echo $this->Form->input('combine_all_sensors', array(
                    'legend' => false,
                    'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip')),
                    'class' => '',
                    'before'=>'<label class="col-sm-6">'.__('Išvesti bendrą?').'</label><div class="col-xs-6">',
                    'after'=>'</div>',
                    'div'=>'form-group row',
                    'type' => 'radio'
                ));
            ?></div>
            <div class="col-xs-6"><?php
                echo $this->Form->input('trendline', array(
                    'legend' => false,
                    'options' => array(0=>__('Ne').'&nbsp;&nbsp;&nbsp;',1=>__('Taip')),
                    'class' => '',
                    'before'=>'<label class="col-sm-6">'.__('Piešti trend liniją?').'</label><div class="col-xs-6">',
                    'after'=>'</div>',
                    'div'=>'form-group row',
                    'type' => 'radio'
                ));
            ?></div>
        </div><?php
        echo $this->Form->input('export_type', array('type' => 'hidden','value'=>0,'class'=>'export_type'));
        echo $this->Form->input('base64_img_src', array('type' => 'hidden','value'=>0,'class'=>'base64_img_src'));
        echo $this->Form->input('ident', array('type' => 'hidden','value'=>isset($widget_settings['ident'])?$widget_settings['ident']:$ident,'class'=>'keyIdent'));
        echo $this->Form->input('file_name', array('type' => 'hidden','value'=>'','class'=>'file_name'));
?>

        <div>
        <div class="span12">
            <?php
            echo $this->Form->button(__('Išsaugoti'), array(
                 'type' => 'button',
                 'class' => 'btn btn-primary saveData'
             )); ?>
         </div>
        </div>
        <div class="clr"></div>
     </div>
    <!-- GRAFIKO ATVAIZDAVIMAS -->
    <div  class="grafikas" style="<?php echo isset($widget_data)?'':'display: none'; ?>">
        <div  class="span12">
            <div class="chart_holder" style="height: 300px;">
                <?php echo $this->Html->image('/images/loaders/loader24.gif',array('class'=>'graphLoader','style'=>array('display: none;'))); ?>
                <div style="height: 100%;" class="widgetOpener" id="graph_<?php echo $uid ?>">
                <?php
                if(isset($widget_data['data'])){
                    echo $this->requestAction('/dashboards/chart/'.$widget_data['type'].'/subscription:'.$subscription,array('widget'=>$widget_data['data'],'key'=>isset($key)?$key:'','graph_id'=>$widget_data['id'], 'uid'=>$uid));
                }
                ?>
            </div></div>
            <?php
            if(!trim($subscription)){
            echo $this->Form->button(__('Keisti'), array(
                 'type' => 'button',
                 'class' => 'btn btn-default changeData'
             ));} ?>
        </div>
        <!-- EOF GRAFIKO ATVAIZDAVIMAS -->
        <div class="clr"></div>
    </div>

        </div>
        <?php echo $this->Form->end() ?>

</div>
<script type="text/javascript" charset="utf-8">
    (function($, window) {
        $.fn.fillData = function(options) {
            var self, $option;
            self = this;
            $.each(options, function(index, option) {
                $option = $("<option></option>").attr("value", index).text(option);
                self.append($option);
                self.removeAttr('disabled');
            });
        };
    })(jQuery, window);
    jQuery(document).ready(function($) {
        $('.export_type').val(0);
        var pref_id = '#<?php echo $uid ?> ';
        $(pref_id+'.iconsweets-fullscreen').live('click',function(){
            $('#graph_'+$(this).attr('rel')).extractor({
                autoOpen: false,
                show: {
                    effect: "blind",
                    duration: 200
                },
                hide: {
                    effect: "fade",
                    duration: 200
                },
                buttons: {
                    "<?php echo __('Uždaryti langą') ?>": function() {
                      $( this ).dialog( "close" );
                    }
                },
                width: 1200,
                height: 800,
                title: '<?php echo __('Grafikas'); ?>',
                close: function(evt, ui){
                    google.charts.setOnLoadCallback(get_data_<?php echo $uid; ?>);
                }
            });
            $('#graph_'+$(this).attr('rel')).dialog( "open" );
            google.charts.setOnLoadCallback(get_data_<?php echo $uid; ?>);
        });

        $(pref_id+'.chosen-select').chosen({
            no_results_text: '<?php echo __('Nepavyko rasti'); ?>:',
            disable_search_threshold: 5,
            placeholder_text_single: '<?php echo __('Pasirinkite'); ?>:',
            hide_results_on_select: false
        });

        var type =  $(pref_id+'.type').val();

         $(pref_id+".saveData").click(function() {
            var mdata = $(pref_id).serialize();
            var temp_val = $(pref_id+'.saveData').text();
            $(pref_id+'.saveData').text('<?php echo __('Saugoma'); ?>...');
            $(this).closest('.widgetcontent').find('img.graphLoader').show();
            $(pref_id+'.chart_holder > div').html('')
            var thisOne = $(this);
            $.post('<?php echo Router::url('/dashboards/saveWidget/',true) ?>',mdata,function(data){
                if($(data).filter('.errorsList').length > 0){
                    $(pref_id+'.errorsList').remove();
                    $(pref_id+'.widgetcontent').prepend(data);
                }else{
                    $(pref_id+'.errorsList').remove();
                    $(pref_id+'.widget_id').val(data);
                    $(pref_id+'.saveData').text(temp_val);
                    $.post('<?php echo Router::url('/dashboards/chart/',true) ?>'+type,mdata,function(data){
                        thisOne.closest('.widgetcontent').find('img.graphLoader').hide()
                        $(pref_id+'.chart_holder > div').html(data);
                    });
                    $(pref_id+'.filtras').slideUp();
                    $(pref_id+'.grafikas').slideDown();
                    $(pref_id+'.closeWidget').attr('widget_id',data).show();
                    $(pref_id+'.shareWidget').attr('widget_id',data).show();
                    $(pref_id+".titleText").html($(pref_id+'#NodeIndicator1 option:selected').html());
               }
            });

        });
         $(pref_id+".changeData").click(function() {
            $(pref_id+'.filtras').slideDown();
            $(pref_id+'.grafikas').slideUp();
         });


        $(pref_id+".field_node").change(function() {
           fillData<?php echo $uid ?> (pref_id);
        });
         $( pref_id+".show_in_groups").change(function() {
           fillData<?php echo $uid ?> (pref_id);
        });

        fillData<?php echo $uid ?> (pref_id);


        /*$(pref_id+'.date_selector').datetimepicker({
            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',
            firstDay : 1,
            closeText: 'Uždaryti',
            currentText: 'Dabar',
            timeText: 'Laikas',
            hourText: 'Valandos',
            minuteText: 'Minutės',
            alwaysSetTime: true,
            autoClose: false,
            onSelect: function(dateText,inst){
                //$('.ui-datepicker-close').trigger('click');
                var nonDateFoun = false;
                $(pref_id+'.date_selector').each(function(){
                    if(new Date($(this).val()) === "Invalid Date" || isNaN(new Date($(this).val()))) nonDateFoun = true;
                });
                if(!nonDateFoun) $(pref_id+'#time_pattern').val('');
            }
        });*/

        $(pref_id+'.daterange_selector').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ "
            },
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            opens: 'right',
            autoUpdateInput: false
        }, function(start, end, label) {
        	$('#datestart<?php echo $uid ?>').val(start.format('YYYY-MM-DD HH:mm')+' ~ '+end.format('YYYY-MM-DD HH:mm'));
        }).on('show.daterangepicker', function(ev, picker) {
            $(pref_id+'#time_pattern').val('');
            $(pref_id+'.field_from').addClass('hidden');
        });
        $(pref_id+'.singledate_selector').daterangepicker({
            singleDatePicker: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            opens: 'right',
            locale: {
                format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ "
            }
        });

        function fillData<?php echo $uid ?> (pref_id) {

        }

        $(pref_id+'i.print').bind('click', function(){
            $(this).parent().find('.iconsweets-fullscreen').trigger('click');
            setTimeout(function() {
                var newWindow = window.open();
                newWindow.document.write($('#graph_<?php echo $uid ?>').html());
                $('.ui-dialog-titlebar-close').trigger('click');
                newWindow.print();
            }, 1000);
            return;
        });

        $(pref_id+'.export').bind('click',function(e){
            e.preventDefault();
            $(this).parent().find('.iconsweets-fullscreen').trigger('click');
            setTimeout(function() {
                get_data_<?php echo $uid ?>(true);
            }, 1000);
            return false;
        });

        $(pref_id+'#NodeIndicator1').bind('change',function(e){
            e.preventDefault();
            if($.inArray(parseInt($(this).val()), [27,26]) < 0){
                $(this).closest('form').find('div.downtimes').addClass('hidden');
                $(this).closest('form').find('div.downtime_level').addClass('hidden');
                $(this).closest('form').find('#NodeDowntimeLevel').val('');
            }else {
                $(this).closest('form').find('div.downtime_level').removeClass('hidden');
                $(this).closest('form').find('div.downtimes').removeClass('hidden');
            }

            if($.inArray(parseInt($(this).val()), [6,18,19,22,26,27]) < 0){
                $(this).closest('form').find('div.add_downtime_exclusions').addClass('hidden');
                $(this).closest('form').find('div.add_disabled_shifts').addClass('hidden');
                $(this).closest('form').find('#NodeAddDowntimeExclusions0').prop('checked',true);
                $(this).closest('form').find('#NodeAddDisabledShifts0').prop('checked',true);
            }else {
                $(this).closest('form').find('div.add_downtime_exclusions').removeClass('hidden');
                $(this).closest('form').find('div.add_disabled_shifts').removeClass('hidden');
            }
        });
        $(pref_id+'#NodeIndicator1').trigger('change');
        /*$(pref_id+'#NodeShifts').on('change',function(){
            var selectedCount = $(pref_id+'#NodeShifts :selected').length;
            if(selectedCount <= 1) return;
            $(pref_id+'#NodeSensors :selected').each(function(i,selected){
                if(i==0) return;
                $(selected).removeAttr('selected');
            });
        })
        $(pref_id+'#NodeSensors').on('change',function(){
            var selectedCount = $(pref_id+'#NodeSensors :selected').length;
            if(selectedCount <= 1) return;
            $(pref_id+'#NodeShifts :selected').each(function(i,selected){
                if(i==0) return;
                $(selected).removeAttr('selected');
            });
        });*/

        $(pref_id+'#timePatterns li a').bind('click',function(){
            if($(this).data('timeformat') == 11){
                //$(pref_id+'.field_start input[type="text"]').val($(this).html());
                $(pref_id+'.field_from').removeClass('hidden');
            }else{
                $(pref_id+'.field_from').addClass('hidden');
            }
            $(pref_id+'.field_start_end input[type="text"]').val($(this).html());
            $(pref_id+'#time_pattern').val($(this).data('timeformat'));
        });

        $(pref_id+'#NodeSensors').bind('change', function(){
            var ceckedSensorsList = $(this).val();
            $(pref_id+'#NodeShifts option').not('option[value="-1"]').remove();
            var sensorsShifts = new Array();
            for(var key in ceckedSensorsList){
                if(!branchesShifts[sensorBranches[ceckedSensorsList[key]]]){ return; }
                sensorsShifts = sensorsShifts.concat(branchesShifts[sensorBranches[ceckedSensorsList[key]]]);
            }
            sensorsShifts = $.unique(sensorsShifts);
            for(var key in sensorsShifts){
                $(pref_id+'#NodeShifts').append('<option value="'+$.trim(sensorsShifts[key].split('~')[0])+'">'+sensorsShifts[key]+'</option>');
            }
            $(pref_id+'#NodeDowntimeLevel').trigger('change');
        });
        $(pref_id+'#NodeSensors').trigger('change');

        var selectedOptionsList = <?php echo isset($this->request->data['Node']['downtimes'])?json_encode($this->request->data['Node']['downtimes']):'[]'; ?>;
        $(pref_id+'#NodeDowntimeLevel').bind('change', function(){
            if($(this).val() <= 0){ return false; }
            var selectedSensors = $(pref_id+'#NodeSensors').val();
            var linesIds = ['0'];
            var options = [];
            var downtimesIncluded = new Array();
            for(var i in selectedSensors){
                var sensorId = selectedSensors[i];
                var lineIdArray = [0, lineSensors[sensorId]];
                var levelNr = $(this).val()-1;
                var sensorTypeArray = [0,typeSensors[sensorId]];
                for(var lineIdKey in lineIdArray){
                    var lineId = lineIdArray[lineIdKey];
                    for(sensorTypeLey in sensorTypeArray){
                        var sensorType = sensorTypeArray[sensorTypeLey];
                        if(typeof downtimes[lineId] === 'undefined' || typeof downtimes[lineId][levelNr] === 'undefined' || typeof downtimes[lineId][levelNr][sensorType] === 'undefined'){ continue; }
                        for(var downtimeKey in downtimes[lineId][levelNr][sensorType]){
                            if($.inArray(downtimeKey, downtimesIncluded) >= 0){ continue; }
                            options.push([downtimes[lineId][levelNr][sensorType][downtimeKey], downtimeKey]);
                            downtimesIncluded.push(downtimeKey);
                        }
                    }
                }
            }
            options = options.sort();
            $(pref_id+'#NodeDowntimes').html('');
            for(i in options){
                if($.inArray(String(options[i][1]), selectedOptionsList) >= 0){ var selected='selected="selected"'; }else{ selected = ''; }
                $(pref_id+'#NodeDowntimes').append('<option '+selected+' value="'+options[i][1]+'">'+options[i][0]+'</option');
            }
        });
        $(pref_id+'#NodeDowntimeLevel').trigger('change');
    });

</script>