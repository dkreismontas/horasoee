
<?php
if(isset($errors) && !empty($errors)){
	echo '<div class="alert alert-danger errorsList" role="alert">';
	foreach($errors as $key => $error){
		echo '<span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>&nbsp;'.$error.'</br>';
		echo '<script type="text/javascript">jQuery("#'.$key.'").parents(".form-group").addClass("alert-danger");</script>';
	}
	echo '</div>';
}
?>