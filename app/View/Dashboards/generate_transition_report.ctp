<?php 
ob_end_clean();
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, 1, "Produktas iš");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, 1, "Produktas į");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, 1, "Perėjimo pav.");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, 1, "Pradžia");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, 1, "Pabaiga");
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, 1, "Trukmė");
foreach($problems as $key => $problem){
    $startDate = new DateTime($problem['FoundProblem']['start']);
    $endDate = new DateTime($problem['FoundProblem']['end']);
    if(!empty($problem['ApprovedOrderPrev'])){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $key+2, $problem['ApprovedOrderPrev']['Plan']['production_code'].' '.$problem['ApprovedOrderPrev']['Plan']['production_name']);
    }
    if(!empty($problem['ApprovedOrderNext'])){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(1, $key+2, $problem['ApprovedOrderNext']['Plan']['production_code'].' '.$problem['ApprovedOrderNext']['Plan']['production_name']);
    }
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(2, $key+2, Settings::translate($problem['Problem']['name']));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(3, $key+2, $problem['FoundProblem']['start']);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(4, $key+2, $problem['FoundProblem']['end']);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(5, $key+2, $startDate->diff($endDate)->format("%H:%I:%S"));
}
for ($col = 'A'; $col != 'N'; $col++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
}
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.$sensor['Sensor']['name'].' '.__('perėjimų ataskaita').'.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();