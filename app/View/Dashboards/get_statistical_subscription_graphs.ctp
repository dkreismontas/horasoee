<style type="text/css">
    .table tbody tr td{
        vertical-align: middle;
        text-align: center;
    }
</style>
<table class="table table-bordered">
    <thead>
        <tr>
            <th><?php echo __('Prenumerata'); ?></th>
            <th><?php echo __('Pavadinimas'); ?></th>
            <th><?php echo __('Tipas'); ?></th>
            <th><?php echo __('Laikotarpis'); ?></th>
            <th><?php echo __('Periodiškumas'); ?></th>
            <th><?php echo __('Perduoti'); ?></th>
            <th colspan="3"></th>
        </tr>
    </thead>
    <tbody><?php foreach($subscriptions as $subscription){
        $subscription = $subscription['StatisticalReportsSubscription'];
        $formData = json_decode($subscription['data']);
    ?>
        <tr data-subscription_id = "<?php echo $subscription['id'] ?>">
            <td><input class="subsribed form-control" type="checkbox" value="1" <?php if($subscription['subscribed']){echo 'checked="checked"';}; ?> /></td>
            <td><input style="margin-bottom: 0;" class="name form-control input-medium" type="text" value="<?php echo $subscription['name']; ?>" /></td>
            <td><?php echo __($formData->form_type); ?></td>
            <td><?php echo $timePatterns[$formData->time_pattern]??__('Intervalas neegzistuoja'); ?></td>
            <td><?php echo $statSubscrPeriodTypes[$subscription['periodicity']]??__('Periodiškumo tipas neegzistuoja'); ?></td>
            <td><a class="assignToOtherUser" href="" title="<?php echo __('Perduoti'); ?>"><i class="iconsweets-shuffle"></i></a></td>
            <td><button class="btn btn-primary save"><?php echo __('Išsaugoti'); ?></button></td>
            <td>
                <button class="btn btn-primary send"><?php echo __('Išsiųsti'); ?></button>
                <?php echo $this->Html->image('/images/loaders/loader15.gif', array('style'=>'position:relative; top: 10px; display: none;')); ?>
            </td>
            <td><button class="btn btn-primary delete"><?php echo __('Ištrinti'); ?></button></td>
            <td></td>
        </tr>
    <?php } ?></tbody>
</table>
<script type="text/javascript">
    jQuery(document).ready(function($){

        $('button.save').bind('click', function(e){
            e.preventDefault();
            var row = $(this).closest('tr');
            var subscribtionId = row.data('subscription_id');
            $.ajax({
                url: "<?php echo $this->Html->url('/dashboards/change_statistical_subscription_graph',true); ?>",
                type: 'post',
                data: {updateWhere:{id:subscribtionId}, updateFields: {
                    subscribed: row.find('input.subsribed').is(':checked')?1:0,
                    name: row.find('input.name').val()
                }}
            })
        });
        $('button.send').bind('click', function(e){
            e.preventDefault();
            var loader = $(this).parent().find('img');
            loader.show();
            var row = $(this).closest('tr');
            var subscribtionId = row.data('subscription_id');
            $.ajax({
                url: "<?php echo $this->Html->url('/dashboards/send_statistical_subscription_graph',true); ?>",
                type: 'post',
                data: {subscribtionId:subscribtionId},
                dataType: 'JSON',
                error: function(data){
                    alert(data.responseJSON.errors);
                },complete: function(){
                    loader.hide();
                }
            })
        });
        $('button.delete').bind('click', function(e){
            e.preventDefault();
            var conf = confirm('<?php echo __('Ar tikrai norite ištrinti prenumeratą?'); ?>');
            if(!conf){ return false; }
            var row = $(this).closest('tr');
            var subscribtionId = row.data('subscription_id');
            $.ajax({
                url: "<?php echo $this->Html->url('/dashboards/delete_statistical_subscription_graph',true); ?>",
                type: 'post',
                data: {subscribtionId:subscribtionId},
                success: function(){
                    row.remove();
                }
            })
        });
        /*$('input.subsribed').bind('change', function(){
            var subscribtionId = $(this).closest('tr').data('subscription_id');
            $.ajax({
                url: "<?php echo $this->Html->url('/dashboards/change_statistical_subscription_graph',true); ?>",
                type: 'post',
                data: {updateWhere:{id:subscribtionId}, updateFields: {subscribed: $(this).is(':checked')?1:0}}
            })
        });
        $('input.name').bind('blur', function(){
            var subscribtionId = $(this).closest('tr').data('subscription_id');
            $.ajax({
                url: "<?php echo $this->Html->url('/dashboards/change_statistical_subscription_graph',true); ?>",
                type: 'post',
                data: {updateWhere:{id:subscribtionId}, updateFields: {name: $(this).val()}}
            })
        });*/
        $('a.assignToOtherUser').bind('click', function(e){
            e.preventDefault();
            var subscribtionId = $(this).closest('tr').data('subscription_id');
            $('#usersHavingMailList li').each(function(){
                $(this).show();
                $(this).children('button').attr('widget_id',subscribtionId).attr('action','share_statistical_subscription');
            });
            $("#usersHavingMailList" ).dialog({
                width: 500,
                title: '<?php echo __('Perduoti kitam vartotojui'); ?>'
            })
        });
    })
</script>