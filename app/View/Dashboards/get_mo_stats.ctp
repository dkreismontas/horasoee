<div class="row">
    <div class="col-md-3" style="border: 1px solid #ccc; border-right: 0;">
        <?php
        echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control',
            )
        ));
        echo __('Ieškoti pagal datą');
        ?>
        <div class="row">
            <div class="col-md-8">
                <?php echo $this->Form->input('start_end_date', array('label' => __('Nuo - iki'), 'class'=>'form-control daterange')); ?>
            </div>
            <!--div class="col-md-4">
                <?php echo $this->Form->input('end_date', array('label' => __('Pabaiga'), 'class'=>'form-control datepicker')); ?>
            </div-->
            <div class="col-md-4">
                <?php echo $this->Form->input('product_code', array('label' => __('Gaminio kodas'))); ?>
            </div>
        </div>

        <?php echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
    <div class="col-md-2" style="border: 1px solid #ccc; ">
        <?php echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control',
            )
        ));
        echo __("Ieškoti pagal užsakymo nr.");
        echo $this->Form->input('mo', array(
            'label' => __('Įveskite užsakymo nr'),
        ));
        echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
</div>
<div class="mo-picker">
    <?php if(isset($mo_list)) echo $this->element('mo_picker'); ?>
</div>


<script>
    jQuery("document").ready(function ($) {
        jQuery(".datepicker").datetimepicker({
            dateFormat: "yy-mm-dd"
        });
        if(jQuery.uniform)
            jQuery('input:checkbox, input:radio, .uniform-file').uniform();


        jQuery("div.mo-picker .date-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");

            var checker =  $(this).closest("li");
//            console.log(checker.find("ul").find(".checker"));
            var input = checker.find("ul").find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
        jQuery("div.mo-picker .line-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");
            var checker =  $(this).closest('span.mo-line').next();
            console.log($(this));
            console.log(checker);
            var input = checker.find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
        $('input.daterange').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ " 
            },
            forceUpdate: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 1,
            opens: 'right',
          }, function(start, end, label) {
            //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });

</script>
<style>.ui-state-active {background-color:#ccc;}</style>
<?php echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon'); ?>

<?php if (!empty($mo_number))
{
    $i = 0;?>
    <br/>
    <div><b><?php echo __("Rezultatų kiekis") . ": " . (is_array($mo_number)?count($mo_number):'1'); ?></b></div>
    <table class="table table-bordered mo-stats">
        <?php foreach ($orders as $order){
            $additionalData = json_decode($order['ApprovedOrder']['additional_data'],true);
            $nrFromForm = $additionalData['add_order_number']??'';
            ?>
            <tr class="mo-header">
                <th><?php echo __('Užsakymo numeris'); ?></th>
                <?php echo trim($nrFromForm)?'<th>'.__('Užsakymo numeris iš formos').'</th>':''; ?>
                <th><?php echo __('Jutiklio užfiksuotas kiekis'); ?></th>
                <th><?php echo __('Operatoriaus deklaruotas kiekis'); ?></th>
            </tr>
            <tr class="mo-combined">
                <td><?php echo $order['OrderCalculation']['mo_number']; ?></td>
                <?php echo trim($nrFromForm)?'<td>'.$nrFromForm.'</td>':''; ?>
                <td><?php echo $order[0]['quantity']; ?></td>
                <td><?php echo $order[0]['quantity']; ?></td>
            </tr>
        <?php } ?>
    </table>

<?php } ?>