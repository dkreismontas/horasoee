<?php
    $rownputFileName = ROOT.DS.'app'.DS.'webroot'.DS.'img'.DS.'opengraphs'.DS.$file_name;
    if(!file_exists($rownputFileName)) die();
    $gdImage = @imagecreatefrompng($rownputFileName);
    $styleArray = array(
           'borders' => array(
                 'outline' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                        'color' => array('argb' => '000000'),
                 ),
           ),
    );
    //Isvedame pirmaja lentele
    $row = 1;
    $objRichText = new PHPExcel_RichText();
    $objBold = $objRichText->createTextRun($title);
    $objBold->getFont()->setBold(true);
    $objPHPExcel->getActiveSheet()->mergeCells('A1:L1');
    $objPHPExcel->getActiveSheet()->getCell('A1')->setValue($objRichText);
    $objPHPExcel->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
    $row++;
    
    $cell = 1;
    $headers = array();
    foreach($data as $sensorTitle => $values){
        $headers = array_merge($headers,array_keys($values));
    }
    $headers = array_unique($headers);
    sort($headers);
    foreach($headers as $headerName){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell, $row, $headerName);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($cell, $row)->applyFromArray($styleArray);
        $cell++;
    }
    $row++;
    foreach($data as $key => $record){
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, $key);
        $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow(0, $row)->applyFromArray($styleArray);
        foreach($record as $key2 => $record2){
            $cell = array_search($key2, $headers)+1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($cell, $row, $record2);
            $objPHPExcel->getActiveSheet()->getStyleByColumnAndRow($cell, $row)->applyFromArray($styleArray);    
        }
        $row++;
    }
    $col = 'A';
    for ($x = 0; $x <= sizeof($headers); $x++){
        $objPHPExcel->getActiveSheet()->getColumnDimension($col++)->setAutoSize(true);
    }
    $file_name = explode('_',$file_name);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToPage(true);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToWidth(1);
    $objPHPExcel->getActiveSheet()->getPageSetup()->setFitToHeight(0);
    ob_end_clean();
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8');
    header('Content-Disposition: attachment;filename="'.(isset($file_name[0])?$file_name[0]:'failas').'-'.date('Y-m-d').'.xlsx"');
    $objDrawing->setName('Sample image');
    $objDrawing->setDescription('Sample image');
    $objDrawing->setImageResource($gdImage);
    $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
    $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
    $objDrawing->setHeight(600);
    $objDrawing->setWidth(1000);
    $objDrawing->setCoordinates('A'.($row+=2));
    $objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
    $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
    $objWriter->save('php://output');
    unlink($rownputFileName);
    die();
