<?php
    $this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas prastovos tipas'); ?></button>
<?php
foreach($pluginFunctionality['problemTypeLinks'] as $func) { echo $func; }
?>
<br /><br />
<table id="downtimesTable" class="table table-bordered table-striped table-hover">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
            <th><?php echo __('Tėvinis'); ?></th>
            <th><?php echo __('Pavadinimas'); ?></th>
            <th><?php echo __('Tipas'); ?></th>
            <th><?php echo __('Linija'); ?></th>
            <th><?php echo __('Greičiui'); ?></th>
            <th><?php echo __('Perėjimui'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="3"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model];  $classColor=$li->disabled?'danger':''; ?>
		<tr class="<?php echo $classColor; ?>">
			<td><?php echo $li->id; ?></td>
            <td><?php echo ($li->parent_id != 0 &&isset($parentOptions[$li->parent_id])) ? 'ID: '.$li->parent_id.' '.Settings::translate($parentOptions[$li->parent_id]) : ''; ?></td>
            <td><?php echo Settings::translate($li->name); ?></td>
			<td><?php echo isset($typeOptions[$li->sensor_type]) ? $typeOptions[$li->sensor_type] : $li->sensor_type; ?></td>
			<td><?php echo isset($lineOptions[$li->line_id]) ? $lineOptions[$li->line_id] : $li->line_id; ?></td>
			<td><?php echo $li->for_speed ? __('Taip') : __('Ne'); ?></td>
			<td><?php echo $li->transition_problem ? __('Taip') : __('Ne'); ?></td>
			<th>
			    <?php if ($li->id != Problem::ID_NEUTRAL && $li->id != Problem::ID_NO_WORK && $li->id != Problem::ID_NOT_DEFINED && $li->id != Problem::ID_WORK && $li->id != 0 ): ?>
				<a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				&nbsp;
				<a class="btn btn-default" href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
				&nbsp;
				<?php $actionType = $li->disabled?__('Įjungti'):__('Išjungti'); ?>
				<a class="btn btn-default" href="<?php printf($disableUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars(__('Ar tikrai norite %s prastovą?', mb_strtolower($actionType))); ?>');" title="<?php echo htmlspecialchars($actionType); ?>"><span class="glyphicon glyphicon-ban-circle"></span>&nbsp;<?php echo $actionType; ?></a>
				<?php endif; ?>
			</th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
    jQuery('#downtimesTable').dataTable({
        "sPaginationType": "full_numbers",
        //"bScrollInfinite" : true,
        "aLengthMenu": [[100, 200, 500, -1], [100, 200, 500, "<?php echo __('Visi'); ?>"]],
        "aaSorting": [],
        "bScrollCollapse" : true,
        "sScrollY" : "800px",
        "iDisplayLength": -1,
        "paging": true,    
        "scrollY": 500,
        "oLanguage":{
            'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
            'sSearch':'<?php echo __('Paieška');?>',
            'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
            'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
            'sInfoEmpty':"",
            'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
            'oPaginate':{
                'sFirst': '<?php echo __('Pirmas');?>',
                'sPrevious': '<?php echo __('Ankstesnis');?>',
                'sNext': '<?php echo __('Kitas');?>',
                'sLast': '<?php echo __('Paskutinis');?>'
            }
        },
        "oSearch": {"sSearch": "<?php echo $this->Session->read('FilterText.'.$this->params['controller']); ?>"}
    });
    $('.maincontentinner').on('keyup','#downtimesTable_filter input', function(){
        $.ajax({
            url: '<?php echo Router::url(array('controller'=>'settings','action'=>'set_filter_text'),true); ?>',
            type: 'POST',
            data: {filter: 'FilterText.<?php echo $this->params['controller']; ?>', value: $(this).val()}
        });
    });
</script>
