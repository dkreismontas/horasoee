<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas').$this->App->addLanguages($possibleLanguages), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php if ($item && ($item[Problem::NAME]['id'] == Problem::ID_NEUTRAL || $item[Problem::NAME]['id'] == Problem::ID_NO_WORK)): ?>
<?php echo $this->Form->input('sensor_type', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php else: ?>
<?php echo $this->Form->input('sensor_type', array('label' => __('Tipas'), 'options' => $typeOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('parent_id', array('label' => __('Tėvinis'), 'options' => $parentOptions, 'class' => 'form-control chosen-select', 'div' => array('class' => 'form-group'))); ?>
<div class="checkbox">
	<label for="ProblemForSpeed">
		<?php echo $this->Form->input('for_speed', array('label' => false, 'div' => null)); ?>
		<?php echo __('Greičiui'); ?>
	</label>
</div>
<?php echo $this->Form->input('mean_time_calculations', array('type'=>'checkbox','label' => __('Įtraukti į MTTR ir MTBF skaičiavimus?'), 'div' => 'checkbox')); ?>
<?php echo $this->Form->input('transition_problem', array('type'=>'checkbox','label' => __('Perėjimo prastova'), 'div' => 'checkbox')); ?>
<?php echo $this->Form->input('transition_time', array('label' => __('Numatyta perėjimo trukmė minutėmis'), 'class' => 'form-control', 'div' => array('class' => 'form-group hidden'))); ?>
<?php endif; ?>
<?php 
if($this->request->params['id'] == 0){
    echo $this->Form->input('line_id', array('label' => __('Linija'), 'options' => $lineOptions, 'multiple'=>true, 'class' => 'form-control chosen-select', 'div' => array('class' => 'form-group'), 'size'=>sizeof($lineOptions)<10?sizeof($lineOptions):10));
}else{
    echo $this->Form->input('line_id', array('label' => __('Linija'), 'options' => $lineOptions, 'class' => 'form-control chosen-select', 'div' => array('class' => 'form-group')));
} ?>
<div>
	<br />
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a class="btn btn-default" href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        var checkTrasition = function(){
            if($('#ProblemTransitionProblem').is(':checked')){
                $('#ProblemTransitionTime').parent().removeClass('hidden');
            }else{
                $('#ProblemTransitionTime').parent().addClass('hidden');
            }
        }
        checkTrasition();
        $('#ProblemTransitionProblem').bind('click', checkTrasition);
        $('#ProblemParentId').bind('change', function(){
        	$.ajax({
        		url: '<?php echo Router::url(array('action'=>'getProblemInfoAjax')); ?>',
        		type: 'POST',
        		data: {problem_id: $(this).val()},
        		dataType: 'JSON',
        		success: function(problem){
        			if(problem.hasOwnProperty('sensor_type')){
        				$('#ProblemSensorType').val(problem.sensor_type);
        			}
        			if(problem.hasOwnProperty('line_id')){
        				$('#ProblemLineId').val(problem.line_id);
        			}
        		}
        	});
        });
    });
</script>
