<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title><?php echo $title_for_layout; ?></title>

<?php
	echo $this->Html->css('style.default');
	echo $this->Html->css('style.navyblue');
?>

<?php 
	 echo $this->Html->script('jquery-1.10.2.min');
	 echo $this->Html->script('jquery-migrate-1.2.1.min');
	 echo $this->Html->script('jquery-ui-1.10.3.min');
	 echo $this->Html->script('modernizr.min');
	 echo $this->Html->script('bootstrap.min');
	 echo $this->Html->script('jquery.cookies');
 echo $this->Html->script('custom');
 ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        jQuery('#login').submit(function(){
            var u = jQuery('#username').val();
            var p = jQuery('#password').val();
            if(u == '' && p == '') {
                jQuery('.login-alert').fadeIn();
                return false;
            }
        });
    });
</script>
</head>

<body class="loginpage">

<div class="loginpanel">
    <div class="loginpanelinner">
        <div class="logo animate0 bounceIn"><?php echo $this->Html->link($this->Html->image(Router::url('/images/logo.png',true)), '/', array('escape'=>false)); ?></div>
    
            <div class="inputwrapper login-alert">
                <div class="alert alert-error"><?php echo __('Neteisingi prisijungimo duomanys'); ?></div>
                  <?php  
	                  echo $this->Session->flash('auth');
					  echo $this->Session->flash();
				  ?>
     
            </div>  
 <?php echo $content_for_layout ?>
    </div><!--loginpanelinner-->
</div><!--loginpanel-->



</body>
</html>
