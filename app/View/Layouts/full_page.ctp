<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?php echo $appName.($title_for_layout ? (' - '.$title_for_layout) : ''); ?></title>

    <?php
    echo $this->Html->css(array('style.default', 'responsive-tables'));
    ?>
    <script type="text/javascript">
        var currentLang = '<?php echo current(explode('_', $currentLang)); ?>';
    </script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-ui-1.10.3.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/modernizr.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.cookies.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.uniform.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/flot/jquery.flot.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/flot/jquery.flot.resize.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/responsive-tables.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.slimscroll.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/chosen.jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/moment.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/daterangepicker.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/main.js"></script>
    <?php
    echo $this->Html->script('jquery.extractor.min');
    echo $this->Html->script('jquery-ui-timepicker-addon');
    echo $this->Html->script('jquery.jeditable.mini');
    echo $this->Html->script('popup_window_caller.js');
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    ?>
</head>
<body style="background: none;">
<?php echo $this->fetch('content'); ?>
</body>
</html>
