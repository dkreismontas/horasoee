<?php
/* @var $navigation MainMenuItem[] */
/* @var $bredcrumbs MainMenuItem[] */
/* @var $curr MainMenuItem */
/* @var $user AppUser */
/* @var $this View */

?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo $appName.($title_for_layout ? (' - '.$title_for_layout) : ''); ?></title>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>

	<script type="text/javascript">
	    var currentLang = '<?php echo current(explode('_', $currentLang)); ?>';
	</script>
	<?php
		echo $this->Html->meta('icon', '/img/favicon.ico');
        if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android')){
            $this->Html->css('android_browser_default_layout.css?v=1', array('inline'=>false));
        }
		echo $this->Html->css(array('style.default.css?v=1', 'responsive-tables', 'style','daterangepicker','bootstrap-xlgrid.min','chosen.min.css?v=2'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');

		if (isset($colorTheme) && $colorTheme) echo $this->Html->css(array($colorTheme));
	?>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/html5shiv.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/respond.min.js"></script>
	<![endif]-->

	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-ui-1.10.3.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/modernizr.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.cookies.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.uniform.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/flot/jquery.flot.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/flot/jquery.flot.resize.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/responsive-tables.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery.slimscroll.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/chosen.jquery.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/moment.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/daterangepicker.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/main.js"></script>
    <?php
        echo $this->Html->script('jquery.extractor.min');
        echo $this->Html->script('jquery-ui-timepicker-addon');
        echo $this->Html->script('jquery.jeditable.mini');
        echo $this->Html->script('popup_window_caller.js');
    ?>
	<!--[if lte IE 8]>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/excanvas.min.js"></script>
	<![endif]-->
</head>
<body<?php if (!$useLeftPanel) echo ' class="body-nobg"'; ?>>
	<div id="mainwrapper" class="mainwrapper">
		<div class="header">
			<div class="logo">
				<a href="<?php echo $this->Html->url('/'); ?>"><img src="<?php echo $baseUri; ?>images/logo.png" alt="" /></a>
			</div>
			<div class="headerinner">
				<ul class="headmenu">
					<?php $cc = 0; ?>
					<?php foreach ($navigation as $li): ?>
					<li class="<?php echo ((($cc % 2) == 0) ? '' : '').($li->getIsSelected() ? ' active' : ''); ?>">
						<a href="<?php echo $li->getUrl(); ?>"<?php if ($li->hasItems()) echo ' class="dropdown-toggle" data-toggle="dropdown"'; ?>>
							<?php if ($li->getIcon()): ?><span class="head-icon head-icon2 <?php echo htmlspecialchars($li->getIcon()); ?>"></span><?php endif; ?>
							<span class="headmenu-label"><?php echo $li->getTitle(); ?></span>
						</a>
						<?php if ($li->hasItems()): ?>
						<ul class="dropdown-menu">
							<li class="nav-header"><?php echo $li->getTitle(); ?></li>
							<?php foreach ($li->getItems() as $sli): ?>
							<li>
								<a href="<?php echo $sli->getUrl(); ?>" <?php if($sli->target){echo 'target="'.$sli->target.'"';} ?>>
									<?php if ($sli->getIcon()): ?><span class="miico <?php echo htmlspecialchars($sli->getIcon()); ?>"></span><?php endif; ?>
									<?php echo $sli->getTitle(); ?>
								</a>
							</li>
							<?php endforeach; ?>
						</ul>
						<?php endif; ?>
					</li>
						<?php $cc++; ?>
					<?php endforeach; ?>
					<li class="right">
                        <?php if(!empty($possibleLanguages)){ ?>
					    <div id="languages">
                            <a href="" data-toggle="dropdown" class="dropdown-toggle"><img src="<?php echo Router::url("/files/flags/{$user->language}.png"); ?>" /></a>
                            <ul class="dropdown-menu pull-right skin-color">
                                <?php
                                foreach($possibleLanguages as $slug => $langText): ?>
                                    <li><a href="<?php echo Router::url(array('controller'=>'users', 'action'=>'change_language', $slug)); ?>"><img src="<?php echo Router::url("/files/flags/$slug.png"); ?>" />&nbsp;<?php echo $langText; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                        <?php } ?>
                        <div class="userloggedinfo">
                            <div class="userinfo">
                                <h4 <?php if(empty($possibleLanguages)){echo 'style="padding-left: 0;"' ;} ?>><?php echo isset($user->username)?$user->username:''; ?></h4>
                                <ul>
                                    <li><h3><?php echo isset($companyTitle)?$companyTitle:''; ?></h3></li>
                                </ul>
                            </div>
                        </div>
                    </li>
				</ul><!--headmenu-->
			</div>
			<br style="clear: both; line-height: 1px; height: 1px;" />
		</div>
		<?php if ($useLeftPanel): ?>
		<div class="leftpanel">

			<div class="leftmenu">
				<ul class="nav nav-tabs nav-stacked">
					<li class="nav-header"><?php echo __('Navigacija'); ?></li>
					<?php
                    foreach ($navigation as $li): ?>
					<li<?php if ($li->getIsSelected()) echo ' class="active"'; ?>>
						<a href="<?php echo $li->getUrl(); ?>">
							<?php if ($li->getIcon()): ?><span class="miico <?php echo htmlspecialchars($li->getIcon()); ?>"></span>&nbsp;<?php endif; ?>
							<?php echo $li->getTitle(); ?>
						</a>
					</li>
					<?php endforeach;
					?>
				</ul>
			</div><!--leftmenu-->

		</div><!-- leftpanel -->
		<?php endif; ?>

		<div<?php if ($useLeftPanel) echo ' class="rightpanel"'; ?>>

			<ul class="breadcrumbs">
				<li><a href="<?php echo $this->Html->url('/'); ?>"><i class="iconfa-home"></i></a> <span class="separator"></span></li>
				<?php foreach ($bredcrumbs as $li): ?>
				<li><?php echo $this->Html->link($li->getTitle(), $li->getUrl()); ?></li>
				<?php endforeach; ?>
			</ul>
			<?php $curr = reset($bredcrumbs); ?>
			<?php if ($curr): ?>
			<div class="pageheader">
				<?php if ($li->getIcon()): ?><div class="pageicon"><span class="<?php echo htmlspecialchars($li->getIcon()); ?>"></span></div><?php endif; ?>
				<div class="pagetitle">
					<h5><?php echo ($d = $curr->getDescription()) ? $d : '&nbsp;'; ?></h5>
					<h1><?php echo isset($h1_for_layout) ? $h1_for_layout : $curr->getTitle(); ?></h1>
					<?php echo isset($unreadedUpdatesExist) && $unreadedUpdatesExist > 0?'<h4 class="widgettitle title-warning">'.__('Sistemoje yra sukurti %d atnaujinimai, su kuriais dar nesate susipažinę. ', $unreadedUpdatesExist).$this->Html->link(__('Susipažinti dabar'),array('controller'=>'settings','action'=>'check_updates')).'</h4>':''; ?>
				</div>
			</div><!--pageheader-->
			<?php elseif(isset($h1_for_layout)): ?>
			    <div class="pageheader">
                    <?php if(isset($icon_for_layout)): ?><div class="pageicon"><span class="<?php echo htmlspecialchars($icon_for_layout); ?>"></span></div><?php endif; ?>
                    <div class="pagetitle"><h1><?php echo $h1_for_layout; ?></h1></div>
                </div>
			<?php endif; ?>

			<div class="maincontent">
				<div class="maincontentinner">
					<div>
						<?php echo $this->Session->flash('flash',array('params'=>array('class'=>'widgettitle title-warning'))); ?>
						<?php echo $this->fetch('content'); ?>
					</div>

					<div class="footer">
						<div class="footer-left">
							<span><?php echo $copyRightMsg; ?></span>
						</div>
						<div class="footer-right">
							<span><?php echo $authorMsg; ?></span>
						</div>
					</div><!--footer-->

				</div><!--maincontentinner-->
			</div><!--maincontent-->

		</div><!--rightpanel-->

	</div><!--mainwrapper-->
<?php
    //uzkraunam visus integruotus pluginus
    if(!$this->request->is('ajax')) {
        foreach (CakePlugin::loaded() as $plugin) {
        	if(!isset($enabledPlugins->$plugin) || $plugin != Configure::read('companyTitle')) continue;
        	?>
            <script type="text/javascript"
                    src="<?php echo $this->Html->url('/', true) . Inflector::underscore($plugin) . '/plugins/?plugin_request&current_page=' . urlencode($this->here); ?>&t=<?php echo uniqid(); ?>"></script>
        <?php
        }
    }
?>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            /*Dropdown selecbox */
            $('.chosen-select[multiple="multiple"]').parent().find('label').css({width: '100%'}).append('<p style="float: right;"><input type="checkbox" class="chosen-select-all" /><?php echo __('Pažymėti viską'); ?></p>');
            $('.chosen-select[multiple="multiple"]').parent().prev('label').css({width: '100%'}).append('<p style="float:right;"><input type="checkbox" class="chosen-select-all" /><?php echo __('Pažymėti viską'); ?></p>');
            $('#dashboardItems .chosen-select[multiple="multiple"]').parent().prev('label').css({width: ''}).find('p').css({float: 'left'});
            $('.chosen-select-all').bind('click', function(){
                if($(this).is(':checked')){
                    $(this).closest('div').find('option').prop('selected', true);
                }else{
                    $(this).closest('div').find('option:selected').removeAttr('selected');
                }
                $(this).closest('div').find('select').trigger('chosen:updated');
            });
            window.addEventListener("pageshow", () => {
                $('.chosen-select[multiple="multiple"]').trigger('chosen:updated');
            });
            $('.chosen-select').chosen({
                no_results_text: '<?php echo __('Nepavyko rasti'); ?>:',
                disable_search_threshold: 5,
                placeholder_text_single: '<?php echo __('Pasirinkite'); ?>',
                placeholder_text_multiple: '<?php echo __('Pasirinkite'); ?>',
                hide_results_on_select: false
            });
        });
    </script>
</body>
</html>
