<?php $v = time(); ?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
    <link rel="manifest" href="<?php echo $baseUri; ?>css/manifest.json">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo $appName.($title_for_layout ? (' - '.$title_for_layout) : ''); ?></title>
    <!-- <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-migrate-1.2.1.min.js"></script> -->
	<?php
		echo $this->Html->meta('icon');
        // if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android')){
        //     $this->Html->css('android_browser_bare_layout.css?v=2', array('inline'=>false));
        // }
        
		// $bodyClass = is_array($bodyClass)?current($bodyClass):$bodyClass;
		if (isset($bodyClass) && isset($bodyClass) && !empty($bodyClass) && $bodyClass && !in_array('loginpage',$bodyClass)) {
			$bodyClassFullExt = array_map(function($class){ return $class = rtrim($class,'.css').'.css?v=8'; }, $bodyClass);
			echo $this->Html->css($bodyClassFullExt);
        }
        
		// echo $this->fetch('meta');
		// echo $this->fetch('css');
        // echo $this->fetch('script');
        
        // echo $this->Html->script('jquery-ui-1.10.3.min.js');
        // echo $this->Html->script('jquery.extractor.min');
        // echo $this->Html->script('popup_window_caller.js');

        echo $this->Html->script("work-center/components/bar-graph.js?v=".$v);
        echo $this->Html->script("work-center/components/radial-progress.js?v=".$v);
        echo $this->Html->script("work-center/components/speedometer-gauge.js?v=".$v);
            
        // help: ???
		// if (isset($colorTheme) && $colorTheme) echo $this->Html->css(array($colorTheme)); // css/style.navyblue.css
	?>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/html5shiv.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/respond.min.js"></script>
    <![endif]-->


    <!-- Font Awesome -->
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    
    <!-- Jquery -->
    <script src="/js/jquery-1.10.2.min.js"></script>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/css/work-center/bootstrap5.css">
    <script src="/js/work-center/bootstrap5.js"></script>

    <!-- Date picker -->
    <link rel="stylesheet" href="/css/work-center/bootstrap-material-datetimepicker.css?v=<?php echo $v; ?>" />
    <script type="text/javascript" src="/js/work-center/moment.js?v=<?php echo $v; ?>"></script>
    <script type="text/javascript" src="/js/work-center/bootstrap-material-datetimepicker.js?v=<?php echo $v; ?>"></script>

    <!-- Angular -->
    <script type="text/javascript" src="/js/polyfill.js"></script>
    <script type="text/javascript" src="/js/angular.min.js"></script>
    <script type="text/javascript" src="/js/angular-sanitize.js"></script>

    <!-- Fonts -->
    <link rel="stylesheet" href="/fonts/-montserrat/index.css">
    <link rel="stylesheet" href="/fonts/-roboto/index.css">
    <link rel="stylesheet" href="/fonts/-varela-round/index.css">
    <link rel="stylesheet" href="/fonts/-material-icons/index.css">

    <!-- Local -->
    <link rel="stylesheet" href="/css/work-center/work-center-main.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/css/work-center/work-center-helper.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/css/work-center/work-center-layout.css?v=<?php echo $v; ?>">
    <link rel="stylesheet" href="/css/work-center/tml-colors.css?v=<?php echo $v; ?>">

    <!-- 
        /*
            CALIBRI BOLD: timeline table, buttons, th
            CALIBRI REGULAR: other texts

            MONTSERRAT SEMIBOLD: header
            VARELA ROUND: numbers	

            TIME MINUTES: #666666 (light), #333333 (dark)
        */
    -->

	<style type="text/css" rel="stylesheet">
        <?php if(isset($sensorData['Sensor']['show_count_marker']) && $sensorData['Sensor']['show_count_marker']){ ?>
        .tml-quantity-box{
            display: none !important;
        }
        <?php } ?>
    </style>
</head>

<body <?php if (isset($bodyClass) && $bodyClass) { echo ' class="'.htmlspecialchars(is_array($bodyClass)?current($bodyClass):$bodyClass).'"'; } ?>>

<!-- <button id="view-fullscreen">Fullscreen</button>
<script type="text/javascript">
    var viewFullScreen = document.getElementById("view-fullscreen");
    if (viewFullScreen) {
        viewFullScreen.addEventListener("click", function() {
            var docElm = document.documentElement;
            if (docElm.requestFullscreen) {
                docElm.requestFullscreen();
            } else if (docElm.msRequestFullscreen) {
                docElm.msRequestFullscreen();
            } else if (docElm.mozRequestFullScreen) {
                docElm.mozRequestFullScreen();
            } else if (docElm.webkitRequestFullScreen) {
                docElm.webkitRequestFullScreen();
            }
            screen.orientation.lock("landscape");
        })
    }
</script> -->

    <?php echo $this->Session->flash(); ?>
    <?php echo $this->fetch('content'); ?>
    
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/work-center/work-center.js?v=<?php echo $v; ?>"></script>
</body>
</html>
