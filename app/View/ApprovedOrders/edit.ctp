<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->element('ApprovedOrderTable', array('relatedPostponedOrders'=>$rpo,'item' => $item, 'lossItems' => $lossItems, 'boldLabels' => true)); ?>
<?php if ($item[ApprovedOrder::NAME]['confirmed'] || ($item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_COMPLETE && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_POSTPHONED && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_PART_COMPLETE)): ?>
<?php if(isset($item[ApprovedOrder::NAME]['modified']) && $item[ApprovedOrder::NAME]['modified'] != $item[ApprovedOrder::NAME]['created'] && (int)$item[ApprovedOrder::NAME]['modified'] != 0){
	echo __('Patvirtinimas atliktas').':'.$item[ApprovedOrder::NAME]['modified'];
} ?>
<div class="form-group">
        <?php
        if($item[ApprovedOrder::NAME]['confirmed']) {
            echo "<b>Kiekis (patvirtintas):</b>" . $item[ApprovedOrder::NAME]['confirmed_quantity'];
            echo "<br><br><b>Nuostoliai:</b>";
            foreach($lossItemsConfirmed as $li) {?>
                <br><b><?php echo $li[LossType::NAME]['name']; ?></b>: <?php echo $li[Loss::NAME]['value'].' '.$li[LossType::NAME]['unit']; ?>
            <?php }
        }?>
</div>
<?php else: ?>

<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
<!--	<div class="col-xs-5">-->
<!--		--><?php
//        $pqt = ($item[ApprovedOrder::NAME]['quantity'] - ($item[ApprovedOrder::NAME]['box_quantity'] * $item[Plan::NAME]['box_quantity']));
//        $finished_boxes = floor(($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity'])/$item[Plan::NAME]['box_quantity']);
//        $box_quantity_label = __('Dėžių kiekis');
//        echo $this->Form->input('confirmed_box_quantity', array('value'=>$finished_boxes,'label' => $box_quantity_label, 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<!--	</div>-->
<!--	<div class="col-xs-1">-->
<!--		<div class="form-group"><label>--><?php //echo __('ir'); ?><!--</label></div>-->
<!--	</div>-->
	<div class="col-xs-12">
		<?php 
		if($editApprovedOrderQuantityPerm){
		  echo $this->Form->input('confirmed_item_quantity', array('label' => __('Gaminių kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'value'=>($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity']))); 
		} ?>
	</div>
</div>
<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
    <div class="col-md-4 col-xs-12">
        <?php foreach($lossTypes as $lt) {
            ?>
            <div class="row">
            <label class="control-label col-xs-6"><?php echo $lt['LossType']['name'] ;?> (<?php echo $lt['LossType']['code'] ;?>)</label>
            <div class="col-xs-6">
                <div class="input-group">
                    <input class="form-control input-default" type="text" name="lt_<?php echo $lt['LossType']['id'] ;?>" value=""/>
                    <span class="input-group-addon"><?php echo $lt['LossType']['unit'] ;?></span>
                </div>
            </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php endif; ?>
<div style="clear:both;"></div>
<div>
	<br />
	<?php if ($canSave && !$item[ApprovedOrder::NAME]['confirmed'] && ($item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_COMPLETE || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_PART_COMPLETE)): ?>
	<input type="submit" class="btn btn-primary" value="<?php echo __('Išsaugoti broką ir tvirtinti užsakymą'); ?>" name="confirm_order" />
	<input type="submit" class="btn btn-primary" value="<?php echo __('Išsaugoti tik broką'); ?>" name="save_loss_only" />
	<?php endif; ?>
	<!--<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a> -->
</div>
<?php echo $this->Form->end();?>