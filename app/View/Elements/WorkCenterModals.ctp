<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->
<!-- left side user action buttons drawer -->

<div class="modal left fade oee-font-calibri" id="toolbar-drawer" tabindex="-1">
	<div class="modal-dialog oee-bg-main text-white">
		<div class="modal-content w-100 oee-bg-main gap-3 p-3" id="drawer-toolbar-btns">
            <!-- filled by JS -->
		</div>
	</div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->
<!-- right side user drawer -->

<div class="modal right fade oee-font-calibri fw-bold" id="user-menu-drawer" tabindex="-1">
	<div class="modal-dialog oee-bg-main">
		<div class="modal-content w-100 h-100 oee-bg-main">
			<div class="p-3 pb-0 d-flex text-uppercase align-items-center">
				<div class="fs-3 flex-grow-1"><?php echo __('Vartotojas'); ?>:</div>
				<button class="btn-close p-2" data-dismiss="modal" aria-label="Close" style="outline: none; box-shadow: none;"></button>
			</div>
			<div class="px-3 mb-3">
				<span class="fs-5"><?php echo trim($user->first_name.' '.$user->last_name)?$user->first_name.' '.$user->last_name:$user->username; ?>
			</div>


			<?php foreach($possibleLanguages as $slug => $langText): ?>
                <a class="pointer px-3 py-1 fs-3 fw-bold text-uppercase <?php echo $user->language === $slug?'oee-bg-dark text-white':''?>" href="<?php echo Router::url(array('controller'=>'users', 'action'=>'change_language', $slug)); ?>">
                    <img class="border border-dark mr-2" src="<?php echo Router::url("/files/flags/$slug.png"); ?>">
					<?php echo explode("_", $slug)[0]; ?>
				</a>
				<div style="padding: 1px; background: #333"></div>
			<?php endforeach; ?>

				
			<div class="d-flex text-uppercase align-items-center p-3 pointer mt-auto" onclick="print()">
				<div class="flex-grow-1 fs-4"><?php echo __('Spausdinti puslapį'); ?></div>
				<img  src="/images/svgs/print.svg" style="height: 2em"> 
			</div>
			
			<div style="padding: 1px; background: #333"></div>

			<a class="oee-bg-dark d-flex text-uppercase align-items-center p-3 py-2 pointer" href="<?php echo htmlspecialchars($logoutUrl); ?>" title="<?php echo __('Atsijungti'); ?>">
				<div class="flex-grow-1 text-white fs-4"><?php echo __('Atsijungti'); ?></div>
				<img  src="/images/svgs/sign-out.svg" class="invert" style="height: 2em"> 
			</a>
		</div>
	</div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div class="modal fade" id="toolbarModal" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-content text-uppercase p-3">
        
        <div class="d-flex">
            <div class="oee-bg-light p-2">
                <img class="invert" style="height: 1.5em;" src="/images/svgs/edit.svg">
            </div>
            <div class="oee-bg-main w-100 d-flex align-items-center">
                <div class="fw-bold p-2 pl-4"><?php echo __('Pasirinkti veiksmą'); ?>:</div>
            </div>
        </div>

        <div class="mt-4"></div>

        <div class="d-flex fw-bold mb-2">
            <div class="lh-sm" style="font-size: 0.7em">Išjungti/įjungti darbo centre</div>
            <div class="w-100 text-center lh-sm mr-4" style="font-size: 1.1em"><!--Veiksmas--></div>
        </div>

        <div class="text-white fw-bold text-center d-grid gap-3" id="modal-toolbar-btns">
        </div>

        <div class="mt-5"></div>

        <div class="text-white fw-bold text-center">
            <a class="py-2 px-4 bg-gray d-inline float-right oee-btn" data-dismiss="modal"><?php echo __('Uždaryti'); ?></a>
        </div>

    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="false" class="modal fade" id="declareQuantitiesModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content p-3">

        <div id="declareQuantitiesBlock">
            <h3 class="pull-left"><?php echo __('Pagamintas kiekis'); ?></h3>
            <h4 class="pull-right" data-ng-if="currentPlan.Plan.quantity > 0"> <?php echo __('Plano kiekis'); ?> {{currentPlan.Plan.quantity}}</h4>

            <div class="input-group">
                <span class="w-100">{{sensor.declare_using_crates_and_pieces?sensor.declare_using_crates_and_pieces:(sensor.declare_quantities_label?sensor.declare_quantities_label:'<?php echo __('Kiekis'); ?>')}}</span>
                <input type="text" class="form-control" oninput="numbersOnly(this)" data-ng-model="partialQuantity">
                <span class="input-group-text"><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantity}}</span>
            </div>

            <div class="input-group mt-2" data-ng-show="sensor.declare_using_crates_and_pieces">
                <span class="w-100"><?php echo __('Vienetai'); ?></span>
                <input type="text" class="form-control" oninput="numbersOnly(this)" data-ng-model="partialQuantityPieces">
                <span class="input-group-text"><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantityPieces}}</span>
            </div>
        </div>

        <div id="declareLossesBlock">
            <h3 class="mt-4"><?php echo __('Įveskite nuostolius'); ?></h3>

            <div class="input-group mb-2" data-ng-repeat="lossType in lossTypes">                
                <span class="w-100">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</span>
                <input type="text" class="form-control" oninput="numbersOnly(this)" data-ng-model="lossType.LossType.value" />
                <span class="input-group-text">{{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}} {{lossType.LossType.unit}}</span>
            </div>

            <div class="m-3">
                <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
            </div>
        </div>

        <div class="d-flex gap-2 text-white" data-ng-if="!stateLoading">
            <button class="oee-btn p-2 bg-green" data-ng-click="saveDeclaredQuantities()"><?php echo __('Patvirtinti'); ?></button>
            <button class="oee-btn p-2 bg-gray" data-ng-click="hideQuantitiesModal()"><?php echo __('Atšaukti'); ?></button>
        </div>

    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->
<!-- DONE ( TODO: jei nepasirinktas naujas produktas galima keisti? ) -->

<div aria-hidden="false" class="modal fade" id="swapCurrentPlanModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content p-3">

        <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>

        <div data-ng-if="!swapCurrentPlanYesPlans && !stateLoading">
            <h4><?php echo __('Ar tikrai norite pakeisti dabartinį planą?'); ?></h4>
            <div class="d-flex gap-2 text-white fw-bold">
                <button class="oee-btn p-2 bg-green" data-ng-click="swapCurrentPlanYes()"><?php echo __('Taip'); ?></button>
                <button class="oee-btn p-2 bg-grey" data-ng-click="swapCurrentPlanNo()"><?php echo __('Ne'); ?></button>
            </div>
        </div>

        <div data-ng-if="swapCurrentPlanYesPlans && !stateLoading && !swapCurrentPlanYesPlansYes">

            <div class="input-group mb-2" data-ng-if="allPlans.length">
                <label class="input-group-text" for="inputGroupSelect01"><?php echo __('Planai'); ?></label>
                <select class="form-select" id="inputGroupSelect01" ng-model="newPlan" ng-change="selectNewPlan(newPlan.Plan)"
                    ng-options="plan.Plan.name  for plan in allPlans"
                >
                    <option value="" disabled selected hidden><?php echo __('-- Nepasirinktas --'); ?></option>
                </select>
            </div>

            <div class="input-group mb-2">
                <label class="input-group-text" for="inputGroupSelect02"><?php echo __('Pasirinktas produktas'); ?></label>
                <select class="form-select" id="inputGroupSelect02" ng-change="selectNewProduct(newProduct)" ng-model="newProduct"
                    ng-options="product.Product.name + ' (' + product.Product.production_code + ')'  for product in products"
                >
                    <option value="" disabled selected hidden><?php echo __('-- Nepasirinktas --'); ?></option>
                </select>
            </div>

            <form method="POST">
                <?php $this->App->printAdditionalFields($structure, $this->Form); ?>
            </form>

            <div class="d-flex gap-2 text-white">
                <button class="oee-btn p-2 bg-green" data-ng-click="swapCurrentPlanConfirm()"><?php echo __('Patvirtinti'); ?></button>
                <button class="oee-btn p-2 bg-gray" data-ng-click="swapCurrentPlanNo()"><?php echo __('Uždaryti'); ?></button>
            </div>
        </div>

        <div data-ng-if="swapCurrentPlanYesPlansYes && !stateLoading">

            <h3><?php echo __('Ar tikrai keisti?'); ?></h4> 
            <p><?php echo __('Keičiama iš'); ?>: <b>{{currentPlan.Plan.production_name}}</b></p>
            <p data-ng-if="sensor.working_on_plans_or_products == 1"><?php echo __('Keičiama į'); ?>: <b>{{newProduct.Product.name}}</b></p>
            <p data-ng-if="sensor.working_on_plans_or_products == 0"><?php echo __('Keičiama į'); ?>: <b>{{newPlan.production_name}}</b></p>

            <div class="d-flex gap-2 text-white">
                <button class="oee-btn p-2 bg-green" data-ng-click="swapCurrentPlanConfirmYes()"><?php echo __('Taip'); ?></button>
                <button class="oee-btn p-2 bg-grey" data-ng-click="swapCurrentPlanNo()"><?php echo __('Ne'); ?></button>
            </div>
        </div>

    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="true" class="modal fade" id="startPlanModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content p-3" style="max-width: 600px;">

        <div data-ng-if="!prodConfirmStartWaitingConfirmation">
            <h3 class="modal-title modal-title-for-blind"><?php echo __('Kas dabar gaminama'); ?>?</h3>
            <!--div data-ng-if="!products.length">
                Nėra produktų.
            </div-->
            <div data-ng-if="products.length || sensor.working_on_plans_or_products == 1">
                <div><?php echo __('Įvykio data laikas'); ?>: {{event.dateTime | date: 'yyyy-MM-dd HH:mm'}}</div>

                <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
                
                <ul class="list-group pt-3" id="additionalFieldsErrors" data-ng-if="additionalFieldsErrors.length">
                    <li class="list-group-item bg-red text-white"><?php echo __('KLAIDA'); ?></li>
                    
                    <li class="list-group-item" data-ng-repeat="error in additionalFieldsErrors">{{error}}</li>
                    <li class="list-group-item">kazkoks klaidos tekstas</li>
                    <li class="list-group-item">kazkoks klaidos tekstas</li>
                    <li class="list-group-item">kazkoks klaidos tekstas</li>
                </ul>
                
                
                
                <div class="mt-4" data-ng-if="!stateLoading">

                    <div>
                        <h4><?php echo __('Produktai'); ?></h4>
                        <!-- <h5><?php echo __('Pasirinktas produktas'); ?>: <b>{{ currentPlan ? currentPlan.Plan.production_name +" (" + currentPlan.Plan.production_code +")" : '&mdash;'}}</b></h5> -->
                        <div class="input-group mb-2">
                            <label class="input-group-text" for="inputGroupSelect03"><?php echo __('Naujas produktas'); ?></label>
                            <select class="form-select" id="inputGroupSelect03" ng-change="selectNewProduct(newProduct)" ng-model="newProduct"
                                ng-options="product.Product.name + ' (' + product.Product.production_code + ')'  for product in products"
                            >
                                <option value="" disabled selected hidden><?php echo __('-- Nepasirinktas --'); ?></option>
                            </select>
    
                        </div>
                        <button class="text-white bg-green oee-btn w-100 p-2" data-ng-click="prodConfirmStartConfirm(1)"><?php echo __('Patvirtinti produktą'); ?></button>
    
                        <form method="POST">
                            <?php $this->App->printAdditionalFields($structure, $this->Form); ?>
                        </form>
                    </div>
                    
                    <div data-ng-if="allPlans.length || sensor.working_on_plans_or_products == 0" class="mt-4">
                        <h4><?php echo __('Planai'); ?></h4>
                        <div class="input-group mb-2">
                            <label class="input-group-text" for="inputGroupSelect04"><?php echo __('Naujas planas'); ?></label>
                            <select class="form-select" id="inputGroupSelect04" ng-model="newPlan" ng-change="selectNewPlan(newPlan.Plan)"
                                ng-options="plan.Plan.name  for plan in allPlans"
                            >
                                <option value="" disabled selected hidden><?php echo __('-- Nepasirinktas --'); ?></option>
                            </select>
    
                        </div>
                        <button type="button" class="oee-btn text-white bg-green w-100 p-2" data-ng-click="prodConfirmStartConfirm(0)"><?php echo __('Patvirtinti planą'); ?></button>
                        
                        <form method="POST">
                            <?php $this->App->printAdditionalFields($structure, $this->Form); ?>
                        </form>
                    </div>

                </div>

                <div class="p-4"></div>

                <button type="button" class="oee-btn bg-grey text-white py-2 px-5 float-right" data-ng-click="idleContinue()"><?php echo __('Uždaryti'); ?></button>
            </div>
        </div>

        <div data-ng-if="!stateLoading && prodConfirmStartWaitingConfirmation">
            <h5 data-ng-if="newProduct != null"><?php echo __('Pasirinktas produktas'); ?>: <b>{{newProduct.Product.name + " (" + newProduct.Product.production_code +")"}}</b></h5>
            <h5 data-ng-if="newPlan != null"><?php echo __('Pasirinktas planas'); ?>: <b>{{newPlan.name}}</b></h5>

            <div class="d-flex gap-2 text-white">
                <button class="oee-btn p-2 bg-green" data-ng-click="prodConfirmStart()"><?php echo __('Patvirtinti'); ?></button>
                <button class="oee-btn p-2 bg-gray" data-ng-click="idleContinue()"><?php echo __('Uždaryti'); ?></button>
            </div>
        </div>
     
    </div>
</div>


<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->
<!-- DONE -->

<div class="modal fade" tabindex="-1" id="endPlanModal"  data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content fw-bold p-3 gap-3">

        <div>
            <div class="p-2 pl-3 mb-2 fs-5 oee-bg-main"><?php echo __('Kas atsitiko?'); ?></div>
            <div class="oee-bg-main p-2 text-dark"><span class="text-uppercase"><?php echo __('Gaminama'); ?></span>: {{currentPlan ? currentPlan.Plan.production_name : '&mdash;'}}</div>
        </div>

        <div class="oee-center" data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>

        <div class="text-white d-grid gap-3" data-ng-if="(orderPostphoneCountdown === 0 && stopProdState === 0 && orderEndVar !== 1 && !stateLoading)">
            <?php if(!isset($pluginFunctionality['dismissWorkCenterElements']['prodPostphoneCountdownConfirm']) ){ //gali buti neisvedamas jei yra per plugina ismetamas ?>
                <button class="oee-btn p-3 bg-orange" data-ng-if="endDlg.currentPlan" data-ng-click="prodPostphoneCountdownConfirm()"><?php echo __('Gamyba atidėta'); ?></button>
            <?php } ?>
            <button class="oee-btn p-3 bg-green" data-ng-if="endDlg.currentPlan" data-ng-click="orderEnd()" data-ng-hide="endDlg.autoEvent"><?php echo __('Gamyba baigta'); ?></button>
            <button class="oee-btn p-3 bg-red" data-ng-if="endDlg.event.type != -9999" data-ng-click="prodRegisterIssue()"><?php echo __('Registruoti prastovos priežastį'); ?></button>
            <button class="oee-btn p-3 bg-yellow" data-ng-if="(!endDlg.currentPlan || sensor.allow_transition_during_work) && (endDlg.event.foundProblemId <= 0 || endDlg.event.problemId == 1 || accessesToMethods.changeProblemToTransition)" data-ng-click="sendRegisterIssue({Problem: {id: 1}, children: {}}, <?php echo $choosePlanAfterTransition > 0?'true':'false'; ?>)"><?php echo __('Perėjimas'); ?></button>
            <button class="oee-btn p-3 bg-grey" data-ng-if="((!endDlg.currentPlan && accessesToMethods.changeProblemToNoWork) || (endDlg.currentPlan && foundProblem.FoundProblem.id > 0 && sensor.allow_nowork_during_work))" data-ng-click="sendRegisterIssue({Problem: {id: 2}, children: {}}, false)"><?php echo $noWorkProblem['Problem']['name']; ?></button>
        </div>

        <div class="text-white d-grid gap-2" data-ng-if="(orderEndVar === 1 && orderPostphoneCountdown === 0 && orderEndCountdown !== 1 && stopProdState < 2)">
            <div class="oee-bg-main p-2 text-dark"><?php echo __("Ar tikrai norite pabaigti planą?"); ?></div>
            <div class="d-flex gap-2 text-white">
                <button class="oee-btn p-3 bg-green" data-ng-click="orderEndYes()" data-ng-hide="endDlg.autoEvent"><?php echo __('Taip'); ?></button>
                <button class="oee-btn p-3 bg-gray" data-ng-click="orderEndNo()" data-ng-hide="endDlg.autoEvent"><?php echo __('Ne'); ?></button>
            </div>
        </div>

        <div class="text-white d-grid gap-2" data-ng-if="(orderPostphoneCountdown === 1)">
            <div class="oee-bg-main p-2 text-dark"><?php echo __("Ar tikrai atidėti užsakymą?"); ?></div>
            <div class="d-flex gap-2 text-white">
                <button class="oee-btn p-3 bg-green" data-ng-click="prodPostphoneCountdown()"><?php echo __('Taip'); ?></button>
                <button class="oee-btn p-3 bg-gray" data-ng-click="orderEndNo()"><?php echo __('Ne'); ?></button>
            </div>
        </div>

        <div data-ng-if="(orderPostphoneCountdown === 2 || orderEndCountdown === 1)">
            <h4 style="font-size:40px;text-align:center;">
                <b><?php echo __("Nevykdykite gamybos"); ?></b><br><small><?php echo __('Iki užsakymo atidėjimo') ?>: <b>{{ counter }}</b></small>
            </h4>
            <img src="<?php echo $baseUri; ?>img/stop.png" alt="" style="display:block; margin:10px auto" />
        </div>

        <div data-ng-if="(orderPostphoneCountdown === 3)" class="text-white d-grid gap-3 mb-3">
            <?php
            if($wc != 0) {
                if(empty($pluginFunctionality['afterProductionProblems']) || $pluginFunctionality['afterProductionProblems'] == null) { ?>
                    <button class="oee-btn p-3 bg-yellow" data-ng-click="prodPostphone(false,false,1)"><?php echo __('Perėjimas'); ?></button>
                    <button class="oee-btn p-3 bg-blue" data-ng-click="prodPostphone(false,false,2)"><?php echo $noWorkProblem['Problem']['name']; ?></button>
                <?php }
                foreach ($pluginFunctionality['afterProductionProblems'] as $aprodp) {
                    $class = 'bg-blue';
                    if($aprodp['problem_id'] == 1) { $class = 'bg-orange'; }
                    if($aprodp['problem_id'] == 2) { $class = 'bg-gray'; }
                    ?>
                    <button class="oee-btn p-3 <?php echo $class; ?>" data-ng-click="prodPostphone(false,false,<?php echo $aprodp['problem_id'] ?>)"><?php echo $aprodp['name'] ?></button>
                <?php }
            }
            ?>
        </div>

        <div class="text-white mt-4" data-ng-if="(orderEndVar !== 1 && orderEndCountdown !== 1 && stopProdState === 0 && orderPostphoneCountdown === 0)">
            <button class="py-2 px-4 bg-gray d-inline float-right oee-btn" data-dismiss="modal" data-ng-click="idleIssue()" data-ng-hide="endDlg.autoEvent"><?php echo __('Uždaryti'); ?></button>
        </div>

        <!-- stop production states -->
        <div data-ng-if="(stopProdState === 1 && !stateLoading)">
            <h3>{{deeperProblemEntered?'<?php echo __('Priežastys'); ?>':'<?php echo __('Pasirinkti tipą'); ?>'}}</h3>

            <p class="m-0 text-muted"><?php echo __('Papildomas komentaras apie prastovą'); ?></p>
            <div class="input-group mb-2"> 
                <textarea class="form-control" data-ng-model="$parent.problemComment" style="min-height: 4rem;"></textarea>

                <span class="input-group-text p-0 text-wrap lh-sm" style="flex: 0">
                    <button class="oee-btn w-100 h-100 oee-bg-main p-2" data-ng-click="sendRegisterIssue({Problem:{id:'send_only_comment'}, children: []}, false)"><?php echo __('Išsaugoti komentarą'); ?></button>
                </span>
            </div>

       
            <div class="big-btn-menu split_downtime_controll" data-ng-if="endDlg.event.foundProblem.state != 1 && accessesToMethods.allowDowntimeSplit">
                <table class="table table-bordered table-condensed table-striped">
                    <thead class="oee-bg-dark text-white"><tr>
                        <th><?php echo __('Prastovos pradžia / pabaiga'); ?></th>
                        <th><?php echo __('Trukmė'); ?></th>
                        <th style="width: 20%;"><?php echo __('Kiek minučių atkirpti?'); ?></th>
                        <th><?php echo __('Atkirpti nuo'); ?></th>
                        <th><?php echo __('Naujos prastovos pradžia/pabaiga'); ?></th>
                    </tr></thead>
                    <tbody><tr>
                        <td>{{endDlg.event.foundProblem.oldStart}}<br />{{endDlg.event.foundProblem.oldEnd}}</td>
                        <td><span data-ng-if="endDlg.event.foundProblem.newDuration">{{endDlg.event.foundProblem.newDuration}}</span><span data-ng-if="!endDlg.event.foundProblem.newDuration">{{endDlg.event.foundProblem.duration}}</span></td>
                        <td style="width: 20%;"><input class="form-control" type="text" min="0" data-ng-model="$parent.$parent.downtimeCutDuration" ng-change="calculateDowntimesAfterCut()"  /></td>
                        <td>
                            <input type="radio" name="split_downtime" value="0" data-ng-model="$parent.$parent.downtimeCutType" ng-change="calculateDowntimesAfterCut()" id="splitDowntimeAtStart" />&nbsp;<label for="splitDowntimeAtStart"><?php echo __('Pradžios'); ?></label><br />
                            <input type="radio" name="split_downtime" value="1" data-ng-model="$parent.$parent.downtimeCutType" ng-change="calculateDowntimesAfterCut()" id="splitDowntimeAtEnd" />&nbsp;<label for="splitDowntimeAtEnd"><?php echo __('Pabaigos'); ?></label>
                        </td>
                        <td>{{endDlg.event.foundProblem.newStart}}<br />{{endDlg.event.foundProblem.newEnd}}</td>
                    </tr></tbody>
                </table>
            </div>

            <div class="d-flex flex-wrap gap-2 problem-buttons">
                <button style="flex: 0 calc(33.3% - .33rem); border: 0.5em solid red; font-size: 18px;" class="oee-btn px-1 py-3 {{problemType.Problem.transition_problem?'transition':''}}" data-ng-repeat="problemType in workingProblemTypes" data-ng-click="sendRegisterIssue(problemType, false)">
                    <strong>{{problemType.Problem.name}}</strong>&nbsp;<i class="iconfa-arrow-down float-right pt-1 pr-1" data-ng-if="problemType.children.length"></i>
                    <!-- {{problemType}} -->
                </button>
            </div>

            <div class="py-2"></div>

            <div class="text-white">
                <button class="py-2 px-5 bg-gray d-inline float-right oee-btn" data-ng-click="backProdEnd()"><?php echo __('Grįžti'); ?></button>
            </div>
        </div>

        <div data-ng-show="(stopProdState === 2 && !stateLoading)">

            <div data-ng-show="sensor.time_interval_declare_count_on_finish">
                <div id="declareQuantitiesBlockOnFinish">
                    <h3><?php echo __('Pagamintas kiekis'); ?> <span class="fs-5 float-right m-1" data-ng-if="currentPlan.Plan.quantity > 0"><?php echo __('Plano kiekis'); ?> {{currentPlan.Plan.quantity}}</span></h3>

                    <div class="input-group mb-2">
                        <span class="w-100">{{sensor.declare_using_crates_and_pieces?sensor.declare_using_crates_and_pieces:(sensor.declare_quantities_label?sensor.declare_quantities_label:'<?php echo __('Kiekis'); ?>')}}</span>
                        <input class="form-control" type="text" oninput="numbersOnly(this)" data-ng-model="partialQuantityFinish" data-ng-click="emptyIfZero('partialQuantityFinish')"/>
                        <span class="input-group-text"><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantity}}</span>
                    </div>

                    <div class="input-group mb-2" data-ng-show="sensor.declare_using_crates_and_pieces">
                        <span class="w-100"><?php echo __('Vienetai'); ?></span>
                        <input class="form-control" type="text" oninput="numbersOnly(this)" data-ng-model="partialQuantityPiecesFinish" />
                        <span class="input-group-text"><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantityPieces}}</span>
                    </div>

                </div>
            </div>

            <div data-ng-if="lossTypes.length > 0" id="declareLossesBlockOnFinish">
                <h3 class="mt-4"><?php echo __('Įveskite nuostolius'); ?></h3>

                <div class="input-group mb-2" data-ng-repeat="lossType in lossTypes">
                    <span class="w-100">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</span>
                    <input class="form-control" type="text" oninput="numbersOnly(this)" data-ng-model="lossType.LossType.value" />
                    <span class="input-group-text">{{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}} {{lossType.LossType.unit}}</span>
                </div>

                <div class="m-3">
                    <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
                </div>
            </div>

            <div class="py-2"></div>

            <div class="text-white">
                <button class="py-2 px-4 bg-gray d-inline float-right oee-btn" data-ng-click="nextProdComplete(partialQuantityFinish)"><?php echo __('Išsaugoti'); ?></button>
            </div>

        </div>

        <div data-ng-if="(stopProdState === 3 && !stateLoading)">
            <div class="text-white d-grid gap-3">
                <?php
                if($wc != 0) {
                    //if(empty($pluginFunctionality['afterProductionProblems']) || $pluginFunctionality['afterProductionProblems'] == null) { ?>
                        <button class="oee-btn p-3 bg-red" data-ng-click="prodRegisterIssueAfterProdEnd()"><?php echo __('Registruoti prastovos priežastį'); ?></button>
                        <button class="oee-btn p-3 bg-yellow" data-ng-click="sendProdComplete(1, <?php echo $choosePlanAfterTransition > 0?'true':'false'; ?>);"><?php echo __('Perėjimas'); ?></button>
                        <button class="oee-btn p-3 bg-gray" data-ng-click="sendProdComplete(2, false)"><?php echo $noWorkProblem['Problem']['name']; ?></button>
                    <?php //}
                    foreach ($pluginFunctionality['afterProductionProblems'] as $aprodp) {
                        $class = 'bg-blue';
                        if($aprodp['problem_id'] == 1) {
                            continue;
                            $class = 'bg-orange';
                        }
                        if($aprodp['problem_id'] == 2) {
                            continue;
                            $class = 'bg-gray';
                        }
                        ?>
                        <button class="oee-btn p-3 <?php echo $class; ?>" data-ng-click="sendProdComplete(<?php echo $aprodp['problem_id'] ?>, <?php echo $choosePlanAfterTransition > 0 && $aprodp['problem_id']==1?'true':'false'; ?>)"><?php echo $aprodp['name'] ?></button>
                    <?php }
                }
                ?>
            </div>
        </div>

    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="true" class="modal fade" id="speedProblemModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content p-3">

        <h3 class="modal-title modal-title-for-blind"><?php echo __('Kodėl mažas greitis?'); ?></h3>

        <div><?php echo __('Įvykio data laikas'); ?>: {{event.dateTime | date: 'yyyy-MM-dd HH:mm'}}</div>
        <div data-ng-if="stateLoading"><img src="/images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
        <div data-ng-if="!stateLoading">
            <h4><?php echo __('Pasirinkti tipą'); ?></h4>

            <div class="d-flex flex-column gap-2 text-white fw-bold text-uppercase">
                <button class="oee-btn p-2 bg-red" data-ng-repeat="problemType in speedProblemTypes" data-ng-click="sendRegisterSpeedIssue(problemType)">{{problemType.Problem.name}}</button>
            </div>

        </div>
    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="true" class="modal fade" id="verificationModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header"><h4 class="modal-title modal-title-for-blind"><?php echo __('Gaminio patikra'); ?></h4></div>
            <div class="modal-body">
                <div class="well well-sm text-center">
                    <div style="font-size: 40px; line-height: 40px;">
                        <?php echo $this->Html->image('./../images/warning_sign.png',array('height'=>36)).'&nbsp;&nbsp;'; ?>
                        <p ng-bind-html="verification.text?verification.text:'<?php echo __('Atlikite detalių patikrą'); ?>'"></p>
                    </div>

                    <br /><br />
                    <button class="btn btn-primary" data-ng-click="registerVerification()"><?php echo __('Atlikta'); ?></button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="true" class="modal fade" id="informationModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-content p-3 <?php echo $sensorData['Sensor']['time_interval_declare_count_button'] == 2?'large':''; ?>">

        <h3 class="modal-title modal-title-for-blind"><?php echo __('Informacija'); ?></h3>

        <?php if($sensorData['Sensor']['time_interval_declare_count_button'] == 2){echo '<div class="row-fluid"><div class="col-lg-6">'; } ?>
            <div class="well well-sm text-left">
                <div id="informationText"></div>
                <br>
                <label><?php echo __('Komentaras'); ?></label>
                <textarea class="form-control" data-ng-model="informationComment" style="max-width: 100%; width: 100%; height: 6em;"></textarea>
                <div class="d-flex gap-2 text-white fw-bold mt-4">
                    <button class="oee-btn py-2 px-4 bg-blue" data-ng-click="saveAndCloseInformationModal(informationFoundProblemId)"><?php echo __('Išsaugoti komentarą'); ?></button>
                    <button class="oee-btn py-2 px-4 bg-blue" data-ng-click="closeInformationModal()"><?php echo __('Uždaryti'); ?></button>
                </div>
            </div>
        <?php if($sensorData['Sensor']['time_interval_declare_count_button'] == 2): ?>
        </div>
        <div class="col-lg-6">
            <div><h3><?php echo __('Įveskite nuostolius'); ?></h3></div><br />
            <div class="form-group" data-ng-repeat="lossType in lossTypes">
                <label class="control-label col-xs-9">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</label>
                <div class="col-xs-3">
                    <div class="input-group">
                        <input class="form-control input-default" type="text" oninput="numbersOnly(this)" data-ng-model="lossType.LossType.value" />
                        <span class="input-group-addon">{{lossType.LossType.unit}}</span>
                    </div>
                    {{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}}
                </div>
            </div>
            <br class="clear" />
            <p class="text-right">
                <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
            </p>
            <button style="width: 100%;" type="button" class="btn btn-success btn-lg pull-right" data-ng-click="saveDeclaredQuantities(informationFoundProblemId)"><?php echo __('Patvirtinti nuostolius'); ?></button>
        </div>
        <?php endif; ?>
    </div>

</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->

<div aria-hidden="true" class="modal fade" id="lockScreenModal" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <img src="<?php echo $baseUri; ?>img/stop.png" alt="" />
            </div>
        </div>
    </div>
</div>

<!-- ---------------------------------------- -->
<!-- ---------------------------------------- -->