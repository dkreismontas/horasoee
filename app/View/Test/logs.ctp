<table class="table table-bordered">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Šaltinis'); ?></th>
        <th><?php echo __('Vartotojas'); ?></th>
        <th><?php echo __('Turinys'); ?></th>
        <th><?php echo __('Sukurta'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($list)): ?>
        <tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
    <?php endif; ?>
    <?php
    $palette = array("#d3d3d3","#a8a8a8");
    $current_index = 0;
    $current_group = "";
    foreach ($list as $li):
        if($li['Log']['group_stamp'] != $current_group) {
            $current_group = $li['Log']['group_stamp'];
            $current_index = 1 - $current_index;
        }
        $content = $li['Log']['content'];
        $pattern = "/{(.+?)};/";
        $content = preg_replace_callback(
            $pattern,
            function($match) {
                $m = ($match[0]);
//                $m = preg_replace('/"(.+?)":"(.+?)"/',"\"$1\":\"<span style='color:#00A100;'>$2</span>\"",$m);
                $m = str_replace(":{",":{<br>&nbsp;&nbsp;",$m);
                $m = str_replace(",",",<br>&nbsp;&nbsp;",$m);

//                $m = j($m);

                return "<span data-toggle=\"popover\" data-placement=\"bottom\" title=\"Content\" style=\"font-weight:bold;\" data-content='".($m)."'>DATA</span> ";
            },
            $content);
        ?>
        <tr style="background-color:<?php echo $palette[$current_index];?> !important;">
            <td><?php echo $li['Log']['id']; ?></td>
            <td><?php echo $li['Log']['source']; ?></td>
            <td><?php echo $li['Log']['user_id']; ?></td>
            <td><?php echo $content; ?></td>
            <td><?php echo $li['Log']['created_at']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<div><?php echo $this->App->properPaging(true, array()); ?></div>

<script>
    jQuery(document).ready(function($) {
        $('[data-toggle="popover"]').popover({
            html: true
        });
    });

</script>