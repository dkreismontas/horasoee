<div id="state-pane">OFF</div>
<!--<div>For sensor: --><?php //echo $sensorId; ?><!--</div>-->
<button class="btn btn-default" id="btn-start" type="button">Start</button>
<button class="btn btn-default" id="btn-stop" type="button">Stop</button>
<button class="btn btn-danger" id="btn-send" style="margin-left: 50px;" type="button">Send Now</button>
<br>
Record Values: <input class="form-control" style="width:120px;" type="text" id="recordValue"/>
Sensor ID: <input class="form-control" style="width:120px;" type="text" value="3" id="sensorId"/>
<br>
<ul>
    <li><b>-1</b> - send random 1-7</li>
    <li><b>>=0</b> - static value</li>
</ul>
<script type="text/javascript">
	$(function() {
		var ticker, statePane = $('#state-pane').eq(0);
		
		var tickerFunc = function() {
            console.log("ticker");
			$.get('<?php echo htmlspecialchars($tickerUrl); ?>',{'val':$("#recordValue").val(),'sensor':$("#sensorId").val()});
		};
		
		$('#btn-start').on('click', function() {
			if (ticker) clearInterval(ticker);
			ticker = setInterval(tickerFunc, 20000);
			statePane.text('ON');
		});
		
		$('#btn-stop').on('click', function() {
			if (ticker) clearInterval(ticker);
			ticker = null;
			statePane.text('OFF');
		});
		
		$('#btn-send').on('click', function() {
			tickerFunc();
		});
		
	});
</script>
