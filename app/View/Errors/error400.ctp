<?php
/* @var $this View */
?>
<h2><?php echo $message; ?></h2>
<?php if ($this->response->statusCode() == 403): ?>
	
<?php else: ?>
	<p class="error">
		<strong><?php echo __d('cake', 'Error'); ?>: </strong>
		<?php printf(
			__d('cake', 'The requested address %s was not found on this server.'),
			"<strong>'{$url}'</strong>"
		); ?>
	</p>
	<?php
	if (Configure::read('debug') > 0):
		echo $this->element('exception_stack_trace');
	endif;
	?>
<?php endif; ?>
