<div class="row-fluid">
    <div class="col-md-6">
        <?php
        echo $this->Form->create();
        echo $this->Form->input('id', array('type'=>'hidden'));
        echo $this->Form->input('name', array('type'=>'text', 'class'=>'form-control', 'label'=>__('Pavadinimas'), 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('sensors_ids', array('type'=>'select', 'class'=>'form-control chosen-select', 'label'=>false, 'multiple'=>true, 'options'=>$sensors, 'before'=>'<label class="col-md-12" style="padding:0">'.__('Darbo centras').'</label>'));
        echo $this->Form->submit(__('Išsaugoti'),array('class'=>'btn btn-info','div'=>false)).'&nbsp;';
        echo $this->Html->link(__('Atšaukti'), array('action'=>'display'),array('class'=>'btn btn-default'));
        echo $this->Form->end();
        ?>
    </div>
</div>

