<div class="row-fluid">
    <div class="col-md-6">
        <?php
        if($hasAccessToEdit){
            echo $this->Html->link(__('Sukurti naują peržiūrą'), array('controller'=>'work_center_6h', 'action'=>'edit'), array('class'=>'btn btn-info'));
        }
        ?>
    </div>
</div>
<div class="row-fluid clearfix">
    <div class="col-md-8">
        <table class="table table-bordered"><thead>
            <tr>
                <th><?php echo __('Pavadinimas'); ?></th>
                <th><?php echo __('Darbo centrų kiekis'); ?></th>
                <th><?php echo __('Veiksmai'); ?></th>
            </thead></tr>
            <tbody>
            <?php foreach($reviews as $review){ ?>
                <tr>
                    <td><?php echo $review['Wc6hReview']['name']; ?></td>
                    <td class="centeralign"><?php echo sizeof(explode(',',$review['Wc6hReview']['sensors_ids'])); ?></td>
                    <td><?php
                        echo $this->Html->link(__('Peržiūrėti'), array('action'=>'show', $review['Wc6hReview']['id']), array('target'=>'_blank','class'=>'btn btn-default')).'&nbsp;';
                        if($hasAccessToEdit){
                            echo $this->Html->link(__('Redaguoti'), array('action'=>'edit', $review['Wc6hReview']['id']), array('class'=>'btn btn-info')).'&nbsp;';
                        }
                        if($hasAccessToRemove){
                            echo $this->Html->link(__('Pašalinti'), array('action'=>'remove', $review['Wc6hReview']['id']), array('class'=>'btn btn-danger'),__('Ar tikrai norite pašalinti šį rinkinį'));
                        } ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
</div>
