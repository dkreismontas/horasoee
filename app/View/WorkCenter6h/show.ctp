<?php
$this->Html->script('vue.js?v=1',array('inline' => false));
$this->Html->css('work-center-6h.css?v=2',null,array('inline'=>false));
?>
<style type="text/css">
    .periodsBlock .hours .hour div.interval.problem-<?php echo $exceededTransitionId; ?>{
        background: #fb9c20;
    }
</style>
<div id="mainContainer">
    <div id="sensors">
        <div v-for="(columnNr, row_sensors) in data.sensors" style="height: {{100/data.sensors.length}}%; margin: 0;" class="row">
            <sensor_comp v-for="(sensor_id, sensor) in row_sensors" :sensor="sensor"></sensor_comp>
        </div>
    </div>
    <template id="sensorTpl">
        <div class="sensor sensor_{{sensor.id}} col-md-{{12/columnsCount}} col-sm-12 col-xs-12">
            <div class="headersBlocks">
                <h3><span><i></i></span>{{sensor.name|shortenString}}</h3>
                <h3><span><i></i></span>{{sensor.current_plan_name|shortenString}}</h3>
            </div>
            <div class="periodsBlock">
                <div class="legends">
                    <div style="width: {{hourNumberBlockWidth}}%"></div>
                    <div style="width: {{getIntervalsBlockWidth()}}%">
                        <div v-for="h in 61" v-if="h%5==0" class="hourLegend"><span>{{h}}</span></div>
                    </div>
                    <div style="width: {{infoBlockWidth}}%">
                        <div class="infoblock" v-for="infoblock in sensor.hours_info_blocks" style="width: {{getSingleBlockWidth(infoblock.addSpace)}}%">
                            {{infoblock.title}}
                        </div>
                    </div>
                </div>
                <div v-for="hour in sensor.periods" class="hours">
                    <div class="hour">
                        <div class="hourTitle" style="width: {{hourNumberBlockWidth}}%">{{hour.value}}</div>
                        <div class="intervalsContainer" style="width: {{getIntervalsBlockWidth()}}%">
                            <div v-for="interval in hour.periods" class="interval type-{{interval.type}} problem-{{interval.problemId}}" style="width: {{interval|getIntervalWidth}}%; left: {{interval|getLeftPos}}%;">
                                <div><span>{{interval.shortTitle}}</span></div>
                            </div>
                        </div>
                        <div style="width: {{infoBlockWidth}}%">
                            <div class="infoblock" v-for="infoblock in sensor.hours_info_blocks" style="width: {{getSingleBlockWidth(infoblock.addSpace)}}%">
                                {{infoblock.value[hour.value]}}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </template>
</div>



<script type="text/javascript">
    setInterval(function(){location.reload();}, 1800000);
    var vm = new Vue({
        el: '#sensors',
        data: {
            data: {},
        },
        methods: {
            loadData: function(){
                jQuery('#mainContainer').css('height', jQuery(window).height()+'px');
                jQuery.ajax({
                    url: "<?php echo $this->Html->url('/work_center6h/update_status/'.$id,true); ?>",
                    type: 'post',
                    context: this,
                    dataType: 'JSON',
                    async: true,
                    success: function(request){
                        this.data = request;
                    }
                })
            }
        },
        beforeCompile: function(){
            this.loadData();
            setInterval(this.loadData,500000);
        },
        components: {
            'sensor_comp': {
                template: '#sensorTpl',
                props: ['sensor'],
                data: function(){return { columnsCount:0, infoBlockWidth:null, hourNumberBlockWidth: 7.33, totalInfoBlocksWidthPx: 0}},
                beforeCompile: function(){
                    this.columnsCount = this.$parent.data.columns;
                    for(let i in this.sensor.hours_info_blocks){
                        this.totalInfoBlocksWidthPx += this.sensor.hours_info_blocks[i].addSpace;
                    }
                    let x = this.columnsCount;
                    if(window.innerWidth <= 1000){
                        x = this.columnsCount = 0.5;
                    }
                    this.infoBlockWidth = this.totalInfoBlocksWidthPx * x / window.innerWidth * 100;
                },
                methods: {
                    getIntervalsBlockWidth: function(){
                        return 100 - this.hourNumberBlockWidth - this.infoBlockWidth;
                    },
                    covertPxToPercentage: function(px){
                        return px / window.innerWidth * 100;
                    },
                    getSingleBlockWidth: function(blockWidthPx){
                        return blockWidthPx / this.totalInfoBlocksWidthPx * 100;
                    }
                },
                filters: {
                    shortenString: function(string){
                        return string.length > 25?string.substring(0, 25)+'...':string;
                    },
                    getIntervalWidth: function(interval){
                        return (interval.end - interval.start) / 3600 * 100 + 0.1;
                    },
                    getLeftPos: function(interval){
                        return interval.start / 3600 * 100;
                    },

                }
            }
        }
    });
</script>
