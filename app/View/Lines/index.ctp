<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<button class="btn btn-default" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Nauja linija'); ?></button>
<br /><br />
<div class="row-fluid">
    <div class="col-lg-8 col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><?php echo __('ID'); ?></th>
                    <th><?php echo __('Pavadinimas'); ?></th>
                    <th><?php echo __('PS ID'); ?></th>
                    <th><?php echo __('Industrinis'); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php if (empty($list)): ?>
                <tr><td colspan="4"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
                <?php endif; ?>
                <?php foreach ($list as $li_): $li = (object) $li_[$model]; $liBranch = (isset($li_[Branch::NAME]) ? ((object) $li_[Branch::NAME]) : null); ?>
                <tr>
                    <td><?php echo $li->id; ?></td>
                    <td><?php echo $li->name; ?></td>
                    <td><?php echo $li->ps_id; ?></td>
                    <td><?php echo $li->industrial ? __('Taip') : __('Ne'); ?></td>
                    <th>
                        <a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                        <a class="btn btn-default" href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
                    </th>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>