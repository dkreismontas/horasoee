<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('ps_id', array('label' => __('PS ID'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php //echo $this->Form->input('branch_id', array('label' => __('Padalinys'), 'options' => $branchOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('diff_card_auto_time', array('label' => __('Prastovos trukmė automatiniam NK kūrimui (min)'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<div class="checkbox">
    <label for="LineIndustrial">
        <?php echo $this->Form->input('industrial', array('label' => false, 'div' => null)); ?>
        <?php echo __('Industrinis'); ?>
    </label>
</div>
<!--div class="form-group">
    <label><?php echo __('Jutikliai, kurie priklauso linijai'); ?></label>
    <div>
        <span id="dualselect" class="dualselect">
            <input type="hidden" name="sensors" value="<?php echo htmlspecialchars($sensors); ?>" />
            <select class="form-control input-default select-inline" multiple="multiple" size="10"> 
                <?php if ($item): ?>
                    <?php foreach ($item[Sensor::NAME] as $li_): $li = (object) $li_; ?>
                <option value="<?php echo htmlspecialchars($li->id); ?>"><?php echo (isset($sensorOptions[$li->id]) ? $sensorOptions[$li->id] : $li->name); ?></option>
                    <?php endforeach; ?>
                <?php endif; ?>
            </select>
            <span class="ds_arrow">
                <button type="button" class="btn btn-inverse ds_prev"><i class="iconfa-chevron-left"></i></button>
                <br />
                <button type="button" class="btn btn-inverse ds_next"><i class="iconfa-chevron-right"></i></button>
            </span>
            <select class="form-control input-default select-inline" name="zz" multiple="multiple" size="10">
                <?php foreach ($sensorBList as $li_): $li = (object) $li_[Sensor::NAME]; $liBranch = (isset($li_[Branch::NAME]) ? ((object) $li_[Branch::NAME]) : null); ?>
                <option value="<?php echo htmlspecialchars($li->id); ?>"><?php echo (isset($sensorOptions[$li->id]) ? $sensorOptions[$li->id] : $li->name); ?></option>
                <?php endforeach; ?>
            </select>
        </span>
    </div>
    <script type="text/javascript"> $(function() { $('#dualselect').initDualSelect(); }); </script>
</div-->
<div>
    <br />
    <button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
    <a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript"> $(function() { $('input:checkbox, input:radio, .uniform-file').uniform(); }); </script>