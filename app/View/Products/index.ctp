<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-default" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas planas'); ?></button>
<br /><br /> */ ?>
<div class="clearfix pull-right">
    <!--form action="/products/import" method="POST" enctype='multipart/form-data'>
        <input style="float:left; margin-top:4px;" type="file" name="doc"/>
        <button class="btn btn-default"><?php echo __('Importuoti'); ?></button>
        <a class="btn btn-danger" href="<?php echo $this->Html->url('/',true).'files/import_products_example.xml' ?>" class="btn btn-default"><?php echo __('XML failo pavyzdys'); ?></a>
    </form-->
    <a class="btn btn-warning" href="<?php echo $this->Html->url('/',true).'products/edit' ?>"><?php echo __('Sukurti naują'); ?></a>
</div>
<br/>
<br/>
<table class="table table-bordered table-striped" id="productsTable">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Produktas'); ?></th>
        <th><?php echo __('Kodas'); ?></th>
        <th><?php echo __('Kiekis formoje'); ?></th>
        <th><?php echo __('Formų greitis (formos/h)'); ?></th>
        <th><?php echo __('Gamybos ciklas (s)'); ?></th>
        <th><?php echo __('Darbo centrai'); ?></th>
        <!--th><?php echo __('Galimi darbo centrai (jei nenurodyta galimi visi)'); ?></th-->
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($list)): ?>
        <tr>
            <td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($list as $li_):
        $li = (object) $li_["Product"]; ?>
        <tr>
            <td><?php echo $li->id; ?></td>
            <td><?php echo $li->name; ?></td>
            <td><?php echo $li->production_code; ?></td>
            <td><?php echo $li->kiekis_formoje; ?></td>
            <td><?php echo $li->formu_greitis; ?></td>
            <td><?php echo $li->production_time; ?></td>
            <td><?php
                $idsList = explode(',', $li->work_center);
                if(empty($li->work_center)){ echo __('Visi darbo centrai'); }
                else echo implode('<br />', array_filter($sensorsList, function($sensorId)use($idsList){return in_array($sensorId, $idsList); }, ARRAY_FILTER_USE_KEY));
            ?></td>
            <!--td><?php echo $li->work_center; ?></td-->
            <td class="full_width">
                <a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span
                        class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a') ?></a>
                <a class="btn btn-default" href="<?php printf($removeUrl, $li->id); ?>"
                   onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');"
                   title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
    jQuery('#productsTable').dataTable({
        "sPaginationType": "full_numbers",
        //"bScrollInfinite" : true,
        "aLengthMenu": [[100, 200, 500, -1], [100, 200, 500, "<?php echo __('Visi'); ?>"]],
        "aaSorting": [],
        "bScrollCollapse" : true,
        "sScrollY" : "800px",
        "iDisplayLength": -1,
        "paging": true,    
        "scrollY": 500,
        "oLanguage":{
            'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
            'sSearch':'<?php echo __('Paieška');?>',
            'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
            'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
            'sInfoEmpty':"",
            'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
            'oPaginate':{
                'sFirst': '<?php echo __('Pirmas');?>',
                'sPrevious': '<?php echo __('Ankstesnis');?>',
                'sNext': '<?php echo __('Kitas');?>',
                'sLast': '<?php echo __('Paskutinis');?>'
            }
        },
        "oSearch": {"sSearch": "<?php echo $this->Session->read('FilterText.'.$this->params['controller']); ?>"}
    });
    $('.maincontentinner').on('keyup','#productsTable_filter input', function(){
        $.ajax({
            url: '<?php echo Router::url(array('controller'=>'settings','action'=>'set_filter_text'),true); ?>',
            type: 'POST',
            data: {filter: 'FilterText.<?php echo $this->params['controller']; ?>', value: $(this).val()}
        });
    });
</script>
<?php
    $this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>