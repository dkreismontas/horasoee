<?php if(!empty($errors)) : ?>
    <div class="alert alert-danger">
        <ul>
            <?php foreach($errors as $err) {?>
                <li><?php echo $err;?></li>
            <?php } ?>
        </ul>
    </div>

<?php endif; ?>
<?php echo $this->Form->create('Plan', array('url' => '/plans/create', 'autocomplete' => 'off')); ?>
<table class="table table-bordered table-condensed table-striped table-proptable">
    <tr>
        <td><label><?php echo __('Linija'); ?></label></td>
        <td>
            <select name="data[Plan][line_id]">
                <?php foreach ($lineOptions as $lid => $li): ?>
                <option value="<?php echo $lid;?>"><?php echo $li; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><label><?php echo __('Darbo centras'); ?></label></td>
        <td>
            <select name="data[Plan][sensor_id]">
                <?php foreach ($sensorOptions as $lid => $li): ?>
                    <option value="<?php echo $lid;?>"><?php echo $li; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>
    <tr>
        <td><label><?php echo __('Tešlos kodas'); ?></label></td>
        <td><input type="text" name="data[Plan][paste_code]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('Gaminio pavadinimas'); ?></label></td>
        <td><input type="text" name="data[Plan][production_name]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('Gaminio kodas'); ?></label></td>
        <td><input type="text" name="data[Plan][production_code]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('MO Numeris'); ?></label></td>
        <td><input type="text" name="data[Plan][mo_number]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('Pradžia'); ?></label></td>
        <td><input class="datepicker" type="text" name="data[Plan][start]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('Pabaiga'); ?></label></td>
        <td><input class="datepicker" type="text" name="data[Plan][end]"/></td>
    </tr>
    <tr>
        <td><label><?php echo __('Kiekis'); ?></label></td>
        <td><input type="text" name="data[Plan][quantity]"/></td>
    </tr>
    <tr><td><label><?php echo __('Linijų kiekis'); ?></label></td><td><input type="text" name="data[Plan][line_quantity]"/></td></tr>
    <tr><td><label><?php echo __('Greitis'); ?></label></td><td><input type="text" name="data[Plan][step]"/></td></tr>
    <tr><td><label><?php echo __('Dėžių kiekis'); ?></label></td><td><input type="text" name="data[Plan][box_quantity]"/></td></tr>
    <tr><td><label><?php echo __('Pakuočių kiekis'); ?></label></td><td><input type="text" name="data[Plan][package_quantity]"/></td></tr>
    <tr><td><label><?php echo __('Kontrolė'); ?></label></td><td><input type="text" name="data[Plan][control]"/></td></tr>
    <tr><td><label><?php echo __('Komentaras'); ?></label></td><td><input type="text" name="data[Plan][comment]"/></td></tr>
    <tr><td><label><?php echo __('Svoris'); ?></label></td><td><input type="text" name="data[Plan][weight]"/></td></tr>
    <tr><td><label><?php echo __('Išeiga'); ?></label></td><td><input type="text" name="data[Plan][yield]"/></td></tr>
    <tr><td><label><?php echo __('Kiekis'); ?></label></td><td><input type="text" name="data[Plan][qty_1]"/></td></tr>

</table>
<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
<?php echo $this->Form->end(); ?>
<?php echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon'); ?>
<script type="text/javascript" charset="utf-8">
    jQuery("document").ready(function () {
        jQuery(".onlydatepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
        jQuery(".datepicker").datetimepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>