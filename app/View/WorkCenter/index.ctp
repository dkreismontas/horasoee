<script type="text/javascript"> 
    window.currentLang = '<?php echo current(explode('_',$currentLang)); ?>';
    window.sensorId = '<?php echo $wc; ?>';
    window.baseUri = '<?php echo $baseUri; ?>';
    window.baseUriDc = '<?php echo (isset($dc) && $dc) ? $dc : ''; ?>';
    window.allowChangeProcess = '<?php echo (isset($allowChangeProcess) && $allowChangeProcess) ? $allowChangeProcess : ''; ?>';
    window.is_operator = <?php echo (int)$sensorData['User']['sensor_id'] > 0?'true':'false'; ?>;
    var text_what_you_produce = '<?php echo __('Parinkite ką gaminate') ?>';
    var text_confirm_shift_change = '<?php echo __('Ar tikrai norite pakeisti pamainą?') ?>';
    <?php echo isset($pluginScripts)?$pluginScripts:'';
    $requiredInputFields = array();
    foreach($structure as $fieldName => $structureField){
        if(!is_array($structureField) || !isset($structureField['required']) || !$structureField['required']){ continue; }
        $requiredInputFields[$fieldName]=$structureField['required'];
    }
    ?>
	var requiredInputTexts = <?php echo json_encode($requiredInputFields); ?>;
</script>

<div class="w-100 h-100 oee-center position-fixed z-3" style="background: white; transition: all 0.2s;" id="foreground">
	<div class="spinner-border text-dark" role="status" style="border-radius: 50%; height: 64px; width: 64px;">
		<span class="visually-hidden"></span>
	</div>
</div>

<main class="w-100 user-group-id-<?php echo $user->group_id; ?>" data-ng-app="WorkCenter" data-ng-controller="WorkCenterCtrl">
	<header class="container-fluid p-1 bg-white oee-center fs-5 fw-bold">
		<a href="/"><img src="/images/icons/horasoee-logo.png" style="height: 1.5em"></a>
		<div class="f-0 text-left mx-2 text-nowrap header_title" style="font-size: 1.1em">Horas OEE</div>
		<div class="f-10 text-center lh-sm">
			<span class=""><?php echo __('Darbo centras');?>:</span> {{ sensor.name }} 
			<span class=""> | </span>
			<span class=""><?php echo __('Vykdomas planas');?>:</span> {{ currentPlan ? currentPlan.Plan.production_name +" (" + currentPlan.Plan.production_code +")" : '&mdash;'}}
		</div>

		<div class="f-0 text-right d-flex justify-content-end align-items-center  header_btns">

			<!-- Work center selection -->
			<div class="mr-2 position-relative" data-ng-if="availableSensors.length > 1"> <!-- help: removed classes: small-sell small-sell-for-blind -->
				<button class="oee-btn dropdown-toggle bg-white" type="button" data-toggle="dropdown" >
					<?php echo __('Pakeisti darbo centrą'); ?>
				</button>

				<ul class="dropdown-menu" style="min-width: 100%;">
					<li data-ng-repeat="availableSensor in availableSensors">
						<a class="dropdown-item" href="<?php echo Router::url(array('controller'=>'WorkCenter','action'=>'changeActiveSensorId')); ?>/{{availableSensor.Sensor.id}}">{{availableSensor.Sensor.name}}</a>
					</li>
				</ul>
			</div>

			<!-- help: 
				 <a data-ng-if="availableSensors.length == 1" class="btn btn-default" style="margin-bottom: 5px;" href="<?php echo Router::url(array('controller'=>'WorkCenter','action'=>'changeActiveSensorId')); ?>/{{availableSensors[0].Sensor.id}}">{{availableSensors[0].Sensor.name}}</a>
			-->

			<!-- Language selection -->
			<div class="d-flex position-relative"> 
				<button class="oee-btn dropdown-toggle bg-white" type="button" data-toggle="dropdown" >
					<img class="border border-dark" src="<?php echo Router::url("/files/flags/{$user->language}.png"); ?>" />
				</button>

				<ul class="dropdown-menu" style="min-width: 100%;">
					<?php foreach($possibleLanguages as $slug => $langText): ?>
						<li class="d-flex bg-white hover"><a class="dropdown-item pl-1" href="<?php echo Router::url(array('controller'=>'users', 'action'=>'change_language', $slug)); ?>">
							<img class="border border-dark ml-1" src="<?php echo Router::url("/files/flags/$slug.png"); ?>" />&nbsp;<?php echo $langText; ?>
						</a></li>
					<?php endforeach; ?>
				</ul>
			</div>
			
			<!-- Print Button -->
			<span class="pl-2 pr-2">
				<img 
					src="/images/svgs/print.svg" 
					class="oee-btn p-1" 
					style="height: 1.5em"
					title="<?php echo __('Spausdinant puslapį pirmą kartą, spausdinimo puslapio peržiūros lange (\'Print preview\') rekomenduojama susikonfiguruoti spausdinimo nuostatas. Optimalus atvaizdavimo dydis A4 formatui - 70%.'); ?>"
					data-ng-click="printPage()"
				>
			</span>
			
			<!-- User -->
			<span class="text-nowrap">
				<span class="text-uppercase"><?php echo __('Vartotojas'); ?></span>: <?php echo trim($user->first_name.' '.$user->last_name)?$user->first_name.' '.$user->last_name:$user->username; ?>
			</span>
			
			<a class="d-flex align-items-center ml-2 mr-2" href="<?php echo htmlspecialchars($logoutUrl); ?>" title="<?php echo __('Atsijungti'); ?>">
				<img  src="/images/svgs/sign-out.svg" style="height: 1.2em"> 
			</a>

		</div>
		
		<div class="header_menu_btn">
			<img src="/images/svgs/user.svg" style="height: 1.5em; padding: 2px;" class="mr-1 oee-btn" data-toggle="modal" data-target="#user-menu-drawer">
		</div>
	

	</header>

	<section class="row g-0" >

		<!-- ROW 1 -->
		<div class="grid_tb-modal-btn upper-section-h   col-md-1 col-2">
			<div class="center h-100 oee-btn oee-bg-dark" id="toolbar-button" ng-click="showToolbarModal()">
				<img class="invert m-3" width="30%" src="/images/svgs/edit.svg">
			</div>
		</div>

		<div class="grid_tb-drawer-btn upper-section-h   col-md-1 col-sm-2 col-3">
			<div class="center h-100 oee-btn oee-bg-dark d-flex" id="toolbar-button" data-toggle="modal" data-target="#toolbar-drawer"  ng-click="showToolbarDrawer()">
				<div class="f-7">
					<img class="invert my-3 w-3-em p-1"  src="/images/svgs/edit.svg">
				</div>
				<div class="f-3 h-100" class="oee-bg-dark" style="background: #4D4D4D">
					<div class="w-100 h-100 center">
						<img src="/images/svgs/chevron-right-simple.svg" class="invert">
					</div>
				</div>
			</div>
		</div>


		<div class="grid_graph bg-white upper-section-h   col-4">
			<bar-graph class="oee-font-varela oee-text-light p-1" style="font-size: 0.5em" max="{{ 50 }}"></bar-graph>
		</div>

		<div class="grid_units bg-white center overflow-hidden upper-section-h   col-lg-1 col-sm-2 col-9">
			<div class="p-1 w-100 text-center lh-1 fw-bold text-uppercase">
				<span class="fs-7"><?php echo __("Nuo pamainos pradžios pagaminta") ?><br>
				<span class="fs-4" id="tml-quantity">2791</span>
				<span class="fs-6" id="tml-quantity-units"> pcs.</span>
			</div>
		</div>

		<div class="grid_speedometer bg-white center upper-section-h  col-lg-2 col-md-3 col-2">
			<speedometer-gauge></speedometer-gauge>
		</div>

		<div class="grid_radials bg-white d-flex gap-2 p-2 ms-0 upper-section-h    col-lg-2 col-sm-3 col-6">
			<!-- OEE spalvu intervalai statiskai sudelioti OEE buvo. [0-75] raudona [75-80] geltona [80 -100] zalia -->
			<radial-progress t0=75 t1=80 value="{{OEE}}" color="#1ea866" text="OEE"></radial-progress>
			<radial-progress t0=75 t1=80 value=0 color="#fea52c" text="avail."></radial-progress>
			<radial-progress t0=75 t1=80 value=0 color="#247cd3" text="perf."></radial-progress>
		</div>

		<div class="grid_shift-metrics text-nowrap fw-bold oee-font-montserrat text-uppercase upper-section-h   col-lg-2 col-sm-3  col-6">

			<table class="w-100 h-100  fs-8" id="shift-metric-table">
				<tbody class="">
					<tr>
						<th>before</th>
						<th>qty</th>
						<th>avail</th>
						<th>perf</th>
						<th>oee</th>
					</tr>
					<tr class="shift-1" data-container="body" data-toggle="popover" data-placement="left" data-content="YYYY-MM-DD hh:mm:ss-YYYY-MM-DD hh:mm:ss">
						<td class="">1 shift</td>
						<td class="fw-700 fs-7">23456</td>
						<td class="fw-700 fs-7">48</td>
						<td class="fw-700 fs-7">43</td>
						<td class="fw-700 fs-7">100</td>
					</tr>
					<tr class="shift-2" data-container="body" data-toggle="popover" data-placement="left" data-content="YYYY-MM-DD hh:mm:ss-YYYY-MM-DD hh:mm:ss">
						<td class="">2 shifts</td>
						<td class="fw-700 fs-7">63459</td>
						<td class="fw-700 fs-7">55</td>
						<td class="fw-700 fs-7">22</td>
						<td class="fw-700 fs-7">100</td>
					</tr>
					<script>
						new bootstrap.Popover(document.querySelector(".shift-1"), { trigger: 'hover' })
						new bootstrap.Popover(document.querySelector(".shift-2"), { trigger: 'hover' })
					</script>
				</tbody>
			</table>
		</div> 

	</section>

	<section class="row g-0">
		<div class="grid_tb-menu text-white fw-bold text-uppercase lh-sm  position-relative   col-1" >

			<div style="height: calc(100% + 2.75rem); max-height: calc(100vh - max(72px, 12vh) - 4.5em); font-size: 90%;" class="w-100 d-flex flex-column position-absolute gap-2" id="toolbar-btns">

				<a data-id="1"
					class="p-1 center oee-btn bg-green		register-event" 
					data-ng-if="(!needTimePager && sensor.time_interval_register_event_button == 1) || sensor.has_access_to_register_events" 
				data-ng-if="(!needTimePager && sensor.time_interval_register_event_button == 1) || sensor.has_access_to_register_events" 
					data-ng-if="(!needTimePager && sensor.time_interval_register_event_button == 1) || sensor.has_access_to_register_events" 
					data-ng-click="endProductionProc()"> <?php echo __('Registruoti įvykį'); ?>
				</a>
				
				<a data-id="2"
					class="p-1 center oee-btn bg-orange		declare-losses" 
					data-ng-if="currentPlan && sensor.time_interval_declare_count_button>=1"
					data-ng-click="declareQuantities()"> <?php echo __('Deklaruoti kiekius'); ?>
				</a>
			
				<a data-id="3"
					class="p-1 center oee-btn bg-light-gray"
					data-ng-if="sensor.time_interval_diff_card_button==1" 
				data-ng-if="sensor.time_interval_diff_card_button==1" 
					data-ng-if="sensor.time_interval_diff_card_button==1" 
					target="_blank"
					onclick="window.open('<?php echo Router::url('/diff_cards/0/edit?wc='.$wc,true); ?>')"> <?php echo __('Nauja neatitikties kortelė'); ?>
				</a>

				<a data-id="4"
					class="p-1 center oee-btn bg-blue"  
					data-ng-if="currentPlan && !needTimePager && sensor.time_interval_change_order_button == 1" 
				data-ng-if="currentPlan && !needTimePager && sensor.time_interval_change_order_button == 1" 
					data-ng-if="currentPlan && !needTimePager && sensor.time_interval_change_order_button == 1" 
					data-ng-click="swapCurrentPlan()"><?php echo __('Keisti užsakymą'); ?>
				</a>

				<a data-id="5"
					class="p-1 center oee-btn bg-light-gray 	ng-scope"
					ng-if="!needTimePager && sensor.operator_can_confirm_order"  
				ng-if="!needTimePager && sensor.operator_can_confirm_order"  
					ng-if="!needTimePager && sensor.operator_can_confirm_order"  
					data-ng-click="confirmOrders()"> <?php echo __('Užsakymų tvirtinimas'); ?>
				</a>

				<a data-id="6"
					class="p-1 center oee-btn bg-red" 
					data-ng-if="waitingDiffCardId != 0 && waitingDiffCardId != null && sensor.time_interval_fill_nk_button==1" 
				data-ng-if="waitingDiffCardId != 0 && waitingDiffCardId != null && sensor.time_interval_fill_nk_button==1" 
					data-ng-if="waitingDiffCardId != 0 && waitingDiffCardId != null && sensor.time_interval_fill_nk_button==1" 
					target="_blank" href="/diff_cards/{{waitingDiffCardId}}/edit"><b>!</b> <?php echo __('Pildyti NK') ?>
				</a>

				<a data-id="7"
					class="p-1 center oee-btn bg-light-gray" 
					data-ng-if="sensor.confirm_orders" 
				data-ng-if="sensor.confirm_orders" 
					data-ng-if="sensor.confirm_orders" 
					target="_blank" data-ng-click="openConfirmationPage()" href=""><?php echo __('Tvirtinti užsakymus') ?>
				</a>

				<a data-id="8"
					class="p-1 center oee-btn bg-light-gray" 
					data-ng-if="sensor.time_interval_speedsgraph_button" 
				data-ng-if="sensor.time_interval_speedsgraph_button" 
					data-ng-if="sensor.time_interval_speedsgraph_button" 
					target="_blank" href="/speeds/plugins/index/{{sensor.id}}"><?php echo __('Greičio grafikas') ?>
				</a>

				<a data-id="9"
					class="p-1 center oee-btn bg-light-gray" 
					data-ng-if="currentPlan.Plan.tech_cards_path" 
				data-ng-if="currentPlan.Plan.tech_cards_path" 
					data-ng-if="currentPlan.Plan.tech_cards_path" 
					target="_blank" href="/settings/view_remote_file/{{currentPlan.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?>
				</a>

				<a data-id="10"
					class="p-1 center oee-btn bg-light-gray" 
					data-ng-if="sensor.show_shift_change_button" 
				data-ng-if="sensor.show_shift_change_button" 
					data-ng-if="sensor.show_shift_change_button" 
					data-ng-click="initiateShiftChange()" target="_blank" href=""><?php echo __('Pamainos pasikeitimas') ?>
				</a>
				
				<?php
					if($wc != 0) {
						foreach ($pluginFunctionality['sidebarLinks'] as $plugin) {
							echo str_replace("{workCenterId}",$wc,$plugin);
						}
					}
				?>

				<a id="verificationCountdown" class="oee-btn oee-center bg-orange" style="white-space: normal;" data-ng-if="verificationCountdown">{{verificationCountdown}}</a>
			</div>
		</div>

		<div class="d-flex text-uppercase grid_tml-container f-10" style="width: 0px; overflow: hidden;">
		
			<div class="d-flex flex-column  oee-font-montserrat">
				<div class="oee-bg-light h-2-rem p-1"><wbr></div>
				<div class="flex-grow-1 d-flex flex-column lh-sm gap-2 pt-1" id="tml-titles">
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">01</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">02</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">03</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">04</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">05</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
					<div class="oee-bg-light oee-center flex-column p-1"><span class="oee-text-dark fs-5" id="tml-hour">06</span><span class="fs-8 oee-text-light text-nowrap" id="tml-date">1970-01-01</span></div>
				</div>
			</div>

			<div class="d-flex flex-column pl-1 oee-font-varela flex-grow-1 ms-0">
				<div class="d-flex oee-text-light oee-bg-light align-items-center h-2-rem p-1 ">
					<div class="f-5"></div>
					<div class="f-10 text-center p-1">5</div>
					<div class="f-10 text-center p-1">10</div>
					<div class="f-10 text-center p-1 oee-text-dark">15</div>
					<div class="f-10 text-center p-1">20</div>
					<div class="f-10 text-center p-1">25</div>
					<div class="f-10 text-center p-1 oee-text-dark">30</div>
					<div class="f-10 text-center p-1">35</div>
					<div class="f-10 text-center p-1">40</div>
					<div class="f-10 text-center p-1 oee-text-dark">45</div>
					<div class="f-10 text-center p-1">50</div>
					<div class="f-10 text-center p-1">55</div>
					<div class="f-5"></div>
				</div>

				<div class="flex-grow-1">
					<div class="position-relative w-100 h-100">

						<!-- background line divs -->
						<div class="position-absolute w-100 h-100 d-flex oee-minute-lines">
							<div ng-repeat="i in [0,1,2,3,4,5,6,7,8,9,10,11]"></div>
						</div>

						<!-- tml hours -->
						<div class="h-100 d-flex flex-column lh-sm gap-2 pt-1 ms-0" id="tml-graphs">
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
							<div class="bg-white h-100 z-1 d-flex flex-column position-relative ms-0"> <div class="f-10 d-flex ms-0" id="tml-1"></div> <div class="f-10 d-flex ms-0" id="tml-2"></div> </div>
						</div>

					</div>
				</div>
			</div>
			<div class="h-100 position-relative ml-1" style="width: calc(1vw + 24px);" id="tml-drawer">

				<div class="position-absolute d-flex oee-font-montserrat text-uppercase gap-sm h-100 z-1 pr-1" style="width: 220px; left: 0px; transition: left 0.2s linear; opacity: 0.85;" id="tml-drawer-metrics"></div>

				<div class="position-relative w-100 h-100 oee-bg-dark z-2 center oee-btn" ng-click="openTmlDrawer()">
					<img ng-src="{{is_side_open ? '/images/svgs/chevron-right-simple.svg' : '/images/svgs/chevron-left-simple.svg'}}" class="invert">
				</div>

			</div>
		</div>

		<div class="grid_tml-metrics d-flex oee-font-montserrat text-uppercase gap-sm   col-lg-2 col-3" id="tml-metrics">
			<div class="d-flex flex-column w-100">
				<div class="oee-bg-light lh-1 center p-1 h-2-rem" id="tml-metric-head" style="font-size:70%;">head</div>
				<div class="pt-1 flex-grow-1 d-flex flex-column gap-2 oee-font-varela fw-bold" id="tml-metric-body" style="font-size: calc(.5em + .5vw);">
					<div class="p-1 bg-white flex-grow-1 center">0</div>
					<div class="p-1 bg-white flex-grow-1 center">0</div>
					<div class="p-1 bg-white flex-grow-1 center">0</div>
					<div class="p-1 bg-white flex-grow-1 center">0</div>
					<div class="p-1 bg-white flex-grow-1 center">0</div>
					<div class="p-1 bg-white flex-grow-1 center">0</div>
				</div>
			</div>
		</div>
	</section>

	<section class="row g-0 h-3-rem">
		<div class="grid_extra col-lg-1" ng-if="needTimePager"></div>

		<div class="grid_time-ctrl d-flex gap-2 oee-font-montserrat fs-7   col-9 " ng-if="needTimePager">
			<div class="oee-bg-dark oee-center f-0 p-2">
				<div ng-if="updateInProgress" class="spinner-border text-white" style="height: 1.5em; width: 1.5em;">
					<span class="visually-hidden"></span>
				</div>
				<div ng-if="!updateInProgress"  style="height: 1.5em; width: 1.5em;"></div>
			</div>

			<div ng-class="getTimeCtrlClass(-graphHoursCount)" class="w-100 center p-1 oee-btn" ng-click="changeTime(-graphHoursCount)"><img class="invert h-100" src="/images/svgs/chevron-bar-left.svg"></div>
			<div ng-class="getTimeCtrlClass(-1)"			   class="w-100 center p-1 oee-btn" ng-click="changeTime(-1)">				<img class="invert h-100" src="/images/svgs/chevron-left.svg"></div>
			<div ng-class="getTimeCtrlClass( 1)"			   class="w-100 center p-1 oee-btn" ng-click="changeTime( 1)">				<img class="invert h-100" src="/images/svgs/chevron-right.svg"></div>
			<div ng-class="getTimeCtrlClass( graphHoursCount)" class="w-100 center p-1 oee-btn" ng-click="changeTime( graphHoursCount)"><img class="invert h-100" src="/images/svgs/chevron-bar-right.svg"></div>

			<div ng-class="getTimeCtrlClass()" class=" h-100 oee-center" style="width: 4em">
				<div>
					<select 
						class="form-select p-1 text-white oee-btn" 
						style="background: none; border: none; outline: none; opacity: 1;" 
						data-ng-change="setGraphHoursCount(this)" 
						ng-model="graphHoursCount"
					> 
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="3">3h </option>
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="6">6h </option>
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="8">8h </option>
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="12">12h</option>
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="18">18h</option>
						<option ng-disabled="updateInProgress" class="text-dark d-block" value="24">24h</option>
					</select>
				</div>
			</div>

			<div ng-class="getTimeCtrlClass()" class="oee-center p-2 px-3 oee-btn" style="flex:0" ng-click=changeTime(0)>  
				<img src="/images/svgs/home.svg" class="invert">	
			</div>
		</div>

		<div class="grid_time-picker  oee-font-montserrat fs-7   col-lg-2 col-3" ng-if="needTimePager" >
			<style> ::placeholder { color: white; opacity: 1; } </style> 
			<input 
				id="date-picker-input"
				class="h-100 w-100 text-center text-white text-uppercase oee-btn oee-bg-dark"  
				type="text" style="border: none;"
				ng-change="changeTime()" ng-model="calendar_input_str"
				ng-disabled="updateInProgress" 
				placeholder="<?php echo __('Pasirinkti laiką'); ?>"
			> 
			<script>
				$('#date-picker-input').bootstrapMaterialDatePicker({ 
					format : 'YYYY-MM-DD HH:00', 
					lang: 'lt',
					weekStart : 1, 
					maxDate: new Date(), 
					year: false,
					cancelText: "<?php echo __('Grįžti'); ?>",
					okText: "<?php echo __('Toliau'); ?>",
					switchOnClick : true,
				}).on('open', function(e, date){
					document.querySelector("body").classList.add("modal-open");
				}).on('close', function(e, date){
					document.querySelector("body").classList.remove("modal-open");
				});
			</script>			
		</div>

	</section>

	<section class="fw-bold" data-ng-if="sensor.marquee_message">
		<div class="marquee text-dark">
			<span class="marquee-text-container">
				<span class="marquee-text pb-0">{{sensor.marquee_message}}</span>
			</span>
		</div>
	</section>

	<footer class="mx-2 fw-bold">
		<table class="w-100" id="data-table">
			<thead class>
				<tr style="border-bottom: 3px solid var(--bg-main-color)">
				<?php 
                    if(!empty($structure)){
                        foreach($structure as $columnname => $column){
                            if(isset($column['outputOnTable']) && !$column['outputOnTable']){ continue; }
                            $title = $column;
                            if(is_array($column) && isset($column['title'])){
                                $title = $column['title'];
                            }
                            ?><th><?php echo __("$title"); ?></th><?php
                        }
                    }else{
                        ?><th><?php echo __('Gaminio pavadinimas'); ?></th><?php
                    }
                    ?>
                    <th data-ng-if="currentPlan.Plan.tech_cards_path"><?php echo __('Tech. kortelė'); ?></th>
				</tr>
			</thead>
			<tbody class="bg-white oee-text-light">
				<tr data-ng-show="currentPlan != null" class="warning-for-blind" data-ng-init="">
                    <?php
                    if(!empty($structure)){
                        foreach($structure as $columnname => $column){
                            $columnname = $columnname == 'step'?$columnname.'/60':$columnname;
                            if(is_array($column) && isset($column['outputOnTable']) && $column['outputOnTable'] === false) {
                                continue;
                            }elseif(is_array($column) && isset($column['additional_info'])){
                                ?><td>{{currentPlan.ApprovedOrder.additional_data.<?php echo $columnname; ?>}}</td><?php
                            }else{
                                if(sizeof(explode('.',$columnname)) > 1){$model = '';}else{$model='Plan.';}
                                ?><td>{{currentPlan.<?php echo $model.$columnname; ?>}}</td><?php
                            }
                        }
                    }else{
                        ?><td>{{currentPlan.Plan.production_name + " ("+currentPlan.Plan.production_code+")"}}</td><?php
                    }
                    ?>
                    <th data-ng-if="currentPlan.Plan.tech_cards_path"><a class="btn btn-info" target="_blank" href="/settings/view_remote_file/{{currentPlan.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?></a></th>
                </tr>
                <tr data-ng-repeat="li in plans" data-ng-if="li.Plan.id != currentPlan.Plan.id" data-ng-class="{'warning-for-blind': (li.Plan.current), 'info': (li.Plan.status_id == <?php echo ApprovedOrder::STATE_POSTPHONED; ?>)}"
                    data-ng-init="pallet_count = Math.floor(((li.quantity / li.box_quantity)/52));">
                    <?php
                    if(!empty($structure)){
                        foreach($structure as $columnname => $columnTitle){
                            if(isset($columnTitle['outputOnTable']) && !$columnTitle['outputOnTable']){ continue; }
                            $columnname = $columnname == 'step'?$columnname.'/60':$columnname;
                            if(sizeof(explode('.',$columnname)) > 1){$model = '';}else{$model='Plan.';}
                            ?><td>{{li.<?php echo $model.$columnname; ?>}}</td><?php
                        }
                    }else{
                        ?><td>{{li.productionName + " ("+li.productionCode+")"}}</td><?php
                    }
                    ?>
                    <th data-ng-if="li.Plan.tech_cards_path"><a class="btn btn-info" target="_blank" href="/settings/view_remote_file/{{li.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?></a></th>
                </tr>
			</tbody>

		</table>
	</footer>
	<div class="p-1 oee-bg-main"></div>

	<div class="modals-container">
		<?php echo $this->element('WorkCenterModals'); ?>
	</div>

</main>
<script>
	let params = { backdrop: 'static', keyboard: false };
	const modals = {
		toolbar: new bootstrap.Modal(document.getElementById('toolbarModal'), params),
		declareQuantities: new bootstrap.Modal(document.getElementById('declareQuantitiesModal'), params),
		swapCurrentPlan: new bootstrap.Modal(document.getElementById('swapCurrentPlanModal'), params),
		startPlan: new bootstrap.Modal(document.getElementById('startPlanModal'), params),
		endPlan: new bootstrap.Modal(document.getElementById('endPlanModal'), params),
		speedProblem: new bootstrap.Modal(document.getElementById('speedProblemModal'), params),
		verification: new bootstrap.Modal(document.getElementById('verificationModal'), params),
		information: new bootstrap.Modal(document.getElementById('informationModal'), params),
		lockScreen: new bootstrap.Modal(document.getElementById('lockScreenModal'), params),
	}

	for(let mod of Object.values(modals)){
		mod._element.addEventListener('shown.bs.modal', function () {
			document.querySelector("body").classList.add("modal-open");
		})
	}
</script>
<script>
	function numbersOnly(e){
		let num = e.value.match(/(\d+[,.]\d+)|(\d+[,.])|(\d+)/g);
		if(num) e.value = num[0].replace(/,/g, "."); else e.value = "";
	}
</script>
<script>
setTimeout(async function(){
	document.querySelector("#foreground").style.opacity = "0";
	await new Promise(r => setTimeout(r, 200));
	document.querySelector("#foreground").style.display = "none";
},5_000)
</script>
<style type="text/css">
    #tml-graphs .tml-gitem-red.problemid_<?php echo $exceeded_transition_id; ?>{
        background-color: #FFA500 !important;
    }
    <?php
    foreach($workIntervalsColors as $intervalTitle => $intervalColor){?>
        #tml-graphs .tml-gitem-<?php echo $intervalTitle; ?> {
            background-color: <?php echo $intervalColor['color']; ?> !important;
            border-color: <?php echo $intervalColor['color']; ?>;
        }
    <?php }
    ?>
</style>