<script type="text/javascript"> 
    window.currentLang = '<?php echo current(explode('_',$currentLang)); ?>';
    window.sensorId = '<?php echo $wc; ?>';
    window.baseUri = '<?php echo $baseUri; ?>';
    window.baseUriDc = '<?php echo (isset($dc) && $dc) ? $dc : ''; ?>';
    window.allowChangeProcess = '<?php echo (isset($allowChangeProcess) && $allowChangeProcess) ? $allowChangeProcess : ''; ?>';
    window.is_operator = <?php echo (int)$sensorData['User']['sensor_id'] > 0?'true':'false'; ?>;
    var text_what_you_produce = '<?php echo __('Parinkite ką gaminate') ?>';
    var text_confirm_shift_change = '<?php echo __('Ar tikrai norite pakeisti pamainą?') ?>';
    <?php echo isset($pluginScripts)?$pluginScripts:'';
    $requiredInputFields = array();
    foreach($structure as $fieldName => $structureField){
        if(!is_array($structureField) || !isset($structureField['required']) || !$structureField['required']){ continue; }
        $requiredInputFields[$fieldName]=$structureField['required'];
    }
    ?>
    var requiredInputTexts = <?php echo json_encode($requiredInputFields); ?>;
</script>
<div id="container" class="user-group-id-<?php echo $user->group_id; ?>" data-ng-app="WorkCenter" data-ng-controller="WorkCenterCtrl">
    <div class="row">
        <div class="col-xs-3">
             <label><i title="<?php echo __('Spausdinant puslapį pirmą kartą, spausdinimo puslapio peržiūros lange (\'Print preview\') rekomenduojama susikonfiguruoti spausdinimo nuostatas. Optimalus atvaizdavimo dydis A4 formatui - 70%.'); ?>" data-ng-click="printPage()" class="iconsweets-printer" style="cursor:pointer;"></i>&nbsp;<?php echo __('Darbo centras'); ?>:</label><br />
             <label class="sensr_header">{{sensor.name}}</label>
        </div>
        <div class="col-xs-5">
             <label><?php echo __('Planas'); ?>:</label><br />
             <label class="sensr_header">{{currentPlan ? currentPlan.Plan.production_name +" (" + currentPlan.Plan.production_code +")" : '&mdash;'}}</label>
        </div>
        <div class="col-xs-4">
             <div style="display: inline-block; margin-left: 60px;">
                <label><?php echo __('Prisijungęs vartotojas'); ?>:</label>
                <div class="btn-group small-sell small-sell-for-blind" data-ng-if="availableSensors.length > 1">
                    <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><?php echo __('Pakeisti darbo centrą'); ?></button>
                    <ul class="dropdown-menu">
                         <li data-ng-repeat="availableSensor in availableSensors"><a style="white-space: normal;" href="<?php echo Router::url(array('controller'=>'WorkCenter','action'=>'changeActiveSensorId')); ?>/{{availableSensor.Sensor.id}}">{{availableSensor.Sensor.name}}</a></li>
                    </ul>
                </div>
                <a data-ng-if="availableSensors.length == 1" class="btn btn-default" style="margin-bottom: 5px;" href="<?php echo Router::url(array('controller'=>'WorkCenter','action'=>'changeActiveSensorId')); ?>/{{availableSensors[0].Sensor.id}}">{{availableSensors[0].Sensor.name}}</a>
                <br />
                <div style="position: relative;">
                    <div id="wc-languages">
                        <a href="" data-toggle="dropdown" class="dropdown-toggle"><img src="<?php echo Router::url("/files/flags/{$user->language}.png"); ?>" /></a>
                        <ul class="dropdown-menu pull-right skin-color">
                            <?php foreach($possibleLanguages as $slug => $langText): ?>
                                <li><a href="<?php echo Router::url(array('controller'=>'users', 'action'=>'change_language', $slug)); ?>"><img src="<?php echo Router::url("/files/flags/$slug.png"); ?>" />&nbsp;<?php echo $langText; ?></a></li>
                            <?php endforeach; ?>
                        </ul>
                    </div>
                    <label class="sensr_header"><?php echo trim($user->first_name.' '.$user->last_name)?$user->first_name.' '.$user->last_name:$user->username; ?>
                        <a href="<?php echo htmlspecialchars($logoutUrl); ?>" title="<?php echo __('Atsijungti'); ?>"><span class="iconfa-signout"></span></a>
                    </label>
                </div>
             </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-md-1 quick-btn-toolbar">
            <div class="">
    <!--            <a class="btn btn-info" href="--><?php //echo htmlspecialchars($newDiffCardUrl); ?><!--" target="_blank">--><?php //echo __('NK'); ?><!--</a>-->
                <!--button class="btn btn-success" type="button" data-ng-if="!needTimePager && sensor.time_interval_register_event_button == 1" data-ng-click="endProductionProc()"><?php echo str_replace(" ","<br>",__('Registruoti įvykį')); ?></button-->
                <button class="btn btn-success register-event" type="button" data-ng-if="(!needTimePager && sensor.time_interval_register_event_button == 1) || sensor.has_access_to_register_events" data-ng-click="endProductionProc()"><?php echo str_replace(" ","<br>",__('Registruoti įvykį')); ?></button>
                <!--            <button class="btn btn-warning" type="button" data-ng-if="currentPlan && !needTimePager && workCenter.type === --><?php //echo Sensor::TYPE_PACKING; ?><!--" data-ng-click="endProductionProc({quickSwap: true})">--><?php //echo __('Gamyba baigta'); ?><!--</button>-->
    <!--            <button class="btn btn-warning" type="button" data-ng-if="currentPlan && !needTimePager && workCenter.type === --><?php //echo Sensor::TYPE_PACKING; ?><!--" data-ng-click="prodPostphone(true, true)">--><?php //echo __('Eksportavimas'); ?><!--</button>-->
    <!---->
    <!---->
                <a class="btn btn-warning declare-losses" type="button" data-ng-if="currentPlan && sensor.time_interval_declare_count_button>=1" data-ng-click="declareQuantities()"><?php echo __('Deklaruoti kiekius'); ?></a>
                <button data-ng-if="sensor.time_interval_diff_card_button==1" class="btn btn-info" target="_blank" onclick="window.open('<?php echo Router::url('/diff_cards/0/edit?wc='.$wc,true); ?>')"><?php echo __('Nauja neatitikties kortelė'); ?></button>
                <button style="font-size:14px;" class="btn btn-grey"  type="button" data-ng-if="currentPlan && !needTimePager && sensor.time_interval_change_order_button == 1" data-ng-click="swapCurrentPlan()"><?php echo __('Keisti užsakymą'); ?></button>
                <button ng-if="!needTimePager && sensor.operator_can_confirm_order" class="btn btn-info ng-scope" data-ng-click="confirmOrders()"><?php echo __('Užsakymų tvirtinimas'); ?></button>
                <a class="btn btn-danger" data-ng-if="waitingDiffCardId != 0 && waitingDiffCardId != null && sensor.time_interval_fill_nk_button==1" target="_blank" href="/diff_cards/{{waitingDiffCardId}}/edit"><b>!</b> <?php echo __('Pildyti NK') ?></a>
                <a class="btn btn-info" data-ng-if="sensor.confirm_orders" target="_blank" data-ng-click="openConfirmationPage()" href=""><?php echo __('Tvirtinti užsakymus') ?></a>
                <a class="btn btn-info" data-ng-if="sensor.time_interval_speedsgraph_button" target="_blank" href="/speeds/plugins/index/{{sensor.id}}"><?php echo __('Greičio grafikas') ?></a>
                <a class="btn btn-info" data-ng-if="currentPlan.Plan.tech_cards_path" target="_blank" href="/settings/view_remote_file/{{currentPlan.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?></a>
                <a class="btn btn-info" data-ng-if="sensor.show_shift_change_button" data-ng-click="initiateShiftChange()" target="_blank" href=""><?php echo __('Pamainos pasikeitimas') ?></a>
                <div class="plugins">
                    <?php
                    if($wc != 0) {
                        foreach ($pluginFunctionality['sidebarLinks'] as $plugin) {
                            echo str_replace("{workCenterId}",$wc,$plugin);
                        }
                    }
                    ?>
                </div>
                <a id="verificationCountdown" class="text-center btn btn-warning" style="white-space: normal;" data-ng-if="verificationCountdown">{{verificationCountdown}}</a>
                <br class="clear" />
            </div>
        </div>
        <div class="col-xs-12 col-md-9" id="timelineContainer">
            <div class="timeline" data-ng-if="!lost_connection">
                <div class="tml-head"></div>
                <div data-graph-timeline="hours" data-graph-timeline-plan="currentPlan" data-graph-timeline-sensor-type="sensorType"></div>
                <div style="padding-top: 5px;" id="timePager" data-ng-if="needTimePager" data-time-pager="currTime" data-time-pager-sel="selTime" data-time-pager-title="<?php echo htmlspecialchars(__('Valandos')); ?>" data-time-pager-uip="updateInProgress">
                    <input placeholder="Data" style="display:inline-block;width:100px; margin-bottom: 10px;" type="text" type="text" class="form-control" id="timepager-date">
                    <div style="display:inline-block; width:100px;" class="btn-group">
                        <input style="margin-bottom: 10px;" title="<?php echo __('Grafike atvaizduojamų valandų kiekis'); ?>" data-toggle="dropdown" class="form-control dropdown-toggle" type="text" data-ng-model="graphHoursCount" />
                        <ul class="dropdown-menu smaller-items">
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(3)">3h</a></li>
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(6)">6h</a></li>
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(8)">8h</a></li>
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(9)">9h</a></li>
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(12)">12h</a></li>
                            <li><a href="javascript:void(0)" data-ng-click="setGraphHoursCount(24)">24h</a></li>
                        </ul>
                    </div>
                    <br>
                </div>
                <div class="marquee" data-ng-if="sensor.marquee_message"><p>{{sensor.marquee_message}}</p></div>
            </div>
            <p style="font-size: 45px; margin-top: 50px; text-align:center;" data-ng-if="lost_connection"><?php echo __('Nėra ryšio su jutikliu'); ?></p>
        </div>
        <div class="col-xs-1 col-md-2" id="gaugeBar" data-ng-if="!lost_connection">
            <div class="oee-gauge-wrap"><div class="oee-gauge" data-oee-gauge="OEE" data-oee-gauge-title="oeeGaugeTitle" data-oee-colors="sensor.half_donut_colors"></div></div>
            <h3 class="gaugeTitle"><?php echo __('Praėjusios pam'); ?> {{prevShiftsOEE[0]}}</h3>
            <h3 class="gaugeTitle"><?php echo __('Užpraėjusios pam'); ?> {{prevShiftsOEE[1]}}</h3>
            <br />            
            <div class="speed-gauge-wrap"><div class="speed-gauge" data-speed-gauge="lastMinuteSpeed"></div><p data-ng-if="showSpeedTitle" class="gaugeTitle" ><?php echo !$sensorData['Sensor']['speedometer_type']?__('Greitis'):__('Pamainos greitis'); ?>: {{lastMinuteSpeed}} {{currentPlan.Plan.step_unit_title}}</p><canvas width="250" id="speedGauge"></canvas></div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12" data-ng-if="!lost_connection">
            <table ng-class="needTimePager?'TimePagerExist':'TimePagerNotExist'" class="table table-bordered table-condensed table-striped plan-table">
                <thead>
                <tr>
                    <?php 
                    if(!empty($structure)){
                        foreach($structure as $columnname => $column){
                            if(isset($column['outputOnTable']) && !$column['outputOnTable']){ continue; }
                            $title = $column;
                            if(is_array($column) && isset($column['title'])){
                                $title = $column['title'];
                            }
                            ?><th><?php echo __("$title"); ?></th><?php
                        }
                    }else{
                        ?><th><?php echo __('Gaminio pavadinimas'); ?></th><?php
                    }
                    ?>
                    <th data-ng-if="currentPlan.Plan.tech_cards_path"><?php echo __('Tech. kortelė'); ?></th>
                </tr>
                </thead>
                <tbody>

                <tr data-ng-show="currentPlan != null" class="warning-for-blind" data-ng-init="">
                    <?php
                    if(!empty($structure)){
                        foreach($structure as $columnname => $column){
                            $columnname = $columnname == 'step'?$columnname.'/60':$columnname;
                            if(is_array($column) && isset($column['outputOnTable']) && $column['outputOnTable'] === false) {
                                continue;
                            }elseif(is_array($column) && isset($column['additional_info'])){
                                ?><td>{{currentPlan.ApprovedOrder.additional_data.<?php echo $columnname; ?>}}</td><?php
                            }else{
                                if(sizeof(explode('.',$columnname)) > 1){$model = '';}else{$model='Plan.';}
                                ?><td>{{currentPlan.<?php echo $model.$columnname; ?>}}</td><?php
                            }
                        }
                    }else{
                        ?><td>{{currentPlan.Plan.production_name + " ("+currentPlan.Plan.production_code+")"}}</td><?php
                    }
                    ?>
                    <th data-ng-if="currentPlan.Plan.tech_cards_path"><a class="btn btn-info" target="_blank" href="/settings/view_remote_file/{{currentPlan.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?></a></th>
                </tr>

                <tr data-ng-repeat="li in plans" data-ng-if="li.Plan.id != currentPlan.Plan.id" data-ng-class="{'warning-for-blind': (li.Plan.current), 'info': (li.Plan.status_id == <?php echo ApprovedOrder::STATE_POSTPHONED; ?>)}"
                    data-ng-init="pallet_count = Math.floor(((li.quantity / li.box_quantity)/52));">
                    <?php
                    if(!empty($structure)){
                        foreach($structure as $columnname => $columnTitle){
                            if(isset($columnTitle['outputOnTable']) && !$columnTitle['outputOnTable']){ continue; }
                            $columnname = $columnname == 'step'?$columnname.'/60':$columnname;
                            if(sizeof(explode('.',$columnname)) > 1){$model = '';}else{$model='Plan.';}
                            ?><td>{{li.<?php echo $model.$columnname; ?>}}</td><?php
                        }
                    }else{
                        ?><td>{{li.productionName + " ("+li.productionCode+")"}}</td><?php
                    }
                    ?>
                    <th data-ng-if="li.Plan.tech_cards_path"><a class="btn btn-info" target="_blank" href="/settings/view_remote_file/{{li.Plan.tech_cards_path}}"><?php echo __('Tech. kortelė') ?></a></th>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div aria-hidden="false" class="modal fade" id="declareQuantitiesModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="declareQuantitiesBlock">
                        <div><h3 class="pull-left"><?php echo __('Pagamintas kiekis'); ?></h3><h4 class="pull-right" data-ng-if="currentPlan.Plan.quantity > 0"> <?php echo __('Plano kiekis'); ?> {{currentPlan.Plan.quantity}}</h4><br class="clear" /><br /></div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <label class="control-label col-xs-{{sensor.declare_using_crates_and_pieces?3:9}}">{{sensor.declare_using_crates_and_pieces?sensor.declare_using_crates_and_pieces:(sensor.declare_quantities_label?sensor.declare_quantities_label:'<?php echo __('Kiekis'); ?>')}}</label>
                                <div class="col-xs-3">
                                    <div class="input-group">
                                        <input class="form-control input-default" type="text" data-ng-model="partialQuantity" /><br /><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantity}}
                                    </div>
                                </div>
                                <label data-ng-show="sensor.declare_using_crates_and_pieces" class="control-label col-xs-3"><?php echo __('Vienetai'); ?></label>
                                <div class="col-xs-3" data-ng-show="sensor.declare_using_crates_and_pieces">
                                    <div class="input-group">
                                        <input class="form-control input-default" type="text" data-ng-model="partialQuantityPieces" /><br /><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantityPieces}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-horizontal" id="declareLossesBlock">
                        <div><h3><?php echo __('Įveskite nuostolius'); ?></h3></div>
                        <div class="form-group" data-ng-repeat="lossType in lossTypes">
                            <label class="control-label col-xs-9">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</label>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <input class="form-control input-default" type="text" data-ng-model="lossType.LossType.value" />
                                    <span class="input-group-addon">{{lossType.LossType.unit}}</span>
                                </div>
                                {{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}}
                            </div>
                        </div>
                        <div class="form-horizontal">
                            <div class="form-group">
                                <div class="col-xs-12 text-right">
                                    <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="big-btn-menu" data-ng-if="!stateLoading">
                        <button type="button" class="btn btn-success btn-lg" data-ng-click="saveDeclaredQuantities()"><?php echo __('Patvirtinti'); ?></button>
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="hideQuantitiesModal()"><?php echo __('Atšaukti'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="false" class="modal fade" id="swapCurrentPlan" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
<!--                <div class="modal-header"><h4 class="modal-title modal-title-for-blind"></h4></div>-->
                <div class="modal-body">
                    <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
                    <div class="big-btn-menu" data-ng-if="!swapCurrentPlanYesPlans && !stateLoading">
                        <h4><b><?php echo __('Ar tikrai norite pakeisti dabartinį planą?'); ?>?</b></h4>
                        <button type="button" class="btn btn-warning btn-lg" data-ng-click="swapCurrentPlanYes()"><?php echo __('Taip'); ?></button>
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="swapCurrentPlanNo()"><?php echo __('Ne'); ?></button>
                    </div>
                    <div data-ng-if="swapCurrentPlanYesPlans && !stateLoading && !swapCurrentPlanYesPlansYes">
                        <div class="btn-group wide-sell wide-sell-for-blind" data-ng-if="1">
                            <div class="btn-group wide-sell wide-sell-for-blind">
                                <div data-ng-if="allPlans.length">
                                    <br/><h3><?php echo __('Planai'); ?></h3>
                                    <div data-ng-if="!stateLoading">
                                        <div class="btn-group wide-sell wide-sell-for-blind" data-ng-if="1">
                                            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle">{{newPlan ? newPlan.name : '<?php echo __('-- Nepasirinktas --'); ?>'}} <span class="caret"></span></button>
                                            <ul class="dropdown-menu">
                                                <li data-ng-repeat="plan in allPlans"><a style="white-space: normal;" href="javascript:void(0)" data-ng-click="selectNewPlan(plan.Plan)">{{plan.Plan.name}}</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div data-ng-if="products.length">        
                                    <br><h3><?php echo __('Pasirinktas produktas'); ?>: {{newProduct.Product ? newProduct.Product.name +" ("+ newProduct.Product.production_code +")" : '<?php echo __('-- Nepasirinktas --'); ?>'}}</h3>
                                    <input style="width:100%;font-size:14px;padding:4px;" data-toggle="dropdown" class="dropdown-toggle" type="text" data-ng-model="productsFilterString"/>
                                    <ul class="dropdown-menu smaller-items">
                                        <li data-ng-repeat="product in products | productsFilter:productsFilterString"><a href="javascript:void(0)" data-ng-click="selectNewProduct(product)">{{product.Product.name +" ("+ product.Product.production_code +")"}}</a></li>
                                    </ul>
                                </div>
                                <form method="POST">
                                <?php 
                                    $this->App->printAdditionalFields($structure, $this->Form);
                                ?>
                                </form>
                            </div>
                        </div>
                        <label data-ng-if="0">{{products[0].name}}</label>
                        <div class="big-btn-menu" data-ng-if="!stateLoading">
                            <button type="button" class="btn btn-primary btn-lg" data-ng-click="swapCurrentPlanConfirm()"><?php echo __('Patvirtinti'); ?></button>
                            <button type="button" class="btn btn-default btn-lg" data-ng-click="swapCurrentPlanNo()"><?php echo __('Uždaryti'); ?></button>
                        </div>
                    </div>
                    <div data-ng-if="swapCurrentPlanYesPlansYes && !stateLoading">

                        <h4><b><?php echo __('Ar tikrai keisti?'); ?></b></h4> 
                        <p><?php echo __('Keičiama iš'); ?>: <b>{{currentPlan.Plan.production_name}}</b></p>
                        <p data-ng-if="sensor.working_on_plans_or_products == 1"><?php echo __('Keičiama į'); ?>: <b>{{newProduct.Product.name}}</b></p>
                        <p data-ng-if="sensor.working_on_plans_or_products == 0"><?php echo __('Keičiama į'); ?>: <b>{{newPlan.production_name}}</b></p>

                        <div class="big-btn-menu" data-ng-if="!stateLoading">
                            <button type="button" class="btn btn-primary btn-lg" data-ng-click="swapCurrentPlanConfirmYes()"><?php echo __('Taip'); ?></button>
                            <button type="button" class="btn btn-default btn-lg" data-ng-click="swapCurrentPlanNo()"><?php echo __('Ne'); ?></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div aria-hidden="true" class="modal fade" id="startPlanModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" style="min-width: 800px;">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title modal-title-for-blind"><?php echo __('Kas dabar gaminama'); ?>?</h4></div>
                <div class="modal-body" data-ng-if="!prodConfirmStartWaitingConfirmation">
                    <!--div data-ng-if="!products.length">
                        Nėra produktų.
                    </div-->
                    <div data-ng-if="products.length || sensor.working_on_plans_or_products == 1">
                        <div class="well well-sm">
                            <div><?php echo __('Įvykio data laikas'); ?>: {{event.dateTime | date: 'yyyy-MM-dd HH:mm'}}</div>
                        </div>

                        <h2><?php echo __('Produktai'); ?></h2>
                        <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
                        <div class="widgetbox box-danger" data-ng-if="additionalFieldsErrors.length">
                            <h4 class="widgettitle"><?php echo __('KLAIDA'); ?></h4>
                            <div class="widgetcontent">
                                <ul style="list-style-type: none;" id="additionalFieldsErrors"><li data-ng-repeat="error in additionalFieldsErrors">{{error}}</li></ul>
                            </div>
                        </div>
                        <div data-ng-if="!stateLoading">
                            <div class="btn-group wide-sell wide-sell-for-blind" data-ng-if="1">
                                <p style="font-size: 15px;">{{newProduct.Product ?'<?php echo __('Pasirinktas produktas'); ?>: '+ newProduct.Product.name +" ("+ newProduct.Product.production_code +")" : '<?php echo __('-- Nepasirinktas --'); ?>'}}</p>
                                <input style="width:100%;font-size:14px;padding:4px;" data-toggle="dropdown" class="dropdown-toggle" type="text" data-ng-model="productsFilterString" placeholder="<?php echo __('Pasirinkite produktą'); ?>" />
                                <ul class="dropdown-menu smaller-items">
                                    <li data-ng-repeat="product in products | productsFilter:productsFilterString"><a href="javascript:void(0)" data-ng-click="selectNewProduct(product)">{{product.Product.name +" ("+ product.Product.production_code +")"}}</a></li>
                                </ul>
                            </div>
                            <label data-ng-if="0">{{products[0].name}}</label>
                            <form method="POST">
                            <?php 
                                $this->App->printAdditionalFields($structure, $this->Form);
                            ?>
                            </form>
                        </div>
                        <div class="big-btn-menu" data-ng-if="!stateLoading">
                            <button type="button" class="btn btn-primary btn-lg" data-ng-click="prodConfirmStartConfirm(1)"><?php echo __('Patvirtinti produktą'); ?></button>
                        </div>
                    </div>
                    <div data-ng-if="allPlans.length || sensor.working_on_plans_or_products == 0">
                        <br/>
                        <h2><?php echo __('Planai'); ?></h2>
                        <div data-ng-if="!stateLoading">
                            <div class="btn-group wide-sell wide-sell-for-blind" data-ng-if="1">
                                <!--button data-toggle="dropdown" class="btn btn-default dropdown-toggle">{{newPlan ? newPlan.name : '<?php echo __('-- Nepasirinktas --'); ?>'}} <span class="caret"></span></button-->
                                <p style="font-size: 15px;">{{newPlan ? '<?php echo __('Parinktas planas'); ?>: ' + newPlan.name : '<?php echo __('-- Nepasirinktas --'); ?>'}}</p>
                                <input style="width:100%;font-size:14px;padding:4px;" data-toggle="dropdown" class="dropdown-toggle" type="text" data-ng-model="plansFilterString" placeholder="<?php echo __('Pasirinkite planą'); ?>"/>
                                <ul class="dropdown-menu">
                                    <li data-ng-repeat="plan in allPlans | plansFilter:plansFilterString"><a style="white-space: normal;" href="javascript:void(0)" data-ng-click="selectNewPlan(plan.Plan)">{{plan.Plan.name}}</a></li>
                                </ul>
                            </div>
                            <label data-ng-if="0">{{plans[0].name}}</label>
                            <form method="POST">
                            <?php 
                                $this->App->printAdditionalFields($structure, $this->Form);
                            ?>
                            </form>
                        </div>
                        <div class="big-btn-menu" data-ng-if="!stateLoading">
                            <button type="button" class="btn btn-primary btn-lg" data-ng-click="prodConfirmStartConfirm(0)"><?php echo __('Patvirtinti planą'); ?></button>
                        </div>
                    </div>
                    <div class="big-btn-menu" data-ng-if="!stateLoading">
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="idleContinue()"><?php echo __('Uždaryti'); ?></button>
                    </div>
                </div>
                <div class="modal-body" data-ng-if="prodConfirmStartWaitingConfirmation">
                    <div class="big-btn-menu" data-ng-if="!stateLoading">
                        <h4 data-ng-if="newProduct != null"><?php echo __('Pasirinktas produktas'); ?>:<b>{{newProduct.Product.name + " (" + newProduct.Product.production_code +")"}}</b></h4>
                        <h4 data-ng-if="newPlan != null"><?php echo __('Pasirinktas planas'); ?>:<b>{{newPlan.name}}</b></h4>
                        <button type="button" class="btn btn-primary btn-lg" data-ng-click="prodConfirmStart()"><?php echo __('Patvirtinti'); ?></button>
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="idleContinue()"><?php echo __('Uždaryti'); ?></button>
                    </div>
                </div>
<!--                <div class="modal-body" data-ng-if="stateLoading">-->
<!--                    <div data-ng-if="stateLoading">-->
<!--                        <img src="--><?php //echo $baseUri; ?><!--images/loaders/loader12.gif" alt="" />&nbsp;--><?php //echo __('kraunasi...'); ?>
<!--                    </div>-->
<!--                </div>-->
            </div>
        </div>
    </div>
    <div aria-hidden="true" class="modal fade" id="endPlanModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title modal-title-for-blind">
                        <span><?php echo __('Kas atsitiko?'); ?></span>
                        <!--                        <span data-ng-if="(stopProdState !== 2 && stopProdState !== 3)">--><?php //echo __('Užregistruokite sustojimo priežastis!'); ?><!--</span>-->
                        <!--                        <span data-ng-if="(stopProdState === 2 || stopProdState === 3)">--><?php //echo __('Gamyba baigta!); ?><!--</span>-->
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="well well-sm modal-info-for-blind">
                        <div><?php echo __('Gaminama'); ?>: {{currentPlan ? currentPlan.Plan.production_name : '&mdash;'}}</div>
<!--                        <div>--><?php //echo __('Įvykio data laikas'); ?><!--: {{endDlg.event.dateTime | date: 'yyyy-MM-dd HH:mm'}}</div>-->
                    </div>
                    <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
                    <div data-ng-if="(orderPostphoneCountdown === 0 && stopProdState === 0 && orderEndVar !== 1 && !stateLoading)"  class="big-btn-menu">
                        <?php if(!isset($pluginFunctionality['dismissWorkCenterElements']['prodPostphoneCountdownConfirm']) ){ //gali buti neisvedamas jei yra per plugina ismetamas ?>
                        <button type="button" class="btn btn-warning btn-lg" data-ng-if="endDlg.currentPlan" data-ng-click="prodPostphoneCountdownConfirm()"><?php echo __('Gamyba atidėta'); ?></button>
                        <?php } ?>
                        <button type="button" class="btn btn-success btn-lg" data-ng-if="endDlg.currentPlan" data-ng-click="orderEnd()" data-ng-hide="endDlg.autoEvent"><?php echo __('Gamyba baigta'); ?></button>
                        <button data-ng-if="endDlg.event.type != -9999" type="button" class="btn btn-danger btn-lg" data-ng-click="prodRegisterIssue()"><?php echo __('Registruoti prastovos priežastį'); ?></button>
                        <!--button data-ng-if="(!endDlg.currentPlan && !(endDlg.event.foundProblemId > 0) || endDlg.event.type === 3)" class="btn btn-warning btn-lg" data-ng-click="sendRegisterIssue({Problem: {id: 1}, children: {}}, <?php echo $choosePlanAfterTransition > 0?'true':'false'; ?>)"><?php echo __('Perėjimas'); ?></button-->
                        <button data-ng-if="(!endDlg.currentPlan || sensor.allow_transition_during_work) && (endDlg.event.foundProblemId <= 0 || endDlg.event.problemId == 1 || accessesToMethods.changeProblemToTransition)" class="btn btn-transition btn-lg" data-ng-click="sendRegisterIssue({Problem: {id: 1}, children: {}}, <?php echo $choosePlanAfterTransition > 0?'true':'false'; ?>)"><?php echo __('Perėjimas'); ?></button>
                        <button data-ng-if="((!endDlg.currentPlan && accessesToMethods.changeProblemToNoWork) || (endDlg.currentPlan && foundProblem.FoundProblem.id > 0 && sensor.allow_nowork_during_work))" class="btn btn-primary btn-lg" data-ng-click="sendRegisterIssue({Problem: {id: 2}, children: {}}, false)"><?php echo $noWorkProblem['Problem']['name']; ?></button>                        <!--button data-ng-if="!endDlg.currentPlan && !(endDlg.event.foundProblemId > 0)" class="btn btn-primary btn-lg" data-ng-click="sendRegisterIssue({Problem: {id: 2}, children: {}}, false)"><?php echo $noWorkProblem['Problem']['name']; ?></button-->
                        <!--button class="btn btn-warning btn-lg" data-ng-click="prodPostphone(false,false,1)"><?php echo __('Perėjimas'); ?></button>
                        <button class="btn btn-primary btn-lg" data-ng-click="prodPostphone(false,false,2)"><?php echo __('Nėra darbo'); ?></button-->
                    </div>
                    <div data-ng-if="(orderEndVar === 1 && orderPostphoneCountdown === 0 && orderEndCountdown !== 1 && stopProdState < 2)" class="big-btn-menu">
                        <h4><b><?php echo __("Ar tikrai norite pabaigti planą?"); ?></b></h4>
                        <button type="button" class="btn btn-warning btn-lg" data-ng-click="orderEndYes()" data-ng-hide="endDlg.autoEvent"><?php echo __('Taip'); ?></button>
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="orderEndNo()" data-ng-hide="endDlg.autoEvent"><?php echo __('Ne'); ?></button>
                    </div>
                    <div data-ng-if="(orderPostphoneCountdown === 1)" class="big-btn-menu">
                        <h4><b><?php echo __("Ar tikrai atidėti užsakymą?"); ?></b></h4>
                        <button type="button" class="btn btn-warning btn-lg" data-ng-click="prodPostphoneCountdown()"><?php echo __('Taip'); ?></button>
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="orderEndNo()"><?php echo __('Ne'); ?></button>
                    </div>
                    <div data-ng-if="(orderPostphoneCountdown === 2)" class="big-btn-menu">
                        <h4 style="font-size:40px;text-align:center;"><b><?php echo __("Nevykdykite gamybos"); ?></b><br>
                            <?php echo __('Iki užsakymo atidėjimo') ?>: <b>{{ counter }}</b></h4>
                        <img src="<?php echo $baseUri; ?>img/stop.png" alt="" style="display:block; margin:10px auto" />
                    </div>
                    <div data-ng-if="(orderPostphoneCountdown === 3)" class="big-btn-menu">
                        <?php
                        if($wc != 0) {
                            if(empty($pluginFunctionality['afterProductionProblems']) || $pluginFunctionality['afterProductionProblems'] == null) { ?>
                                <button class="btn btn-transition btn-lg" data-ng-click="prodPostphone(false,false,1)"><?php echo __('Perėjimas'); ?></button>
                                <button class="btn btn-primary btn-lg" data-ng-click="prodPostphone(false,false,2)"><?php echo $noWorkProblem['Problem']['name']; ?></button>
                            <?php }
                            foreach ($pluginFunctionality['afterProductionProblems'] as $aprodp) {
                                $class = 'btn-primary';
                                if($aprodp['problem_id'] == 1) {
                                    $class = 'btn-warning';
                                }
                                if($aprodp['problem_id'] == 2) {
                                    $class = 'btn-grey';
                                }
                                ?>
                                <button class="btn <?php echo $class; ?> btn-lg" data-ng-click="prodPostphone(false,false,<?php echo $aprodp['problem_id'] ?>)"><?php echo $aprodp['name'] ?></button>
                            <?php }
                        }
                        ?>
                    </div>
                    <div data-ng-if="(orderEndCountdown === 1)" class="big-btn-menu">
                        <h4 style="font-size:40px;text-align:center;"><b><?php echo __("Nevykdykite gamybos"); ?></b><br>
                            <?php echo __('Iki užsakymo pabaigos'); ?>: <b>{{ counter }}</b></h4>
                        <img src="<?php echo $baseUri; ?>img/stop.png" alt="" style="display:block; margin:10px auto" />
<!--                        <button type="button" class="btn btn-default btn-lg" data-ng-click="orderEndCountdownStop()" data-ng-hide="endDlg.autoEvent">--><?php //echo __('Atšaukti'); ?><!--</button>-->
                    </div>
                    <!---->
                    <div data-ng-if="(orderEndVar !== 1 && orderEndCountdown !== 1 && stopProdState === 0 && orderPostphoneCountdown === 0)" class="big-btn-menu">
                        <button type="button" class="btn btn-default btn-lg" data-ng-click="idleIssue()" data-ng-hide="endDlg.autoEvent"><?php echo __('Uždaryti'); ?></button>
                    </div>
                    <div data-ng-if="(stopProdState === 1 && !stateLoading)">
                        <div><h3>{{deeperProblemEntered?'<?php echo __('Priežastys'); ?>':'<?php echo __('Pasirinkti tipą'); ?>'}}</h3></div>
                        <div class="big-btn-menu row">
                            <div class="col-xs-9">
                                <label><?php echo __('Papildomas komentaras apie prastovą'); ?></label>
                                <textarea style="margin-bottom: 0;" class="form-control input-default" data-ng-model="$parent.problemComment"></textarea>
                            </div>
                            <div class="col-xs-3">
                                <button data-ng-click="sendRegisterIssue({Problem:{id:'send_only_comment'}, children: []}, false)" class="btn btn-primary"><?php echo __('Išsaugoti komentarą'); ?></button>
                            </div>
                        </div>
                        <div class="big-btn-menu split_downtime_controll" data-ng-if="endDlg.event.foundProblem.state != 1 && accessesToMethods.allowDowntimeSplit">
                            <table class="table table-bordered table-condensed table-striped">
                                <thead><tr>
                                    <th><?php echo __('Prastovos pradžia / pabaiga'); ?></th>
                                    <th><?php echo __('Trukmė'); ?></th>
                                    <th style="width: 20%;"><?php echo __('Kiek minučių atkirpti?'); ?></th>
                                    <th><?php echo __('Atkirpti nuo'); ?></th>
                                    <th><?php echo __('Naujos prastovos pradžia/pabaiga'); ?></th>
                                </tr></thead>
                                <tbody><tr>
                                    <td>{{endDlg.event.foundProblem.oldStart}}<br />{{endDlg.event.foundProblem.oldEnd}}</td>
                                    <td><span data-ng-if="endDlg.event.foundProblem.newDuration">{{endDlg.event.foundProblem.newDuration}}</span><span data-ng-if="!endDlg.event.foundProblem.newDuration">{{endDlg.event.foundProblem.duration}}</span></td>
                                    <td style="width: 20%;"><input class="form-control" type="text" min="0" data-ng-model="$parent.$parent.downtimeCutDuration" ng-change="calculateDowntimesAfterCut()"  /></td>
                                    <td>
                                        <input type="radio" name="split_downtime" value="0" data-ng-model="$parent.$parent.downtimeCutType" ng-change="calculateDowntimesAfterCut()" id="splitDowntimeAtStart" />&nbsp;<label for="splitDowntimeAtStart"><?php echo __('Pradžios'); ?></label><br />
                                        <input type="radio" name="split_downtime" value="1" data-ng-model="$parent.$parent.downtimeCutType" ng-change="calculateDowntimesAfterCut()" id="splitDowntimeAtEnd" />&nbsp;<label for="splitDowntimeAtEnd"><?php echo __('Pabaigos'); ?></label>
                                    </td>
                                    <td>{{endDlg.event.foundProblem.newStart}}<br />{{endDlg.event.foundProblem.newEnd}}</td>
                                </tr></tbody>
                            </table>
                        </div>
                        <div class="big-btn-menu big-btn-menu-2col">
                            <button class="btn btn-danger btn-lg {{problemType.Problem.transition_problem?'transition':''}}" data-ng-repeat="problemType in workingProblemTypes" data-ng-click="sendRegisterIssue(problemType, false)">
                                <strong style="white-space: normal; display: inline-block; min-height: 40px;">{{problemType.Problem.name}}</strong>
                                &nbsp;<i class="iconfa-arrow-down" data-ng-if="problemType.children.length"></i>
                            </button>
                        </div>
                        <div class="big-btn-menu">
                            <button class="btn btn-default btn-default-for-blind btn-lg" data-ng-click="backProdEnd()"><?php echo __('Grįžti'); ?></button>
                        </div>
                    </div>
                    <div data-ng-show="(stopProdState === 2 && !stateLoading)">
                        <div data-ng-show="sensor.time_interval_declare_count_on_finish">
                            <div id="declareQuantitiesBlockOnFinish">
                                <div><h3 class="pull-left"><?php echo __('Pagamintas kiekis'); ?></h3><h4 class="pull-right" data-ng-if="currentPlan.Plan.quantity > 0"> <?php echo __('Plano kiekis'); ?> {{currentPlan.Plan.quantity}}</h4><br class="clear" /><br /></div>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="control-label col-xs-{{sensor.declare_using_crates_and_pieces?3:9}}">{{sensor.declare_using_crates_and_pieces?sensor.declare_using_crates_and_pieces:(sensor.declare_quantities_label?sensor.declare_quantities_label:'<?php echo __('Kiekis'); ?>')}}</label>
                                        <div class="col-xs-3">
                                            <div class="input-group">
                                                <input class="form-control input-default" type="text" data-ng-model="partialQuantityFinish" data-ng-click="emptyIfZero('partialQuantityFinish')" /><br /><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantity}}
                                            </div>
                                        </div>
                                        <label data-ng-show="sensor.declare_using_crates_and_pieces" class="control-label col-xs-3"><?php echo __('Vienetai'); ?></label>
                                        <div class="col-xs-3" data-ng-show="sensor.declare_using_crates_and_pieces">
                                            <div class="input-group">
                                                <input class="form-control input-default" type="text" data-ng-model="partialQuantityPiecesFinish" /><br /><?php echo __('Jau įvesta');  ?>: {{orderPartialQuantityPieces}}
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-ng-if="lossTypes.length > 0" id="declareLossesBlockOnFinish">
                            <div><h3><?php echo __('Įveskite nuostolius'); ?></h3></div>
                            <div class="form-horizontal">
                                <div class="form-group" data-ng-repeat="lossType in lossTypes">
                                    <label class="control-label col-xs-9">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</label>
                                    <div class="col-xs-3">
                                        <div class="input-group">
                                            <input class="form-control input-default" type="text" data-ng-model="lossType.LossType.value" />
                                            <span class="input-group-addon">{{lossType.LossType.unit}}</span>
                                        </div>
                                        {{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}}
                                    </div>
                                </div>
                            </div>
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <div class="col-xs-12 text-right">
                                        <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="big-btn-menu">
                            <button class="btn btn-primary btn-lg" data-ng-click="nextProdComplete(partialQuantityFinish)"><?php echo __('Išsaugoti'); ?></button>
                            <!--                                <button class="btn btn-default btn-default-for-blind btn-lg" data-ng-click="backProdEnd()">--><?php //echo __('Grįžti'); ?><!--</button>-->
                        </div>
                    </div>
                    <div data-ng-if="(stopProdState === 3 && !stateLoading)">
                        <br />
                        <div class="big-btn-menu">
<!--                            <button class="btn btn-warning btn-lg" data-ng-click="sendProdComplete()">--><?php //echo __('Perėjimas'); ?><!--</button>-->
<!--                            <button class="btn btn-primary btn-lg" data-ng-click="sendProdComplete(true)">--><?php //echo __('Nėra darbo'); ?><!--</button>-->
                            <?php
                            if($wc != 0) {
                                //if(empty($pluginFunctionality['afterProductionProblems']) || $pluginFunctionality['afterProductionProblems'] == null) { ?>
                                    <!--button class="btn btn-warning btn-lg" data-ng-click="sendProdComplete(1)"><?php echo __('Perėjimas'); ?></button-->
                                    <button type="button" class="btn btn-danger btn-lg" data-ng-click="prodRegisterIssueAfterProdEnd()"><?php echo __('Registruoti prastovos priežastį'); ?></button>
                                    <button class="btn btn-transition btn-lg" data-ng-click="sendProdComplete(1, <?php echo $choosePlanAfterTransition > 0?'true':'false'; ?>);"><?php echo __('Perėjimas'); ?></button>
                                    <button class="btn btn-lg" data-ng-click="sendProdComplete(2, false)"><?php echo $noWorkProblem['Problem']['name']; ?></button>
                                <?php //}
                                foreach ($pluginFunctionality['afterProductionProblems'] as $aprodp) {
                                    $class = 'btn-primary';
                                    if($aprodp['problem_id'] == 1) {
                                        continue;
                                        $class = 'btn-warning';
                                    }
                                    if($aprodp['problem_id'] == 2) {
                                        continue;
                                        $class = 'btn-grey';
                                    }
                                    ?>
                                    <button class="btn <?php echo $class; ?> btn-lg" data-ng-click="sendProdComplete(<?php echo $aprodp['problem_id'] ?>, <?php echo $choosePlanAfterTransition > 0 && $aprodp['problem_id']==1?'true':'false'; ?>)"><?php echo $aprodp['name'] ?></button>
                                <?php }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="true" class="modal fade" id="speedProblemModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog widen">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title modal-title-for-blind"><?php echo __('Kodėl mažas greitis?'); ?></h4></div>
                <div class="modal-body">
                    <div class="well well-sm">
                        <div><?php echo __('Įvykio data laikas'); ?>: {{event.dateTime | date: 'yyyy-MM-dd HH:mm'}}</div>
                    </div>
                    <div data-ng-if="stateLoading"><img src="<?php echo $baseUri; ?>images/loaders/loader12.gif" alt="" />&nbsp;<?php echo __('kraunasi...'); ?></div>
                    <div data-ng-if="!stateLoading">
                        <div><h3><?php echo __('Pasirinkti tipą'); ?></h3></div>
                        <div class="big-btn-menu big-btn-menu-2col">
                            <button class="btn btn-danger btn-lg" data-ng-repeat="problemType in speedProblemTypes" data-ng-click="sendRegisterSpeedIssue(problemType)"><strong>{{problemType.Problem.name}}</strong></button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="true" class="modal fade" id="verificationModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title modal-title-for-blind"><?php echo __('Gaminio patikra'); ?></h4></div>
                <div class="modal-body">
                    <div class="well well-sm text-center">
                        <div style="font-size: 40px; line-height: 40px;">
                            <?php echo $this->Html->image('./../images/warning_sign.png',array('height'=>36)).'&nbsp;&nbsp;'; ?>
                            <p style="line-height: 41px;" ng-bind-html="verification.text?verification.text:'<?php echo __('Atlikite detalių patikrą'); ?>'"></p>
                        </div>

                        <br /><br />
                        <button class="btn btn-primary" data-ng-click="registerVerification()"><?php echo __('Atlikta'); ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="true" class="modal fade" id="informationModal" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog <?php echo $sensorData['Sensor']['time_interval_declare_count_button'] == 2?'large':''; ?>">
            <div class="modal-content">
                <div class="modal-header"><h4 class="modal-title modal-title-for-blind"><?php echo __('Informacija'); ?></h4></div>
                <div class="modal-body">
                    <?php if($sensorData['Sensor']['time_interval_declare_count_button'] == 2){echo '<div class="row-fluid"><div class="col-lg-6">'; } ?>
                        <div class="well well-sm text-left">
                            <div id="informationText"></div>
                            <label><?php echo __('Komentaras'); ?></label>
                            <textarea class="form-control" data-ng-model="informationComment"></textarea>
                            <button class="btn btn-primary" data-ng-click="saveAndCloseInformationModal(informationFoundProblemId)"><?php echo __('Išsaugoti komentarą'); ?></button>
                            <button class="btn btn-primary" data-ng-click="closeInformationModal()"><?php echo __('Uždaryti'); ?></button>
                        </div>
                    <?php if($sensorData['Sensor']['time_interval_declare_count_button'] == 2): ?>
                    </div>
                    <div class="col-lg-6">
                        <div><h3><?php echo __('Įveskite nuostolius'); ?></h3></div><br />
                        <div class="form-group" data-ng-repeat="lossType in lossTypes">
                            <label class="control-label col-xs-9">{{lossType.LossType.name + ' (' + lossType.LossType.code + ')'}}</label>
                            <div class="col-xs-3">
                                <div class="input-group">
                                    <input class="form-control input-default" type="text" data-ng-model="lossType.LossType.value" />
                                    <span class="input-group-addon">{{lossType.LossType.unit}}</span>
                                </div>
                                {{orderLossesQuantities[lossType.LossType.id] != null?'<?php echo __('Jau įvesta');  ?>: '+orderLossesQuantities[lossType.LossType.id]:''}}
                            </div>
                        </div>
                        <br class="clear" />
                        <p class="text-right">
                            <?php echo __('Iš viso įvesta nuostolių');  ?>: {{totalOrderLossesQuantities}}
                        </p>
                        <button style="width: 100%;" type="button" class="btn btn-success btn-lg pull-right" data-ng-click="saveDeclaredQuantities(informationFoundProblemId)"><?php echo __('Patvirtinti nuostolius'); ?></button>
                    </div>
                    <?php endif; ?>
                    <br class="clear" />
                </div>
            </div>
        </div>
    </div>
    <div aria-hidden="true" class="modal fade" id="lockScreen" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body" style="text-align: center;">
                    <img src="<?php echo $baseUri; ?>img/stop.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .work-center .timeline .tml-hours > div .tml-graph .tml-gitem.tml-gitem-red.problemid_<?php echo $exceeded_transition_id; ?>{
        background-color: #FFA500 !important;
    }
    <?php
    foreach($workIntervalsColors as $intervalTitle => $intervalColor){?>
        .work-center .timeline .tml-hours > div .tml-graph .tml-gitem-<?php echo $intervalTitle; ?> {
            background-color: <?php echo $intervalColor['color']; ?> !important;
            border-color: <?php echo $intervalColor['color']; ?>;
        }
    <?php }
    ?>
</style>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/soundjs-0.5.2.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/raphael.2.1.0.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/justgage.1.0.1.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/gauge.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-datepicker.min.js"></script>
<!--<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-timepicker.min.js"></script>-->
<script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-datepicker.lt.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/angular.min.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/angular-sanitize.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/polyfill.js"></script>
<script type="text/javascript" src="<?php echo $baseUri; ?>js/work-center.js?v=12"></script>
<?php echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon'); ?>
<div class="popover"></div>
