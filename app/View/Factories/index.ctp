<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas fabrikas'); ?></button>
<br /><br />
<div class="row-fluid">
    <div class="col-lg-8 col-md-12">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th><?php echo __('ID'); ?></th>
                    <th><?php echo __('Pavadinimas'); ?></th>
                    <th>&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php if (empty($list)): ?>
                <tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
                <?php endif; ?>
                <?php foreach ($list as $li_): $li = (object) $li_[$model];
                ?>
                <tr>
                    <td><?php echo $li->id; ?></td>
                    <td><?php echo $li->name; ?></td>
                    <td>
                        <a class="btn btn-primary" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                        <a class="btn btn-primary" href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>
