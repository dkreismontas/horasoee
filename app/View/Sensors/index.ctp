<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas jutiklis'); ?></button>
<br /><br />
<table class="table table-bordered table-striped sortable">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
            <th><?php echo __('PS ID'); ?></th>
            <th><?php echo __('Pin'); ?></th>
            <th><?php echo __('IP / Port'); ?></th>
			<th><?php echo __('Pavadinimas'); ?></th>
			<th><?php echo __('Tipas'); ?></th>
			<th><?php echo __('Linija'); ?></th>
			<th><?php echo __('Padalinys'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="4"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; $liBranch = (isset($li_[Branch::NAME]) ? ((object) $li_[Branch::NAME]) : null);  ?>
		<tr style="<?php echo ($li->is_partial) ? "color:#888" : ""; ?>" data-sort_item="<?php echo $li->id; ?>">
			<td><?php echo $li->id; ?></td>
            <td><?php echo $li->ps_id; ?></td>
            <td><?php echo $li->pin_name; ?></td>
            <td><?php echo $li->port; ?></td>
			<td><?php echo $li->name; ?></td>
			<td><?php echo isset($typeOptions[$li->type]) ? $typeOptions[$li->type] : $li->type; ?></td>
			<td><?php echo $li_['Line']['name']??''; ?></td>
			<td><?php echo ($liBranch ? $liBranch->name : $li->branch_id); ?></td>
			<td class="full_width">
				<a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				<button class="btn btn-default removeSensor" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></button>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.removeSensor').bind('click',function(){
            var conf = confirm('<?php echo htmlspecialchars($removeMessage); ?>');
            if(conf){
                window.location.href = '<?php printf($removeUrl, $li->id); ?>';
            }
            return false;
        });
        makeSortable('table.sortable tbody', 'Sensor');
    });
</script>
