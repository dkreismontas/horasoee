<div class="col-xs-6">
<?php
echo $this->Form->create();
if(!empty($this->request->data['Possibility'])){
    foreach($this->request->data['Possibility'] as $possibility){
        if(empty($possibility['Sensor'])) continue;
        $possibilities[$possibility['Sensor']['id']] = $possibility['Sensor']['name'];
    }
	echo $this->Form->input('sensor_id', array('type'=>'select', 'label'=>__('Alternatyvos'), 'options'=>$possibilities, 'class'=>'form-control', 'div'=>'form-group'));
    foreach($columns as $name => $column){
    	echo $this->Form->input($name, array(
    		'type'=>isset($column['type'])?$column['type']:'text',
    		'col_name'=>isset($column['rename'])?$column['rename']:$name, 
    		'label'=>$column['title'], 
    		'class'=>'form-control', 
    		'div'=>'form-group',
    		//'disabled'=>'disabled'
    	));
    }
    echo $this->Form->submit(__('Išsaugoti'), array('class'=>'btn btn-primary'));
}else{ echo __('Pasirinkimo galimybių nėra'); }
echo $this->Form->end();
$tableElem = '';
if(isset($this->request->data['Possibility'])){
    foreach($this->request->data['Possibility'] as $possibility){
        $zeroData = $possibility;
        $zeroData = array_filter($zeroData, function($data){ return !is_array($data) && $data == 0;});
        $tableElem .= isset($possibility['Sensor']['name'])?'<tr class="'.(empty($possibility['Sensor'])?'problem':'').'"><td colspan="2" class="text-center">'.__('Sensorius').': '.$possibility['Sensor']['name'].'</td></tr>':'';
        $tableElem .= isset($possibility['speed'])?'<tr class="'.(isset($zeroData['speed'])?'problem':'').'"><td>'.__('Greitis vnt/min').'</td><td>'.round($possibility['speed']/60,2).'</td></tr>':'';
        $tableElem .= isset($possibility['client_speed'])?'<tr class="'.(isset($zeroData['client_speed'])?'problem':'').'"><td>'.__('Numatytas greitis iš kliento (vnt/h)').'</td><td>'.$possibility['client_speed'].'</td></tr>':'';
        $tableElem .= isset($possibility['preparation_time'])?'<tr class="'.(isset($zeroData['preparation_time'])?'problem':'').'"><td>'.__('Pasiruošimo laikas (min.)').'</td><td>'.$possibility['preparation_time'].'</td></tr>':'';
        $tableElem .= isset($possibility['production_time'])?'<tr class="'.(isset($zeroData['production_time'])?'problem':'').'"><td>'.__('Gamybos laikas (min.)').'</td><td>'.$possibility['production_time'].'</td></tr>':'';
    }
}
echo '<table class="table">'.$tableElem.'</table>';
?>
</div>
<div class="col-xs-6">
    <table class="table table-bordered table-condensed table-striped table-proptable">
    <tbody>
        <tr><td><label><?php echo __('Darbo centras'); ?></label></td><td><?php echo Sensor::buildName($this->request->data, false); ?></td></tr>
        <tr><td><label><?php echo __('Operacijos kodas'); ?></label></td><td><?php echo $this->request->data['Plan']['paste_code']; ?></td></tr>
        <tr><td><label><?php echo __('Produkcijos pavadinimas'); ?></label></td><td><?php echo $this->request->data['Plan']['production_name']; ?></td></tr>
        <tr><td><label><?php echo __('Produkcijos kodas'); ?></label></td><td><?php echo $this->request->data['Plan']['production_code']; ?></td></tr>
        <tr><td><label><?php echo __('Mo numeris'); ?></label></td><td><?php echo $this->request->data['Plan']['mo_number']; ?></td></tr>
        <tr><td><label><?php echo __('Pradžia'); ?></label></td><td><?php echo $this->request->data['Plan']['start']; ?></td></tr>
        <tr><td><label><?php echo __('Pabaiga'); ?></label></td><td><?php echo $this->request->data['Plan']['end']; ?></td></tr>
        <tr><td><label><?php echo __('Greitis vnt/min'); ?></label></td><td><?php echo round($this->request->data['Plan']['step']/60, 2); ?></td></tr>
        <tr><td><label><?php echo __('Kiekis dėžėje'); ?></label></td><td><?php echo $this->request->data['Plan']['box_quantity']; ?></td></tr>
        <tr><td><label><?php echo __('Kiekis'); ?></label></td><td><?php echo $this->request->data['Plan']['quantity']; ?></td></tr>
        <tr><td><label><?php echo __('Pasiruošimo laikas (min.)'); ?></label></td><td><?php echo $this->request->data['Plan']['preparation_time']; ?></td></tr>
        <tr><td><label><?php echo __('Gamybos laikas (min.)'); ?></label></td><td><?php echo $this->request->data['Plan']['production_time']; ?></td></tr>
        <tr><td><label><?php echo __('Numatytas greitis iš kliento (vnt/h)'); ?></label></td><td><?php echo $this->request->data['Plan']['client_speed']; ?></td></tr>
        <tr><td><label><?php echo __('Papildoma informacija'); ?></label></td><td><?php
            $serializedData = json_decode($this->request->data['Plan']['serialized_data'],true);
            if(is_array($serializedData)){
                foreach($serializedData as $key => $sData){
                    echo '<b>'.$key.'</b>: '.$sData.'<br />';
                }
            }
            ?></td></tr>
    </tbody>
</table>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		var planId = <?php echo $this->request->data['Plan']['id']; ?>;
		$('#PlanSensorId').bind('change', function(){
			$.ajax({
				url: '<?php echo $this->Html->url($this->here,true); ?>',
				type: 'POST',
				data: {sensor_id: $(this).val(), plan_id: planId},
				dataType: 'JSON',
				success: function(data){
					$('#PlanChangeDataForm input').each(function(){
						if(data.hasOwnProperty($(this).attr('col_name'))){
							$(this).val(data[$(this).attr('col_name')]);
						}
					});
					checkValues();
				}
			});
		});
		var checkValues = function(){
			$('form input').each(function(){
				if($(this).val() <= 0){
					$(this).addClass('problem');
				}else{
				    $(this).removeClass('problem');
				}
			});
		}
		checkValues();
	});
</script>
