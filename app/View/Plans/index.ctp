<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-default" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas planas'); ?></button>
<br /><br /> */ ?>
<?php if($forced_sensorId == null) {?>
<div class="clearfix">
    <a style="display:none;" href="/plans/create" class="btn btn-default">Sukurti planą</a>
	<div class="btn-group pull-right">

		<button class="btn btn-grey" id="reloadPage"><?php echo __('Perkrauti puslapį') ?></button>
		<button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Darbo centras') ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
		<ul class="dropdown-menu">
			<?php foreach ($sensorOptions as $lid => $li): ?>
			<li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
			<?php endforeach; ?>			
		</ul>
	</div>
</div>
<br/>
<?php } ?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('Veiksmai'); ?></th>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Darbo centras'); ?></th>
			<!--th><?php echo __('Linija'); ?></th-->
			<th><?php echo __('Operacijos kodas'); ?></th>
			<th><?php echo __('Gaminys'); ?></th>
			<th><?php echo __('Gaminio kodas'); ?></th>
			<th><?php echo __('MO Numeris'); ?></th>
			<th><?php echo __('Kiekis'); ?></th>
			<th><?php echo __('Pradžia'); ?></th>
			<th><?php echo __('Pabaiga'); ?></th>
			<th><?php echo __('Greitis vnt/min'); ?></th>
			<th><?php echo __('Kiekis formoje'); ?></th>
			<th><?php echo __('Būsena'); ?></th>
			<?php if(isset($arr['plan_exist'])){ ?><th><?php echo __('Planas'); ?></th><?php } ?>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model];
			$liSensor = (isset($li_[Sensor::NAME]) ? ((object) $li_[Sensor::NAME]) : null);
			$liLine = (isset($li_[Line::NAME]) ? ((object) $li_[Line::NAME]) : null); ?>
		<tr class="<?php 
			$state = isset($li_[ApprovedOrder::NAME]['status_id']) ? intval($li_[ApprovedOrder::NAME]['status_id']) : 0;
			if ($state == ApprovedOrder::STATE_COMPLETE) echo 'success';
			else if ($state == ApprovedOrder::STATE_CANCELED) echo 'danger';
			else if ($state == ApprovedOrder::STATE_POSTPHONED) echo 'warning';
			else if ($state == ApprovedOrder::STATE_IN_PROGRESS) echo 'info';
			else if ($li_[Plan::NAME]['has_problem']) echo 'problem';
			?>" id="order_<?php echo $li->id; ?>">
			<td><?php echo '
			    <i order_id="' . $li->id . '" class="' . ($li->position_frozen ? 'iconfa-star' : 'iconfa-star-empty') . ' freezePosition" title="' . __('Užšaldyti / atšildyti užsakymo pozicijos keitimą') . '"></i>&nbsp;&nbsp;
			';?></td>
			<td><?php echo $li->id; ?></td>
			<td><?php echo Sensor::buildName($li_, false); ?></td>
			<!--td><?php //echo Line::buildName($li_, false); ?></td-->
			<td><?php echo $li->paste_code; ?></td>
			<td><?php echo $li->production_name; ?></td>
			<td><?php echo $li->production_code; ?></td>
			<td><?php echo $li->mo_number; ?></td>
			<td><?php echo $li->quantity; ?></td>
			<td><?php $sdt = new DateTime($li->start); echo $sdt->format('Y-m-d H:i'); ?></td>
			<td><?php $edt = new DateTime($li->end); echo $edt->format('Y-m-d H:i'); ?></td>
			<td><?php echo round($li->step/60,2); ?></td>
			<td><?php echo $li->box_quantity; ?></td>
            <td><?php echo (isset($li_[ApprovedOrder::NAME]['id'])) ? ((array_key_exists($li_[ApprovedOrder::NAME]['status_id'],$statuses)) ? __($statuses[$li_[ApprovedOrder::NAME]['status_id']]) : __('Nežinoma')) : __('Nepradėta') ?></td>
            <?php if(isset($arr['plan_exist'])){ ?>
                <td><?php echo $li->plan_start.' - '.$li->plan_end; ?></td>
            <?php } ?>
            <td class="full_width">
				<?php if (isset($li_[ApprovedOrder::NAME]['status_id']) && $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_IN_PROGRESS): ?>
				    <a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                <?php elseif($li_[Plan::NAME]['control'] == 'T'):?>
                    <?php if( $li_[ApprovedOrder::NAME]['status_id'] != 2 ) : ?>
                    <a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Patvirtinti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Patvirtinti') ?></a>
                    <a class="btn btn-default" href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
				    <?php endif; ?>
                <?php elseif (!isset($li_[ApprovedOrder::NAME]['id']) || $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_CANCELED || $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED): ?>
				    <a class="btn btn-default" href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
				<?php else: ?>
				&nbsp;
				<?php endif; ?>
				<?php
					echo $this->Html->link('&nbsp;<span class="glyphicon glyphicon-edit"></span>&nbsp;'.__('Keisti duomenis'), array('action'=>'change_data', $li->id),array('escape'=>false,'class'=>'btn btn-default'));
				?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
<script type="text/javascript">
    jQuery(document).ready(function(){
        <?php if($sensorId > 0){ ?>
        var fixHelper = function(e, ui) { 
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
        $( "table.table > tbody").sortable({
            helper: fixHelper,
            containment: "parent",
            items: "> tr",
            update: function(event, ui) {
                $.post("<?php echo $this->Html->url('/'.$this->params['controller'].'/change_order_position/',true); ?>", { order: $('table.table > tbody').sortable('serialize') } );
            }
        });
        
        //uzsaldyti / atsildyti uzsakymo pozicijos keitima
        $('.freezePosition').bind('click',function(){
            var frozenStatus = $(this).hasClass('iconfa-star')?'0':'1';
            var thisOne = $(this);
            $.ajax({
                url: "<?php echo $this->Html->url('/'.$this->params['controller'].'/freeze_order/',true); ?>",
                type: 'post',
                data: {order_id: $(this).attr('order_id'), frozenStatus: frozenStatus},
                complete: function(){
                    var currentClickedId = thisOne.parents('tr').attr('id');
                    <?php if(isset($frozing_orders_type) && $frozing_orders_type==1){ ?>
                        thisOne.toggleClass('iconfa-star-empty iconfa-star');
                        return false;
                    <?php } ?>
                    if(frozenStatus == '1'){
                        thisOne.parents('tbody').children('tr').each(function(){
                            if($(this).attr('id') == currentClickedId){
                                $(this).find('.freezePosition').addClass('iconfa-star').removeClass('iconfa-star-empty');
                                return false;
                            }
                            $(this).find('.freezePosition').addClass('iconfa-star').removeClass('iconfa-star-empty');
                        });
                    }else{
                        var letUncheck = false;
                        thisOne.parents('tbody').children('tr').each(function(){
                            if($(this).attr('id') == currentClickedId){
                                letUncheck = true;
                            }
                            if(letUncheck){
                                $(this).find('.freezePosition').removeClass('iconfa-star').addClass('iconfa-star-empty');
                            }
                        });
                    }

                }
            });
        });
        $('#reloadPage').bind('click', function(){
            location.reload();
        });
        <?php } ?>
    })
</script>
