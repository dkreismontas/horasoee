<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php $canConfirm = ($item[Plan::NAME]['control'] == "T"); ?>
<table class="table table-bordered table-condensed table-striped table-proptable">
	<tbody>
		<tr><td><label><?php echo __('Darbo centras'); ?></label></td><td><?php echo Sensor::buildName($item, false); ?></td></tr>
		<!--tr><td><label><?php echo __('Linija'); ?></label></td><td><?php //echo Line::buildName($item, false); ?></td></tr-->
		<tr><td><label><?php echo __('Tešlos kodas'); ?></label></td><td><?php echo $item[plan::NAME]['paste_code']; ?></td></tr>
		<tr><td><label><?php echo __('Gaminio pavadinimas'); ?></label></td><td><?php echo $item[plan::NAME]['production_name']; ?></td></tr>
		<tr><td><label><?php echo __('Gaminio kodas'); ?></label></td><td><?php echo $item[plan::NAME]['production_code']; ?></td></tr>
		<tr><td><label><?php echo __('MO Numeris'); ?></label></td><td><?php echo $item[plan::NAME]['mo_number']; ?></td></tr>
		<tr><td><label><?php echo __('Pradžia'); ?></label></td><td><?php $sdt = new DateTime($item[plan::NAME]['start']); echo $sdt->format('Y-m-d H:i'); ?></td></tr>
		<?php if (!$canEdit && !$canConfirm): ?>
		<tr><td><label><?php echo __('Pabaiga'); ?></label></td><td><?php $edt = new DateTime($item[plan::NAME]['end']); echo $edt->format('Y-m-d H:i'); ?></td></tr>
		<tr><td><label><?php echo __('Kiekis'); ?></label></td><td><?php echo $item[plan::NAME]['quantity']; ?></td></tr>
		<?php endif; ?>
		<!--tr><td><label><?php echo __('Linijų kiekis'); ?></label></td><td><?php //echo $item[plan::NAME]['line_quantity']; ?></td></tr-->
		<tr><td><label><?php echo __('Dėžių kiekis'); ?></label></td><td><?php echo $item[plan::NAME]['box_quantity']; ?></td></tr>
		<!--tr><td><label><?php echo __('Pakuočių kiekis'); ?></label></td><td><?php //echo $item[plan::NAME]['package_quantity']; ?></td></tr-->
		<tr><td><label><?php echo __('Būsena'); ?></label></td><td><?php echo isset($item[ApprovedOrder::NAME]['id']) ? ((isset($item[Status::NAME]['name']) && $item[Status::NAME]['name']) ? __($item[Status::NAME]['name']) : __('Nežinoma')) : __('Nepradėta'); ?></td></tr>
	</tbody>
</table>
<?php if ($canEdit && !$canConfirm): ?>
<?php echo $this->Form->input('quantity', array('label' => __('Kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('end', array('label' => __('Pabaiga'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php endif; ?>
<div>
	<br />
	<?php if ($canEdit && !$canConfirm): ?>
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<?php endif; ?>
	<!--<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>-->
</div>
<?php echo $this->Form->end(); ?>


<?php if($canConfirm && $item[ApprovedOrder::NAME]['status_id'] != 2): ?>
    <h2>Užsakymo patvirtinimas</h2>
    <?php echo $this->Form->create('ApprovedOrder',array('url' => '/plans/create_approved_order/planId:'.$item[Plan::NAME]['id'], 'autocomplete' => 'off')); ?>
    <div class="row-fluid" style="margin: 0px -15px 0px -15px;">
        <div class="col-xs-5">
            <?php
            echo $this->Form->input('confirmed_box_quantity', array('label' => __('Dėžių kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        </div>
        <div class="col-xs-1">
            <div class="form-group"><label><?php echo __('ir'); ?></label></div>
        </div>
        <div class="col-xs-6">
            <?php echo $this->Form->input('confirmed_item_quantity', array('label' => __('Gaminių kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        </div>
    </div>
    <input type="submit" class="btn btn-success" name="production_complete" value="<?php echo __('Gamyba baigta'); ?>"/>
    <input type="submit" class="btn btn-warning" name="production_postphoned" value="<?php echo __('Gamyba atidėta'); ?>"/>
<!--    <button type="submit" class="btn btn-success"></button>-->
<!--    <button type="submit" class="btn btn-warning">--><?php //echo __('Gamyba atidėta'); ?><!--</button>-->
    <?php echo $this->Form->end(); ?>
<?php endif; ?>
