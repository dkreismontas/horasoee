<style type="text/css">
	.table {
		font-family: Arial,sans-serif;
		font-size: 14px;
		line-height: 16px;
		color: #000000;
	}
	.table-bordered {
		border: none;
		border-collapse: collapse;
	}
	
	.table-bordered td {
		border: 1px solid #000000;
		border-collapse: collapse;
		padding: 4px;
	}
</style>
<a href="<?php echo htmlspecialchars("http://www.horasoee.eu"); ?>" title="HorasOEE">HorasOEE</a>
<br />
<br />
<table class="table table-bordered">
	<tbody>
		<tr><td><?php echo __('Data ir laikas'); ?></td><td><?php $date = new DateTime($item[DiffCard::NAME]['date']); echo $date->format('Y-m-d H:i'); ?></td></tr>
		<tr><td><?php echo __('Darbo centras'); ?></td><td><?php echo Sensor::buildName($item, false); ?></td></tr>
		<tr><td><?php echo __('Padalinys'); ?></td><td><?php echo Branch::buildName($item, false); ?></td></tr>
		<tr><td><?php echo __('Linija'); ?></td><td><?php echo Line::buildName($item, false); ?></td></tr>
		<tr><td><?php echo __('Gaminys'); ?></td><td><?php echo Plan::buildName($item, false); ?></td></tr>
		<tr><td><?php echo __('Prastova'); ?></td><td><?php echo Problem::buildName($item, false); ?></td></tr>
		<tr><td><?php echo __('Aprašymas'); ?></td><td style="color: #ff0000;"><?php echo $item[DiffCard::NAME]['description']; ?></td></tr>
		<tr><td><?php echo __('Būsena'); ?></td><td><?php echo isset($states[$item[DiffCard::NAME]['state']]) ? ($states[$item[DiffCard::NAME]['state']] . $additionalStateText) : '&mdash;'; ?></td></tr>
		<tr><td><?php echo __('Atsakingas asmuo'); ?></td><td><?php echo $usersList[$item[DiffCard::NAME]['responsive_section_user_id']]??''; ?></td></tr>
        <?php if ($item && in_array($item[DiffCard::NAME]['state'], array(DiffCard::STATE_ACCEPTED, DiffCard::STATE_EXPLAINED, DiffCard::STATE_EXPLANATION_DENIED, DiffCard::STATE_COMPLETE))): ?>
            <tr><td><?php echo __('Paaiškinimas'); ?></td><td><?php echo $item[DiffCard::NAME]['clarification']; ?></td></tr>
            <tr><td><?php echo __('Kokie veiksmai atlikti nedelsiant?'); ?></td><td><?php echo $item[DiffCard::NAME]['measures_taken']; ?></td></tr>
            <tr><td><?php echo __('Kokie daromi koregavimo veiksmai, kad to išvengti ateityje?'); ?></td><td><?php echo $item[DiffCard::NAME]['measures_for_future']; ?></td></tr>
            <tr><td><?php echo __('Koregavimo veiksmų atlikimo terminas'); ?></td><td><?php echo $item[DiffCard::NAME]['deadline']?date('Y-m-d',strtotime($item[DiffCard::NAME]['deadline'])):''; ?></td></tr>
		<?php endif; ?>
        <tr><td colspan="2"><a href="<?php echo htmlspecialchars($url); ?>"><?php echo $_SERVER['HTTP_HOST']; ?></a></td></tr>
	</tbody>
</table>
<br />
<a href="<?php echo htmlspecialchars("http://www.horasoee.eu"); ?>" title="HorasOEE">
    <img style="width: 200px;" src="http://www.horasoee.eu/wp-content/uploads/2014/12/horas-oee-web-logo-copy.png" alt="HorasOEE" />
</a>
