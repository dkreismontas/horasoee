<style type="text/css">
	.table {
		font-family: Arial,sans-serif;
		font-size: 14px;
		line-height: 16px;
		color: #000000;
	}
	.table-bordered {
		border: none;
		border-collapse: collapse;
	}
	
	.table-bordered td {
		border: 1px solid #000000;
		border-collapse: collapse;
		padding: 4px;
	}
</style><?php

echo $this->element('ApprovedOrderTable', array('item' => $item, 'lossItems' => $lossItems, 'url' => $url));
