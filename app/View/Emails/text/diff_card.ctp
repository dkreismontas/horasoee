
<?php $date = new DateTime($item[DiffCard::NAME]['date']); echo __('Data ir laikas').': '.$date->format('Y-m-d H:i')."\n"; ?>
<?php echo __('Darbo centras').': '.Sensor::buildName($item, false)."\n"; ?>
<?php echo __('Padalinys').': '.Branch::buildName($item, false)."\n"; ?>
<?php echo __('Linija').': '.Line::buildName($item, false)."\n"; ?>
<?php echo __('Gaminys').': '.Plan::buildName($item, false)."\n"; ?>
<?php echo __('Prastova').': '.Problem::buildName($item, false)."\n"; ?>
<?php echo __('Būsena').': '.(isset($states[$item[DiffCard::NAME]['state']]) ? ($states[$item[DiffCard::NAME]['state']].$additionalStateText) : '&mdash;')."\n"; ?>
<?php echo $url;
