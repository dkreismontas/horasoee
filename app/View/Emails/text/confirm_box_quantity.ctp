<?php

$html = $this->element('ApprovedOrderTable', array('item' => $item, 'lossItems' => $lossItems, 'url' => $url));
$text = strip_tags(preg_replace('#(?:<br[^>]*>|</p>|</tr>)#i', "\n", preg_replace('#<style[^>]*>.*</style>#ismU', '', $html)));
echo str_replace('&nbsp;', ' ', $text);
