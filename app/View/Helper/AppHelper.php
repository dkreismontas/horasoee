<?php

App::uses('Helper', 'View');

/**
 * @property PaginatorHelper $Paginator
 */
class AppHelper extends Helper {
	
	public $helpers = array('Paginator');
	
	public function properPaging($cppChooser = false, array $options = array()) {
		$html = $this->Paginator->numbers(array(
			'before' => '',
			'after' => '',
			'separator' => '',
			'modulus' => 6,
			'tag' => 'li',
			'currentTag' => 'span',
			'currentClass' => 'active',
			'ellipsis' => '<li><span>...</span></li>',
			'first' => 1,//'&raquo;',
			'last' => 1//'&laquo;'
		));
		$pnOpts = array('tag' => 'li', 'disabledTag' => 'span', 'escape' => false);
		$pnOptsDis = $pnOpts;
		$pnOptsDis['class'] = 'disabled';
		$prevHtml = $this->Paginator->prev('&laquo;', $pnOpts, null, $pnOptsDis);
		$nextHtml = $this->Paginator->next('&raquo;', $pnOpts, null, $pnOptsDis);
		$cppHtml = '';
		if ($cppChooser) {
			$limit = $this->Paginator->param('limit');
			$model = $this->Paginator->defaultModel();
			$cppHtml .= '<div class="btn-group dropup" style="vertical-align: baseline; margin: 20px 10px 20px 0px;">'.
				'<button data-toggle="dropdown" class="btn btn-default dropdown-toggle" style="font-size: 12px;">'.$limit.'&nbsp;<span class="caret"></span></button>'.
				'<ul class="dropdown-menu">'.
					'<li><a href="'.  htmlspecialchars($this->Paginator->url(array_merge($options, array('limit' => 10, 'page' => 1)), false, $model)).'">10</a></li>'.
					'<li><a href="'.  htmlspecialchars($this->Paginator->url(array_merge($options, array('limit' => 25, 'page' => 1)), false, $model)).'">25</a></li>'.
					'<li><a href="'.  htmlspecialchars($this->Paginator->url(array_merge($options, array('limit' => 50, 'page' => 1)), false, $model)).'">50</a></li>'.
					'<li><a href="'.  htmlspecialchars($this->Paginator->url(array_merge($options, array('limit' => 100, 'page' => 1)), false, $model)).'">100</a></li>'.
				'</ul>'.
			'</div>';
		}
		return ($cppHtml ? $cppHtml : '').($html ? ('<ul class="pagination">'.$prevHtml.$html.$nextHtml.'</ul>') : '');
	}
	
	public function multilineText($text, $default = '&mdash;') {
		return $text ? str_replace("\n", '<br />', htmlspecialchars($text)) : $default;
	}
	
	public function datePickerConfig() {
		return array(
			'dateFormat' => 'yy-mm-dd',
			'firstDay' => 1,
			'dayNames' => array(__('Sekmadienis'), __('Pirmadienis'), __('Antradienis'), __('Trečiadienis'), __('Ketvirtadienis'), __('Penktadienis'), __('Šeštadienis')),
			'dayNamesMin' => array(__('Se'), __('Pr'), __('An'), __('Tr'), __('Ke'), __('Pe'), __('Še')),
			'dayNamesShort' => array(__('Sek'), __('Pir'), __('Ant'), __('Tre'), __('Ket'), __('Pen'), __('Šeš')),
			'monthNames' => array(__('Sausis'), __('Vasaris'), __('Kovas'), __('Balandis'), __('Gegužė'), __('Birželis'), __('Liepa'), __('Rugpjūtis'), __('Rugsėjis'), __('Spalis'), __('Lapkritis'), __('Gruodis')),
			'monthNamesShort' => array(__('Sau'), __('Vas'), __('Kov'), __('Bal'), __('Geg'), __('Bir'), __('Lie'), __('Rgp'), __('Rgs'), __('Spa'), __('Lap'), __('Gru')),
			'prevText' => __('Ankstesnis'),
			'nextText' => __('Kitas'),
		);
	}
	
	public function timePickerConfig() {
		return array(
			'template' => 'dropdown',
			'minuteStep' => 1,
			'showSeconds' => false,
			'secondStep' => 15,
			'defaultTime' => 'current',
			'showMeridian' => false,
			'showInputs' => true,
			'disableFocus' => true,
			'disableMousewheel' => false,
			'modalBackdrop' => false
		);
	}


    public function changeUnixSecondsToHumanTime($unix,$no_breaks = false)
    {
        $is_negative = false;
        if($unix < 0) {
            $is_negative = true;
            $unix = abs($unix);
        }
        $result = "";
        $points = array(
            __('val')   => 3600,
            __('min') => 60,
            __('s')   => 1
        );

        foreach ($points as $point => $value)
        { 
            if ($unix >= $value)
            {
                $fl = floor($unix / $value);
                $result .= $fl . " " . $point . " ";
                $unix -= $fl * $value;
            }
        }

        if($is_negative) {
            $result = "- " . $result;
        }

        if($no_breaks) {
            $result = str_replace(" ","&nbsp;",$result);
        }


        return $result;
    }

    public function changeToNonBreakingSpaces($input) {
        return str_replace(" ","&nbsp;",$input);
    }
    
    public function printAdditionalFields($structure, &$formHelper){
        foreach($structure as $columnname => $column){
            if(!is_array($column) || !isset($column['additional_info'])){continue;}
            if($column['type'] == 'number'){
                
            }elseif($column['type'] == 'select'){
                echo $formHelper->input('',array('type'=>'select', 'name'=>$columnname, 'options'=>$column['options'],'label'=>(isset($column['required'])?'* ':'').__($column['title']), 'class'=>'form-control','ng-model'=>'additionalPlanData.'.$columnname, 'required_text'=>$column['required']??0));
            }elseif($column['type'] == 'checkbox'){
                echo $formHelper->input('',array('type'=>'checkbox', 'id'=>'additionalPlanData.'.$columnname, 'name'=>$columnname, 'label'=>__($column['title']), 'class'=>'','ng-model'=>'additionalPlanData.'.$columnname));
            }elseif($column['type'] == 'type_and_select'){
                echo '<div class="btn-group wide-sell wide-sell-for-blind">
                    <label>'.__($column['title']).'</label>
                    <input style="width:100%;font-size:14px;padding:4px;" data-toggle="dropdown" class="dropdown-toggle" type="text" data-ng-model="additionalPlanData.'.$columnname.'"/>
                    <ul class="dropdown-menu smaller-items">
                        <li data-ng-repeat="item in '.$column['values'].' | additionalFieldsFilter:additionalPlanData.'.$columnname.'"><a data-ng-click="selectAdditionalField(item,\''.$columnname.'\')" href="javascript:void(0)">{{item}}</a></li>
                    </ul></div>';
            }else{
                echo $formHelper->input('', array('type'=>'text', 'name'=>$columnname, 'label'=>__($column['title']), 'class'=>'form-control','ng-model'=>'additionalPlanData.'.$columnname));
            }
        }
    }
	
    public function addLanguages($possibleLanguages){
        //Configure::read('languagesDescription');
        $html = '';
        foreach($possibleLanguages as $languageSlug => $languageTitle){
            $html .= '<li style="display: inline-block; margin-left: 3px;"><a class="'.current(explode('_', $languageSlug)).'" href="javascript:void(0)" title="'.$languageTitle.'" onclick="langselect(\''.$languageSlug.'\',this)"><img src="'.Router::url('/files/flags/'.$languageSlug.'.png').'" alt="'.$languageTitle.'" /></a></li>';
        }
        return sprintf('<ul style="list-style-type:none; display: inline-block;" class="langselect">%s</ul>',$html);
    }
	
	
}
