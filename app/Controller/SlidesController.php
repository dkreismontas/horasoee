<?php
class SlidesController extends AppController{
    
    public $name = 'Slides';
    public $uses = array('SlidesCollection','Sensor');
    private static $defaultSettings = array(
        'transition_interval'=>10, 'hidden'=>1, 'width'=>1200, 'height'=>700
    );

    public function beforeFilter(){
        $parameters = array(&$this);
        $this->Help->callPluginFunction('Slides_beforeBeforeFilter_Hook', $parameters, Configure::read('companyTitle'));
        parent::beforeFilter();
    }
    
    /** @requireAuth Skaidrių sąrašas */
    public function display(){
        $selectedSensors = Configure::read('user')->selected_sensors;
        $slidesCollections = $this->SlidesCollection->find('all', array('conditions'=>array('SlidesCollection.hidden'=>0)));
        $slidesCollections = array_filter($slidesCollections, function($collection)use($selectedSensors){
            $slides = json_decode($collection['SlidesCollection']['slides_json']);
            foreach($slides as $slide){
                if(!is_array($slide->sensors)){ return false; }
                if(!empty(array_diff($slide->sensors, $selectedSensors))){ return false; }
            }
            return true;
        });
        $this->set(array(
            'slidesCollections'=>$slidesCollections,
            'hasAccessToEdit'=>$this->hasAccessToAction(array('controller'=>'slides', 'action'=>'edit')),
            'hasAccessToRemove'=>$this->hasAccessToAction(array('controller'=>'slides', 'action'=>'remove_collection')),
        ));
    }
    
    /** @requireAuth Redaguoti skaidres */
    public function edit($slidesCollectionId = 0){
        if(!$slidesCollectionId){
            $this->SlidesCollection->save(self::$defaultSettings);
            $this->redirect(array($this->SlidesCollection->id));
        }elseif(!empty($this->request->data)){
            $this->SlidesCollection->id = $slidesCollectionId;
            $this->SlidesCollection->save(array('hidden'=>0)+$this->request->data['SlidesCollection']);
            $this->redirect(array('action'=>'display'));
        }
        $this->request->data = $this->SlidesCollection->findById($slidesCollectionId);
        $viewVariables = array(
            'h1_for_layout' => __('Skaidrės redagavimas'),
            'icon_for_layout' => 'iconfa-laptop',
            'slidesCollectionId' => $slidesCollectionId,
            'viewTypes' => array(0=>__('Apibendrintas'), 1=>__('Valandinis'),2=>__('24h grafikas')),
            'sensors'=>$this->Sensor->getAsSelectOptions(false, array('Sensor.pin_name <>'=>'')),
            'bredcrumbs'=>array(
                new MainMenuItem(__('Skaidrių rinkiniai'), Router::url(array('plugin'=>'','controller' => 'slides', 'action' => 'display')), (strtolower($this->name) == 'slides' &&  $this->params->action == 'display'), 'iconfa-laptop'),
            ),
            'sensorsLabel'=>__('Darbo centrai')
        );
        $parameters = array(&$viewVariables);
        $pluginData = $this->Help->callPluginFunction('SlidesAfterEditHook', $parameters, Configure::read('companyTitle'));
        $this->set($viewVariables);
    }

    /** @requireAuth Peržiūrėti skaidrių turinį */
    public function show($slidesCollectionId = 0){
        $this->layout = 'full_page';
        $slidesCollection = $this->SlidesCollection->findById($slidesCollectionId);
        $this->SlidesCollection->attachLinkToSlide($slidesCollection);
        $viewVariables = array(
            'slidesCollection'=>$slidesCollection
        );
        $parameters = array(&$viewVariables);
        $pluginData = $this->Help->callPluginFunction('SlidesAfterShowHook', $parameters, Configure::read('companyTitle'));
        $this->set($viewVariables);
    }

    /** @requireAuth Pašalinti skaidrių kolekciją */
    public function remove_collection($slidesCollectionId = 0){
        $this->SlidesCollection->id = $slidesCollectionId;
        $this->SlidesCollection->delete();
        $this->redirect(array('action'=>'display'));
    }
    
    /** @requireAuth Pridėti skaidrę į kolekciją */
    public function add_slide($slidesCollectionId = 0){
        if(!$this->request->is('ajax') || !$slidesCollectionId){ die(); }
        $this->SlidesCollection->id = $slidesCollectionId;
        $collectionData = $this->SlidesCollection->read();
        if(empty($this->request->data)){
            $slidesList = trim($collectionData['SlidesCollection']['slides_json'])?json_decode($collectionData['SlidesCollection']['slides_json'],true):array();
            $slidesList[] = array('type'=>'', 'sensors'=>'');
            $this->SlidesCollection->save(array('slides_json'=>json_encode($slidesList)));
            echo json_encode($slidesList);
            die();
        }
    }
    
    public function get_slides($slidesCollectionId = 0){
        if(!$this->request->is('ajax') || !$slidesCollectionId){ die(); }
        $this->SlidesCollection->id = $slidesCollectionId;
        $collectionData = $this->SlidesCollection->read();
        echo json_encode(array(
            'slidesList'=>trim($collectionData['SlidesCollection']['slides_json'])?json_decode($collectionData['SlidesCollection']['slides_json'],true):array(),
        ));
        die();
    }
    
    /** @requireAuth Pašalinti skaidrę iš kolekcijos */
    public function remove_slide($slidesCollectionId = 0){
        if(!$this->request->is('ajax') || !$slidesCollectionId){ die(); }
        $this->SlidesCollection->id = $slidesCollectionId;
        $collectionData = $this->SlidesCollection->read();
        $slidesList = json_decode($collectionData['SlidesCollection']['slides_json'],true);
        if(isset($slidesList[$this->request->data['slideKey']])){
            unset($slidesList[$this->request->data['slideKey']]);
        }
        $this->SlidesCollection->save(array('slides_json'=>json_encode($slidesList)));
        die();
    }
    
    public function change_value($slidesCollectionId = 0){
        if(!$this->request->is('ajax') || !$slidesCollectionId){ die(); }
        $this->SlidesCollection->id = $slidesCollectionId;
        $collectionData = $this->SlidesCollection->read();
        $slidesList = json_decode($collectionData['SlidesCollection']['slides_json'],true);
        if(isset($this->request->data['slideKey'])){
            $slidesList[$this->request->data['slideKey']] = $this->request->data['saveData'];
        }elseif(isset($this->request->data['changePos'])){
            $slidesList = $this->request->data['changePos'];
        }
        $this->SlidesCollection->save(array('slides_json'=>json_encode($slidesList)));
        die();
    }
    
}
