<?php
App::uses('OeeMpmSoapServer', 'Model');
class OeeMpmController extends AppController{
    
    public $name = 'OeeMpm';
    public $uses = array('OeeMpmSoapServer');
    
    public function beforeFilter(){ 
        parent::beforeFilter();
        $this->Auth->allow('initiate_data_exchange');
    }
    
    public function initiate_data_exchange(){
        $this->layout = 'ajax';
        $params = array(
            'location'=>Router::url(array('plugin'=>false, 'controller'=>'oee_mpm', 'action'=>'initiate_data_exchange'),true),
            'uri'=>'soapserver'
        );
        $server = new SoapServer(null, $params);
        $server->setClass('OeeMpmSoapServer');
        $server->handle(); 
        $this->render(false);
    }
    
     public function test(){
         pr($this->OeeMpmSoapServer->get_data_from_oee());
         die();
     }
    
}
