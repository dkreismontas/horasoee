<?php

App::uses('Sensor', 'Model');
App::uses('LossType', 'Model');

/**
 * @property LossType $LossType
 */
class LossTypesController extends AppController {
	
	const ID = 'loss_types';
	const MODEL = LossType::NAME;
	
	public $uses = array(self::MODEL,'Line');
	
	/** @requireAuth Peržiūrėti nuostolių tipus */
	public function index() {
		$this->requestAuth(true);
		$availableLines = array_unique($this->Line->getAsSelectOptions(false,true));
		$list = $this->LossType->find('all',array('conditions'=>array(
		    'OR'=>array(
                'FIND_INTERSECT(LossType.line_ids, \''.implode(',', $availableLines).'\', \'\') != \'\'',
                'LossType.line_ids'=>''
            )
		)));
		$this->set(array(
			'title_for_layout' => __('Nuostolių tipai'),
			'list' => $list,
			'model' => self::MODEL,
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?'),
			'lines' => $this->Line->find('list',array('fields'=>array('Line.id','Line.name'))),
			'typeOptions' => Sensor::getTypes(),
		));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('LossTypes_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Redaguoti nuostolių tipus */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		
		if (empty($this->request->data)) {
			$this->request->data = $item = $this->LossType->findById($id);
		} else {
			$item = null;
            $this->request->data['LossType']['line_ids'] = isset($this->request->data['LossType']['line_ids']) && !empty($this->request->data['LossType']['line_ids'])?implode(',', $this->request->data['LossType']['line_ids']):'';
            $this->request->data['LossType']['sensor_types'] = isset($this->request->data['LossType']['sensor_types']) && is_array($this->request->data['LossType']['sensor_types'])?implode(',', $this->request->data['LossType']['sensor_types']):'';
			if ($this->LossType->save($this->request->data)) {
				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			} else {
				//$this->Session->setFlash(__('Nepavyko išsaugoti įrašo').': '.print_r($this->LossType->validationErrors, true), 'default', array(), 'saveMessage');
				$this->Session->setFlash(__('Nepavyko išsaugoti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$title = $item ? sprintf(__('Nuostolių tipas %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Naujas nuostolių tipas');
		$this->set(array(
		    'lines'=>$this->Line->getAsSelectOptions(false),
			'title_for_layout' => Settings::translate($title),
			'h1_for_layout' => Settings::translate($title),
			'model' => self::MODEL,
			'listUrl' => $listUrl,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true),
			'typeOptions' => array_filter(Sensor::getTypes())
		));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('LossTypes_AfterEdit_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Pašalinti nuostolių tipus */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		try {
			if ($this->LossType->delete($id, false)) {
				$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		} catch (PDOException $ex) {
			$code = ''.$ex->getCode();
			if (substr($code, 0, 2) == '23') {
				$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$this->redirect($listUrl);
	}
	
}
