<?php

App::uses('Plan', 'Model');
App::uses('Line', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Status', 'Model');
App::uses('ApprovedOrder', 'Model');

/**
 * @property Plan $Plan
 * @property Line $Line
 * @property Sensor $Sensor
 * @property ApprovedOrder $ApprovedOrder
 */
class PlansController extends AppController {
	
	const ID = 'plans';
	const MODEL = Plan::NAME;
	
	public $uses = array(self::MODEL, Line::NAME, Sensor::NAME, ApprovedOrder::NAME,'Status', 'Settings','Log');
	
	public $components = array('Session', 'Paginator');
	
	/** @requireAuth Peržiūrėti planus */
	public function index() {
		$this->requestAuth(true);
        $parameters = array();
        if(!$sensorOptions = $this->Help->callPluginFunction('Sensor_getAsSelectOptions_Hook', $parameters, Configure::read('companyTitle'))){
            $sensorOptions = $this->Sensor->getAsSelectOptions(true, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1));
        }

        $forced_sensorId = self::$user->sensorId;
        if($forced_sensorId != null && $forced_sensorId != 0) {
            $sensorId = $forced_sensorId;
        } else {
            $sensorId = isset($this->request->params['named']['sensorId']) ? $this->request->params['named']['sensorId'] : null;
        }
		$limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
		if (!$limit) $limit = 200;
		//$list = $this->Plan->withRefs()->find('all');
		$arr = array('limit' => $limit, 'order' => array(self::MODEL.'.has_problem' => 'desc', self::MODEL.'.position' => 'asc', self::MODEL.'.end' => 'asc'));

        // Find all plan ids where the plan is finished
        $conds[self::MODEL.'.disabled'] = 0;
        $conds[self::MODEL.'.sensor_id'] = Configure::read('user')->selected_sensors;
		if ($sensorId && isset($sensorOptions[$sensorId])) {
            /*$finished_plans = $this->ApprovedOrder->find('list',array(
                'fields'=>array('plan_id'),
                'conditions'=>array(
                    'status_id'=>ApprovedOrder::STATE_COMPLETE,
                    'sensor_id'=>$sensorId
                )
            ));*/
			$conds[self::MODEL.'.sensor_id'] = $sensorId;
		} else {
            /*$finished_plans = $this->ApprovedOrder->find('list',array(
                'fields'=>array('plan_id'),
                'conditions'=>array(
                    'status_id'=>ApprovedOrder::STATE_COMPLETE
                )
            ));*/
			$sensorId = null;
		}

		if (!empty($conds)) $arr['conditions'] = $conds;
        /*$finished_plans = array_values($finished_plans);
        if(isset($this->request->params['named']['control'])) {
            $arr['conditions'][Plan::NAME.".control"] = "T";
        }*/

        // Add condition to not include finished plans
        //$arr['conditions'][] = array('Plan.id !='=>$finished_plans);
        $planTypes = array(
            array(ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_IN_PROGRESS),
            array(ApprovedOrder::NAME.'.status_id' => null)
        );
        $arr['conditions']['OR'] = $planTypes;  //isvedame tik tuos uzsakymus, kurie neturi baigto approved_order statuso
        $arr['conditions'][] = 'Sensor.pin_name <> \'\'';
        if(isset($this->request->query['order_number']) && trim($this->request->query['order_number'])){ $arr['conditions'][] = 'LOWER(Plan.mo_number) LIKE \'%'.mb_strtolower($this->request->query['order_number']).'%\''; }
        if(isset($this->request->query['product_code']) && trim($this->request->query['product_code'])){ $arr['conditions'][] = 'LOWER(Plan.production_code) LIKE \'%'.mb_strtolower($this->request->query['product_code']).'%\''; }
        $parameters = array(&$arr, &$this->params);
        $this->Help->callPluginFunction('beforePlanPaginateHook', $parameters, Configure::read('companyTitle'));
		$this->Paginator->settings = $arr;
		try {
		    //$condForAppOrd = array('conditions'=>array('ApprovedOrder.id = (SELECT MAX(id) FROM '.$this->ApprovedOrder->tablePrefix.$this->ApprovedOrder->useTable.' WHERE plan_id = Plan.id )'));
			$condForAppOrd = array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE, 'ApprovedOrder.sensor_id = Plan.sensor_id')); //jungiame tik su baigtais uzsakymais
			$this->Plan->withRefs(false, $condForAppOrd); 
			$list = $this->Paginator->paginate(self::MODEL);
		} catch (NotFoundException $ex) {
			$this->request->params['named']['page'] = 1;
			$this->Paginator->paginate(self::MODEL);
			$url = array('controller' => self::ID, 'action' => 'index');
			if (isset($this->request['paging'][self::MODEL]['pageCount'])) {
				$url['page'] = max(intval($this->request['paging'][self::MODEL]['pageCount']), 1);
			}
			if ($limit) { $url['limit'] = $limit; }
			if ($sensorId) { $url['sensorId'] = $sensorId; }
			$this->redirect(Router::url($url, true));
		}
        $listFixed = array();
        foreach($list as $item) {
            $last_ao = $this->ApprovedOrder->find('first',array('conditions'=>array('plan_id'=>$item['Plan']['id'], 'sensor_id'=>$item['Plan']['sensor_id']),'order'=>array('end DESC')));
            if(!empty($last_ao)) {
                $item['ApprovedOrder'] = $last_ao['ApprovedOrder'];
            }
            $listFixed[] = $item;
        }
        $list = $listFixed;

        $statuses = $this->Status->find('list',array('id,name'));

		$this->set(array(
			'title_for_layout' => __('Planai'),
			'arr' => $arr,
			'list' => $list,
            'statuses' => $statuses,
            'forced_sensorId'=>$forced_sensorId,
			'sensorId' => $sensorId,
			'sensorOptions' => $sensorOptions,
			'model' => self::MODEL,
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'filterUrl' => Router::url(array('controller' => self::ID, 'action' => 'index', 'limit' => $limit, 'sensorId' => '__DATA__')),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?'),
			'frozing_orders_type' => intval($this->Settings->getOne('frozing_orders_type'))
		));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Plans_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
	}

    public function create() {
        $this->requestAuth(true);

        if(!empty($this->request->data))
        {
//            var_dump($this->request);
//            die();

            $identical_plan = $this->Plan->find('first',array(
               'conditions'=>array(
                   'sensor_id'=>$this->request->data['Plan']['sensor_id'],
                   'mo_number'=>$this->request->data['Plan']['mo_number']
               )
            ));
            if(!empty($identical_plan)) {
                $this->set(array(
                    'errors'=> array('Planas su šiuo MO jau egzistuoja')
                ));
            } else {
                $this->Plan->save($this->request->data);
                $this->redirect('/plans/' . $this->Plan->id . '/edit');
                return;
            }
        }

        $this->set(array(
            'lineOptions' => $this->Line->getAsSelectOptions(),
            'sensorOptions' => $this->Sensor->getAsSelectOptions(),
        ));
    }
	
	/** @requireAuth Redaguoti planus */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		$refUri = $this->request->referer(true);
		
		$item = $this->Plan->withRefs()->findById($id);
        $all_approved_orders = $this->ApprovedOrder->find('first',array('conditions'=>array('plan_id'=>$id),'order'=>array('end DESC')));
        if(!empty($all_approved_orders)) {
            $item['ApprovedOrder'] = $all_approved_orders['ApprovedOrder'];
        }
//        foreach($all_approved_orders as $ao) {
//            var_dump($ao);
//            die();
//            if($ao['ApprovedOrder']['status_id'] == 4) {
//                $item['ApprovedOrder'] = $ao;
//                break;
//            }
//        }

		$canEdit = ($item && isset($item[ApprovedOrder::NAME]['status_id']) && $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_IN_PROGRESS);
		if (empty($this->request->data)) {
			$this->request->data = $item;
		} else if ($canEdit) {
			if ($this->Plan->save(array(Plan::NAME => array('id' => $this->request->data[Plan::NAME]['id'], 'quantity' => $this->request->data[Plan::NAME]['quantity'])))) {
				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			}
		}
		
		$title = $item ? sprintf(__('Planas  %s (ID: %d)'), '', $item[self::MODEL]['id']) : __('Naujas planas');
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'model' => self::MODEL,
			'item' => $item,
			'canEdit' => $canEdit,
			'lineOptions' => $this->Line->getAsSelectOptions(),
			'sensorOptions' => $this->Sensor->getAsSelectOptions(true, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1)),
			'listUrl' => ($refUri ? Router::url($refUri, true) : $listUrl),
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true)
		));
	}

    public function create_approved_order() {
        $plan = $this->Plan->findById($this->request->params['named']['planId']);
        $bq = intval($this->request->data(ApprovedOrder::NAME.'.confirmed_box_quantity'));
        $iq = intval($this->request->data(ApprovedOrder::NAME.'.confirmed_item_quantity'));
        $this->ApprovedOrder->create();
        $item = array(
            'created' => date("Y-m-d H:i:s"),
            'sensor_id' => $plan['Plan']['sensor_id'],
            'plan_id' => $plan['Plan']['id'],
            'status_id' => (isset($this->request->data['production_complete'])) ? ApprovedOrder::STATE_COMPLETE : ApprovedOrder::STATE_POSTPHONED,
            'end' => date("Y-m-d H:i:s"),
            'quantity' => 0,//$bq * $plan[Plan::NAME]['box_quantity'],
            'box_quantity' => 0,//$bq,
            'confirmed_quantity' => (($bq * $plan[Plan::NAME]['box_quantity']) + $iq),
            'confirmed' => 1
        );
        $this->ApprovedOrder->save($item);
        $item = $this->ApprovedOrder->withRefs()->findById($this->ApprovedOrder->id);
        App::uses('Exporter', 'Controller/Component');

        if(isset($this->request->data['production_complete'])) {
            Exporter::exportCompleted($item);
        }
        else {
            Exporter::exportPostphoned($item);
        }

        $this->redirect('/approved_orders/'.$this->ApprovedOrder->id.'/edit');
    }
	
	/** @requireAuth Pašalinti planus */
	public function remove(){
		$this->requestAuth(true);
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		$id = $this->request->params['id'];
		$item = $this->Plan->withRefs()->findById($id);
		$states = array(); // because plan can have multiple aproved orders
		if ($item && isset($item[ApprovedOrder::NAME]['id'])) {
			$aos = $this->ApprovedOrder->find('all', array('conditions' => array(ApprovedOrder::NAME.'.plan_id' => $id)));
			foreach ($aos as $ao) { $states[intval($ao[ApprovedOrder::NAME]['status_id'])] = 1; }
		}
		if ($item && (!isset($item[ApprovedOrder::NAME]['id'])
					|| $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_CANCELED
					|| $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED
					|| isset($states[ApprovedOrder::STATE_CANCELED])
					|| isset($states[ApprovedOrder::STATE_POSTPHONED])
				)) {
			try {
				if (!isset($item[ApprovedOrder::NAME]['id'])) {
				    if ($this->Plan->delete($id, false)) {
                        $this->Log->write("Pasalintas planas: " . json_encode($item) . ";");
						$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
					} else {
						$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
					}
				} else {
					$this->Plan->removeWithDeps($id);
				}
			} catch (PDOException $ex) {
				$code = ''.$ex->getCode();
				if (substr($code, 0, 2) == '23') {
					$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
				} else {
					$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
				}
			}
		}
		
		$refUri = $this->request->referer(true);
		if ($refUri && $refUri != '/') {
			$this->redirect(Router::url($refUri, true));
		} else {
			$this->redirect($listUrl);
		}
	}
	
	/** @requireAuth Pakeisti plano duomenis į alternatyvius */
	public function change_data($planId){
		$this->requestAuth(true);
		if($this->request->is('ajax') && sizeof(array_intersect_key($this->request->data, array_flip(array('sensor_id', 'plan_id')))==2 )){
			$possibilityModel = ClassRegistry::init('Possibility');
			echo json_encode(
				array_diff_key(current($possibilityModel->find('first',array('conditions'=>$this->request->data))), array_flip(array('id')))
			);
			die();
		}elseif($this->request->is(array('post', 'put'))){
			/*$hasProblem = 0;
			foreach($this->request->data['Plan'] as $key => $value){
				if(floatval($value) <= 0){
					$hasProblem = 1;
					break;
				}
			}
			$this->request->data['Plan']['has_problem'] = $hasProblem;*/
            $possibilityModel = ClassRegistry::init('Possibility');	
            $possibilityModel->virtualFields = array('step'=>'speed');
            $possibility = $possibilityModel->find('first', array('conditions'=>array('Possibility.plan_id'=>$this->request->data['Plan']['id'], 'Possibility.sensor_id'=>$this->request->data['Plan']['sensor_id'])));
			$updateData = array_merge(
    			 isset($possibility['Possibility'])?$possibility['Possibility']:array(), 
    			 array('id'=>$this->request->data['Plan']['id'], 'sensor_id'=>$this->request->data['Plan']['sensor_id'])
            );
			$this->Plan->save($updateData);
			//$this->redirect(array('action'=>'index'));
		}
        $planInProgress = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.plan_id'=>$planId, 'ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS)));
		if(!$planInProgress){
		    $this->Plan->withRefs(false)->bindModel(array('hasMany'=>array('Possibility')));
		}else{
		    $this->Plan->withRefs(false);
		}
		$this->request->data = $this->Plan->find('first', array('recursive'=>2,'conditions'=>array('Plan.id'=>$planId)));
		$this->set(array(
			'columns'	=> array(
				'id'=>array('title'=>'', 'type'=>'hidden'),
				'step'=>array('title'=>__('Greitis'),'rename'=>'speed','type'=>'hidden'), 
				'client_speed'=>array('title'=>__('Kliento greitis'), 'type'=>'hidden'),
				'preparation_time'=>array('title'=>__('Pasiruošimo laikas'),'type'=>'hidden'), 
				'production_time'=>array('title'=>__('Gamybos laikas'),'type'=>'hidden')
			),
		));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Plans_ChangeData_After_Hook', $parameters, Configure::read('companyTitle'));
	}

    public function change_order_position(){
        parse_str($this->request->data['order'],$ordersSort);
        foreach($ordersSort['order'] as $key => $order_place){
            $this->Plan->updateAll(array('position'=>$key),array('id'=>$order_place, 'Plan.has_problem'=>0));
        }
        die();
    }
    //uzsakymu bloko saldymas
    public function freeze_order(){
        if(!$this->request->is('ajax')) die();
        $this->Plan->id = $this->request->data['order_id'];
        $position_frozen = $this->request->data['frozenStatus'];
        $currentOrder = $this->Plan->find('first',array('conditions'=>array('Plan.id'=>$this->Plan->id)));
        $this->Plan->withRefs(false, array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE)));
        $frozing_orders_type = intval($this->Settings->getOne('frozing_orders_type'));
        if($frozing_orders_type==1){
            $this->Plan->updateAll(array('position_frozen'=>$position_frozen),array('Plan.id' => $this->Plan->id, 'Plan.has_problem'=>0));
        }else{
            if($position_frozen){
                $this->Plan->updateAll(array('position_frozen'=>$position_frozen),array('Plan.position <=' => $currentOrder['Plan']['position'],'ApprovedOrder.id IS NULL', 'Plan.sensor_id'=>$currentOrder['Plan']['sensor_id'], 'Plan.has_problem'=>0));
            }else{
                $this->Plan->updateAll(array('position_frozen'=>$position_frozen),array('Plan.position >=' => $currentOrder['Plan']['position'], 'ApprovedOrder.id IS NULL', 'Plan.sensor_id'=>$currentOrder['Plan']['sensor_id'], 'Plan.has_problem'=>0));
            }
        }
        die();
    }
	
}
