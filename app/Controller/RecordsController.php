<?php

App::uses('Record', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');

/**
 * @property Record $Record
 * @property Sensor $Sensor
 * @property Branch $Branch
 */
class RecordsController extends AppController {
	
	const ID = 'records';
	const MODEL = Record::NAME;
	
	public $uses = array(self::MODEL, Sensor::NAME, Branch::NAME);
	
	public $components = array('Session', 'Paginator');
	
	/** @requireAuth Peržiūrėti jutiklių įrašus */
	public function index() {
        $this->Record->unbindModel(array('belongsTo'=>array('Sensor')),false);
		$this->requestAuth(true);
		$sensorOptions = $this->Sensor->getAsSelectOptions(true);
		$sensorId = isset($this->request->params['named']['sensorId']) ? $this->request->params['named']['sensorId'] : null;
		$limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
		if (!$limit) $limit = 100;
		//$list = $this->Record->withRefs(true)->find('all');
		$arr = array('limit' => $limit, 'order' => array(self::MODEL.'.created' => 'desc'), 'fields'=>array('Record.*','Sensor.*','Plan.*','Shift.name'));
		$conds = array();
		if ($sensorId && isset($sensorOptions[$sensorId])) {
			$conds[self::MODEL.'.sensor_id'] = $sensorId;
		} else {
			$sensorId = null;
            $conds[self::MODEL.'.sensor_id'] = Configure::read('user')->selected_sensors;
		}
		if(isset($_GET['date'])){
			 $date = explode(' ~ ',$_GET['date']);
			 if(isset($date[0])) $conds[self::MODEL.'.created >='] = $date[0];
			 if(isset($date[1])) $conds[self::MODEL.'.created <'] = $date[1];
			 $this->set('date', $_GET['date']);
		}
        if(isset($this->params['named']['last_records'])){
            $lastRecordsIds = $this->Record->find('all', array('fields'=>array('MAX(id) AS id'), 'group'=>array('sensor_id')));
            $conds['Record.id'] = Set::extract('/0/id',$lastRecordsIds);
        }
		if (!empty($conds)){
		    $arr['conditions'] = $conds;
            $dataSum = $this->Record->find('first', array('fields'=>array('SUM(quantity) AS totalQ, SUM(unit_quantity) AS totalUQ'), 'conditions'=>$conds));
            $this->set(compact('dataSum'));        
        }
		$this->Paginator->settings = $arr;
		try {
			$this->Record->withRefs(true, false);
            $list = $this->Paginator->paginate(self::MODEL);
		} catch (NotFoundException $ex) {
			$this->request->params['named']['page'] = 1;
			$this->Paginator->paginate(self::MODEL);
			$url = array('controller' => self::ID, 'action' => 'index');
			if (isset($this->request['paging'][self::MODEL]['pageCount'])) {
				$url['page'] = max(intval($this->request['paging'][self::MODEL]['pageCount']), 1);
			}
			if ($limit) { $url['limit'] = $limit; }
			if ($sensorId) { $url['sensorId'] = $sensorId; }
			$this->redirect(Router::url($url, true));
		}
		$branches = $this->Branch->find('all');
		$branchIndex = array();
		foreach ($branches as $b) {
			$branchIndex[$b[Branch::NAME]['id']] = $b[Branch::NAME];
		}
		foreach ($list as $idx => $li) {
			$list[$idx][Branch::NAME] = isset($branchIndex[$li[Sensor::NAME]['branch_id']]) ? $branchIndex[$li[Sensor::NAME]['branch_id']] : null;
		}
		$this->set(array(
			'title_for_layout' => __('Įrašai'),
			'list' => $list,
			'sensorId' => $sensorId,
			'sensorOptions' => $sensorOptions,
			'model' => self::MODEL,
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'filterUrl' => Router::url(array('controller' => self::ID, 'action' => 'index', 'limit' => $limit, 'sensorId' => '__DATA__')),
			'sensorsLabel'=>__('Darbo centras')
		));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Records_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
	}
	
}
