<?php

App::uses('Branch', 'Model');
App::uses('User', 'Model');

/**
 * @property Branch $Branch
 * @property User $User
 */
class BranchesController extends AppController {
	
	const ID = 'branches';
	const MODEL = Branch::NAME;
	
	public $uses = array(self::MODEL, User::NAME, 'Factory');
	
	/** @requireAuth Peržiūrėti padalinius */
	public function index() {
		$this->requestAuth(true);
		$list = $this->Branch->withRefs()->find('all');
		$this->set(array(
			'title_for_layout' => __('Padaliniai'),
			'list' => $list,
			'model' => self::MODEL,
			'userOptions' => $this->User->getAsSelectOptions(false),
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?')
		));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Branches_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Redaguoti padalinius */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
        $sensorOptions = array();
		if (empty($this->request->data)) {
			$this->request->data = $item = $this->Branch->findById($id);
            $passSensorsList = false;
			if($item) {
                $branchBelongsToOtherSensors = $this->Sensor->find('first', array('conditions' => array('Sensor.branch_id' => $id, 'Sensor.id <>' => $item['Branch']['sensor_id'])));
                if (empty($branchBelongsToOtherSensors)) {
                    $passSensorsList = true;
                }
            }
            if(!$item || $passSensorsList){
                if (!$sensorOptions = $this->Help->callPluginFunction('Sensor_getAsSelectOptions_Hook', $parameters, Configure::read('companyTitle'))) {
                    $sensorOptions = $this->Sensor->getAsSelectOptions(true);
                }
            }
			//$this->request->data['Branch']['quality_user_id'] = explode(',', $this->request->data['Branch']['quality_user_id']);
		} else {
		    $this->Branch->requestData = $this->request->data;
			$item = null;
            $this->request->data['Branch']['quality_user_id'] = implode(',',$this->request->data['Branch']['quality_user_id']?$this->request->data['Branch']['quality_user_id']:array());
			if ($this->Branch->save($this->request->data)) {
				if(isset($this->request->data['Branch']['sensor_id']) && $this->request->data['Branch']['sensor_id']>0){
				    $this->Sensor->id = $this->request->data['Branch']['sensor_id'];
				    $this->Sensor->save(array('branch_id'=>$this->Branch->id));
                }
			    $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			}
		}
		$title = $item ? sprintf(__('Padalinys  %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Naujas padalinys');
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'model' => self::MODEL,
			'userOptions' => $this->User->getAsSelectOptions(false),
			'listUrl' => $listUrl,
			'sensorOptions' => $sensorOptions,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true),
			'factoryOptions' => array(0=>'---')+$this->Factory->find('list', array('fields'=>array('id','name'))),
		));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Branches_AfterEdit_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Pašalinti padalinius */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		try {
			if ($this->Branch->delete($id, false)) {
				$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		} catch (PDOException $ex) {
			$code = ''.$ex->getCode();
			if (substr($code, 0, 2) == '23') {
				$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$this->redirect($listUrl);
	}
	
}
