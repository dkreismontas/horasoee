<?php

/**
 * Description of LoginController
 * @property CakeRequest $request
 * @property SessionComponent $Session
 * @property User $User
 */
class LoginController extends AppController {
	
	public $uses = array('User','Sensor');
	
	public function index() {
		$this->layout = 'bare';
		$this->set('bodyClass', array('loginpage'));
		if ($this->request->data('login')) {
			$uname = $this->request->data('login_user');
			$upass = $this->request->data('login_pass');
			$returnUrl = $this->request->data('return');
            $login = $this->Session->read('Auth.user');
            $user = $this->User->find('first', array('conditions' => array('User.username' => $uname, 'User.password' => md5($uname.$upass))));
            if($user && $user['User']['taken_over_user_id'] > 0){
                $user = $this->User->findById($user['User']['taken_over_user_id']);
            }
			if ($uname && $upass && $user) {
                $this->Log->write("Successful log in. User:".$uname);
				$this->Session->write('Auth.user', $user);
				if(!isset($_COOKIE['login']) && !empty($user)){
	                $this->Cookie->write('login',base64_encode(serialize($this->Session->read('Auth'))),true,'1 year','/');
	            }
				if ($returnUrl) {
					$this->redirect($returnUrl);
				} else {
					$fUser = $this->User->withRefs()->findById($user[User::NAME]['id']);
					if ($fUser && !empty($fUser) && isset($fUser[Sensor::NAME]['id']) && isset($fUser[Line::NAME]['id'])) {
					    if(sizeof(explode(',',$fUser['User']['sensor_id'])) > 1){
                            $this->redirect(Router::url(array('controller' => 'login', 'action' => 'choose_sensor'), true));
                        }else{
                            $this->User->updateAll(array('active_sensor_id'=>0), array('id'=>$fUser['User']['id']));
						    $this->redirect(Router::url(array('controller' => 'work-center', 'action' => 'index'), true));
                        }
					}
					$this->redirect(Router::url('/', true));
				}
			} else {
				$this->Session->setFlash(__('Neteisingas vartotojo vardas ar slaptažodis'), 'default', array(), 'loginMessage');
				$this->Session->write('Auth.user', null);
			}
		} else {
			$uname = '';
			$upass = '';
			$returnUrl = $this->request->query('return');
		}
		$this->set(array(
			'uname' => $uname,
			'upass' => $upass,
			'returnUrl' => $returnUrl,
			'formUrl' => Router::url(array('controller' => 'login', 'action' => 'index')),
			'title_for_layout' => __('Prisijungti')
		));
	}

    public function choose_sensor($choosedSensorId = 0){
        $this->layout = 'bare';
        $this->set('bodyClass', array('loginpage'));
        $fUser = $this->User->withRefs()->findById(self::$user->id);
        if($fUser){
            $passibileSensorsIds = explode(',',$fUser['User']['sensor_id']);
            if($choosedSensorId > 0 && in_array($choosedSensorId, $passibileSensorsIds)){
                $this->User->updateAll(array('active_sensor_id'=>$choosedSensorId), array('id'=>$fUser['User']['id']));
                $this->redirect(Router::url(array('controller' => 'work-center', 'action' => 'index'), true));
            }
            if(sizeof($passibileSensorsIds) <= 1) $this->redirect(Router::url(array('controller' => 'login', 'action' => 'index'), true));
            $sensors = $this->Sensor->find('all', array('conditions'=>array('Sensor.id'=>$passibileSensorsIds)));
            $this->set(compact('sensors'));
        }
    }
	
	public function logout() {
	    $this->User->updateAll(array('taken_over_user_id'=>0));
		//$this->Session->write('Auth.user', null);
        $this->Cookie->delete('login'); 
        $this->Session->delete('Auth.User');
		$this->Auth->logout();
		$this->redirect(Router::url(array('controller' => 'login', 'action' => 'index'), true));
	}
	
}
