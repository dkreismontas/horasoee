<?php

App::uses('ApprovedOrder', 'Model');
App::uses('Sensor', 'Model');
App::uses('Plan', 'Model');
App::uses('Loss', 'Model');
App::uses('LossType', 'Model');
App::uses('Settings', 'Model');
App::uses('PartialQuantity', 'Model');
App::uses('Branch', 'Model');
App::uses('Exporter', 'Controller/Component');
App::uses('CakeEmail', 'Network/Email');

/**
 * @property ApprovedOrder $ApprovedOrder
 * @property Sensor $Sensor
 * @property Loss $Loss
 */
class ApprovedOrdersController extends AppController {

    const ID = 'approved_orders';
    const MODEL = ApprovedOrder::NAME;

    public $uses = array(self::MODEL, Sensor::NAME, Loss::NAME, Settings::NAME, 'Plan', 'PartialQuantity', 'Branch','LossType','FoundProblem','QualityFactor','Shift','Log','ShiftTimeProblemExclusion','OrderCalculation','Problem','Record','Verification');

    public $components = array('Session', 'Paginator');

    /** @requireAuth Peržiūrėti patvirtintus užsakymus */
    public function index() {
        $this->requestAuth(true);
        $this->Session->write('approoved_orders_last_url_params', $this->params->query);
        $forced_sensorId = self::$user->sensorId;
        if ($forced_sensorId != null) {
            $sensorId = $forced_sensorId;
        } else {
            $sensorId = isset($this->request->params['named']['sensorId']) ? $this->request->params['named']['sensorId'] : null;
        }
//        var_dump(self::$user->sensorId);
//        die();
        $parameters = array();
        if(!$sensorOptions = $this->Help->callPluginFunction('Sensor_getAsSelectOptions_Hook', $parameters, Configure::read('companyTitle'))){
            $sensorOptions = $this->Sensor->getAsSelectOptions(true, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1));
        }

        $limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
        if (!$limit) $limit = 50;
        //$list = $this->ApprovedOrder->withRefs()->find('all', array('order' => ApprovedOrder::NAME.'.created DESC'));
        $arr = array('limit' => $limit, 'order' => array(self::MODEL . '.created' => 'desc'), 'contain'=>array('Plan'), 'recursive'=>1);
        $conds = array(self::MODEL.'.plan_id <>'=>1);
        if ($sensorId && isset($sensorOptions[$sensorId])) {
            $conds[self::MODEL . '.sensor_id'] = $sensorId;
        } else {
            $conds[self::MODEL . '.sensor_id'] = Configure::read('user')->selected_sensors;
        }
        if(isset($this->request->query['order_number']) && trim($this->request->query['order_number'])){ $conds[] = 'LOWER(Plan.mo_number) LIKE \'%'.mb_strtolower($this->request->query['order_number']).'%\''; }
        if(isset($this->request->query['product_code']) && trim($this->request->query['product_code'])){ $conds[] = 'LOWER(Plan.production_code) LIKE \'%'.mb_strtolower($this->request->query['product_code']).'%\''; }
        $userliveData = $this->User->findById(Configure::read('user')->id);
        if(Configure::read('user')->sensor_id > 0){
            $conds['ApprovedOrder.confirmed'] = 0;
            $conds['ApprovedOrder.status_id'] = array(ApprovedOrder::STATE_COMPLETE, ApprovedOrder::STATE_POSTPHONED);
            $conds['ApprovedOrder.sensor_id'] = $userliveData['User']['active_sensor_id'] > 0?$userliveData['User']['active_sensor_id']:Configure::read('user')->sensor_id;
        }
        $conds['ApprovedOrder.sensor_id'] = is_array($conds['ApprovedOrder.sensor_id'])?$conds['ApprovedOrder.sensor_id']:array($conds['ApprovedOrder.sensor_id']);
        $conds['ApprovedOrder.sensor_id'] = array_intersect($conds['ApprovedOrder.sensor_id'], Configure::read('user')->selected_sensors);
        if (!empty($conds)) $arr['conditions'] = $conds;
        $parameters = array(&$arr, &$this->request, &$this->ApprovedOrder);
        $pluginData = $this->Help->callPluginFunction('ApprovedOrders_Index_BeforeSearch_Hook', $parameters, Configure::read('companyTitle'));
        //pr($arr);die();
        $this->Paginator->settings = $arr;
        try{
            $this->ApprovedOrder->withRefs(false);
            $list = $this->Paginator->paginate(self::MODEL);
        }catch(NotFoundException $ex){
            $this->request->params['named']['page'] = 1;
            $this->Paginator->paginate(self::MODEL);
            $url = array('controller' => self::ID, 'action' => 'index');
            if (isset($this->request['paging'][self::MODEL]['pageCount'])) {
                $url['page'] = max(intval($this->request['paging'][self::MODEL]['pageCount']), 1);
            }
            if ($limit) {
                $url['limit'] = $limit;
            }
            if ($sensorId) {
                $url['sensorId'] = $sensorId;
            }
            $this->redirect(Router::url($url, true));
        }
        
        $this->set(array(
            'title_for_layout' => __('Patvirtinti užsakymai'),
            'list'             => $list,
            'forced_sensorId'  => $forced_sensorId,
            'sensorId'         => $sensorId,
            'sensorOptions'    => $sensorOptions,
            'model'            => self::MODEL,
            'typeOptions'      => Sensor::getTypes(),
            'newUrl'           => Router::url('edit/0'),
            'viewUrl'          => Router::url('view/%d/'.($sensorId?'sensorId:'.$sensorId:'')),
            'editUrl'          => Router::url('edit/%d/'.($sensorId?'sensorId:'.$sensorId:'')),
            'removeUrl'        => Router::url('remove/%d'),
            'finishUrl'        => Router::url('finish/%d'),
            'filterUrl'        => Router::url(array('controller' => self::ID, 'action' => 'index', 'limit' => $limit, 'sensorId' => '__DATA__')),
            'removeMessage'    => __('Ar tikrai norite pašalinti šį įrašą?'),
            'editApprovedOrdersPerm' => $this->hasAccessToAction(array('controller'=>'ApprovedOrders', 'action'=>'edit')),
            'returnOrderPerm' => $this->hasAccessToAction(array('controller'=>'ApprovedOrders', 'action'=>'return_order')),
            'sensorsLabel'=>__('Darbo centrai')
        ));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('ApprovedOrdersIndexHook', $parameters, Configure::read('companyTitle'));
    }

    public function finish($id=0) {
        $this->requestAuth(true);
        $item = $this->ApprovedOrder->withRefs()->findById($id);
        if (empty($item)) {
            return $this->redirect($this->referer());
        }
        if ($item['ApprovedOrder']['status_id'] != 4) {
            return $this->redirect($this->referer());
        }
        $new_ao = array(
            'id'=>$id,
            'status_id' => ApprovedOrder::STATE_COMPLETE,
        );
        // $new_ao = array('ApprovedOrder' => array(
            // 'created'            => date('Y-m-d H:i:s'),
            // 'sensor_id'          => $item['ApprovedOrder']['sensor_id'],
            // 'plan_id'            => $item['ApprovedOrder']['plan_id'],
            // 'status_id'          => ApprovedOrder::STATE_COMPLETE,
            // 'end'                => date('Y-m-d H:i:s'),
            // 'quantity'           => 0,
            // 'box_quantity'       => 0,
            // 'confirmed_quantity' => 0,
        // ));
        $this->ApprovedOrder->save($new_ao);
        // $id = $this->ApprovedOrder->id;
        // unset($this->ApprovedOrder->id);
        // $this->ApprovedOrder->withRefs()->updateAll(array(
            // 'ApprovedOrder.status_id' => ApprovedOrder::STATE_PART_COMPLETE
        // ), array(
            // 'ApprovedOrder.status_id' => ApprovedOrder::STATE_POSTPHONED,
            // 'Plan.mo_number'          => $item['Plan']['mo_number']
        // ));
//
//        $item['ApprovedOrder']['status_id'] = ApprovedOrder::STATE_PART_COMPLETE;
//        $this->ApprovedOrder->save($item);

        $sysEmail = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL);
        $sysEmailName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
        $shiftManagerEmail = $item[Branch::NAME]['shift_manager_email'];
        $shiftManagerEmail = $this->Branch->parseBranchManagerEmails($shiftManagerEmail);

        if ($sysEmail && $shiftManagerEmail && Configure::read('sendEmails') === null) {

            $mail = new \CakeEmail(self::EMAIL_CONFIG);
            $mail->from('oee@pergale.lt');
            $mail->to($shiftManagerEmail);
            $mail->subject(sprintf(__('Dėžių kiekio patvirtinimas %s'), date("Y-m-d H:i")));
            $mail->emailFormat('both');
            $mail->template('confirm_box_quantity', null);
            $url = Router::url(array('controller' => ApprovedOrdersController::ID, 'action' => ($id . '/edit')), true);
            $mail->viewVars(array(
                'item'      => $this->ApprovedOrder->withRefs()->findById($id),
                'lossItems' => $this->Loss->withRefs()->find('all', array(
                    'conditions' => array(Loss::NAME . '.approved_order_id' => $id),
                    'order'      => Loss::NAME . '.loss_type_id ASC'
                )),
                'url'       => $url
            ));
            try {
                $mail->send();
            } catch (Exception $e) {
                $this->Log->write("Email send failed: " . json_encode($e->getMessage()) . ";");
            }
        } else {
            $this->Log->write("Configuration does not allow mail sending");
            // TODO: log configuration error
        }

        return $this->redirect($this->referer());
    }

    /** @requireAuth Redaguoti patvirtintus užsakymus */
    public function edit($id) {
        $this->requestAuth(true);
        //$id = $this->request->params['id'];
        $listPageUrlParams = is_array($this->Session->read('approoved_orders_last_url_params'))?$this->Session->read('approoved_orders_last_url_params'):array();
        if(isset($this->params['named']['sensorId'])){
            $listUrl = Router::url(array_merge(array('action' => 'index', 'sensorId'=>$this->params['named']['sensorId']),array('?'=>$listPageUrlParams)), true);
        }else{
            $listUrl = Router::url(array_merge(array('action' => 'index'),array('?'=>$listPageUrlParams)), true);
        }
        $refUri = $this->request->referer(true);
        $item = $this->ApprovedOrder->withRefs()->findById($id);

        $canSave = true;
        if($item['ApprovedOrder']['end'] != null && strtotime($item['ApprovedOrder']['end'])+120 >= strtotime(date("Y-m-d H:i:s"))) {
            $canSave = false;
        }
        $forced_sensorId = self::$user->sensorId;
        if($forced_sensorId != null && $item['ApprovedOrder']['sensor_id'] != $forced_sensorId) {
            throw new ForbiddenException(__('You do not have rights to access this page.'));
        }
        $lossItems = Settings::translateThreaded($this->Loss->withRefs()->find('all', array(
            'conditions' => array(Loss::NAME.'.approved_order_id' => $id),
            'order' => Loss::NAME.'.loss_type_id ASC'
        )), array('LossType.name'));
        $lossItemsConfirmed = Settings::translateThreaded($this->Loss->withRefs()->find('all', array(
            'conditions' => array(Loss::NAME . '.approved_order_id' => $id, Loss::NAME . '.while_confirming' => 1),
            'order'      => array(Loss::NAME . '.loss_type_id ASC', Loss::NAME . '.id ASC')
        )), array('LossType.name'));
        $this->PartialQuantity->bindModel(array('belongsTo'=>array('User')));
        $partialQuantities = $this->PartialQuantity->find('all', array(
            'conditions' => array('PartialQuantity.approved_order_id' => $id),
            'order'      => array('PartialQuantity.id ASC')
        ));
        $redirectAddr = '';
        if (empty($this->request->data)) {
            $this->request->data = $item;
        } else {
            if (!$item[ApprovedOrder::NAME]['confirmed'] && ($item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_COMPLETE || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_PART_COMPLETE)) {
                $this->request->data[ApprovedOrder::NAME]['confirmed_box_quantity'] = isset($this->request->data[ApprovedOrder::NAME]['confirmed_box_quantity'])?str_replace(',','.',$this->request->data[ApprovedOrder::NAME]['confirmed_box_quantity']):0;
                $this->request->data[ApprovedOrder::NAME]['confirmed_item_quantity'] = str_replace(',','.',$this->request->data[ApprovedOrder::NAME]['confirmed_item_quantity']);
                $bq = floatval($this->request->data(ApprovedOrder::NAME . '.confirmed_box_quantity'));
                $iq = floatval($this->request->data(ApprovedOrder::NAME . '.confirmed_item_quantity'));
                
                $confirmed_quantity = (($bq * $item[Plan::NAME]['box_quantity']) + $iq);
                $currentTime = date('Y-m-d H:i:s');
                $currentShift = $this->Shift->find('first', array('conditions'=>array('Shift.start <='=>$currentTime, 'Shift.end >='=>$currentTime, 'Shift.branch_id'=>$item['Branch']['id'])));
                if($item['Sensor']['type'] == 1) {
                    $confirmed_quantity = $this->request->data(ApprovedOrder::NAME.'.confirmed_box_quantity');
                }
                $this->Log->write("Confirming Approved Order ".$item['ApprovedOrder']['id'].". Box quantity: " . $bq ." ; Item quantity: " . $iq. " Total confirmed quantity: " . $confirmed_quantity);
                $arr['id'] = $item[ApprovedOrder::NAME]['id'];
                if($confirmed_quantity > 0){
                    $arr['confirmed_quantity'] = $confirmed_quantity;
                }
                if(isset($this->request->data['confirm_order'])){
                    $arr['confirmed'] = 1;
                }
                // Declare Losses
                $this->loadModel('LossType');
                $fLossTypes = $this->LossType->find('all',array('conditions'=>array('LossType.id >'=>0)));
                foreach ($fLossTypes as $idx => $li){
                    $fLossTypes[$idx][LossType::NAME]['value'] = (($v = $this->request->data('lt_' . $li[LossType::NAME]['id'])) ? floatval($v) : 0);
                }
                foreach ($fLossTypes as $idx => $li) {
                    $this->Loss->newItem($id, $li[LossType::NAME]['id'], $li[LossType::NAME]['value'], $currentShift['Shift']['id'], 1, $this->Auth->user());
                }
                $this->QualityFactor->calculateOrderQualityOnProductionEnd($item, $item);
                if(!isset($this->request->data['save_loss_only'])){
                    if($this->ApprovedOrder->save(array(ApprovedOrder::NAME => $arr))) {
                        $this->Session->setFlash(__('Užsakymas patvirtintas'), 'default', array(), 'saveMessage');
                        $item = $this->ApprovedOrder->withRefs()->findById($item[ApprovedOrder::NAME]['id']);
                        $parameters = array(&$this, &$item, &$lossItems);
                        $pluginData = $this->Help->callPluginFunction('ApprovedOrders_Edit_AfterSave_Hook', $parameters, Configure::read('companyTitle'));
                        $redirectAddr = $listUrl;
                    } else {
                        $this->Session->setFlash(__('Failed Saving item').': '.print_r($this->ApprovedOrder->validationErrors, true), 'default', array(), 'saveMessage');
                    }
                }
                $userliveData = $this->User->findById(Configure::read('user')->id);
                if($userliveData['User']['active_sensor_id'] > 0){
                    echo '<script type="text/javascript">window.close();</script>';die();
                }
            } else {
                $redirectAddr = $listUrl;
            }
        }

        $rpo = $this->ApprovedOrder->withRefs()->find('all',array(
            'conditions'=>array(
                'ApprovedOrder.plan_id'=>$item['ApprovedOrder']['plan_id'],
                'ApprovedOrder.id !='=>$item['ApprovedOrder']['id'],
                ),
            'order'=>array('ApprovedOrder.id ASC')
        ));
        
        $title = $item ? sprintf(__('Užsakymo %s (ID: %d)'), '', $item[self::MODEL]['id']) : __('Naujas užsakymas');
        $this->set(array(
            'title_for_layout' => $title,
            'h1_for_layout' => $title,
            'model' => self::MODEL,
            'item' => $item,
            'rpo' => $rpo,
            'lossItems' => $lossItems,
            'lossItemsConfirmed' => $lossItemsConfirmed,
            'partialQuantities' => $partialQuantities,
            'lossTypes' => Settings::translateThreaded($this->LossType->find('all', array('conditions'=>array('LossType.id >'=>0))), array('LossType.name')),
            'typeOptions' => Sensor::getTypes(),
            'listUrl' => ($refUri ? Router::url($refUri, true) : $listUrl),
            'formUrl'=> Router::url(array_merge(array('action'=>'edit',($id ? $id : 0)),(isset($this->params['named']['sensorId'])?array('sensorId'=>$this->params['named']['sensorId']):array())), true),
            'edit_approved_order_quantities'=>$this->hasAccessToAction(array('plugin'=>Configure::read('companyTitle'),'controller'=>'Plugins', 'action'=>'edit_approved_order_quantities')),
            'editApprovedOrderQuantityPerm'=>$this->hasAccessToAction(array('controller'=>'ApprovedOrders', 'action'=>'edit_approved_order_quantities')),
            'canSave'=>$canSave
        ));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('ApprovedOrdersEditHook', $parameters, Configure::read('companyTitle')); 
        
        if(trim($redirectAddr)){$this->redirect($redirectAddr);}
    }

    /** @requireAuth Peržiūrėti vykdomo užsakymo duomenis */
    public function view($id){
        $this->edit($id);
    }

    /** @requireAuth Pašalinti patvirtintus užsakymus */
    public function remove($id=0) {
        $this->requestAuth(true);
        $listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
        $item = $id ? $this->ApprovedOrder->findById($id) : null;
        if ($item && $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED) {
            try {
                if ($this->ApprovedOrder->delete($id, false)) {
                    $this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
                } else {
                    $this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
                }
            } catch (PDOException $ex) {
                $code = '' . $ex->getCode();
                if (substr($code, 0, 2) == '23') {
                    $this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
                } else {
                    $this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
                }
            }
        }

        $refUri = $this->request->referer(true);
        if ($refUri && $refUri != '/') {
            $this->redirect(Router::url($refUri, true));
        } else {
            $this->redirect($listUrl);
        }
    }
    
    /** @requireAuth Atkurti užbaigtus užsakymus */
    public function return_order($orderId){
        $this->requestAuth(true);
        $perm = $this->hasAccessToAction(array('controller'=>'ApprovedOrders', 'action'=>'edit'));
        if(!$perm) die();
        $listPageUrlParams = is_array($this->Session->read('approoved_orders_last_url_params'))?$this->Session->read('approoved_orders_last_url_params'):array();
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $approvedOrder = $this->ApprovedOrder->findById($orderId);
        if(!$approvedOrder){
            $this->redirect(Router::url(array_merge(array('action' => 'index'),array('?'=>$listPageUrlParams)), true));
        }
        $this->Log->write('Atkuriamas užbaigtas užsakymas '.json_encode($approvedOrder));
        $this->ApprovedOrder->updateAll(array('status_id'=>ApprovedOrder::STATE_POSTPHONED), array('ApprovedOrder.id'=>$orderId));
        //Jei nera ne vieno uzbaigto uzsakymo pagal plana, plano parametra exported_to_mpm reikia nuresetinti, kad vel butu siunciami duomenys i mpm
//        $finishApprovedOrderExist = $this->ApprovedOrder->find('first', array(
//            'conditions'=>array('ApprovedOrder.plan_id'=>$approvedOrder['ApprovedOrder']['plan_id'], 'ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE)
//        ));
        //if(!$finishApprovedOrderExist){
            $planModel = ClassRegistry::init('Plan');
            $planModel->updateAll(array('exported_to_mpm'=>0), array('Plan.id'=>$approvedOrder['ApprovedOrder']['plan_id']));
        //}
        $this->redirect(Router::url(array_merge(array('action' => 'index'),array('?'=>$listPageUrlParams)), true));
    }
    
    /** @requireAuth Ar leidžiama keisti gaminių kiekį užsakymo tvirtinimo metu? */
    public function edit_approved_order_quantities(){
        die();
    }

    public function get_mo_stats() {
        if(isset($this->request->data['MoStat']['start_end_date'])){
            list($this->request->data['MoStat']['start_date'], $this->request->data['MoStat']['end_date']) = explode(' ~ ', $this->request->data['MoStat']['start_end_date']);
        }
        if (isset($this->request->data['MoStat']['start_date']) && isset($this->request->data['MoStat']['end_date'])) {
            $mo_list = $this->Plan->getMOListByRange($this->request->data);
            $this->set(compact('mo_list'));
            $parameters = array(&$this);
            $pluginData = $this->Help->callPluginFunction('DashboardsGetMoStatsHook', $parameters, Configure::read('companyTitle'));
            return;
        }
        if (!isset($this->request->data['MoStat']['mo'])) { // Nėra mo numerio. Rodom tuščią puslapį.
            return;
        }
        $mo_number = $this->request->data['MoStat']['mo']; // Mo numeris
        if (is_array($mo_number)) {
            $mo_number = Set::flatten($mo_number);
            $mo_number = array_filter($mo_number);
            $this->request->data['MoStat']['mo'] = $mo_number; // Set the field to a proper one if we select the values from array
        }
        $parameters = array(&$mo_number);
        $pluginData = $this->Help->callPluginFunction('Dashboards_ReplaceMoNumber_Hook', $parameters, Configure::read('companyTitle'));
        if (is_array($mo_number)) {
            $this->request->data['MoStat']['mo'] = implode(",", $mo_number); // Set the field to a proper one if we select the values from array
        } else {
            $this->request->data['MoStat']['mo'] = $mo_number;
        }
        $this->OrderCalculation->bindModel(array('belongsTo'=>array('ApprovedOrder','User')));
        $orders = $this->OrderCalculation->find('all', array(
            'fields'=>array(
                'OrderCalculation.id',
                'OrderCalculation.step',
                'OrderCalculation.mo_number',
                'OrderCalculation.sensor_id',
                'ApprovedOrder.additional_data',
                'ApprovedOrder.id',
                'GROUP_CONCAT(OrderCalculation.problems SEPARATOR \'\n\') AS problems',
                'MIN(OrderCalculation.order_start) AS order_start',
                'MAX(OrderCalculation.order_end) AS order_end',
                'SUM(OrderCalculation.quantity) AS quantity',
                'SUM(OrderCalculation.confirmed_quantity) AS confirmed_quantity',
                'SUM(OrderCalculation.time_full) AS shift_length',
                'SUM(OrderCalculation.time_full_with_exclusions) AS shift_length_with_exclusions',
                'SUM(OrderCalculation.time_green) AS fact_prod_time',
                'SUM(OrderCalculation.time_yellow) AS transition_time',
                'SUM(OrderCalculation.exceeded_time_yellow) AS exceeded_transition_time',
                'SUM(OrderCalculation.time_red) AS problem_time',
                'CONCAT(User.first_name,\' \',User.last_name) AS operator_name'
            ),'conditions' => array(
                'OrderCalculation.mo_number' => $mo_number,
                'OrderCalculation.sensor_id' => Configure::read('user')->selected_sensors,
            ),'group'=>array('OrderCalculation.mo_number'),
            // 'order'=>array('ApprovedOrder.created')
        ));
        $transitionProblems = $this->Problem->find('list', array(
            'fields' => array('Problem.id', 'Problem.name'),
            'conditions' => array('OR'=>array('Problem.transition_problem'=>1, 'Problem.id'=>Problem::ID_NEUTRAL))
        ));
        $transitionProblems = Settings::translateThreaded($transitionProblems, array(''));
        $allProblems = $this->Problem->find('list', array(
            'fields' => array('Problem.id', 'Problem.name'),
        ));
        $allProblems = Settings::translateThreaded($allProblems, array(''));
        $this->ShiftTimeProblemExclusion->bindModel(array('belongsTo'=>array('Problem')));
        $exclusionProblems = $this->ShiftTimeProblemExclusion->find('list', array(
            'fields' => array('Problem.id', 'Problem.name'),
            'recursive' => 1
        ));
        $exclusionProblems = Settings::translateThreaded($exclusionProblems, array(''));
        $ordersIdsList = Set::extract('/ApprovedOrder/id', $orders);
        $partialQuantities = Hash::combine($this->PartialQuantity->find('all', array(
            'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'PartialQuantity.approved_order_id'),
            'conditions'=>array('PartialQuantity.approved_order_id'=>$ordersIdsList),
            'group'=>array('PartialQuantity.approved_order_id')
        )),'{n}.PartialQuantity.approved_order_id', '{n}.0.quantity');
        $this->Verification->bindModel(array('belongsTo'=>array('Plan','ApprovedOrder')));
        $verificationsTmp = $this->Verification->find('all', array(
            'fields'=>array('Verification.*','Plan.production_code','ApprovedOrder.created','ApprovedOrder.end','Plan.mo_number'),
            'conditions'=>array('Plan.mo_number'=>$mo_number),
            'order'=>array('Verification.created')
        ));
        foreach($verificationsTmp as $ver){
            $verifications[$ver['Plan']['mo_number']][] = $ver;
        }
        foreach($orders as &$order){
            $moNumber = $order['OrderCalculation']['mo_number'];
            $order[0]['count_delay'] = $order['OrderCalculation']['step'] > 0?$order['0']['quantity'] / ($order['OrderCalculation']['step'] / 3600):0;
            $order['verifications'] = $verifications[$moNumber]??array();
            $oeeParams = $this->Record->calculateOee($order[0]);
            $order = $order + $oeeParams;
            $downtimesByType = array(
                'work'=>array(-1=>array('name'=>__('Gamybos laikas'), 'color'=>'mo-green', 'length'=>$order[0]['fact_prod_time'])),
                'transitions'=>array(),
                'problems'=>array(),
                'exclusions'=>array()
            );
            foreach(explode("\n",$order['0']['problems']) as $downtimes){
                $downtimes = json_decode($downtimes,true);
                if(empty($downtimes)){ continue; }
                foreach($downtimes as $problemId => $downtimeData){
                    if(in_array($problemId, array(-1))){ continue; }
                    if(isset($transitionProblems[$problemId])){
                        $downtimeType = 'transitions';
                        $problemData = array('name'=>$transitionProblems[$problemId], 'color'=>'mo-yellow');
                    }elseif(isset($exclusionProblems[$problemId])){
                        $downtimeType = 'exclusions';
                        $problemData = array('name'=>$exclusionProblems[$problemId], 'color'=>'mo-darkgrey');
                    }elseif(isset($allProblems[$problemId])){
                        $problemData = array('name'=>$allProblems[$problemId], 'color'=>'mo-red');
                        $downtimeType = 'problems';
                    }else{ continue; }
                    if(!isset($downtimesByType[$downtimeType][$problemId])){
                        $downtimesByType[$downtimeType][$problemId] = array_merge($downtimeData, $problemData);
                    }else{
                        $downtimesByType[$downtimeType][$problemId]['length'] += $downtimeData['length'];
                    }
                }
            }
            foreach($downtimesByType as $type => &$downtimes){
                usort($downtimes, function($a, $b){
                    return $a['length'] < $b['length'];
                });
                unset($downtimes);
            }
            $order['downtimes'] = $downtimesByType;
        }
        $exclusionProblems = Settings::translateThreaded($exclusionProblems, array(''));
        $verifications = $records = array();
        $this->set(compact('mo_number', 'orders', 'partialQuantities'));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('DashboardsGetMoStatsHook', $parameters, Configure::read('companyTitle'));
    }

}
