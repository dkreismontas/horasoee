<?php

App::uses('User', 'Model');
App::uses('Sensor', 'Model');

/**
 * @property User $User
 */
class DashboardController extends AppController {
	
	const ID = 'dashboard';
	
	public $uses = array('Sensor','Update',);
	
	/** @requireAuth Matyti info skydą */
	public function index() {
		//$this->requestAuth(true);
        $this->Sensor->bindModel(array('belongsTo'=>array('Branch','Factory'=>array('foreignKey'=>false,'conditions'=>array('Factory.id = Branch.factory_id')))));
        $factoriesList = $this->Sensor->find('list', array(
            'recursive'=>1,
            'fields'=>array('Factory.id', 'Factory.name'),
            'conditions'=>array('Sensor.id'=>Configure::read('user')->selected_sensors)
        ));
		$workCenters = array();
		$conditions = array('Sensor.marked'=>1, 'Sensor.line_id IS NOT NULL');
        if(isset($this->request->params['named']['factory_id'])){
            $conditions['Factory.id'] = $this->request->params['named']['factory_id'];
        }
        if(isset(self::$user->selected_sensors) && !empty(self::$user->selected_sensors)){
            $conditions['Sensor.id'] = self::$user->selected_sensors;
        }
        $rWorkCenters = $this->Sensor->withRefs()->find('all', array(
            'conditions'=>$conditions,
            'order'=>array('Sensor.position','Line.name', 'Sensor.name')
        ));
		foreach ($rWorkCenters as $workCenter) {
			$workCenters[$workCenter[Line::NAME]['id'].'_'.$workCenter[Sensor::NAME]['id']] = $workCenter;
		}
        if(AppController::$user->id != 1 && file_exists(Configure::read('updatesInfoPath'))){
            $fh = fopen(Configure::read('updatesInfoPath'), 'r');
            $updatesList = array();
            if($fh){
                while(($line = fgets($fh)) !== false) {
                    $lineData = json_decode($line,true);
                    if(isset($lineData['created'])){$updatesList[] = $lineData['created'];}
                }
            }
            fclose($fh);
            $readedUpdates = $this->Update->find('list', array('fields'=>array('Update.id','Update.update_created'),'conditions'=>array('Update.user_id'=>AppController::$user->id)));
        }
        $this->set(array(
            'title_for_layout' => __('Info skydas'),
            'workCenters' => $workCenters,
            'viewUrl' => Router::url(array('controller' => 'work-center', 'action' => '%s'), false),
            'view24HUrl' => Router::url(array('controller' => 'work_center_24h', 'action' => 'index/%s'), false),
            'unreadedUpdatesExist' => isset($updatesList)?sizeof(array_diff($updatesList, $readedUpdates)):0,
            'factoriesList' => $factoriesList
        ));
	}
	
}
