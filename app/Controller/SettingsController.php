<?php

App::uses('Settings', 'Model');

App::uses('CakeEmail', 'Network/Email');

/**
 * @property Settings $Settings
 */
class SettingsController extends AppController {
	
	const ID = 'settings';
	const MODEL = Settings::NAME;
	
	public $uses = array(self::MODEL, 'Update');

	/** @requireAuth Peržiūrėti/redaguoti nustatymus... */
	public function index() {
		$this->requestAuth(true);
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		$list = $this->Settings->find('all', array('conditions'=>array('Settings.hidden'=>0)));
		if (!empty($this->request->data)) {
			$ok = true;
			foreach ($this->request->data as $key => $item) {
				$this->Settings->create();
				if (!$this->Settings->save(array(Settings::NAME => array('id' => $item['id'], 'key' => $key, 'value' => $item['value'])))) {
					$ok = false;
					break;
				}
			}
			if ($ok) {
				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			}
		}
		
		$this->set(array(
			'title_for_layout' => __('Nustatymai'),
			'list' => $list,
			'model' => self::MODEL
		));
	}
	
	public function controll_plugins(){
	    $user = $this->Auth->user();
        if($user['id'] != 1) die('Šį puslapį pasiekti gali tik id: 1 turintis vartotojas');
		$settingsFile = Configure::read('pluginsSettings');
		if($this->request->is('ajax')){
			$fileContent = null;
			if(file_exists($settingsFile)){
				$myfile = fopen($settingsFile, "r") or die("Unable to open file!");
				$fileContent = json_decode(fgets($myfile));
				fclose($myfile);	
			}
			$pluginName = $this->request->data['name'];
			if(isset($fileContent->$pluginName)) unset($fileContent->$pluginName);
				else $fileContent->$pluginName = 'enabled';
			$myfile = fopen($settingsFile, "w") or die("Unable to open file!");
			fwrite($myfile, json_encode($fileContent));
			fclose($myfile);	
			die();
		}
		$pluginsList = array();
		$plugins = array();
		$pluginsDir = ROOT.'/app/Plugin/';
		$creationDate = '';
		$fileContent = null;
		if(file_exists($settingsFile)){
			$myfile = fopen($settingsFile, "r") or die("Unable to open file!");
			$fileContent = json_decode(fgets($myfile));
			fclose($myfile);	
		}
		foreach(CakePlugin::loaded() as $pluginName){
			if(file_exists($pluginsDir.$pluginName)){
				if(file_exists($pluginsDir.$pluginName.'/description.php')) include($pluginsDir.$pluginName.'/description.php');
				if(isset($pluginDescription)){
					if(is_array($pluginDescription)){
						$pluginDescription = '<ul style="list-style-type: none;"><li>- '.implode('</li><li>- ',$pluginDescription).'</li></ul>';	
					}
				}else{
					$pluginDescription = '';
				}
				$plugins[] = array(
					'name'=>$pluginName, 
					'description'=>$pluginDescription, 
					'enabled'=>isset($fileContent->$pluginName)?0:1, 
					'created'=>isset($created)?$created:'',
					'company'=>isset($company)?$company:'',
				);
				unset($company);
				unset($created);
				unset($pluginDescription);
			}
		}
		$this->set(compact('plugins'));
	}

    public function check_updates(){
        $filePath = Configure::read('updatesInfoPath');
        $fh = fopen($filePath, 'c+') or die("negalima irasyti failo");
        $updatesList = array();
        $requestData = $this->request->data;
        if($fh){
            while(($line = fgets($fh)) !== false) {
                $lineData = json_decode($line,true);
                if(isset($this->params['named']['edit']) && $lineData['created'] == $this->params['named']['edit']){
                    if(!empty($requestData)){
                        $lineData = array_merge($lineData,$requestData['Settings']);
                    }
                    $this->request->data['Settings'] = $lineData;
                }
                if(!isset($this->params['named']['delete']) || $lineData['created'] != $this->params['named']['delete']){
                    $updatesList[] = $lineData;
                }
            }
        }
        if(!empty($requestData) && !isset($this->params['named']['edit'])){
            $newUpdate = array(
                'update_info'=>$this->request->data['Settings']['update_info'],
                'instruction'=>$this->request->data['Settings']['instruction'],
                'created'=>time(), 
            );
            fwrite($fh, json_encode($newUpdate)."\n");
            $updatesList[] = $newUpdate;
        }
        fclose($fh);
        if(!empty($this->params['named']) && array_intersect_key($this->params['named'], array_flip(array('delete','edit')))){
            $stringToTxt = array_map(function($update){ return json_encode($update)."\r\n"; },array_filter($updatesList));
            $fh = fopen($filePath, 'w');
            fwrite($fh, implode('',$stringToTxt));
            fclose($fh);
        }
        $updatesList = array_reverse($updatesList);
        $user = $this->Auth->user();
        if(isset($this->params['named']['confirmed'])){
            $this->Update->save(array('update_created'=>$this->params['named']['confirmed'], 'user_id'=>$user['id']));
            $this->redirect(array('controller'=>'settings', 'action'=>'check_updates'));
        }
        $confirmedUpdates = $this->Update->find('list', array('fields'=>array('Update.update_created'),'conditions'=>array('Update.user_id'=>$user['id'])));
        $this->set(compact('updatesList','confirmedUpdates'));
    }

    public function check_save_data_status(){
	    die();
        if($this->Auth->user()['id'] != 1){ die('Pasiekti gali tik ID: 1 vartotojas'); }
        $projects = glob(ROOT.'/../*');
        foreach($projects as $project){
            if(substr(current(array_reverse(explode('/',$project))),0,1) == '_'){ continue; }
            $database_config_path = $project.'/app/Config/database.php';
            if(!file_exists($database_config_path)) { continue; }
            $file = fopen($database_config_path,'r');
            $content = fread($file,filesize($database_config_path));
            fclose($file);
            preg_match("/\'plugin\'\s*=>\s*\'(.+?)\'/", $content,$plugin);
            if(sizeof($plugin) == 2) {
                $plugin = $plugin[1];
            }
            if(is_array($plugin) || !trim($plugin)){ continue; }
            $pushStartMarkerPath = $project.'/app/Plugin/'.$plugin.'/webroot/files/data_push_start.txt';
            if(isset($this->params['named']['remove_marker']) && $this->params['named']['remove_marker'] == $plugin){
                unlink($pushStartMarkerPath);
                echo 'Failas markeris pasalintas: '.$pushStartMarkerPath;
            }
            if(file_exists($pushStartMarkerPath) && !isset($this->params['named']['remove_marker'])){
                if(time() - filemtime($pushStartMarkerPath) > 3600){
                    $headers = "Content-Type: text/html; charset=UTF-8; From: info@horasoee.eu";
                    $to = "d.kreismontas@horasmpm.eu,support@horasmpm.eu";
                    $subject = "Projekte $plugin sukurtas įrašų siuntimo failas markeris ilgiau kaip 1 val";
                    $txt = $plugin.': įrašai pradėti siųsti ir vis dar siunčiasi nuo: '.date('Y-m-d H:i:s', filemtime($pushStartMarkerPath)).'. Patikrinkite ar duomenys tikrai siunčiasi. Jei ne - pašalinkite failą markerį: app/Plugins/'.($plugin.'/webroot/files/data_push_start.txt');
                    $txt .= "\r\n".'Pašalinti markerį galite per šią nuorodą: <a href="'.Router::url('/settings/check_save_data_status/remove_marker:'.$plugin,true).'">Pašalinti markerį</a> DĖMESIO! Jei sukurtas failas markeris o duomenys šiuo metu eina į duomenų bazę, po markerio pašalinimo sekantis cronas pradės naują duomenų siuntimą, todėl vienu metu pradės siųstis įrašai iš skirtingų laikų, kas įtakos found_problems lentelės neatitikimus. Šalinkite markerį tik įsitikinę, kad duomenys į DB neina.';
                    mail($to,$subject,$txt,$headers);
                }
            }
            $failedQueriesPaths = array(
                $project.'/app/Plugin/'.$plugin.'/webroot/files/failed_records_import_queries.txt',
                $project.'/app/webroot/files/failed_records_import_queries.txt',
            );
            foreach($failedQueriesPaths as $failedQueriesPath){
                if(!file_exists($failedQueriesPath)){ continue; }
                $dataInFile = file($failedQueriesPath);
                if(sizeof(array_filter($dataInFile)) > 2){
                    echo $project.' turi nesurasytu irasu faile: '.$failedQueriesPath;
                    pr(array_slice($dataInFile,0,20));
                }
            }
        }
        die();
    }

    public function set_filter_text(){
        $this->Session->write($this->request->data['filter'],$this->request->data['value']);
        die();
    }

    public function view_remote_file($domain = ''){
        if(!trim($domain)){ die(); }
        try{
            $domain = base64_decode($domain);
            if(preg_match('/^ftp:\/\/([^:]+):([^@]+)@([^:\/]+):*([^\/]*)(\/.+)/i', $domain, $match)){
                if(sizeof($match) == 6){
                    array_shift($match);
                    list($ftpLogin, $ftpPass, $ftpServer, $ftpPort, $remoteFilePath) = $match;
                    $ftpPort = !$ftpPort?21:$ftpPort;
                }
            }
            $ftpConn = ftp_connect($ftpServer,$ftpPort,3) or die("Could not connect to $ftpServer");
            $login = ftp_login($ftpConn, $ftpLogin, $ftpPass);
            ftp_set_option($ftpConn,FTP_TIMEOUT_SEC,60);
            ftp_pasv($ftpConn, true);
            $localFile = tmpfile();
            $pathToTmpLocation = ROOT.DS.APP_DIR.DS.'webroot/files/tmp/';
            if (!file_exists($pathToTmpLocation)) {
                mkdir($pathToTmpLocation, 0777, true);
            }
            $localFileName = (array_reverse(explode('/',$remoteFilePath))[0]);
            $localFile = $pathToTmpLocation.$localFileName;
            if (($dirOpen = opendir($pathToTmpLocation)) !== false) {
                while (($bcFile = readdir($dirOpen)) !== false) {
                    if (!is_file($pathToTmpLocation.'/'.$bcFile)){continue;}
                    unlink($pathToTmpLocation.'/'.$bcFile);
                }
                closedir($dirOpen);
            }
            if(ftp_size($ftpConn, $remoteFilePath) < 0){
                die(__('Failas %s nerastas', $remoteFilePath));
            }
            if(ftp_get($ftpConn, $localFile, $remoteFilePath, FTP_BINARY)){
                ftp_close($ftpConn);
                $this->redirect(Router::url('/files/tmp/'.$localFileName));
            }
        }catch(Exception $e){ pr($e); }
        die();
    }

    public function sort_elements(){
	    $model = ClassRegistry::init($this->request->data['model']);
        $caseString = "CASE {$model->name}.id";
        $index = 1;
        foreach($this->request->data['order'] as $id){
            $caseString .= " WHEN ".$id." THEN ".$index++;
        }
        $caseString .= " END";
        $model->updateAll(array($model->name.'.position'=>$caseString),array($model->name.'.id'=>$this->request->data['order']));
        die();
    }
    
}
