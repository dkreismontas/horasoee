<?php

App::uses('Problem', 'Model');
App::uses('Sensor', 'Model');
App::uses('Line', 'Model');

/**
 * @property Problem $Problem
 * @property Sensor $Sensor
 */
class ProblemsController extends AppController {
	
	const ID = 'problems';
	const MODEL = Problem::NAME;
	
	public $uses = array(self::MODEL, Sensor::NAME, Line::NAME, 'ShiftTimeProblemExclusions');
	
	/** @requireAuth Peržiūrėti prastovų tipus */
	public function index() {
		$this->requestAuth(true);
		$list = $this->Problem->find('all', array('conditions'=>array('Problem.id NOT IN(0)', 'Problem.line_id'=>$this->Line->getAsSelectOptions(true,true))));
		$this->Problem->addLevelNr($list);
        $loadedPlugins = CakePlugin::loaded();
        $pluginFunctionality = array(
            'problemTypeLinks' => array()
        );
        foreach($loadedPlugins as $plugin) {
            $plugin_model = $plugin . 'AppModel';
            $this->loadModel($plugin . '.' . $plugin_model);
            if (method_exists($plugin_model, 'getProblemTypeLinks')) {
                $pluginFunctionality['problemTypeLinks'][] = $plugin_model::getProblemTypeLinks();
            }
        }
		$this->set(array(
			'title_for_layout' => __('Prastovų tipai'),
			'list' => $list,
			'model' => self::MODEL,
			'typeOptions' => Sensor::getTypes(),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
            'parentOptions' => $this->Problem->getAsSelectOptions(true,false),
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'disableUrl' => Router::url('%d/disable'),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?'),
            'pluginFunctionality'=>$pluginFunctionality
		));
	}
	
	/** @requireAuth Redaguoti prastovų tipus */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);

        $item = null;
        if($id != 0) {
            $item = $this->Problem->findById($id);
        }
		if (empty($this->request->data)) {
			$this->request->data = $item;
		} else {
			if ($this->request->data[Problem::NAME]['id'] == Problem::ID_NEUTRAL || $this->request->data[Problem::NAME]['id'] == Problem::ID_NO_WORK) {
				$this->request->data[Problem::NAME]['sensor_type'] = Sensor::TYPE_NONE;
				$this->request->data[Problem::NAME]['for_speed'] = 0;
			}
            $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
            if($this->request->data['Problem']['id'] > 0){
                $this->request->data['Problem']['level'] = sizeof($problemsTreeList[$this->request->data['Problem']['id']])-1;
            }elseif($this->request->data['Problem']['parent_id'] > 0){
                $this->request->data['Problem']['level'] = sizeof($problemsTreeList[$this->request->data['Problem']['parent_id']]);
            }
            if(is_array($this->request->data['Problem']['line_id'])){
                foreach($this->request->data['Problem']['line_id'] as $lineId){
                    $this->Problem->create();
                    $this->request->data[Problem::NAME]['line_id'] = $lineId;
                    $this->Problem->save($this->request->data[Problem::NAME]);
                }
                $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                $this->redirect($listUrl);
            }else{
    			if ($this->Problem->save(array(Problem::NAME => $this->request->data[Problem::NAME]))) {
    				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
    				$this->redirect($listUrl);
    			} else {
    				$this->Session->setFlash(__('Nepavyko išsaugoti įrašo').': '.print_r($this->Problem->validationErrors, true), 'default', array(), 'saveMessage');
    			}
            }
		}
		$title = $item ? sprintf(__('Prastovos tipas %s (ID: %d)'), Settings::translate($item[self::MODEL]['name']), $item[self::MODEL]['id']) : __('Naujas prastovos tipas');
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'model' => self::MODEL,
			'item' => $item,
			'typeOptions' => Sensor::getTypes(),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
            'parentOptions' => $this->Problem->getAsSelectOptions(true,false,false,array($id,Problem::ID_NEUTRAL,Problem::ID_NO_WORK,Problem::ID_NOT_DEFINED,Problem::ID_WORK)),
			'listUrl' => $listUrl,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true)
		));
	}
	
	/** @requireAuth Pašalinti prastovų tipus */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		if ($id != Problem::ID_NEUTRAL && $id != Problem::ID_NO_WORK) {
			try {
				if ($this->Problem->delete($id, false)) {
				    $this->ShiftTimeProblemExclusions->deleteAll(array('problem_id'=>$id));
					$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
				} else {
					$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
				}
			} catch (PDOException $ex) {
				$code = ''.$ex->getCode();
				if (substr($code, 0, 2) == '23') {
					$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
				} else {
					$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
				}
			}
		}
		$this->redirect($listUrl);
	}

	/** @requireAuth Įjungti/išjungti prastovos tipą */ 
	public function disable() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$problem = $this->Problem->findById($id);
		$this->Problem->updateAll(array('disabled'=>'ABS(disabled - 1)'), array('id'=>$id));
		$note = $problem[Problem::NAME]['disabled'] == 1?__('Prastovos tipas įjungtas'):__('Prastovos tipas išjungtas');
		$this->Session->setFlash($note, 'default', array(), 'saveMessage');
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		$this->redirect($listUrl);
	}
	
	public function getProblemInfoAjax(){
		if(!$this->request->is('ajax') || !isset($this->request->data['problem_id'])){die();}
		$problem = $this->Problem->findById($this->request->data['problem_id']);
		if(!empty($problem)){
			echo json_encode(current($problem));
		}
		die();
	}
    
}
