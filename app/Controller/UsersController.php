<?php
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');
App::uses('User', 'Model');
App::uses('AppController', 'Controller');

class UsersController extends AppController {
	public $name = 'Users';
	public $uses = array('User','Group','OrdersAdditionalParameter','Branch','Sensor','Line','Factory');
	public $paginate = array();
	public $components = array(
		'Auth' => array(
			'loginAction' => array('controller'=>'users','action'=>'login'),
			//'loginRedirect' => array('controller'=>'orders','action'=>'sorting'),
			'authError' => 'Did you really think you are allowed to see thatssss?',
	        'authenticate' => array(
	        	'Form' => array(
		 			'userModel'=>'User',
		 			'fields' => array('username'=>'username','password'=>'password'),
		 			'scope' => array('User.active' => 1)
		 		)
			)
		)
	);
	
	public function beforeFilter(){
		if(isset($this->request->params['sensor_id'])){
			$this->Auth->loginAction[] = $this->request->params['sensor_id']; 
		}
		if(isset($this->request->params['pass'][0]) && (int)$this->request->params['pass'][0] > 0){
			//pr($this->request->params['pass']);die();
			$this->Auth->loginAction[] = $this->request->params['pass'][0]; 
		}
		parent::beforeFilter();
	}
	
	 function login($sensorId = 0){
		if(isset($_SESSION['group_id'])) unset($_SESSION['group_id']);
	 	if($this->request->is('ajax')){
            $this->layout = 'ajax';
        }else $this->layout = 'login';
    	if ($this->request->is('post')) {
            $parameters = array(&$this, $sensorId);
            $pluginData = $this->Help->callPluginFunction('Users_beforeLogin_Hook', $parameters, Configure::read('companyTitle'));
            if(is_array($pluginData) && isset($pluginData['errors'])){
                $this->Session->setFlash($pluginData['errors'], 'default', array());
            }elseif($this->Auth->login()) {
	            //$this->Help->createEvent(__('Vartotojas prisijungė'),'User','edit',array(),Router::url( $this->here, true ),$this->Auth->user());
                $user = $this->Auth->user();
                // if($user['taken_over_user_id']>0){
                    // $takenUser = $this->User->findById($user['taken_over_user_id']);
                    // if($takenUser){
                        // $user['default_start_path'] = $takenUser['User']['default_start_path'];
                    // }
                // }
                if(isset($user['taken_over_user_id']) && $user['taken_over_user_id'] > 0){
                    $this->User->bindModel(array('belongsTo'=>array('Group')));
                    $takenOverUser = $this->User->findById($this->Auth->user()['taken_over_user_id']);
                    $takenOverUser['User'] += array('Group'=>$takenOverUser['Group']);
                    //$this->Cookie->write('taken_over_user',base64_encode(serialize($takenOverUser)),true,0,'/');
                    $this->Session->write('Auth',$takenOverUser);
                    $user = $this->Auth->user();
                }
				if(isset($this->request->data['User']['sensor_id']) && $this->request->data['User']['sensor_id'] > 0){
					$choosedSensorId = $this->request->data['User']['sensor_id'];
					$passibileSensorsIds = explode(',',$user['sensor_id']);
		            if($choosedSensorId > 0 && in_array($choosedSensorId, $passibileSensorsIds)){
		                $fSensor = $this->Sensor->findById($choosedSensorId);
		                $sensorIsInUse = time() - strtotime($fSensor['Sensor']['user_update_time']) < 30?true:false;
		                //$activeUserInSensor = $this->User->find('first', array('conditions'=>array('User.active_sensor_id'=>$choosedSensorId)));
		                if(!$sensorIsInUse || !$fSensor['Sensor']['active_user_id']){
		                    $this->Log->write('Vartotojas prisijunge prie darbo centro per logino nuoroda. Vartotojo id: '.$user['id'],$fSensor);
		                    $this->User->updateAll(array('active_sensor_id'=>0), array('User.active_sensor_id'=>$choosedSensorId));
		                    $this->Sensor->updateAll(array('user_update_time'=>'\''.date('Y-m-d H:i:s').'\'', 'active_user_id'=>$user['id']), array('Sensor.id'=>$choosedSensorId));
		                    $this->redirect(Router::url(array('controller' => 'work-center', 'action' => $fSensor['Sensor']['line_id'].'_'.$fSensor['Sensor']['id']), true));
		                }
		            }else{
		            	$this->Session->setFlash(__('Jūs neturite teisės dirbti prie šio darbo centro'));
						$this->redirect($this->Auth->logout());
		            }
				}elseif(sizeof(explode(',',$user['sensor_id'])) > 1){
                    $this->redirect(Router::url(array('controller' => 'users', 'action' => 'choose_sensor'), true));
                }elseif($user['sensor_id'] > 0){
                    $this->User->updateAll(array('active_sensor_id'=>0), array('User.active_sensor_id'=>$user['sensor_id']));
                    $this->User->updateAll(array('active_sensor_id'=>$user['sensor_id']), array('User.id'=>$user['id']));
                    $this->redirect(Router::url(array('controller' => 'work-center', 'action' => 'index'), true));
                }elseif(isset($user['default_start_path']) && $user['default_start_path']){
                    $this->redirect($user['default_start_path']);
                }
				//$this->User->checkAllUsersLogoutStatus($user, $this->Cookie);
                // if(!empty($user['default_start_path'])) {
                    // return $this->redirect($user['default_start_path']);
                // }
	            //return $this->redirect($this->Auth->redirect());
	            return $this->redirect(Router::url('/',true));
	        } else {
	            $this->Session->setFlash(__('Neteisingas vartotojo vardas arba slaptažodis'));
	        }
	    }
		if($this->Session->read('login_sensor_id') > 0){
			
		}
	 }
     
     /** @requireAuth Pasirinkti darbo centrą iš galimų centrų sąrašo */
     public function choose_sensor($choosedSensorId = 0){
        $this->layout = 'bare';
        $this->set('bodyClass', array('loginpage'));
        $fUser = $this->User->withRefs()->findById(self::$user->id);
        $parameters = array(&$this, &$fUser);
        $pluginData = $this->Help->callPluginFunction('Users_beforeChooseSensor_Hook', $parameters, Configure::read('companyTitle'));
        if($fUser){
            if($fUser['User']['active_sensor_id'] > 0){
                $activeSensor = $this->Sensor->findById($fUser['User']['active_sensor_id']);
                if(time() - strtotime($activeSensor['Sensor']['user_update_time']) < 30){
                    $this->Session->setFlash(__('Šis vartotojas jau yra prisijungęs prie %s darbo centro. Pasirinkite kitą operatorių.', $activeSensor['Sensor']['name']));
                    $this->redirect(Router::url(array('controller' => 'users', 'action' => 'login'), true));
                }
            }
            $passibileSensorsIds = explode(',',$fUser['User']['sensor_id']);
            if($choosedSensorId > 0 && in_array($choosedSensorId, $passibileSensorsIds)){
                $fSensor = $this->Sensor->findById($choosedSensorId);
                $sensorIsInUse = time() - strtotime($fSensor['Sensor']['user_update_time']) < 30?true:false;
                $activeUserInSensor = $this->User->find('first', array('conditions'=>array('User.active_sensor_id'=>$choosedSensorId)));
                if(!$sensorIsInUse || empty($activeUserInSensor)){
                    $this->Log->write('Vartotojas prisijunge prie darbo centro is keletos galimu. Vartotojo id: '.$fUser['User']['id'],$fSensor);
                    $this->User->updateAll(array('active_sensor_id'=>0), array('User.active_sensor_id'=>$choosedSensorId));
                    $this->User->updateAll(array('active_sensor_id'=>$choosedSensorId), array('User.id'=>$fUser['User']['id']));
                    $this->Sensor->updateAll(array('user_update_time'=>'\''.date('Y-m-d H:i:s').'\''), array('Sensor.id'=>$choosedSensorId));
                    $parameters = array(&$choosedSensorId);
                    $pluginData = $this->Help->callPluginFunction('Users_ChooseSensorBeforeRedirect_Hook', $parameters, Configure::read('companyTitle'));
                    //$this->redirect(Router::url(array('controller' => 'work-center', 'action' => 'index'), true));
                    $this->redirect(Router::url(array('controller' => 'work-center', 'action' => $fSensor['Sensor']['line_id'].'_'.$fSensor['Sensor']['id']), true));
                }
            }
            if(sizeof($passibileSensorsIds) <= 1) $this->redirect(Router::url(array('controller' => 'login', 'action' => 'index'), true));
            $sensors = $this->Sensor->find('all', array('conditions'=>array('Sensor.id'=>$passibileSensorsIds)));
            $this->set(compact('sensors'));
        }
     }

     public function reviveSession(){
         $this->layout = 'ajax';
         $this->render(false);
     }

	 function change_capcha(){
	 	$this->layout = 'ajax';
	 }

	 function check_name(){
	 	if(!isset($_POST['validateValue'])) return false;
	 	$this->layout = 'ajax';
	 	$this->render(false);
	 	/* RECEIVE VALUE */
		$validateValue=$_POST['validateValue'];
		$validateId=$_POST['validateId'];
		$validateError=$_POST['validateError'];

			/* RETURN VALUE */
			$arrayToJs = array();
			$arrayToJs[0] = $validateId;
			$arrayToJs[1] = $validateError;

		$res = $this->User->find('first',array('conditions'=>array('`User`.`username` = \''.$validateValue.'\' AND `User`.`username` <> \''.$this->Auth->user('username').'\'')));
		if(sizeof($res)==1){		// validate??
			$arrayToJs[2] = "true";			// RETURN TRUE
			echo '{"jsonValidateReturn":'.json_encode($arrayToJs).'}';			// RETURN ARRAY WITH success
		}else{
			$arrayToJs[2] = "false";
			echo '{"jsonValidateReturn":'.json_encode($arrayToJs).'}';		// RETURN ARRAY WITH ERROR
		}
	 }

	function edit_data($id=null){
		if(isset($this->request->data) && isset($id)){
			$this->User->id = $id;
			if($this->User->save($this->request->data['User'])){
				$message = '$.blockUI({ message: \''.__('Jūsų duomenys sėkmingai pakeisti.',true).'<br /><font style="cursor: pointer; color: #8BC63E; text-align: center; " onclick="$.unblockUI({});" face="Arial" style="color:#149b0d;">Gerai</font>\',
						css: { width: \'540px\', opacity: \'1\' }
					}); ';
				$this->set('message',$message);
			}
		}
		$this->request->data = $this->User->findById($this->Auth->user('id'));
	}
    
    /** @requireAuth Pašalinti vartotoją */
	function delete($id=null){
		if(isset($id)){
			$user = $this->Auth->user();
			$this->User->id = $id;
            $this->Help->createEvent(__('Pašalino iš sistemos vartotoją'),'User','delete',$this->User->id,Router::url( $this->here, true ),$this->Auth->user());

            $superadmin_count = $this->User->find('count',array(
                'conditions' => array(
                    'User.group_id' => 1
                )
            ));
            $user_to_delete = $this->User->find('first',array(
               'conditions' => array(
                   'User.id' => $id
               )
            ));
            if($superadmin_count == 1 && $user_to_delete['User']['group_id'] == 1) {
                $this->redirect('index');
            }
			$this->User->delete();
		}
		$this->redirect('index');
	}
    
    /** @requireAuth Pakeisti vartotojo grupę */
	function change_user_group($id=null,$type=null,$model=null,$field=null){
		if($this->request->is('ajax')){
			$this->layout = '';
			$this->render(false);
			$this->User->id = $id;
			$this->User->save(array($type=>$_POST['value']));
            $this->Help->createEvent(__('Pakeitė vartotojo grupę'),'User','edit',$this->User->id,Router::url( $this->here, true ),$this->Auth->user());
			$data = $this->$model->findById($_POST['value']);
			echo $data[$model][$field]; die();
		}
	}
    
    /** @requireAuth Atverti vartotojų sąrašo puslapį */
	function index($id=null,$type=null,$model=null,$field=null){ //CakeLog::write('debug',print_r($options,true));
		if($this->request->is('post') && !isset($this->request->data['User']['filter'])){
			$this->User->id = $this->request->data['User']['id'];
			$this->User->save($this->request->data['User']);
		}
		$in = '';
		if(isset($this->request->data['User']['filter'])){
			unset($_SESSION['user_filter']);
			$in = isset($this->request->data['User']['groups'])?$this->request->data['User']['groups']:array();
            $_SESSION['user_filter'] = $in;
			$this->paginate = array('conditions'=>$in?array('group_id'=>$in):array());
		}elseif(isset($_SESSION['user_filter']) && $_SESSION['user_filter']){
			$this->paginate = array('conditions'=>array('group_id'=>$_SESSION['user_filter']));
		}
        $superadmin_count = $this->User->find('count',array(
            'conditions' => array(
                'User.group_id' => 1
            )
        ));
        $last_superadmin = ($superadmin_count > 1) ? false : true;

		$this->paginate['limit'] = 50;
		$this->paginate['order'] = 'User.username';
		$data = $this->paginate('User');
		$groups = $this->Group->find('list',array('fields'=>array('id','name'), 'order'=>array('name'))); //$groups[0] = ''; asort($groups);
		$sensorsList = $this->Sensor->find('list', array('fields'=>array('Sensor.id','Sensor.name')));
		$title_for_layout = __('Vartotojai');
		$this->set(compact('groups','last_superadmin','data','sensorsList','title_for_layout'));
		$this->set('user_id',$this->Auth->user()); 
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Users_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
    }
    
    /** @requireAuth Aktyvuoti/deaktyvuoti vartotoją */
	function activate_user($user_id){
	    $this->layout = 'ajax';
		$this->render(false);
		$this->User->id = $user_id;
		$user = $this->User->find('first',array('conditions'=>array('User.id'=>$user_id)));
		$currentLoggedUser = $this->Auth->user();
		if(!empty($user)){
			if($this->Auth->user('id') == $user['User']['id']){
				//$jason = '{%s,%s}';
				$data = array(
					'"string":"<div class=\"errorsList\">'.__('Esamo vartotojo statuso keisti negalite').'</div>"',
					'"state":"'.$user['User']['active'].'"',
					'"id":"'.$user_id.'"'
				);
				echo '{'.implode($data,',').'}';
				die();
			}
			if($user['Group']['id'] == 1 && $currentLoggedUser['Group']['id'] != 1){
				$data = array(
					'"string":"<div class=\"errorsList\">'.__('SuperAdmin vartotojų būsenas gali keisti tik SuperAdmin grupės vartotojai').'</div>"',
					'"state":"'.$user['User']['active'].'"',
					'"id":"'.$user_id.'"'
				);
				echo '{'.implode($data,',').'}';
				die();
			}
			$this->User->save(array('active'=>$user['User']['active']==1?0:1));
            if($user['User']['active']==1){
                $this->Help->createEvent(__('Aktyvavo vartotoją'),'User','edit',$this->User->id,Router::url( $this->here, true ),$this->Auth->user());
            }else{
                $this->Help->createEvent(__('Pavertė kitą vartotoją neaktyviu'),'User','edit',$this->User->id,Router::url( $this->here, true ),$this->Auth->user());
            }
		}
	}

    public function logout($sensorId='') {//public
    	//$this->components['Auth']['loginAction']['?'] = array('lang'=>Configure::read('Config.language'));
    	$activeSensor = $this->Sensor->findByActiveUserId(self::$user->id);
    	$sensorUpdateCond = array('Sensor.active_user_id'=>self::$user->id);
    	$userUpdateCond = array('User.id'=>self::$user->id);
    	if($sensorId > 0){
            $sensorUpdateCond['Sensor.id'] = $sensorId;
            $userUpdateCond['User.active_sensor_id'] = $sensorId;
        }
    	$this->Sensor->updateAll(array('Sensor.active_user_id'=>0), $sensorUpdateCond);
    	$this->User->updateAll(array('active_sensor_id'=>0), $userUpdateCond);
    	$this->User->updateAll(array('taken_over_user_id'=>0));
    	$this->Session->delete('Config.language');
    	$this->Session->delete('Auth.User');
        $this->Cookie->delete('login');
        $this->Help->createEvent(__('Vartotojas paliko sistemą'),'User','edit',array(),Router::url( $this->here, true ),$this->Auth->user());
        if(!empty($activeSensor)){
        	$this->Auth->logout();
			$this->redirect(array_merge($this->Auth->loginAction, array($activeSensor['Sensor']['id'])));	
        }else{
        	$this->redirect($this->Auth->logout());
		}	
    }

	/** @requireAuth Redaguoti vartotoją */
    public function edit($id = 0) {
        $this->requestAuth(true);
        $item = $id ? $this->User->findById($id) : null;
        if (empty($this->request->data)) {
            if(!empty($item)){
                $item[User::NAME]['sensor_id'] = explode(',', $item[User::NAME]['sensor_id']);
                $this->request->data = $item;
                $this->request->data['User']['selected_sensors'] = explode(',', $this->request->data['User']['selected_sensors']);
            }
        } else {
            $this->request->data[User::NAME]['selected_sensors'] = isset($this->request->data[User::NAME]['selected_sensors']) && is_array($this->request->data[User::NAME]['selected_sensors'])?implode(',', $this->request->data[User::NAME]['selected_sensors']):'';
            if ($this->request->data[User::NAME]['id'] == User::ID_SUPER_ADMIN) {
                $this->request->data[User::NAME]['username'] = 'admin';
            } else if ($this->request->data[User::NAME]['username'] == 'admin') {
                $this->request->data[User::NAME]['username'] = 'not admin';
            }
            if ((isset($this->request->data[User::NAME]['password']) && !strlen($this->request->data[User::NAME]['password'])) || ($item && $this->request->data[User::NAME]['password'] == $item[User::NAME]['password'])) {
                unset($this->request->data[User::NAME]['password']);
            } else if (isset($this->request->data[User::NAME]['password'])) {
                $this->request->data[User::NAME]['password'] = Security::hash($this->request->data['User']['password'], null, true);
            }
            if (!isset($this->request->data[User::NAME]['branch_id']) || !$this->request->data[User::NAME]['branch_id']) $this->request->data[User::NAME]['branch_id'] = null;
            if (!isset($this->request->data[User::NAME]['sensor_id']) || !$this->request->data[User::NAME]['sensor_id']) $this->request->data[User::NAME]['sensor_id'] = null;
            if (!isset($this->request->data[User::NAME]['line_id']) || !$this->request->data[User::NAME]['line_id']) $this->request->data[User::NAME]['line_id'] = null;
            $this->request->data[User::NAME]['sensor_id'] = isset($this->request->data[User::NAME]['sensor_id'])?implode(',', $this->request->data[User::NAME]['sensor_id']):'';
            if ($this->User->save($this->request->data[User::NAME])) {
                $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                $user = $this->User->findById($this->Auth->User('id'));
                $user['User']['Group'] = $user['Group'];
                $this->Session->write('Auth', $user);
                $this->redirect(array('controller' => 'users', 'action' => 'index'));
            }
        }
        $title = $item ? sprintf(__('Vartotojas %s (ID: %d)'), $item['User']['username'], $item['User']['id']) : __('Naujas vartotojas');
        $this->set(array(
            'title_for_layout' => $title,
            'h1_for_layout' => $title,
            'model' => 'User',
            'item' => $item,
            'branchOptions' => $this->Branch->getAsSelectOptions(true),
            'sensorOptions' => $this->Sensor->getAsSelectOptions(false,array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1),true),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
            'formUrl'=> Router::url(array('action'=>'edit',($id ? $id : '')), true),
            'permGroups'=>$this->Group->find('list', array('order'=>array('Group.name')))
            //'permKeys' => require_once(__DIR__.'/Component/AuthDescriptions.php')
        ));
		$parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Users_AfterEdit_Hook', $parameters, Configure::read('companyTitle'));
    }
    
    /** @requireAuth Priskirti vartotojui prieeigos teisę */
    function give_permission($record_id,$group_id){
    	$this->layout = 'ajax';
    	$this->render(false);
    	$res = $this->Permission->findById($record_id);
    	$mas = unserialize($res['Permission']['users']);
    	if(in_array($group_id,$mas)){
    		$key = array_search($group_id, $mas);
    		unset($mas[$key]);
    	}else{
    		$mas[] = $group_id;
    	}
    	$this->Permission->id = $record_id;
    	$this->Permission->save(array('users'=>serialize($mas)));
        $this->Help->createEvent(__('Pakeitė prieeigos prie sistemos funkcijų teises'),'Permission','edit',$this->Permission->id,Router::url( $this->here, true ),$this->Auth->user());
    	//Teisiu cash isvalymas
		Cache::config('sql', array('path' => CACHE.'sql'.DS, 'prefix'=>'permission-users_permissions_cache', 'engine'=>'File'));
    	Cache::clear(false, 'sql');
    }
    
    /** @requireAuth Atverti teisių skirstymo puslapį */
    function check_permissions(){
    	$users = $this->Group->find('all');
	    $this->set('roles',$users);
		$path = ROOT.DS.'app'.DS.'Controller'.DS;
		$pluginsPath = ROOT.DS.'app'.DS.'Plugin'.DS;
    	$continue = array('beforeFilter','afterFilter','login','logout');
    	$ids = $this->Permission->find('all',array('fields'=>array('Permission.id')));
    	$savedAllIds = array();
    	foreach($ids as $val){
    		$savedAllIds[] = $val['Permission']['id'];
    	}
        $dir = new Folder(WWW_ROOT."../Plugin");
        $folders = $dir->read();
        $folders[0] = array_combine($folders[0],$folders[0]);
        array_walk($folders[0], function(&$data)use($pluginsPath){ $data = $pluginsPath.$data.DS.'Controller'.DS; }); 
        $pathsList = array_merge(array(''=>$path), isset($folders[0])?$folders[0]:array());
    	$savedIds = array();
        foreach($pathsList as $pluginTitle => $path){
        	if($handler= opendir($path)){
    			while(false !== ($fileName= @readdir($handler))){
    				if(is_file($path. $fileName)){
    					$fh = fopen($path.$fileName, 'r');
    					$theData = fread($fh,filesize($path. $fileName));
    					$controllerName = substr($fileName,0,-14);
    					if (preg_match_all('/(\/*\* @requireAuth(.+)\*\/[\ \t\n\r]+)*.*function\s*([^_][a-zA-Z_]+)\s*\([^\)]*\)\s*{\s*\/*\/*\s*/i', $theData, $match)) {
    						foreach($match[2] as $key=> $methodDescr){
    							//if(isset($match[2]) && isset($match[2][$key]) && $match[2][$key]=='public'){continue;}
    							$action = isset($match[3]) && isset($match[3][$key])?trim($match[3][$key]):'';
                                $methodDescr = trim($methodDescr);
    							if(in_array($action,$continue) || !trim($methodDescr)) continue;
    							$res = $this->Permission->find('first',array('conditions'=>array('plugin'=>$pluginTitle,'controllers'=>$controllerName,'actions'=>$action)));
    							if(!$res){
                                    $this->Permission->create();
    								$this->Permission->save(array('plugin'=>$pluginTitle,'controllers'=>$controllerName,'actions'=>$action,'users'=>serialize(array()), 'description'=>$methodDescr));
    							}else{
    								$savedIds[] = $res['Permission']['id'];
    							}
    						}
    					}
    				}
    			}
        	}
    	}
    	foreach($savedAllIds as $val){
    		if(!in_array($val,$savedIds)){
    			$this->Permission->id = $val;
    			$this->Permission->delete();
    		}
    	}
		
    	if(isset($this->request->data['Filter'])){
			$permissions = $this->Permission->find('all',array('order'=>array('Permission.plugin','Permission.controllers','Permission.created DESC')));
			$keysDelete = array();
			
			array_walk($permissions,function($val,$key,$filters)use(&$keysDelete){
				$checkedUsers = unserialize($val['Permission']['users']);
				if(trim($filters['plugin'],'MainApp') && $filters['plugin']!=$val['Permission']['plugin']){$keysDelete[$key]=$key; return;}
				if(trim($filters['controller']) && $filters['controller']!=$val['Permission']['controllers']){$keysDelete[$key]=$key; return;}
				if(trim($filters['action']) && $filters['action']!=$val['Permission']['actions']){$keysDelete[$key]=$key; return;}
				foreach($filters as $group_id => $selectedType){
					if(!is_numeric($group_id)) continue;
					if($selectedType==2 && !in_array($group_id,$checkedUsers)){$keysDelete[$key]=$key; return;} //action turi buti nepazymetas grupei
					if($selectedType==1 && in_array($group_id,$checkedUsers)){$keysDelete[$key] = $key; return;} //action priskirtas grupei
				}
			},$this->request->data['Filter']);
			$diff = array_diff_key($permissions, $keysDelete);
			$this->set('data',$diff);
		}
        $this->Permission->virtualFields = array('pluginTitle'=>'CONCAT(plugin," ")', 'actionsGroups'=>'CONCAT(plugin,"-",controllers)');
        $controllersList = $this->Permission->find('list',array('fields'=>array('Permission.controllers','Permission.controllers','Permission.pluginTitle'),'order'=>array('Permission.controllers')));
        $controllersList = array('MainApp '=>$controllersList[' ']) + $controllersList; unset($controllersList[' ']);
        //$controllersList['TekstileNodeStage']['TekstileNodeStage2'] = 'TekstileNodeStage2';
        $this->set('availableActions',$this->Permission->find('list',array('fields'=>array('Permission.actions','Permission.actions','Permission.actionsGroups'),'order'=>array('Permission.plugin','Permission.controllers'))));
        $this->set(array('pluginsList'=>array_combine(array_keys($folders[0]),array_keys($folders[0]))));
        $this->set('availableControllers',$controllersList);
    }

	public function get_additional_field_values(){
		if(!$this->request->is('ajax')) die();
        $this->layout = 'ajax';
		$additionalFields = array(current($this->OrdersAdditionalParameter->findById($this->request->data['slug'])));
		$view = new View($this, false);
		$view->Form->create('UserAdd');
		echo $view->My->constructAdditionalField($additionalFields, $view->Form, true);
		die();
	}
    
    /** @requireAuth Sukurti/redaguoti vartotojų grupę */
    public function add_change_group(){
        $this->layout = 'ajax';
        if($this->request->data && !$this->request->is('ajax')){
            $this->Group->save($this->request->data);
            if($this->request->data['Group']['id']){
                $this->Help->createEvent(__('Sukūrė naują vartotojų grupę'),'Group','add',$this->Group->id,Router::url( $this->here, true ),$this->Auth->user());
            }else{
                $this->Help->createEvent(__('Atnaujino vartotojo grupės duomenis'),'Group','edit',$this->Group->id,Router::url( $this->here, true ),$this->Auth->user());
            }
            $this->redirect(array('action'=>'index'));
        }elseif($this->request->is('ajax') && array_key_exists('group_id',$this->request->data)){
            $this->request->data = $this->Group->findById($this->request->data['group_id']);
        }
    }
    
    /** @requireAuth Pašalinti vartotojų grupę */
    public function remove_group(){
        if(!$this->request->is('ajax') || !isset($this->request->data['group_id']) || $this->request->data['group_id'] == 1) die();
        $this->Group->id = $this->request->data['group_id'];
        if(in_array($this->Group->id, array(User::ID_SUPER_ADMIN, User::ID_MODERATOR, User::ID_GUEST, User::ID_OPERATOR))){ die(); }
        $users = $this->User->find('all', array('fields'=>array('User.username', 'User.first_name','User.last_name'), 'conditions'=>array('User.group_id'=>$this->request->data['group_id'])));
        $this->User->deleteAll(array('group_id'=>$this->Group->id));
        $this->Group->delete();
        die();
    }

	function giv_permissions($id=null,$type=null,$model=null,$field=null){ //CakeLog::write('debug',print_r($options,true));
		if($this->request->is('post') && !isset($this->request->data['User']['filter'])){
			$this->User->id = $this->request->data['User']['id'];
			$this->User->save($this->request->data['User']);
		}
		$in = '';
		if(isset($this->request->data['User']['filter'])){
			unset($_SESSION['user_filter']);
			$in = isset($this->request->data['User']['groups'])?$this->request->data['User']['groups']:array();
            $_SESSION['user_filter'] = $in;
			$this->paginate = array('conditions'=>$in?array('group_id'=>$in):array());
		}elseif(isset($_SESSION['user_filter']) && !empty($_SESSION['user_filter'])){
			$this->paginate = array('conditions'=>array('group_id'=>$_SESSION['user_filter']));
		}

        $superadmin_count = $this->User->find('count',array(
            'conditions' => array(
                'User.group_id' => 1
            )
        ));
        $last_superadmin = ($superadmin_count > 1) ? false : true;

		$this->paginate['limit'] = 500;
		$data = $this->paginate('User');
		$groups = $this->Group->find('list',array('fields'=>array('id','name'), 'order'=>array('name'))); //$groups[0] = ''; asort($groups);
		$this->set(compact('groups','last_superadmin','data'));
		$this->set('user_id',$this->Auth->user()); 
    }

    public function change_language($langSlug = ''){
        if(preg_match('/[a-z]{2}_[A-Z]{2}/',$langSlug)){
            $user = $this->Auth->user();
            $this->User->updateAll(array('language'=>'\''.$langSlug.'\''), array('User.id'=>$user['id'])); 
            $user = $this->User->findById($this->Auth->User('id'));
            $user['User']['Group'] = $user['Group'];
            $this->Session->write('Auth', $user);
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

}

?>