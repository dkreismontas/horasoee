<?php

App::uses('Plan', 'Model');
App::uses('Line', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Settings', 'Model');
App::uses('User', 'Model');
App::uses('Shift', 'Model');
App::uses('CakeEmail', 'Network/Email');

/**
 * @property Plan $Plan
 * @property Line $Line
 * @property Sensor $Sensor
 * @property Settings $Settings
 * @property User $User
 */
class TestController extends AppController {
	
	const NAME = 'test';
	
	public $uses = array(Plan::NAME, Line::NAME, Sensor::NAME, Settings::NAME, User::NAME,Shift::NAME,'Record','FoundProblem');

    public $components = array('Paginator');
	
	/** @requireAuth Atidaryti testinių įrašų generatorių */
	public function index() {
//        $mail = new CakeEmail(self::EMAIL_CONFIG);
//        $mail->from('oee@pergale.lt');
//        $mail->to('andriussev@gmail.com');
//        $mail->subject('hello');
//        $mail->emailFormat('both');
//        $mail->send();


		$this->requestAuth(true);
        $this->Log->write("Test controller accessed");
		$this->set(array(
			'sensorId' => $this->detectSensor(),
			'tickerUrl' => Router::url('tick')
		));
	}

    public function calcOrderStats() {
        $shift = $this->Shift->findById(1);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $shell = new OeeShell();
        $out = $shell->calculateOrderStats(23698,$shift);
        pr($out);

        die();
    }

    public function logs() {
        $this->requestAuth(true);

        $limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
        if (!$limit) $limit = 20;
        $arr = array('limit' => $limit, 'order' => array('id' => 'desc'));
        $this->Paginator->settings = $arr;

        $list = $this->Paginator->paginate("Log");

        $this->set(array(
            'title_for_layout' => __('Registrai'),
            'list' => $list,
        ));
    }
	
	private function detectSensor($sensorId = 3) {
		$user = $this->User->findById(self::$user->id);
//		if ($user && isset($user[User::NAME]['sensor_id']) && $user[User::NAME]['sensor_id']) {
//			$sensorId = $user[User::NAME]['sensor_id'];
//		}
		return $sensorId;
	}
	
	/** @requireAuth Generuoti testinius įrašus */
	public function tick() {
        echo "tick";
        $record_vals = $this->request->query['val'];
        if($record_vals == "") {
            $record_vals = -1;
        }
        $sensorId = $this->request->query['sensor'];
        if($sensorId == "") {
            $sensorId = 3;
        }

        $multiple = false;
        $sensors = array();
        if(strpos($this->request->query['sensor'],',') !== false) {
            $multiple = true;
            $sensors = explode(',',$this->request->query['sensor']);
        }


		$this->requestAuth(true);
		$date = new DateTime();
		$date->setTimezone(new DateTimeZone('Europe/Vilnius'));
		//$date->sub(new DateInterval('PT40S'));
		$p20s = new DateInterval('PT20S');

        $vals_array = array(0,1,1,2);

        var_dump($date->format('Y-m-d H:i:s'));

        if($multiple) {
            foreach($sensors as $sens) {
                $this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(':date_time' => $date->format('Y-m-d H:i:s'), ':sensor_id' => $sens,
                                                                                               ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals)  ));
                $date->add($p20s);
                $this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(':date_time' => $date->format('Y-m-d H:i:s'), ':sensor_id' => $sens,
                                                                                               ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals) ));
                $date->add($p20s);
                $this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(':date_time' => $date->format('Y-m-d H:i:s'), ':sensor_id' => $sens,
                                                                                               ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals) ));

                $date->sub(new DateInterval('PT40S'));
            }
            exit();
        } 
        if(Configure::read('ADD_FULL_RECORD')){
            //$lastRecord = $this->Record->find('first', array('conditions'=>array('Record.sensor_id'=>$sensorId), 'order'=>array('Record.id DESC')));
            $date = !empty($lastRecord) && time() - strtotime($lastRecord['Record']['created']) < Configure::read('recordsCycle')/2?date('Y-m-d H:i:s', (strtotime($lastRecord['Record']['created'])+Configure::read('recordsCycle'))):$date->format('Y-m-d H:i:s');
            //$date = !empty($lastRecord) && time() - strtotime($lastRecord['Record']['created']) < 99999999999999?date('Y-m-d H:i:s', (strtotime($lastRecord['Record']['created'])+Configure::read('recordsCycle'))):$date->format('Y-m-d H:i:s');
            $this->Line->query("CALL ADD_FULL_RECORD(:date_time, :sensor_id, :quantity, :recordsCycle)", array(
              ':date_time' => $date, 
              ':sensor_id' => $sensorId, 
              ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals),
              ':recordsCycle' => Configure::read('recordsCycle') 
            ));
            $this->FoundProblem->manipulate(array($sensorId));
        }else{
    		$this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(
    		  ':date_time' => $date->format('Y-m-d H:i:s'), 
    		  ':sensor_id' => $sensorId, 
    		  ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals)  
            ));
        }
		/*$date->add($p20s);
		$this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(':date_time' => $date->format('Y-m-d H:i:s'), ':sensor_id' => $sensorId,
                                                                                       ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals) ));
		$date->add($p20s);
		$this->Line->query("CALL ADD_RECORD_NO_TIME_FIX(:date_time, :sensor_id, :quantity)", array(':date_time' => $date->format('Y-m-d H:i:s'), ':sensor_id' => $sensorId,
                                                                                     ':quantity' => (($record_vals == -1) ? $vals_array[rand(0,3)] : $record_vals) ));
        */
        echo " tock";
		exit();
	}
	
//	public function mail() {
//        echo "Mail send";
//		$sysEmail = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL);
//		$sysEmailName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
//		$mail = new CakeEmail(self::EMAIL_CONFIG);
//		$mail->from(array($sysEmail => $sysEmailName));
//		$mail->to('test@realmdev.lt');
//		$mail->subject('Email Send Test');
//		$mail->emailFormat('both');
//		var_dump($mail->send('Test Email Test Body'));
//		exit();
//	}
	
}
