<?php

App::uses('PlanParser', 'Controller/Component');
App::uses('Plan', 'Model');
App::uses('Line', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Shift', 'Model');
App::uses('Record', 'Model');
App::uses('Product', 'Model');
App::uses('Settings', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('Log', 'Model');

/**
 * @property Plan $Plan
 * @property Line $Line
 * @property Sensor $Sensor
 * @property Branch $Branch
 * @property Shift $Shift
 */
class CronController extends AppController {
	
	const NAME = 'cron';
	
	public $uses = array(Plan::NAME, Line::NAME, Sensor::NAME, Branch::NAME, Shift::NAME,Record::NAME,'Product',ApprovedOrder::NAME, Settings::NAME, Log::NAME);

    public function importProducts() {
        $inputPath = ROOT.DS.APP_DIR.DS.'external/in';
        if (($dh = opendir($inputPath)) !== false) {
            while (($file = readdir($dh)) !== false) {
                $filePath = $inputPath.'/'.$file;
                if (!is_file($filePath) || !preg_match('#^(?:(?:.+\.|)plan|(?:.+|))\.xml$#i', $file)) {
                    continue;
                }
                $xml = simplexml_load_file($filePath);
                $xmlArray = Xml::toArray($xml);
                $importProducts = $xmlArray['PRODUCTS']['PRODUCT'];
                $importProducts = array_change_key_case($importProducts,CASE_LOWER);
                $importProductsFixed = array();
                foreach($importProducts as $ip) {
                    $importProductsFixed[]['Product'] = array_change_key_case($ip,CASE_LOWER);
                }
                $importProducts = $importProductsFixed;
                $production_codes = array();
                foreach($importProducts as $product) {
                    $production_codes[] = $product['Product']['production_code'];
                }
				
                $existingProducts = $this->Product->find('list',array('fields'=>array('production_code','id'),'conditions'=>array('production_code'=>$production_codes)));
                foreach($importProducts as &$product) {
                    if(array_key_exists($product['Product']['production_code'],$existingProducts)) {
                        $product['Product']['id'] = (int)$existingProducts[$product['Product']['production_code']];
                    }
                }
                $this->Product->saveAll($importProducts);

                //@unlink($filePath);
            }
        }
        exit();
    }

    public function importPlans() {
//		if ($_SERVER['REMOTE_ADDR'] != '127.0.0.1' && $_SERVER['REMOTE_ADDR'] != '::1') exit();
        $inputPath = $this->Settings->getOne('plan_import_path');
        if(trim($inputPath)){
            $inputPath = $this->Settings->getLatestFilePathFromFtp($this->Settings->getOne('plan_import_path'), true, array('xml'));
            if(key($inputPath)=='success'){
                $this->importPlansFromFile(current($inputPath));
            }else{
                pr($inputPath);
            }
        }else{
            $inputPath = ROOT.DS.APP_DIR.DS.'external/in';
            if (($dh = opendir($inputPath)) !== false) {
                while (($file = readdir($dh)) !== false){
                    $filePath = $inputPath.'/'.$file;
                    if (!is_file($filePath) || !preg_match('#^(?:(?:.+\.|)plan|(?:.+|))\.xml$#i', $file)) {
                        continue;
                    }
                    $this->importPlansFromFile($filePath);
                }
                closedir($dh);
            }
        }
        exit();
    }

    private function importPlansFromFile($filePath){
        static $sensors, $parser, $lines, $sensorLines = null;
        if($sensors == null){
            $parser = new PlanParser();
            $fLines = $this->Line->find('all');
            $lines = array(); foreach ($fLines as $li) { $lines[$li[Line::NAME]['ps_id']] = $li[Line::NAME]['id']; }
            $fSensors = $this->Sensor->withRefs()->find('all');
            $sensors = array();
            $sensorLines = array();
            foreach ($fSensors as $li) {
                if(Configure::read('companyTitle') == 'Vilniausduona'){
                    $sensors[$li[Branch::NAME]['ps_id'].'::'.$li[Sensor::NAME]['ps_id']] = $li[Sensor::NAME]['id'];
                }else{
                    $sensors[$li[Sensor::NAME]['ps_id']] = $li[Sensor::NAME]['id'];
                }
                if (isset($li[Line::NAME]['id'])) $sensorLines[$li[Sensor::NAME]['id']] = $li[Line::NAME]['id'];
            }
            $removedUnused = false;
        }
        try {
            echo '<pre>';
            $planAC = $this->Plan;
            $parser->parse($filePath, function($plan) use (&$planAC, &$lines, &$sensors, &$sensorLines, &$removedUnused) {
                if (!$removedUnused) { $removedUnused = true; $planAC->removeUnused(); }
                $planAC->create();
                if(Configure::read('companyTitle') == 'Vilniausduona'){
                    $skey = $plan['branch_id'].'::'.$plan['sensor_id'];
                }else{ $skey = $plan['sensor_id']; }
                unset($plan['branch_id']);
                $plan['sensor_id'] = ($plan['sensor_id'] && isset($sensors[$skey])) ? $sensors[$skey] : null;
                if(!$plan['sensor_id']) return;
                //$plan['line_id'] = ($plan['line_id'] && isset($lines[$plan['line_id']])) ? $lines[$plan['line_id']] : null;
                $plan['line_id'] = ($plan['sensor_id'] && isset($sensorLines[$plan['sensor_id']])) ? $sensorLines[$plan['sensor_id']] : null;
                $parameters = array(&$plan);
                $pluginData = $this->Help->callPluginFunction('Cron_BeforeImportPlanSave_Hook', $parameters, Configure::read('companyTitle'));
                // Copy to R1 DATA
                $planCopyToR1 = null;
                if($plan['production_code'] == 39000) {
                    $planCopyToR1 = $plan;
                    $planCopyToR1['sensor_id'] = 2;
                }
                if(in_array($skey, ['280::L46', '280::L45'])){ //idetas 2020-12-11 tik panevezio dviems jutikliams
                    $plan['step'] = 1800;
                }
                try {
                    if($plan['production_code'] == 39000) {
                        // Save to R1
                        $planAC->save(array(Plan::NAME => $planCopyToR1));
                        $planAC->create();
                    }
                    $planExist = $planAC->find('first', array('conditions'=>array('Plan.sensor_id'=>$plan['sensor_id'], 'Plan.mo_number'=>$plan['mo_number'])));
                    if($planExist){  //jei planas yra ir jis siuo metu vykdomas, plana perrasyti galima
                        $planInProgress = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS, 'ApprovedOrder.plan_id'=>$planExist['Plan']['id'])));
                        if(!empty($planInProgress)){  $plan['id'] = $planInProgress['ApprovedOrder']['plan_id']; $planExist = null; }
                    }
                    if(!$planExist){
                        if (!$plan['line_id'] || !$plan['sensor_id'] || substr($plan['start'], 0, 1) == '#' || substr($plan['end'], 0, 1) == '#' || !$planAC->save(array(Plan::NAME => $plan))) {
                            // TODO: log save errors
                            //print_r($plan);
                            if (!$plan['sensor_id']) {
                                $msg = sprintf(__("Sensorius nerastas. (padalinys::sensorius = '%s')."), $skey);
                            } else if (!$plan['line_id']) {
                                $msg = __('Sensorius neturi linijos.');
                            } else if (substr($plan['start'], 0, 1) == '#') {
                                $msg = __("Blogas pradžios datos laiko formatas: '%s'.", substr($plan['start'], 1));
                            } else if (substr($plan['end'], 0, 1) == '#') {
                                $msg = __("Blogas pabaigos datos laiko formatas: '%s'.", substr($plan['end'], 1));
                            } else {
                                $msg = __('Klaida įrašant į duomenų bazę.');
                            }
                            echo $msg.'<br/>';
                            CakeLog::write('debug','Error: '.$msg);
                            file_put_contents(LOGS.'/plan_import_error.log', ('['.date('Y-m-d H:i:s').'] '.$msg.' in file \''.$filePath.'\''."\n"), FILE_APPEND);
                        }
                    }
                } catch (PDOException $ex) {
                    CakeLog::write('debug','PDO EXC: '.json_encode($ex));
                    echo 'DB Error, most likely duplicate record.<br/>';
                    file_put_contents(LOGS.'/plan_import_error.log', ('['.date('Y-m-d H:i:s').'] '.$ex->getMessage().' in file \''.$filePath.'\''."\n"), FILE_APPEND);
                }
            });
            if (!file_exists(ROOT.DS.APP_DIR.DS.'external/in/Archive/')) {
                mkdir(ROOT.DS.APP_DIR.DS.'external/in/Archive/', 0777, true);
            }
            $backupPath = ROOT.DS.APP_DIR.DS.'external/in/Archive/';
            copy($filePath, $backupPath.date('Y-m-d_H:i:s').'.xml');
            @unlink($filePath);
            if (($dirOpen = opendir($backupPath)) !== false) {
                while (($bcFile = readdir($dirOpen)) !== false) {
                    if (!is_file($backupPath.'/'.$bcFile) || !preg_match('#^(?:(?:.+\.|)plan|(?:.+|))\.xml$#i', $bcFile)){continue;}
                    if(time() - filemtime($backupPath.'/'.$bcFile) >  3600*24*14){
                        unlink($backupPath.'/'.$bcFile);
                    }
                }
                closedir($dirOpen);
            }
            $this->Log->write('Atliktas sekmingas planu importavimas');
        } catch (ErrorException $ex) {
            throw $ex;
            CakeLog::write('debug','Err: '.json_encode($ex));
            // TODO: log parse errors
        }
    }

    public function generateShifts($redirecBack = false, $generateBranchId = null){
        $markerPath = __dir__.'/../webroot/files/shifts_under_constructions.txt';
        if(file_exists($markerPath) && time() - filemtime($markerPath) < 300 ){die('Kalendorius jau kuriamas');}
        $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
        fclose($fh);
        $this->Shift->generateShifts($generateBranchId);
        unlink($markerPath);
		if(trim($redirecBack) && isset($_SERVER['HTTP_REFERER'])){
			$lastDate = $this->Shift->find('first', array('order'=>array('Shift.end DESC')));
			$this->Session->setFlash(__('Pamainos sėkmingai sugeneruotos iki %s', $lastDate['Shift']['end']), 'default', array(), 'saveMessage');
			$this->redirect($_SERVER['HTTP_REFERER']);
		}
        return;
    }
    
    public function calcOee($daysBack=2, $returnReferrer=0) {
        $markerPath = __dir__.'/../Plugin/'.Configure::read('companyTitle').'/webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath)){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
        }
        ini_set('memory_limit','-1');
        set_time_limit(2000);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $shell = new OeeShell();
        $shell->daysBack = $daysBack;
        $out = $shell->main(); //23516 //23519
        if(file_exists($markerPath)){
        	unlink($markerPath);
		}
        $this->Session->setFlash(__('OEE sėkmingai perskaičiuotas'), 'default', array(), 'oeeRecalSussess');
        if($returnReferrer) $this->redirect($_SERVER['HTTP_REFERER']);
        die();
    }

    public function sendSubscriptions() {
        //$this->Sensor->informAboutProblems();
        ini_set('memory_limit','-1');
        set_time_limit(2000);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'SubscriptionShell');
        $shell = new SubscriptionShell();
        $out = $shell->main();
        die();
    }

    public function moveDatesToPlans() {
        $this->Log->write("moveDatesToPlans");
        $q = 'UPDATE plans as p
            INNER JOIN (
                    SELECT MIN(created) as min_cr, MAX(end) as max_end, plan_id, status_id
                    FROM approved_orders
                    GROUP BY plan_id

                ) as ao ON p.id = ao.plan_id
            SET p.start = ao.min_cr, p.end = ao.max_end
            WHERE start IS NULL and ao.status_id IN (2,3,4,5,6)';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);

        die();
    }

    public function moveOldRecords() {
        $this->sendSubscriptions();
        $this->Log->write("moveOldRecords");
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 150000 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 150000;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);

        die();
    }

    public function removeSessions(){
        $projects = glob('/var/www/html/*'); //get all file names
        foreach($projects as $project){
            if(is_dir($project)){
                $files = glob($project.'/app/tmp/sessions/*'); //get all file names
                foreach($files as $file){
                    if(is_file($file)){
                        unlink($file);
                    }
                }
            }
        }
        die();
    }
    
}