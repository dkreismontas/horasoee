<?php

App::uses('CakeEmail', 'Network/Email');
App::uses('Problem', 'Model');

class DashboardsController extends AppController {

    public $name = 'Dashboards';
    public $components = array(
        'Opengraph', 'Help', 'Googlechart'
    );
    public $uses = array('Dashboard', 'DashboardsSetting', 'Sensor', 'Shift', 'Plan', 'Branch', 'Record', 'DiffCard', 'ApprovedOrder', 'Loss', 'FoundProblem', 'DashboardsCalculation', 'DashboardsCalculationMo', 'Problem','User','StatisticalReportsSubscription','Log');
    private $indicators = array();

    private function setIndicators() {
        $this->indicators = $this->Dashboard->setIndicators();
    }

    public function get_time_pattern() {
        $pattern = array('start' => '', 'end' => '');
        switch ($this->request->data['pattern_id']) {
            case 0: //praejusi pamaina

                break;
        }
        echo json_encode($pattern);
        die();
    }

    public function beforeFilter() {
        parent::beforeFilter();
        $this->setIndicators();
        $this->Opengraph->indicators = $this->indicators;
    }
	
	/** @requireAuth Atverti statistikos skydelį */
    public function inner() {
       // fb($this->Dashboard->time_patterns);
        $this->requestAuth(true);
        $conditions = array();
        $subscription = '';
        if (isset($this->params['named']['subscription'])) {
            $subscription = $this->params['named']['subscription'];
            $conditions[] = array('DashboardsSetting.id' => array_keys(unserialize(base64_decode($subscription))));
        } else {
            $conditions[] = array('DashboardsSetting.user_id' => self::$user->id);
        }
        $this->FoundProblem->addSupperParentToProblems($this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded')));
        $this->Sensor->bindModel(array('belongsTo' => array('Branch')));
        $sensorsList = $this->Sensor->getAsSelectOptions(false, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1));
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $operatorsList = $this->User->find('list', array('fields'=>array('id', 'full_name'), 'conditions'=>array('User.group_id'=>User::ID_OPERATOR,'or'=>array('User.first_name <>'=>'','User.last_name <>'=>''))));
        //$conditions[] = array('DashboardsSetting.id' => 69);
        $widgets = $this->DashboardsSetting->find('all', array('conditions' => $conditions, 'order' => array('DashboardsSetting.position','DashboardsSetting.id DESC')));
        $usersHavingMailList = $this->User->find('list', array('fields'=>array('User.id', 'full_name'),'conditions'=>array('User.id <>'=>$this->Auth->user()['id'],'User.email <>'=>'','or'=>array('User.first_name <>'=>'', 'User.last_name <>'=>''))));
        $this->User->virtualFields = array();
        $sensorBranches = $this->Sensor->find('list', array('fields'=>array('Sensor.id','Sensor.branch_id'), 'conditions'=>array('Sensor.id'=>Configure::read('user')->selected_sensors)));
        //$shiftsTmp = $this->Shift->find('list', array('fields' => array('Shift.type'), 'order' => array('Shift.id DESC'), 'limit' => 100, 'group' => 'Shift.type', 'conditions'=>array('Shift.branch_id'=>array_unique($sensorBranches))));
        //$shiftsFromConfig = Set::combine(Set::extract('/shifts',Configure::read('shift_patterns')), '{n}.shifts.shift_title', '{n}.shifts');
        $shiftsFromConfigTmp = Set::extract('/shifts',Configure::read('shift_patterns'));
        foreach($shiftsFromConfigTmp as $shiftFromConfigTmp){
            $shiftTitle = $shiftFromConfigTmp['shifts']['shift_title'];
            if($shiftFromConfigTmp['shifts']['disabled']){ continue; }
            if(isset($shiftsFromConfig[$shiftTitle])){
                $shiftsFromConfig[$shiftTitle] = $shiftTitle;
            }else {
                $shiftsFromConfig[$shiftTitle] = $shiftTitle.' ~ ' . $shiftFromConfigTmp['shifts']['start'] . ' - ' . $shiftFromConfigTmp['shifts']['end'];
            }
        }
        $branches = $this->Branch->find('all');
        foreach($branches as $branch) {
            $branchId = $branch['Branch']['id'];
            foreach ($shiftsFromConfig as $shiftTitle) {
                $branchesShifts[$branchId][] = $shiftTitle;
            }
        }

        $downtimesTmp = $this->Problem->find('all', array('conditions'=>array('Problem.disabled'=>0,'Problem.id >'=>0)));
        foreach($downtimesTmp as $downtimeTmp){
            $downtimes[$downtimeTmp['Problem']['line_id']][$downtimeTmp['Problem']['level']][$downtimeTmp['Problem']['sensor_type']][$downtimeTmp['Problem']['id']] = Settings::translate($downtimeTmp['Problem']['name']);
        }
        //$this->Session->write('downtimes',$downtimes);
        $lineSensors = $this->Sensor->find('list', array('fields'=>array('id','line_id')));
        $typeSensors = $this->Sensor->find('list', array('fields'=>array('id','type')));
        $timePatterns = $this->Dashboard->getTimePatterns();
        $statSubscrPeriodTypes = $this->Dashboard->getStatisticalSubscriptionsPeriodicityTypes();
        $widgets = array_filter($widgets, fn($widget) => array_key_exists(unserialize($widget['DashboardsSetting']['data'])['Node']['indicator1'], $this->indicators));
        $this->set(compact('widgets', 'subscription', 'sensorsList','operatorsList','usersHavingMailList','branchesShifts','sensorBranches','downtimes','lineSensors','typeSensors','timePatterns','statSubscrPeriodTypes'));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Dashboards_AfterInner_Hook', $parameters, Configure::read('companyTitle'));
    }

    public function widget($type = 1) {
        $this->layout = 'ajax';
        $this->set(compact('type'));
        $ident = rand(0, time());
        $sensorsList = $this->Sensor->getAsSelectOptions(false, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1));
        $subscription = isset($this->params['named']['subscription']) ? $this->params['named']['subscription'] : '';
        $maxDowntimeLevel = (int)current($this->Problem->find('first', array('fields'=>array('MAX(Problem.level) AS max_level')))[0]);
        $downtimeLevelsList = array();
        for($i=0; $i<=$maxDowntimeLevel; $i++){
            $downtimeLevelsList[$i] = __('%d lygmuo', $i);
        }
        if (isset($this->request['widget'])) {
            $widget_data = $this->request['widget']['DashboardsSetting'];
            $ident = $this->request['ident'];
            $this->set(compact('widget_data', 'ident'));
        }
        $this->set(compact('ident', 'sensorsList', 'subscription','downtimeLevelsList'));

        $this->set('indicators', $this->indicators);
        $parameters = array(&$this, &$this->indicators);
        if($pluginData = $this->Help->callPluginFunction('Dashboards_widget_Hook', $parameters, Configure::read('companyTitle'))){
            return $pluginData;
        }
        return $this->render('widgets/widget' . $type);
    }


    public function chart($type = 1) {
        $this->layout = 'ajax';
        $params = array(&$this->Dashboard);
        $this->Help->callPluginFunction('Dashboards_BeforeChart_Hook', $params, Configure::read('companyTitle'));
        $data = isset($this->request->data) ? $this->request->data : array();
        $ident = time();
        if (isset($this->request->params['widget'])) {
            $data = unserialize($this->request->params['widget']);
            //$ident = $this->request->params['key'];
        }
        if (isset($data['Node']['base64_img_src']) && trim($data['Node']['base64_img_src'])) { //atsiunciami eksportuojant grafika i xls faila
            $data['Node']['export_type'] = 2;
            $data['Node']['file_name'] = time().'.png';
            $scr = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '',$data['Node']['base64_img_src']));
            $path = ROOT.'/app/'.WEBROOT_DIR.'/img/opengraphs/';
            file_put_contents($path.$data['Node']['file_name'], $scr);
            $ident = $data['Node']['uid'];
        }elseif (isset($this->request->params['uid'])) {
            $ident = $this->request->params['uid'];
        }elseif(isset($this->request->data['Node']['uid']) && $this->request->data['Node']['uid']){
            $ident = $this->request->data['Node']['uid'];
        }else $ident = '';
        //$this->render(false);
        $subscription = isset($this->params['named']['subscription']) ? @unserialize(base64_decode($this->params['named']['subscription'])) : '';
        if (is_array($subscription) && isset($subscription[$this->request->params['graph_id']]) && sizeof($subscription[$this->request->params['graph_id']]) >= 1) {
            $data['Node']['date_start_end'] = $subscription[$this->request->params['graph_id']][0].' ~ '.$subscription[$this->request->params['graph_id']][1];
            // $data['Node']['date_start'] = $subscription[$this->request->params['graph_id']][0];
            // $data['Node']['date_end'] = $subscription[$this->request->params['graph_id']][1];
            $data['Node']['time_pattern'] = '';
        }
        if (!empty($data)) {
            //$ident = $data['Node']['ident'];
            $componentsList = array(
                'duration'  => $this->Duration,
                'help'      => $this->Help,
                'opengraph' => $this->Opengraph,
                'googlechart' => $this->Googlechart
            );
            $this->Dashboard->components = &$componentsList;
            //jei rodomas grafikas per prenumerata, jis gaus subscription parametra ir is jo pasiimame laiko intervala
//            fb($data['Node'],'nodedata');
            $dashboardModel = ClassRegistry::init('Dashboard');
            $graphData = $dashboardModel->start_calculation($data['Node'], false);
            unset($dashboardModel);
            if (is_array($graphData)) {
                $this->render(false);
                $this->_export_files($graphData['data'], $graphData['title'], $graphData['file_name']);
            } else {
                if (!is_object(json_decode($graphData)) && !is_array(json_decode($graphData))) {
                    $this->render(false);
                }else{
                    $this->set(compact('graphData', 'ident'));
                    if(is_object(json_decode($graphData))){
                        $this->render('charts/chart1');
                    }else{
                        $this->render('charts/googlechart');
                    }
                }
            }
        }
    }

    private function _export_files($data, $title, $file_name) {
        $this->layout = 'empty';
        App::import('Vendor', 'mexel/PHPExcel');
        App::import('Vendor', 'mexel/PHPExcel/IOFactory');
        $objPHPExcel = new PHPExcel();
        $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
        $this->set(compact('objPHPExcel', 'objDrawing', 'data', 'title', 'file_name'));
        $this->render('xls_display');
    }

    public function saveWidget($node_id = null) { 
        if(preg_match('/\d{4}-/', trim($this->request->data['Node']['date_start_end']))){
            $this->request->data['Node']['time_pattern'] = -1;
            if(isset($this->request->data['Node']['date_start_end']) && strpos($this->request->data['Node']['date_start_end'], '~') > 0){
                list($this->request->data['Node']['date_start'], $this->request->data['Node']['date_end']) = explode(' ~ ', $this->request->data['Node']['date_start_end']);
            }else{
                $this->request->data['Node']['date_start'] = $this->request->data['Node']['date_end'] = date('Y-m-d');
            }
        }else{
            $this->request->data['Node']['date_start'] = $this->request->data['Node']['date_end'] = trim($this->request->data['Node']['date_start_end']);
        }
        $post_data = $this->request->data['Node'];
        $data = array();
        $data['id'] = $post_data['id'];
        $data['type'] = $post_data['type'];
        $data['data'] = serialize($this->request->data);
        $data['user_id'] = $post_data['user_id']??self::$user->id;
        
        $data['node_id'] = $node_id;
        $errors = $this->Dashboard->validateData($post_data);
        if (!empty($errors)) {
            $this->layout = 'ajax';
            $this->set(compact('errors'));
            return $this->render('errors');
        }
        $this->DashboardsSetting->save($data);
        if(!$data['id']){
            $this->Log->write('Sukurtas naujas statistikos skydelis su duomenimis: '.json_encode($data));
        }
        echo $this->DashboardsSetting->id;
        $this->layout = 'empty';
        die();
    }

    public function remove_widget() {
        $this->layout = 'ajax';
        $this->render(false);
        $this->DashboardsSetting->id = $this->request->data['widget_id'];
        $this->Log->write('Pašalintas statistikos skydelis su duomenimis: '.json_encode($this->DashboardsSetting->read()));
        $this->DashboardsSetting->delete();
    }

    public function share_widget(){
        $dashboardCopy = $this->DashboardsSetting->findById($this->request->data['widget_id'])['DashboardsSetting'];
        $data = unserialize($dashboardCopy['data']);
        $data['Node']['uid'] = uniqid('uid');
        $dashboardCopy['data'] = serialize($data);
        unset($dashboardCopy['id']);
        unset($dashboardCopy['subscribed_users']);
        $dashboardCopy['user_id'] = $this->request->data['user_id'];
        $this->DashboardsSetting->save($dashboardCopy);
        die();
    }

    public function check_file_created() {
        $rownputFileName = ROOT . DS . 'app' . DS . 'webroot' . DS . 'img' . DS . 'opengraphs' . DS . $this->request->data['file_name'];
        if (!file_exists($rownputFileName)) {
            echo json_encode(array('status' => 0));
            die();
        }
        echo json_encode(array('status' => 1));
        die();
    }

    public function get_subscription_graphs() {
        $this->layout = 'ajax';
        $adminUsersIds = $this->User->find('list', array('fields'=>array('User.id'), 'conditions'=>array('User.group_id'=>1)));
        //$user_id = array_merge($adminUsersIds,array(self::$user->id)); //prasyta atjungti kggroupui 2020-04-22
        $user_id = self::$user->id;
        $graphs = $this->DashboardsSetting->find('all', array(
            'fields' => array('DashboardsSetting.id', 'DashboardsSetting.data', 'DashboardsSetting.subscribed_users','DashboardsSetting.user_id'),
            'conditions' => array('DashboardsSetting.user_id' => $user_id,'DashboardsSetting.user_id >'=>0), 'order' => array('DashboardsSetting.position'))
        );
        fb($graphs);
        $list = array();
        $selectedList = array();
        foreach ($graphs as $graph) {
            $rowData = $graph['DashboardsSetting'];
            $data = unserialize($rowData['data']);
            if (!in_array($data['Node']['time_pattern'], $this->Dashboard->timeGroupsForSubcribsions)) continue;
            $list[$rowData['id']] = $data['Node']['graph_name'] ? $data['Node']['graph_name'] : __("- Be pavadinimo");
            $selected = explode(',', $graph['DashboardsSetting']['subscribed_users']);
            if (in_array(self::$user->id, $selected)) {
                $selectedList[] = $rowData['id'];
            }
        }
        $this->set(compact('list', 'selectedList'));
    }

    public function save_subscription_graphs() {
        $this->layout = 'ajax';
        $this->render(false);
        $data = $this->request->data;
        $subscribed_users = $this->DashboardsSetting->find('first', array('fields' => array('DashboardsSetting.subscribed_users'), 'conditions' => array('DashboardsSetting.id' => $data['graph_id'])));
        switch ($data['action']) {
            case 0://remove
                $ids = explode(',', $subscribed_users['DashboardsSetting']['subscribed_users']);
                while (is_numeric($key = array_search(self::$user->id, $ids))) {
                    unset($ids[$key]);
                }
                break;
            case 1://append
                $ids = trim($subscribed_users['DashboardsSetting']['subscribed_users']) ? explode(',', $subscribed_users['DashboardsSetting']['subscribed_users']) : array();
                $ids[] = self::$user->id;
                break;
        }
        $this->DashboardsSetting->id = $data['graph_id'];
        $this->DashboardsSetting->save(array('subscribed_users' => implode(',', $ids)));
    }

    public function change_dashboard_position(){
        if(!$this->request->is('ajax'))die();
        $case_string = "CASE DashboardsSetting.id";
        foreach($this->request->data['items'] as $index => $id) {
            $case_string .= " WHEN ".$id." THEN ".$index;
        }
        $case_string .= " END";
        $this->DashboardsSetting->updateAll(array('DashboardsSetting.position'=>$case_string),array('DashboardsSetting.id'=>$this->request->data['items']));
        die();
    }

    public function generateMassXLS() {
//        $date_start = '2015-06-12';
//        $date_end = '2015-06-17';
//        $sensor_id = 3;


        $settings = ClassRegistry::init('Settings');
        $sysName = $settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);

        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator($sysName)
            ->setLastModifiedBy($sysName)
            ->setTitle("Gamybos Ataskaita");

        $workSheet = $objPHPExcel->setActiveSheetIndex(0);
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        $sensor_id = $this->request->data['sensors'];

        $sensor = $this->Sensor->withRefs()->findById($sensor_id);

        $interval = DateInterval::createFromDateString('1 day');
        $dateObjEnd = new DateTime($date_end);
        $period = new DatePeriod(new DateTime($date_start), $interval, $dateObjEnd->modify('+1 day'));

        $currentRow = 2;
        $workSheet->setCellValue('A' . $currentRow, __('Laiko intervalas'));
        $workSheet->setCellValue('B' . $currentRow ++, $date_start . " - " . $date_end);
        $workSheet->setCellValue('A' . $currentRow, __('Darbo centras'));
        $workSheet->setCellValue('B' . $currentRow ++, $sensor['Branch']['name'] . " " . $sensor['Sensor']['ps_id']);

        // Set headers
        $currentRow = 5;
        $workSheet->setCellValue('A' . $currentRow, '');
        $workSheet->setCellValue('B' . $currentRow, '');
        $workSheet->setCellValue('C' . $currentRow, __('Suplanuotas kiekis'));
        $workSheet->setCellValue('D' . $currentRow, __('Supakuotas kiekis'));
        $workSheet->setCellValue('E' . $currentRow, __('Skirtumas nuo plano'));
        $workSheet->setCellValue('F' . $currentRow, __('Pagamintas kiekis (kg)'));
        $workSheet->setCellValue('G' . $currentRow, __('Brokas (kg)'));
        $workSheet->setCellValue('H' . $currentRow, __('Broko %'));

        $workSheet->getStyle('A' . $currentRow . ':H' . $currentRow)
            ->getBorders()
            ->getBottom()
            ->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);

        // * DAY

        foreach ($period as $dt) {
            $period_date = $dt->format('Y-m-d');

            $approved_orders = $this->ApprovedOrder->withRefs()->find('all', array(
                'conditions' => array(
                    'ApprovedOrder.end >'     => $period_date . " 00:00:00",
                    'ApprovedOrder.end <'     => $period_date . " 23:59:59",
                    'ApprovedOrder.sensor_id' => $sensor_id
                )
            ));
            $total_date_sums = array('plan_quantity' => 0, 'fact_quantity' => 0, 'produced_quantity' => 0, 'waste' => 0, 'losses' => 0);


            // Parse all finished approved orders with plan ids as index
            $aos_finished = array();
            foreach ($approved_orders as $approved_order) {
                if ($approved_order['ApprovedOrder']['status_id'] == 2) {
                    $aos_finished[$approved_order['ApprovedOrder']['plan_id']] = $approved_order;
                }
            }

            foreach ($approved_orders as $approved_order) {
                if ($approved_order['ApprovedOrder']['status_id'] != 2) {
                    $plan_id = $approved_order['ApprovedOrder']['plan_id'];
                    if (array_key_exists($approved_order['ApprovedOrder']['plan_id'], $aos_finished)) {
                        $aos_finished[$plan_id]['ApprovedOrder']['quantity'] += $approved_order['ApprovedOrder']['quantity'];
                        $aos_finished[$plan_id]['ApprovedOrder']['box_quantity'] += $approved_order['ApprovedOrder']['box_quantity'];
                        $aos_finished[$plan_id]['ApprovedOrder']['confirmed_quantity'] += $approved_order['ApprovedOrder']['confirmed_quantity'];
                    }
                }
            }

            $approved_orders = array_values($aos_finished);

            // Grouped approved orders by production code
            $aos_grouped = array();
            foreach ($approved_orders as $approved_order) {
                $production_code = $approved_order['Plan']['production_code'];
                $aos_grouped[$production_code][] = $approved_order;
            }
//        foreach($plans as $plan) {
//            $production_code = $plan['Plan']['production_code'];
//            //if(array_key_exists($production_code,$plans_grouped))
//            $plans_grouped[$production_code][] = $plan;
//        }
            usort($aos_grouped, function ($a, $b) {
                return sizeof($a) == 1;
            });
//        usort($plans_grouped,function($a,$b) {
//            return sizeof($a) == 1;
//        });

            ++ $currentRow;

            $workSheet->setCellValue('A' . $currentRow, $period_date);
            foreach ($aos_grouped as $ao_group) {
                $ao_group_size = sizeof($ao_group);
                $ao_group_sums = array('plan_quantity' => 0, 'fact_quantity' => 0, 'produced_quantity' => 0, 'waste' => 0, 'losses' => 0);
                foreach ($ao_group as $i => $ao) {
                    $ao_format = $ao['Plan']['start'] . " " . $ao['Plan']['end'] . " " . $ao['Plan']['mo_number'] . " " . $ao['Plan']['production_code'] . " " . $ao['Plan']['production_name'];
//                var_dump($plan_format);
//                var_dump($plan['Plan']['quantity']);
//                var_dump($plan['ApprovedOrder']['confirmed_quantity']);
//                echo "<br>";
                    $workSheet->setCellValue('B' . $currentRow, $ao_format);
                    $workSheet->setCellValue('C' . $currentRow, $ao['Plan']['quantity']);
                    $workSheet->setCellValue('D' . $currentRow, $ao['ApprovedOrder']['confirmed_quantity']);
                    $workSheet->setCellValue('E' . $currentRow, $ao['ApprovedOrder']['confirmed_quantity'] - $ao['Plan']['quantity']);
                    $workSheet->setCellValue('F' . $currentRow, $ao['Plan']['weight'] * $ao['ApprovedOrder']['confirmed_quantity']);

                    $losses = $this->Loss->find('list', array(
                        'fields'     => array('Loss.value'),
                        'conditions' => array('approved_order_id' => $ao['ApprovedOrder']['id'])
                    ));
                    $loss_quantity = 0;
                    foreach ($losses as $loss) {
                        $loss_quantity += $loss;
                    }
                    $workSheet->setCellValue('G' . $currentRow, $loss_quantity);
                    if ($ao['Plan']['weight'] * $ao['ApprovedOrder']['confirmed_quantity'] != 0) {
                        $workSheet->setCellValue('H' . $currentRow, round(($loss_quantity / ($ao['Plan']['weight'] * $ao['ApprovedOrder']['confirmed_quantity'])) * 100, 3) . "%");
                    }

                    $currentRow ++;
                    if ($ao_group_size > 1) {
                        $ao_group_sums['plan_quantity'] += $ao['Plan']['quantity'];
                        $ao_group_sums['fact_quantity'] += $ao['ApprovedOrder']['confirmed_quantity'];
                        $ao_group_sums['produced_quantity'] += $ao['Plan']['weight'] * $ao['ApprovedOrder']['confirmed_quantity'];
                        $ao_group_sums['waste'] += $loss_quantity;
                    }
                    $total_date_sums['plan_quantity'] += $ao['Plan']['quantity'];
                    $total_date_sums['fact_quantity'] += $ao['ApprovedOrder']['confirmed_quantity'];
                    $total_date_sums['produced_quantity'] += $ao['Plan']['weight'] * $ao['ApprovedOrder']['confirmed_quantity'];
                    $total_date_sums['waste'] += $loss_quantity;
                }
                if ($ao_group_size > 1) {
                    $ao_group_name = $ao_group[0]['Plan']['production_code'] . " " . $ao_group[0]['Plan']['production_name'];
//                var_dump($plan_group_name);
//                echo "<br>";
                    $workSheet->setCellValue('B' . $currentRow, $ao_group_name . " Summary");
                    $workSheet->setCellValue('C' . $currentRow, $ao_group_sums['plan_quantity']);
                    $workSheet->setCellValue('D' . $currentRow, $ao_group_sums['fact_quantity']);
                    $workSheet->setCellValue('E' . $currentRow, $ao_group_sums['fact_quantity'] - $ao_group_sums['plan_quantity']);
                    $workSheet->setCellValue('F' . $currentRow, $ao_group_sums['produced_quantity']);
                    $workSheet->setCellValue('G' . $currentRow, $ao_group_sums['waste']);
                    if ($ao_group_sums['produced_quantity'] != 0) {
                        $workSheet->setCellValue('H' . $currentRow, round(($ao_group_sums['waste'] / $ao_group_sums['produced_quantity']) * 100, 3) . "% ");
                    }

                    $workSheet->getStyle('B' . $currentRow . ':H' . $currentRow)->applyFromArray(
                        array(
                            'fill' => array(
                                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                                'color' => array('rgb' => 'DDDDDD')
                            )
                        )
                    );
                    $currentRow ++;
                }
            }

            $workSheet->setCellValue('B' . $currentRow, 'Summary');
            $workSheet->setCellValue('C' . $currentRow, $total_date_sums['plan_quantity']);
            $workSheet->setCellValue('D' . $currentRow, $total_date_sums['fact_quantity']);
            $workSheet->setCellValue('E' . $currentRow, $total_date_sums['fact_quantity'] - $total_date_sums['plan_quantity']);
            $workSheet->setCellValue('F' . $currentRow, $total_date_sums['produced_quantity']);
            $workSheet->setCellValue('G' . $currentRow, $total_date_sums['waste']);
            if ($total_date_sums['produced_quantity'] != 0) {
                $workSheet->setCellValue('H' . $currentRow, round(($total_date_sums['waste'] / $total_date_sums['produced_quantity']) * 100, 3) . "% ");
            }

            $workSheet->getStyle('B' . $currentRow . ':H' . $currentRow)->applyFromArray(
                array(
                    'fill' => array(
                        'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => 'BBBBBB')
                    )
                )
            );

            $workSheet->getStyle('A' . $currentRow . ':H' . $currentRow)
                ->getBorders()
                ->getBottom()
                ->setBorderStyle(PHPExcel_Style_Border::BORDER_MEDIUM);


        }


        // * END DAY
        $workSheet->getStyle('A2:H5')->getFont()->setBold(true);

        for ($col = 'A'; $col !== 'I'; $col ++) {
            $objPHPExcel->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }


        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Gamybos_ataskaita.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
        header('Cache-Control: cache, must-revalidate');
        header('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }
    
     public function save_statistical_reports_subscription(){
         parse_str($this->request->data['Subscription']['form_data'], $formData);
         if(!empty($formData)){
             $this->StatisticalReportsSubscription->save(array(
                 'user_id'=>self::$user->id,
                 //'type'=>$formData['data']['form_type']??__('nenurodyta'),
                 'name'=>__('Be pavadinimo'),
                 'data'=>isset($formData['data'])?json_encode($formData['data']):'',
                 'periodicity'=>$this->request->data['Subscription']['period_type'],
                 'form_action'=>$this->request->data['Subscription']['form_action'],
                 'subscribed'=>1
             ));
             die();
         }
         header("HTTP/1.0 404 Not Found");
         die();
     }

     public function get_statistical_subscription_graphs(){
        $this->layout = 'ajax';
        $subscriptions = $this->StatisticalReportsSubscription->find('all', array('conditions'=>array('user_id'=>self::$user->id)));
        if(empty($subscriptions)){
            echo '<h3>Nėra sukurtų prenumeratų</h3>'; die();
        }
        $timePatterns = $this->Dashboard->getTimePatterns();
        $statSubscrPeriodTypes = $this->Dashboard->getStatisticalSubscriptionsPeriodicityTypes();
        $this->set(compact('subscriptions','timePatterns','statSubscrPeriodTypes'));
     }

     public function change_statistical_subscription_graph(){
         $updateFields = array_map(function($data){
             return '\''.$data.'\'';
         },$this->request->data['updateFields']);
         $this->StatisticalReportsSubscription->updateAll($updateFields, $this->request->data['updateWhere']);
         die();
     }

    public function share_statistical_subscription(){
        $dashboardCopy = $this->StatisticalReportsSubscription->findById($this->request->data['widget_id'])['StatisticalReportsSubscription'];
        unset($dashboardCopy['id']);
        unset($dashboardCopy['created']);
        unset($dashboardCopy['modified']);
        $dashboardCopy['user_id'] = $this->request->data['user_id'];
        $this->StatisticalReportsSubscription->save($dashboardCopy);
        die();
    }

    public function send_statistical_subscription_graph(){
        $this->layout = 'ajax';
        $this->StatisticalReportsSubscription->bindModel(array('belongsTo'=>array('User')));
        $dashboard = $this->StatisticalReportsSubscription->findById($this->request->data['subscribtionId']);
        $settings = json_decode($dashboard['StatisticalReportsSubscription']['data'],true);
        $extension = $settings['extension']??'xls';
        $filename = Inflector::slug($dashboard['StatisticalReportsSubscription']['name']).'.'.$extension;
        $emailsList = explode(',', $dashboard['User']['email']);
        $emailsList = array_map(function($email){ return trim(strtolower($email)); }, $emailsList);
        $additionalEmailsList = explode("\n", $dashboard['User']['additional_emails']);
        $additionalEmailsList = array_map(function($email){ return trim($email); }, $additionalEmailsList);
        $emailsList = array_merge($emailsList, $additionalEmailsList);
        $emailsList = array_unique(array_filter($emailsList));
        if(empty($emailsList)){
            echo json_encode(array('errors'=>__('Vartotojas neturi nurodyto el. pašto, kuriuo galima siųsti ataskaitą')));
            header("HTTP/1.0 404 Not Found");
            die();
        }
        $this->requestAction(Router::parse($dashboard['StatisticalReportsSubscription']['form_action']), array('data'=>$settings, 'named'=>array('exportToFile'=>$filename)));
        $statSubscrPeriodTypes = $this->Dashboard->getStatisticalSubscriptionsPeriodicityTypes();
        list($start,$end) = $this->Help->setStartEndByPattern($settings);
        $mailText = '
                    <table>
                        <tr><th colspan="2">'.Configure::read('companyTitle').' '.__('sistemos ataskaitų prenumerata').'</th></tr>
                        <tr><td colspan="2"></td></tr>
                        <tr><td><b>'.__('Ataskaitos tipas').':</b></td><td>'.$settings['form_type'].'</td></tr>
                        <tr><td><b>'.__('Ataskaitos pavadinimas').':</b></td><td>'.$dashboard['StatisticalReportsSubscription']['name'].'</td></tr>
                        <tr><td><b>'.__('Ataskaitos periodiškumas').':</b></td><td>'.($statSubscrPeriodTypes[$dashboard['StatisticalReportsSubscription']['periodicity']]??__('Periodiškumo tipas neegzistuoja')).'</td></tr>
                        <tr><td><b>'.__('Laikotarpis').':</b></td><td>'.$start.' - '.$end.'</td></tr>
                    </table>
                ';
        $mail = new CakeEmail('default');
        $mail->from('noreply@horasoee.eu');
        $mail->to(array_unique($emailsList));
        $mail->subject(Configure::read('companyTitle').' '.__('sistemos ataskaitų prenumerata'));
        $mail->emailFormat('both');
        $mail->attachments(WWW_ROOT.'files/'.$filename);
        $mail->send($mailText);
        unlink(WWW_ROOT.'files/'.$filename);
        $this->render(false);
    }

    public function delete_statistical_subscription_graph(){
        $this->StatisticalReportsSubscription->delete($this->request->data['subscribtionId']);
        die();
    }

}
