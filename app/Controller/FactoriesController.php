<?php
App::uses('Factory', 'Model');
class FactoriesController extends AppController{
    
    public $uses = array('Factory');
    public $name = 'Factories';
    const MODEL = Factory::NAME;
    
    /** @requireAuth Peržiūrėti fabrikus */
    public function index() {
        $this->requestAuth(true);
        $this->set(array(
            'title_for_layout' => __('Fabrikai'),
            'list' => $this->Factory->find('all'),
            'model' => self::MODEL,
            'newUrl' => Router::url('edit/0'),
            'editUrl' => Router::url('edit/%d'),
            'removeUrl' => Router::url('remove/%d'),
            'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?')
        ));
    }
    
    /** @requireAuth Redaguoti padalinius */
    public function edit($id) {
        $this->requestAuth(true);
        $listUrl = Router::url(array('controller' => strtolower($this->name), 'action' => 'index'), true);
        if (empty($this->request->data)) {
            $this->request->data = $item = $this->Factory->findById($id);
        }else{
            $item = null;
            if ($this->Factory->save($this->request->data)) {
                $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                $this->redirect($listUrl);
            }
        }
        $title = $item ? sprintf(__('Fabrikas  %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Naujas fabrikas');
        $this->set(array(
            'title_for_layout' => $title,
            'h1_for_layout' => $title,
            'model' => self::MODEL,
            'listUrl' => $listUrl,
            'formUrl'=> Router::url('edit/'.($id ? $id : 0), true)
        ));
    }
    
    /** @requireAuth Pašalinti fabrikus */
    public function remove($id) {
        $this->requestAuth(true);
        $listUrl = Router::url(array('controller' => strtolower($this->name), 'action' => 'index'), true);
        try {
            if ($this->Factory->delete($id, false)) {
                $this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
            } else {
                $this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
            }
        } catch (PDOException $ex) {
            $code = ''.$ex->getCode();
            if (substr($code, 0, 2) == '23') {
                $this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
            } else {
                $this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
            }
        }
        $this->redirect($listUrl);
    }
    
}
