<?php
App::uses('ApprovedOrder', 'Model');
class WorkCenter6hController extends AppController{
    
    public $uses = array('Wc6hReview','Sensor','Shift','WorkCenter','ApprovedOrder','Record','Settings');
    
    public function display($workCenters=''){
        $selectedSensors = Configure::read('user')->selected_sensors;
        $reviews = $this->Wc6hReview->find('all', array('order'=>array('Wc6hReview.name')));
        $reviews = array_filter($reviews, function($review)use($selectedSensors){
            return empty(array_diff(explode(',', $review['Wc6hReview']['sensors_ids']), $selectedSensors));
        });
        $this->set(array(
            'title_for_layout' => __('Darbo centrų peržiūra 6h'),
            'reviews' => $reviews,
            'hasAccessToEdit'=>$this->hasAccessToAction(array('controller'=>'workcenter6h', 'action'=>'edit')),
            'hasAccessToRemove'=>$this->hasAccessToAction(array('controller'=>'workcenter6h', 'action'=>'remove')),
        ));
    }

    /** @requireAuth Darbo centrų peržiūra 6h: sukurti naują peržiūros rinkinį */
    public function edit($id = 0){
        if(!empty($this->request->data)){
            $data = array(
                'name'=>$this->request->data['Wc6hReview']['name']??'',
                'sensors_ids'=>isset($this->request->data['Wc6hReview']['sensors_ids'])?implode(',', $this->request->data['Wc6hReview']['sensors_ids']):'',
            );
            if(isset($this->request->data['Wc6hReview']['id']) && $this->request->data['Wc6hReview']['id'] > 0){
                $data['id'] = $this->request->data['Wc6hReview']['id'];
            }
            if($this->Wc6hReview->save($data)){
                $this->redirect(array('action'=>'display'));
            }
        }
        if($id > 0){
            $this->request->data = $this->Wc6hReview->findById($id);
            $this->request->data['Wc6hReview']['sensors_ids'] = explode(',', $this->request->data['Wc6hReview']['sensors_ids']);
        }
        $title_for_layout = $h1_for_layout = __('Darbo centrų rinkinio %s', $id==0?__('sukūrimas'):__('redagavimas'));
        $icon_for_layout = 'iconfa-laptop';
        $sensors = $this->Sensor->getAsSelectOptions(false,array(),true);
        $this->set(compact('title_for_layout','h1_for_layout','icon_for_layout','sensors'));
    }

    /** @requireAuth Darbo centrų peržiūra 6h: pašalinti peržiūros rinkinį */
    public function remove($id = 0){
        $this->Wc6hReview->id = $id;
        $this->Wc6hReview->delete();
        $this->redirect(array('action'=>'display'));
    }

    /** @requireAuth Darbo centrų peržiūra 6h: peržiūrėti rinkinį */
    public function show($id = 0){
        $this->layout = 'full_page';
        $this->set(array(
            'id'=>$id,
            'exceededTransitionId'=>$this->Settings->getOne('exceeded_transition_id')
        ));
    }

    public function update_status($reviewId){
        $this->layout = 'empty';
        $sensorsIds = explode(',',$this->Wc6hReview->findById($reviewId)['Wc6hReview']['sensors_ids']);
        $sensors = array();
        foreach($sensorsIds as $sensorId) {
            $this->Sensor->bindModel(array('belongsTo'=>array(
                'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.sensor_id = Sensor.id AND ApprovedOrder.status_id ='.ApprovedOrder::STATE_IN_PROGRESS)),
                'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
            )));
            $fSensor = $this->Sensor->findById($sensorId);
            $fSensor['Sensor']['dark_and_light_green_intervals'] = 0;
            //$fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
            Configure::write('graphHoursDefaultLinesCount', 6);
            $hours = $this->WorkCenter->buildTimeGraph($fSensor);
            $dcHoursInfoBlocks = is_array(Configure::read('workcenter_hours_info_blocks'))?Configure::read('workcenter_hours_info_blocks'):array();
            $this->Record->calculateMainFactorsInHour($dcHoursInfoBlocks,$hours,$fSensor);
            $sensors[] = array(
                'periods' => $hours,
                'id'=>$fSensor['Sensor']['id'],
                'name'=>$fSensor['Sensor']['name'],
                'current_plan_name'=>$fSensor['Plan']['production_name']??'-',
                'hours_info_blocks'=>$dcHoursInfoBlocks,
                //'oee' => $oee,
            );
        }
        $columnsCount = sizeof($sensors) <= 2?1:2;
        $sensors = $this->Wc6hReview->combineSensorsIntoColumns($sensors, $columnsCount);
        ob_start('ob_gzhandler');
        echo json_encode(array(
            'sensors' => $sensors,
            'columns'=> $columnsCount,
        ));
        ob_end_flush();
        die();
    }
    
}
