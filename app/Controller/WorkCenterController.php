<?php

App::uses('Plan', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('Sensor', 'Model');
App::uses('Line', 'Model');
App::uses('Problem', 'Model');
App::uses('FoundProblem', 'Model');
App::uses('Shift', 'Model');
App::uses('Record', 'Model');
App::uses('User', 'Model');
App::uses('DiffCard', 'Model');
App::uses('Loss', 'Model');
App::uses('LossType', 'Model');
App::uses('Settings', 'Model');
App::uses('Branch', 'Model');
App::uses('CakeEmail', 'Network/Email');
App::uses('ApprovedOrdersController', 'Controller');
App::uses('DiffCardsController', 'Controller');
App::uses('TimelineEvent', 'Controller/Component');
App::uses('TimelinePeriod', 'Controller/Component');
App::uses('Exporter', 'Controller/Component');
App::uses('Log', 'Model');
App::uses('Product', 'Model');
App::uses('PartialQuantity', 'Model');

/**
 * @property Plan $Plan
 * @property ApprovedOrder $ApprovedOrder
 * @property Sensor $Sensor
 * @property Line $Line
 * @property Problem $Problem
 * @property FoundProblem $FoundProblem
 * @property Shift $Shift
 * @property Record $Record
 * @property User $User
 * @property DiffCard $DiffCard
 * @property Loss $Loss
 * @property LossType $LossType
 * @property Settings $Settings
 * @property SessionComponent $Session
 */
class WorkCenterController extends AppController {

    const ID = 'workCenter';

    const KEY_TIMESTAMP_STORE = 'timestamp_store';

    public $uses = array(
        Plan::NAME,
        Sensor::NAME,
        Line::NAME,
        ApprovedOrder::NAME,
        Problem::NAME,
        FoundProblem::NAME,
        Shift::NAME,
        Record::NAME,
        User::NAME,
        DiffCard::NAME,
        Loss::NAME,
        LossType::NAME,
        Settings::NAME,
        Log::NAME,
        PartialQuantity::NAME,
        'Product','Branch','DashboardsCalculation','WorkCenter','QualityFactor','Mail'
    );
    public $components = array('RequestHandler','Help');
    private $onlyUpdateStatus = false;
    private $currentSensor = array();

    /** @var DateInterval */
    private $recTimeOffset;
    private $timezone = 'Europe/Vilnius';

    public function beforeFilter() {
        parent::beforeFilter();
        $off = intval($this->Settings->getOne(Settings::S_RECORD_TIME_OFFSET));
        if ($off) $this->recTimeOffset = new DateInterval('PT'.$off.'S');
        $this->Record->user = $this->Log->user;
        set_time_limit(60);
    }

    public function beforeRender() {
        $res = parent::beforeRender();
        $this->layout = 'bare';
        $bodyClass = array('work-center');
		$fSensor = $this->getSensor();
        $parameters = array(&$bodyClass, &$fSensor);
        $pluginData = $this->Help->callPluginFunction('Workcenter_afterBeforeRender_Hook', $parameters, Configure::read('companyTitle'));
        $this->set('bodyClass', $bodyClass);
        return $res;
    }

    /** @requireAuth Darbo centras: atidaryti */
    public function index() {
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $structure = Configure::read('workcenter_orders_table_structure');
        if($fSensor['Sensor']['type'] == Sensor::TYPE_PACKING && $fSensor['Sensor']['calculate_quality'] > 0 && array_key_exists('add_mo_number', Configure::read('workcenter_orders_table_structure'))){
            $structure['add_mo_number']['type']='type_and_select';
            $structure['add_mo_number']['values']='sensor.available_order_numbers';
        }
		$data = array(
            'pluginFunctionality' => $this->getPluginFunctionality(),
            'title_for_layout' => $fSensor['Sensor']['name'],
            //'logoutUrl' => Router::url(array('controller' => 'users', 'action' => 'logout', (int)$fSensor['User']['sensor_id']>0?$fSensor['Sensor']['id']:'')),
            'logoutUrl' => Router::url(array('controller' => 'users', 'action' => 'logout')),
            'newDiffCardUrl' => Router::url(array('controller' => 'diff_cards', 'action' => '0/edit')),
            'wc' => ($fSensor != null && !empty($fSensor)) ? $fSensor['Sensor']['id'] : 0,
            'sensorData'=>($fSensor != null && !empty($fSensor)) ? $fSensor : 0,
            'dc' => (isset($fSensor['dc']) ? $fSensor['dc'] : null),
            'structure' => $structure,
            'choosePlanAfterTransition'=>$fSensor['Sensor']['choose_plan_after_transition'],
            'allowChangeProcess'=>$this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'allowChangeProcess')),
            'exceeded_transition_id'=>$this->Settings->getOne('exceeded_transition_id'),
            'workIntervalsColors' => TimelinePeriod::createColorsIntervals($fSensor['Sensor']['work_intervals_colors']),
            'pluginScripts'=>'',
            'noWorkProblem' => $this->Problem->findById(Problem::ID_NO_WORK),
        );
        //pr(json_encode($data));die();
		$parameters = array(&$data, &$fSensor);
        $pluginData = $this->Help->callPluginFunction('WorkCenter_index_Hook', $parameters, Configure::read('companyTitle'));
        $this->set($data);
    }

    /** @requireAuth Darbo centras: Redaguoti darbo procesų tipus */
    public function allowChangeProcess() {
        die();//metodas reikalingas tik del teisiu galimybes keisti procesus operatoriui
    }

    /** @requireAuth Darbo centras: Pakeisti prastovas į perėjimo prastovą*/
    public function changeProblemToTransition() {die();}

    /** @requireAuth Darbo centras: Pakeisti prastovas į nėra darbo prastovą*/
    public function changeProblemToNoWork() {die();}

    /** @requireAuth Darbo centras: Leisti skaldyti pasibaigusias prastovas*/
    public function allowDowntimeSplit() {die();}

    public function storeTimestamp() {
        $this->requestAuth(true, false, 'WorkCenterController::completeProd');
        $dateTime = new DateTime();
        $this->Session->write(self::KEY_TIMESTAMP_STORE, $dateTime);
        $this->set($val = array(

        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function createAndStartPlan() {
        //$this->requestAuth(true);
        $newPlan = $this->request->data['plan'];
        $identical_plan = $this->Plan->find('first',array(
            'conditions'=>array(
                'sensor_id'=>$newPlan['sensor_id'],
                'mo_number'=>$newPlan['mo_number'],
                'disabled'=>0,
            )
        ));
        if($newPlan['end'] < $newPlan['start']) {
            echo '{"errors":["Plano pabaiga ankstesnė už pradžią"]}';
            die();
        }
        if(!empty($identical_plan)) {
            echo '{"errors":["Planas su šiuo MO jau egzistuoja"]}';
            die();
        }

        $this->Plan->save($newPlan);
        $newPlan['id'] = $this->Plan->id;
        echo '{"newPlan":'.json_encode($newPlan).'}';

        die();
    }

    /** @requireAuth Darbo centras: registruoti gamybos pradžią */
    public function startProd() {
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $type = $this->request->query('type');
        $additionalData = $this->request->query('additionalData');
        try {
            if ($type == 0) {
                $planId = $this->request->params['id'];
                //patikra kad nebutu vykdomas netycia jau uzbaigtas planas
                $completedApprovedOrderExist = $this->ApprovedOrder->find('first', array(
                    'recursive' => -1,
                    'conditions' => array(
                        'ApprovedOrder.sensor_id' => $fSensor['Sensor']['id'],
                        'ApprovedOrder.plan_id' => $planId,
                        'ApprovedOrder.status_id' => ApprovedOrder::STATE_COMPLETE
                    )
                ));
                $fPlan = $this->Plan->withRefs()->findById($planId);
                $parameters = array(&$fPlan, $fSensor, &$completedApprovedOrderExist);
                $pluginData = $this->Help->callPluginFunction('WorkCenter_afterChoosePlan_Hook', $parameters, Configure::read('companyTitle'));
                //if (empty($fPlan) || $fPlan[Plan::NAME]['sensor_id'] != $fSensor[Sensor::NAME]['id'] || $completedApprovedOrderExist) {
                if (empty($fPlan) || $completedApprovedOrderExist) {
                    $fPlan = null;
                }
            } else if ($type == 1) {//vykdomi gaminiai ir is ju sukuriamas planas
                $productId = $this->request->params['id'];
                $fProduct = $this->Product->findById($productId);
                $line_id = 0;
                if (in_array($fSensor['Sensor']['id'], array(4, 5, 6, 7, 8))) {
                    $line_id = 2;
                } elseif (in_array($fSensor['Sensor']['id'], array(14, 17, 18, 19, 20, 21, 22))) {
                    $line_id = 3;
                } elseif (in_array($fSensor['Sensor']['id'], array(9))) {
                    $line_id = 4;
                } elseif (in_array($fSensor['Sensor']['id'], array(10))) {
                    $line_id = 5;
                } elseif (in_array($fSensor['Sensor']['id'], array(11))) {
                    $line_id = 6;
                }
                $dataFromInputForm = json_decode($additionalData, true);
                $errorsList = $this->Plan->checkInputFormFields($dataFromInputForm, $fSensor, $fProduct);
                if (!empty($errorsList)) {
                    echo json_encode(array('errors' => $errorsList));
                    die();
                }
                $fPlan = array(
                    'sensor_id' => $fSensor['Sensor']['id'],
                    'line_id' => $line_id,
                    'production_name' => $fProduct['Product']['name'],
                    'production_code' => $fProduct['Product']['production_code'],
                    'mo_number' => isset($dataFromInputForm['add_mo_number']) ? $dataFromInputForm['add_mo_number'] : $fSensor['Sensor']['id'] . "_" . uniqid(),
                    'product_id' => $fProduct['Product']['id'],
                    'box_quantity' => $fProduct['Product']['kiekis_formoje'],
                    'package_quantity' => 20,
                    'disabled' => 1,
                    'step' => $fProduct['Product']['kiekis_formoje'] * $fProduct['Product']['formu_greitis'],
                    'preparation_time' => isset($fProduct['Product']['preparation_time']) ? $fProduct['Product']['preparation_time'] : 0
                );
                $parameters = array(&$fPlan, &$fProduct, $fSensor, $dataFromInputForm);
                $pluginData = $this->Help->callPluginFunction('WorkCenter_BuildStep_Hook', $parameters, Configure::read('companyTitle'));
                $this->Plan->save($fPlan);
                $fPlan = $this->Plan->findById($this->Plan->id);
            }
            $currentPlan = $this->Plan->getCurrentPlan($fSensor);
            if (!empty($currentPlan) && $fPlan['Plan']['mo_number'] == $currentPlan['Plan']['mo_number']) {
                die();//Stabdome uzsakymo kurima, nes sis planas jau siuo metu ir vykdomas
            }
            if ($fPlan) {
                $this->ApprovedOrder->findAndComplete($fSensor);
            }
            $lastApprovedOrder = $this->ApprovedOrder->find('first', array(
                    'conditions' => array('ApprovedOrder.sensor_id' => $fSensor['Sensor']['id'], 'ApprovedOrder.status_id' => ApprovedOrder::STATE_COMPLETE),
                    'order' => array('ApprovedOrder.id DESC'))
            );
            $lastTransitionProblem = $this->FoundProblem->find('first', array(
                    'conditions' => array(
                        'FoundProblem.problem_id' => array(Problem::ID_NEUTRAL, Problem::ID_NO_WORK),
                        'FoundProblem.sensor_id' => $fSensor['Sensor']['id'],
                        'FoundProblem.end >' => !empty($lastApprovedOrder) ? $lastApprovedOrder['ApprovedOrder']['end'] : 0,
                    ), 'order' => array('FoundProblem.id DESC'))
            );
            if (!empty($lastTransitionProblem)) {
                if ($lastTransitionProblem['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL) {
                    //Jei nuo paskutinio uzsakymo buvo perejimas, pradedame nuo perejimo pradzios
                    $dateTime = new DateTime($lastTransitionProblem['FoundProblem']['start']);
                } elseif ($lastTransitionProblem['FoundProblem']['problem_id'] == Problem::ID_NO_WORK) {
                    //Jei nuo paskutinio uzsakymo buvo nera darbo, tai gamyba prasideda arba nuo nera darbo pabaigos arba nuo is eiles einanciu geru irasu kiekio (jei si data velesne uz nera darbo laika)
                    $startByNoWork = new DateTime($lastTransitionProblem['FoundProblem']['end']);
                    if ((int)$fSensor['Sensor']['new_production_start'] > 0) {
                        $newProdStartBySensor = new DateTime($fSensor['Sensor']['new_production_start']);
                    } else {
                        $newProdStartBySensor = DateTime::createFromFormat('Y-m-d?H:i:sP', $this->request->query('dateTime'));
                    }
                    $dateTime = $newProdStartBySensor > $startByNoWork ? $newProdStartBySensor : $startByNoWork;
                }
                //$dateTime = $startByTransition > $dateTime?$startByTransition:$dateTime;
            } elseif (Configure::read('ADD_FULL_RECORD') && (int)$fSensor['Sensor']['new_production_start'] > 0) {
                $dateTime = new DateTime($fSensor['Sensor']['new_production_start']);
            } else {
                $dateTime = $this->request->query('dateTime');
                if ($dateTime) $dateTime = DateTime::createFromFormat('Y-m-d?H:i:sP', $dateTime);
            }
            if (!empty($lastApprovedOrder)){
                $lastOrderEnd = new DateTime($lastApprovedOrder['ApprovedOrder']['end']);
                $dateTime = $lastOrderEnd > $dateTime ? $lastOrderEnd : $dateTime;
            }

            if ($fPlan && $dateTime) {
                $this->ApprovedOrder->create();
                $appOrder = array(ApprovedOrder::NAME => array(
                    'created' => $dateTime->format('Y-m-d H:i:s'),
                    'end' => $dateTime->format('Y-m-d H:i:s'),
                    'sensor_id' => $fSensor[Sensor::NAME]['id'],
                    'plan_id' => $fPlan[Plan::NAME]['id'],
                    'status_id' => ApprovedOrder::STATE_IN_PROGRESS,
                    'additional_data' => $additionalData
                ));
                $parameters = array(&$appOrder, &$fPlan, &$fSensor);
                $pluginData = $this->Help->callPluginFunction('WorkCenter_BeforeAppOrderSave_Hook', $parameters, Configure::read('companyTitle'));

                $this->ApprovedOrder->save($appOrder);
                // if(Configure::read('ADD_FULL_RECORD')){
                // $this->Record->updateAll(array('approved_order_id'=>$this->ApprovedOrder->id), array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$dateTime->format('Y-m-d H:i:s'), 'Record.approved_order_id IS NULL'));
                // }
                $this->Log->write("Sukuriamas uzsakymas" . json_encode($appOrder) . ' paskutinis gamintas uzsakymas: ' . json_encode($lastApprovedOrder) . ' Laikas is GET' . $this->request->query('dateTime') . ' Laikas is sensoriaus: ' . $fSensor['Sensor']['new_production_start'] . ' Paskutine perejimu probl: ' . json_encode($lastTransitionProblem), $fSensor);
                $this->FoundProblem->updateAll(
                    array(
                        'approved_order_id' => $this->ApprovedOrder->id,
                        'plan_id' => $fPlan[Plan::NAME]['id']),
                    array(
                        'FoundProblem.end >' => $dateTime->format('Y-m-d H:i:s'),
                        'FoundProblem.problem_id NOT' => Problem::ID_NO_WORK,
                        'FoundProblem.sensor_id' => $fSensor['Sensor']['id'],
                        'FoundProblem.plan_id IS NULL'
                    )
                );
                $this->Record->updateAll(
                    array(
                        'approved_order_id' => $this->ApprovedOrder->id,
                        'plan_id' => $fPlan[Plan::NAME]['id'],
                        'Record.unit_quantity' => 'Record.quantity * ' . $fPlan[Plan::NAME]['box_quantity']),
                    array(
                        'Record.created >=' => $dateTime->format('Y-m-d H:i:s'),
                        'Record.sensor_id' => $fSensor['Sensor']['id'],
                        'Record.plan_id IS NULL'
                    )
                );
                $quantitiesBeforePlanSelect = $this->Record->find('first', array(
                    'fields' => array('SUM(Record.unit_quantity) AS unit_quantity'),
                    'conditions' => array('approved_order_id' => $this->ApprovedOrder->id)
                ));
                $quantitiesBeforePlanSelect = $quantitiesBeforePlanSelect['0']['unit_quantity'] ?? 0;
                if ($quantitiesBeforePlanSelect > 0) {
                    $this->ApprovedOrder->updateAll(
                        array(
                            'ApprovedOrder.quantity' => $quantitiesBeforePlanSelect,
                            'ApprovedOrder.box_quantity' => $fPlan[Plan::NAME]['box_quantity'] > 0 ? bcdiv($quantitiesBeforePlanSelect, $fPlan[Plan::NAME]['box_quantity'], 4) : 0,
                        ), array(
                            'ApprovedOrder.id' => $this->ApprovedOrder->id
                        )
                    );
                }
            }
        }catch (Exception $e){
            pr($e);
            $this->Mail->send_proxy_sms(Configure::read('companyTitle') . ' projekte nepavyko suformuoti uzsakymo. '.$e->getMessage());
        }
        $this->set($val = array());
        $this->set('_serialize', array_keys($val));
        $this->Log->write("Production started. Plan:".json_encode($fPlan).";", $fSensor);
        $this->RequestHandler->renderAs($this, 'json');
    }

    /** @requireAuth Darbo centras: registruoti produkcijos gamybos atidėjimą */
    public function postphoneProd() {
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        $dateTime = $this->request->query('dateTime');
        $fake = ($this->request->query('fake') ? true : false);
        if ($dateTime) {
            if ($fake) {
                $dateTime = new DateTime($dateTime);
                $dateTime->setTimezone(new DateTimeZone(date_default_timezone_get()));
            } else {
                $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
            }
        }
        $this->Log->write("Postphoning production at : ". json_encode($dateTime) .";", $fSensor);
        if ($currentPlan && isset($currentPlan[ApprovedOrder::NAME]['id']) && $dateTime) {
            $this->Log->write("+ Production postphoned. Sensor:".json_encode($fSensor) ."; Current Plan:".json_encode($currentPlan).";", $fSensor);
            $ord = $this->ApprovedOrder->findById($currentPlan[ApprovedOrder::NAME]['id']);
            if ($fake) {
                $ord[ApprovedOrder::NAME]['quantity'] = $this->Record->findAndSumOrder($currentPlan[ApprovedOrder::NAME]['id']);
                $ord[Plan::NAME] = $currentPlan[Plan::NAME];
                $ord[Branch::NAME] = $fSensor[Branch::NAME];
                $this->Log->write('Exporting FAKE postponed: ' . json_encode($ord) . ";",$fSensor);
                //Exporter::exportPostphoned($ord);

                // Updating exported quantity
                $this->ApprovedOrder->id = $currentPlan[ApprovedOrder::NAME]['id'];
                $this->ApprovedOrder->set('exported_quantity',$currentPlan[ApprovedOrder::NAME]['exported_quantity'] + ($currentPlan[ApprovedOrder::NAME]['quantity']-$currentPlan[ApprovedOrder::NAME]['exported_quantity']));
                $this->ApprovedOrder->save();
            } else {
                $ord[ApprovedOrder::NAME]['status_id'] = ApprovedOrder::STATE_POSTPHONED;
                $ord[ApprovedOrder::NAME]['end'] = $dateTime->format('Y-m-d H:i:s');
                $this->Record->findAndSetOrder($fSensor, new DateTime($ord[ApprovedOrder::NAME]['created']), new DateTime($ord[ApprovedOrder::NAME]['end']), $ord[ApprovedOrder::NAME]['id'], $currentPlan);
                $q = $this->Record->findAndSumOrder($ord[ApprovedOrder::NAME]['id']);
                $ord[ApprovedOrder::NAME]['quantity'] = $q;
                $ord[ApprovedOrder::NAME]['box_quantity'] = $currentPlan[Plan::NAME]['box_quantity'] > 0?floor($q / floatval($currentPlan[Plan::NAME]['box_quantity'])):0;
                $ord[ApprovedOrder::NAME]['confirmed_quantity'] = 0;
                $ord[ApprovedOrder::NAME]['confirmed'] = 0;
                $this->ApprovedOrder->create(false);
                $parameters = array(&$ord, &$fSensor);
                $pluginData = $this->Help->callPluginFunction('WorkCenter_BeforePostphoneProdSave_Hook', $parameters, Configure::read('companyTitle'));
                $this->ApprovedOrder->save($ord);
                $this->QualityFactor->calculateOrderQualityOnProductionEnd($fSensor, $currentPlan);
                $this->Sensor->save(array(
                    'id'=>$fSensor['Sensor']['id'],
                    'quantity_records_count'=>0,
                    'new_production_start'=>'0000-00-00',
                ));
                $ord[Plan::NAME] = $currentPlan[Plan::NAME];
                $ord[Branch::NAME] = $fSensor[Branch::NAME];
                $this->Log->write('Exporting postponed: ' . json_encode($ord) . ";", $fSensor);
                $fFoundProblem = $this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);
                $shift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], $dateTime);
                $this->Log->write('problem id in query: ' . $this->request->query('problemId'), $fSensor);
                $problemId = $this->request->query('problemId');

                $this->setProductionEnd($fFoundProblem,$fSensor,$shift,$problemId);
            }



        }
        $this->set($val = array(

        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    /** @requireAuth Darbo centras: registruoti gamybos pabaigą */
    public function completeProd(){
//        CakeLog::write('debug', 'Completing production');
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        $fLossTypes = $this->LossType->find('all', array('conditions'=>array('LossType.id >'=>0,'OR'=>array('LossType.line_ids'=>'', 'FIND_IN_SET('.$fSensor['Line']['id'].',LossType.line_ids)'))));
        foreach ($fLossTypes as $idx => $li) {
            $fLossTypes[$idx][LossType::NAME]['value'] = (($v = $this->request->query('lt_'.$li[LossType::NAME]['id'])) ? floatval($v) : 0);
        }
        $problemId = $this->request->query('problemId');
        $quickSwap = $this->request->query('quickSwap') ? true : false;
        if ($quickSwap) {
            /* @var $timeStamp DateTime */
            $timeStamp = $this->Session->read(self::KEY_TIMESTAMP_STORE);
        }
        //$dateTime = $this->request->query('dateTime');
        //if ($dateTime) $dateTime = DateTime::createFromFormat('Y-m-d H:i:s', $dateTime);
        $dateTime = new DateTime();
        $this->Log->write("Completing production at : ". json_encode($dateTime) .";", $fSensor);
        $this->Log->write("Atsiunciami duomenys per GET : ". json_encode($this->request->query) .";", $fSensor);

        $maxDiff = 91;
        if(!Configure::read('ADD_FULL_RECORD')){
            $this->detectEvents($fSensor,$currentPlan,$maxDiff,$dateTime,$problemId);
        }

        if ($currentPlan && isset($currentPlan[ApprovedOrder::NAME]['id']) && $dateTime && (!$quickSwap || $timeStamp)) {
            $this->Log->write("Production completing. Plan:" . json_encode($currentPlan). ";", $fSensor);

            $ord = $this->ApprovedOrder->findById($currentPlan[ApprovedOrder::NAME]['id']);
            $this->ApprovedOrder->partialComplete($fSensor[Sensor::NAME]['id'], $ord[ApprovedOrder::NAME]['plan_id']);
            $ord[ApprovedOrder::NAME]['status_id'] = $quickSwap ? ApprovedOrder::STATE_WAITING_FOR_DATA : ApprovedOrder::STATE_COMPLETE;
            if ($quickSwap) {
                $this->Log->write("Quickswap turned on", $fSensor);
                $this->Session->write(self::KEY_TIMESTAMP_STORE, null);
                $ord[ApprovedOrder::NAME]['swap_time'] = $timeStamp->format('Y-m-d H:i:s');
            }
            $end = $quickSwap ? $timeStamp->add(new DateInterval('PT25S'))->format('Y-m-d H:i:s') : $dateTime->format('Y-m-d H:i:s');
            $fFoundProblem = $this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);
            //jei uzsakymo uzdarymo metu vyksta nepazymeta prastova, tai ji bus verciama derinimu, kuris turi priklausyti sekanciam uzsakymui. Taigi uzsakymas uzdaromas anksciau, pagal sio derinimo pradzia,kad sekantis uzsakymus vyktu kartu su nauju derinimu 2019-01-02
            if($fFoundProblem && $fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_NOT_DEFINED) {
                $end = $fFoundProblem[FoundProblem::NAME]['start'];
                $this->Log->write("Uzsakymo uzdarymo metu vyksta nepazymeta prastova, kuri virs i derinima, todel uzsakymo pabaiga perrasome i derinimo pradzia. ".json_encode($fFoundProblem), $fSensor);
            }
            if(new DateTime($end) > new DateTime($ord[ApprovedOrder::NAME]['created'])){
                $ord[ApprovedOrder::NAME]['end'] = $end;
            }
            $this->Record->findAndSetOrder($fSensor, new DateTime($ord[ApprovedOrder::NAME]['created']), new DateTime($ord[ApprovedOrder::NAME]['end']), $ord[ApprovedOrder::NAME]['id'], $currentPlan);
            $q = $this->Record->findAndSumOrder($ord[ApprovedOrder::NAME]['id']);
            $ord[ApprovedOrder::NAME]['quantity'] = $q;
            if($currentPlan[Plan::NAME]['box_quantity'] != 0) {
                $ord[ApprovedOrder::NAME]['box_quantity'] = floor($q / floatval($currentPlan[Plan::NAME]['box_quantity']));
            }
            $ord[ApprovedOrder::NAME]['confirmed_quantity'] = 0;
            $ord[ApprovedOrder::NAME]['confirmed'] = 0;
            $shift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], $dateTime);
            foreach ($fLossTypes as $idx => $li) {
                $this->Loss->newItem($ord[ApprovedOrder::NAME]['id'], $li[LossType::NAME]['id'], $li[LossType::NAME]['value'], ($shift ? $shift[Shift::NAME]['id'] : null), 0, $this->Auth->user());
            }
            // Partial Quantity uzsakymo uzbaigimo metu
            $quantity = (int) $this->request->query('declaredQuantity');
            //if($quantity > 0){
            $item = array(
                'created' => date('Y-m-d H:i:s'),
                'sensor_id' => $fSensor['Sensor']['id'],
                'shift_id' => $shift['Shift']['id'],
                'approved_order_id' => $currentPlan[ApprovedOrder::NAME]['id'],
                'quantity' => $quantity,
                'user_id'=>$this->Auth->user()['id']
            );
            $parameters = array(&$this->request, &$item, &$fSensor, &$currentPlan);
            $pluginData = $this->Help->callPluginFunction('Workcenter_BeforeSavePartialQuantity_Hook', $parameters, Configure::read('companyTitle'));
            $this->PartialQuantity->save($item);
            //}
            $this->ApprovedOrder->create(false);
            $this->Log->write('Atnaujinamas ApprovedOrderis completeProd funkcijoje: '.json_encode($ord), $fSensor);
            $parameters = array(&$ord, $shift, &$fSensor);
            $pluginData = $this->Help->callPluginFunction('WorkCenter_BeforeCompleteProdSave_Hook', $parameters, Configure::read('companyTitle'));
            $this->QualityFactor->calculateOrderQualityOnProductionEnd($fSensor, $currentPlan);
            $this->ApprovedOrder->save($ord);
            //papildoma apsauga, kad po uzsakymo uzbaigimo tikrai nebeliktu aktyviu uzsakymu, nes kartais buna atsiranda orderiai be planu, kurie ir pasilieka aktyvus
            $this->ApprovedOrder->updateAll(array('status_id'=>ApprovedOrder::STATE_COMPLETE), array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS, 'ApprovedOrder.sensor_id'=>$fSensor['Sensor']['id']));
            $this->Sensor->save(array(
                'id'=>$fSensor['Sensor']['id'],
                'quantity_records_count'=>0,
                'new_production_start'=>'0000-00-00',
            ));
            $this->FoundProblem->updateAll(array(
                'plan_id'=>$ord[ApprovedOrder::NAME]['plan_id'],
                'approved_order_id'=>$ord[ApprovedOrder::NAME]['id'],
            ),array(
                'FoundProblem.approved_order_id IS NULL',
                'FoundProblem.sensor_id'=>$fSensor['Sensor']['id'],
                'FoundProblem.start >='=>$ord[ApprovedOrder::NAME]['created'],
            ));
            $this->setProductionEnd($fFoundProblem,$fSensor,$shift,$problemId);

            $sysEmail = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL);
            $sysEmailName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
            $shiftManagerEmail = $fSensor[Branch::NAME]['shift_manager_email'];
            $shiftManagerEmail = $this->Branch->parseBranchManagerEmails($shiftManagerEmail);

            if ($sysEmail && $shiftManagerEmail && !empty($shiftManagerEmail) && Configure::read('sendEmails') === null) {
                $mail = new CakeEmail(self::EMAIL_CONFIG);
                $mail->from('oee@pergale.lt');
                $mail->to($shiftManagerEmail);
                $mail->subject(sprintf(__('Dėžių kiekio patvirtinimas %s'), $dateTime->format('Y-m-d H:i')));
                $mail->emailFormat('both');
                $mail->template('confirm_box_quantity', null);
                $url = Router::url(array('controller' => ApprovedOrdersController::ID, 'action' => ($ord[ApprovedOrder::NAME]['id'].'/edit')), true);
                $mail->viewVars(array(
                    'item' => $this->ApprovedOrder->withRefs()->findById($ord[ApprovedOrder::NAME]['id']),
                    'lossItems' => $this->Loss->withRefs()->find('all', array(
                        'conditions' => array(Loss::NAME.'.approved_order_id' => $ord[ApprovedOrder::NAME]['id']),
                        'order' => Loss::NAME.'.loss_type_id ASC'
                    )),
                    'url' => $url
                ));
                try {
                    $mail->send();
                } catch(Exception $e) {
                    $this->Log->write("Email send failed: " .json_encode($e->getMessage()).";", $fSensor);
                }
            } else {
                $this->Log->write("Configuration does not allow mail sending", $fSensor);
            }

        }
        $this->set($val = array(
        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    private function setProductionEnd($fFoundProblem,$fSensor,$shift,$problemId) {
        if(!$problemId) {
            $problemId = 1;
        }
        $transitionProblemId = 0;
        $fProblemType = $this->Problem->findById($problemId);
        if($fProblemType[Problem::NAME]['transition_problem']){ //kai fiksuijame perejimo problema, i Found_problem visados siunciame ta pati perejimo statusa
            $problemId = Problem::ID_NEUTRAL;
            $transitionProblemId = $fProblemType[Problem::NAME]['id'];
        }
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        $doAdditionalCheck = true;
        if ($fFoundProblem && $fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_NEUTRAL && ($fFoundProblem[FoundProblem::NAME]['while_working'])) {
            $doAdditionalCheck = false;
            $this->Log->write("Found neutral problem. Changing to a different problem id.", $fSensor);
            $this->FoundProblem->create(false);
            $this->FoundProblem->save(array(FoundProblem::NAME => array(
                'id' => $fFoundProblem[FoundProblem::NAME]['id'],
                'problem_id' => ($problemId),
                'transition_problem_id'=> $transitionProblemId,
                'while_working' => 0
            )));
        } else {
            if($fFoundProblem) {
                $this->Log->write("Found non neutral problem. ".json_encode($fFoundProblem), $fSensor);
                if($fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_NOT_DEFINED) {
                    $doAdditionalCheck = false;
                    $this->Log->write("Undefined problem switched to whatever was chosen when ending.", $fSensor);
                    $this->Log->write("Undefined problem switched to no work. Problem id: ".$problemId, $fSensor);
                    $this->FoundProblem->create(false);
                    $this->FoundProblem->id = $fFoundProblem[FoundProblem::NAME]['id'];
                    $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
                    $problemData = array(FoundProblem::NAME => array(
                        'problem_id' => $problemId,
                        'transition_problem_id'=> $transitionProblemId,
                        'while_working' => 0
                    ));
                    if(isset($problemsTreeList[$problemId])){
                        $problemData[FoundProblem::NAME]['super_parent_problem_id'] = current(array_reverse(array_keys($problemsTreeList[$problemId])));
                    }
                    if(!$currentPlan){
                        $problemData[FoundProblem::NAME]['plan_id'] = null;
                        $problemData[FoundProblem::NAME]['approved_order_id'] = null;
                        $this->Record->updateAll(
                            array('approved_order_id'=>null, 'plan_id'=>null),
                            array('found_problem_id'=>$fFoundProblem[FoundProblem::NAME]['id'], 'sensor_id'=>$fSensor[Sensor::NAME]['id'])
                        );
                        $updateRecords = $this->Record->find('all', array('conditions'=>array('found_problem_id'=>$fFoundProblem[FoundProblem::NAME]['id'], 'sensor_id'=>$fSensor[Sensor::NAME]['id'])));
                        $this->Log->write("Po gamybos pabaigos susikure nepazymeta problema, kuri vartotojui parinkus tapo perejimo. Nuimame nuo problemos plana tiek problems tiek records lentelsese.".json_encode($updateRecords), $fSensor);
                    }
                    $this->FoundProblem->save($problemData);
//                    if($noWork) {
//                        $this->Log->write("Undefined problem switched to no work.");
//                        $this->FoundProblem->create(false);
//                        $this->FoundProblem->id = $fFoundProblem[FoundProblem::NAME]['id'];
//                        $this->FoundProblem->save(array(FoundProblem::NAME => array(
//                            'problem_id' => Problem::ID_NO_WORK,
//                            'while_working' => 0
//                        )));
//                    } else {
//                        $this->Log->write("Undefined problem switched to transition.");
//                        $this->FoundProblem->create(false);
//                        $this->FoundProblem->id = $fFoundProblem[FoundProblem::NAME]['id'];
//                        $this->FoundProblem->save(array(FoundProblem::NAME => array(
//                            'problem_id' => Problem::ID_NEUTRAL,
//                            'while_working' => 0
//                        )));
//                    }

                } else {
                    $this->Log->write("Problem is defined. Ending it.", $fSensor);
                    $this->FoundProblem->create(false);
                    $this->FoundProblem->id = $fFoundProblem[FoundProblem::NAME]['id'];
                    $this->FoundProblem->save(array(FoundProblem::NAME => array(
                        'end'   => date('Y-m-d H:i:s'),
                        'state' => 2,
                    )));

                }
            }
        }
        if($doAdditionalCheck && $problemId > 0) {
            $fLastProblem = array();
            if($fFoundProblem){
                $startProblem = DateTime::createFromFormat('Y-m-d H:i:s', $this->request->query('startEndProductionDate'));
                $fLastProblem = $this->FoundProblem->findLast($fSensor[Sensor::NAME]['id']);
                if($fLastProblem && $startProblem < new DateTime($fLastProblem[FoundProblem::NAME]['end'])){
                    $startProblem = new DateTime($fLastProblem[FoundProblem::NAME]['end']);
                }
                $startProblem = $startProblem->add(new DateInterval('PT1S'));
            }else{
                $startProblem = new DateTime();
                $startProblem->add(new DateInterval('PT2S'));
            }
            $this->FoundProblem->findAndComplete($fSensor);
            unset($this->FoundProblem->id);
            $this->FoundProblem->create();
            $problemData = array(FoundProblem::NAME => array(
                'sensor_id'     => $fSensor[Sensor::NAME]['id'],
                'shift_id'      => $shift[Shift::NAME]['id'],
                'problem_id'    => $problemId,
                'transition_problem_id'=> $transitionProblemId,
                'while_working' => 0,
                'start'         => $startProblem->format('Y-m-d H:i:s'),
                'end'           => $startProblem <= new DateTime()?date_format(new DateTime, "Y-m-d H:i:s"):$startProblem->format('Y-m-d H:i:s'),
                'state'         => 1,
                'user_id'       => $this->Auth->user()['id'],
            ));
            $this->FoundProblem->save($problemData);
            $this->Log->write('Creating problem xppoo: ' . json_encode($problemData).'. FoundProblem id: '.$this->FoundProblem->id.' Request duomenys: '.json_encode($this->request->query).' Esama problema: '.json_encode($fFoundProblem).' Paskutine problema: '.json_encode($fLastProblem), $fSensor);
//            if ($noWork) {
//                $this->Log->write("No work");
//
////                $dateStartNoWork = new DateTime();
////                $dateStartNoWork->sub(new DateInterval('PT40S'));
//                unset($this->FoundProblem->id);
//                $this->FoundProblem->create();
//                $this->FoundProblem->save(array(FoundProblem::NAME => array(
//                    'sensor_id'     => $fSensor[Sensor::NAME]['id'],
//                    'shift_id'      => $shift[Shift::NAME]['id'],
//                    'problem_id'    => Problem::ID_NO_WORK,
//                    'while_working' => 0,
//                    'start'         => $startProblem->format('Y-m-d H:i:s'),
//                    'end'           => date('Y-m-d H:i:s'),
//                    'state'         => 1,
//                )));
//            } else {
//                $this->Log->write("Switching to transition");
//                unset($this->FoundProblem->id);
//                $this->FoundProblem->create();
//                $this->FoundProblem->save(array(FoundProblem::NAME => array(
//                    'sensor_id'     => $fSensor[Sensor::NAME]['id'],
//                    'shift_id'      => $shift[Shift::NAME]['id'],
//                    'problem_id'    => Problem::ID_NEUTRAL,
//                    'while_working' => 0,
//                    'start'         => $startProblem->format('Y-m-d H:i:s'),
//                    'end'           => date('Y-m-d H:i:s'),
//                    'state'         => 1,
//                )));
//            }
        }
    }

    public function swapCurrentPlan() {
        $currentPlanId = $this->request->query['currentPlanId'];
        $sensorId = $this->request->query['sensorId'];
        $additionalData = $this->request->query('additionalData');
        $fSensor = $this->getSensor();
        if($currentPlanId == $newProductId) die();
        $oldPlan = $this->Plan->findById($currentPlanId);
        if(isset($this->request->query['newProductId'])){
            $newPlanId = $currentPlanId;
            $newProductId = $this->request->query['newProductId'];
            $newProduct = $this->Product->findById($newProductId);
            $newProductBoxQuantity = $newProduct['Product']['kiekis_formoje'] > 0?$newProduct['Product']['kiekis_formoje']:1;
            $this->Log->write('Keičiamas produktas. Buvusio plano ID:'.json_encode($currentPlanId) .'; Buvusio produkto ID: '.($oldPlan['Plan']['product_id']).'. Naujo produkto ID:'.json_encode($newProductId), $fSensor);
            $fPlan = array(
                'id'=>$currentPlanId,
                'box_quantity'=>$newProductBoxQuantity,
                'production_name'=>$newProduct['Product']['name'],
                'production_code'=>$newProduct['Product']['production_code'],
                'product_id'=>$newProduct['Product']['id'],
                'package_quantity'=>20,
                'step'=>$newProduct['Product']['kiekis_formoje'] * $newProduct['Product']['formu_greitis']
            );
            // Change plan box quantity
            $parameters = array(&$fPlan, &$newProduct, $fSensor);
            $pluginData = $this->Help->callPluginFunction('WorkCenter_BuildStep_Hook', $parameters, Configure::read('companyTitle'));
            $this->Plan->save($fPlan);
        }elseif(isset($this->request->query['newPlanId'])){
            $newPlanId = $this->request->query['newPlanId'];
            $newPlan = $this->Plan->findById($newPlanId);
            $this->Log->write('Keičiamas planas. Buvusio plano ID: '.json_encode($currentPlanId) ."; Naujo plano ID:".json_encode($newPlanId), $fSensor);
            $newProductBoxQuantity = (int)$newPlan['Plan']['box_quantity'] > 0?(int)$newPlan['Plan']['box_quantity']:1;
        }
        // Update record quantity with new box_quantity and plan_id
        $this->Record->updateAll(
            array(
                'Record.unit_quantity' => '(Record.quantity * '.$newProductBoxQuantity.')',
                'Record.plan_id' => $newPlanId
            ),array('Record.plan_id'=>$currentPlanId)
        );

        //Update ApprovedOrder
        $approvedOrder = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.sensor_id'=>$sensorId, 'ApprovedOrder.plan_id'=>$currentPlanId), 'order'=>array('ApprovedOrder.created DESC')));
        $this->ApprovedOrder->id = $approvedOrder['ApprovedOrder']['id'];
        $q = $this->Record->findAndSumOrder($this->ApprovedOrder->id);
        $timeStamp = new DateTime();
        $this->ApprovedOrder->save(array(
            'additional_data'=>$additionalData,
            'box_quantity'=>floor($q / $newProductBoxQuantity) ,
            'quantity'=>$q,
            'swap_time'=> $timeStamp->format('Y-m-d H:i:s'),
            'plan_id'=>$newPlanId
        ));
        $this->FoundProblem->updateAll(array('plan_id'=>$newPlanId), array('FoundProblem.plan_id'=>$currentPlanId, 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
        echo "DONE";
        die();
    }

    /** @requireAuth Darbo centras: registruoti gamybos prastovą */
    public function registerIssue() {
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        $problemComment = $this->request->query['problemComment'];
        $problemTypeId = $this->request->params['id'];
        $foundProblemId = $this->request->query('foundProblemId');
        $downtimeCutType = $this->request->query('downtimeCutType');
        $downtimeCutDuration = $this->request->query('downtimeCutDuration');
        $this->Log->write("+ Issue registering started. ProblemTypeId:".$problemTypeId.'; $foundProblemId: '.$foundProblemId, $fSensor);
        if((int)$problemTypeId > 0){
            if($this->FoundProblem->splitProblemByUserRequest($fSensor, $foundProblemId, $problemTypeId, $downtimeCutType, $downtimeCutDuration)){
                die();//prastova skaldoma, todel jokiu kitu veiksmu atlikti nebereikia
            }
        }
        $fProblemType = $this->Problem->find('first',array('conditions'=>array('Problem.id'=>$problemTypeId, 'Problem.id >'=>0)));
        $fFoundProblem = $foundProblemId ? $this->FoundProblem->findById($foundProblemId) : null;
        if($fFoundProblem){
            $dateTime = new DateTime($fFoundProblem[FoundProblem::NAME]['start']);
        }else{
            $dateTime = $this->request->query('dateTime');
            if ($dateTime) $dateTime = DateTime::createFromFormat('Y-m-d?H:i:sP', $dateTime);
			$dateTime = $dateTime > new DateTime()?new DateTime():$dateTime;
            $dateTimeSub = clone $dateTime;
			$dateTimeSub->sub(new DateInterval('PT'.Configure::read('recordsCycle').'S'));
            $fLastProblem = $this->FoundProblem->findLast($fSensor[Sensor::NAME]['id']);
            $this->Log->write("Pries tai buvusi prastova: ".json_encode($fLastProblem), $fSensor);
            if($fLastProblem && $dateTimeSub < new DateTime($fLastProblem[FoundProblem::NAME]['end'])){
                $dateTime = new DateTime($fLastProblem[FoundProblem::NAME]['end']);
                $dateTime->add(new DateInterval('PT1S'));
            }
        }
        $problemData = $this->request->query('transitionProblem');
        $fShift = $dateTime ? $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], $dateTime) : null;
        //self::debugPrint($dateTime, $foundProblemId); exit();
        $state = false;
        if (!empty($fProblemType) && $fShift && $dateTime) {
            $this->Log->write("Found problem Id:".$foundProblemId.' Gautas laikas: '.$dateTime->format('Y-m-d H:i:s'), $fSensor);
            /*if($fFoundProblem == null) {
                $conds = array(
                    'sensor_id' => $fSensor['Sensor']['id'],
                    'shift_id'  => $fShift['Shift']['id'],
                    'state'     => FoundProblem::STATE_IN_PROGRESS,
                    'problem_id'=> 3,
                );
                $tryFindProblem = $this->FoundProblem->find('first', array(
                    'conditions' => $conds
                ));
                $this->Log->write("Trying to find problem:".json_encode($tryFindProblem)."; Conditions:".json_encode($conds).";", $fSensor);
                if(!empty($tryFindProblem)) {
                    $fFoundProblem = $tryFindProblem;
                }
            }*/
            $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
            $dtStart = clone $dateTime;
            //$dtStart->sub(new DateInterval('PT59S'));
            if (isset($fFoundProblem[FoundProblem::NAME]['id']) && $fFoundProblem[FoundProblem::NAME]['id']) {
                $this->Log->write("Problem is set, updating ".json_encode($fFoundProblem), $fSensor);
                $arr = $fFoundProblem[FoundProblem::NAME];
                $dateTime = new DateTime($arr['end']);
				//ieskome ar jutiklis yra suporuotas su kitu jutikliu ir kitam jutikliui reikia uzdeti ta pacia prastova jei jis turi prastova tuo paciu laiku
				$sensorsDowntimesGroups = $this->Settings->getOne('combined_sensors_downtimes_marking');
				if(!empty($sensorsDowntimesGroups)){
					$this->FoundProblem->addDowntimeTypeToOtherSameGroupSensors($fSensor,$sensorsDowntimesGroups,$fProblemType,$fFoundProblem,$problemsTreeList);
				}
            } else {
                $activeOperator = $this->User->getActiveUser($fSensor['Sensor']['id']);
                $arr = array(
                    'start' => $dtStart->format('Y-m-d H:i:s'),
                    'end' => $dtStart->format('Y-m-d H:i:s'),
                    'state' => FoundProblem::STATE_IN_PROGRESS,
                    'sensor_id' => $fSensor[Sensor::NAME]['id'],
                    'shift_id' => $fShift[Shift::NAME]['id'],
                    'plan_id'   => $currentPlan?$currentPlan[Plan::NAME]['id']:null,
                    'approved_order_id' => $currentPlan?$currentPlan[ApprovedOrder::NAME]['id']:null,
                    'user_id' => $activeOperator['User']['id']??0
                );
                $this->Log->write("Problem NOT set, creating a new one. ".json_encode($arr), $fSensor);
                $dtStartClone = clone($dtStart);
                $dtStartClone->sub(new DateInterval('PT1S'));
                //$this->Record->findAndSetProblem($fSensor[Sensor::NAME]['id'], new DateTime($fLastProblem[FoundProblem::NAME]['start']), $dtStartClone, $fLastProblem[FoundProblem::NAME]['id']);
                $this->FoundProblem->findAndComplete($fSensor, $dtStartClone);
            }
            $arr['comments'] = $problemComment;
            $arr['transition_problem_id'] = 0;
            $arr['mail_inform_sended'] = '';
            $arr['problem_id'] = $fProblemType[Problem::NAME]['id'];
            $arr['exported_to_mpm'] = 0;
            if($fProblemType[Problem::NAME]['transition_problem']){ //kai fiksuijame perejimo problema, i Found_problem visados siunciame ta pati perejimo statusa
                $arr['problem_id'] = $arr['super_parent_problem_id'] = Problem::ID_NEUTRAL;
                $arr['transition_problem_id'] = $fProblemType[Problem::NAME]['id'];
            }
            if($arr['problem_id'] == 1){ $arr['while_working'] = 0; }
            if(!isset($arr['id'])){
                $problemsForDelete = $this->FoundProblem->find('all',array('conditions'=>array('sensor_id' => $fSensor[Sensor::NAME]['id'], 'start' => $dtStart->format('Y-m-d H:i:s'))));//jei serveris neatsako ir atsiunciamos kelios uzklausos, pasalinti senas uzklausas kad nebutu dubliaciju kai serveris atsigaus ir viska surasys
                if($problemsForDelete){
                    $this->Log->write('Salinamos problemos del sutapimu: '.json_encode($problemsForDelete), $fSensor);
                    $this->FoundProblem->deleteAll(array('sensor_id' => $fSensor[Sensor::NAME]['id'], 'start' => $dtStart->format('Y-m-d H:i:s')));//jei serveris neatsako ir atsiunciamos kelios uzklausos, pasalinti senas uzklausas kad nebutu dubliaciju kai serveris atsigaus ir viska surasys
                }
            }
            if(isset($problemsTreeList[$arr['problem_id']])){
                $arr['super_parent_problem_id'] = current(array_reverse(array_keys($problemsTreeList[$arr['problem_id']])));
            }
            $this->FoundProblem->create();
            $this->FoundProblem->save(array(FoundProblem::NAME => $arr));

            $this->FoundProblem->splitProblemIfExceeds($fFoundProblem, $arr, $fSensor, $problemsTreeList);
            $this->Log->write('Saaved problem data: '.json_encode($arr).'. Saved problem id: '.$this->FoundProblem->id, $fSensor);
            //$this->Record->findAndSetProblem($fSensor[Sensor::NAME]['id'], $dateTime, $dateTime, $this->FoundProblem->id);
            $state = true;
        }elseif($problemTypeId == 'send_only_comment' && $foundProblemId > 0){
            $this->FoundProblem->save(array(FoundProblem::NAME => array('comments'=>$problemComment, 'id'=>$foundProblemId, 'exported_to_mpm'=>0)));
            $this->Log->write('Prastovai pridetas komentaras: '.json_encode($this->FoundProblem->findById($foundProblemId)), $fSensor);
        }
        $this->Log->write('Siuo metu sistemoje aktyviu prastovu sarasas: '.json_encode(array_values($this->FoundProblem->find('list',array('conditions'=>array('FoundProblem.state'=>1,'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']))))), $fSensor);
        $this->set($val = array(
            'ok' => $state,
            'dateTime' => ($dateTime ? $dateTime->format('c') : null),
            'foundProblemId' => ($foundProblemId ? $foundProblemId : null)
        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    /** @requireAuth Darbo centras: registruoti gamybos greičio problemą */
    public function registerSpeedIssue() {
        $this->requestAuth(true);
        $fSensor = $this->getSensor();
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        $problemTypeId = $this->request->params['id'];
        $fProblemType = $this->Problem->findById($problemTypeId);
        $dateTime = $this->request->query['dateTime'];
        if ($dateTime) $dateTime = DateTime::createFromFormat('Y-m-d\TH:i:sP', $dateTime);
        $fShift = $dateTime ? $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], $dateTime) : null;
        if (!empty($fProblemType) && $fShift && $dateTime) {
            $this->Log->write("Registering speed issue. Problem Type: ".json_encode($fProblemType) . "; Shift ID: ".$fShift[Shift::NAME]['id'].";", $fSensor);
            $this->FoundProblem->create();
            $this->FoundProblem->save  (array(FoundProblem::NAME => array(
                'start' => $dateTime->format('Y-m-d H:i:s'),
                'end' => $dateTime->format('Y-m-d H:i:s'),
                'state' => FoundProblem::STATE_COMPLETE,
                'problem_id' => $fProblemType[Problem::NAME]['id'],
                'sensor_id' => $fSensor[Sensor::NAME]['id'],
                'shift_id' => $fShift[Shift::NAME]['id'],
                'while_working' => 0,
                'user_id'=>$this->Auth->user()['id'],
                'approved_order_id' => $currentPlan[ApprovedOrder::NAME]['id'],
                'plan_id' => $currentPlan[Plan::NAME]['id'],
            )));
        } else {
            $this->Log->write("something is missing to register speed issue!",$fSensor);
            $this->Log->write('Problem:' . json_encode($fProblemType) . ";",$fSensor);
            $this->Log->write('Shift:' . json_encode($fShift) . ";",$fSensor);
            $this->Log->write('DT:' . json_encode($dateTime) . ";",$fSensor);
        }
        $this->set($val = array(

        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function getPluginFunctionality() {
        $loadedPlugins = CakePlugin::loaded();
        $functionality = array(
            'sidebarLinks' => array(),
            'afterProductionProblems'=>array()
        );
        foreach($loadedPlugins as $plugin) {
            $plugin_model = $plugin.'AppModel';
            $this->loadModel($plugin.'.'.$plugin_model);
            if(method_exists($plugin_model,'getWCSidebarLinks')) {
                $functionality['sidebarLinks'][] = $plugin_model::getWCSidebarLinks();
            }
            if(method_exists($plugin_model,'getAllAfterProductionProblems')) {
                $functionality['afterProductionProblems'] = $plugin_model::getAllAfterProductionProblems();
            }
            if(method_exists($plugin_model,'dismissWorkCenterElements')) {
                $functionality['dismissWorkCenterElements'] = $plugin_model::dismissWorkCenterElements();
            }

        }
        return $functionality;
    }

    public function declareQuantities(){
        $fSensor = $this->getSensor();
        if($this->request->query('foundProblemId') > 0){
            $foundProblem = $this->FoundProblem->findById($this->request->query('foundProblemId'));
            if(!empty($foundProblem) && $foundProblem['FoundProblem']['approved_order_id'] > 0){
                $ord = $this->ApprovedOrder->findById($foundProblem['FoundProblem']['approved_order_id']);
                $shift = $this->Shift->findById($foundProblem['FoundProblem']['shift_id']);
            }
        }
        if(!isset($ord)){//PrtialQuantities saugome tik jei dekalruojami kiekiai esamam uzsakymui. Jei atsiunciamas foundProblemId, reiskia deklaruojama spaudziant ant zalio intervalo, kur yra tik nuostoliai
            $currentPlan = $this->Plan->getCurrentPlan($fSensor);
            $ord = $this->ApprovedOrder->findById($currentPlan[ApprovedOrder::NAME]['id']);
            $dateTime = new DateTime();
            $shift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], $dateTime);
            // Quantity
            $quantity = (int) $this->request->query('declaredQuantity');
            $item = array(
                'created' => date('Y-m-d H:i:s'),
                'sensor_id' => $fSensor['Sensor']['id'],
                'shift_id' => $shift['Shift']['id'],
                'approved_order_id' => $currentPlan[ApprovedOrder::NAME]['id'],
                'quantity' => $quantity,
                'user_id'=>$this->Auth->user()['id']
            );
            $parameters = array(&$this->request, &$item, &$fSensor, &$currentPlan);
            $pluginData = $this->Help->callPluginFunction('Workcenter_BeforeSavePartialQuantity_Hook', $parameters, Configure::read('companyTitle'));
            //if($item['quantity'] > 0) {//negalima tikrinti konkrecios quantity reiksmes, nes saugojimas gali buti daromas del kitu laukeliu
            $this->PartialQuantity->save($item);
            //}
        }

        // Losses
        $fLossTypes = $this->LossType->find('all', array('conditions'=>array('LossType.id >'=>0,'OR'=>array('LossType.line_ids'=>'', 'FIND_IN_SET('.$fSensor['Line']['id'].',LossType.line_ids)'))));
        foreach ($fLossTypes as $idx => $li) {
            $fLossTypes[$idx][LossType::NAME]['value'] = (($v = $this->request->query('lt_'.$li[LossType::NAME]['id'])) ? floatval($v) : 0);
        }
        foreach ($fLossTypes as $idx => $li) {
            $this->Loss->newItem($ord[ApprovedOrder::NAME]['id'], $li[LossType::NAME]['id'], $li[LossType::NAME]['value'], ($shift ? $shift[Shift::NAME]['id'] : null), 0, $this->Auth->user());
        }
        die();
    }

    /** @requireAuth Darbo centras: apdoroti jutiklių duomenis
     * wget -qO- http://domenas/work-center/update-status?update_all=1 */
    public function updateStatus(){
        $workingOnPlans = $workingOnProducts = false;
        $this->requestAuth(true);
		$fSensor = $this->getSensor();
        if($this->request->query('update_all')){
            $this->calcOee3($fSensor);
        }
        if(empty($fSensor) && $this->onlyUpdateStatus){ $this->render(false); return; }
        $fSensors = !isset($fSensor[0])?array($fSensor):$fSensor;
       // echo json_encode(array('lost_connection'=>1)); die();
        foreach($fSensors as $fSensor){
            $parameters = array(&$fSensor, $this->Auth->user());
            $pluginData = $this->Help->callPluginFunction('WorkCenter_BeforeUpdateStatus_Hook', $parameters, Configure::read('companyTitle'));
            if(!$fSensor['Sensor']['id']){ continue; }
            if(!$this->request->query('update_all') && (!isset($fSensor['Sensor']['is_virtual']) || !$fSensor['Sensor']['is_virtual'])){
                $lastRecord = $this->Record->find('first', array('recursive'=>-1,'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id']), 'order'=>array('Record.id DESC')));
                if(!empty($lastRecord) && time() - strtotime($lastRecord['Record']['created']) > Configure::read('recordsCycle')*15){
                    //echo json_encode(array('lost_connection'=>1)); die();
                }
            }
            if(intval($fSensor[Sensor::NAME]['working_on_plans_or_products'] == 1)) $workingOnProducts = true;
                else $workingOnPlans = true;
            $this->currentSensor = $fSensor;
			date_default_timezone_set($this->currentSensor['Sensor']['timezone']);
            $r1_plan_filters = array();
            if(isset($this->request->query['toggle_r1']) && $this->request->query['toggle_r1'] === true) {
                $fSensor['Sensor']['id'] = 2;
                $fSensor['Sensor']['ps_id'] = "R1";
                $fSensor['Sensor']['type'] = 2;
                $r1_plan_filters["Plan.production_code"] = 39000;
            }
            $currentPlan = $this->Plan->getCurrentPlan($fSensor);
            if(isset($this->request->query['only_oee'])) {
                $oee = $this->calcOee($fSensor,$currentPlan);
            }
            //Is Vilniaus duonos, todel sio nereikia kitiems projektams
            /*if (isset($currentPlan[ApprovedOrder::NAME]['created']) && strtotime($currentPlan[ApprovedOrder::NAME]['created']) < (time() - 3600*24)) {
                $this->ApprovedOrder->partialComplete($fSensor[Sensor::NAME]['id'], $currentPlan[ApprovedOrder::NAME]['plan_id']);
                $this->ApprovedOrder->create(false);
                $this->ApprovedOrder->save(array('id' => $currentPlan[ApprovedOrder::NAME]['id'], 'status_id' => ApprovedOrder::STATE_CANCELED));
                $currentPlan = $this->Plan->getCurrentPlan($fSensor);
            }*/
            if($currentPlan) {
                //$currentPlan['Plan']['start'] = date('c',strtotime($currentPlan['Plan']['start']));
                //$currentPlan['Plan']['end'] = date('c',strtotime($currentPlan['Plan']['end']));
                $currentPlan['Plan']['box_quantity'] = (float) $currentPlan['Plan']['box_quantity'];
                $currentPlan['Plan']['quantity'] = (float) $currentPlan['Plan']['quantity'];

                //$div = $sensor['speedometer_type'] == 0?60:1;
                if($fSensor['Sensor']['speedometer_type'] == 0){
                    $currentPlan['Plan']['step_unit_title'] = __('vnt/min');
                    $currentPlan['Plan']['step_unit_divide'] = 60; //greicio grafikui, ar dalinti stepa is 60 work-center greicio grafiko generavimo metu ar ne
                }else{
                    $currentPlan['Plan']['step_unit_title'] = __('vnt/h');
                    //$currentPlan['Plan']['step_unit_divide'] = 1;
                }
            }
            //if($this->request->query('update_all') && empty($currentPlan)){ continue; } //negali buti naudojamas, nes jei yra statusas nera darbo, plano nebus, bet dirbt su irasais reikia
            $maxDiff = 91;
            if(Configure::read('ADD_FULL_RECORD')){ //kai recordai tvarkome myslqe per procedura, sis metodas reikalingas nusakyti ar pradeti nauja gamyba ar ne, siusti mailus kai problemos trukmes virsijamos ir kt.
                if($this->request->query('update_all') && empty($currentPlan)){ continue; }
                $event = $this->detectFullRecordsEvents($fSensor, $currentPlan, $maxDiff);
            }else{//senasis irasu apdorojimo metodas per PHP
                $event = $this->detectEvents($fSensor, $currentPlan, $maxDiff);
            }
            if($this->onlyUpdateStatus || $this->request->query('update_all')) continue;
            $graphHoursCount = isset($this->request->query['graphHoursCount']) && $this->request->query['graphHoursCount'] > 0?$this->request->query['graphHoursCount']:0;
            $hours = $this->WorkCenter->buildTimeGraph($fSensor, $graphHoursCount);
            $planTypes = array(
                array(ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_IN_PROGRESS),
                array(ApprovedOrder::NAME.'.status_id' => null)
            );
            if (isset($fSensor[Sensor::NAME]['type']) && $fSensor[Sensor::NAME]['type'] == Sensor::TYPE_PACKING) {
                $planTypes[] = array(ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_POSTPHONED);
            }
            //$condForAppOrd = array('conditions'=>array('ApprovedOrder.id = (SELECT MAX(id) FROM '.$this->ApprovedOrder->tablePrefix.$this->ApprovedOrder->useTable.' WHERE plan_id = Plan.id )'));
            $show_plans_count_on_new_production = intval($fSensor[Sensor::NAME]['show_plans_count_on_new_production']);
            $show_plans_count_on_new_production = $show_plans_count_on_new_production > 0?$show_plans_count_on_new_production:4;
            $show_plans_count_on_table = intval($fSensor[Sensor::NAME]['show_plans_count_on_table']);
            $condForAppOrd = array('conditions'=>array('ApprovedOrder.status_id'=>array(ApprovedOrder::STATE_COMPLETE, ApprovedOrder::STATE_IN_PROGRESS))); //jungiame su baigtais ir siuo metu vykdomais (siuo metu reikia nes jei ta pati plana gali daryti keli jutikliai, tai kai tik vienas ju pradeda kiti nebeturi matyti) uzsakymais
            $planSearchParams = array(
                'fields'=>array('Plan.*','Sensor.*','ApprovedOrder.*','MIN(ApprovedOrder.status_id) as status'),
                'conditions' => array(
                    Plan::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Plan::NAME.'.position_frozen'=>1,
                    Plan::NAME.'.has_problem'=>0,
                    Plan::NAME.'.disabled'=>0,
                    ApprovedOrder::NAME.'.id' => null
                    //'OR' => $planTypes
                ),
                'order' => array('IFNULL('.ApprovedOrder::NAME.'.status_id, 999) ASC', Plan::NAME.'.position ASC', Plan::NAME.'.end ASC'),
                'group' => Plan::NAME.'.id',
                'limit' => max($show_plans_count_on_new_production, $show_plans_count_on_table)
            );
            $parameters = array(&$planSearchParams, &$fSensor);
            $pluginData = $this->Help->callPluginFunction('changePlanSearchInWorkCenterHook', $parameters, Configure::read('companyTitle'));
            $fPlans = $this->Plan->withRefs(true, $condForAppOrd)->find('all', $planSearchParams);
            if(empty($fPlans)){
                $planSearchParams = array(
                    'conditions' => array(
                        Plan::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                        Plan::NAME.'.has_problem'=>0,
                        Plan::NAME.'.disabled'=>0,
                        ApprovedOrder::NAME.'.id' => null
                        //'OR' => $planTypes
                    ),
                    'order' => array('IFNULL('.ApprovedOrder::NAME.'.status_id, 999) ASC', Plan::NAME.'.position ASC', Plan::NAME.'.end ASC'),
                    'group' => Plan::NAME.'.id',
                    'limit' => max($show_plans_count_on_new_production, $show_plans_count_on_table)
                );
                $parameters = array(&$planSearchParams, &$fSensor);
                $pluginData = $this->Help->callPluginFunction('changePlanSearchInWorkCenterHook', $parameters, Configure::read('companyTitle'));
                $fPlans = $this->Plan->withRefs(true, $condForAppOrd)->find('all', $planSearchParams);
            }
            $dziuv_plans = $this->Plan->withRefs()->find('first',array(
                'conditions' => array(
                    'Plan.production_code' => 39000,
                    'OR' => $planTypes),
                    'order' => array('IFNULL('.ApprovedOrder::NAME.'.status_id, 999) ASC', Plan::NAME.'.start ASC'),

            ));
    //        CakeLog::write('debug', 'Dziuv planai:' . json_encode($dziuv_plans));
            //$this->Log->write('Gauti einamieji planai: '.json_encode(fPlans).' ',$fSensor);
            //ieskome ar planai neturejo kitokiu statusu, pvz atidetas
            $getPossibleCurrentStatuses = $this->ApprovedOrder->find('list', array(
                'fields'=>array('ApprovedOrder.plan_id', 'ApprovedOrder.status_id'),
                'conditions'=>array('ApprovedOrder.plan_id'=>Set::extract('/Plan/id', $fPlans)),
                'order'=>array('ApprovedOrder.id DESC')
            ));
            $plans = $allPlans = array();
            foreach ($fPlans as $key => $li_) {
                $this->WorkCenter->attacheTechCardPath($li_, $fSensor);
                $li = $li_[Plan::NAME];
				if(!empty($currentPlan) && $li['id'] == $currentPlan[Plan::NAME]['id']) continue;
                $params = array(&$li_);
                $planName = $this->Help->callPluginFunction('WorkCenter_buildPlanName_Hook', $params, Configure::read('companyTitle'));
				$li_[Plan::NAME] = array_merge($li_[Plan::NAME], array(
                    'current' => (!empty($currentPlan) && $li['id'] == $currentPlan[Plan::NAME]['id']),
                    'status_id' => isset($getPossibleCurrentStatuses[$li['id']])?$getPossibleCurrentStatuses[$li['id']]:0,
                    'start' => date('Y-m-d H:i', strtotime($li['start'])),
                    'end' => date('Y-m-d H:i', strtotime($li['end'])),
                    'name' => $planName?$planName:Plan::buildName($li_, true),
                ));
                $allPlans[] = $li_;
            }
            $plans = array_slice($allPlans, 0, $show_plans_count_on_table);
            $allPlans = array_slice($allPlans, 0, $show_plans_count_on_new_production);
            /*if($workingOnPlans){
                $planTypes2 = array(ApprovedOrder::NAME.'.status_id' => null);
                if (isset($fSensor[Sensor::NAME]['type']) && $fSensor[Sensor::NAME]['type'] == Sensor::TYPE_PACKING) {
                    $planTypes2[] = array(ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_POSTPHONED);
                }
                //$show_plans_count_on_new_production = intval($this->Settings->getOne('show_plans_count_on_new_production'));
                $show_plans_count_on_new_production = intval($fSensor[Sensor::NAME]['show_plans_count_on_new_production']);
                $show_plans_count_on_new_production = $show_plans_count_on_new_production > 0?$show_plans_count_on_new_production:4;
                $planSearchParams = array(
                    'conditions' => array(
                        Plan::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                        Plan::NAME.'.has_problem'=>0,
                        Plan::NAME.'.position_frozen'=>1,
                        'OR' => $planTypes2,
                    ),
                    'order' => array(ApprovedOrder::NAME.'.status_id DESC', Plan::NAME.'.position ASC', Plan::NAME.'.end ASC'),
                    'group' => Plan::NAME.'.id',
                    'limit' => $show_plans_count_on_new_production
                );
                $parameters = array(&$planSearchParams);
                $pluginData = $this->Help->callPluginFunction('changePlanSearchInWorkCenterHook', $parameters, Configure::read('companyTitle'));
                $fAllPlans = $this->Plan->withRefs()->find('all',$planSearchParams);
                if(empty($fAllPlans)){
                    $fAllPlans = $this->Plan->withRefs()->find('all', array(
                        'conditions' => array(
                            Plan::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                            Plan::NAME.'.has_problem'=>0,
                            'OR' => $planTypes2,
                        ),
                        'order' => array(ApprovedOrder::NAME.'.status_id DESC', Plan::NAME.'.position ASC', Plan::NAME.'.end ASC'),
                        'group' => Plan::NAME.'.id',
                        'limit' => $show_plans_count_on_new_production
                    ));

                }
                foreach ($fAllPlans as $li) {
                    $allPlans[] = array(
                        'id' => $li[Plan::NAME]['id'],
                        'lineId' => $li[Plan::NAME]['line_id'],
                        'plan_start' => date('c', strtotime($li[Plan::NAME]['start'])),
                        'plan_end' => date('c', strtotime($li[Plan::NAME]['end'])),
                        'quantity' => $li[Plan::NAME]['quantity'],
                        'mo_number' => $li[Plan::NAME]['mo_number'],
                        'paste_code' => $li[Plan::NAME]['paste_code'],
                        'production_code' => $li[Plan::NAME]['production_code'],
                        'production_name' => $li[Plan::NAME]['production_name'],
        //                'comment' => $li[Plan::NAME]['comment'],
                        'step' => $li[Plan::NAME]['step'],
                        'client_speed' => $li[Plan::NAME]['client_speed'],
                        'preparation_time' => $li[Plan::NAME]['preparation_time'],
                        'production_time' => $li[Plan::NAME]['production_time'],
                        'name' => Plan::buildName($li, true),
                    );
                }
            }*/
            $fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
            if(isset($fSensor['dcTime']) && $fSensor['dcTime'] < new DateTime($fShift['Shift']['start'])){
                $datePoint = clone $fSensor['dcTime'];
                $datePoint->add(new DateInterval('PT1S'));
                $selectedTimeShift = $this->Shift->find('first', array('conditions'=>array(
                    'Shift.branch_id'=>$fSensor['Sensor']['branch_id'],
                    'Shift.start <=' => $datePoint->format('Y-m-d H:i:s'),
                    'Shift.end >=' => $datePoint->format('Y-m-d H:i:s'),
                )));
                if($selectedTimeShift['Shift']['id'] == $fShift['Shift']['id']){ $selectedTimeShift['Shift']['end'] = date('Y-m-d H:i'); }
            }else{
                $selectedTimeShift = $fShift;
                $selectedTimeShift[Shift::NAME]['end'] = date('Y-m-d H:i');
            }
            $allProducts = array();
            if($workingOnProducts){
                $allProducts = $this->Product->find('all', array('conditions'=>array(
                    'OR'=>array(
                        'TRIM(Product.work_center)'=>'',
                        'FIND_IN_SET('.$fSensor['Sensor']['id'].', work_center)'
                    )
                )));
            }

            if (empty($plans) && $event && $event->type == TimelineEvent::TYPE_STARTED) {
                //$event->type = TimelineEvent::TYPE_NONE;
            }
            $fProblemTypes = $this->Problem->find('threaded', array(
                'conditions' => array(
                    Problem::NAME.'.sensor_type' => array(0, $fSensor[Sensor::NAME]['type']),
                    Problem::NAME.'.line_id'=>array(0,$fSensor[Line::NAME]['id']),
                    Problem::NAME.'.disabled'=>0,
                    Problem::NAME.'.transition_problem'=>0,
                    //Problem::NAME.'.for_speed'=>0,
                    Problem::NAME.'.id >='=>4,
                ),
                'order' => array(Problem::NAME.'.name ASC')
            ));
            $fTransitionProblemTypes = $this->Problem->find('threaded', array(
                'conditions' => array(
                    Problem::NAME.'.sensor_type' => array(0, $fSensor[Sensor::NAME]['type']),
                    Problem::NAME.'.line_id'=>array(0,$fSensor[Line::NAME]['id']),
                    Problem::NAME.'.disabled'=>0,
                    Problem::NAME.'.transition_problem'=>1,
                ),
                'order' => array(Problem::NAME.'.name ASC')
            ));
            $fzeroTimeTransitionProblemTypes = array();
            if($fSensor['Line']['id'] > 0) {
                $fLossTypes = $this->LossType->find('all', array('conditions' => array(
                    'LossType.id >' => 0,
                    array('OR' => array('LossType.line_ids' => '', 'FIND_IN_SET(' . $fSensor['Line']['id'] . ',LossType.line_ids)')),
                    array('OR' => array('LossType.sensor_types' => '', 'FIND_IN_SET(' . $fSensor['Sensor']['type'] . ',LossType.sensor_types)'))
                )));
            }else{ $fLossTypes = array();}
            $fFoundProblem = $this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);

            $now = new DateTime();
            $now->setTimezone(new DateTimeZone($fSensor['Sensor']['timezone']));
            if ($this->recTimeOffset) $now->sub($this->recTimeOffset);
            $quantityOutput = array();
            if(isset($currentPlan[Plan::NAME]['id']) && $currentPlan[Plan::NAME]['id']){
                if(Configure::read('ADD_FULL_RECORD')){
                    $quantityOutput['quantity'] = $currentPlan[ApprovedOrder::NAME]['quantity'].' '.__('vnt.');
                }else{
                    $quantityOutput['quantity'] = $this->Record->findAndSumOrder($currentPlan[ApprovedOrder::NAME]['id']).' '.__('vnt.');
                }
                $approvedOrdersIdsSamePlan = $this->ApprovedOrder->find('list', array('fields'=>array('ApprovedOrder.id'), 'conditions'=>array('ApprovedOrder.plan_id'=>$currentPlan[Plan::NAME]['id'])));
            }else{
                $approvedOrdersIdsSamePlan = array();
            }
            if(isset($currentPlan[ApprovedOrder::NAME])){
                $currentPlan[ApprovedOrder::NAME]['additional_data'] = json_decode($currentPlan[ApprovedOrder::NAME]['additional_data']);
            }
            $waitingDiffCards = $this->DiffCard->find('first',array('conditions'=>array('state'=>0,'sensor_id'=>$fSensor['Sensor']['id'])));
            $waitingDiffCardId = null;
            if(!empty($waitingDiffCards)) {
                $waitingDiffCardId = $waitingDiffCards['DiffCard']['id'];
            }
            $verificationHookParameters = array($this->Record, $currentPlan, &$fFoundProblem, &$fSensor);
            $parameters = array(&$fSensor,&$currentPlan, &$fShift);
            if(!$lastMinuteSpeed = $this->Help->callPluginFunction('Record_getLastMinuteSpeed_Hook', $parameters, Configure::read('companyTitle'))){
                $lastMinuteSpeed = round($this->Record->getLastMinuteSpeed($fSensor,$currentPlan, $fShift),2);
            }
            if(isset($fSensor['User'])) {
                $availableSensors = $this->Sensor->find('all', array('fields' => array('Sensor.name', 'Sensor.id', 'Sensor.user_update_time'), 'recursive' => -1, 'conditions' => array('Sensor.id' => explode(',', $fSensor['User']['sensor_id']))));
                $availableSensors = array_filter($availableSensors, function ($sensor) use ($fSensor) {
                    return time() - strtotime($sensor['Sensor']['user_update_time']) < 30 || $sensor['Sensor']['id'] == $fSensor['Sensor']['id'] ? false : true;
                });
            }else{ $availableSensors = array(); }
            $dcHoursInfoBlocks = is_array(Configure::read('workcenter_hours_info_blocks'))?Configure::read('workcenter_hours_info_blocks'):array();
            array_walk($dcHoursInfoBlocks, function(&$data){
                $data['title'] = __("{$data['title']}");
            });
            $this->DashboardsCalculation->bindModel(array('belongsTo'=>array('Shift')));
            switch($fSensor['Sensor']['show_oee_or_other_in_wc']){
                case 1: $this->DashboardsCalculation->virtualFields = array('prev_shift_val'=>'CONCAT(\''.__('priein.').': \',ROUND(DashboardsCalculation.exploitation_factor,2)*100)'); break;
                default: $this->DashboardsCalculation->virtualFields = array('prev_shift_val'=>'CONCAT(\''.__('OEE').': \',ROUND(DashboardsCalculation.oee_with_exclusions,0))'); break;
            }
            $prevShiftsOEE = $this->DashboardsCalculation->find('list', array(
                'fields'=>array('DashboardsCalculation.shift_id','prev_shift_val'),
                'conditions'=>array(
                    'DashboardsCalculation.sensor_id'=>$fSensor['Sensor']['id'],
                    'DashboardsCalculation.shift_id <'=>$selectedTimeShift['Shift']['id'],
                    'Shift.disabled'=>0,
                ), 'order'=>array('DashboardsCalculation.shift_id DESC'),
                'limit'=>2, 'recursive'=>1
            ));
            //$prevShiftsOEE[0] .= '<div>xxx</div>';
            $this->DashboardsCalculation->virtualFields = array();
            $this->Record->calculateMainFactorsInHour($dcHoursInfoBlocks,$hours,$fSensor);
            $this->QualityFactor->addSuggestionToAvailableMos($fSensor);
            $this->WorkCenter->attacheTechCardPath($currentPlan, $fSensor);
            $fSensor['Sensor']['has_access_to_register_events'] = !isset($fSensor['dc']) || (isset($fSensor['dc']) && $this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'detectFullRecordsEvents')));
            $fSensor['Sensor']['show_shift_change_button'] = $this->WorkCenter->showShiftChangeButton($fSensor, $fShift);
            $parameters = array(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours);
            $pluginData = $this->Help->callPluginFunction('changeUpdateStatusVariablesHook', $parameters, Configure::read('companyTitle'));
            $val = array(
                'workCenter' => array(
                    'id' => $fSensor[Sensor::NAME]['id'],
                    'name' => Sensor::buildName($fSensor),
                    'type' => intval($fSensor[Sensor::NAME]['type'])
                ),
                'line' => array(
                    'id' => $fSensor[Line::NAME]['id'],
                    'name' => $fSensor[Line::NAME]['name'],
                    'industrial' => ($fSensor[Line::NAME]['industrial'] ? true : false)
                ),
                'event' => $event->jsonSerialize(),
                'hours' => $hours,
                'currentPlan' => ($currentPlan ? array(Plan::NAME => $currentPlan[Plan::NAME], Product::NAME => $currentPlan[Product::NAME], ApprovedOrder::NAME => array_merge((isset($currentPlan[ApprovedOrder::NAME])?$currentPlan[ApprovedOrder::NAME]:array()), $quantityOutput)) : null),
                'foundProblem' => ($fFoundProblem ? array(FoundProblem::NAME => $fFoundProblem[FoundProblem::NAME]) : null),
                'currentShift' => $fShift,
                'historyShift' => $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id'], isset($fSensor['dcTime'])?$fSensor['dcTime']: new DateTime),
                'plans' => $plans, //$plans
                'allPlans' => $allPlans, //$allPlans
                'problemTypes' => Settings::translateThreaded($fProblemTypes, array('Problem.name')),
                'transitionProblemTypes' => Settings::translateThreaded($fTransitionProblemTypes, array('Problem.name')),
                'zeroTimeTransitionProblemTypes' => $fzeroTimeTransitionProblemTypes,
                'lossTypes' => (isset($fLossTypes) ? Settings::translateThreaded($fLossTypes, array('LossType.name')) : null),
                'time' => $now->format('Y-m-d H:i:s'),
                'helpUrl' => (($fSensor[Branch::NAME]['ps_id'] == 260) ? 'http://172.30.6.48/SMS_B1' : 'http://172.30.6.48/SMS'),
                //'OEE' => $this->calcOee($fSensor, $currentPlan),
                'dziuv_plans' => ($dziuv_plans != null && !empty($dziuv_plans)) ? true : false,
                'sensorType' => $fSensor['Sensor']['type'],
                'sensor' => $fSensor['Sensor']+array('line_id'=>isset($fSensor['Line'])?$fSensor['Line']['id']:0),
                'products' => $allProducts,
                'orderPartialQuantity' => isset($currentPlan[ApprovedOrder::NAME])?$this->PartialQuantity->sumByApprovedOrderId($approvedOrdersIdsSamePlan):0,
                'orderLossesQuantities'=>isset($currentPlan[ApprovedOrder::NAME])?$this->Loss->getLossesQuantitiesByOrder($approvedOrdersIdsSamePlan):array(),
                'waitingDiffCardId' => $waitingDiffCardId,
                'settings' => $this->Settings->getAll(),
                'verification' => !$this->request->query('update_all') && self::$user->sensorId > 0?$this->Help->callPluginFunction('verificationHook', $verificationHookParameters, Configure::read('companyTitle')):false,
                'texts'=>array(
                    'confirmProblemChoose'=>__('Ar tikrai norite parinkti šį prastovos tipą?')
                ),
                'mainPluginTitle'=>Inflector::camelize(Configure::read('companyTitle')),
                'lastMinuteSpeed'=>strval($lastMinuteSpeed),
                'accessesToMethods'=>array(
                    'changeProblemToTransition'=>$this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'changeProblemToTransition'),false),
                    'changeProblemToNoWork'=>$this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'changeProblemToNoWork'),false),
                    'allowDowntimeSplit'=>$this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'allowDowntimeSplit'),false),
                ),
                'workcenterHoursInfoBlocks'=>$dcHoursInfoBlocks,
                'availableSensors'=>array_values($availableSensors),
                'prevShiftsOEE' => array_values($prevShiftsOEE),
                'selectedTimeShift'=>$selectedTimeShift,
                'newRecords' => $this->Record->getNewestSensorRecords( $fSensor["Sensor"] ),
                'hiddenButtons' => Hash::extract(ClassRegistry::init('HiddenButton')->find('all',["conditions" => ["user_id" => $this->Auth->user()["id"]]]), '{n}.HiddenButton.id')
            );
            list($val['OEE'], $val['oeeGaugeTitle']) = $this->Record->calcOee2($selectedTimeShift, $fSensor, $currentPlan, $fShift);
            $parameters = array(&$val, $fSensor);
            $pluginData = $this->Help->callPluginFunction('Workcenter_changeUpdateStatusVariables_Hook2', $parameters, Configure::read('companyTitle'));
            if(Configure::read('GZIP_ENABLED')){
            	ob_start('ob_gzhandler');
				echo json_encode($val);
				ob_end_flush();
				die();
            }else{
            	$this->set($val);
	            $this->set('_serialize', array_keys($val));
	            $this->RequestHandler->renderAs($this, 'json');
            }
        }
        if($this->onlyUpdateStatus){
            $this->render(false);
            return;
        }
        echo json_encode(array());
        die();
    }

    /** @requireAuth Darbo centras: uzbaigti esama pamaina rankiniu budu **/
    public function setShiftEnd(){
        $sensorId = $this->request->data['sensor_id'];
        $fSensor = $this->Sensor->findById($sensorId);
        $currentShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $manualShiftChangeSettings = array_map(function($val){return (float)trim($val);}, explode(',', $fSensor['Sensor']['manual_shift_change']));
        if(sizeof($manualShiftChangeSettings) == 2 && strtotime($currentShift['Shift']['start']) + 3600 < time()){
            $fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
            $this->Branch->id = $fSensor[Sensor::NAME]['branch_id'];
            $currentTime = new DateTime();
            $subTime = clone $currentTime;
            $addTime = clone $currentTime;
            $subTime->sub(new DateInterval(sprintf('PT%dM', $manualShiftChangeSettings[1]))); //atimame nustatymo laika tam, kad einant irasams prie to laiko prodejus ta pacia reiksme laikas butu sensesnis uz dabarti, todel kursis nauja pamaina
            $addTime->add(new DateInterval(sprintf('PT%dM', Configure::read('recordsCycle')))); //dabartine pamaina uzbaigsime 20s veliau, kad darbo centre nesigautu erroras del pamainos trukumo, kol ateis irasas, pratesiantis pamaina
            $this->Branch->save(array(
                'last_shift_end' => json_encode(array('shift_id' => $fShift['Shift']['id'], 'end' => $subTime->format('Y-m-d H:i:s'))),
                'shift_change_init'=>1
            ));
            $this->Shift->id = $fShift['Shift']['id'];
            $this->Shift->save(array('end'=>$currentTime->format('Y-m-d H:i:s')));
            $this->Log->write('Rankiniu budu inicijuotas pamainos uzbaigimas. Uzbaigta pamaina: '.json_encode($fShift), $fSensor);
            $this->Shift->generateShifts($this->Branch->id);
        }
        die();
    }


    /** @requireAuth Darbo centras: Ar gali parinkti ir užbaigti gamybą "Registruoti įvykį"? */
    public function detectFullRecordsEvents($fSensor, $currentPlan, $maxDiff){
        $event = $this->createEvent(TimelineEvent::TYPE_NONE);
        //if(isset($fSensor['dc'])){ return $event;}
        $hasAccess = $this->hasAccessToAction(array('controller'=>'WorkCenter', 'action'=>'detectFullRecordsEvents'));
        if(isset($fSensor['dc']) && !$hasAccess){ return $event; }
        //$fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $fFoundProblem = $this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);
        $parameters = array(&$fSensor,&$fFoundProblem,&$currentPlan);
        $pluginData = $this->Help->callPluginFunction('WorkCenter_beforeDetectFullRecordsEvents_Hook', $parameters, Configure::read('companyTitle'));
        if(!$currentPlan){
            if((int)$fSensor['Sensor']['new_production_start']){
                $event = $this->createEvent(TimelineEvent::TYPE_STARTED, new DateTime($fSensor['Sensor']['new_production_start']));
            }elseif($fSensor['Sensor']['choose_plan_after_transition']){//jei paspaustas derinimas ir nustatyme jutikliui nurodyta pradeti gamyba tuo paciu metu kaip ir derinimas, mesti gamyba
                $fFoundProblem = isset($fFoundProblem)?$fFoundProblem:$this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);
                if(!empty($fFoundProblem) && $fFoundProblem['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL){
                    $event = $this->createEvent(TimelineEvent::TYPE_STARTED, new DateTime($fFoundProblem['FoundProblem']['start']));
                }
            }
        }
        //$this->FoundProblem->manipulate();
		$this->getSlowSpeedEvent($event, $fFoundProblem, $currentPlan, $fSensor);
        return $event;
    }

    /**
     * @param array $fSensor
     * @param array $currentPlan
     * @param int $maxDiff
     * @return TimelineEvent
     */
    private function detectEvents($fSensor, $currentPlan, $maxDiff,$ordEndDateTime = null,$problemId = 0) { // $ordEndDateTime - nurodomas uzsakymui baigiantis
        set_time_limit(300);
        $maxProblemDiff = 3 * 60; // 3 min.
        $now = new DateTime();
        if ($this->recTimeOffset) $now->sub($this->recTimeOffset);
        $event = $this->createEvent(TimelineEvent::TYPE_NONE);
        if (isset($fSensor['dc'])){ return $event;}
        $fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $prevShift = $this->Shift->getPrev($fShift[Shift::NAME]['start'], $fSensor[Sensor::NAME]['branch_id']);
//fb('harcode pamaina'); $fShift = $this->Shift->findById(4947);
//        $this->Log->write('Shift ID: ' . $fShift['Shift']['id']);
        $waitingOrder = $this->ApprovedOrder->withRefs()->findWaitingForData($fSensor[Sensor::NAME]['id']);
        $transitionProblemId = 0;
        if($problemId > 0){
            $fProblemType = $this->Problem->findById($problemId);
            if(!empty($fProblemType) && $fProblemType[Problem::NAME]['transition_problem']){ //kai fiksuijame perejimo problema, i Found_problem visados siunciame ta pati perejimo statusa
                $problemId = Problem::ID_NEUTRAL;
                $transitionProblemId = $fProblemType[Problem::NAME]['id'];
            }
        }
        if ($waitingOrder && !$currentPlan) {
            if (strtotime($waitingOrder[ApprovedOrder::NAME]['end']) < (time() - 3600*24)) {
                $this->ApprovedOrder->partialComplete($fSensor[Sensor::NAME]['id'], $waitingOrder[ApprovedOrder::NAME]['plan_id']);
                $this->ApprovedOrder->create(false);
                // maybe cancel instead of complete
                $this->Log->write('Uzdaromas uzsakymas del laukiamo uzsakymo',$fSensor);
                $this->ApprovedOrder->save(array('id' => $waitingOrder[ApprovedOrder::NAME]['id'], 'status_id' => ApprovedOrder::STATE_COMPLETE));
            } else if (strtotime($waitingOrder[ApprovedOrder::NAME]['end']) <= time()) {
                $event = $this->createEvent(TimelineEvent::TYPE_STARTED, new DateTime($waitingOrder[ApprovedOrder::NAME]['end']));
            }
            return $event;
        }
        $fFoundProblem = $this->FoundProblem->findCurrent($fSensor[Sensor::NAME]['id']);
//        $this->Log->write('Continuing problem.. ' . json_encode($fFoundProblem) . ";");
        if ($fFoundProblem && $fFoundProblem[FoundProblem::NAME]['shift_id'] && $fFoundProblem[FoundProblem::NAME]['shift_id'] != $fShift[Shift::NAME]['id'] && !empty($prevShift) && $prevShift[Shift::NAME]['id'] == $fFoundProblem[FoundProblem::NAME]['shift_id']) {
            $this->Log->write("Problem exists in different shift",$fSensor);
            $prevShift = $this->Shift->findById($fFoundProblem[FoundProblem::NAME]['shift_id']);
            $this->Log->write("Prev shift: " . json_encode($prevShift).";",$fSensor);
            if ($prevShift) {
                $this->Log->write("1 Esama problema is praejusios pamainos kuria perrasysime: " . json_encode($fFoundProblem)."; vartotojas: ".self::$user->id.' Atsiunciama problema: '.$problemId, $fSensor);
                $dateToEndProblem = $prevShift[Shift::NAME]['end'];
                if($ordEndDateTime != null) {
                    $dateToEndProblem = $ordEndDateTime->format('Y-m-d H:i:s');
                }
                $this->FoundProblem->create(false);
                if($fFoundProblem[FoundProblem::NAME]['problem_id'] != 1) {
                    $this->FoundProblem->save(array(
                        'id' => $fFoundProblem[FoundProblem::NAME]['id'],
                        'end' => $dateToEndProblem,
                        'state' => FoundProblem::STATE_COMPLETE,
                        'problem_id'=> $fFoundProblem[FoundProblem::NAME]['problem_id'],
                        'sensor_id' => $fSensor[Sensor::NAME]['id'],
                        //'transition_problem_id'=> $transitionProblemId,
                        //'shift_id' => $fShift[Shift::NAME]['id']
                    ));
                    $this->Log->write("fsdfdksfu Problemos perrasymas",$fSensor);
                    ////              Might be needed
                    if($ordEndDateTime != null && strtotime($ordEndDateTime->format('Y-m-d H:i:s')) < strtotime($prevShift[Shift::NAME]['end'])) {
                        $this->FoundProblem->create();
                        $this->FoundProblem->save(array(
                            'start' => $ordEndDateTime->format('Y-m-d H:i:s'),
                            'end' => $prevShift[Shift::NAME]['end'],
                            'state' => FoundProblem::STATE_COMPLETE,
                            'problem_id' => $problemId,
                            'transition_problem_id'=> $transitionProblemId,
                            'sensor_id' => $fSensor[Sensor::NAME]['id'],
                            //'shift_id' => $fShift[Shift::NAME]['id']
                        ));
                        $this->Log->write('Creating problem wqerttt: ' . $problemId.'. FoundProblem id: '.$this->FoundProblem->id, $fSensor);
                    }
                } elseif($problemId > 0) {
                    $this->Log->write("Problem rewrite. New one: ".$problemId, $fSensor);
                    $this->FoundProblem->save(array(
                        'id' => $fFoundProblem[FoundProblem::NAME]['id'],
                        'end' => $dateToEndProblem,
                        'state' => FoundProblem::STATE_COMPLETE,
                        'problem_id'=> $problemId,
                        'sensor_id' => $fSensor[Sensor::NAME]['id'],
                        //'transition_problem_id'=> $transitionProblemId,
                        //'shift_id' => $fShift[Shift::NAME]['id']
                    ));
                }

                $fFoundProblem[FoundProblem::NAME]['prev_shift_problem_id'] = $fFoundProblem[FoundProblem::NAME]['id'];
                $this->FoundProblem->create();
                unset($fFoundProblem[FoundProblem::NAME]['id']);
                if(isset($fFoundProblem[FoundProblem::NAME]['created'])) unset($fFoundProblem[FoundProblem::NAME]['created']);
                if(isset($fFoundProblem[FoundProblem::NAME]['modified'])) unset($fFoundProblem[FoundProblem::NAME]['modified']);
                $fFoundProblem[FoundProblem::NAME]['start'] = $fShift[Shift::NAME]['start'];
                if(strtotime($fFoundProblem[FoundProblem::NAME]['start']) > strtotime($fFoundProblem[FoundProblem::NAME]['end'])){
                    $fFoundProblem[FoundProblem::NAME]['end'] = $fFoundProblem[FoundProblem::NAME]['start'];
                }
                $fFoundProblem[FoundProblem::NAME]['shift_id'] = $fShift[Shift::NAME]['id'];
                $fFoundProblem[FoundProblem::NAME]['diff_card_id'] = null;
                if ($this->FoundProblem->save($fFoundProblem[FoundProblem::NAME])) {
                    $this->Log->write('Created new problem for new shift: ' . json_encode($fFoundProblem)."; Problemos id: ".$this->FoundProblem->id.' Vartotojas: '.self::$user->id, $fSensor);
                    $fFoundProblem[FoundProblem::NAME]['id'] = $this->FoundProblem->id;
                } else {
                    $this->Log->write('Could not create a new problem for new shift: ' . json_encode($fFoundProblem). ";", $fSensor);
                    $fFoundProblem = null;
                }
            }
        }
        $start = $currentPlan ? $currentPlan[ApprovedOrder::NAME]['created'] : $fShift[Shift::NAME]['start'];
        $start = new DateTime($start) > new DateTime($fShift[Shift::NAME]['start'])?$start : $fShift[Shift::NAME]['start'];
        $lastOrder = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.sensor_id'=>$fSensor['Sensor']['id'], 'ApprovedOrder.status_id <>'=>ApprovedOrder::STATE_IN_PROGRESS), 'order'=>array('ApprovedOrder.created DESC')));
        if($lastOrder && new DateTime($lastOrder['ApprovedOrder']['end']) > new DateTime($start)){ $start = $lastOrder['ApprovedOrder']['end']; }
        $recs = $this->Record->find('all', array(
                'conditions' => array(
                    Record::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Record::NAME.'.approved_order_id' => null,
                    Record::NAME.'.found_problem_id' => null,
                    Record::NAME.'.created >=' => $start,
                    Record::NAME.'.created <=' => $now->format('Y-m-d H:i:s')
                ),
                'order' => array(Record::NAME.'.created ASC'),
                'limit' => 2000
            ));
        /*if(false && !empty($recs) && $fFoundProblem && $recs[0]['Record']['created'] < $fFoundProblem['FoundProblem']['end']) {
            $fFoundProblem['FoundProblem']['start'] = $recs[0]['Record']['created'];
            $this->FoundProblem->save($fFoundProblem);
            $this->Log->write("ay");

            $recs = $this->Record->find('all', array(
                'conditions' => array(
                    Record::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Record::NAME.'.approved_order_id' => null,
                    Record::NAME.'.found_problem_id' => null,
                    Record::NAME.'.created >=' => $start,
                    Record::NAME.'.created <=' => $now->format('Y-m-d H:i:s')
                ),
                'order' => array(Record::NAME.'.created ASC'),
                'limit' => 500
            ));
        }*/
        $recent_recs = $this->Record->find('all',array(
            'conditions' => array(
                Record::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                Record::NAME.'.approved_order_id !=' => null,
                Record::NAME.'.shift_id' => $fShift[Shift::NAME]['id']
            ),
            'order' => array('Record.created'=>'DESC'),
            'limit' => 4
        ));

        $conds = array(
            Record::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
            Record::NAME.'.created >=' => $start
        );
        if (($firstRec = reset($recs))) {
            $conds[Record::NAME.'.created <'] = $firstRec[Record::NAME]['created'];
        }
        $lastRec = $this->Record->find('first', array(
            'conditions' => $conds,
            'order' => array(Record::NAME.'.created DESC'),
            'limit' => 1
        ));
        if (!$lastRec) $lastRec = array(Record::NAME => array('created' => $start, 'quantity' => 0, 'unit_quantity' => 0));
        $records = array();
        foreach ($recs as $rec) {
            $lastRecTime = strtotime($lastRec[Record::NAME]['created']);
            if ((strtotime($rec[Record::NAME]['created']) - $lastRecTime) > $maxDiff) {
                $records[] = array(Record::NAME => array('created' => date('Y-m-d H:i:s', $lastRecTime + 20), 'quantity' => 0, 'unit_quantity' => 0));
            }
            $records[] = $rec;
            $lastRec = $rec;
        }
        unset($recs);


        $lastRecTime = strtotime($lastRec[Record::NAME]['created']);
        $dis = new DateInterval('PT1S');
        $di19s = new DateInterval('PT19S');
        $diRecordsCycle = new DateInterval('PT'.Configure::read('recordsCycle').'S');

        // Get already marked zeroes from previous records
        $zeroes_in_a_row = 0;
        $zeroes_in_a_row_ids = array();
        $problem_start = null;
        foreach($recent_recs as $recent_record) {

            // Problem already exists no need to count recent records to get count in a row
            if($recent_record['Record']['found_problem_id']) {
                break;
            }

            // Increment zeroes in a row
            if($recent_record['Record']['quantity'] == 0) {
//                $recent_zero_record_ids[] = $recent_record['Record']['id'];
                $problem_start = $recent_record['Record']['created'];
                $zeroes_in_a_row_ids[] = $recent_record['Record']['id'];
                $zeroes_in_a_row++;
            }

            // If non-zeroes start, we finish this because we everything should have been taken care of previously
            if($recent_record['Record']['quantity'] != 0) {
                break;
            }
        }
//        if($zeroes_in_a_row > 0) {
//            $this->Log->write("We got zeroes in a row from before!: " . $zeroes_in_a_row);
//            $this->Log->write("Records: " . json_encode($records).";");
//        }

        $firstZeroRecordTime = null;
        $zeroRecordsIds = array();
        // Start looping through new records
        $siblingRowsHavingQuantity = array('rowsCount'=>0,'rowsZeros'=>0,'ids'=>array());
        //$quantoRecToStartOrder = intval($this->Settings->getOne('quantity_records_to_start_order'));
        $quantoRecToStartOrder = intval($fSensor[Sensor::NAME]['quantity_records_to_start_order']);
        $quantoRecToStartOrder = $quantoRecToStartOrder <= 0?1:$quantoRecToStartOrder;
        $product = $this->Product->findById($currentPlan['Plan']['product_id']);
        $mustReachZerosCountToStartProblem = !empty($product) && $product['Product']['production_time'] > 0?round($product['Product']['production_time']*2/Configure::read('recordsCycle')):3;
        $fFoundProblemCopy = $fFoundProblem?$fFoundProblem:array();
        $previousRecord = array();
        //uzklausimas kas gaminama turi sokineti tol, kol vyksta derinimas ir planas dar neparinktas
        if(!$currentPlan && $fSensor['Sensor']['choose_plan_after_transition'] > 0 && $fFoundProblem && $fFoundProblem[FoundProblem::NAME]['problem_id'] == FoundProblem::STATE_IN_PROGRESS){
            $event = $this->createEvent(TimelineEvent::TYPE_STARTED, new DateTime($fFoundProblem['FoundProblem']['start']));
        }
        $prodStartsOn = floatval($fSensor[Sensor::NAME]['prod_starts_on']) > 0?floatval($fSensor[Sensor::NAME]['prod_starts_on']):0.1;
        $problemStartsOn = $prodStartsOn - 1 > 0 ? $prodStartsOn - 1 : 0;
        $latestRecordHavingGoodQuantity = current(array_filter(array_reverse($records), function($record)use($prodStartsOn){
            return $record['Record']['quantity'] >= $prodStartsOn;
        }));
        foreach($records as $key=>$record) {
            if(array_key_exists('id',$record['Record']) && in_array($record['Record']['id'],$zeroes_in_a_row_ids)) {
                $zeroes_in_a_row--;
            }
            // Record comes with > 0 quantity
            if($record['Record']['quantity'] >= $prodStartsOn){
                if($siblingRowsHavingQuantity['rowsCount'] == 0){ $siblingRowsHavingQuantity['date_start'] = $record[Record::NAME]['created']; }
                //irasus su kiekiu galima skaiciuoti tik kai jie eina vienas po kito, be tarpu
                if($quantoRecToStartOrder == 1 || empty($previousRecord) || strtotime($record['Record']['created']) - strtotime($previousRecord['Record']['created']) <= Configure::read('recordsCycle')){
                   $siblingRowsHavingQuantity['rowsCount']++;
                }
                $siblingRowsHavingQuantity['ids'][] = $record['Record']['id'];
            }
            $parameters = array(&$record, &$siblingRowsHavingQuantity, $currentPlan, &$mustReachZerosCountToStartProblem, &$event, $previousRecord, &$fFoundProblem, &$prodStartsOn, &$problemStartsOn, &$fSensor);
            $pluginData = $this->Help->callPluginFunction('detectEventsHook', $parameters, Configure::read('companyTitle'));
            $previousRecord = $record;
            if($record['Record']['quantity'] >= $prodStartsOn) {
                $firstZeroRecordTime = null;
                $zeroes_in_a_row = 0;
                $zeroes_in_a_row_ids = array();
                if($currentPlan && $fFoundProblem[FoundProblem::NAME]['problem_id'] == FoundProblem::STATE_IN_PROGRESS){
                    //jei yra planas ir vyksta derinimas, vadinasi uzsakymas buvo parinktas pradedant derinima. Tokiu atveju derinima galima uzdaryti tik kai pareina reikiamas irasu kiekius su kiekiais
                     if(time() - strtotime($fFoundProblem[FoundProblem::NAME]['end']) >= $quantoRecToStartOrder * Configure::read('recordsCycle')){
                        $this->Log->write('Uzdaroma esama problema kai yra planas, nes prasideda gamyba: '.json_encode($fFoundProblem),$fSensor);
                        $this->FoundProblem->findAndComplete($fSensor);
                     }
                }elseif($fFoundProblem && new DateTime($record['Record']['created']) > new DateTime($fFoundProblem[FoundProblem::NAME]['end']) && ($currentPlan || $event->type == 'started') ) {
                    $date = new DateTime($record['Record']['created']);
                    $this->Log->write('Uzdaroma esama problema antra vieta, nes prasideda gamyba: '.json_encode($fFoundProblem).' Irasas: '.json_encode($record),$fSensor);
                    $this->extendProblem($fSensor,$date->sub($diRecordsCycle),$fFoundProblem,$fShift,true,$record);
                    $fFoundProblem = null;
                }
                if($currentPlan && new DateTime($record['Record']['created']) > new DateTime($currentPlan[ApprovedOrder::NAME]['end'])) { // Already exists a plan, we extend it.
                    $this->extendProd($record, $currentPlan, $fSensor, $waitingOrder);
                    if (!$this->request->query('update_all') && $waitingOrder && $waitingOrder[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_WAITING_FOR_DATA) {
                        $waitingOrder = null;
                        return $event;
                    }
                //}elseif($siblingRowsHavingQuantity['rowsCount'] >= $quantoRecToStartOrder) { // No plan exists, create event to start it.
                }elseif(!empty($latestRecordHavingGoodQuantity) && strtotime($latestRecordHavingGoodQuantity['Record']['created']) - strtotime($record['Record']['created']) >= ($quantoRecToStartOrder-1) * Configure::read('recordsCycle')) { // No plan exists, create event to start it. vienetu maziname del to,kad pirmas irasas su kiekiu yra 0 atskaitos taskas, antras bus po 20s, trecias po 40s ir t.t. todel jei reikia 3 irasu, gaunasi gamyba prasides kai 40 >= 20*(3-1)
                    $smaller_date = isset($siblingRowsHavingQuantity['date_start'])?$siblingRowsHavingQuantity['date_start']:$record[Record::NAME]['created'];
                    $smaller_date = isset($siblingRowsHavingQuantity['date_start_plugin'])?$siblingRowsHavingQuantity['date_start_plugin']:$smaller_date; //jei laikas perrasomas ir paduodamas is plugino, imame ji
                    //$this->Log->write('Vartotojui paduodamas naujo uzsakymo parinkimo langas del iraso: '.json_encode($record), $fSensor);
                    if($event->type == 'none'){
//fb($record, $smaller_date);
                        $event = $this->createEvent(TimelineEvent::TYPE_STARTED, new DateTime($smaller_date));
                        //Kai gamyba iskvieciama, reikia sutvarkytiu visus recordus iki gamybos pradzios, kurie turejo quantity, bet nepradejo gamybos. Jiems reikia uzdeti salia ju esanciu problemu id
                        $this->Record->addProblemOnQuantityRecords($record, $fShift);
                    }
                    $siblingRowsHavingQuantity['ids'] = array();
                    if(!$currentPlan){
                        $laterCreatedProblem = $this->FoundProblem->find('first', array('conditions'=>array('FoundProblem.start >='=>$record['Record']['created'], 'FoundProblem.sensor_id'=>$fSensor[Sensor::NAME]['id'])));
                        if(empty($laterCreatedProblem)){
                            $fFoundProblem = null;
                        }
                    }
                    //break;
                }
                continue;
            }
            // Record comes with = 0 quantity
            if($record['Record']['quantity'] <= $problemStartsOn) {
                $firstZeroRecordTime = $firstZeroRecordTime == null && isset($record['Record']['id'])?$record['Record']['created']:$firstZeroRecordTime;
                $siblingRowsHavingQuantity['rowsCount'] = 0;
                $siblingRowsHavingQuantity['rowsZeros']++;
                $zeroes_in_a_row++;
                //$zeroes_in_a_row_ids[] = $record['Record']['id'];
                if($fFoundProblem) { // Problem exists, we extend the problem
                    //if($zeroes_in_a_row > $mustReachZerosCountToStartProblem){
                    if($currentPlan && $fFoundProblem['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL){
                        $fProblemJsonData = json_decode($fFoundProblem['FoundProblem']['json']);
                        if($this->Settings->getOne('exceeded_transition_id')){ //tikrinsime ar perejimas nera pasiekes leidziamos ribos. Jei yra, kursime nauja derinimo uzdelsimo problema
                            $this->FoundProblem->createExceedingTransitionByPlan($fSensor, $currentPlan, $fFoundProblem, $fShift);
                        }elseif(!isset($fProblemJsonData->transition_exceeded)){//jei perejimas dar nera virsijes ir nuo jo nesusikure derinimo virsyjimo problema
                            $problem = $this->Problem->findById($fFoundProblem['FoundProblem']['transition_problem_id']);
                            $this->FoundProblem->createExceedingTransitionByTransition($fSensor, $currentPlan, $fFoundProblem, $fShift, $problem);
                        }
                    }
                    if(new DateTime($record['Record']['created']) > new DateTime($fFoundProblem['FoundProblem']['end'])){
                        $status = $this->extendProblem($fSensor, new DateTime($record['Record']['created']), $fFoundProblem, $fShift, false, $record);
                    }
                    //}
                    if($currentPlan) { // Plan exists, we extend the plan
                        $this->extendProd($record, $currentPlan, $fSensor, $waitingOrder);
                        if (!$this->request->query('update_all') && $waitingOrder && $waitingOrder[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_WAITING_FOR_DATA) {
                            $waitingOrder = null;
                            return $event;
                        }
                    }
                } else {
                    if(!$currentPlan && !empty($siblingRowsHavingQuantity['ids'])){
                        //kai gamyba nevyksta ir tarp irasu atsiunciamas irasas su kiekiu > 0, kuris nepradeda gamybos, jisai realiai yra tik neteisingas signalas (trugdis), todel priskiriame ji paskutinei turetai problemai arba order_id =0 (jei nera problemso reikia kito statuso, kad sekanti karta sis irasas nebutu vel naudojamas i records masyva)
                        // $this->Record->updateAll(
                            // array('Record.approved_order_id'=>0, 'Record.found_problem_id'=>(!empty($fFoundProblemCopy)?$fFoundProblemCopy[FoundProblem::NAME]['id']:null)),
                            // array('Record.id'=>$siblingRowsHavingQuantity['ids'])
                        // );
                        $siblingRowsHavingQuantity['ids'] = array();
                    }

                    if($zeroes_in_a_row > $mustReachZerosCountToStartProblem) {
                        $start = new DateTime($firstZeroRecordTime);
                        //if($problem_start != null) {
                        //    $start = new DateTime($problem_start);
                        //}
                        $date = $start;
                        $this->FoundProblem->findAndComplete($fSensor);
                        $fLastProblem = $this->FoundProblem->findLast($fSensor[Sensor::NAME]['id']);
                        if(empty($fLastProblem) || $date > new DateTime($fLastProblem[FoundProblem::NAME]['end'])){
                            $fFoundProblem = $this->FoundProblem->newItem(
                            $start->sub($diRecordsCycle),
                            $date,
                            Problem::ID_NOT_DEFINED, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $currentPlan[Plan::NAME]['id']);
                            $this->Log->write('Kuriama nauja problema kai nera plano'.json_encode($fFoundProblem).' pask problema'.json_encode($fLastProblem).' '.$firstZeroRecordTime,$fSensor);
                        }
                        // fb(array(
                            // $start->sub($di19s),
                            // $date,
                            // Problem::ID_NEUTRAL, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, true, $currentPlan[Plan::NAME]['id']),'xxxxxxxxx');
                    }
                }
//               else { // Plan does not exist, weird, should never come here.
//                    $date = new DateTime($record['Record']['created']);
//                    $fFoundProblem = $this->FoundProblem->newItem($date->sub($di19s), $date, Problem::ID_NEUTRAL, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id']);
//                }
            }
        }
		$this->getSlowSpeedEvent($event, $fFoundProblem, $currentPlan, $fSensor);
        return $event;
    }

	private function getSlowSpeedEvent(&$event, $fFoundProblem, $currentPlan, $fSensor){
		// if ($event->type == TimelineEvent::TYPE_NONE &&
            // $fFoundProblem &&
            // ($fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_NEUTRAL || $fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_NO_WORK) &&
            // $currentPlan)
        // {
            /*if ((strtotime($fFoundProblem[FoundProblem::NAME]['end']) - strtotime($fFoundProblem[FoundProblem::NAME]['start'])) > $maxProblemDiff) {
                $event = $this->createEvent(TimelineEvent::TYPE_STOPPED, new DateTime($fFoundProblem[FoundProblem::NAME]['start']), $fFoundProblem[FoundProblem::NAME]['id']);
            }*/
        //} else
        if ($event->type == TimelineEvent::TYPE_NONE && !$fFoundProblem && $currentPlan) { // if no events detected, no current problems and production is in progress
            $interval = intval($fSensor[Sensor::NAME][Settings::S_SPEED_DETECT_INTERVAL]) * 60;
            $threshold = floatval($fSensor[Sensor::NAME][Settings::S_OK_SPEED_THRESHOLD]);
            if ($interval > 0 && $threshold > 0) { // if settings are properly set
                $start = new DateTime($currentPlan[ApprovedOrder::NAME]['created']);
                $end = new DateTime($currentPlan[ApprovedOrder::NAME]['end']);
                $sDiff = $end->getTimestamp() - $start->getTimestamp();
                $newIntProg = $sDiff % $interval;
                if ($newIntProg > 1) $end->sub(new DateInterval('PT'.($newIntProg - 1).'S'));
                if ($end->getTimestamp() >= ($start->getTimestamp() + $interval)) { // if previous interval exists (because current interval is still in progress)
                    $start = clone $end;
                    $start->sub(new DateInterval('PT'.$interval.'S'));
                    $fp = $this->FoundProblem->withRefs()->find('first', array(
                        'conditions' => array(
                            FoundProblem::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                            FoundProblem::NAME.'.start >=' => $start->format('Y-m-d H:i:s'),
                            FoundProblem::NAME.'.end <=' => $end->format('Y-m-d H:i:s'),
                            Problem::NAME.'.for_speed' => 1
                        ),
                        'limit' => 1
                    ));
                    if (!$fp || empty($fp)) { // if previous interval is not registered for slow speed
                        $total = $this->Record->findAndSumInterval($fSensor[Sensor::NAME]['id'], $start, $end);
                        $idealTotal = floor($currentPlan[Plan::NAME]['step'] / 3600 * $interval);
                        if ($idealTotal != 0 && ($total * 100 / $idealTotal) < $threshold) { // if slow speed
                            $event = $this->createEvent(TimelineEvent::TYPE_SPEED, $end, null, $total.' of '.$idealTotal.' = '.($total * 100 / $idealTotal).'%');
                        }
                    }
                }
            }
        }
	}


    //apskaiciuojamas praejusios pamainos OEE rodiklis ir irasomas i DB
    public function calcOee3($fSensor=array()){
        $branches = $this->Branch->find('list', array('fields'=>array('id')));
        foreach($branches as $branchId){
            $fShift = $this->Shift->findCurrent($branchId);
            $prevShift = $this->Shift->getPrev($fShift[Shift::NAME]['start'], $branchId);
            $sensors = $this->Sensor->find('list', array('conditions'=>array('Sensor.branch_id'=>$branchId, 'Sensor.pin_name <>'=>'')));
            if($prevShift && $sensors){
                $oeeCalculated = $this->DashboardsCalculation->find('first', array('conditions'=>array('DashboardsCalculation.shift_id'=>$prevShift[Shift::NAME]['id'])));
                if(!$oeeCalculated){ //atnaujiname tik crono metu, kadangi ijungti sensoriai per daznai kreipiasi abu vienu metu ir gali pridubluoti oee skaiciavima
                    $this->Log->write('Apskaiciuojamas OEE praejusiai pamainai. Pamainos ID, kuri nebuvo apskaiciuota '.$prevShift[Shift::NAME]['id'],$fSensor);
                    //$markerpath = ROOT."/app/tmp/logs/dashboard_calculations_started.txt";
                    //if(!file_exists($markerpath) || time() - filemtime($markerpath) > 300 ){
                        //$myfile = fopen($markerpath, "w") or die("Unable to open file!");
                        set_time_limit(2000);
                        App::import('Console/Command', 'AppShell');
                        App::import('Console/Command', 'OeeShell');
                        $shell = new OeeShell();
                        $out = $shell->main();
                        break;
                        //fclose($myfile);
                        //unlink($markerpath);
                    //}
                }
            }
        }
        if(empty($fSensor)){
            $this->render(false);
            return;
        }
    }

    private function calcOee($fSensor, $currentPlan = null) {
        //return 0;
        $oee = 0;
        $problem_duration = 0;

        // Šiuo metu niekas nevykdoma
        if($currentPlan == null) {
            return 0;
        }

        // Tarpas tarp praeito užsakymo ir dabartinio
        $last_approved_order = $this->ApprovedOrder->find('first', array(
            'conditions' => array(
                'end <='    => $currentPlan['ApprovedOrder']['created'],
                'sensor_id' => $currentPlan['ApprovedOrder']['sensor_id']),
            'order'      => 'id DESC'
        ));
        if(!$last_approved_order) {
            return 0;
        }
        $fp = $this->FoundProblem->find('all',array(
            'conditions'=>array(
                'start >= ' => $last_approved_order['ApprovedOrder']['created'],
                'end <=' => $currentPlan['ApprovedOrder']['created'],
                'sensor_id' => $currentPlan['ApprovedOrder']['sensor_id'],
                'problem_id !=' => Problem::ID_NO_WORK
            ),
            'order' => 'id ASC'
        ));

        if(!empty($fp)) {
            foreach($fp as &$fp_single) {
                if($fp_single['FoundProblem']['end'] < $last_approved_order['ApprovedOrder']['end']) {
                    continue;
                }
                if($fp_single['FoundProblem']['start'] < $last_approved_order['ApprovedOrder']['end']) {
                    $fp_single['FoundProblem']['start'] = $last_approved_order['ApprovedOrder']['end'];
                }
                if(strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']) < 0) {
                    continue;
                }
                $problem_duration += strtotime($fp_single['FoundProblem']['end']) - strtotime($fp_single['FoundProblem']['start']);
            }
        }
        // Problemų trukmė užsakymo metu
        $fps = $this->FoundProblem->find('all',array(
            'fields'=>array('SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) as total'),
            'conditions'=>array(
               'plan_id'=>$currentPlan['Plan']['id'],
               'sensor_id'=>$currentPlan['ApprovedOrder']['sensor_id']
            )
        ));

        // Pilnas faktinis laikas
        $fact_start = $last_approved_order['ApprovedOrder']['end'];
//        if($fp != null && $fp['FoundProblem']['problem_id'] == 1) {
//            $fact_start = $fp['FoundProblem']['start'];
//            $problem_duration = strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start']);
//        }
        $fact_end = $currentPlan['ApprovedOrder']['end'];
        $fact_duration = strtotime($fact_end) - strtotime($fact_start);

        // Problemų laikas
        if($fps[0][0]['total'] != null) {
            $problem_duration += intval($fps[0][0]['total']);
        }

        $current_quantity = $currentPlan['ApprovedOrder']['quantity'];

        $plan_step = $currentPlan['Plan']['step'];
        if($fact_duration == 0) {
            return 0;
        }

        $avail = ($fact_duration - $problem_duration) / $fact_duration;
        $speed = 0;

        if(($fact_duration - $problem_duration) != 0 && $plan_step != 0) {
            $speed = ($current_quantity / ($fact_duration - $problem_duration)) / ($plan_step / 3600);
        }
        $oee = $avail * $speed;

        if(isset($this->request->query['only_oee'])) {
            echo('current quantity : '.$current_quantity.'<br>');
            echo('fact duration : '.$fact_duration.'<br>');
            echo('problem duration : '.$problem_duration.'<br>');
            echo('step/h : '.$plan_step.'<br>');
            echo('avail : '.$avail.'<br>');
            echo('speed : '.$speed.'<br>');
            echo('oee : '.$oee.'<br>');
        }

        /*if($currentPlan['Plan']['sensor_id'] == 16) {
            $data = "Current plan: " . json_encode($currentPlan) .";";
            $data .= "Current quantity: " . $current_quantity ."\r\n";
            $data .= "Perejimas DATA : " . json_encode($fp) .";";
            //$data .= "Perejimas: " . (strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start'])) ."\r\n";
            $data .= "Fact start: " . $fact_start ."\r\n";
            $data .= "Fact start (plan started): " . $currentPlan['ApprovedOrder']['created'] ."\r\n";
            $data .= "Fact end: " . $fact_end ."\r\n";
            $data .= "Fact duration: " . $fact_duration ."\r\n";
            $data .=  "Problem duration: " . $problem_duration ."\r\n";
            $data .=  "Plan step (per hour): " . $plan_step ."\r\n";
            $data .=  "Avail: " . $avail ."\r\n";
            $data .=  "Speed: " . $speed ."\r\n";
            $data .= "OEE: " . $oee;
            $this->Log->write("Data: " . $data);
//            print_r($data);
        } */
        //die();

        return min(intval($oee*100), 100);

        // Old below

        $this->Record->setSource('records');
        /*
            DC Prieinamumo (išnaudojimo) koeficientas = DC faktinė gamybos trukmė / suplanuota DC gamybos trukmė;
            DC Veiklos efektyvumo koeficientas = Idealus ciklo laikas / (DC faktinė gamybos trukmė / DC pagamintas produkcijos kiekis);
            [Jeigu raikymo jutikliu yra ir jie skaiciuoja tada]
                DC Kokybės koeficientas = DC Kokybiškos produkcijos kiekis / DC1 tos linijos raikymo jutiklio suskaiciuotas kikis;
            [kitu atveju]
                DC Kokybės koeficientas = 1;
            DC OEE= DC Prieinamumo (išnaudojimo) koeficientas * DC Veiklos efektyvumo koeficientas * DC Kokybės koeficientas;
         */
        $fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $approvedOrders = $this->ApprovedOrder->withRefs()->find('all', array(
            'conditions' => array(
                ApprovedOrder::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                ApprovedOrder::NAME.'.created >=' => $fShift[Shift::NAME]['start']
            )
        ));

//        var_dump($planDuration);
//        die();
        $duration = 0; $quantity = 0; $sliceQuantity = 0; $planedDuration = 0; $effTotal = 0; $effCount = 0; //
        foreach ($approvedOrders as $ao) {
            $quantity += $ao[ApprovedOrder::NAME]['quantity'];
            $durrSec = strtotime($ao[ApprovedOrder::NAME]['end']) - strtotime($ao[ApprovedOrder::NAME]['created']);
            $duration += $durrSec;
            if ($currentPlan && $currentPlan[ApprovedOrder::NAME]['id'] == $ao[ApprovedOrder::NAME]['id']) {
                $planedDuration += min((strtotime($ao[Plan::NAME]['end']) - strtotime($ao[Plan::NAME]['start'])), $durrSec);
            } else {
                $planedDuration += (strtotime($ao[Plan::NAME]['end']) - strtotime($ao[Plan::NAME]['start']));
            }
            $effTotal += ($ao[ApprovedOrder::NAME]['quantity'] != 0 && $durrSec != 0) ? ((3600 / $ao[Plan::NAME]['step']) / ($durrSec / $ao[ApprovedOrder::NAME]['quantity'])) : 0;
            $effCount++;
        }


        $quantity = 0;
        if (isset($fSensor[Line::NAME]['id']) && $fSensor[Line::NAME]['id']) {
            $fLine = $this->Line->withRefs()->findById($fSensor[Line::NAME]['id']);
            foreach ($fLine[Sensor::NAME] as $sensor) {
                if ($sensor['type'] == Sensor::TYPE_SLICING) {
                    $total = $this->Record->find('first', array(
                        'fields' => array(('SUM('.Record::NAME.'.quantity) AS total')),
                        'conditions' => array(
                            Record::NAME.'.sensor_id' => $sensor['id'],
                            Record::NAME.'.shift_id' => $fShift[Shift::NAME]['id']
                        )
                    ));
//                    var_dump($total);
                    if (isset($total[0]['total'])) $sliceQuantity = floatval($total[0]['total']);
                    //break;
                }
                if($sensor['type'] == Sensor::TYPE_PACKING) {
                    $total = $this->Record->find('first', array(
                        'fields' => array(('SUM('.Record::NAME.'.unit_quantity) AS total')),
                        'conditions' => array(
                            Record::NAME.'.sensor_id' => $sensor['id'],
                            Record::NAME.'.shift_id' => $fShift[Shift::NAME]['id']
                        )
                    ));
//                    var_dump($total);
                    if (isset($total[0]['total'])) $quantity += floatval($total[0]['total']);
                }
            }
        }
        $availKoef = $planedDuration ? ($duration / $planedDuration) : 0;
        $effKoef = $effCount ? ($effTotal / $effCount) : 0;
        $effKoef2 = $sliceQuantity ? ($quantity / $sliceQuantity) : 1;

        //$this->Log->write($quantity);
        //$this->Log->write($sliceQuantity);

        return min(round(($availKoef * $effKoef * $effKoef2) * 10000) / 100, 100);
        //return rand(5,95);
    }

    private function createDiffCard($fFoundProblem) {
        $fSensor = $this->getSensor();
        $fApprovedOrder = $this->ApprovedOrder->findCurrent($fSensor[Sensor::NAME]['id']);
        if (!$fApprovedOrder) return;
        $this->DiffCard->create();
        $this->DiffCard->save(array(DiffCard::NAME => array(
            'date' => date('Y-m-d H:i:s'),
            'sensor_id' => $fSensor[Sensor::NAME]['id'],
            'branch_id' => $fSensor[Sensor::NAME]['branch_id'],
            'line_id' => $fSensor[Line::NAME]['id'],
            'start' => $fFoundProblem[FoundProblem::NAME]['start'],
            'end' => null,
            'plan_id' => ($fApprovedOrder ? $fApprovedOrder[ApprovedOrder::NAME]['plan_id'] : null),
            'problem_id' => $fFoundProblem[FoundProblem::NAME]['problem_id'],
            'found_problem_id' => $fFoundProblem[FoundProblem::NAME]['id'],
            'consequences' => 0,
            'shift_manager_user_id' => null,
            'description' => null,
            'diff_type_id' => null,
            'diff_sub_type_id' => null,
            'responsive_section_user_id' => null,
            'clarification' => null,
            'measures_taken' => null,
            'measures_for_future' => null,
            'deadline' => null,
            'quality_section_user_id' => $fSensor[Branch::NAME]['quality_user_id'],
            'measure_evaluation' => null,
            'production_manager_user_id' => $fSensor[Branch::NAME]['production_user_id'],
            'production_manager_comment' => null,
            'shift_id' => $fFoundProblem[FoundProblem::NAME]['shift_id']
        )));
        if ($this->DiffCard->id) {
            $this->FoundProblem->create(false);
            $this->FoundProblem->save(array(FoundProblem::NAME => array('id' => $fFoundProblem[FoundProblem::NAME]['id'], 'diff_card_id' => $this->DiffCard->id)));
            $this->Log->write("Atnaujinama problema pagal diff card ", $fSensor);
        }

        $item = $this->DiffCard->withRefs()->findById($this->DiffCard->id);
        $dateTime = new DateTime($item[DiffCard::NAME]['date']);

        $sysEmail = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL);
        $sysEmailName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
        $shiftManagerEmail = $fSensor[Branch::NAME]['shift_manager_email'];
        $shiftManagerEmail = $this->Branch->parseBranchManagerEmails($shiftManagerEmail);
        if ($sysEmail && !empty($shiftManagerEmail) && Configure::read('sendEmails') === null) {
            $mail = new CakeEmail(self::EMAIL_CONFIG);
            $mail->from('oee@pergale.lt');
            $mail->to($shiftManagerEmail);
            $mail->subject(sprintf(__('Neatitikties kortelė %s'), $dateTime->format('Y-m-d H:i')));
            $mail->emailFormat('both');
            $mail->template('diff_card', null);
            $url = Router::url(array('controller' => DiffCardsController::ID, 'action' => ($item[DiffCard::NAME]['id'].'/edit')), true);
            $mail->viewVars(array(
                'item' => $item,
                'states' => DiffCard::getStates(),
                'url' => $url
            ));
            $mail->send();
        } else {
        }
    }

    private function extendProblem($fSensor, DateTime $date, &$fFoundProblem, $fShift, $complete = false, $record) {
        $fFoundProblem[FoundProblem::NAME]['end'] = $date->format('Y-m-d H:i:s');
        //$this->Record->updateAll(array('Record.found_problem_id'=>$fFoundProblem['FoundProblem']['id']), array('Record.created >='=>$fFoundProblem['FoundProblem']['start'], 'Record.created <'=>$fFoundProblem['FoundProblem']['id']));
        if($date <= new DateTime($fFoundProblem[FoundProblem::NAME]['start'])){
            //kai baigiasi gamyba vartotojas gali paspausti perejima ir po to greitai spausti kita problema. Tada susikuria tarpinis recordas kuris nebeigauna problemos statuso
            if(array_key_exists('approved_order_id', $record['Record']) && !isset($record['Record']['approved_order_id']) && !isset($record['Record']['found_problem_id'])){
                $problemFitt = $this->FoundProblem->find('first', array('conditions'=>array('FoundProblem.start >='=>$date->format('Y-m-d H:i:s'), 'FoundProblem.start <> FoundProblem.end'), 'order'=>array('FoundProblem.start')));
                if($problemFitt){
                    $this->Log->write('Perrasomas tarpnis irasas tarp gamybos pabaigos perejimo ir greito kitos problemsoi parinktimo: '.json_encode($fFoundProblem).'. Irasas: '.json_encode($record), $fSensor);
                    $this->Record->updateAll(array('Record.found_problem_id'=>$problemFitt['FoundProblem']['id']), array('Record.id'=>$record['Record']['id']));
                    return false;
                }
            }
        }
        if($this->FoundProblem->id > 0 || $fFoundProblem[FoundProblem::NAME]['id'] > 0 ){
        }else{ $this->FoundProblem->create(); }

        $data = array(
            'id' => isset($fFoundProblem[FoundProblem::NAME]['id'])?$fFoundProblem[FoundProblem::NAME]['id']:null,
            'start' => $fFoundProblem[FoundProblem::NAME]['start'],
            'end' => $fFoundProblem[FoundProblem::NAME]['end'],
            'problem_id' => $fFoundProblem[FoundProblem::NAME]['problem_id'],
            'sensor_id' => $fFoundProblem[FoundProblem::NAME]['sensor_id'],
            'shift_id' => $fFoundProblem[FoundProblem::NAME]['shift_id'],
            'plan_id' => $fFoundProblem[FoundProblem::NAME]['plan_id'],
        );
        $this->FoundProblem->save($data);
        if ($complete) {
            $this->FoundProblem->findAndComplete($fSensor);
        }
        $start = new DateTime($fFoundProblem[FoundProblem::NAME]['start']);
        $start = new DateTime($fShift['Shift']['start']) > $start?new DateTime($fShift['Shift']['start']):$start;
        //$this->Log->write("Prastova ekstendinama: ".json_encode($fFoundProblem).'; Pradzia: '.$start->format('Y-m-d H:i:s').', pabaiga: '.$date->format('Y-m-d H:i:s'), $fSensor);
        $this->Record->findAndSetProblem($fFoundProblem[FoundProblem::NAME]['sensor_id'], $start, $date, $fFoundProblem[FoundProblem::NAME]['id']);
        /* @var $diff DateInterval */
        $diff = $start->diff($date);
        $diffMins = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i + ($diff->s / 60);
        $createDiffCardAfterMinutes = (isset($fSensor['Line']['diff_card_auto_time']) && $fSensor['Line']['diff_card_auto_time'] == 0) ? $fSensor['Line']['diff_card_auto_time']: 5;
        if (intval($fSensor[Sensor::NAME]['auto_generate_diff_cards'] == 1) && !$fFoundProblem[FoundProblem::NAME]['diff_card_id'] && $fFoundProblem[FoundProblem::NAME]['problem_id'] != Problem::ID_NEUTRAL && $fFoundProblem[FoundProblem::NAME]['problem_id'] != Problem::ID_NO_WORK && ($diffMins >= $createDiffCardAfterMinutes)) {
            $fDiffCard = $this->DiffCard->find('first', array(
                'conditions' => array(
                    DiffCard::NAME.'.found_problem_id' => $fFoundProblem[FoundProblem::NAME]['id']
                ),
                'limit' => 1
            ));
            if (!$fDiffCard || empty($fDiffCard)) {
                $this->createDiffCard($fFoundProblem);
            }
        }
        if(trim($fSensor[Sensor::NAME]['inform_about_problems'])){
            $this->FoundProblem->sendMailsOnProblemOutgo($fFoundProblem, $fSensor, $diffMins);
            //$this->Log->write("Prastova po laisku siuntimo: ".json_encode($this->FoundProblem->findById($fFoundProblem[FoundProblem::NAME]['id'])), $fSensor);
        }
        if(trim($fSensor[Sensor::NAME]['new_fproblem_when_current_exceeds'])){
            $this->FoundProblem->createNewFProblemWhenCurrentExceeds($fFoundProblem, $fSensor, $diffMins);
        }
    }

    /**
     *
     * @param array $lastRec
     * @param array $currentPlan
     * @param array $fSensor
     * @param array $waitingOrder
     */
    private function extendProd($lastRec, &$currentPlan, $fSensor, &$waitingOrder) {
        $order = null;
        if ($waitingOrder && $waitingOrder[ApprovedOrder::NAME]['end'] >= $lastRec[Record::NAME]['created']) {
            $order = &$waitingOrder;
        } else if ($waitingOrder && $waitingOrder[ApprovedOrder::NAME]['id'] > 0) {
            $waitingOrder[ApprovedOrder::NAME]['status_id'] = ApprovedOrder::STATE_COMPLETE;
            $waitingOrder[ApprovedOrder::NAME]['end'] = date('Y-m-d H:i:s', strtotime($lastRec[Record::NAME]['created']) - 1);
            $this->Record->findAndSetOrder($fSensor, new DateTime($waitingOrder[ApprovedOrder::NAME]['created']), new DateTime($waitingOrder[ApprovedOrder::NAME]['end']), $waitingOrder[ApprovedOrder::NAME]['id'], $waitingOrder);
            $q = $this->Record->findAndSumOrder($waitingOrder[ApprovedOrder::NAME]['id']);
            $waitingOrder[ApprovedOrder::NAME]['quantity'] = $q;
            $waitingOrder[ApprovedOrder::NAME]['box_quantity'] = floor($q / floatval($currentPlan[Plan::NAME]['box_quantity']));
            $waitingOrder[ApprovedOrder::NAME]['confirmed_quantity'] = 0;
            $waitingOrder[ApprovedOrder::NAME]['confirmed'] = 0;
            $this->ApprovedOrder->create(false);
            $save = array(
                'id' => $waitingOrder[ApprovedOrder::NAME]['id'],
                'status_id' => $waitingOrder[ApprovedOrder::NAME]['status_id'],
                'end' => $waitingOrder[ApprovedOrder::NAME]['end'],
                'quantity' => $waitingOrder[ApprovedOrder::NAME]['quantity'],
                'box_quantity' => $waitingOrder[ApprovedOrder::NAME]['box_quantity'],
                'confirmed_quantity' => $waitingOrder[ApprovedOrder::NAME]['confirmed_quantity'],
                'confirmed' => $waitingOrder[ApprovedOrder::NAME]['confirmed']
            );
            $this->Log->write('Vykdomas uzdelstas uzsakymas', $fSensor);
            $this->ApprovedOrder->save($save);
            $currentPlan[ApprovedOrder::NAME]['created'] = $lastRec[Record::NAME]['created'];
            $order = &$currentPlan;
        } else {
            $order = &$currentPlan;
        }
        $ao = $this->ApprovedOrder->findById($order[ApprovedOrder::NAME]['id']);
        if ($ao && !empty($ao)) {

            if ($ao[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_WAITING_FOR_DATA) {
                if ($waitingOrder) {
                    $ao[ApprovedOrder::NAME]['created'] = $lastRec[Record::NAME]['created'];
                }
                $ao[ApprovedOrder::NAME]['end'] = $lastRec[Record::NAME]['created'];
            }
            $this->Record->findAndSetOrder($fSensor, new DateTime($ao[ApprovedOrder::NAME]['created']), new DateTime($ao[ApprovedOrder::NAME]['end']), $ao[ApprovedOrder::NAME]['id'], $order);
            if ($ao[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_IN_PROGRESS || $ao[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_WAITING_FOR_DATA) {
                $parameters = array(&$ao, &$this->Record,&$currentPlan);
                if(!$this->Help->callPluginFunction('WorkCenter_ExtendProdUpdateOrderQuantities_Hook', $parameters, Configure::read('companyTitle'))){ //vykdome uzsakymo vykdymo piesima per plugina (unikalus veikimas)
                    $ao[ApprovedOrder::NAME]['quantity'] = $this->Record->findAndSumOrder($order[ApprovedOrder::NAME]['id']);
                }
                if($currentPlan[Plan::NAME]['box_quantity'] > 0){
                    $ao[ApprovedOrder::NAME]['box_quantity'] = floor($ao[ApprovedOrder::NAME]['quantity'] / floatval($currentPlan[Plan::NAME]['box_quantity']));
                }
            }
            //$this->Log->write('Atnaujinamas ApprovedOrderis extendProd funkcijoje: '.json_encode($ao), $fSensor);
            $this->ApprovedOrder->save($ao);
        }
    }

    /**
     * @param string $type
     * @param DateTime $date
     * @param int $foundProblemId
     * @return TimelineEvent
     */
    private function createEvent($type, DateTime $date = null, $foundProblemId = null, $message = null) {
        if (!$date) {
            $date = new DateTime();
            if ($this->recTimeOffset) $date->sub($this->recTimeOffset);
        }
        return new TimelineEvent($type, $date, $foundProblemId, $message);
    }

    /**
     * @param array $rec
     * @return boolean
     */
    private function evalRec($rec) {
        if (!$rec[Record::NAME]['unit_quantity']) return false;
        //if (!$rec[Record::NAME]['plan_id']) return false;
        //if ($rec[ApprovedOrder::NAME]['id']) return false;
        return true;
    }

    private function getSensor() {
        if($this->currentSensor){
            date_default_timezone_set($this->currentSensor['Sensor']['timezone']);
            return $this->currentSensor;
        }
        if (self::$user) {
            $dc = $this->request->param('dc');
            $fUser = $this->User->withRefs()->findById(self::$user->id);
            if ($fUser && !empty($fUser) && isset($fUser[Sensor::NAME]['id']) && $fUser[Sensor::NAME]['id'] > 0) {
                $this->Sensor->id = $fUser['Sensor']['id'];
                $activeSensorsList = $this->Sensor->find('list',array('fields'=>array('Sensor.id'),'conditions'=>array('Sensor.active_user_id'=>$fUser['User']['id'])));
                if(sizeof($activeSensorsList) > 1 && preg_match('#^[0-9]+\_[0-9]+$#', $dc)){//jei vienas useris jungiasi prie keliu darbo centru vienu metu, ir adrese yra jutiklio ID, darbo centra imama is URL
                    list($lineId, $sensorId) = array_map('intval', explode('_', $dc));
                    $fUser['User']['active_sensor_id'] = $sensorId;
                    $fUser['dc'] = $dc;
                }elseif(!empty($activeSensorsList) && sizeof($activeSensorsList) == 1){//jei priskirtas tik vienam darbo centrui, tik tada perrasome
                    $fUser['User']['active_sensor_id'] = current($activeSensorsList);
                    $fUser['dc'] = $dc;
                }
                if(isset($fUser['User']['active_sensor_id']) && $fUser['User']['active_sensor_id'] > 0){
                    $this->Sensor->id = $fUser['User']['active_sensor_id'];
                    $fSensor = $this->Sensor->withRefs()->findById($this->Sensor->id);
                    foreach($fSensor as $keyModel => $modelData){
                        $fUser[$keyModel] = $modelData;
                    }
                }elseif(sizeof(explode(',',$fUser['User']['sensor_id']))==1){
                	$sensorHasOtherUser = $this->User->find('first',array('conditions'=>array('User.active_sensor_id'=>$this->Sensor->id)));
					if(empty($sensorHasOtherUser)){
						$this->User->updateAll(array('active_sensor_id'=>$this->Sensor->id), array('User.id'=>self::$user->id));
					}
                }
                if($fUser['User']['active_sensor_id'] == $fUser['Sensor']['id']){
                    $this->Sensor->save(array('user_update_time'=>date('Y-m-d H:i:s')));
                }
            } else if ($dc && preg_match('#^[0-9]+\_[0-9]+$#', $dc)) {
                list($lineId, $sensorId) = array_map('intval', explode('_', $dc));
//                $fUser = $this->User->withRefs()->find('first', array(
//                    'conditions' => array(
//                        Sensor::NAME.'.id' => $sensorId,
//                        Line::NAME.'.id' => $lineId
//                    ),
//                    'limit' => 1
//                ));
                //if(!$fUser){
                $fUser = $this->Sensor->withRefs()->findById($sensorId);
                $fUser['User'] = $this->Auth->user();
                //}
                //negalima sis skriptas, nes jei koks moderatorius prisijungia trumpam ir iseina lauk, tai jis perkselia esamas prastovas prisiskirdamas jas sau.2020-04-16
//				if($fUser['User']['active_sensor_id'] != $sensorId && $fUser['Sensor']['active_user_id'] == 0){
//					$sensorHasOtherUser = $this->User->find('first',array('conditions'=>array('User.active_sensor_id'=>$sensorId)));
//					if(empty($sensorHasOtherUser) && in_array($sensorId, explode(',',$fUser['User']['sensor_id']))){
//						$this->User->updateAll(array('active_sensor_id'=>$sensorId), array('User.id'=>self::$user->id));
//					}
//				}
				$fUser['dc'] = $dc;
            }
        }
		$fUsers = isset($fUsers)?$fUsers:array($fUser);
        foreach($fUsers as &$fUser){
            $fUser['Sensor']['timezone'] = 'Europe/Vilnius';
            $parameters = array(&$fUser);
            $pluginData = $this->Help->callPluginFunction('Workcenter_beforeReturnUserSensor_Hook', $parameters, Configure::read('companyTitle'));
            if(isset($fUser['Factory'])){
            	$fUser['Sensor']['name'] .= (trim($fUser['Factory']['name'])?' ':'').$fUser['Factory']['name'];
			}
            if (trim($this->request->query('time'))) {
                $time = $this->request->query('time');
                //$time = gmdate('Y-m-d H:i:s', strtotime($time)).' UTC';
                $fUser['dcTime'] = ($time ? new DateTime($time, new DateTimeZone($fUser['Sensor']['timezone'])) : new DateTime());
                //$fUser['dcTime']->setTimezone(new DateTimeZone($fUser['Sensor']['timezone']));
            }
        }
		if(sizeof($fUsers)==1){//i darbo centro langa reikia grazinti tik viena sensoriu
			$this->currentSensor = $fUser;
            date_default_timezone_set($this->currentSensor['Sensor']['timezone']);
			return current($fUsers);
		}else{ //kai kreipiasi is irasu irasymo, graziname visus jutiklius, kad pereitu per juos ieskant ar reikia pakeisti prastovas ir pan
			return $fUsers;
		}
        throw new ForbiddenException('You do not have rights to access this page.');
    }

    public function time() {
        $this->set($val = array(
            'time' => date("Y-m-d H:i:s"),
            'unix' => time(),
        ));
        $this->set('_serialize', array_keys($val));
        $this->RequestHandler->renderAs($this, 'json');
    }

    public function registerVerification(){
        $fSensor = $this->getSensor();
        $currentPlan = $this->Plan->getCurrentPlan($fSensor);
        if(!$currentPlan) return false;
        $parameters = array(&$currentPlan, &$fSensor);
        $pluginData = $this->Help->callPluginFunction('Workcenter_beforeRegisterVerification_Hook', $parameters, Configure::read('companyTitle'));
        $madedCount = $this->Record->find('first', array(
            'fields'=>array('SUM(Record.unit_quantity) AS sum'),
            'conditions'=>array('Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id'])
        ));
        $verificationModel = ClassRegistry::init('Verification');
        $verificationModel->save(array(
            'approved_order_id'=>$currentPlan['ApprovedOrder']['id'],
            'plan_id'=>$currentPlan['ApprovedOrder']['plan_id'],
            'current_count'=>$madedCount[0]['sum']
        ));
        die();
    }

    /** @requireAuth Darbo centras: Pakeisti aktyvų darbo centrą */
    public function changeActiveSensorId($changeToSensorId){
        $fSensor = $this->getSensor();
        $availableSensors = explode(',', $fSensor['User']['sensor_id']);
        if(!in_array($changeToSensorId, $availableSensors)){ $this->redirect($_SERVER['HTTP_REFERER']); }
        $this->User->id = $fSensor['User']['id'];
        $this->User->save(array('active_sensor_id'=>$changeToSensorId));
        $this->Sensor->updateAll(array('user_update_time'=>'\''.date('Y-m-d H:i:s').'\''), array('Sensor.id'=>$changeToSensorId));
        $this->redirect(array('controller'=>'work-center'));
    }

    public function updateRecordsManualy(){
        //die();
        $start = '2019-09-09 14:30:00';
        $end = '2019-09-09 22:53:56';
        $sensors = $this->Sensor->find('all', array('conditions'=>array(
            'Sensor.pin_name <>'=>'',
            'Sensor.id'=>0
        )));
        $this->Record->updateAll(array('Record.found_problem_id'=>null, 'Record.approved_order_id'=>null), array('Record.sensor_id'=>Set::extract('/Sensor/id',$sensors), 'Record.created >='=>$start, 'Record.created <'=>$end));
        $this->FoundProblem->deleteAll(array('FoundProblem.sensor_id'=>Set::extract('/Sensor/id',$sensors), 'FoundProblem.start >='=>$start, 'FoundProblem.start <'=>$end));
        set_time_limit(300);
        foreach($sensors as $fSensor){
            $prodStartsOn = floatval($fSensor[Sensor::NAME]['prod_starts_on']) > 0?floatval($fSensor[Sensor::NAME]['prod_starts_on']):0.1;
            $problemStartsOn = $prodStartsOn - 1 > 0 ? $prodStartsOn - 1 : 0;
            $this->Record->unbindModel(array('belongsTo'=>array('Sensor')));
            $this->Record->bindModel(array('belongsTo'=>array('Shift')));
            $records = $this->Record->find('all', array(
                'conditions' => array(
                    Record::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Record::NAME.'.approved_order_id' => null,
                    Record::NAME.'.found_problem_id' => null,
                    Record::NAME.'.created >=' => $start,
                    Record::NAME.'.created <=' => $end,
                ),
                'order' => array(Record::NAME.'.created ASC'),
                //'limit'=>1
            ));
            $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan','Product'=>array('foreignKey'=>false,'conditions'=>array('Product.id = Plan.product_id')))));
            $approvedOrders = $this->ApprovedOrder->find('all', array('conditions'=>array(
                'ApprovedOrder.created <' => $end,
                'ApprovedOrder.end >' => $start,
                'ApprovedOrder.sensor_id' => $fSensor[Sensor::NAME]['id'],
            )));
            foreach($records as $key=>$record) {
                $approvedOrder = array_filter($approvedOrders, function($approvedOrder)use($record){
                    return strtotime($approvedOrder['ApprovedOrder']['created']) <= strtotime($record['Record']['created']) && strtotime($approvedOrder['ApprovedOrder']['end']) > strtotime($record['Record']['created']);
                });
                $approvedOrder = !empty($approvedOrder)?current($approvedOrder):array();
                $updateData = array();
                $foundProblem = $this->FoundProblem->find('first', array('conditions'=>array(
                    'FoundProblem.start <'=>$record['Record']['created'],
                    'FoundProblem.end >='=>date('Y-m-d H:i:s',strtotime($record['Record']['created'])-Configure::read('recordsCycle')),
                    'FoundProblem.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    'FoundProblem.shift_id' => $record['Record']['shift_id'],
                )));
                $fProblemStart = date('Y-m-d H:i:s',max((strtotime($record['Record']['created'])-(Configure::read('recordsCycle')-1)), strtotime($record['Shift']['start'])));
                $fProblemEnd = date('Y-m-d H:i:s',min(strtotime($record['Record']['created']), strtotime($record['Shift']['end'])-1));
                if(!empty($approvedOrder)){
                    $updateData['approved_order_id'] = $approvedOrder['ApprovedOrder']['id'];
                }
                if($record['Record']['quantity'] >= $prodStartsOn){
                    $createNewProblem = false;
                    if(!empty($foundProblem) && $foundProblem['FoundProblem']['problem_id'] > Problem::ID_WORK){
                        $allowProblemDuration = !empty($approvedOrder) && isset($approvedOrder['Product']['id']) && $approvedOrder['Product']['production_time'] > 0?$approvedOrder['Product']['production_time']*2:$fSensor['Sensor']['records_count_to_start_problem']*Configure::read('recordsCycle');
                        $problemDuration = strtotime($foundProblem['FoundProblem']['end']) + 1 - strtotime($foundProblem['FoundProblem']['start']);
                        if($problemDuration < $allowProblemDuration){
                            $lastWork = $this->FoundProblem->find('first', array(
                                'conditions'=>array('FoundProblem.sensor_id'=>$fSensor[Sensor::NAME]['id'],'FoundProblem.problem_id'=>Problem::ID_WORK,'FoundProblem.end <'=>$fProblemEnd),
                                'order'=>array('FoundProblem.start DESC')
                            ));
                            $this->Record->updateAll(array('Record.found_problem_id'=>null), array('Record.found_problem_id'=>$foundProblem['FoundProblem']['id']));
                            $this->FoundProblem->delete(array('id'=>$foundProblem['FoundProblem']['id']));
                            if(!empty($lastWork)){
                                $this->FoundProblem->updateAll(array('end'=>'\''.$fProblemEnd.'\'', 'quantity'=>'quantity + '.($record['Record']['quantity']+$foundProblem['FoundProblem']['quantity'])), array('id'=>$lastWork['FoundProblem']['id']));
                            }
                        }else{
                            $createNewProblem = true;
                        }
                    }elseif(!empty($foundProblem) && $foundProblem['FoundProblem']['problem_id'] == Problem::ID_WORK){
                        $this->FoundProblem->updateAll(array('end'=>'\''.$fProblemEnd.'\'', 'quantity'=>'quantity + '.$record['Record']['quantity']), array('id'=>$foundProblem['FoundProblem']['id']));
                    }else{
                        $createNewProblem = true;
                    }
                    if($createNewProblem){
                        $foundProblem = $this->FoundProblem->newItem(
                            new DateTime($fProblemStart),
                            new DateTime($fProblemEnd),
                            Problem::ID_WORK,
                            $fSensor[Sensor::NAME]['id'],
                            $record['Record']['shift_id'],
                            2,
                            null,
                            false,
                            (!empty($approvedOrder)?$approvedOrder['ApprovedOrder']['plan_id']:0),
                            array(),
                            $record['Record']['quantity'],
                            0,
                            (!empty($approvedOrder)?$approvedOrder['ApprovedOrder']['id']:0)
                        );
                    }
                }else{
                    if(empty($foundProblem) || $foundProblem['FoundProblem']['problem_id'] == Problem::ID_WORK){
                        $foundProblem = $this->FoundProblem->newItem(
                            new DateTime($fProblemStart),
                            new DateTime($fProblemEnd),
                            Problem::ID_NOT_DEFINED,
                            $fSensor[Sensor::NAME]['id'],
                            $record['Record']['shift_id'],
                            2,
                            null,
                            false,
                            (!empty($approvedOrder)?$approvedOrder['ApprovedOrder']['plan_id']:0),
                            array(),
                            $record['Record']['quantity'],
                            0,
                            (!empty($approvedOrder)?$approvedOrder['ApprovedOrder']['id']:0)
                        );
                    }elseif(!empty($foundProblem)){
                        $this->FoundProblem->updateAll(array('end'=>'\''.$fProblemEnd.'\'', 'quantity'=>'quantity + '.$record['Record']['quantity']), array('id'=>$foundProblem['FoundProblem']['id']));
                    }
                    $updateData['found_problem_id'] = $foundProblem['FoundProblem']['id'];
                }
                if(!empty($updateData)){
                    $updateData['id'] = $record['Record']['id'];
                    $this->Record->save($updateData);
                }
            }
        }
        die('Atlikta');
    }
}
