<?php
//
App::import('Vendor', 'autoload');
App::uses('MainMenuItem', 'Controller/Component');
App::uses('AppUser', 'Controller/Component');
App::uses('Log', 'Model');
App::uses('User', 'Model');
App::uses('ConnectionManager', 'Model');

/**
 * Description of AppController
 * @property CakeRequest $request
 * @property SessionComponent $Session
 */
class AppController extends Controller {

	const EMAIL_CONFIG = 'default';
	
	/** @var MainMenuItem[] */
	protected static $navigation;
	/** @var MainMenuItem[] */
	protected static $bredcrumbs;
	/**
	 * Currently logged-in user
	 * @var AppUser
	 */
	public static $user;
	/** @var Boolean */
	private static $useLeftPanel = false;
	/**
	 * Color Themes: style.brown, style.green, style.red, style.palegreen, style.navyblue
	 * for default assign NULL
	 * @var string
	 */
	private static $colorTheme = 'style.navyblue';
	/** @var boolean */
	private static $authComplete = false;
	/** @var string */
	private $authActionId = null;
    public $adminRedirect = array('plugin'=>false,'controller'=>'users','action'=>'login');
	public $components = array('Cookie','Session','Help','Auth','RequestHandler');
    public $uses = array('User','Group','Permission','Settings','Sensor','SMS');
    public $enabledPlugins = array();

	public function beforeFilter() {
	    //$this->SMS->send('tekstas','862412214','OEE','eventas',160,true);die();
	    $byPassServersIP = is_array(Configure::read('ByPassServersIP'))?Configure::read('ByPassServersIP'):array();
        $serverIp = is_array(Configure::read('ServerIP'))?Configure::read('ServerIP'):array(Configure::read('ServerIP'));
        if(!isset($_COOKIE['Security_login']) && !empty(Configure::read('Security.login')) && !in_array($_SERVER['REMOTE_ADDR'],$serverIp) && !in_array($_SERVER['REMOTE_ADDR'], $byPassServersIP)){
            if($this->params->url != Configure::read('Security.login')){
                header("Location: http://www.horasoee.eu");
                die();
            }else{
                setCookie('Security_login', $this->params->url, time() + (20 * 365 * 24 * 60 * 60), '/');
                $this->redirect(array('controller' => 'login', 'action' => 'index'));
            }
        }elseif(isset($_COOKIE['Security_login']) && $this->params['controller'] == $_COOKIE['Security_login']){
            $this->redirect(array('controller'=>'login'));
        }
        if ($this->name == 'CakeError') {$this->layout = 'ajax'; return null;parent::beforeFilter(); }
        $dataSource = ConnectionManager::getDataSource('default');
        if(isset($dataSource->config['plugin'])){
            Configure::write('companyTitle', $dataSource->config['plugin']);
        }
        $parameters = array(&$this);
        $this->Help->callPluginFunction('App_beforeBeforeFilter_Hook', $parameters, Configure::read('companyTitle'));
		Configure::write('pluginsSettings',ROOT.'/app/tmp/logs/pluginsSettings.txt');
        Configure::write('updatesInfoPath', ROOT.'/app/tmp/logs/updates.txt');
		App::import('Vendor', 'FirePHPCore/fb');
		App::import('Vendor', 'FirePHPCore/ChromePhp');
		$this->authActionId = get_class($this).'::'.$this->request->param('action');

		$this->Cookie->path = '/';
        $availableFiltersText = $this->Session->read('FilterText');
        if(is_array($availableFiltersText)){
            foreach($availableFiltersText as $filterController => $filterValue){
                //if(!in_array($filterController, array($this->params['controller'],'plugins'))){
                if($filterController != $this->params['controller'] && $this->params['controller'] != 'plugins'){
                    $this->Session->write('FilterText.'.$filterController,'');
                }
            }
        }
        /*$login = array(); //
        $login = $this->Session->read('Auth.user');

        if(empty($login) && isset($_COOKIE['CakeCookie']['login'])){
            try{
                $un = @unserialize(base64_decode($this->Cookie->read('login')));
                $this->Session->write('Auth',$un);
            }catch(Exception $e){

            }
        }
        if(!isset($_COOKIE['login']) && !empty($login)){
            $this->Cookie->write('login',base64_encode(serialize($this->Session->read('Auth'))),true,3600*24*30*2,'/');
        }
		if (!self::$authComplete) {
			self::$authComplete = true;
			$user = $this->Session->read('Auth.user');
            if($_SERVER['REMOTE_ADDR'] == $_SERVER['SERVER_ADDR']){
                $userModel = ClassRegistry::init('User');
                $user = $userModel->find('first', array('conditions'=>array('username'=>'admin')));
            }
			if ($user) {
				self::$user = new AppUser();
				self::$user->populate($user);
			} else {
				self::$user = null;
                if($this->params['controller'] != 'Login' && !$this->request->data('login')){
                    $this->redirect(array('controller'=>'Login', 'action'=>'index'));
                }
			}
		}*/
		$login = $this->Session->read('Auth');

        // if(!isset($_COOKIE['CakeCookie']['login'])){
            // die('Susinaikino cookie');//TODO
        // }
		if(empty($login) && isset($_COOKIE['CakeCookie']['login'])){
            $this->Session->write('Auth',@unserialize(base64_decode($this->Cookie->read('login'))));
        }

		if((isset($_SERVER['HTTP_SLAPTAZODIS']) && $_SERVER['HTTP_SLAPTAZODIS'] == Configure::read('Security.cipherSeed')) || (isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_PW'] == Configure::read('PHP_AUTH_PW'))){
            $userModel = ClassRegistry::init('User');
            $user = $userModel->find('first', array('conditions'=>array('User.id'=>User::ID_SUPER_ADMIN)));
            $userData = $user['User']; unset($user['User']); $user = $userData + $user;
            $this->Session->write('Auth.User',$user);
        }elseif(!self::$user){
             $user = $this->Auth->user();
        }else{
            $user = (Array)self::$user;
        }
        if(!empty($user)){
            $user['sensorId'] = $user['sensor_id'];
        }else{
            $user['group_id'] = 0;
            $user['Group']['name'] = '';
        }
        if(isset($_GET['lang'])){
            $this->Session->write('Config.language', $_GET['lang']);
            Configure::write('Config.language', $_GET['lang']);
        }elseif(!empty($user) && isset($user['language']) && trim($user['language'])) {
            $this->Session->write('Config.language', trim($user['language']));
            Configure::write('Config.language', trim($user['language']));
        }elseif($this->Session->check('Config.language')) {
            if($user && !isset($user['language'])){
                $this->Session->write('Config.language', Configure::read('defaultLanguage'));
            }else{
                $this->Session->write('Config.language', $this->Session->read('Config.language'));
            }
        }else{
            $this->Session->write('Config.language', Configure::read('defaultLanguage'));
        }
        if(isset($user['id'])){
            if(isset($user['selected_sensors']) && is_array($user['selected_sensors'])){
                //nedarome nieko, nes jutikliai jau yra uzkrauti anksciau dar pries requestAction
            }else {
                $user['selected_sensors'] = !isset($user['selected_sensors']) || !trim($user['selected_sensors']) ? $this->Sensor->find('list', array('fields' => array('id', 'id'))) : explode(',', $user['selected_sensors']);
            }
        }
        self::$user = (Object)$user;
        Configure::write('user', self::$user);
		if (!isset($this->data['User']['username']) && !isset($this->data['User']['password'])) {
            // if(!$this->User->checkAllUsersLogoutStatus($user, $this->Cookie)){
                // $this->redirect($this->Auth->logout());
            // }
            if(!isset($_COOKIE['login']) && !empty($user)){
                $this->Cookie->write('login',base64_encode(serialize($this->Session->read('Auth'))),true,0,'');
            }
            $this->set('group_id',$user['group_id']??0);
            if(isset($user['User']['group_id']) && !isset($user['User']['Group'])){
                $group_id = $user['User']['group_id'];
                $group = $this->Group->findById($group_id);
                $this->Session->write('Auth.User.group',$group['Group']['name']);
                $this->redirect(trim($prefix)?$this->simpleRedirect:$this->adminRedirect);
            }
            if(isset($user['Group']) && !in_array($user['Group']['name'], array('SuperAdmin'))){
                $this->_checkPermission();
            }
        }
        $this->enabledPlugins = $this->Help->enabledPlugins = $this->getEnabledPlugins();
        if (!self::$navigation) {
            self::$navigation = array(
                //new MainMenuItem(__('Info skydas'), Router::url(array('plugin'=>'','controller' => 'dashboard', 'action' => 'index')), (strtolower($this->name) == 'dashboard' &&  $this->params->action == 'index'), 'iconfa-laptop'),
                new MainMenuItem(__('Info skydas'), '#', false, 'iconfa-laptop', null, array(
                    new MainMenuItem(__('Info skydas'), Router::url(array('plugin'=>'','controller' => 'dashboard', 'action' => 'index')), (strtolower($this->name) == 'dashboard' &&  $this->params->action == 'index'), 'iconfa-check'),
                    new MainMenuItem(__('Skaidrių rinkiniai'), Router::url(array('plugin'=>'','controller' => 'slides', 'action' => 'display')), (strtolower($this->name) == 'slides' &&  $this->params->action == 'display'), 'iconfa-laptop'),
                    new MainMenuItem(__('Darbo centrų peržiūra'), Router::url(array('plugin'=>'','controller' => 'work_center_6h', 'action' => 'display')), (strtolower($this->name) == 'workcenter6h' &&  $this->params->action == 'display'), 'iconfa-laptop')
                )),
                new MainMenuItem(__('Užsakymų statusas'), Router::url(array('plugin'=>'','controller' => 'approved_orders', 'action' => 'index')), (strtolower($this->name) == 'approvedorders' &&  $this->params->action == 'index'), 'iconfa-book'),
                //new MainMenuItem(__('Gaminiai'), Router::url(array('plugin'=>'','controller' => 'products', 'action' => 'index')), (strtolower($this->name) == 'products'), 'iconfa-check'),
                new MainMenuItem(__('Gaminiai'), '#', false, 'iconfa-check', null, array(
                    new MainMenuItem(__('Gaminiai'), Router::url(array('plugin'=>'','controller' => 'products', 'action' => 'index')), (strtolower($this->name) == 'products' &&  $this->params->action == 'index'), 'iconfa-check')
                )),
                new MainMenuItem(__('Užsakymai'), '#', false, 'iconfa-calendar', null, array(
                    new MainMenuItem(__('Užsakymai'), Router::url(array('plugin'=>'','controller' => 'plans', 'action' => 'index')), (strtolower($this->name) == 'plans' &&  $this->params->action == 'index'), 'iconfa-calendar'),
                    new MainMenuItem(__('Užsakymo duomenys'), Router::url(array('plugin'=>'','controller' => 'approved_orders', 'action' => 'get_mo_stats')), (strtolower($this->name) == 'dashboards' &&  $this->params->action == 'get_mo_stats'), 'iconfa-signal')
                )),
                new MainMenuItem(__('Statistika'), '#', false, 'iconfa-signal', null, array(
                    new MainMenuItem(__('Neatitikties kortelės'), Router::url(array('plugin'=>'','controller' => 'diff_cards', 'action' => 'index')), (strtolower($this->name) == 'diffcards' &&  $this->params->action == 'index'), 'iconfa-tags'),
                    new MainMenuItem(__('Įrašai'), Router::url(array('plugin'=>'','controller' => 'records', 'action' => 'index')), (strtolower($this->name) == 'records' &&  $this->params->action == 'index'), 'iconfa-pencil'),
                    new MainMenuItem(__('Skydelis'), Router::url(array('plugin'=>'','controller' => 'dashboards', 'action' => 'inner')), (strtolower($this->name) == 'dashboards' &&  $this->params->action == 'inner'), 'iconfa-signal'),
                )),
                new MainMenuItem(__('Nustatymai'), '#', false, 'iconfa-cog', null, array(
                    new MainMenuItem(__('Sistemos nustatymai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'index')), (strtolower($this->name) == 'settings' &&  $this->params->action == 'index'), 'iconfa-cog'),
                    new MainMenuItem(__('Linijos'), Router::url(array('plugin'=>'','controller' => 'lines', 'action' => 'index')), (strtolower($this->name) == 'lines' &&  $this->params->action == 'index'), 'iconfa-code-fork'),
                    new MainMenuItem(__('Jutikliai'), Router::url(array('plugin'=>'','controller' => 'sensors', 'action' => 'index')), (strtolower($this->name) == 'sensors' &&  $this->params->action == 'index'), 'iconfa-signal'),
                    new MainMenuItem(__('Prastovų tipai'), Router::url(array('plugin'=>'','controller' => 'problems', 'action' => 'index')), (strtolower($this->name) == 'problems' &&  $this->params->action == 'index'), 'iconfa-warning-sign'),
                    new MainMenuItem(__('Nuostolių tipai'), Router::url(array('plugin'=>'','controller' => 'loss_types', 'action' => 'index')), (strtolower($this->name) == 'losstypes' &&  $this->params->action == 'index'), 'iconfa-trash'),
                    new MainMenuItem(__('Neatitikties tipai'), Router::url(array('plugin'=>'','controller' => 'diff_types', 'action' => 'index')), (strtolower($this->name) == 'difftypes' &&  $this->params->action == 'index'), 'iconfa-tag'),
                    new MainMenuItem(__('Fabrikai'), Router::url(array('plugin'=>'','controller' => 'factories', 'action' => 'index')), (strtolower($this->name) == 'factories' &&  $this->params->action == 'index'), 'iconfa-briefcase'),
                    new MainMenuItem(__('Padaliniai'), Router::url(array('plugin'=>'','controller' => 'branches', 'action' => 'index')), (strtolower($this->name) == 'branches' &&  $this->params->action == 'index'), 'iconfa-home'),
                    new MainMenuItem(__('Pamainos'), Router::url(array('plugin'=>'','controller' => 'shifts', 'action' => 'index')), (strtolower($this->name) == 'shifts' &&  $this->params->action == 'index'), 'iconfa-retweet'),
                    new MainMenuItem(__('Atnaujinimai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'check_updates')), (strtolower($this->name) == 'settings' && $this->params->action == 'check_updates'), 'iconfa-home'),
                    new MainMenuItem(__('Įskiepiai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'controll_plugins')), (strtolower($this->name) == 'settings' && $this->params->action == 'controll_plugins'), 'iconfa-home'),
                )),
                new MainMenuItem(__('Vartotojai'), '#', false, 'iconfa-group', null, array(
                    new MainMenuItem(__('Sukurti vartotoją'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'edit')), (strtolower($this->name) == 'users' &&  $this->params->action == 'edit'), 'iconfa-group'),
                    new MainMenuItem(__('Vartotojų sąrašas'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'index')), (strtolower($this->params->controller) == 'users' && $this->params->action == 'index'), 'iconfa-group'),
                    new MainMenuItem(__('Teisių paskirstymas'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'check_permissions')), (strtolower($this->name) == 'users' &&  $this->params->action == 'check_permissions'), 'iconfa-group'),
                    new MainMenuItem(__('Vartotojo vadovas'), Router::url(array('plugin'=>'files','controller' => 'HorasOEE_'.$this->Session->read('Config.language').'.pdf', 'action' => '')), (strtolower($this->name) == 'users'), 'iconfa-book', null, null, '_blank'),
                )),
                new MainMenuItem(__('Atsijungti'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'logout')), false, 'iconfa-signout')
            );
            $parameters = array(&self::$navigation, $this->params);
            $this->Help->callPluginFunction('App_createMenu_Hook', $parameters, Configure::read('companyTitle'));

            $loaded_plugins = CakePlugin::loaded();
            foreach($loaded_plugins as $plugin) {
                if(!isset($this->enabledPlugins->$plugin) && $this->enabledPlugins) continue;
                try {
                    $plugin_model = $plugin.'AppModel';
                    $this->loadModel($plugin.'.'.$plugin_model);
                } catch(MissingModelException $e) {
                    // Model not found!
                    var_dump($e->getMessage());
                    continue;
                }

                if(!property_exists($plugin_model,'navigationAdditions') || !method_exists($plugin_model,'loadNavigationAdditions')) {
                    continue;
                }
                $plugin_model::loadNavigationAdditions();
                $additions = $plugin_model::$navigationAdditions;
                foreach($additions as $key => $item) {
                    $linkParts = explode('/',trim($item['route'],'/'));
                    $pluginTitle = isset($linkParts[0])?$linkParts[0]:'';
                    $pluginController = isset($linkParts[1])?$linkParts[1]:'';
                    $pluginAction = isset($linkParts[2])?$linkParts[2]:'index';
                    $isSelected = $this->params->plugin == $pluginTitle && $this->params->controller == $pluginController && $this->params->action == $pluginAction;
                    $menuItem = new MainMenuItem($item['name'],$item['route'], $isSelected, $item['icon'],null,null, isset($item['target'])?$item['target']:null);
//                    var_dump(self::$navigation[$key]);
                    $key = isset($item['menu_pos'])?$item['menu_pos']:$key;
                    self::$navigation[$key+1]->items[] = $menuItem;
                }
            }
            self::$bredcrumbs = array(); $nb = false;
            foreach (self::$navigation as $li) {
                if ($li->getIsSelected()) {
                    self::$bredcrumbs[] = $li;
                    break;
                }
                foreach ($li->getItems() as $sli) {
                    if ($sli->getIsSelected()) {
                        self::$bredcrumbs[] = $sli;
                        $nb = true;
                        break;
                    }
                }
                if ($nb) break;
            }
        }
		Configure::write('languagesDescription', array(
            array('title'=>__('Vokiečių k.'),'slug'=>'de_DE'),
            array('title'=>__('Lietuvių k.'),'slug'=>'lt_LT'),
            array('title'=>__('Anglų k.'),'slug'=>'en_EN'),
            array('title'=>__('Anglų k.'),'slug'=>'en_US'),
            array('title'=>__('Rusų k.'),'slug'=>'ru_RU'),
            array('title'=>__('Latvių k.'),'slug'=>'lv_LV'),
            array('title'=>__('Estų k.'),'slug'=>'ee_EE')
        ));
		$appName = $this->Settings->getOne('system_email_name');
        $langDir = scandir(ROOT.'/app/Locale/');
        $possibleLanguages = array_filter($langDir, function($value){ return !preg_match('/^\..*/',$value); });
		$possibleLanguages = array('---')+array_combine($possibleLanguages, $possibleLanguages);
        $possibleLanguages = array_intersect_key(Hash::combine(Configure::read('languagesDescription'),'{n}.slug','{n}.title'), array_flip($possibleLanguages));
        $parameters = array(&$this, self::$user);
        $this->Help->callPluginFunction('App_afterBeforeFilter_Hook', $parameters, Configure::read('companyTitle'));
        $this->set(array(
			'colorTheme' => self::$colorTheme,
			'navigation' => self::$navigation,
			'bredcrumbs' => self::$bredcrumbs,
			'useLeftPanel' => self::$useLeftPanel,
			'user' => self::$user,
			'appName' => $appName,
			'copyRightMsg' => ('&copy; ' . date('Y') . '. ' . $appName . '. ' . __('Visos teisės saugomos') . '.'),
			'authorMsg' => (__('Sukūrė') . ': <a href="http://www.horasmpm.lt/" target="_blank">HorasMPM</a>'),
			'baseUri' => Router::url('/'),
			'enabledPlugins' => $this->getEnabledPlugins(),
			'companyTitle'=>$appName,
			'possibleLanguages'=>$possibleLanguages,
			'currentLang'=>$this->Session->read('Config.language')
		));
		Configure::write('currentLang', $this->Session->read('Config.language'));
        $this->loadModel('Log');
        $this->Log->user = self::$user;
		$this->update_db();
	}

	/**
	 * @param boolean $redirect if true redirect to login page
	 * @param boolean $superAdminOnly
	 * @return boolean returns true if user is logged in
	 */
	protected function requestAuth($redirect = false, $superAdminOnly = false, $onBehalfOf = null) {
	    return true;
		if (self::$user && (!$superAdminOnly || self::$user->id == User::ID_SUPER_ADMIN)) {
			$db = new UserPerm();
			if (self::$user->id == User::ID_SUPER_ADMIN || $db->hasPermission(($onBehalfOf ? $onBehalfOf : $this->authActionId), self::$user->id)) {
				return true;
			} else {
				throw new ForbiddenException(__('Jūs neturite teisės pasiekti šį puslapį.'));
			}
		}
		if ($redirect) {die();
			$urlInfo = array('controller' => 'login', 'action' => 'index');
			if ($this->request->url) {
				$urlInfo['?'] = array('return' => Router::url(null, true));
			}
            
			$this->redirect(Router::url($urlInfo, true));
		}
		return false;
	}

    public function hasAccessToAction($address, $permissionSet = false) {
        if(!isset(self::$user->id)){ return false; }
	    if(self::$user && self::$user->id == User::ID_SUPER_ADMIN) {
            return true;
        }
        $controller = isset($address['controller'])?$address['controller']:'';
        $action = isset($address['action'])?$address['action']:'';
        $plugin = isset($address['plugin'])?$address['plugin']:'';
        $permission = $this->Permission->find('first', array('conditions'=>array('controllers'=>$controller, 'actions'=>$action, 'plugin'=>$plugin)));
        if(empty($permission) || (!empty($permission) && empty(unserialize($permission['Permission']['users'])))){
            return !$permissionSet?true:false;
        }else{
            $users = unserialize($permission['Permission']['users']);
            if($users){
                $users = array_combine($users, $users);
                return array_search(self::$user->group_id, $users);
            }
        }
    }
	
	public static function debugPrint($data) {
		echo '<pre style="color: #8A6D3B; background: #FCF8E3; border: 1px solid #EACD9B; display: block; margin: 0px 0px 5px 0px; padding: 10px;">';
		$args = func_get_args();
		for ($i = 0, $c = count($args); $i < $c; $i++) {
			if ($i > 0) echo '<hr style="display: block; padding: 0px; margin: 10px 0px 5px 0px; background: transparent; font-size: 1px; line-height: 1px; border: none; border-top: 1px solid #EACD9B;" />';
			if ($args[$i] instanceof DateTime) {
				echo $args[$i]->format('Y-m-d H:i:s');
			} else if (is_array($args[$i]) || is_object($args[$i])) {
				print_r($args[$i]);
			} else {
				var_dump($args[$i]);
			}
		}
		echo '</pre>';
	}
	
	public function error404() {
		throw new NotFoundException();
	}
	
	private function update_db(){//public
        if(in_array($_SERVER['REMOTE_ADDR'],array('127.0.0.1'))) {
            return;
        }
    	//atnaujinama duomenu bazes struktura
        $myFile = ROOT."/app/tmp/logs/oee_sql_updates.sql";
        $debugMode = Configure::read('debug');
        if(file_exists($myFile) && $debugMode == 2){
            $handle = fopen($myFile, 'r');
            while (!feof($handle)) {
               $sql = fgets($handle, 2000);
               if(trim($sql)){
                   try{
                        $this->Log->query($sql);
                    }catch(Exception $e){}
               }
            }
            $handle = fopen($myFile, 'w+');
            fclose($handle);
            if(isset($sql) && trim($sql)){
                foreach (new DirectoryIterator(ROOT.'/app/tmp/cache/models') as $fileInfo) {
                    if(!$fileInfo->isDot()) {
                        unlink($fileInfo->getPathname());
                    }
                }
            }
        }
    }

	private function getEnabledPlugins(){
		$fileContent = null;
		if(file_exists(Configure::read('pluginsSettings'))){
			$myfile = fopen(Configure::read('pluginsSettings'), "r") or die("Unable to open file!");
			$fileContent = json_decode(fgets($myfile));
			fclose($myfile);	
		}
		return $fileContent;
	}

    function _checkPermission(){ //public
        $user = $this->Auth->user();
        if(!$user){ $user = (Array)self::$user; }
        if(empty($user)){ return false; }
        $currentGroupId = $user['group_id']??0;
        $currentGroupId = !trim($currentGroupId)?5:$currentGroupId;
        if($currentGroupId == 1){ return true; }
        if($this->params['controller'] == 'users' && in_array($this->params['action'], array('login','logout'))){ return true; }
        if($user && $this->request->is('Ajax') && $this->params['action'] == 'give_permission') return true;
        $res = $this->Permission->find('all',array('cache' => 'users_permissions_cache','+1 month')); //pr($res);
        $this->set('permissions',$res);
        foreach($res as $val){
            if(!isset($val['Permission']['plugin'])){
                Cache::config('sql', array('path' => CACHE.'sql'.DS, 'prefix'=>'permission-users_permissions_cache', 'engine'=>'File'));
                Cache::clear(false, 'sql');
                $this->update_db();
                $this->redirect(array('controller'=>'users', 'action'=>'logout'));
            }
            if(
                strtolower(Inflector::underscore($this->plugin)) == Inflector::underscore($val['Permission']['plugin']) &&
                strtolower(Inflector::underscore($this->params['controller'])) == Inflector::underscore($val['Permission']['controllers']) &&
                strtolower(Inflector::underscore($this->params['action'])) == Inflector::underscore($val['Permission']['actions'])
            ){
                $mas = unserialize($val['Permission']['users']);
                if((sizeof($mas) > 0 && !trim($currentGroupId)) ||
                (sizeof($mas)>0 && !in_array($currentGroupId,$mas))){
                    $pluginTitle = isset($this->params['plugin']) && trim($this->params['plugin'])?trim($this->params['plugin']):'';
                    if(trim($pluginTitle) && trim($this->params['action']) == 'index'){die();}//si eilute reikalinga pluginu skriptams, kurie kraunasi per <script src="">. kai nera teisiu prieiti prie plugino, jo turi tiesiog net nekrauti viduje
                    $this->Cookie->delete('login');
                    $this->Session->setFlash(__('Jūs neturite teisės pasiekti šią sritį:').' '.$pluginTitle.'/'.$this->params['controller'].'/'.$this->params['action']);
                    $redirectPath = isset($user['redirect_page'])?$user['redirect_page']:'';
                    $redirectPath = !trim($redirectPath) && isset($user['default_start_path']) && trim($user['default_start_path'])?$user['default_start_path']:$redirectPath;
                    $redirectPath = !trim($redirectPath)?$this->adminRedirect:$redirectPath;
                    if(isset($_SERVER['HTTP_REFERER'])) {
                        $this->redirect($_SERVER['HTTP_REFERER']);
                    }else{
                        $this->Auth->logout();
                        $this->redirect($redirectPath);
                    }
                }
            }
        }
    }
	
}
