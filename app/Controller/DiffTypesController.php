<?php

App::uses('DiffType', 'Model');

/**
 * @property DiffType $DiffType
 */
class DiffTypesController extends AppController {
	
	const ID = 'diff_types';
	const MODEL = DiffType::NAME;
	
	public $uses = array(self::MODEL);
	
	/** @requireAuth Peržiūrėti neatitikties tipus */
	public function index() {
		$this->requestAuth(true);
		$list = $this->DiffType->find('all');
		$this->set(array(
			'title_for_layout' => __('Neatitikties tipai'),
			'list' => $list,
			'model' => self::MODEL,
			'parentOptions' => $this->DiffType->getAsSelectOptions(true, null),
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?')
		));
	}
	
	/** @requireAuth Redaguoti neatitikties tipus */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		
		if (empty($this->request->data)) {
			$this->request->data = $item = $this->DiffType->findById($id);
		} else {
			$item = null;
			if (!$this->request->data[DiffType::NAME]['parent_id']) $this->request->data[DiffType::NAME]['parent_id'] = null;
			if ($this->DiffType->save($this->request->data)) {
				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			} else {
				$this->Session->setFlash(__('Nepavyko išsaugoti įrašo').': '.print_r($this->DiffType->validationErrors, true), 'default', array(), 'saveMessage');
			}
		}
		$title = $item ? sprintf(__('Neatitikties tipas %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Naujas neatitikties tipas');
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'model' => self::MODEL,
			'parentOptions' => $this->DiffType->getAsSelectOptions(true, null),
			'listUrl' => $listUrl,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true)
		));
	}
	
	/** @requireAuth Pašalinti neatitikties tipus */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		try {
			if ($this->DiffType->delete($id, false)) {
				$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		} catch (PDOException $ex) {
			$code = ''.$ex->getCode();
			if (substr($code, 0, 2) == '23') {
				$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$this->redirect($listUrl);
	}
	
}
