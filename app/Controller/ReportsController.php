<?php
App::uses('Problem', 'Model');
class ReportsController extends AppController{
    var $name = 'Reports';
    var $uses = array('Sensor','Shift', 'Report', 'OrderCalculation', 'DashboardsCalculation', 'Plan','Record','FoundProblem','Loss','LossType','ApprovedOrder','User','Problem','PartialQuantity');
    public $components = array('Help');
    
    public function beforeFilter(){
        $this->Report->Help = $this->Help;
        parent::beforeFilter(); 
    }
    
    public function generateXLS() {
        set_time_limit(800);
        //$shift_id = 1019;
        $params = array(&$this->Report, &$this->request->data);
        $this->Help->callPluginFunction('ReportsContr_BeforeGenerateXLS_Hook', $params, Configure::read('companyTitle'));
        $this->loadModels();
        if($this->request->data['view_angle'] > 0){
            $this->calculatePeriodXLS();
            return null;
        }
        $date_start = isset($date[0])?trim($date[0]):date('Y-m-d');
        $date_end = isset($date[1])?trim($date[1]):date('Y-m-d');
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        if(empty($this->request->data['sensors'])){
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('controller'=>'dashboards','action'=>'inner'));
        }
        $sensor_id = $this->request->data['sensors']; 
//        $sensorBranches = $this->Sensor->find('list',array('fields'=>array('Sensor.id','Sensor.branch_id'),'conditions'=>array('Sensor.id'=>$sensor_id)));
//        $shifts = $this->Shift->find('all', array(
//            'conditions' => array(
//                'start <'   => $date_end,
//                'end >'     => $date_start,
//                'branch_id' => array_unique($sensorBranches)
//            ),
//            'order'      => array('start ASC')
//        ));
        $this->DashboardsCalculation->bindModel(array('belongsTo' => array(
            'Shift',
        )));
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'SUM(DashboardsCalculation.'.$col.') AS '.$col; });
        $fields[] = 'Shift.*';
        $calculations = $this->DashboardsCalculation->find('all', array(
            'fields'=>$fields,
            'conditions' => array(
                'DashboardsCalculation.sensor_id' => $sensor_id,
                'Shift.start <' => $date_end,
                'Shift.end >' => $date_start,
            ),'group'=>array('DashboardsCalculation.shift_id'),
            'order'=>array('Shift.start')
        ));
        $settings = ClassRegistry::init('Settings');
        $sysName = $settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator($sysName)
            ->setLastModifiedBy($sysName)
            ->setTitle(__('Ataskaita'));
        $generatedGlobalData = array();
        $shifts_start = null;
        $shifts_end = null; 
        $this->Report->maxDowntimeLevel = (int)current($this->Problem->find('first', array('fields'=>array('MAX(Problem.level) AS max_level')))[0]);
        $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
        $this->Report->downtimesExclusionsIds = $plugin_model::getExclusionIdList();
        $this->Report->sheetNr = 1;
        foreach ($calculations as $i => $calculation){
            if ($i == 0) {
                $shifts_start = $calculation['Shift']['start'];
            }
            $shifts_end = $calculation['Shift']['end'];
            $sheet = $this->Report->generateShiftSheet($objPHPExcel, $calculation, $sensor_id, $i + 1);
			if(!empty($sheet)){
				$generatedGlobalData[] = $sheet;
			}
        }
        $objPHPExcel->setActiveSheetIndex(0);
        $this->Report->generateGlobalSheet($objPHPExcel, $generatedGlobalData, $shifts_start, $shifts_end, $sensor_id);
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        if(isset($this->request->params['named']['exportToFile'])){
            $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
        }else{
            header('Content-Type: application/vnd.ms-excel');
            header('Content-Disposition: attachment;filename="' . __('Ataskaita') . '.xls"');
            header('Cache-Control: max-age=0');
            header('Cache-Control: max-age=1');
            header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
            header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
            header('Cache-Control: cache, must-revalidate');
            header('Pragma: public');
            $objWriter->save('php://output');
            exit();
        }
    }

    public function generate_found_problems_report(){
        $date_start = isset($date[0])?trim($date[0]):date('Y-m-d');
        $date_end = isset($date[1])?trim($date[1]):date('Y-m-d');
        if(!trim($this->request->data['start_end_date'])){
            $this->Session->setFlash(__('Neparinkta intervalo pradžia'),'default');
            $this->redirect(array('controller'=>'dashboards','action'=>'inner'));
        }
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        $sensor_id = $this->request->data['sensors'];
        $sensor = $this->Sensor->findById($sensor_id);
        $sensorBranches = $this->Sensor->find('list',array('fields'=>array('Sensor.id','Sensor.branch_id'),'conditions'=>array('Sensor.id'=>$sensor_id)));
        $shifts = $this->Shift->find('all', array(
            'conditions' => array(
                'start <'   => $date_end,
                'end >'     => $date_start,
                'branch_id' => array_unique($sensorBranches)
            ),
            'order'      => array('start ASC')
        ));
        $problemsByShifts = array();
        foreach($shifts as $shift){
            $problemsByShifts[$shift['Shift']['id']] = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,0);
        }
        $settings = ClassRegistry::init('Settings');
        $sysName = $settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);

        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        //$objPHPExcel->getProperties()->setCreator($sysName->setLastModifiedBy($sysName))->setTitle(__('Įvykių žurnalas'), false);
        $maxDowntimeLevel = (int)current($this->Problem->find('first', array('fields'=>array('MAX(Problem.level) AS max_level')))[0]);

        $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
        $downtimesExclusionsIds = $plugin_model::getExclusionIdList();
        $colors = array(-1=>'97dc97', 1=>'e8c775',2=>'c4c4c4', 3=>'fe9b98');
        $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        $this->set(compact('objPHPExcel','shifts','problemsByShifts','maxDowntimeLevel','downtimesExclusionsIds','colors','sensor','problemsTreeList'));
    }

    public function loadModels(){
        $this->Report->Help = $this->Help;
        $this->Report->OrderCalculation = $this->OrderCalculation;
        $this->Report->FoundProblem = $this->FoundProblem;
        $this->Report->DashboardsCalculation = $this->DashboardsCalculation;
        $this->Report->Record = $this->Record;
        $this->Report->Plan = $this->Plan;
        $this->Report->Loss = $this->Loss;
        $this->Report->LossType = $this->LossType;
        $this->Report->User = $this->User;
        $this->Report->Problem = $this->Problem;
        $this->Report->PartialQuantity = $this->PartialQuantity;
        $this->Report->ApprovedOrder = $this->ApprovedOrder;
    }
    
    private function calculatePeriodXLS(){
        $this->layout = 'ajax';
        $templateTitle = 'calculate_period_xls';
        $recordModel = ClassRegistry::init('Record');
        if(empty($this->request->data['sensors'])){
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('controller'=>'dashboards','action'=>'inner'));
        }
        $params = array(&$this->Report, &$templateTitle);
        $this->Help->callPluginFunction('ReportsContr_BeforeCalculatePeriodXLS_Hook', $params, Configure::read('companyTitle'));
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($this->request->data['start_date'],$this->request->data['end_date']) = $this->Help->setStartEndByPattern($this->request->data);
        }elseif(isset($this->request->data['start_end_date'])){
            list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
        }
        $viewAngle = $this->request->data['view_angle'];
        if($viewAngle == 1){
            $periodDetails = array(
                'title' => __('Savaitė'),
                'time_group_mechanism' => '%Y-%v',
                'time_title_mechanism' => ' '.__('savaitė'),
                'type'=>'week'
            );   
        }elseif($viewAngle == 2){
            $periodDetails = array(
                'title' => __('Mėnuo'),
                'time_group_mechanism' => '%Y-%m',
                'time_title_mechanism' => ' '.__('mėnuo'),
                'type'=>'month'
            ); 
        }elseif($viewAngle == 3){
            $periodDetails = array(
                'title' => __('Para'),
                'time_group_mechanism' => '%Y-%z',
                'time_title_mechanism' => ' '.__('para'),
                'type'=>'day'
            ); 
        }
        list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        $calculation = $this->Report->getWeekleXLSData($this->request->data, $periodDetails);
        $sensors = $this->Sensor->find('list', array('fields'=>array('id', 'name')));
        $shiftsExtremumsParams = array(
            'fields'=>array('Shift.branch_id','MIN(Shift.start) as first_shift_start', 'MAX(Shift.end) AS last_shift_end'),
            'conditions'=>array('Shift.start >=' => $this->request->data['start_date'], 'Shift.start <' => $this->request->data['end_date']),
            'group'=>array('Shift.branch_id')
        );
        $params = array(&$calculation, &$shiftsExtremumsParams,&$this->request->data);
        $this->Help->callPluginFunction('ReportsContr_AfterCalculatePeriodXLS_Hook', $params, Configure::read('companyTitle'));
        $shiftsExtremums = Set::combine($this->Shift->find('all', $shiftsExtremumsParams),'{n}.Shift.branch_id', '{n}.0');
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle($periodDetails['title'].' '. __("Periodo ataskaita"));
        $this->set(compact('objPHPExcel','calculation','sensors','periodDetails','viewAngle','recordModel','shiftsExtremums'));
        $this->render($templateTitle);
    }

    public function generateTransitionReport(){
        $this->layout = 'ajax';

        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['trans_start_end_date'])){
                list($date_start, $date_end) = explode(' ~ ',$this->request->data['trans_start_end_date']);
            }
        }
//        if(isset($this->request->data['trans_start_end_date'])){
//            list($this->request->data['trans_start_date'], $this->request->data['trans_end_date']) = explode(' ~ ',$this->request->data['trans_start_end_date']);
//        }
//        $date_start = $this->request->data['trans_start_date'];
//        $date_end = $this->request->data['trans_end_date'];
        $sensor_id = $this->request->data['sensors'];
        $sensor = $this->Sensor->findById($sensor_id);
        $this->FoundProblem->bindModel(array(
            'belongsTo'=>array(
                // 'ApprovedOrderPrev'=>array(
                    // 'fields'=>array('MAX(ApprovedOrderPrev.created) as prev_created'),
                    // 'className'=>'ApprovedOrder',
                    // 'foreignKey'=>false, 
                    // 'conditions'=>array('ApprovedOrderPrev.sensor_id = FoundProblem.sensor_id AND ApprovedOrderPrev.created < FoundProblem.start'),
                // ),
                // 'ApprovedOrderNext'=>array(
                    // 'fields'=>array('MIN(ApprovedOrderNext.created) as next_created'),
                    // 'className'=>'ApprovedOrder',
                    // 'foreignKey'=>false, 
                    // 'conditions'=>array('ApprovedOrderNext.sensor_id = FoundProblem.sensor_id AND ApprovedOrderNext.created >= FoundProblem.start'),
                // ),
                 'TransitionProblem'=>array('foreignKey'=>'transition_problem_id', 'className'=>'Problem'),
                 'Problem',
                 'User'
                //'Record'=>array('foreignKey'=>false, 'className'=>'Record', 'conditions'=>array('Record.found_problem_id = FoundProblem.id'))
            ),
        ));

        $exceededTransitionId = $this->Settings->getOne('exceeded_transition_id');

        $problems = $this->FoundProblem->find('all', array(
            'conditions' => array(
                'FoundProblem.start <'   => $date_end,
                'FoundProblem.end >'     => $date_start,
                'OR'=>array(
                    'FoundProblem.transition_problem_id >'=>0,
                    'FoundProblem.problem_id'=>array(Problem::ID_NEUTRAL, $exceededTransitionId)
                ),
                'FoundProblem.sensor_id' => $sensor_id,
                //'Record.id IS NOT NULL'
            ),
            'order'      => array('FoundProblem.start ASC'),
            'group'=>array('FoundProblem.id')
        ));
        //$problemsIds = Set::extract('/FoundProblem/id', $problems);
        // $recordHavingProblem = $this->Record->find('list', array(
            // 'fields'=>array('Record.found_problem_id'),
            // 'conditions'=>array('Record.found_problem_id'=>$problemsIds),
            // 'group'=>array('Record.found_problem_id')
        // ));
        // $problems = array_values(array_filter($problems, function($problem)use($recordHavingProblem){ return in_array($problem['FoundProblem']['id'], $recordHavingProblem); }));
        $this->ApprovedOrder->bindModel(array(
            'belongsTo'=>array('Plan')
        ));
        $plans = Hash::combine($this->ApprovedOrder->find('all', array(
            'fields'=>array('Plan.*','ApprovedOrder.created AS created'),
            'conditions'=>array(
                'ApprovedOrder.created >'=>date('Y-m-d H:i:s',strtotime($date_start)-(7*24*3600)),
                'ApprovedOrder.created <'=>date('Y-m-d H:i:s',strtotime($date_end)+(7*24*3600)),
                'ApprovedOrder.sensor_id' => $sensor_id
            ),'order'=>array('ApprovedOrder.created')
        )),'{n}.ApprovedOrder.created', '{n}');
        array_walk($problems, function(&$problem)use($plans){
            $problem['ApprovedOrderPrev'] = current(array_reverse(array_filter($plans, function($date)use($problem){
                return strtotime($date) < strtotime($problem['FoundProblem']['start']);
            }, ARRAY_FILTER_USE_KEY)));
            $problem['ApprovedOrderNext'] = current(array_filter($plans, function($date)use($problem){
                return strtotime($date) >= strtotime($problem['FoundProblem']['start']);
            }, ARRAY_FILTER_USE_KEY));
        });
        $settings = ClassRegistry::init('Settings');
        $sysName = $settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator($sysName)
            ->setLastModifiedBy($sysName)
            ->setTitle($sensor['Sensor']['name'].' '.__("perėjimų ataskaita"));
        $objPHPExcel->setActiveSheetIndex(0);
        $this->set(compact('problems','objPHPExcel','sensor'));
        $this->render('generate_transition_report');
    }

    public function generateWorkersReport(){
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            $sensors = $this->User->find('list', array('fields'=>array('id','sensor_id'), 'conditions'=>array('User.id'=>$this->request->data['operators'])));
            $this->request->data['sensors'] = $sensors;
            list($this->request->data['start_date'],$this->request->data['end_date']) = $this->Help->setStartEndByPattern($this->request->data);
        }else if(isset($this->request->data['start_end_date'])){
            list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
        }
        $date_start = $this->request->data['start_date'];
        $date_end = $this->request->data['end_date'];
        if(empty($this->request->data['operators'])){
            $this->Session->setFlash(__('Neparinktas nei vienas darbuotojas'));
            $this->redirect(array('controller'=>'dashboards', 'action'=>'inner'));
        }elseif(!trim($this->request->data['start_date'])){
            $this->Session->setFlash(__('Neparinkta intervalo pradžia'));
            $this->redirect(array('controller'=>'dashboards', 'action'=>'inner'));
        }elseif(!trim($this->request->data['end_date'])){
            $this->Session->setFlash(__('Neparinkta intervalo pabaiga'));
            $this->redirect(array('controller'=>'dashboards', 'action'=>'inner'));
        }
        $params = array($date_start, $date_end, $this->request->data);
        $this->Help->callPluginFunction('ReportsContr_generateWorkersReport_Hook', $params, Configure::read('companyTitle'));
        //Jei nera ataskaitos generavimo per imones plugina, tuomet generuojame cia
        $this->OrderCalculation->bindModel(array('belongsTo'=>array(
            'Sensor','ApprovedOrder','User',
            'Plan'=>array('foreignKey'=>false,'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $data = $this->OrderCalculation->find('all', array(
            'fields'=>array(
                'OrderCalculation.id',
                'CONCAT(User.first_name,\' \',User.last_name) AS worker',
                'User.id',
                'Sensor.name',
                'Plan.production_name',
                'Plan.preparation_time',
                'Plan.step',
                'SUM(OrderCalculation.time_green) AS production_time',
                'TIMESTAMPDIFF(SECOND,MIN(OrderCalculation.order_start), MAX(OrderCalculation.order_end)) AS work_time',
                'ROUND((OrderCalculation.time_yellow + OrderCalculation.exceeded_time_yellow) / 60, 2) AS total_transition_time',
                'SUM(OrderCalculation.quantity) AS quantity'
            ),'conditions'=>array(
                'Plan.id <>'=>1,
                'OrderCalculation.user_id'=>$this->request->data['operators'],
                'OrderCalculation.order_start >='=>$date_start,
                'OrderCalculation.order_end <='=>$date_end,
            ),'group'=>array('OrderCalculation.user_id', 'OrderCalculation.approved_order_id'),
            'order'=>array('OrderCalculation.user_id','OrderCalculation.order_start')
        ));
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(compact('objPHPExcel','data'));
        $this->render('generate_workers_report');
    }

    public function get_attributes_usage_xls(){
    	if(empty($this->request->data['sensors'])){
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('controller'=>'dashboards','action'=>'inner'));
        }
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($this->request->data['start_date'],$this->request->data['end_date']) = $this->Help->setStartEndByPattern($this->request->data);
        }else if(isset($this->request->data['start_end_date'])){
            list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
        }
        $date_start = $this->request->data['start_date'];
        $date_end = $this->request->data['end_date'];
        $bindModel = array('belongsTo'=>array('Sensor','Problem'=>array('foreignKey'=>'super_parent_problem_id')));
        $searchParameters = array(
            'fields'=>array(
                'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)) AS duration',
                'TRIM(Sensor.name) AS sensor_name',
                'TRIM(Problem.name) AS problem_name',
                'TRIM(Problem.id) AS problem_id',
                'Problem.id',
                'Sensor.id'
            ),'conditions'=>array(
                'Problem.parent_id'=>0,
                'FoundProblem.sensor_id'=>$this->request->data['sensors'],
                'FoundProblem.start >='=>$date_start,
                'FoundProblem.end <='=>$date_end,
            ),'group'=>array('FoundProblem.sensor_id', 'Problem.id'),
            'order'=>array('Sensor.name')
        );
        $params = array(&$bindModel, &$searchParameters, &$this->request->data);
        $this->Help->callPluginFunction('Reports_BeforeSearchGenerateAttributesUsage_Hook', $params, Configure::read('companyTitle'));
        $this->FoundProblem->bindModel($bindModel);
        $foundProblemsTmp = $this->FoundProblem->find('all', $searchParameters);
        $foundProblems = array();
        foreach($foundProblemsTmp as $foundProblemTmp){
            $foundProblems[$foundProblemTmp['Problem']['id']][$foundProblemTmp['Sensor']['id']] = $foundProblemTmp[0]; 
        }
        unset($foundProblemsTmp);
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(compact('objPHPExcel','foundProblems'));
        $params = array(&$this);
        $this->Help->callPluginFunction('Reports_AftergenerateAttributesUsage_Hook', $params, Configure::read('companyTitle'));
        $this->render('get_attributes_usage_xls');
    }

}
