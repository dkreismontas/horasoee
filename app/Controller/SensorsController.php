<?php

App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Factory', 'Model');

/**
 * @property Sensor $Sensor
 * @property Branch $Branch
 */
class SensorsController extends AppController {
	
	const ID = 'sensors';
	const MODEL = Sensor::NAME;
	
	public $uses = array(self::MODEL, Branch::NAME, Factory::NAME, Line::NAME);
	
	/** @requireAuth Peržiūrėti jutiklius */
	public function index() {
		$this->requestAuth(true);
		$list = $this->Sensor->withRefs()->find('all', array('conditions'=>array('Sensor.id'=>Configure::read('user')->selected_sensors), 'order'=>array('Sensor.position','Line.name', 'Sensor.name')));
		$viewVars = array(
            'title_for_layout' => __('Jutikliai'),
            'list' => $list,
            'model' => self::MODEL,
            'typeOptions' => Sensor::getTypes(),
            'newUrl' => Router::url('0/edit'),
            'editUrl' => Router::url('%d/edit'),
            'removeUrl' => Router::url('%d/remove'),
            'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?')
        );
        $this->Sensor->addDashboardsToFakeUser();
        $this->set($viewVars);
        $parameters = array(&$this, &$viewVars);
        $this->Help->callPluginFunction('SensorsController_afterIndex_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Redaguoti jutiklius */
	public function edit() {
		$this->requestAuth(true);
		$id = current(array_intersect(array($this->request->params['id']), Configure::read('user')->selected_sensors));
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		
		if (empty($this->request->data)) {
			$this->request->data = $item = $this->Sensor->findById($id);
		} else {
            $errors = $this->Sensor->checkDataCorrectivity($this->request->data);
			if(empty($errors)){
                $this->Sensor->uploadImage($this->request->data['Sensor']);
                $item = null;
                $this->Log->write("Atnaujinami jutiklio nustatymai. Pries atnaujinima buve nustatymai: ".json_encode($this->Sensor->findById($this->request->data['Sensor']['id'])));
                $this->Log->write("Atnaujinami jutiklio nustatymai. Po atnaujinima esantys nustatymai: ".json_encode($this->request->data));
                if ($this->Sensor->save($this->request->data)) {
                    $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                    $this->Sensor->addDashboardsToFakeUser($this->Sensor->id);//kursime skydeliu sarasa, kurie bus priskirti id:-1 vartotojui, kad per ji butu galima siusti mailu prenumerata
                    $this->redirect($listUrl);
                }
            }else{
                //$this->request->data = $item = $this->Sensor->findById($id);
                $this->Session->setFlash(implode('<br />',$errors), 'default', array(), 'saveMessage');
            }
		}
        if(empty($this->request->data)){
            $this->request->data['Sensor']['records_count_to_start_problem'] = 3;
            $this->request->data['Sensor']['half_donut_colors'] = '75:#D64D49, 80:#FDEE00, 100:#5AB65A';
        }
		$title = $item ? sprintf(__('Jutiklis %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Naujas jutiklis');
		$this->Branch->virtualFields = array('factory_name'=>'IF(Factory.id IS NULL, \''.__('Nepriskirtas fabrikas').'\', Factory.name)');
        $viewVars = array(
            'title_for_layout' => $title,
            'h1_for_layout' => $title,
            'model' => self::MODEL,
            'branchOptions' => $this->Branch->withRefs()->find('list', array('recursive'=>1, 'fields'=>array('Branch.id','Branch.name','factory_name'),'conditions'=>array('OR'=>array('Branch.sensor_id'=>$id, 'Branch.sensor_id IS NULL')))),
            'typeOptions' => Sensor::getTypes(),
            'sensorOptions'=> $this->Sensor->find('list',array('id','CONCAT(ps_id," ",name)')),
            'listUrl' => $listUrl,
            'formUrl'=> Router::url(($id ? $id : 0).'/edit', true),
            'helpComponent'=>$this->Help
        );
        $this->Branch->virtualFields = array();
        $viewVars['linesOptions'] = $this->Line->getAsSelectOptions();
        $this->set($viewVars);
        $parameters = array(&$this, &$viewVars);
        $this->Help->callPluginFunction('SensorsController_afterEdit_Hook', $parameters, Configure::read('companyTitle'));
	}
	
	/** @requireAuth Pašalinti jutiklius */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		try {
			if ($this->Sensor->delete($id, false)) {
				$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		} catch (PDOException $ex) {
			$code = ''.$ex->getCode();
			if (substr($code, 0, 2) == '23') {
				$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$this->redirect($listUrl);
	}
    
}
