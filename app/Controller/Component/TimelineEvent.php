<?php

/**
 * Description of TimelineEvent
 */
class TimelineEvent {
	
	const TYPE_NONE = 'none';
	const TYPE_STARTED = 'started';
	const TYPE_STOPPED = 'stopped';
	const TYPE_SPEED = 'speed';
	
	/** @var string one of TYPE_* constants */
	public $type;
	/** @var DateTime */
	public $dateTime;
	/** @var int */
	public $foundProblemId;
	/** @var string */
	public $message;
	
	/**
	 * @param string $type  one of TYPE_* constants
	 * @param DateTime $dateTime
	 * @param int $foundProblemId
	 */
	public function __construct($type, DateTime $dateTime = null, $foundProblemId = null, $message = null) {
		$this->type = $type;
		$this->dateTime = $dateTime;
		$this->foundProblemId = $foundProblemId;
		$this->message = $message;
	}
	
	/** @return array */
	public function jsonSerialize() {
		return array(
			'type' => $this->type,
			'dateTime' => ($this->dateTime ? $this->dateTime->format('c') : null),
			'foundProblemId' => $this->foundProblemId,
			'message' => $this->message
		);
	}
	
}
