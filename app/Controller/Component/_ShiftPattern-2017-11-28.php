<?php

/**
 * Description of ShiftPattern
 */
class ShiftPattern {
	
	const TYPE_DAY = 'D';
	const TYPE_NIGHT = 'N';
	const TYPE_FREE = 'P';
	
	/** @var int */
	private $branchCode;
	
	/** @var stdClass[] */
	private $pattern;
	/** @var string[] */
	private $patternIndex;
	
	/** @var DateTime[] */
	private $offsets;
	
	/** @var array */
	private $times;
	
	/**
	 * 
	 * @param int $branchCode
	 * @param string[] $pattern
	 * @param string[] $offsets
	 */
	public function __construct($branchCode, array $pattern, array $offsets, array $times) {
		$this->branchCode = $branchCode;
		$this->pattern = array();
		$this->patternIndex = array();
		foreach ($pattern as $p) {
			$pa = explode(':', $p);
			$this->pattern[] = (object) array('type' => $pa[0], 'days' => $pa[1]);
			for ($i = 0, $c = intval($pa[1]); $i < $c; $i++) {
				$this->patternIndex[] = $pa[0];
			}
		}
		$this->offsets = array();
		foreach ($offsets as $o) {
			$this->offsets[] = new DateTime($o);
		}
		$this->times = array();
		foreach ($times as $k => $t) {
			$this->times[$k] = explode('-', $t);
		}
	}
	
	/**
	 * @param DateTime $date
	 * @param string $type
	 * @return DateTime[]
	 */
	public function getTimesFor(DateTime $date, $type) {
		$dt = clone $date;
		$sDate = new DateTime($dt->format('Y-m-d').' '.$this->times[$type][0]);
		if ($this->times[$type][1] < $this->times[$type][0]) $dt->add(new DateInterval('P1D'));
		$eDate = new DateTime($dt->format('Y-m-d').' '.$this->times[$type][1]);
		return array($sDate, $eDate);
	}
	
	/** @return int */
	public function getBranchCode() {
		return $this->branchCode;
	}
	
	/** @return int */
	public function getOffsetCount() {
		return count($this->offsets);
	}
	
	/**
	 * @param int $index
	 * @return DateTime
	 */
	public function getOffset($index) {
		return $this->offsets[$index];
	}
	
	/** @return int */
	public function getLength() {
		return count($this->patternIndex);
	}
	
	/**
	 * @param int $index
	 * @return string one of TYPE_* constants
	 */
	public function getTypeAtIndex($index) {
		return $this->patternIndex[$index];
	}
	
	/**
	 * @param DateTime $date
	 * @param int $offsetIndex
	 * @return int
	 */
	public function getPatternIndexFor(DateTime $date, $offsetIndex) {
		$offset = $this->getOffset($offsetIndex);
		/* @var $diff DateInterval */
		$diff = $date->diff($offset, false);
		$idx = $diff->days - floor($diff->days / $this->getLength()) * $this->getLength();
		return ($diff->invert || $idx == 0) ? $idx : ($this->getLength() - $idx);
	}
	
}