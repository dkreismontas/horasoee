<?php

/**
 * Description of PlanParser
 */
class PlanParser {
	
	/** @var string */
	private $file;
	/** @var array */
	private $plan;
	/** @var int */
	private $lvl;
	/** @var boolean */
	private $badBlock;
	/** @var string[] */
	private $path;
	/** @var callable */
	private $callback;
	
	public function __construct() {
		
	}
	
	/**
	 * Parse plan file
	 * @param string $file
	 * @param callable $callback
	 * @throws ErrorException
	 */
	public function parse($file, $callback) {
		$this->file = $file;
		$this->callback = $callback;
		$this->plan = null;
		$this->lvl = 0;
		$this->badBlock = false;
		$this->path = array();
		/* @var $parser resource */
		$parser = xml_parser_create();
		xml_set_object($parser, $this);
		xml_set_element_handler($parser, 'onOpenTag', 'onCloseTag');
		xml_set_character_data_handler($parser, 'onCharData');
		if (($fh = fopen($file, 'rb'))) {
			while (!feof($fh) && ($data = fread($fh, filesize($file))) !== false) {
				$res = xml_parse($parser, $data, false);
				if (!$res) throw $this->handleError($file, $parser, true);
			}
			fclose($fh);
		}
		$res = xml_parse($parser, '', true);
		if (!$res) throw $this->handleError($file, $parser, true);
		if ($this->badBlock) throw $this->handleCustomError(__('Bad file format'), 0, $file, $parser, true);
		xml_parser_free($parser);
	}
	
	/**
	 * @param resource $parser
	 * @param string $tag
	 * @param array $attributes
	 */
	protected function onOpenTag($parser, $tag_, $attributes) {
		$tag = strtolower($tag_);
		$this->path[] = $tag;
		if (!$this->badBlock) {
			if ($this->lvl == 0 && $tag != 'vfpdata') { $this->badBlock = true; }
			if ($this->lvl == 1 && $tag == 'oee_plan') {
				$this->plan = array();
			}
			if ($this->lvl == 2 && $this->plan !== null) {
				$this->plan[$tag] = '';
			}
		}
			
		$this->lvl++;
	}
	
	/**
	 * @param resource $parser
	 * @param string $data
	 */
	protected function onCharData($parser, $data) {
		$tag = end($this->path);
		if (!$this->badBlock) {
			if ($this->lvl == 3 && $this->plan !== null) {
				$this->plan[$tag] .= $data;
			}
		}
	}
	
	/**
	 * @param resource $parser
	 * @param string $tag_
	 */
	protected function onCloseTag($parser, $tag_) {
		$tag = strtolower($tag_);
		array_pop($this->path);
		$this->lvl--;
		if (!$this->badBlock) {
			if ($this->lvl == 1 && $tag == 'oee_plan') {
				if (is_callable($this->callback)) {
				    $dataArray = array(
                        'line_id' => (isset($this->plan['line']) ? $this->plan['line'] : null),
                        'paste_code' => (isset($this->plan['dough']) ? $this->plan['dough'] : null),
                        'production_name' => (isset($this->plan['prod_name']) ? $this->plan['prod_name'] : null),
                        'production_code' => (isset($this->plan['prod']) ? $this->plan['prod'] : null),
                        'mo_number' => (isset($this->plan['mo']) ? intval($this->plan['mo']) : null),
                        'start' => $this->parseDateTime($this->plan, 'start_date', 'start_time'),
                        'end' => $this->parseDateTime($this->plan, 'end_date', 'end_time'),
                        'sensor_id' => (isset($this->plan['w_c']) ? $this->plan['w_c'] : null),
                        'branch_id' => (isset($this->plan['bakery']) ? $this->plan['bakery'] : null),
                        'line_quantity' => (isset($this->plan['qty_line']) ? intval($this->plan['qty_line']) : 0),
                        'box_quantity' => (isset($this->plan['qty_crate']) ? intval($this->plan['qty_crate']) : 0),
                        'package_quantity' => (isset($this->plan['qty_pack']) ? intval($this->plan['qty_pack']) : 0),
                        'quantity' => (isset($this->plan['plan']) ? floatval($this->plan['plan']) : 0),
                        'step' => (isset($this->plan['tact']) ? floatval($this->plan['tact']) : 0),
                        'report_no' => (isset($this->plan['report_no']) ? $this->plan['report_no'] : null),
                        'comment' => (isset($this->plan['comment']) ? $this->plan['comment'] : null),
                        'control' => (isset($this->plan['control']) ? $this->plan['control'] : null),
                        'weight' => (isset($this->plan['weight']) ? $this->plan['weight'] : 0),
                        'yield' => (isset($this->plan['yield']) ? $this->plan['yield'] : 0),
                        'qty_1' => (isset($this->plan['qty_1']) ? $this->plan['qty_1'] : 0),
                        'preparation_time' => (isset($this->plan['prep_time']) ? $this->plan['prep_time'] : 0),
                    );
                    $dataArray = array_merge($this->plan, $dataArray);
                    if(!$dataArray['preparation_time']){
                        unset($dataArray['preparation_time']);
                    }
					call_user_func($this->callback, $dataArray);
				}
				$this->plan = null;
			}
		}
	}
	
	/**
	 * Get date time from array data
	 * @param array $plan
	 * @param string $dateKey
	 * @param string $timeKey
	 * @return string date time in format 'Y-m-d H:i:s'
	 */
	private function parseDateTime(array $plan, $dateKey, $timeKey) {
		$date = (isset($plan[$dateKey]) ? $plan[$dateKey] : '00000000');
		$dateY = intval(substr($date, 0, 4));
		$dateM = intval(substr($date, 4, 2));
		$dateD = intval(substr($date, 6, 2));
		if ($dateY <= 0) return '#'.$plan[$dateKey].' '.$plan[$timeKey];
		if ($dateM <= 0 || $dateM > 12) return '#'.$plan[$dateKey].' '.$plan[$timeKey];
		if ($dateD <= 0 || $dateD > 31) return '#'.$plan[$dateKey].' '.$plan[$timeKey];
		$time = (isset($plan[$timeKey]) ? $plan[$timeKey] : '0000');
		$timeH = intval(substr($time, 0, 2));
		$timeM = intval(substr($time, 2, 2));
		if ($timeH < 0 || $timeH > 23) return '#'.$plan[$dateKey].' '.$plan[$timeKey];
		if ($timeM < 0 || $timeM > 59) return '#'.$plan[$dateKey].' '.$plan[$timeKey];
		$tstr = trim($date.'T'.sprintf('%02d%02d', $timeH, $timeM).'00');
		return date('Y-m-d H:i:s', strtotime($tstr));
	}
	
	/**
	 * @param string $file
	 * @param resource $parser
	 * @return ErrorException
	 */
	private function handleError($file, $parser, $freeParser = false) {
		$errCode = xml_get_error_code($parser);
		$errMsg = xml_error_string($errCode);
		return $this->handleCustomError($errMsg, $errCode, $file, $parser, $freeParser);
	}
	
	/**
	 * @param string $errMsg
	 * @param int $errCode
	 * @param string $file
	 * @param resource $parser
	 * @return ErrorException
	 */
	private function handleCustomError($errMsg, $errCode, $file, $parser, $freeParser = false) {
		$errLine = xml_get_current_line_number($parser);
		if ($freeParser) xml_parser_free($parser);
		return new ErrorException($errMsg, $errCode, E_ERROR, $file, $errLine);
	}
	
}
