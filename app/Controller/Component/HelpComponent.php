<?php
	class HelpComponent extends Component {
	    
        public $enabledPlugins = array();

        public function collecLineData($data){
            $newData = array();
            foreach($data as $record){
                $newData[0][] = $record[$this->name][key($record[$this->name])];
                if(!isset($record[0])) continue;
                foreach($record[0] as $namedParam => $value){
                    if(!isset($newData[1][$namedParam])) $newData[1][$namedParam] = array();
                    $newData[1][$namedParam][] = $value;
                }
            }
            return $newData;
        }
        
        public function createEvent(){
            return;
        }

        public function splitMulticolumnData($array,$timeGroup) {
            $default_array_keys = array('xLabel','ps_id','sum');
            $output_array = array();
            $i=0;
            foreach($array as $arr) {
                $arr_values = $arr[0];
                $arr_keys = array_keys($arr_values);
                $arr_keys_diff = array_diff($arr_keys,$default_array_keys);
                foreach($arr_keys_diff as $arr_key) {
                    $content = array(0=>array(
                        'xLabel'=>$arr_values['xLabel'],
                        $arr_key=>$arr_key,
                        'sum'=>$arr_values[$arr_key])
                    );
                    if($timeGroup == 0) {
                        $content[0]['xLabel'] = $arr_key;
                    }
                    if(array_key_exists('ps_id',$arr_values)) {
                        $content[0][0] = $arr_values['ps_id'];
                    }
                    if(array_key_exists('Shift',$arr)) {
                        $content['Shift'] = $arr['Shift'];
                    }
                    $output_array[$arr_key][] = $content;
//                    $output_array[$i][0]['xLabel'] = $arr_values['xLabel'];
//                    $output_array[$i][0]['ps_id'] = $arr_values['ps_id'];
//                    $output_array[$i][0][$arr_key] = $arr_values[$arr_key];
                    $i++;
                }
            }
            return $output_array;
        }

		public function collectData(&$data,$axis,$pareto,$valueColumn=0,$sumValues = true, $time_group=0){
			$collection = array();
			$causes = array(); //pr($data);
			$record_counts = array();
			foreach($data as $row){
				//surandame kas bus masyvu raktai
				if(is_array($axis['columns'])){
					$column = isset($row[$axis['columns'][0]][$axis['columns'][1]])? $row[$axis['columns'][0]][$axis['columns'][1]]:'';
				}else{
					$column = isset($row[$axis['columns']])?implode(' ',$row[$axis['columns']]):'';
				}
				if(is_array($axis['xAxis'])){
				    $xAxis = '';
				    foreach($axis['xAxis'] as $key => $columns){
				        foreach($columns as $value){
				            $xAxis .= $row[$key][$value];
				        }
				    }
				}else{
				    $xAxis = @implode(' ',$row[$axis['xAxis']]);
				}
                $xAxis = Settings::translate(trim($xAxis));
				$part1 = trim($column) && $axis['showType']=='multiple'?$column:0;
				$part1 = Settings::translate($part1);
				//if(!isset($collection[$part1][$xAxis])){
                if(array_key_exists('sum',$row[$valueColumn])) {
                    if(!isset($collection[$part1][$xAxis])){ $collection[$part1][$xAxis] = 0; }
                    $collection[$part1][$xAxis] += $row[$valueColumn]['sum'];
                } else {
                    if(array_key_exists('Prastovos',$row[$valueColumn])) {
                        if(!isset($collection[$part1][$xAxis])){ $collection[$part1][$xAxis] = 0; }
                        $collection[$part1][$xAxis] = $row[$valueColumn]['Prastovos'];
                    }
                }
				//}else{
				//	$collection[$part1][$xAxis]+=$row[$valueColumn]['sum'];
				//}
				
				if(isset($row[$valueColumn]['record_count'])){
					$record_counts[]=round($row[$valueColumn]['record_count']);
				}
				$causes[] = $xAxis;
			}
			$causes = array_unique($causes);
			$totalSum = 0;
            if($sumValues){
    			foreach($collection as $key => $data){
    				foreach($causes as $cause){
    					if(!array_key_exists($cause, $data)){
    						$collection[$key][$cause] = 0;
    					}else{
    						$totalSum += $collection[$key][$cause];
    					}
    				}
    				if($pareto && sizeof($collection)==1){
    					arsort($collection[$key]);
    				}else{
    					ksort($collection[$key]);
    				}
    			}
            }

			$paretoSum = 0;
			$paretoData = array();
			if($pareto && sizeof($collection)==1){
				foreach($collection[key($collection)] as $key => $data){
					$paretoSum += $data;
					$paretoData[] = round($paretoSum / $totalSum * 100);
				}
			}
            if($time_group > 0){
                $joiner = '';
                switch($time_group){
                    case 3: //savaites
                        $joiner = 'W';
                    break;
                    case 4: //menesiai
                        $joiner = 'm';
                    break;
                    case 5: //metai
                        $joiner = 'Y';
                    break;
                }
				if(trim($joiner) && $joiner != 'W'){
	                $collection2 = array();
	                foreach($collection as $node_name => $node_dates){
	                    foreach($node_dates as $date_key => $value){
	                    	if(!isset($collection2[$node_name][date($joiner, strtotime($date_key))])) $collection2[$node_name][date($joiner, strtotime($date_key))] = $value;
							else $collection2[$node_name][date($joiner, strtotime($date_key))] += $value;
	                    }
	                }
					$collection = $collection2;
                }


            }
			return array($collection,$paretoData,$record_counts);
		}

        public function strip_empty_elements($array){
            foreach($array as $key => $element){
                if((!is_array($element) && !trim($element)) || (is_array($element) && empty($element))){
                    unset($array[$key]);
                }
            }
            return $array;
        }

        public function collectSerializedData(&$model,$field,$data,$recordId){
            $order = $model->find('first',array('fields'=>array($model->name.'.'.$field), 'conditions'=>array($model->name.'.id'=>$recordId)));
            if(trim($order[$model->name][$field])){
                $unlockedData = unserialize($order[$model->name][$field]);
            }
            if(!empty($data)){
                $unlockedData[key($data)] = current($data);
                $model->id = $recordId;
                $model->save(array($field=>serialize($unlockedData)));
            }
        }

        public function csv_lt_encoder($text){
            $search = array('à','è','æ', 'ë', 'á', 'ð', 'ø', 'û', 'þ', 'À', 'È', 'Æ', 'Ë', 'Á', 'Ð', 'Ø', 'Û', 'Þ');
            $replace = array('ą','č','ę','ė','į','š','ų','ū','ž','Ą','Č','Ę','Ė','Į','Š','Ų','Ū','Ž');
            return str_replace($search,$replace,$text);
        }

		public static function getUrlFriendlyString($str){
		   // convert spaces to '-', remove characters that are not alphanumeric
		   // or a '-', combine multiple dashes (i.e., '---') into one dash '-'.
		   $str = ereg_replace("[-]+", "-", ereg_replace("[^a-z0-9-]", "",
		      strtolower( str_replace(" ", "-", $str) ) ) );
		   return $str;
		}

		public function setStartEndByPattern($primaryData){
            $start = $end = date('Y-m-d H:i');
            $pattern = $primaryData['time_pattern'];
            $sensors = $primaryData['sensors']??array();
            $conditions = array();
            if(!empty($sensors)){ $conditions = array('Sensor.id'=>$sensors);}
            $shiftModel = ClassRegistry::init('Shift');
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->bindModel(array('belongsTo'=>array('Branch')));
            $sensorData = $sensorModel->find('first', array('conditions'=>$conditions, 'recursive'=>-1));
            if(empty($sensorData)){ return array(0,0); }
            $shiftModel = ClassRegistry::init('Shift');
            $shiftSearchBy = 'start';
            $parameters = array(&$shiftSearchBy);
            $this->callPluginFunction('HelpComponent_beforeDatePatternFormation_Hook', $parameters, Configure::read('companyTitle'));
            switch($pattern){
                case 0: //praejusi pamaina
                    $lastShift = $shiftModel->find('first',array('conditions'=>array('Shift.end <'=>date('Y-m-d H:i'), 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.end DESC')));
                    if (empty($lastShift)) {
                        die('Paskutinė pamaina nerasta duomenų bazėje');
                    }
                    $start = $lastShift['Shift']['start'];
                    $end = $lastShift['Shift']['end'];
                break;
                case 1: //praejusi para
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d', strtotime('-1 day')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d', strtotime('-1 day')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m-d',strtotime('-1 day')).' 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }
                    if (empty($lastShift)) { $end = date('Y-m-d').' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 2: //praejusi savaite
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.date('Y-W',strtotime('-1 week')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.date('Y-W',strtotime('-1 week')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m-d',strtotime('last monday'.(strtolower(date('l'))=='monday'?'':'-7 day'))).' 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('+7 day',strtotime($start))).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }  
                break;
                case 3: //praejes menuo // sausi neveikia
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.date('Y-m',strtotime('-1 month')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.date('Y-m',strtotime('-1 month')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m',strtotime('-1 month')).'-01 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d', strtotime('first day of this month')).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 4: //praeje metai
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y\') = \''.date('Y',strtotime('-1 year')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y\') = \''.date('Y-m',strtotime('-1 year')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y',strtotime('-1 year')).'-01-01 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y').'-01-01 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 5: //siandien
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'], 'Shift.end <'=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m-d').' 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('+1 day')).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }  
                break;
                case 6: //si savaite
                	$intervalHasFullShiftEnded = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.date('Y-W').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'], 'Shift.end <='=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start')));
                    if(!empty($intervalHasFullShiftEnded)){ $interval = date('Y-W'); }else{ $interval = date('Y-W',strtotime('-1 week')); }
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.$interval.'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.$interval.'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'],'Shift.end <'=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m-d', (strtolower(date('l'))=='monday'?time():strtotime('last monday'))).' 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('+7 day',strtotime($start))).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }  
                break;
                case 7: //sis menuo
                	$intervalHasFullShiftEnded = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.date('Y-m').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'], 'Shift.end <='=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start')));
                    if(!empty($intervalHasFullShiftEnded)){ $interval = date('Y-m'); }else{ $interval = date('Y-m',strtotime('-1 month')); }
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.$interval.'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.$interval.'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'], 'Shift.end <'=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m').'-01 06:00:00'; }else{$start = $firstShift['Shift']['start'];}
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('first day of next month')).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 8: //sie metai
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y\') = \''.date('Y').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y\') = \''.date('Y').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id'], 'Shift.end <'=>date('Y-m-d H:i:s')), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) {  $start = date('Y').'-01-01 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = (date('Y')+1).'-01-01 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 9: //praejes menuo iki dabar
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m\') = \''.date('Y-m',strtotime('-1 month')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m',strtotime('-1 month')).'-01 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('+1 day')).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 10: //praejusi savaite iki dabar
                    $firstShift = $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%v\') = \''.date('Y-W',strtotime('-1 week')).'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start')));
                    $lastShift= $shiftModel->find('first',array('conditions'=>array('DATE_FORMAT(Shift.'.$shiftSearchBy.',\'%Y-%m-%d\') = \''.date('Y-m-d').'\'', 'Shift.branch_id'=>$sensorData['Sensor']['branch_id']), 'order'=>array('Shift.start DESC')));
                    if (empty($firstShift)) { $start = date('Y-m-d',strtotime('last monday'.(strtolower(date('l'))=='monday'?'':'-7 day'))).' 06:00:00'; }else{ $start = $firstShift['Shift']['start']; }             
                    if (empty($lastShift)) { $end = date('Y-m-d',strtotime('+1 day')).' 05:59:59'; }else{ $end = $lastShift['Shift']['end']; }
                break;
                case 11: //pnuo nurodytos datos iki dabar
                	$start = isset($primaryData['date_start']) && (int)$primaryData['date_start'] > 0?$primaryData['date_start']:$start;
                    $start = isset($primaryData['date_from']) && (int)$primaryData['date_from'] > 0?$primaryData['date_from']:$start;
                    $end = $end;
                break;
            }
            return array($start,$end);    
        }

        public function callPluginFunction($methodTitle, &$parameters, $companyName){
            //kviesime plugino modelyje esancia funkcija
            $companyPluginRoot = ROOT.DS.'app'.DS.'Plugin'.DS.$companyName;
            if(file_exists($companyPluginRoot.DS.'Model')){
                $extensionFiles = scandir($companyPluginRoot.DS.'Model');
                $extensionFiles = array_filter($extensionFiles, function($val){return strpos($val, '.') && !strpos($val, 'AppModel');});
                foreach($extensionFiles as $modelFile){
                    $companyMainPluginModel = ClassRegistry::init($companyName.'.'.str_replace('.php','',$modelFile));
                    if(method_exists($companyMainPluginModel,$methodTitle)) {
                        return call_user_func_array(array($companyMainPluginModel, $methodTitle), $parameters);
                    }
                }
            }
            //kviesime plugino controleryje esancia funkcija
            foreach (CakePlugin::loaded() as $plugin) {
                if(!isset($this->enabledPlugins->$plugin) || $plugin != $companyName) continue;
                App::uses('PluginsController', $plugin.'.Controller');
                $contr = 'PluginsController';
                if(!class_exists($contr)) continue;
                $company = new $contr();
                if(method_exists($company,$methodTitle)) {
                    //$appendedRoles = $company->afterManualOrderPositioning($ordersSort['order']);
                    return call_user_func_array(array($company, $methodTitle), $parameters);
                }
            }           
        }

	}
