<?php
class GooglechartComponent extends Component {
    
    private static $colors = array('#fa768b', '#4734fc', '#2db14c', '#a5a4a6', '#ff0000', '#c34cfb', '#27868e', '#5efd8b', '#98a432', '#060606', '#b435a1', '#5a6d91', '#5c876f', '#aaa687', '#d67515', '#7fd614', '#13d6bf', '#d6129b', '#cfd614', '#aa314b', '#7731a9', '#31a87c', '#dbf727', '#38a830', '#98a830');
    private $decimalCount = 2;
    
    public function constructGraph($options,$lineParameters){
        if(!isset($options['selectedInfo']['uid'])){ echo __('Atnaujinkite grafiko nustatymus');return; }
        $colorsBase = self::$colors;
        if(isset($options['selectedInfo']['trendline']) && $options['selectedInfo']['trendline']){
            array_splice($colorsBase,sizeof($options['data']),0,array_slice($colorsBase,0,sizeof($options['data'])));
        }
        $columnsColors = "colors: ['".implode("','", $colorsBase)."'],";
        $legendColors = array();
        if(isset($options['selectedInfo']['decimal_count'])){
            $this->decimalCount = (int)$options['selectedInfo']['decimal_count'];
        }
        if(false && sizeof($options['data']) == 1){ //suminis grupavimas arba tik viena grupine reiksme(nera kolonu grupiu)
            /*$key = key($options['data']);
            $chart = "var data = new google.visualization.DataTable(); data.addColumn('string', 'X');";
            $legend = key($options['data']);
            $chart .= "data.addColumn('number','$legend'); data.addColumn({type:'string',role:'tooltip'}); data.addColumn({type: 'number', role: 'annotation'}); data.addColumn({type: 'string', role: 'style'});";
            $preparedData = current($options['data']);
            if(isset($options['selectedInfo']['order_columns_desc']) && $options['selectedInfo']['order_columns_desc']){
                arsort($preparedData);
            }
            $this->assignColorsForSpecificGraphs($preparedData,$options,$columnsColors,$legendColors);
            //$options['data'] = array($key=>$preparedData);
            list($trendData, $trendSeriesStr) = $this->prepareTrendData($options, $chart);
            $chart .= "data.addRows([";
            foreach($preparedData as $title => $value){
                //$title = utf8_decode($title);
                $trendValues = array();
                $value = round($value, $this->decimalCount);
                $color = isset($legendColors[$title])?$legendColors[$title]:current($colorsBase);
                $chart .= "['$title', parseFloat('$value'), '$title: '+parseFloat('$value').toFixed(2), parseFloat('$value'), 'color: $color',";
                if(isset($trendData[$key][$title])){
                     $trendValues[] = $trendData[$key][$title];
                     $trendValues[] = "'color: $color'";
                }
                $chart .= (implode(',', $trendValues)).'],';
            }
            $chart .= "]);";
            $legend = $legend == 0?'false':"{ position: 'top', maxLines: 4 }";*/
        }else{
            $i = 0; $series = array();
            if(isset($options['selectedInfo']['order_columns_desc']) && $options['selectedInfo']['order_columns_desc'] && sizeof($options['data']) == 1){
                arsort($options['data'][key($options['data'])]);
            }
            foreach($options['data'] as $title => $values){
                $title = ($title);
                $color = isset($colorsBase[$i])?$colorsBase[$i++]:'#ffffff';
                $legendColors[$title] = $color;
                foreach($values as $key=>$value){
                    $series[] = $key;
                }
            }
            $this->assignColorsForSpecificGraphs($options['data'],$options,$columnsColors,$legendColors);
            $series = array_unique($series);
            if($options['selectedInfo']['time_group'] > 0 && (!isset($options['selectedInfo']['order_columns_desc']) || !$options['selectedInfo']['order_columns_desc'])){
                sort($series);
            }
            $chart = "var data = new google.visualization.DataTable(); data.addColumn('string', 'Title');";
            foreach(array_keys($options['data']) as $title){
                $title = ($title);
                $chart .= "data.addColumn('number', '$title'); data.addColumn({type:'string',role:'tooltip'});  data.addColumn({type: 'string', role: 'annotation'}); data.addColumn({type: 'string', role: 'style'}); ";
            }
            list($trendData, $trendSeriesStr) = $this->prepareTrendData($options, $chart);
            $chart .= "data.addRows([";
            foreach($series as $key){
                $chart .= "['$key'";
                $preparedData = array();
                foreach($options['data'] as $title => $data){
                     $value = array_key_exists($key, $data)?$data[$key]:(is_array($data)?0:$data);
                     $value = !$value?0:$value;
                     $preparedData[$title] = round($value, $this->decimalCount);
                }
                if(isset($options['selectedInfo']['order_columns_desc']) && $options['selectedInfo']['order_columns_desc']){
                    arsort($preparedData);
                }
                $trendValues = [];
                foreach($preparedData as $title => $value){
                     if(isset($legendColors[$title])){ $color = $legendColors[$title]; }
                     elseif(isset($legendColors[$key])){ $color = $legendColors[$key]; }
                     else{ $color = current($colorsBase); }
                     $title = ($title);
                     $tooltip = '\''.$title.': '.round($value, $this->decimalCount).'\r\n'.$key.'\'';
                     $chart .= ", parseFloat('$value'), $tooltip, String($value), 'color: $color'";
                     if(isset($trendData[$title][$key])){
                         $trendValues[] = $trendData[$title][$key];
                         $trendValues[] = "'color: {$legendColors[$title]}'";
                     }elseif(!empty($trendData)){
                         $trendValues[] = 0;
                         $trendValues[] = "'color: #ffffff'";
                     }
                }
                $chart .= ','.(implode(',', $trendValues)).'],';
                //fb($chart);die();
            }
            $chart .= "]);";
            //fb($chart);die();
            $legend = isset($options['data'][0])?'\'\'':"{ position: 'top', maxLines: 4 }";
        }
        $subType = 'bars';
        switch($options['selectedInfo']['graph_type']){
            case 0: $graphType = 'ComboChart'; $subType = 'line';  break; 
            case 2: $graphType = 'PieChart'; $subType = ''; $legend = '{position: \'labeled\', textStyle: {fontSize: 12}}'; break; 
            default: $graphType = 'ComboChart'; break; 
        }
        $title = str_replace(array("\r", "\n"), ' ', $options['graph_title']);
        $chart .= "var options = {
              legend: $legend,
              title: '$title',
              vAxis: {
                  gridlines: {color: '#eee', count: 10},
                  textStyle: {fontSize: 12},
                  titleFontSize: 14,
                  position: 'left',
                  title: '{$options['y_legend']}',
                  minValue: 0
              },
              hAxis: {
                  gridlines: {color: '#eee', count: 13},
                  format: 'yyyy-MM-dd hh:mm',
                  textStyle: {fontSize: 10},
                  titleTextStyle: {fontSize: 14},
                  direction:1, slantedText:true, slantedTextAngle:60,
                  //type: 'category'
              },
              seriesType: '$subType',
              $trendSeriesStr
              $columnsColors
              chartArea:{".($graphType=='PieChart'?"left:10,top:50,right: 0,width:'86%',height:'45%',bottom:0":"left:60,top:50,right: 0,width:'86%',height:'45%'")."},
              annotations: {
                  textStyle: {
                      fontSize: 13,
                  },alwaysOutside: true
              },
              sliceVisibilityThreshold: 0
            };
            var containerId = 'googlechartContainer{$options['selectedInfo']['uid']}';
            var chart = new google.visualization.$graphType(document.getElementById(containerId));
            if(returnImage){
                google.visualization.events.addListener(chart, 'ready', function (){
                     startExport{$options['selectedInfo']['uid']}(chart.getImageURI());
                });
            }
            chart.draw(data, options);
            ";  
        return json_encode(array($chart));
    }
    
    public function assignColorsForSpecificGraphs($preparedData,$options,&$columnsColors,&$legendColors){
    	$colorsList = isset($options['colors'])?$options['colors']:array();	
    	if(empty($colorsList)){ return; }
        static $problemsList = array(), $problemsExclusions = array();
        if(empty($problemsList)){
            $problemsModel = ClassRegistry::init('Problem');
            $exclusionModel = ClassRegistry::init('ShiftTimeProblemExclusion');
            $problemsModel->virtualFields = array('name'=>'TRIM(name)');
            $problemsList = $problemsModel->find('list', array('fields'=>array('id','name')));
            foreach($problemsList as &$problem){
                $problem = Settings::translate($problem);
            }
            $problemsExclusions = $exclusionModel->find('list', array('fields'=>array('id','problem_id')));
        }
        //if($options['selectedInfo']['indicator1'] == 18){
        $legendColors = array();
        $preparedData = isset($preparedData[0])?$preparedData[0]:$preparedData;
        foreach($preparedData as $title => $value){
            //$title = utf8_decode($title);
            $problemId = array_search(Settings::translate(trim($title)),$problemsList);
            $title = Settings::translate(trim($title));
            $color = isset($colorsList[$title])?$colorsList[$title]:(isset($colorsList[''])?$colorsList['']:'#fa768b');
            if(in_array($problemId, $problemsExclusions) && $problemId >= 3){ $color = '#647ff5'; }
            $legendColors[$title] = $color;
        }
        if($legendColors){
            $columnsColors = "colors: ['".implode("','", $legendColors)."'],";
        }
    }

    private function prepareTrendData($options, &$chart){
        if(!isset($options['selectedInfo']['trendline']) || !$options['selectedInfo']['trendline']) return array(array(),'');
        $data = $options['data'];
        foreach($data as &$dg){
            if(isset($options['selectedInfo']['order_columns_desc']) && $options['selectedInfo']['order_columns_desc'] && sizeof($options['data']) == 1){
                arsort($dg);
            }
            $dataGroup = array_combine(range(1,sizeof($dg)), $dg);
            $keyAvg = array_sum(array_keys($dataGroup)) / sizeof($dataGroup);
            $valAvg = array_sum($dataGroup) / sizeof($dataGroup);
            $square = 0;
            foreach($dataGroup as $key => &$value){
                $value = ($key-$keyAvg) * ($value - $valAvg);
                $square += pow(($key-$keyAvg), 2);
            }
            $m = $square > 0 ? array_sum($dataGroup) / $square : 0;
            $b = $valAvg - ($m * $keyAvg);
            $i = 1;
            foreach($dg as &$dgValue){
                $dgValue = round($m*($i++) + $b, $this->decimalCount);
            }
        }
        $i = sizeof($data);
        $series = 'series: {';
        foreach($data as $title => $d){
            $chart .= "data.addColumn('number','$title ".__('trend')."'); data.addColumn({type: 'string', role: 'style'}); "; 
            $series .= $i++.": {type: 'line', lineDashStyle: [1, 4]},";
        }
        $series .= '},';
        return array($data, $series);
    }
    
}
        