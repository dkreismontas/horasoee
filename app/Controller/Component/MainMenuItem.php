<?php

/**
 * Description of MainMenuItem
 */
class MainMenuItem { 
	
	/** @var string */
	private $title;
	/** @var string */
	private $description;
	/** @var string */
	private $url;
	/** @var boolean */
	private $isSelected;
	/** @var string */
	private $icon;
	/** @var MainMenuItem[] */
	public $items;
	public $target;
	
	/**
	 * 
	 * @param string $title
	 * @param string $url
	 * @param boolean $isSelected
	 * @param string $icon
	 * @param string $description
	 */
	public function __construct($title, $url, $isSelected = false, $icon = null, $description = null, $items = null, $target=null) {
		$this->title = $title;
		$this->url = $url;
		$this->isSelected = ($isSelected ? true : false);
		$this->icon = $icon;
		$this->description = $description;
		$this->items = ($items && is_array($items)) ? $items : array();
		$this->target = $target;
	}
	
	/** @return boolean */
	public function hasItems() {
		return !empty($this->items);
	}
	
	/** @return MainMenuItems[] */
	public function getItems() {
		return $this->items;
	}
	
	/** @return string */
	public function getTitle() {
		return $this->title;
	}

	/** @return string */
	public function getUrl() {
		return $this->url;
	}

	/** @return boolean */
	public function getIsSelected() {
		return $this->isSelected;
	}

	/** @return string */
	public function getIcon() {
		return $this->icon;
	}
	
	/** @return string */
	public function getDescription() {
		return $this->description;
	}
	
	/** @param string $title */
	public function setTitle($title) {
		$this->title = $title;
	}

	/** @param string $url */
	public function setUrl($url) {
		$this->url = $url;
	}

	/** @param boolean $isSelected */
	public function setIsSelected($isSelected) {
		$this->isSelected = $isSelected;
	}

	/** @param string $icon */
	public function setIcon($icon) {
		$this->icon = $icon;
	}
	
	/** @param string $description */
	public function setDescription($description) {
		$this->description = $description;
	}
	
}
