<?php

App::uses('Plan', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('Loss', 'Model');
App::uses('LossType', 'Model');
App::uses('Branch', 'Model');

/**
 * Description of Exporter
 */
class Exporter {
    
    private static function getExportPath($createIfNotExists = false) {
        $settingsModel = ClassRegistry::init('Settings');
        $path = $settingsModel->getOne('plan_export_path');
        if(!trim($path)){
            $path = ROOT.DS.APP_DIR.DS.'external'.DS.'out';
        }
        if ($createIfNotExists && !is_dir($path)){
            $old = umask(0);
            mkdir($path, 0755);
            umask($old);
        }
        return $path;
    }

    public static function exportPostphoned($ord) {
        $date = new DateTime();
        $filePath = self::getExportPath(true).DS.'HALFRPT_'.$date->format('Ymd').'_'.$date->format('His').$ord[Plan::NAME]['mo_number'].'.txt';
        //var_dump($filePath);
        $data = ''
                .$ord[Branch::NAME]['ps_id'].';'
                .$ord[Plan::NAME]['production_code'].';'
                .$ord[Plan::NAME]['mo_number'].';'
                .$ord[Plan::NAME]['report_no'].';'
                .($ord[ApprovedOrder::NAME]['confirmed_quantity']-$ord[ApprovedOrder::NAME]['exported_quantity']).';'
                .number_format(((strtotime($ord[ApprovedOrder::NAME]['end']) - strtotime($ord[ApprovedOrder::NAME]['created'])) / 3600), 2, '.', '').';'
                .$date->format('Ymd').';'
                .'0'."\r\n";
        file_put_contents($filePath, $data);
    }

    public static function testPluginComponent() {
        if(CakePlugin::loaded('ExporterExportCompletedOverride')) {
            App::import('Component', 'ExporterExportCompletedOverride.Exporter');
            ExporterComponent::test();
//            $this->Exporter->test();
        }
    }
    
    /**
     * Tada kai pamainos vadovas patvirtina deziu kiekius
     * @param array $ord
     */
    public static function exportCompleted($ord,$type='csv') {

        if(CakePlugin::loaded('ExporterExportCompletedOverride')) {
            App::import('Component', 'ExporterExportCompletedOverride.Exporter');
            ExporterComponent::exportCompleted($ord,self::getExportPath(true),$type);
            return;
        }

        $date = new DateTime();
        $filePath = self::getExportPath(true).DS.'MORPT_'.$date->format('Ymd').'_'.$date->format('His').$ord[Plan::NAME]['mo_number'].'.txt';

        $production_code_value = $ord[Plan::NAME]['production_code'];
        $confirmed_quantity_value = $ord[ApprovedOrder::NAME]['confirmed_quantity'];

        $data = ''
                .$ord[Branch::NAME]['ps_id'].';'
                .$production_code_value.';' // Rodyti paste_code jei maiymo
                .$ord[Plan::NAME]['mo_number'].';'
                .$ord[Plan::NAME]['report_no'].';'
                .$confirmed_quantity_value.';' // Padauginti i yield'o jei maiymo
                .number_format(((strtotime($ord[ApprovedOrder::NAME]['end']) - strtotime($ord[ApprovedOrder::NAME]['created'])) / 3600), 2, '.', '').';'
                .$date->format('Ymd').';'
                .'1'."\r\n";
        file_put_contents($filePath, $data);
    }
    
    public static function exportLosses($ord,$loss_code,$loss_value,$addSeconds) {
        $date = new DateTime();
        if($addSeconds != 0) {
            $date->add(new DateInterval('PT'.$addSeconds.'S'));
        }
        $filePath = self::getExportPath(true).DS.'ORDLESS_'.$ord[Plan::NAME]['mo_number'].'_'.$date->format('Ymd').'_'.$date->format('His').'.txt';
        $data = ''
                .$ord[Branch::NAME]['ps_id'].';'
                .$loss_code.';' // broko kodas
                .number_format(floatval($loss_value), 2, '.', '').';' // broko kiekis
                .$date->format('Ymd').';'."\r\n";
        file_put_contents($filePath, $data);
    }
    
}