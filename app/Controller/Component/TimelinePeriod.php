<?php

App::uses('Plan', 'Model');
App::uses('Problem', 'Model');

/**
 * Description of TimelinePeriod
 */
class TimelinePeriod {
    
    const TYPE_NONE = 0;
    const TYPE_PRODUCT = 1;
    const TYPE_PROBLEM = 2;
    const TYPE_TRANSITION = 3;
    const TYPE_UNDEFINED = 4;
    const TYPE_PRINTING = 5;
    const TYPE_SHUTDOWN = 6;
    const TYPE_PRODUCT_SLOW_SPEED = 7;
    const TYPE_SHORT_PROBLEM = 8; //kai problema trunka labai trumpa laika (laikas nurodomas per DC nustatyma)
    const TYPE_PROBLEM_EXCLUSIONS = 9;
    const TYPE_PLAN = 10;
    
    /** @var int one of TYPE_* constants */
    public $type;
    /** @var int start time in sec. fron begining of hour */
    public $start;
    /** @var int end time in sec. fron begining of hour */
    public $end;
    /** @var string */
    public $title;
    /** @var string */
    public $shortTitle = null;
    /** @var int */
    public $foundProblemId = null;
    public $problemId = null;
    /** @var string */
    public $dateTime = null;
    /** @var boolean */
    public $whileWorking = false;
    /** @var array */
    public $data;
    /** @var DateTime */
    public $dtStart;
    /** @var DateTime */
    public $dtEnd;
    public $shiftId;
    public $json;
    public $intervalQuantity = 0;
    
    /**
     * @param int $type one of TYPE_* constants
     * @param DateTime $start start time
     * @param DateTime $end end time
     * @param array $data
     * @param boolean $whileWorking
     */
    public function __construct($type, DateTime $start, DateTime $end, array $data = null, $whileWorking = false) {
        $this->type = $type;
        $this->dtStart = $start;
        $this->dtEnd = $end;
        if(isset($data['Record'])){
            $this->foundProblemId = $data['Record']['found_problem_id'];
            $this->problemId = $data['FoundProblem']['problem_id'];
            $this->shiftId = $data['Record']['shift_id'];
        }else{
            $this->data = $data;
            if($data != null && array_key_exists('FoundProblem',$data)) {
                $this->foundProblemId = $data['FoundProblem']['id'];
                $this->problemId = $data['FoundProblem']['problem_id'];
                $this->json = json_decode($data['FoundProblem']['json']);
                $this->shiftId = $data['FoundProblem']['shift_id'];
            }
        }
        if ($this->type == self::TYPE_TRANSITION) {
            $this->whileWorking = ($whileWorking ? true : false);
        }
    }
    
    /** @return DateTime */
    public function getDateStart() {
        return $this->dtStart;
    }

    /** @return DateTime */
    public function getDateEnd() {
        return $this->dtEnd;
    }
    
    /**
     * Test if eaquals by type
     * @param TimelinePeriod $p
     * @return boolean
     */
    public function equals(TimelinePeriod $p) {
        if (isset($this->data['FoundProblem']) && isset($p->data['FoundProblem']) && $this->data['FoundProblem']['transition_problem_id'] != $p->data['FoundProblem']['transition_problem_id']) return false;
        if (isset($this->data['FoundProblem']) && isset($p->data['FoundProblem']) && $this->data['FoundProblem']['shift_id'] != $p->data['FoundProblem']['shift_id']) return false;
        if ($this->type != $p->type) return false;
        if ($this->type != $p->type) return false;
        if ($this->whileWorking != $p->whileWorking) return false;
        if ((!$this->data && $p->data) || ($this->data && !$p->data)) return false;
        if ($this->data && $p->data) {
            if (isset($this->data['Plan']['id']) && isset($p->data['Plan']['id']) && $this->data['Plan']['id'] != $p->data['Plan']['id']) {
                return false;   
            }
            if (in_array($this->type,array(self::TYPE_PROBLEM,self::TYPE_PROBLEM_EXCLUSIONS)) && $this->data[Problem::NAME]['id'] != $p->data[Problem::NAME]['id']) {
                return false;
            } else if (in_array($this->type, array(self::TYPE_PRODUCT) || preg_match('/production_speed/', $this->type)) && $this->data[Plan::NAME]['id'] != $p->data[Plan::NAME]['id']) {
                return false;
            }
        }
        return true;
    }
        
    /**
     * Combine periods with period using binary operation
     * @param TimelinePeriod[] $periods
     * @param TimelinePeriod $period
     */
    public static function binaryIntersect(array &$periods, TimelinePeriod $period, $test = false) {
        if (!$period || $period->dtStart >= $period->dtEnd) {
            return;
        }
        $ps = array();
        $di = new DateInterval('PT1S');
        $open = false; $close = false; $lastp = null;
        //$period->dtEnd->sub($di);
        foreach ($periods as $p) {
            if ($p->dtStart >= $p->dtEnd){ continue; }
            if (!$lastp && !$open && $p->dtStart > $period->dtStart && $p->dtStart < $period->dtEnd) {
                $period->dtStart = clone $p->dtStart;
                $open = true;
            }
            if (!$open && $p->dtStart <= $period->dtStart && $p->dtEnd >= $period->dtStart) {
                $a = clone $p;
                $a->dtEnd = clone $period->dtStart; $a->dtEnd->sub($di);
                $ps[] = $a;
                $open = true;
                $p->dtStart = clone $period->dtStart;
            }
            if ($open && !$close && $p->dtStart <= $period->dtEnd && $p->dtEnd >= $period->dtEnd) {
                $ps[] = $period;
                $p->dtStart = clone $period->dtEnd; $p->dtStart->add($di);
                $close = true;
            }
            if (!$open || $close) {
                $ps[] = $p;
            }
            $lastp = $p;
        }
        if ($open && !$close && $lastp) {
            $period->dtEnd = clone $lastp->dtEnd;
            $ps[] = $period;
        }
        //array_splice($periods, 0, count($periods));
        $periods = array();
        foreach ($ps as $p) {
            $periods[] = $p;
        }
    }
    
    /**
     * Slice of from begining a datetime
     * @param TimelinePeriod[] $periods
     * @param DateTime $dateTime
     * @param DateTime $refDateTime
     * @return TimelinePeriod[] sliced part
     */
    public static function binarySlice(array &$periods, DateTime $dateTime, DateTime $refDateTime) {
        $ps = array();
        $di = new DateInterval('PT1S');
        for ($i = 0, $c = count($periods); $i < $c; $i++) {
            /* @var $p TimelinePeriod */
            $p = reset($periods);
            /* @var $diff DateInterval */
            $diff = $p->dtEnd->diff($p->dtStart, false);
            $dissSec = ($diff->days * 3600 * 24) + ($diff->h * 3600) + ($diff->i * 60) + $diff->s;
            if ($dissSec <= 19 && ($p->type === 0 || $p->whileWorking || (isset($p->data[FoundProblem::NAME]['while_working']) && $p->data[FoundProblem::NAME]['while_working']))) { array_shift($periods); continue; }
            if ($p->dtEnd >= $dateTime) {
                $a = clone $p;
                //$a->full_interval = array('dtStart'=>$a->dtStart,'dtEnd'=>$a->dtEnd);
                $a->dtEnd = clone $dateTime;
                if ($a->dtStart->format('Y-m-d H:i:s') < $a->dtEnd->format('Y-m-d H:i:s')) $ps[] = $a;
                $p->dtStart = clone $dateTime; $p->dtStart->add($di);
                break;
            } else {
                $a = array_shift($periods);
                if ($a->dtStart->format('Y-m-d H:i:s') < $a->dtEnd->format('Y-m-d H:i:s')) $ps[] = $a;
            }
        }
        for ($i = 0, $c = count($periods); $i < $c; $i++) {
            /* @var $p TimelinePeriod */
            $p = reset($periods);
            if (($p->dtEnd->getTimestamp() - $p->dtStart->getTimestamp()) < 60 && $p->type === 0) {
                 array_shift($periods); continue; }
            if (($p->dtEnd->getTimestamp() - $p->dtStart->getTimestamp()) <= 0) { array_shift($periods); continue; }
            break;
        }
        if (empty($periods)) {
            $a = end($ps);
            if ($a && ($a->dtEnd->getTimestamp() - $a->dtStart->getTimestamp()) < 60 && ($a->type === self::TYPE_PROBLEM || $a->type === self::TYPE_TRANSITION) && !$a->data) {
                array_pop($ps);
                $a = end($ps);
                if ($a && $refDateTime && ($refDateTime->getTimestamp() - $a->dtEnd->getTimestamp()) > 0 && $a->type === self::TYPE_PRODUCT) {
                    $a->dtEnd = clone $refDateTime;
                }
                reset($ps);
            }
        }
        return $ps;
    }
    
    /**
     * Fill spaces between periods by extending or merging them
     * @param TimelinePeriod[] $periods
     * @param DateTime $refDateTime
     */
    public static function conectSpaces(array &$periods, DateTime $refDateTime = null) {
        $dis = new DateInterval('PT1S');
        /* @var $lperiod TimelinePeriod */
        $lperiod = null;
        for ($i = (count($periods) - 1); $i >= 0; $i--) {
            if ($lperiod && ($lperiod->dtStart->getTimestamp() - $periods[$i]->dtEnd->getTimestamp()) > 1) {
                $periods[$i]->dtEnd = clone $lperiod->dtStart;
                $periods[$i]->dtEnd->sub($dis);
            }
            if ($lperiod && $lperiod->equals($periods[$i])) {
                $periods[$i]->dtEnd = clone $lperiod->dtEnd;
                array_splice($periods, $i + 1, 1);
            }
            $lperiod = $periods[$i];
        }
        if ($refDateTime) {
            $a = end($periods);
            if ($a && $refDateTime && ($refDateTime->getTimestamp() - $a->dtEnd->getTimestamp()) > 0 && $a->type == self::TYPE_PRODUCT) {
                $a->dtEnd = clone $refDateTime;
            }
        }
    }
    
    public function buildData() {
        static $ApprovedOrder = null,$settingModel = null,$problemModel = null; 
        $this->title = '';
        if($settingModel == null){ $settingModel = ClassRegistry::init('Settings'); }
        if($ApprovedOrder == null){ $ApprovedOrder = ClassRegistry::init('ApprovedOrder'); }
        if($problemModel == null){ $problemModel = ClassRegistry::init('Problem'); }
        $exceededTransitionId = $settingModel->getOne('exceeded_transition_id');
        $parentProblemsList = $problemModel->find('list', array('fields'=>array('Problem.id','Problem.name'),'conditions'=>array('Problem.parent_id'=>0)));
        $pluginModel = ClassRegistry::init(Configure::read('companyTitle'));
        if (in_array($this->type, array(self::TYPE_PROBLEM, self::TYPE_UNDEFINED, self::TYPE_SHUTDOWN, self::TYPE_SHORT_PROBLEM, self::TYPE_PROBLEM_EXCLUSIONS))) {
//            CakeLog::write('debug', 'Period:' . json_encode($this->data));
            if ($this->data) {
                $productCode = isset($this->data[Plan::NAME]) && $exceededTransitionId > 0?$this->data[Plan::NAME]['production_code']:'';
                $productCode = $exceededTransitionId != $this->data[FoundProblem::NAME]['problem_id']?'':' '.$productCode;
                if ($this->data[Problem::NAME]['id'] == Problem::ID_NEUTRAL) $this->type = self::TYPE_TRANSITION;
                if ($this->data[Problem::NAME]['id'] == Problem::ID_NO_WORK) $this->type = self::TYPE_NONE;
                if ($this->type != self::TYPE_TRANSITION || !$this->data[FoundProblem::NAME]['while_working']){
                    $this->title .= Settings::translate($this->data[Problem::NAME]['name']).$productCode;
                    if(isset($this->data['Transition'])){
                        $this->title .= '<br />'.Settings::translate($this->data['Transition']['name']);
                    }
                }
                if(isset($this->data[Plan::NAME])){
                    if(method_exists($pluginModel, 'setTimeGraphHoverProduct')){
                        $this->title .= $pluginModel->setTimeGraphHoverProduct($this);
                    }else{
                        $this->title .= '<br/><b>'.__('Gaminys').'</b>: '.$this->data[Plan::NAME]['production_code'].' ('.$this->data[Plan::NAME]['production_name'].')';
                    }
                    $this->title .= '<br/><b>'.__('Užsakymo nr.').'</b>: '.$this->data[Plan::NAME]['mo_number'];
                    if($this->type == self::TYPE_TRANSITION || $this->data['FoundProblem']['problem_id'] == $exceededTransitionId){
                        $this->title .= '<br/><b>'.__('Derinimo norma').'</b>: '.$this->data[Plan::NAME]['preparation_time'].' '.__('min.');
                    }
                }
                $this->title .= '<br /><b>'.__('Komentaras').': </b>'.(isset($this->data[FoundProblem::NAME]['comments'])?$this->data[FoundProblem::NAME]['comments']:'');
                if($this->data[FoundProblem::NAME]['super_parent_problem_id'] > 0 && isset($parentProblemsList[$this->data[FoundProblem::NAME]['super_parent_problem_id']])){
                    $this->title .= '<br /><b>'.__('Tėvinė prastova').': </b>'.Settings::translate($parentProblemsList[$this->data[FoundProblem::NAME]['super_parent_problem_id']]);
                }
                $adtStart = new DateTime($this->data[FoundProblem::NAME]['start']);
                $adtEnd = new DateTime($this->data[FoundProblem::NAME]['end']);
                //$this->title .= '<br/>['.$adtStart->format('Y-m-d H:i').' - '.$adtEnd->format('Y-m-d H:i').']';
                $this->foundProblemId = $this->data[FoundProblem::NAME]['id'];
                $this->dateTime = $adtStart->format('c');
                
                if($this->data[Problem::NAME]['id'] == Problem::ID_NEUTRAL){
                    $productCode = isset($this->data[Plan::NAME]) && $exceededTransitionId > 0?$this->data[Plan::NAME]['production_code']:'';
                    $this->shortTitle = isset($this->data['Transition'])?Settings::translate($this->data['Transition']['name']):Settings::translate($this->data[Problem::NAME]['name']).$productCode;
                }else{
                    $this->shortTitle = (isset($this->data['Transition'])?Settings::translate($this->data['Transition']['name']):Settings::translate($this->data[Problem::NAME]['name'])).$productCode;
                }
                $this->whileWorking = ($this->data[FoundProblem::NAME]['while_working'] ? true : false);
            } else {
                $this->dateTime = $this->dtStart->format('c');
            }
        } else if (in_array($this->type, array(self::TYPE_PRODUCT, self::TYPE_PRINTING, self::TYPE_PRODUCT_SLOW_SPEED, self::TYPE_PLAN)) || preg_match('/production_speed/', $this->type)) {
            if ($this->data) {
                $this->title .= '<b>'.__('Gamyba').'</b>';
                if(method_exists($pluginModel, 'setTimeGraphHoverProduct')){
                    $this->title .= $pluginModel->setTimeGraphHoverProduct($this);
                }else{
                    $this->title .= '<br/><b>'.__('Gaminys').'</b>: '.$this->data[Plan::NAME]['production_code'].' ('.$this->data[Plan::NAME]['production_name'].')';
                    $this->title .= '<br/><b>'.__('Užsakymo nr.').'</b>: '.$this->data[Plan::NAME]['mo_number'];
                    if(isset($this->data[ApprovedOrder::NAME])) {
                        $relatedCount = $ApprovedOrder->getRelatedApprovedOrders($this->data[Plan::NAME]['id'], $this->data[ApprovedOrder::NAME]['id']);             
                        $this->title .= '<br/><b>' . __('Pagaminta').':</b> '. ($this->data[ApprovedOrder::NAME]['quantity'] + $relatedCount).' '.__('vnt.').($this->data[Plan::NAME]['quantity']>0?' '.__('iš').' '.$this->data[Plan::NAME]['quantity']:'') ;
                        //$this->title .= '<br/><b>' . __('Intervalo kiekis').':</b> '. $this->intervalQuantity;
                        $this->title .= '<br/><b>' . __('Pažymėto intervalo kiekis').':</b> '. round($this->intervalQuantity,2).' '.__('vnt.');
						if(isset($this->data['Work']) && !empty($this->data['Work'])){
						    $multiplier = $this->data['Sensor']['plan_relate_quantity'] == 'quantity'?1:$this->data['Plan']['box_quantity'];
                        	$this->title .= '<br/><b>' . __('Viso intervalo kiekis').':</b> '.bcmul($this->data['Work']['quantity'],$multiplier,2).' '.__('vnt.');
                            $this->title .= '<br/><b>' . __('Komentaras').':</b> '.$this->data['Work']['comments'];
                        }
                        if($this->data[Plan::NAME]['quantity'] > 0){
                            $this->title .= '<br/><b>' . __('Likęs kiekis').':</b> '.($this->data[Plan::NAME]['quantity'] - $this->data[ApprovedOrder::NAME]['quantity'] - $relatedCount);
                        }
                        if($relatedCount > 0){
                            $this->title .= '<br/><b>' . __('Nuo paskutinio atidėjimo pagaminta').':</b> '.$relatedCount;
                        }
                    }
                }
                $adtStart = new DateTime(isset($this->data[ApprovedOrder::NAME]['created']) ? $this->data[ApprovedOrder::NAME]['created'] : $this->data[Plan::NAME]['start']);
                $adtEnd = new DateTime(isset($this->data[ApprovedOrder::NAME]['end']) ? $this->data[ApprovedOrder::NAME]['end'] : $this->data[Plan::NAME]['end']);
                //$this->title .= '<br/>['.$adtStart->format('Y-m-d H:i').' - '.$adtEnd->format('Y-m-d H:i').']';
                $this->dateTime = $adtStart->format('c');
                $diff = $this->dtStart->diff($this->dtEnd);
                if($diff->format('%i') >= 3){
                    $this->shortTitle = trim($this->data[Plan::NAME]['production_name'])?$this->data[Plan::NAME]['production_name']:$this->data[Plan::NAME]['production_code'];
                }
            } else {
                $this->dateTime = $this->dtStart->format('c');
            }
        } else if ($this->type == self::TYPE_TRANSITION && !$this->data ) {
            $this->dateTime = $this->dtStart->format('c');
        }elseif(!$this->data ){
            $this->type= 3;
            $this->whileWorking= true;
            $this->shortTitle = __('Nėra ryšio');
        }else{
            $this->type= 1;
        }
        //$this->updateTimes();
    }

    public function updateTimes($operatos = array()) {
        //$this->title .= ($this->title ? '<br/>' : '').$this->dtStart->format('Y-m-d H:i:s').' - '.$this->dtEnd->format('Y-m-d H:i:s');
        $intervalStartClone = clone $this->dtStart;
        $intervalStartClone->sub(new DateInterval('PT1S'));
        $intervalEndClone = clone $this->dtEnd;
        $intervalEndClone->add(new DateInterval('PT1S'));
        $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Intervalas').'</b>: '.$intervalStartClone->format('H:i:s').' - '.$intervalEndClone->format('H:i:s');
        if(isset($this->data['FoundProblem'])){
            //$this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Trukmė').'</b>: '.round((strtotime($this->data['FoundProblem']['end']) - strtotime($this->data['FoundProblem']['start'])) / 60, 2).' '.__('min.');
            $diff = $intervalStartClone->diff($intervalEndClone);
            $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Trukmė').'</b>: '. __('%d min. %d s.', $diff->i, $diff->s);
            $dtStart = new DateTime($this->data['FoundProblem']['start']);
            $dtEnd = new DateTime($this->data['FoundProblem']['end']);
            $diff = $dtStart->diff($dtEnd);
            $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Bendra trukmė').'</b>: '.  __('%d h. %d min. %d s.', $diff->h, $diff->i, $diff->s);
        }else{
            $diff = isset($this->data['ApprovedOrder']['work_duration_from_start'])?$this->data['ApprovedOrder']['work_duration_from_start']:0;
            //fb($this->data['ApprovedOrder']);die();
            $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Užsakymo "žalio" gamybos laiko trukmė nuo gamybos pradžios').'</b>: '. __('%d val. %d min. %d s.', (int)($diff/3600),(int)($diff%3600/60),(int)($diff%3600%60));
            $diff = $this->dtStart->diff($this->dtEnd);
            $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Pažymėto intervalo trukmė').'</b>: '. __('%d min. %d s.', $diff->i, $diff->s);
            if(isset($this->data['Work']) && !empty($this->data['Work'])){
                $dtStart = new DateTime($this->data['Work']['start']);
                $dtEnd = new DateTime($this->data['Work']['end']);
                $diff = $intervalStartClone->diff($intervalEndClone);
                $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Bendra trukmė').'</b>: '.  __('%d h. %d min. %d s.', $diff->h, $diff->i, $diff->s);
                $durationInMin = bcdiv(1+strtotime($this->dtEnd->format('Y-m-d H:i:s')) - strtotime($this->dtStart->format('Y-m-d H:i:s')),60,4);
                $avgSpeedInCurrentInterval = bcdiv($this->intervalQuantity, $durationInMin,2);
                $this->title .= '<br/><b>'.__('Vid. pažymėto intervalo greitis').'</b>: '.$avgSpeedInCurrentInterval.' '.__('vnt/min');
                $multiplier = $this->data['Sensor']['plan_relate_quantity'] == 'quantity'?1:$this->data['Plan']['box_quantity'];
                $durationInMin = bcdiv(strtotime($this->data['Work']['end']) - strtotime($this->data['Work']['start']),60,4);
                $avgSpeedInAllInterval = $durationInMin > 0?bcdiv(bcmul($this->data['Work']['quantity'], $multiplier,4),$durationInMin,2):0;
                $this->title .= '<br/><b>'.__('Vid. viso intervalo greitis').'</b>: '.$avgSpeedInAllInterval.' '.__('vnt/min');
            }
            //$this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Trukmė').'</b>: '.round((strtotime($this->dtEnd->format('Y-m-d H:i:s')) - strtotime($this->dtStart->format('Y-m-d H:i:s'))) / 60, 2).' '.__('min.');
        }
        if(isset($this->data['Work']['user_id'])){
            $operatorId = $this->data['Work']['user_id'];
        }elseif(isset($this->data['FoundProblem']['user_id'])){
            $operatorId = $this->data['FoundProblem']['user_id'];
        }else{ $operatorId = -1; }
        $operator = isset($operatos[$operatorId])?$operatos[$operatorId]:'';
        $this->title .= ($this->title ? '<br/>' : '').'<b>'.__('Operatorius').'</b>: '.$operator;
        if(isset($this->data[ApprovedOrder::NAME]['losses']) && !empty($this->data[ApprovedOrder::NAME]['losses'])){
            $this->title .= '<br/><b>' . __('Nuostoliai').':</b>';
            foreach($this->data[ApprovedOrder::NAME]['losses'] as $loss){
                $this->title .= '<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.$loss['name'].': </b>'.(float)$loss['quantity'].' '.$loss['unit'];
            }
        }
        $this->start = (intval($this->dtStart->format('i')) * 60) + intval($this->dtStart->format('s'));
        $this->end = (intval($this->dtEnd->format('i')) * 60) + intval($this->dtEnd->format('s'));
    }
    
    /** @param TimelinePeriod[] $periods */
    public static function debugPeriods(array $periods, $type = '', $skipBad = true) { 
        foreach ($periods as $period) {
            if ($skipBad && $period->getDateStart() >= $period->getDateEnd()) continue;
            echo $type.' '.$period->type.': '.$period->getDateStart()->format('Y-m-d H:i:s').' :: '.$period->getDateEnd()->format('Y-m-d H:i:s')."<br/>\n";
        }
    }
    
    public static function attachApprovedOrderToPeriod($fApprovedOrders, &$periods, $fRecords, $fSensor, $fFoundProblems){
        $redZoneStartDeviation = false;
        if(preg_match('/[^,]+\s*,\s*\[(\d+)\s*-\s*\d+\]/i',trim($fSensor['Sensor']['tv_speed_deviation']), $match)){
            $redZoneStartDeviation = isset($match[1])?$match[1]:false;
        }
        $prodStartsOn = floatval($fSensor[Sensor::NAME]['prod_starts_on']) > 0?floatval($fSensor[Sensor::NAME]['prod_starts_on']):0.1;
        /*if($redZoneStartDeviation === false){//jei nustatyme nera ijungto greicio isporcijavimo i zonas, naudojame standartini algoritma*/
        if(isset($fSensor['Sensor']['dark_and_light_green_intervals']) && $fSensor['Sensor']['dark_and_light_green_intervals'] == 0){ //rodome tik vienos spalvos darbo laikus
            foreach ($fApprovedOrders as $fApprovedOrder) {
                TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(
                        TimelinePeriod::TYPE_PRODUCT,
                        new DateTime(date('Y-m-d H:i:s', strtotime($fApprovedOrder['ApprovedOrder']['created'])+5)),
                        new DateTime($fApprovedOrder[ApprovedOrder::NAME]['end']),
                        $fApprovedOrder
                    ));
            }
        }else{
            $redZoneStartDeviation = strlen($redZoneStartDeviation) && $redZoneStartDeviation >= 0 && $redZoneStartDeviation !== null?$redZoneStartDeviation:0;
            $slowSpeedDetected = null;
            $currentIntervalIndex = null;
            $prevIntervalIndex = null;
            $dis = new DateInterval('PT2S');
            $backDis = new DateInterval('PT'.(Configure::read('recordsCycle')-1).'S');
            $recordsCountInminute = 60 / Configure::read('recordsCycle');
            //$colorsIntervals = array(1000=>'#45Ff33', 1500=>'#ca0019', 2000=>'#094560');
            foreach ($fApprovedOrders as $fApprovedOrder) { //kiekvienam uzsakymui atskirai reikia sukti visus irasus, nes skirtingi uzsakymai gaminami skirtingais greiciais
                $openDateTime = null;
                $currentMinute = -1;
                $quantityInMinute = 0;
                $productionRecordsCountInMinute = 0;//irasu kiekis minuteje, kada vyko gamyba ir nebuvo problemos
                foreach($fRecords as $key => $fRecord){
                    //if(strtotime($fApprovedOrder['ApprovedOrder']['created'])-Configure::read('recordsCycle') <= strtotime($fRecord['Record']['created']) && strtotime($fApprovedOrder['ApprovedOrder']['end']) >= strtotime($fRecord['Record']['created'])){
                    if(strtotime($fApprovedOrder['ApprovedOrder']['created']) <= strtotime($fRecord['Record']['created']) && strtotime($fApprovedOrder['ApprovedOrder']['end']) >= strtotime($fRecord['Record']['created'])){
                        $cq = isset($fRecord['Record'][$fSensor['Sensor']['plan_relate_quantity']])?$fRecord['Record'][$fSensor['Sensor']['plan_relate_quantity']]:$fRecord['Record']['unit_quantity'];
                        $cq = $fRecord['Record']['found_problem_id'] > 0?0:$cq;
                        if(date('i',strtotime($fRecord['Record']['created'])) == $currentMinute){ //kai dirbame su minute kuri buvo uzregistruota, kiekius dedame i einamaja minute
                            if($fRecord['Record']['found_problem_id'] && $problemStartInPrevMinute == null){
                                $problemStartInPrevMinute = $fRecord['Record']['created'];
                            }
                            if ($fRecord['Record']['found_problem_id']){ continue; } //jei esama minute turi irasa su kiekiu, tai reikia uzbaigti skaiciavimus jai, bet probleminiu irasu netraukti niekur
                            $quantityInMinute += $cq;
                            $productionRecordsCountInMinute++;
                        }else{
                            if($currentMinute >= 0){//si salyga reikalinga tik pirmoje iteracijoje, nes cia negalima ieiti kol nera suskaiciuotas praejusios minutes kiekis
                                $shiftChanged = $key > 0 && $fRecords[$key-1]['Record']['shift_id'] != $fRecord['Record']['shift_id']?true:false;
                                $speedByPlan = $fApprovedOrder['Plan']['step'] / 60 * $productionRecordsCountInMinute / $recordsCountInminute; //numatomas pagaminti vienetu kiekis per minute
                                $slowSpeedLimit = $speedByPlan - ($speedByPlan * ($redZoneStartDeviation / 100)); //greitis laikomas letu, kai minutinis greitis patenka i raudona greicio zona, t.y. uz geltonos zonos ribu
                                $quantityInMinute = $quantityInMinute / $productionRecordsCountInMinute * $recordsCountInminute;
                                $colorsIntervals = self::createColorsIntervals($fSensor['Sensor']['work_intervals_colors']);
                                $colorsIntervals = empty($colorsIntervals)?array(
                                    'production_speed_low'=>array('min'=>0,'max'=>$slowSpeedLimit),
                                    'production_speed_good'=>array('min'=>$slowSpeedLimit,'max'=>PHP_INT_MAX),
                                ):$colorsIntervals;
                                if($slowSpeedDetected === null){//pirmoje iteracijoje reikia nuspresti ar musu pirma darbo linija prades piesti gera ar bloga greiti.
                                    $slowSpeedDetected = $quantityInMinute < $slowSpeedLimit?true:false;
                                }
                                if ($openDateTime) { //jei zinome darbo pradzios laika, spresime ar galime uzdaryti gera/bloga greiti ir pradeti piesti kita tipa
                                    if($shiftChanged){
                                        $dateTime = new DateTime($fRecord['Record']['created']);
                                        $dateTime->sub(new DateInterval('PT1S'));
                                        //$periodType = $slowSpeedDetected?TimelinePeriod::TYPE_PRODUCT_SLOW_SPEED:TimelinePeriod::TYPE_PRODUCT;
                                        $dateTime->sub(new DateInterval('PT'.(Configure::read('recordsCycle')-1).'S'));
                                        TimelinePeriod::binaryIntersect($periods, new TimelinePeriod($prevIntervalIndex, $openDateTime, $dateTime, $fApprovedOrder));
                                        $openDateTime = clone $dateTime;//atveriamas laikas
                                    }else{
                                        $currentIntervalIndex = key(array_filter($colorsIntervals, function($interval)use($quantityInMinute){
                                            return $quantityInMinute >= $interval['min'] && $quantityInMinute < $interval['max'];
                                        }));
                                        if($prevIntervalIndex != null && $currentIntervalIndex != $prevIntervalIndex){
                                            $dateTime->sub(new DateInterval('PT'.(Configure::read('recordsCycle')-1).'S'));
                                            TimelinePeriod::binaryIntersect($periods, new TimelinePeriod($prevIntervalIndex, $openDateTime, $dateTime, $fApprovedOrder)); //sukuriame gero greicio intervala nuo atverimo iki blogo greicio pradzios
                                            $openDateTime = clone $dateTime;//atveriamas laikas
                                        }
                                        $prevIntervalIndex = $currentIntervalIndex;
                                    }
                                    // elseif(($quantityInMinute < $slowSpeedLimit && !$slowSpeedDetected) || $shiftChanged){//naudojame atvirkstini metoda: zinoma, jog iki siol oejo geras greitis, bet praejusi minute parodo bloga greiti, todel nuo sio momento uzdarome gero greicio gamyba ir atveriama laika blogam greiciui
                                        // //$workInterval = self::getWorkInterval($openDateTime, $fFoundProblems);
                                        // TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(TimelinePeriod::TYPE_PRODUCT, $openDateTime, $dateTime, $fApprovedOrder)); //sukuriame gero greicio intervala nuo atverimo iki blogo greicio pradzios
                                        // $openDateTime = clone $dateTime;//atveriamas laikas
                                        // //$openDateTime->add($dis); //paslenkame laika per 1 realia sekunde, kad naujasis intervalas buti 1s tolesnis nei praejes uzsibaiges
                                        // $slowSpeedDetected = true; //kadangi praejusi minute turi bloga greiti, ta ir nustatome
                                    // }elseif(($quantityInMinute >= $slowSpeedLimit && $slowSpeedDetected) || $shiftChanged){//naudojame atvirkstini metoda: zinoma, jog iki siol oejo blogas greitis, bet praejusi minute parodo gera greiti, todel nuo sio momento uzdarome blogo greicio gamyba ir atveriama laika geram greiciui
                                        // //$workInterval = self::getWorkInterval($openDateTime, $fFoundProblems);
                                        // TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(TimelinePeriod::TYPE_PRODUCT_SLOW_SPEED, $openDateTime, $dateTime, $fApprovedOrder));//sukuriame blogo greicio intervala nuo atverimo iki gero greicio pradzios
                                        // $openDateTime = clone $dateTime;
                                        // //$openDateTime->add($dis);//paslenkame laika per 1 realia sekunde, kad naujasis intervalas buti 1s tolesnis nei praejes uzsibaiges
                                        // $slowSpeedDetected = false;//kadangi praejusi minute turi gera greiti, ta ir nustatome
                                    // }
                                }
                            }
    //                        if($fRecord['Record']['created'] == '2020-04-24 12:14:41'){
    //                            fb($openDateTime);
    //                            fb($dateTime);
    //                            die();
    //                        }
                            if ($fRecord['Record']['found_problem_id']){ //jei esamas irasas yra probleminis (ideta papildomas fixas jei irasas neigavo problemos, taciau turetu, nes kiekis mazesnis uz produkcijos kieki gamybai), su juo nedirbame
                                $currentMinute = -1;//resetiname esama minute
                                $quantityInMinute = 0; //resetiname kiekius
                                if($openDateTime && $openDateTime < $dateTime && $fRecord['Record']['found_problem_id']){
    //                                fb($dateTime);
    //                                fb($openDateTime);
    //                                fb($prevIntervalIndex);
    //                                fb($fRecord['Record']['created']);
    //                                fb('----------');
                                    //TimelinePeriod::binaryIntersect($periods, new TimelinePeriod($type, $openDateTime, $dateTime, $fApprovedOrder));
                                    //$workInterval = self::getWorkInterval($openDateTime, $fFoundProblems);
                                    $problemStartInPrevMinute = $problemStartInPrevMinute != null && strtotime($problemStartInPrevMinute) < strtotime($fRecord['Record']['created'])?$problemStartInPrevMinute:$fRecord['Record']['created'];
                                    TimelinePeriod::binaryIntersect($periods, new TimelinePeriod($prevIntervalIndex, $openDateTime, new DateTime($problemStartInPrevMinute), $fApprovedOrder));
                                    $openDateTime = null;
                                }
                                $problemStartInPrevMinute = null;
                                continue;
                            }
                            if(isset($dateTime)){ $prevOrderEnd = clone $dateTime; }
                            $dateTime = new DateTime($fRecord['Record']['created']); //kadangi dirbame su kiekiais minuteje, reikia paimti minutes pradzios laika. Cia patenka ciklas tik minutes pradzioje
                           // fb($dateTime->format('H:i:s'));
                            if(isset($prevOrderEnd)){
                                $diff = $dateTime->diff($prevOrderEnd, true);
                                if (($diff->days * 3600 + $diff->i * 60 + $diff->s) > Configure::read('recordsCycle')) {
                                    //$dateTime->sub($backDis);
                                }
                            }
                            if (!$openDateTime && $fRecord['Record']['found_problem_id'] == 0) { //uzsakymo iteraciju pradzioje atveriamas pirmas darbo laiko  langas
                                $openDateTime = clone $dateTime;
                                $openDateTime->sub($backDis);
                            }
                            $currentMinute = date('i',strtotime($fRecord['Record']['created'])); //nusakome kokia dabar minute prasideda
                            $quantityInMinute = $cq; //priskirame pirmos minutes kieki
                            $productionRecordsCountInMinute = 1; //resetiname irasu kieki su gamyba
                            $problemStartInPrevMinute = null;
                        }
                    }

                }
                if($openDateTime){
                    $orderStartDateTime = new DateTime($fApprovedOrder['ApprovedOrder']['created']);
                    $dateTime = new DateTime($fApprovedOrder['ApprovedOrder']['end']);
                    //uzsakymo pabaiga gali sutapti su problemos pradzia, tada rodoma melina linija grafike. Kad taip nebutu, sutapimo atveju pakeiciame truputi laika
                    $fFoundProblemsMatchStart = array_filter($fFoundProblems, function($fFoundProblem)use($fApprovedOrder){
                       return $fFoundProblem['FoundProblem']['plan_id'] == $fApprovedOrder['ApprovedOrder']['plan_id'] &&
                           strtotime($fFoundProblem['FoundProblem']['start']) < strtotime($fApprovedOrder['ApprovedOrder']['end']) &&
                           strtotime($fFoundProblem['FoundProblem']['end']) > strtotime($fApprovedOrder['ApprovedOrder']['created']);
                    });
                    if(!empty($fFoundProblemsMatchStart)){
                        $dateTime = new DateTime(current((array_reverse($fFoundProblemsMatchStart)))['FoundProblem']['end']);
                    }
                    TimelinePeriod::binaryIntersect($periods, new TimelinePeriod($prevIntervalIndex, $openDateTime, $dateTime, $fApprovedOrder));
                }
            }
        }
    }

	private static function getWorkInterval($openDateTime, $fFoundProblems){
		$workInterval = array_filter($fFoundProblems, function($fFoundProblem)use($openDateTime){
        	 return $openDateTime >= new DateTime($fFoundProblem['FoundProblem']['start']) && $openDateTime <= new DateTime($fFoundProblem['FoundProblem']['end']);
		});
		if(!empty($workInterval)){
			$workInterval = current($workInterval);
			$workInterval['Work'] = $workInterval['FoundProblem'];
			unset($workInterval['FoundProblem']);
		}
		return $workInterval;
	}

    public static function attachIntervalQuantitiesToPeriod($fRecords, &$periods, $fSensor, $fFoundProblems){
        foreach($periods as $period){
            if(!$period->data)continue;
//fb($period->dtStart->format('H:i:s').' - '.$period->dtEnd->format('H:i:s'));            
            $recordsInInterval = array_filter($fRecords, function($record)use($period){
                //$period->dtEnd->add(new DateInterval('PT1S'));
                $dateObjStart = new DateTime($record['Record']['created']);
				//$dateObjStart->sub(new DateInterval('PT1S'));
                $dateObjEnd = new DateTime($record['Record']['created']);
                //$dateObjEnd->sub(new DateInterval('PT1S'));
                return $dateObjStart >= $period->dtStart && $dateObjEnd <= $period->dtEnd && $record['Record']['approved_order_id'] == $period->data['ApprovedOrder']['id'];
            });
            $quantities = Set::extract('/Record/'.$fSensor['Sensor']['plan_relate_quantity'], $recordsInInterval);
            $period->intervalQuantity = !empty($quantities)?array_sum($quantities):0;
            $period->dtEnd->sub(new DateInterval('PT1S'));
            $workInterval = self::getWorkInterval($period->dtEnd, $fFoundProblems);
			$period->data['Work'] = !empty($workInterval)?$workInterval['Work']:array();
        }
    }

    public static function createColorsIntervals($intervals){
        static $intervalsCreated = null;
        if($intervalsCreated !== null){ return $intervalsCreated; }
        if(!trim($intervals)){ return array(); }
        $intervals = explode(',', $intervals);
        $intervals = array_map(function($interval){ return trim($interval); }, $intervals);
        foreach($intervals as $key => $interval){
            $values = explode(':', $interval);
            $prevIntervalValues = isset($intervals[$key-1])?explode(':', $intervals[$key-1]):array();
            //if(sizeof($values) != 2){ continue; }
            //if(!empty($nextIntervalValues) && sizeof($nextIntervalValues) != 2){ continue; }
            $min = !empty($prevIntervalValues)?floatval($prevIntervalValues[0]):0;
            $max = sizeof($values)==2?$values[0]:PHP_INT_MAX;
            $intervalsCreated['production_speed_'.$max] = array('min'=>$min,'max'=>$max, 'color'=>current(array_reverse($values)));
        }
        return $intervalsCreated;
    }
    
}