<?php
	class OpengraphComponent extends Component {
		var $y;
		var $total=0,$maxY=-10000,$minY=0,$columnsPairs=false,$colorIndex=1,$lineMax=0;
		var $colorsArray = array('0'=>'#FFD4FF','1'=>'#FF0000','2'=>'#00FF00','3'=>"#0000ff",'4'=>"#000000",'5'=>"#41367b",'6'=>"#8c05d2",'7'=>"#cf007f",'8'=>"#c36500",'9'=>"#5e3100",'10'=>"#5e0b00",'11'=>"#184f6f",'12'=>"#464671",'13'=>'#FFD4FF','14'=>'#2850ff','15'=>'#3b3b3b','16'=>'#CE794E');
		var $mainIndicatorNr = -1;
        var $indicators,$currentBarName;
		var $uniqueKeys = array(); //saugomi unikalus raktai multiple grafiko atveju

		private function rand_color() {
		    return sprintf('#%06X', mt_rand(0, 0xFFFFFF));
		}

		private function createBar($value,$name=""){
			$this->maxY = $this->maxY < $value?$value:$this->maxY;
			$bar = new bar_value($value);
			if(!$this->columnsPairs){
				$barColor = isset($this->colorsArray[$this->colorIndex++])?$this->colorsArray[$this->colorIndex++]:$this->rand_color();
                $bar->set_colour($barColor);
//                if($name == 'Darbo laikas') {
//                    fb("1");
//                    $bar->set_colour("#5AB65A");
//                }
//                elseif(utf8_decode($name) == 'Perėjimas') {
//                    fb("2");
//                    $bar->set_colour('#FDEE00');
//                }
//                elseif($name == 'Prastovos') {
//                    fb("3");
//                    $bar->set_colour('#D64D49');
//                }
			}
			if($value != 0){
				$this->currentBarName = !$this->currentBarName?'':$this->currentBarName;
				$bar->set_tooltip( $this->currentBarName.' '.($this->mainIndicatorNr >= 0 && isset($this->indicators[$this->mainIndicatorNr])?$this->indicators[$this->mainIndicatorNr]:'').' #val# ');
			}else{
				$bar->set_tooltip( 'Nėra reikšmių');
			}
	    	return $bar;
		}

		private function prepareData($value){
			 $value = (float)$value;
		     $this->total += $value>0?$value:0;
			 $this->lineMax = $this->lineMax < $value?$value:$this->lineMax;
		     return $value;
		}

		private function clearDateFromZeros($value){
			 $value = (float)$value < 0?0:(float)$value;
		     $this->total += $value>0?$value:0;
			 $this->lineMax = $this->lineMax < $value?$value:$this->lineMax;
		     return $value;
		}
		
		private function getUniqueKeys($data,$showType){
			if($showType=='multiple'){
				foreach($data as $index=>$key){

					$this->uniqueKeys = ($this->uniqueKeys + $key);

				}
				$this->uniqueKeys = array_keys($this->uniqueKeys);
			}else{
				$this->uniqueKeys = array_keys($data[0]);
			}

//            sort($this->uniqueKeys);
        }

		public function constructGraph($options,$lineParameters=array()){
            //fb($options,'options');
			$this->_initiateLibs();
            if(isset($options['selectedInfo'])){
                $this->mainIndicatorNr = $options['selectedInfo']['indicator1'];
            }
			$index = 0;
			//CakeLog::write('debug',print_r($options,true));
			$chart = new open_flash_chart();
            $chart->set_bg_colour( '#FFFFFF' );

			$maxY = 0;
			switch($options['columnsType']){
				case 0:
					$columnsType = 'bar_filled';
				break;
				case 1:
					$columnsType = 'bar_glass';
				break;
				case 2:
					$columnsType = 'bar_cylinder';
				break;
				case 3:
					$columnsType = 'bar_rounded_glass';
				break;
			}
			if($options['showType'] == 'multiple') $this->columnsPairs = true;
			$this->getUniqueKeys($options['data'],$options['showType']);
//            fb($options['data']);
//            fb($this->uniqueKeys);
            $this->minY = 0;

            $bar_index = 0;
            $total_bars = sizeof($options['data']);
			foreach($options['data'] as $key => $data){
			    $this->currentBarName = $key;
				$emptyValues = array_flip(array_diff($this->uniqueKeys,array_keys($data)));
				array_walk($emptyValues, function(&$val){$val=0;});
				$data = array_merge($emptyValues, $data);

                sort($this->uniqueKeys);
                ksort($data);
                $values = array_values($data);
				$this->total = 0;
				$values = array_map(array($this, 'prepareData'), $values);
                $bars = array_map(array($this, 'createBar'), $values);
                $this->colorIndex = 1;
                /* Fix bar colors for darbo laikas, perėjimas, prastovos */
                $i = 0;
                foreach($data as $name=>$val) {
                    if(property_exists($bars[$i],'colour')) {
                        $barColor = $bars[$i]->colour;
                        if(isset($options['colors'][utf8_decode($name)])){
                            $barColor = $options['colors'][utf8_decode($name)];
                        }
                        $bars[$i]->colour = $barColor;
                    }
                    $i++;
                }
//                $changeColour = false;
//                fb($key);
//                if ($key == 'Darbo laikas') {
//                    $barColor = "#5AB65A";
//                    $changeColour = true;
//                } elseif (utf8_decode($name) == 'Perėjimas') {
//                    $barColor = '#FDEE00';
//                    $changeColour = true;
//                } elseif ($name == 'Prastovos') {
//                    $barColor = '#D64D49';
//                    $changeColour = true;
//                }
//                if($changeColour) {
//                    foreach($bars as &$bar) {
//                        $bar->colour = $barColor;
//                    }
//                }


				$tags = new ofc_tags();
				$tags->font("Verdana", 10)
				    ->colour("#000000")
				    ->align_y_above()
				    ->align_x_center()
				    ->text('#y#');
				$x=0;
                $bar_pos = 0.8/$total_bars;
                $halfwidth = $bar_pos/2;
                foreach($values as $v){
                    if($this->minY > $v) {
                        $this->minY = $v;
                    }
                    $pos = round($x - 0.4 + $bar_pos*$bar_index + $halfwidth,4);
                    $ofc_tag = new ofc_tag($pos, round($v,2));
				    $tags->append_tag($ofc_tag);
                    $x++;
				}

				$chart->add_element( $tags );

				eval('$bar'.$index.' = new '.$columnsType.'();');
				$barColor = isset($this->colorsArray[$index])?$this->colorsArray[$index]:$this->rand_color();

				eval('$bar'.$index.'->colour($barColor);');
                if(trim($key)){
                    eval('$bar'.$index.'->key($key, 12);');
                }

                eval('$bar'.$index.'->set_values( $bars );');
//				eval('$bar'.$index.'->set_on_show(new bar_on_show(\'grow-up\',1,0));');
				eval('$bar'.$index.'->set_tooltip(\'#val# \'.$index);');
				eval('$bar'.$index.'->set_alpha(0.9);');
				eval('$chart->add_element( $bar'.$index.' );');
				//eval('$chart->set_alpha(1);');
				$index++;
                $bar_index++;
			}

            if($options['showType']=='multiple'){
                $splitter = array();
                $i = 0;
                $groupsCount = sizeof($this->uniqueKeys);
                foreach($options['data'] as $key => $data){
                    $splitter[] = new ofc_arrow($i+0.5, -5, $i+0.5, $this->maxY, '#784016',0);
                    $i++;
                    if($i>($groupsCount-2)) break;
                }
                foreach($splitter as $s){
//                    $chart->add_element( $s );
                }
                unset($splitter);
            }

    			if(!empty($options['paretoData'])){
    				$this->add_pareto_line($chart, $options['y2_legend'], $options['paretoData'],"Paretas: #val#%", $options['step2']);
    			}elseif(isset($options['recordCount']) && !empty($options['recordCount'])){
    				$this->add_pareto_line($chart, $options['y2_legend'], $options['recordCount'],$options['y2_legend'].": #val#", $options['step2']);
    			}

			$y_legend = new y_legend( $options['y_legend'] );
			$y_legend->set_style( '{font-size: 18px; color: #778877}' );
			$chart->set_y_legend( $y_legend );
			$x_legend = new x_legend( $options['x_legend'] );
			$x_legend->set_style( '{font-size: 22px; color: #778877}' );
			$chart->set_x_legend( $x_legend );

            $additionalTitle = '';
            if(isset($options['selectedInfo']['indicator1']) && is_numeric($options['selectedInfo']['indicator1'])){
                $additionalTitle .= $this->indicators[$options['selectedInfo']['indicator1']];
            }
            if(isset($options['selectedInfo']['indicator2']) && is_numeric($options['selectedInfo']['indicator2'])){
                $additionalTitle .= (trim($additionalTitle)?' '.__('ir').' ':'').$this->indicators[$options['selectedInfo']['indicator2']];
            }
            $title = new title($additionalTitle."\n".$options['graph_title']);
			$title->set_style('{font-size: 16px;}');
			$chart->set_title( $title );
			$x_labels = new x_axis_labels();
			$x_labels->rotate(15);
			$x_labels->set_size(isset($options['fonts']) && $options['fonts']?17:12);
            reset($options['data']);
			$x_labels->set_labels(array_map('utf8_decode',$this->uniqueKeys));
			$x = new x_axis();
			$x->set_offset( true );
			$x->set_labels( $x_labels );
			$chart->set_x_axis( $x );

			$y = new y_axis();
			$y_labels = new y_axis_labels();
			$y_labels->set_size(isset($options['fonts']) && $options['fonts']?17:10);
			$y->set_labels( $y_labels );
			$step = (int)$this->maxY>=10?ceil((int)$this->maxY/10):1;
			$step = $options['step'] > 0 && $options['step'] <= $this->maxY?$options['step']:$step;
            if($this->maxY < 0) {
                if($this->maxY >= -10) {
                    $step = 1;
                } else {
                    $step = ceil((int)$this->maxY/10);
                }

                $this->maxY = 0;
            }

//            fb(round($this->maxY*($this->maxY>10?1.2:1.4),-1),'max range');
            $max_y = $this->maxY*($this->maxY>10?1.2:1.4);
            if($max_y > 10) {
                $max_y = round($this->maxY*($this->maxY>10?1.3:1.5),-1);
            } else {
                round($this->maxY*($this->maxY>10?1.2:1.4));
            }

			$y->set_range((($this->minY>=0)?0:round($this->minY,-1)), $max_y, $step);
			$chart->set_y_axis( $y );
			$chart->set_number_format(0, true, false, false);
			
            if(!empty($lineParameters)){
                $this->addLines($chart,$lineParameters);
            }
			return $chart->toString();
		}

		private function add_pareto_line(&$chart,$y2_legend,$data,$tooltip,$step=0){
			$stepAuto = max($data)>=50?ceil(max($data)/20):1; //CakeLog::write('debug',print_r($step,true));
			$step = $step > 0?$step:$stepAuto;
			$y = new y_axis_right();
			$y->set_range( 0, max($data), $step );
			$y->set_stroke( 3 );
			$y->set_colour( '#ff0000' );
			$y->set_tick_length( 12 );
			$y_legend = new y2_legend( $y2_legend );
			$y_legend->set_style( '{font-size: 22px; color: #778877}' );
			$chart->set_y2_legend( $y_legend );
			$chart->set_y_axis_right( $y );
			$d = new hollow_dot();
			$d->tooltip($tooltip);
			$d->size(4)->halo_size(0)->colour('#000000');
			$line_dot = new line();
			$line_dot->set_colour('#ff0000');
			$line_dot->set_default_dot_style($d);
			$line_dot->set_values( $data );
			$line_dot->attach_to_right_y_axis();
			$chart->add_element( $line_dot );
		}

        private function addLines(&$chart,$options){
            $lines=array();
            $i = 1;
            $maxValue = 0;
            foreach($options['data'] as $param => $values){
                $maxValue = $maxValue < ceil(max($values))?ceil(max($values)):$maxValue;
                $stepAuto = $maxValue>=20?ceil($maxValue/15):1;
                $step = $stepAuto;
				$this->colorsArray = array_reverse($this->colorsArray);
                $barColor = isset($this->colorsArray[$i])?$this->colorsArray[$i]:$this->rand_color();
                //$default_dot = new dot();
               // $default_dot->size(4)->halo_size(2)->colour('#000000')->tooltip( '#x_label#'."\n".$param.': #val#' );
                $lines[$i] = new line();
                $d = new anchor();
                $d->size(4)->halo_size(1)->colour('#668053')->rotation(45)->tooltip($this->indicators[$options['selectedInfo']['indicator2']].' '.($param=='0'?'':$param).": #val# ".$options['y_legend'])->sides(5);
                $lines[$i]->set_default_dot_style($d);
                $values = array_values(array_map(array($this, 'clearDateFromZeros'), $values));
                $lines[$i]->set_values( $values );
                $lines[$i]->set_colour( $barColor );
                $lines[$i]->line_style( new line_style(4, 3) );
                $lines[$i]->set_width( 3 );
                $lines[$i]->set_on_show('pop');
                $lines[$i]->set_on_show(new line_on_show('fade-in',0,0));
                $lines[$i]->attach_to_right_y_axis();
               // $lines[$i]->set_key($param, 18 );
                $i++;

//                $tags = new ofc_tags();
//                $tags->font("Verdana", 10)
//                    ->colour("#000000")
//                    ->align_y_above()
//                    ->align_x_center()
//                    ->text('#y#');
//                $x=0;
//                foreach($values as $v){
//                    $ofc_tag = new ofc_tag($x++, $v);
//                    $tags->append_tag($ofc_tag);
//                }
//                $chart->add_element( $tags );
            }


//            $chart->add_element( $tags );
            $y = new y_axis_right();
            $y->set_range( 0, ceil($maxValue*1.1), $stepAuto );
            $y->set_stroke( 2 );
            $y->set_colour( '#784016' );
            $y->set_tick_length( 5 );
            $y_legend = new y2_legend( $options['y_legend']."\n".__('linijos') );
            $y_legend->set_style( '{font-size: 18px; color: #778877}' );
            $chart->set_y2_legend( $y_legend);
            $chart->set_y_axis_right( $y );

            //$d = new hollow_dot();
            //$d->tooltip("Paretas: #val#%");
            //$d->size(4)->halo_size(0)->colour('#000000');
            foreach($lines as $line){
                $chart->add_element( $line );
            }
        }

		public function constructGraphPie($options){
			$this->_initiateLibs();
			$title = new title( $options['graph_title'] );

			$pie = new pie();
			$pie->set_alpha(0.8);
			$pie->set_start_angle( 35 );
			$pie->add_animation( new pie_fade() );
			$pie->set_tooltip( __('#val# iš #total#<br>#percent# iš 100%'));

            //fb($options,'opts');

            $colors = $this->colorsArray;

			$values = array();
			$total = array_sum(current($options['data']));
            $total = $total > 0?$total:1;
            $i = 0;
            if($total > 0){
    			foreach(current($options['data']) as $param => $value){
    				$values[] = new pie_value(round($value/$total*100), utf8_decode($param));
                    if(isset($options['colors'][utf8_decode($param)])){
                        $colors[$i] = $options['colors'][utf8_decode($param)];
                    }
                    $i++;
    			}
    		}

            $pie->set_colours( $colors );
			$pie->set_values( $values );

			$chart = new open_flash_chart();
            $chart->set_bg_colour( '#FFFFFF' );
			$chart->set_title( $title );
			$chart->add_element( $pie );

			$chart->x_axis = null;
			return $chart->toString();
		}

        private function prepareSequentialData($data) {
            // Join keys to one array.
            $dataOut = array();
            $keyArray = array();
            foreach($data as $d=>$arr) {
                foreach($arr as $date=>$val) {
                    $keyArray[] = $date;
                }
            }
            sort($keyArray);

            // Add each value to specific index
            foreach($keyArray as $index=>$date) {
                foreach($data as $d=>$arr) {
                    foreach($arr as $dataDate=>$dataValue) {
                        $dataOut[$d][array_search($dataDate,$keyArray)] = (float)$dataValue;
                    }
                }
            }

            // Fill empty indexes with null
            $keyArraySize = sizeof($keyArray);
            foreach($dataOut as $dataIndex=>&$dataArray) {
                for($i=0;$i<$keyArraySize;$i++) {
                    if(!array_key_exists($i,$dataArray)) {
                        $dataArray[$i] = null;
                    }
                }
                ksort($dataArray);
            }

            return array($dataOut,$keyArray);
        }

		public function constructGraphLine($options,$lineParameters=array()){
            list($data,$realKeys) = $this->prepareSequentialData($options['data']);
            $this->_initiateLibs();
            $title = new title( $options['graph_title'] );
            $title->set_style('{font-size: 20px;}');
			$chart = new open_flash_chart();
            $lines=array();
            $i = 1;
            foreach($options['data'] as $param => $values){
                $barColor = isset($this->colorsArray[$i])?$this->colorsArray[$i]:$this->rand_color();
                $default_dot = new dot();
                $default_dot->size(4)->halo_size(2)->colour('#000000')->tooltip( '#x_label#'."\n".$param.': #val#' );
                $lines[$i] = new line();
                $lines[$i]->set_default_dot_style($default_dot); //CakeLog::write('debug',print_r($options['data'][1]['PARAMETRAS_kaitinimo_lempos_zona_1'],true));
                $values = array_values(array_map(array($this, 'prepareData'), $values));
                if(empty($data)) {
                    $data[$param] = $values;
                }
                $lines[$i]->set_values( $data[$param] );
                $lines[$i]->set_colour( $barColor );
                $lines[$i]->set_width( 3 );
                if(trim($param)){
                	$lines[$i]->set_key($param, 12 );
				}
				$tags = new ofc_tags();
				$tags->font("Verdana", 10)
				    ->colour("#000000")
				    ->align_y_above()
				    ->align_x_center()
				    ->text('#y#');
				$x=0;
				foreach($data[$param] as $v){
                    if($v != null) {
                        $tags->append_tag(new ofc_tag($x, $v));
                    }
                    $x++;
				}
				$chart->add_element( $tags );
                $i++;
            }

            $y = new y_axis();
            $autoStep = $options['steps'] > 0?$options['steps']:(ceil($this->lineMax*1.2) > 10?round(ceil($this->lineMax*1.2)/10):1);
            $y->set_range( 0, ceil($this->lineMax*1.2),$autoStep);
//            fb(ceil($this->lineMax*1.2),'y range');

            $x_labels = new x_axis_labels();
            $keys = array_keys($options['data'][key($options['data'])]);
            foreach($realKeys as $k=>&$name) {
                $name = utf8_decode($name);
            }
            $x_labels->set_labels($realKeys);
            $x_labels->rotate(15);
            $nr = ceil($options['limit']/floor($options['graphWidth']/50));
            $step = $nr>1?$nr:1;
            $x_labels->set_steps($step);

            $x_labels->set_size( 12 );

            $x = new x_axis();

            $x->set_offset( false );
            $x->set_colour( '#428C3E' );
            $x->set_labels( $x_labels );

            $y_legend = new y_legend( $options['y_legend']."\n".__('stulpeliai') );
            $y_legend->set_style( '{font-size: 18px; color: #778877}' );
            $chart->set_y_legend( $y_legend );
            $x_legend = new x_legend( $options['x_legend'] );
            $x_legend->set_style( '{font-size: 22px; color: #778877}' );
            $chart->set_x_legend( $x_legend );

            $chart->set_bg_colour( '#FFFFFF' );
            $chart->set_title( $title );
            foreach($lines as $line){
                $chart->add_element( $line );
            }

            //$chart->add_element( $line_2 );
            $chart->set_y_axis( $y );
            $chart->set_x_axis( $x );
			if(!empty($lineParameters)){
                $this->addLines($chart,$lineParameters);
            }
            return $chart->toString();
        }

        public function paint_horizontal_bars($options){
            $this->_initiateLibs();
            $additionalTitle = '';
            if(isset($options['selectedInfo']['indicator1']) && is_numeric($options['selectedInfo']['indicator1'])){
                $additionalTitle .= $this->indicators[$options['selectedInfo']['indicator1']];
            }
            if(isset($options['selectedInfo']['indicator2']) && is_numeric($options['selectedInfo']['indicator2'])){
                $additionalTitle .= ' '.__('ir').' '.$this->indicators[$options['selectedInfo']['indicator2']];
            }
            $title = new title($additionalTitle."\n".$options['graph_title']);
            $title->set_style('{font-size: 16px;}');

            $valuesOfChat = array();
            $keys = array();
            foreach($options['data'] as $param => $values){
                $valuesOfChat[] = current(array_values(array_map(array($this, 'prepareData'), $values))) / ($options['y_legend'] == '%'?3600:1);
                $keys[] = $param;
            }
            $hbar = new hbar( '#86BBEF' );
            $hbar->set_tooltip( $options['legends']['widget1Tooltip']. ': #val#' );
            $hbar->set_values( $valuesOfChat );

            $chart = new open_flash_chart();
            $chart->set_bg_colour( '#FFFFFF' );
            $chart->set_title( $title );
            $chart->add_element( $hbar );

            $x = new x_axis();
            $x->set_offset( false );
            $x->set_range( 0, $maxValue = max(array_values($valuesOfChat)));
            $x->set_steps($maxValue>=10?ceil($maxValue/10):1);

            $chart->set_x_axis( $x );

            $y = new y_axis();
            $y->set_offset( true );
            $y->set_labels( array_reverse($keys) );
            $chart->add_y_axis( $y );

            $tooltip = new tooltip();
            //
            // LOOK:
            //
            $tooltip->set_hover();
            //
            //
            //
            $tooltip->set_stroke( 1 );
            $tooltip->set_colour( "#000000" );
            $tooltip->set_background_colour( "#ffffff" );
            $chart->set_tooltip( $tooltip );

            return $chart->toString();
        }



		private function _initiateLibs(){
			App::import( 'Vendor', 'php-ofc-library/open-flash-chart' );
            $this->indicators = Configure::read('INDICATORS');
		}

	}
