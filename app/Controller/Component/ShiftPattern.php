<?php
Class ShiftPattern{
    
    public function constructCalendar($startPoint=null, $duration = 14, $branchPattern, $branchId, $lastDate=array()){
        $originalStart = clone $startPoint;
        //$startPoint->sub(new DateInterval('P1D'));
        $availableWeekdays = $branchPattern['weekdays']; //date('N') savaites dienos reprezentacija, kur 6 - sestad, 7 - sekmad ir t.t.
        $availableHoures = $branchPattern['shifts'];
        $overwriteShifts = isset($branchPattern['overwriteShifts'])?$branchPattern['overwriteShifts']:array();
        $holidays = $branchPattern['shifts'];
        $reachPoint = $startPoint < new DateTime()?new DateTime():clone $startPoint;
        $reachPoint->add(new DateInterval('P'.$duration.'D'));
        $calendar = array();
        set_time_limit(20);
        while($startPoint < $reachPoint){
            //patikriname ar esama diena nera tarp savaites dienu, kuriomis nedirbama
            foreach($availableHoures as $key => $interval){
                //ieskome to sablono numerio, kuris pratestu paskutini irasa, esanti duombazeje pagal eiles nr.
                if(empty($calendar) && !empty($lastDate) && $lastDate['Shift']['ps_id'] < sizeof($availableHoures) && ($key+1) - $lastDate['Shift']['ps_id'] <= 0){ continue; }
                //$startPointSecond = $startPoint - strtotime(date('Y-m-d', $startPoint));
                //$start = max($startPointSecond, $interval['start']*3600);
                list($shoure,$sminute,$ssecond) = explode(':',$interval['start']);
                list($ehoure,$eminute,$esecond) = explode(':',$interval['end']);
                $startObj = clone $startPoint; 
                $endObj = clone $startPoint;
                if($shoure>$ehoure) $endObj->add(new DateInterval('P1D'));
                $startObj->setTime($shoure,$sminute,$ssecond);
                $endObj->setTime($ehoure,$eminute,$esecond);
                $diff = $startObj->diff($endObj);
                $startPoint->setTime($shoure,$sminute,$ssecond);
                $start = clone $startPoint;
                $startPoint->add(new DateInterval('PT'.($diff->h*3600 + $diff->i*60 + $diff->s).'S'));
                if($startPoint <= $originalStart){ continue; }
                //patikriname ar esama diena nera tarp atostogu
                $disabled = false;
                if(in_array($start->format('m-d'), $holidays)){
                    //GOTO START_LOOP; //sokame i kita diena su laiku 00:00
                    $disabled = true;
                }
                if(!in_array($start->format('N'), $availableWeekdays)){
                    //GOTO START_LOOP; //sokame i kita diena su laiku 00:00
                    $disabled = true;
                }
                foreach($overwriteShifts as $overShiftKey => $overShiftStatus){//jei reikia isjungti ar ijungti specifine pamaina savaiteje
                    list($overWeekDay, $overShiftTitle) = explode('_', $overShiftKey); 
                    if($start->format('N') == $overWeekDay && $interval['shift_title'] == $overShiftTitle){
                        $disabled = $overShiftStatus == 'disabled'?1:0;
                    }
                }
                $calendar[] = array(
                    'start'=>$start->format('Y-m-d H:i:s'), 
                    'end'=>$startPoint->format('Y-m-d H:i:s'), 
                    'type'=>$interval['shift_title'], 
                    'ps_id' => $key+1,
                    'disabled'=>$interval['disabled'] || $disabled,
                    'branch_id'=>$branchId,
                    'name' => $start->format('Y-m-d').' ['.$interval['shift_title'].'] '.($key+1)
                );
            }
            continue;
            START_LOOP:
            $startPoint->add(new DateInterval('P1D'));
        }
        return $calendar;
    }
    
}
