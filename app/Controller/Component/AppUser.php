<?php

App::uses('User', 'Model');

/**
 * Description of AppUser
 *
 */
class AppUser {
	
	/** @var int */
	public $id;
	/** @var string */
	public $userName;
	/** @var string */
	public $firstName;
	/** @var string */
	public $lastName;
	/** @var string */
	public $email;
	/** @var string */
	public $password;
	/** @var boolean */
	public $isActive = false;
	
	public function __construct() {
	}

	public function populate(array $f) {
		$this->id = isset($f[User::NAME]['id']) ? intval($f[User::NAME]['id']) : null;
		$this->userName = isset($f[User::NAME]['username']) ? $f[User::NAME]['username'] : null;
		$this->email = isset($f[User::NAME]['email']) ? $f[User::NAME]['email'] : null;
		$this->password = isset($f[User::NAME]['password']) ? $f[User::NAME]['password'] : null;
		$this->firstName = isset($f[User::NAME]['first_name']) ? $f[User::NAME]['first_name'] : null;
		$this->lastName = isset($f[User::NAME]['last_name']) ? $f[User::NAME]['last_name'] : null;
		$this->isActive = (isset($f[User::NAME]['active']) && $f[User::NAME]['active']);
        $this->sensorId = isset($f[User::NAME]['sensor_id']) ? $f[User::NAME]['sensor_id'] : null;
	}
	
}
