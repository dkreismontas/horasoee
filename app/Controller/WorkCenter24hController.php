<?php 
class WorkCenter24hController extends AppController{
    
    public $uses = array('Record','ShiftTimeProblemExclusion','WorkCenter','Sensor','Shift');
    
    public function index($workCenters=''){
        $title_for_layout = __('Darbo centrai 24h');
        $this->layout = 'full_page';
        $this->set(compact('title_for_layout','workCenters'));
    }
    
    public function get_data($workCenters=''){
        $conditions = array();
        $conditions = array(
            'Sensor.marked'=>1, 
            'Sensor.id'=>Configure::read('user')->selected_sensors,
        );
        $sensorsids = trim($workCenters)?array_intersect(Configure::read('user')->selected_sensors, explode('_', $workCenters)):Configure::read('user')->selected_sensors;
        if(trim($workCenters)){
            $conditions['Sensor.id'] = $sensorsids;
        }
        $sensorsList = Hash::combine($this->Sensor->find('all', array('conditions'=>$conditions)), '{n}.Sensor.id', '{n}.Sensor');
        $exclusionDowntimesList = array(0)+$this->ShiftTimeProblemExclusion->find('list',array('fields'=>array('problem_id')));
        $conditions = array(
            'Record.sensor_id'=>Set::extract('/id',$sensorsList),
            'Record.created >'=>date('Y-m-d H', strtotime('-23 hour')).':00:00',
            //'DATE_FORMAT(Record.created,\'%H\') = \'23\''
        );
        $bindModel = array('belongsTo'=>array('FoundProblem'));
        $searchQuery = array(
            'fields'=>array(
                'SUM(IF(Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS green_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id=1,' . (Configure::read('recordsCycle')) . ',0)) as yellow_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id>=3 AND FoundProblem.problem_id NOT IN (' . implode(',', $exclusionDowntimesList) . '),' . (Configure::read('recordsCycle')) . ',0)) as red_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id<>1 AND FoundProblem.problem_id IN (' . implode(',', $exclusionDowntimesList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS grey_time',
                'TRIM(Record.sensor_id) AS sensor_id',
                'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\') AS groupper'
            ),'conditions'=>$conditions,
            'group'=>array('Record.sensor_id', 'DATE_FORMAT(Record.created,\'%Y-%m-%d %H\')'),
            'order'=>array('Record.sensor_id','Record.created')
        );
		$shift['Shift'] = array('id'=>0, 'branch_id'=>0, 'start'=>date('Y-m-d H', strtotime('-23 hour')).':00:00', 'end'=>date('Y-m-d H:i:s'));
        $parameters = array(&$searchQuery,&$bindModel,&$shift, &$sensorsList);
        $pluginData = $this->Help->callPluginFunction('WorkCenter24_BeforeRecordSearch_Hook', $parameters, Configure::read('companyTitle'));
        $this->Record->bindModel($bindModel);
        if(is_array($searchQuery)) {
            $sensorsDataTmp = $this->Record->find('all', $searchQuery);
        }else{
            $sensorsDataTmp = $this->Record->query($searchQuery);
        }
        $sensorsData = array();
        foreach($sensorsDataTmp as $sensorDataTmp){
            $sensorId = $sensorDataTmp[0]['sensor_id'];
            $groupper = $sensorDataTmp[0]['groupper'];
            $sensorDataTmp[0]['unknown_time'] = 3600-array_sum(array_intersect_key($sensorDataTmp[0], array_flip(array('green_time','yellow_time','red_time','grey_time'))));
            $sensorsData[$sensorId]['hours'][$groupper] = $sensorDataTmp[0];
            $sensorsData[$sensorId]['sensor'] = $sensorsList[$sensorId];
        }
        
        $oeeCalculations = $this->calcOee($shift,$sensorsids);
        foreach($oeeCalculations as $sensorId =>$oee){
            if(!isset($sensorsData[$sensorId])){ continue; }
            // $total = array_sum($time);
            // if($total > 0){
                // array_walk($time, function(&$param)use($total){ $param = round($param/$total*100); });
            // }
            //$sensors[$sensorId] = array_merge($sensors[$sensorId], $time);
            //$sensors[$sensorId]['oee'] = min(100, (isset($oee[$sensorId]['oee'])?round($oee[$sensorId]['oee'],1):0));
            if(isset($oee['oee_with_exclusions'])){
                $sensorsData[$sensorId]['oee_data']['oee'] =  min(100, round($oee['oee_with_exclusions'],0));
                $sensorsData[$sensorId]['oee_data']['exploitation_factor'] =  min(100, round($oee['exploitation_factor_with_exclusions']*100,0));
            }elseif(isset($oee['oee'])){
                $sensorsData[$sensorId]['oee_data']['oee'] = min(100, round($oee['oee'],0));
                $sensorsData[$sensorId]['oee_data']['exploitation_factor'] =  min(100,round($oee['exploitation_factor']*100,0));
            }else{
                $sensorsData[$sensorId]['oee_data']['oee'] = 0; 
            }
            $totalTimes = array();
            $sensorsData[$sensorId]['oee_data']['operational_factor'] = min(100,round($oee['operational_factor']*100,0));
            $totalTimes['green_time'] = array_sum(Set::extract('/green_time', array_values($sensorsData[$sensorId]['hours'])));
            $totalTimes['yellow_time'] = array_sum(Set::extract('/yellow_time', array_values($sensorsData[$sensorId]['hours'])));
            $totalTimes['grey_time'] = array_sum(Set::extract('/grey_time', array_values($sensorsData[$sensorId]['hours'])));
            $totalTimes['red_time'] = array_sum(Set::extract('/red_time', array_values($sensorsData[$sensorId]['hours'])));
            $totalTimes['total_time'] = array_sum($totalTimes);
            $totalTimes['green_time'] = $totalTimes['total_time'] > 0?round(bcdiv($totalTimes['green_time'], $totalTimes['total_time'], 4)*100):0;
            $totalTimes['yellow_time'] = $totalTimes['total_time'] > 0?round(bcdiv($totalTimes['yellow_time'], $totalTimes['total_time'], 4)*100):0;
            $totalTimes['grey_time'] = $totalTimes['total_time'] > 0?round(bcdiv($totalTimes['grey_time'], $totalTimes['total_time'], 4)*100):0;
            $totalTimes['red_time'] = $totalTimes['total_time'] > 0?round(bcdiv($totalTimes['red_time'], $totalTimes['total_time'], 4)*100):0;
            $totalTimes['single_type_exist'] = !empty(array_filter($totalTimes, function($type){ return $type == 100; }));
            //$sensors[$sensorId]['quality_factor'] = round($oee[$sensorId]['quality_factor'],2);
            $sensorsData[$sensorId]['oee_data'] += $totalTimes;
            $parameters = array(&$sensorsData[$sensorId]);
            $pluginData = $this->Help->callPluginFunction('WorkCenter24_AfterGetData_Hook', $parameters, Configure::read('companyTitle'));
        }
//fb($sensorsData);
        $sensors = $this->WorkCenter->combineSensorsIntoColumns($sensorsData, 2);
        
        echo json_encode(array(
            'sensors'=>$sensors
        ));
        die();
    }

    private function calcOee($fShift,$sensors){
        if(!$fShift){ return array(); }
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $oeeObj = new OeeShell;
        $fShift[Shift::NAME]['end'] = date('Y-m-d H:i');
        $oeeObj->shift = $fShift;
        $oeeObj->workWithMo = false;
        $oeeObj->searchFromShift = false;
        $data = $oeeObj->start_sensors_calculation($sensors,true);
        return $data;
    }
}
