<?php

App::uses('DiffCard', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Line', 'Model');
App::uses('Plan', 'Model');
App::uses('Problem', 'Model');
App::uses('User', 'Model');
App::uses('DiffType', 'Model');
App::uses('Settings', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('CakeEmail', 'Network/Email');

/**
 * @property DiffCard $DiffCard
 * @property User $User
 * @property DiffType $DiffType
 * @property Settings $Settings
 * @property Sensor $Sensor
 * @property Branch $Branch
 * @property Line $Line
 * @property Problem $Problem
 * @property Plan $Plan
 */
class DiffCardsController extends AppController {
	
	const ID = 'diff_cards';
	const MODEL = DiffCard::NAME;
	public $uses = array(self::MODEL, User::NAME, DiffType::NAME, Settings::NAME, Sensor::NAME, Branch::NAME, Line::NAME, Problem::NAME, Plan::NAME, ApprovedOrder::NAME);
	public $components = array('Session', 'Paginator','Help');
		
	/** @requireAuth Peržiūrėti neatitikties korteles */
	public function index() {
		$this->requestAuth(true);
		$this->Paginator->settings = array('limit' => 1000, 'order' => array(DiffCard::NAME.'.date' => 'desc', 'conditions'=>array('DiffCard.sensor_id'=>Configure::read('user')->selected_sensors)));
		try {
			$this->DiffCard->bindModel(array('belongsTo'=>array('Sensor','Branch','Plan','Line','FoundProblem','Problem','DiffType')));
			$list = $this->Paginator->paginate(DiffCard::NAME);
		} catch (NotFoundException $ex) {
			$this->request->params['named']['page'] = 1;
			$this->Paginator->paginate(self::MODEL);
			$url = array('controller' => self::ID, 'action' => 'index');
			if (isset($this->request['paging'][self::MODEL]['pageCount'])) {
				$url['page'] = max(intval($this->request['paging'][self::MODEL]['pageCount']), 1);
			}
			$this->redirect(Router::url($url, true));
		}
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
		$this->set(array(
			'title_for_layout' => __('Neatitikties kortelės'),
			'list' => $list,
			'model' => self::MODEL,
			'states' => DiffCard::getStates(),
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'excelUrl' => Router::url('genxls'),
			'usersList'=> $this->User->find('list', array('fields'=>array('User.id', 'full_name'))),
            'colors' => array(DiffCard::STATE_NEW => '#ffadb3', DiffCard::STATE_ACCEPTED => '#ffee7a', DiffCard::STATE_EXPLAINED => '#a8bfff', DiffCard::STATE_EXPLANATION_DENIED => '#ddadff', DiffCard::STATE_COMPLETE => '#75ff95'),
            'helpComponent'=>$this->Help
		));
	}
	/** @requireAuth Redaguoti neatitikties korteles */
	public function edit() {
        $this->requestAuth(true);
        $id = $this->request->params['id'];
        $listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
        $item = $id ? $this->DiffCard->withRefs()->findById($id) : null;
        $user = $this->User->findById(self::$user->id);
        $canEdit = false;
        $canSaveErrors = array();

        $additionalText = '';
        if (!empty($item) && ($item[DiffCard::NAME]['responsive_section_user_id']??0)) {
            $responsiveUser = $this->User->findById($item[DiffCard::NAME]['responsive_section_user_id']);
            $quality_email = preg_split('/$\R?^/m', $responsiveUser['User']['additional_emails'])[0];
        }
		if (!$item) {
			$canEdit = true;
		} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) {
			$canEdit = true;
		} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $user[User::NAME]['id'] == $item[DiffCard::NAME]['responsive_section_user_id']) {
			$canEdit = true;
		} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED && trim($quality_email) == trim($user['User']['email'])) { // && $user[User::NAME]['id'] == $item[DiffCard::NAME]['quality_section_user_id']
			$canEdit = true;
		} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLANATION_DENIED && $user[User::NAME]['id'] == $item[DiffCard::NAME]['production_manager_user_id']) {
			$canEdit = true;
		}
        if(isset($item['Plan'])){
            $approvedOrder = $this->ApprovedOrder->find('first',array('conditions'=>array('ApprovedOrder.plan_id'=>$item['Plan']['id'], 'ApprovedOrder.created <' => $item['DiffCard']['date'], 'ApprovedOrder.end >' => $item['DiffCard']['date'], 'ApprovedOrder.sensor_id' => $item['Plan']['sensor_id'])));
            if($approvedOrder){
                $item['ApprovedOrder'] = $approvedOrder['ApprovedOrder'];
            }
        }
		if (empty($this->request->data)) {
			$this->request->data = $item;
		} else {
            if(array_key_exists('redirectToOtherResponsive',$this->request->data)) {
                $item['DiffCard']['state'] = DiffCard::STATE_NEW;
                $responsiveUser = $this->User->findById($item[DiffCard::NAME]['responsive_section_user_id']);
                $otherResponsiveUser = $this->User->findById($this->request->data('otherResponsive'));
                $additionalText = ' (Peradresuotas. '.$this->User->buildName($responsiveUser) .' -> '.$this->User->buildName($otherResponsiveUser).')';
            }
            $sensor = $this->Sensor->withRefs()->findById($this->request->data(DiffCard::NAME.'.sensor_id'));
			if ($item) {
				$arr = array('id' => $item[DiffCard::NAME]['id']);
			} else {
				$arr = array();
				$item = array(DiffCard::NAME => array(
					'id' => null,
					'date' => null,
					'state' => DiffCard::STATE_NEW
				));
			}
			if(isset($item[DiffCard::NAME]['sensor_id'])) $arr['sensor_id'] = $item[DiffCard::NAME]['sensor_id'];
			if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW ) {
				if (!$item[DiffCard::NAME]['id'] && !empty($sensor)) {
					$arr['date'] = $item[DiffCard::NAME]['date'] = date('Y-m-d H:i:s');
					$arr['sensor_id'] = $this->request->data(DiffCard::NAME.'.sensor_id');
					$arr['branch_id'] = $sensor['Sensor']['branch_id'];
					$arr['line_id'] = !empty($sensor['Line'])?$sensor['Line']['id']:0;
					$arr['start'] = $this->request->data(DiffCard::NAME.'.start_date').' '.$this->request->data(DiffCard::NAME.'.start_time');
					$arr['end'] = $this->request->data(DiffCard::NAME.'.end_date').' '.$this->request->data(DiffCard::NAME.'.end_time');
					$arr['plan_id'] = $this->request->data(DiffCard::NAME.'.plan_id');
					$arr['problem_id'] = $this->request->data(DiffCard::NAME.'.problem_id');
					$arr['approved_order_id'] = $this->request->data(DiffCard::NAME.'.approved_order_id');
				}
				$arr['consequences'] = $this->request->data(DiffCard::NAME.'.consequences');
				if ($arr['consequences']) {
					$arr['consequences'] = floatval(str_replace(',', '.', trim($arr['consequences'])));
					$this->request->data(DiffCard::NAME.'.consequences', ($arr['consequences'] ? $arr['consequences'] : ''));
				} else {
					$arr['consequences'] = 0;
				}
				$arr['shift_manager_user_id'] = $user[User::NAME]['id'];
				$arr['description'] = $this->request->data(DiffCard::NAME.'.description');
				$arr['diff_type_id'] = $this->request->data(DiffCard::NAME.'.diff_type_id');
				$arr['diff_sub_type_id'] = $this->request->data(DiffCard::NAME.'.diff_sub_type_id');
				$arr['responsive_section_user_id'] = $this->request->data(DiffCard::NAME.'.responsive_section_user_id');
				if(array_key_exists('redirectToOtherResponsive',$this->request->data)) {
                    $arr['responsive_section_user_id'] = $this->request->data('otherResponsive');
                    $arr['description'] = $item['DiffCard']['description'];
                    $arr['diff_type_id'] = $item['DiffCard']['diff_type_id'];
                    $arr['diff_sub_type_id'] = $item['DiffCard']['diff_sub_type_id'];
                }
				$arr['state'] = DiffCard::STATE_ACCEPTED;
                if (!isset($arr['sensor_id'])){
                    $canSaveErrors[] = __('Nenurodytas darbo centras');
                }
                if (!$arr['responsive_section_user_id']){
                    $canSaveErrors[] = __('Nenurodytas atsakingas asmuo');
                }
                if (!$arr['description']) {
					$canSaveErrors[] = __('Neįvestas aprašymas');
				}
//                if (!$arr['diff_type_id'] && !$arr['diff_sub_type_id']) {
//					$canSaveErrors[] = __('Neįvestas neatitikties tipas');
//				}
			} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED && $canEdit) {
				$arr['clarification'] = $this->request->data(DiffCard::NAME.'.clarification');
				$arr['measures_taken'] = $this->request->data(DiffCard::NAME.'.measures_taken');
				$arr['measures_for_future'] = $this->request->data(DiffCard::NAME.'.measures_for_future');
				$arr['deadline'] = $this->request->data(DiffCard::NAME.'.deadline');
				$arr['state'] = DiffCard::STATE_EXPLAINED;
				if (!$arr['clarification']) {
                    $canSaveErrors[] = __('Neįvestas paaiškinimas');
				}
				if (!$arr['measures_taken']) {
                    $canSaveErrors[] = __('Nenurodyti nedelsiant atlikti veiksmai');
				}
				if (!$arr['measures_for_future']) {
                    $canSaveErrors[] = __('Nenurodyti veiksmai, kurie turėtų užtikrinti, kad ateityje tai nebepasikartos');
				}
				if (!$arr['deadline']) {
                    $canSaveErrors[] = __('Nenurodytas koregavimo veiksmų atlikimo terminas');
				}
			} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED && $canEdit) {
				$arr['measure_evaluation'] = $this->request->data(DiffCard::NAME.'.measure_evaluation');
				$arr['explanation_denied'] = ($this->request->data('explanation_denied') ? 1 : 0);
				$arr['state'] = ($arr['explanation_denied'] ? DiffCard::STATE_ACCEPTED : DiffCard::STATE_COMPLETE);
                if($arr['state'] == DiffCard::STATE_ACCEPTED) {
                   // $item[DiffCard::NAME]['state'] = DiffCard::STATE_ACCEPTED;
                    $additionalText = ' ('.__('Nepatvirtintas').' - '.$this->request->data(DiffCard::NAME.'.measure_evaluation').')';
                } else {
                    $additionalText = ' ('.__('Patvirtintas').' - '.$this->request->data(DiffCard::NAME.'.measure_evaluation').')';
                }
				if (!$arr['measure_evaluation']) {
                    $canSaveErrors[] = __('Neįvestas koregavimo ir prevencinių veiksmų efektyvumo įvertinimas');
				}
			} else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLANATION_DENIED && $canEdit) {
				$arr['production_manager_comment'] = $this->request->data(DiffCard::NAME.'.production_manager_comment');
				$arr['state'] = DiffCard::STATE_COMPLETE;
				if (!$arr['production_manager_comment']) {
                    $canSaveErrors[] = __('Neįvestas komentaras');
				}
			} else {
				$this->redirect($listUrl);
			}
			if (empty($canSaveErrors)) {
			    if(isset($this->request->data['DiffCard']['attachment']) && is_array($this->request->data['DiffCard']['attachment'])){
			        $arr['attachment'] = isset($item['DiffCard']['attachment'])?explode(',',$item['DiffCard']['attachment']):array();
			        foreach($this->request->data['DiffCard']['attachment'] as $attachment){
			            if(!trim($attachment['name']) || !$attachment['size']){ continue; }
    			        $fileName = pathinfo($attachment['name'], PATHINFO_FILENAME);
    			        $fileExtension = pathinfo($attachment['name'], PATHINFO_EXTENSION);
                        $fileName = date('Y-m-d_H:i').'_'.trim(Inflector::underscore(Inflector::slug(strtolower(basename($fileName))))).'.'.$fileExtension;
                        $uploadPath = WWW_ROOT.DS.'files'.DS.'diff_cards_attachments'.DS;
                        if(!file_exists($uploadPath)) {
                            mkdir($uploadPath,0777);
                        }
                        $uploadFile = $uploadPath.$fileName;
                        if(move_uploaded_file($attachment['tmp_name'],$uploadFile)){
                            $arr['attachment'] = array_merge($arr['attachment'], array($fileName));
                        }
                    }
                    $arr['attachment'] = implode(',', $arr['attachment']);
                }
                $parameters = array(&$arr);
                $pluginData = $this->Help->callPluginFunction('extendDiffCardDataHook', $parameters, Configure::read('companyTitle'));
                if($this->DiffCard->save(array(DiffCard::NAME => $arr))) {
                    if (!$item[DiffCard::NAME]['id']) $item[DiffCard::NAME]['id'] = $this->DiffCard->id;

                    if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) {
                        // Siųsti atsakovui, kokybės kontrolei ir CC kitiems vartotojams sąraše
                        $responsiveUser = $this->User->find('first',array('conditions'=>array('User.id'=>$arr['responsive_section_user_id'])));
                        $recipients = array();
                        $recipients[] = $responsiveUser['User']['email'];
                        $cc = preg_split('/$\R?^/m',$responsiveUser['User']['additional_emails']);
                        $recipients[] = array_shift($cc);
                        $this->sendMail($item,$recipients,$cc,$additionalText);
                    }
                    elseif($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED) {
                        // Siųsti kokybės kontrolei ir CC atsakovo sąraše
                        $responsiveUser = $this->User->find('first',array('conditions'=>array('User.id'=>$item['DiffCard']['responsive_section_user_id'])));
                        $recipients = array();
                        $cc = preg_split('/$\R?^/m',$responsiveUser['User']['additional_emails']);
                        $recipients[] = array_shift($cc);
                        $this->sendMail($item,$recipients,$cc,$additionalText);
                    }
                    elseif($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED) {
                        $responsiveUser = $this->User->find('first',array('conditions'=>array('User.id'=>$item['DiffCard']['responsive_section_user_id'])));
                        $recipients = array();
                        $cc = preg_split('/$\R?^/m',$responsiveUser['User']['additional_emails']);
                        $recipients[] = array_shift($cc);
                        $this->sendMail($item,$recipients,$cc,$additionalText);
                    }
                    else {
                        $this->sendMail($item);
                    }
                    $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                    $item = null;
                    $this->redirect($listUrl);
				} else {
					$this->Session->setFlash(__('Nepavyko išsaugoti įrašo').': '.print_r($this->DiffCard->validationErrors, true), 'default', array(), 'saveMessage');
				}
			} else {
                if($this->request->is('ajax')){
			        echo json_encode($canSaveErrors);die();
                }else {
                    if ($item && !$item[self::MODEL]['id']) $item = null;
                    $this->Session->setFlash('<ul><li>' . implode('</li><li>', $canSaveErrors) . '</li></ul>', 'default', array(), 'saveMessage');
                }
			}
		}
        if (isset($this->request->query['wc'])) {
            $sensor = $this->Sensor->withRefs()->find('first', array('conditions' => array('Sensor.id' => $this->request->query['wc'])));
            if (!empty($sensor)) {
                $fPlan = $this->Plan->withRefs()->find('first', array(
                    'conditions' => array(
                        Plan::NAME . '.sensor_id'          => $sensor[Sensor::NAME]['id'],
                        ApprovedOrder::NAME . '.status_id' => ApprovedOrder::STATE_IN_PROGRESS
                    ),
                    'order'      => array(ApprovedOrder::NAME . '.created DESC'),
                ));
                $data = array(
                    'DiffCard' => array(
                        'sensor_id' => $sensor['Sensor']['id'],
                        'branch_id' => $sensor['Sensor']['branch_id'],
                        'line_id'   => $sensor['Line']['id'],
                        'plan_id'   => (!empty($fPlan)) ? $fPlan['Plan']['id'] : ""
                    )
                );
                $this->request->data = $data;
            }
        }
		
		$qualityUser = $item ? $this->User->find('all',array('conditions'=>array('User.id'=>$item[Branch::NAME]['quality_user_id']))) : null;
		$productionUser = $item ? $this->User->findById($item[Branch::NAME]['production_user_id']) : null;
		$responsiveUsers = $this->User->find('all', array('conditions' => array(User::NAME.'.responsive' => 1, 'User.email <>'=>''), 'order' => User::NAME.'.id ASC'));
		foreach ($responsiveUsers as $idx => $v) { $responsiveUsers[$idx][User::NAME]['label'] = User::buildName($v, false); }
		$diffTypes = $this->DiffType->find('all', array('order' => (DiffType::NAME.'.parent_id ASC, '.DiffType::NAME.'.id ASC')));
		$title = $item ? sprintf(__('Neatitikties kortelė %s (ID: %d)'), $item[self::MODEL]['date'], $item[self::MODEL]['id']) : __('Nauja neatitikties kortelė');

        //$responsiveUsers[0]['User']['id'] = 0;
		//$responsiveUsers = array('User'=>array('id'=>0, 'name'=>'xxx')) + $responsiveUsers;
        $planOptions = array();
        if(isset($sensor) && $sensor) {
            $currentPlan = $this->Plan->withRefs()->find('first', array(
                'conditions' => array(
                    Plan::NAME.'.sensor_id' => $sensor[Sensor::NAME]['id'],
                    ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_IN_PROGRESS
                ),
                'order' => array(ApprovedOrder::NAME.'.created DESC'),
            ));
            if(!empty($currentPlan)) {
                $additionalData = isset($currentPlan['ApprovedOrder'])?json_decode($currentPlan['ApprovedOrder']['additional_data'],true):array();
                $nr = isset($additionalData['add_order_number'])?', nr.: '.$additionalData['add_order_number']:'';
                $planOptions[$currentPlan['Plan']['id']] = $currentPlan['Plan']['production_name'] .' ('.$currentPlan['Plan']['mo_number'].$nr.')';
            }
        }
        $problemOptions = $this->Problem->getAsSelectOptions(false, true, false);
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'model' => self::MODEL,
			'userData' => $user,
			'canEdit' => $canEdit,
			'qualityUser' => $qualityUser,
			'productionUser' => $productionUser,
			'responsiveUsers' => $responsiveUsers,
			'item' => $item,
			'diffTypes' => $diffTypes,
			'branchOptions' => $this->Branch->getAsSelectOptions(false),
			'sensorOptions' => $this->Sensor->getAsSelectOptions(true, array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1)),
			'lineOptions' => $this->Line->getAsSelectOptions(false),
			'problemOptions' => $problemOptions,
			'planOptions' => $planOptions,//$this->Plan->getAsSelectOptions(false, true),
			'listUrl' => $listUrl,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true),
            'childDiffCardExist'=>$this->DiffType->find('first', array('conditions'=>array('DiffType.parent_id >'=>0))),
		));
        if(preg_match('/.*work-center\/*.*/i',$_SERVER['HTTP_REFERER']??'')){
            $this->set(array(
                'navigation'=>array(),
                'possibleLanguages'=>array()
            ));
        }
	}
	
	/** @requireAuth Atsisiųsti neatitikties kortelių xls ataskaitą */
	public function genxls($fromTo = '') {
	    App::import('Vendor', 'mexel/PHPExcel');
        App::import('Vendor', 'mexel/PHPExcel/IOFactory');
		$sysName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
		if(trim($fromTo)){
		    $date = explode(' ~ ',$fromTo);
            $from = isset($date[0])?date('Y-m-d H:i:s', trim($date[0])):date('Y-m-d');
            $to = isset($date[1])?date('Y-m-d H:i:s', trim($date[1])):date('Y-m-d');
		}else{
    		$date = explode(' ~ ',$this->request->query('fromto'));
    		$from = isset($date[0])?trim($date[0]):date('Y-m-d');
		    $to = isset($date[1])?trim($date[1]):date('Y-m-d');
        }
		if ($from) {
			$fdt = new DateTime($from);
		} else { 
			$fdt = new DateTime();
			$fdt->sub(new DateInterval('P1M'));
		}
		$fdt->setTime(0, 0, 0);
		if ($to) {
			$tdt = new DateTime($to);
		} else {
			$tdt = new DateTime();
		}
		$tdt->setTime(23, 59, 59);
		
		$brdStyle = array('allborders' => array('style' => PHPExcel_Style_Border::BORDER_THIN, 'color' => array('rgb' => '000000')));
		$columns = array(
			array('name' => __('Data (kada surašyta)'), 'width' => 14),
			array('name' => __('Savaitė'), 'width' => 14),
			array('name' => __('Užpildęs asmuo'), 'width' => 14),
			array('name' => __('Linija'), 'width' => 14),
			array('name' => __('Darbo centras'), 'width' => 14),
			array('name' => __('Gaminys'), 'width' => 14),
			array('name' => __('Mo'), 'width' => 14),
			array('name' => __('Nuostoliai'), 'width' => 14),
			array('name' => __('Atsakingas asmuo'), 'width' => 14),
			array('name' => __('Neatitikties kodas [1]'), 'width' => 14),
			array('name' => __('Neatitikties kodas [1.1]'), 'width' => 14),
			array('name' => __('Prastova (nuo iki), trukmė'), 'width' => 14),
			array('name' => __('Neatitikties aprašymas'), 'width' => 14),
			array('name' => __('Neatitikties priežastis'), 'width' => 14),
			array('name' => __('Kokie veiksmai atlikti nedelsiant?'), 'width' => 14),
			array('name' => __('Numatyti koregavimo veiksmai'), 'width' => 14),
			array('name' => __('Koregavimo veiksmų įvertinimas'), 'width' => 14),
			array('name' => __('Įvykdymo terminas'), 'width' => 14),
			//array('name' => __('Gamybos vadovo komentaras'), 'width' => 14),
			//array('name' => __('Įvykdymas'), 'width' => 14)
		);
        $searchParameters = array('conditions' => array(
            DiffCard::NAME.'.date >=' => $fdt->format('Y-m-d H:i:s'),
            DiffCard::NAME.'.date <=' => $tdt->format('Y-m-d H:i:s')
        ), 'order' => DiffCard::NAME.'.date ASC');
        $parameters = array(&$this,&$columns,&$searchParameters);
        $pluginData = $this->Help->callPluginFunction('DiffCards_changeReportHeaders_Hook', $parameters, Configure::read('companyTitle'));
		
		$objPHPExcel = new PHPExcel();
		$objPHPExcel->getProperties()
				->setCreator($sysName)
				->setLastModifiedBy($sysName)
				->setTitle(__('Neatitikties kortelė'));
		
		$workSheet = $objPHPExcel->setActiveSheetIndex(0);
		$workSheet->setShowGridlines(false)->setTitle('Menuo');
		$workSheet->getStyle('A1:S1')->getBorders()
				->applyFromArray($brdStyle);
		$workSheet->getStyle('A1:S1')->getFont()
				->setBold(true);
		
		$workSheet->getRowDimension(1)->setRowHeight(42);
		
		foreach ($columns as $idx => $col) {
			$colId = chr(ord('A') + $idx);
			$workSheet->setCellValue($colId.'1', $this->foxColumnText($col['name']));
			$workSheet->getColumnDimension($colId)->setWidth($col['width']);
		}
		$list = $this->DiffCard->withRefs()->find('all', $searchParameters);
		$rowCount = count($list);
		$workSheet->getStyle('A2:S'.(1 + $rowCount))->getBorders()
				->applyFromArray($brdStyle);
		$workSheet->getStyle('A2:S'.(1 + $rowCount))->getAlignment()
				->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
		$workSheet->getStyle('A2:S'.(1 + $rowCount))->getNumberFormat()
				->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_TEXT);
		
		$rowIdx = 2; 
		foreach ($list as $idx => $li) {
			$dt = new DateTime($li[DiffCard::NAME]['date']);
			$sdt = new DateTime($li[DiffCard::NAME]['start']);
			$edt = new DateTime($li[DiffCard::NAME]['end']);
			$ddt = new DateTime($li[DiffCard::NAME]['deadline']);
			$col = 'A';
            $values = array(
                 $dt->format('Y-m-d H:i'),
                 'W'.$dt->format('W'),
                 $li[User::NAME.DiffCard::PFX_SHIFT_MANAGER]['first_name'].' '.$li[User::NAME.DiffCard::PFX_SHIFT_MANAGER]['last_name'],
                 $li[Line::NAME]['name'],
                 $li[Sensor::NAME]['name'],
                 $li['Product']['production_code'],
                 $li[Plan::NAME]['mo_number'],
                 $li[DiffCard::NAME]['consequences'],
                 $li[User::NAME.DiffCard::PFX_RESPONSIVE_SECTION]['first_name'].' '.$li[User::NAME.DiffCard::PFX_RESPONSIVE_SECTION]['last_name'],
                 $li[DiffType::NAME]['code'],
                 ' '.$li[DiffType::NAME.DiffCard::PFX_SUB_TYPE]['code'].' ',
                 $sdt->format('H:i').' - '.$edt->format('H:i'),
                 $li[DiffCard::NAME]['description'],
                 $li[DiffCard::NAME]['clarification'],
                 $li[DiffCard::NAME]['measures_taken'],
                 $li[DiffCard::NAME]['measures_for_future'],
                 $li[DiffCard::NAME]['measure_evaluation'],
                 $ddt->format('Y-m-d'),
                 $li[DiffCard::NAME]['production_manager_comment'],
                 ''
            );
            $parameters = array(&$values, &$li);
            $pluginData = $this->Help->callPluginFunction('DiffCards_changeReportValues_Hook', $parameters, Configure::read('companyTitle'));
            for($valueIndex = 0; $valueIndex < sizeof($values); $valueIndex++){
                $workSheet->setCellValue($col++.$rowIdx, $values[$valueIndex]);
            }
            $workSheet->getRowDimension($rowIdx)->setRowHeight(40);
			$rowIdx++;
		}
        $workSheet->getStyle('A1:S'.$rowIdx)->getAlignment()
            ->setWrapText(true)
            ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
            ->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
        $parameters = array(&$this,&$objPHPExcel);
        if(!$this->Help->callPluginFunction('DiffCards_beforeXlsOutput_Hook', $parameters, Configure::read('companyTitle'))){
    		header('Content-Type: application/vnd.ms-excel');
    		header('Content-Disposition: attachment;filename="report.xls"');
    		header('Cache-Control: max-age=0');
    		header('Cache-Control: max-age=1');
    		header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    		header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
    		header ('Cache-Control: cache, must-revalidate');
    		header ('Pragma: public');
    		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
    		$objWriter->save('php://output');
    		exit();
        }
	}
	
	private function foxColumnText($text) {
		$idx = strpos($text, ' (');
		if ($idx === false) return $text;
		$a = substr($text, 0, $idx + 1);
		$b = substr($text, $idx + 1);
		$richText = new PHPExcel_RichText();
		$richText->createTextRun($a."\n")
				->getFont()->setBold(true);
		$richText->createTextRun($b)
				->getFont()->setBold(false);
		return $richText;
	}
	
	private function sendMail($item,$recipients = array(),$cc = array(),$additionalStateText = '') {
        set_time_limit(30);
        $recipients = array_filter($recipients);
        $cc = array_filter($cc);
		$newItem = $this->DiffCard->withRefs()->findById($item[DiffCard::NAME]['id']);
		$sysEmail = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL);
		$sysEmailName = $this->Settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);
        if(empty($recipients)) {
            if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_NEW) {
                $responsiveUser = $newItem[User::NAME . DiffCard::PFX_RESPONSIVE_SECTION];
                if (isset($responsiveUser['email']) && $responsiveUser['email']) $recipients[$responsiveUser['email']] = User::buildName($newItem, false, DiffCard::PFX_RESPONSIVE_SECTION);
                $qualityUser = $newItem[User::NAME . DiffCard::PFX_QUALITY_SECTION];
                if (isset($qualityUser['email']) && $qualityUser['email']) $recipients[$qualityUser['email']] = User::buildName($newItem, false, DiffCard::PFX_QUALITY_SECTION);
            } else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_ACCEPTED) {
                $qualityUser = $newItem[User::NAME . DiffCard::PFX_QUALITY_SECTION];
                if (isset($qualityUser['email']) && $qualityUser['email']) $recipients[$qualityUser['email']] = User::buildName($newItem, false, DiffCard::PFX_QUALITY_SECTION);
            } else if($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLAINED) {
                if ($newItem[DiffCard::NAME]['explanation_denied']) {
                    $productionUser = $newItem[User::NAME . DiffCard::PFX_PRODUCTION_MANAGER];
                    if (isset($productionUser['email']) && $productionUser['email']) $recipients[$productionUser['email']] = User::buildName($newItem, false, DiffCard::PFX_PRODUCTION_MANAGER);
                }
            } else if ($item[DiffCard::NAME]['state'] == DiffCard::STATE_EXPLANATION_DENIED) {
                $shiftUser = $newItem[User::NAME . DiffCard::PFX_SHIFT_MANAGER];
                if (isset($shiftUser['email']) && $shiftUser['email']) $recipients[$shiftUser['email']] = User::buildName($newItem, false, DiffCard::PFX_SHIFT_MANAGER);
                $responsiveUser = $newItem[User::NAME . DiffCard::PFX_RESPONSIVE_SECTION];
                if (isset($responsiveUser['email']) && $responsiveUser['email']) $recipients[$responsiveUser['email']] = User::buildName($newItem, false, DiffCard::PFX_RESPONSIVE_SECTION);
                $qualityUser = $newItem[User::NAME . DiffCard::PFX_QUALITY_SECTION];
                if (isset($qualityUser['email']) && $qualityUser['email']) $recipients[$qualityUser['email']] = User::buildName($newItem, false, DiffCard::PFX_QUALITY_SECTION);
            }
        }
		if ($sysEmail && !empty($recipients)) {
            $recipients = array_map('trim',$recipients);
            $cc = array_map('trim',$cc);
            $dateTime = new DateTime($item[DiffCard::NAME]['date']);
            $mail = new CakeEmail(self::EMAIL_CONFIG);
            $mail->from('noreply@horasoee.eu');
            $mail->to($recipients);
            if(!empty($cc)) $mail->cc($cc);
            $mail->subject(sprintf('HorasOEE '.__('Neatitikties kortelė %s'), $dateTime->format('Y-m-d H:i')));
            $mail->emailFormat('both');
            $mail->template('diff_card', null);
            $url = Router::url(array('controller' => self::ID, 'action' => ($item[DiffCard::NAME]['id'].'/edit')), true);
            $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
            $mail->viewVars(array(
                'item' => $newItem,
                'states' => DiffCard::getStates(),
                'additionalStateText'=>$additionalStateText,
                'url' => $url,
                'usersList'=> $this->User->find('list', array('fields'=>array('User.id', 'full_name'))),
            ));
            $mail->send();
        } else {
			// TODO: log configuration error
		}
	}
	
}
