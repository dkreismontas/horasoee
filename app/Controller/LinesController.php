<?php

App::uses('Line', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');

/**
 * Description of LinesController
 * @property Line $Line
 * @property Sensor $Sensor
 * @property Branch $Branch
 */
class LinesController extends AppController {
	
	const ID = 'lines';
	const MODEL = Line::NAME;
	
	public $uses = array(self::MODEL, Sensor::NAME, Branch::NAME);
	
	/** @requireAuth Peržiūrėti linijas */
	public function index() {
		$this->requestAuth(true);
        $this->Line->bindModel(array('belongsTo'=>array(
            'Sensor'=>array('foreignKey'=>false,'conditions'=>array('Line.id = Sensor.line_id'))
        )));
        $list = $this->Line->find('all',array(
            'conditions'=>array(
                'OR'=>array('Sensor.id'=>Configure::read('user')->selected_sensors, 'Sensor.id IS NULL'),
            ),'order'=>array('Line.id'),'group'=>array('Line.id')));
		$this->set(array(
			'title_for_layout' => __('Linijos'),
			'list' => $list,
			'model' => self::MODEL,
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove'),
			'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?')
		));
	}
	
	/** @requireAuth Redaguoti linijas */
	public function edit() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		
		$itemSensorIndex = array();
		if (empty($this->request->data)) {
			$this->request->data = $item = $this->Line->withRefs()->findById($id);
			if (isset($item[Sensor::NAME])) foreach ($item[Sensor::NAME] as $li) { $itemSensorIndex[$li['id']] = true; }
		} else {
			$item = null;
			if ($this->Line->save($this->request->data)) {
				$this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
				$this->redirect($listUrl);
			}
		}
		$sensorBList = $this->Sensor->withRefs()->find('all', array('conditions'=>array('Sensor.id'=>Configure::read('user')->selected_sensors)));
		for ($i = (count($sensorBList) - 1); $i >= 0; $i--) {
			if (isset($itemSensorIndex[$sensorBList[$i][Sensor::NAME]['id']])) unset($sensorBList[$i]);
		}
		
		$title = $item ? sprintf(__('Linija %s (ID: %d)'), $item[self::MODEL]['name'], $item[self::MODEL]['id']) : __('Nauja linija');
		$this->set(array(
			'title_for_layout' => $title,
			'h1_for_layout' => $title,
			'item' => $item,
			'sensors' => implode(';', array_keys($itemSensorIndex)),
			'model' => self::MODEL,
			'sensorBList' => $sensorBList,
			'sensorOptions' => $this->Sensor->getAsSelectOptions(),
			'branchOptions' => $this->Branch->getAsSelectOptions(),
			'listUrl' => $listUrl,
			'formUrl'=> Router::url(($id ? $id : 0).'/edit', true)
		));
	}
	
	/** @requireAuth Pašalinti linijas */
	public function remove() {
		$this->requestAuth(true);
		$id = $this->request->params['id'];
		$listUrl = Router::url(array('controller' => self::ID, 'action' => 'index'), true);
		try {
			if ($this->Line->delete($id, false)) {
				$this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		} catch (PDOException $ex) {
			$code = ''.$ex->getCode();
			if (substr($code, 0, 2) == '23') {
				$this->Session->setFlash(__('Įrašas yra naudojamas ir todėl negali būti pašalintas'), 'default', array(), 'saveMessage');
			} else {
				$this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
			}
		}
		$this->redirect($listUrl);
	}
	
}
