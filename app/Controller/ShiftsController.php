<?php

App::uses('Shift', 'Model');
App::uses('Branch', 'Model');

/**
 * @property Shift $Shift
 */
class ShiftsController extends AppController {
	
	const ID = 'shifts';
	const MODEL = Shift::NAME;
	
	public $uses = array(self::MODEL, Branch::NAME, 'Log');
	
	public $components = array('Session', 'Paginator');
	
	/** @requireAuth Peržiūrėti pamainas */
	public function index() {
		$this->requestAuth(true);
		// $list = $this->Shift->find('all');
		$branchOptions = $this->Branch->getAsSelectOptions();
		$this->Paginator->settings = array('limit' => 30, 'order' => array(Shift::NAME.'.start' => 'desc'), 'conditions'=>array('Shift.branch_id'=>array_keys($branchOptions)));
        if(isset($this->params['named']['branch_id'])){
            $this->Paginator->settings['conditions'] = array('Shift.branch_id'=>$this->params['named']['branch_id']);
        }
		try {
			/* $this->Shift->withRefs(); */$list = $this->Paginator->paginate(Shift::NAME);
		} catch (NotFoundException $ex) {
			$this->request->params['named']['page'] = 1;
			$this->Paginator->paginate(self::MODEL);
			$url = array('controller' => self::ID, 'action' => 'index');
			if (isset($this->request['paging'][self::MODEL]['pageCount'])) {
				$url['page'] = max(intval($this->request['paging'][self::MODEL]['pageCount']), 1);
			}
			$this->redirect(Router::url($url, true));
		}
		$this->set(array(
			'title_for_layout' => __('Pamainos'),
			'list' => $list,
			'model' => self::MODEL,
			'branchOptions' => $branchOptions,
			'newUrl' => Router::url('0/edit'),
			'editUrl' => Router::url('%d/edit'),
			'removeUrl' => Router::url('%d/remove')
		));
	}
    
    /** @requireAuth Įjungti / išjungti pamainą */
    public function disableEnableShift($shiftId) {
        $this->requestAuth(true);
        $this->Shift->updateAll(array('Shift.disabled'=>'ABS(Shift.disabled - 1)'), array('Shift.id'=>$shiftId));
        $shift = $this->Shift->findById($shiftId);
        if($shift['Shift']['disabled']){
            $this->Log->write('Išjungė pamainą ID: '.$shiftId.'. Pamainos duomenys: '.json_encode($shift));
        }else{
            $this->Log->write('Įjungė pamainą ID: '.$shiftId.'. Pamainos duomenys: '.json_encode($shift));
        }
        $this->redirect($_SERVER['HTTP_REFERER']);
    }
	
}
