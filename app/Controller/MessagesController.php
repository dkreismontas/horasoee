<?php
class MessagesController extends AppController{

    public $name = 'Messages';
    public $uses = array('Record','ProjectTester','Mail');

    public function beforeFilter(){
        $proxyData = Configure::read('SMS_PROXY_DATA');
        if(isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] == $proxyData['user'] && isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_PW'] == $proxyData['password']){
            $this->Auth->allow(array('execute_proxy_sms','get_latest_record'));
        }
        parent::beforeFilter();
    }

    public function execute_proxy_sms(){
        $this->layout = 'empty';
        try{
            $postJSON = json_decode(file_get_contents('php://input'));
            $smsModel = ClassRegistry::init('SMS');
            $subject = $postJSON->subject??'';
            $textFrontFromSubject = trim($subject)?$subject."\n":'';
            $smsModel->send($textFrontFromSubject.$postJSON->text, $postJSON->to, 'HorasOEE',$subject,160,true);
        }catch (Exception $e){
            pr($e);
        }
        die();
    }

    public function get_latest_record(){
        $lastRecord = $this->Record->find('first', array('fields'=>array('Record.created'),'order'=>array('Record.created DESC')));
        echo json_encode(current($lastRecord));
        die();
    }

    public function check_project_alive(){
        $proxyData = Configure::read('SMS_PROXY_DATA');
        if(!isset($proxyData['phone_nr']) || empty($proxyData['phone_nr'])){ return null; }
        $projectList = array(
            //projektu sarasas paduodamas per projekto plugina pvz
            //'DuonaPanevezys'=>'http://172.30.6.97/messages/get_latest_record'
        );
        $parameters = array(&$projectList);
        $pluginData = $this->Help->callPluginFunction('Messages_beforeCheckProjectAlive_Hook', $parameters, Configure::read('companyTitle'));
        $allowServerDowntime = 300; //sekundemis
        $text = '';
        foreach($projectList as $project => $projectUrl){
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_USERPWD        => $proxyData['user'].":".$proxyData['password'],
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_SSL_VERIFYPEER => true,
                CURLOPT_VERBOSE        => true,
                CURLOPT_URL            => $projectUrl,
                CURLOPT_POST           => false,
                CURLOPT_HTTPHEADER     => array(
                    'Content-Type:application/text',
                ),
                CURLOPT_TIMEOUT        => 10,
                CURLOPT_CONNECTTIMEOUT => 10,
            );
            curl_setopt_array($ch , $options);
            $output = curl_exec($ch);
            $output = json_decode($output);
            $text = '';
            $this->ProjectTester->create();
            $issueWasRegistered = $this->ProjectTester->find('first', array('conditions'=>array('ProjectTester.domain'=>$project, 'ProjectTester.report_closed IS NULL')));
//            pr(date('Y-m-d H:i:s', $issueWasRegistered['ProjectTester']['message_send_time']));
//            pr(date('Y-m-d H:i:s', time()));
            if(is_object($output) && isset($output->created)){//reiskia serveris pasiekiamas, tikriname ar iraso laikas nera pasenes (tais atvejais jei pvz servas gyvas bet cronas neveikia ir servas neuzklaisneja saves)
                if(strtotime($output->created) < time()-600){ //jei irasai neina ilgiau kaip 10 minutes
                    if(empty($issueWasRegistered) || !$issueWasRegistered['ProjectTester']['message_has_been_sent']){
                        if(!empty($issueWasRegistered)){ $this->ProjectTester->id = $issueWasRegistered['ProjectTester']['id']; }
                        $problemDescription = 'negaunami irasai nuo '.$output->created;
                        $this->ProjectTester->save(array(
                            'domain' => $project,
                            'message_has_been_sent' => 1,
                            'problem_description'=>$problemDescription
                        ));
                        $text .= $project.' '.$problemDescription."\n";
                    }
                }elseif(!empty($issueWasRegistered)){ //Reiskia irasu siuntimas atsigavo, todel saliname si domena is probleminiu saraso
                    if($issueWasRegistered['ProjectTester']['message_has_been_sent']) {
                        $text .= $project.' problema pasalinta' . "\n";
                        $this->ProjectTester->updateAll(array('ProjectTester.report_closed'=>'\''.date('Y-m-d H:i:s').'\''),array('ProjectTester.id'=>$issueWasRegistered['ProjectTester']['id']));
                    }else{
                        $this->ProjectTester->deleteAll(array('ProjectTester.id'=>$issueWasRegistered['ProjectTester']['id']));
                    }
                }
            }elseif(empty($issueWasRegistered)){ //jei serveris nepasiekiamas arba grazino ne json, siusime pranesima po x min jei vis dar bus problema
                $this->ProjectTester->save(array(
                    'domain'=>$project,
                    'message_send_time'=>time()+$allowServerDowntime,
                    'problem_description'=>'serveris nepasiekiamas'
                ));
            }elseif(!$issueWasRegistered['ProjectTester']['message_has_been_sent'] && (int)$issueWasRegistered['ProjectTester']['message_send_time'] < time()){
                $this->ProjectTester->id = $issueWasRegistered['ProjectTester']['id'];
                $this->ProjectTester->save(array(
                    'message_has_been_sent' => 1,
                ));
                $text .= $project.' serveris nepasiekiamas'."\n";
            }
        }
        if(trim($text)){
            $this->Mail->send_proxy_sms($text);
        }
        die();
    }



}