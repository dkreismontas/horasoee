<?php
App::uses('Record', 'Model');
App::uses('Product', 'Model');
class ProductsController extends AppController {


	public $uses = array('Sensor','Product');
	public $components = array('Session');
    
    /** @requireAuth Importuoti produktus */
	public function import(){
	    $this->requestAuth(true);
        if (!$this->request->is('post')) {
            die("");
        }
        $file = $this->request->form['doc'];
        if(!trim($file['tmp_name']) || pathinfo($file['name'], PATHINFO_EXTENSION) != 'xml'){
            $this->Session->setFlash(__('Neparinktas failas arba netinkamas failo formatas'), 'default', array(), 'saveMessage');
            $this->redirect('/products/index');
        }
        $xml = simplexml_load_file($file['tmp_name']);
        $xmlArray = Xml::toArray($xml);
        $importProducts = $xmlArray['PRODUCTS']['PRODUCT']; 
        if(!isset($importProducts[0])) $importProducts = array($importProducts);
        $importProducts = array_change_key_case($importProducts,CASE_LOWER);
        $importProductsFixed = array();
        foreach($importProducts as $ip) {
            $importProductsFixed[] = array_change_key_case($ip,CASE_LOWER);
        }
        $importProducts = $importProductsFixed;
        $production_codes = array();
        foreach($importProducts as $product) {
            $production_codes[] = $product['production_code'];
        }
        $existingProducts = $this->Product->find('list',array('fields'=>array('production_code','id'),'conditions'=>array('production_code'=>$production_codes)));
        foreach($importProducts as &$product) {
            if(array_key_exists($product['production_code'],$existingProducts)) {
                $product['id'] = (int)$existingProducts[$product['production_code']];
            }
        }
        $this->Product->saveAll($importProducts);
        $this->Session->setFlash(__('Produktai importuoti ir atnaujinti'), 'default', array(), 'saveMessage');
        $this->redirect('/products/index');
    }

	/** @requireAuth Peržiūrėti produktus */
	public function index() {
        $this->requestAuth(true);
        $limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
        if (!$limit) $limit = 100;

        $list = $this->Product->find('all', array('conditions'=>array(''),'order'=>array('Product.id ASC')));
        $availableSensors = Configure::read('user')->selected_sensors;
        $list = array_filter($list, function($product)use($availableSensors){
            return !empty(array_intersect(explode(',',$product['Product']['work_center']), $availableSensors)) || !$product['Product']['work_center'];
        });
        $viewVars = array(
            'title_for_layout' => __('Produktai'),
            'list' => $list,
            'newUrl' => Router::url('0/edit'),
            'editUrl' => Router::url('%d/edit'),
            'removeUrl' => Router::url('%d/remove'),
            'filterUrl' => Router::url(array('controller' => 'products', 'action' => 'index', 'limit' => $limit, 'sensorId' => '__DATA__')),
            'removeMessage' => __('Ar tikrai norite pašalinti šį įrašą?'),
            'sensorsList'=>$this->Sensor->find('list', array('fields'=>array('id','name'), 'order'=>array('name')))
        );
		$this->set($viewVars);
        $parameters = array(&$this, &$viewVars);
        $pluginData = $this->Help->callPluginFunction('Products_Index_Hook', $parameters, Configure::read('companyTitle'));
	}
    
    /** @requireAuth Redaguoti produktus */
    public function edit() {
        $this->requestAuth(true);
        $listUrl = Router::url(array('controller' => 'products', 'action' => 'index'), true);
        $refUri = $this->request->referer(true);
        
        if(isset($this->request->params['id'])){
            $id = $this->request->params['id'];
            $item = $this->Product->findById($id);
            if($item) $item['Product']['work_center'] = explode(',', $item['Product']['work_center']);
        }else{
            $item = array(); $id = 0;
        }
        if (empty($this->request->data)) {
            $this->request->data = $item;
        } else {
            $errors = $this->Product->checkDataCorrectivity($this->request->data);
            if(empty($errors)) {
                if (isset($this->request->data['Product']['work_center'])) {
                    $this->request->data['Product']['work_center'] = is_array($this->request->data['Product']['work_center']) ? implode(',', $this->request->data['Product']['work_center']) : $this->request->data['Product']['work_center'];
                }
                $productExist = $this->Product->find('first', array('conditions' => array(
                    'Product.production_code' => $this->request->data['Product']['production_code'],
                    'Product.work_center' => $this->request->data['Product']['work_center'],
                )));
                if ($productExist && isset($this->request->data['Product']['id']) && $productExist['Product']['id'] == $this->request->data['Product']['id']) {
                    $productExist = array();
                }
                if (empty($productExist) && $this->Product->save($this->request->data)) {
                    $this->Session->setFlash(__('Įrašas išsaugotas'), 'default', array(), 'saveMessage');
                    $this->redirect($listUrl);
                } else {
                    $this->Session->setFlash(__('ĮRAŠO IŠSAUGOTO NEPAVYKO. Gaminio kodas %s jau yra įtrauktas į gaminių sąrašą nurodytam darbo centrui.', $this->request->data['Product']['production_code']), 'default', array(), 'saveMessage');
                    $this->redirect($listUrl);
                }
            }else{
                //$this->request->data = $item = $this->Product->findById($id);
                $this->Session->setFlash(implode('<br />',$errors), 'default', array(), 'saveMessage');
            }
        }

        $title = $item ? sprintf(__('Gaminys %s (ID: %d)'), '', $item['Product']['id']) : __('Naujas gaminys');
        $viewVars = array(
            'title_for_layout' => $title,
            'h1_for_layout' => $title,
            'model' => 'Product',
            'item' => $item,
            'listUrl' => ($refUri ? Router::url($refUri, true) : $listUrl),
            'formUrl'=> Router::url(($id ? $id : 0).'/edit', true),
            'sensorsList'=>$this->Sensor->getAsSelectOptions(false, array('Sensor.pin_name <>'=>''))
        );
        $this->set($viewVars);
        $parameters = array(&$this, &$viewVars);
        $pluginData = $this->Help->callPluginFunction('Products_Edit_Hook', $parameters, Configure::read('companyTitle'));
    }

    /** @requireAuth Pašalinti produktus */
    public function remove() {
        $this->requestAuth(true);

        $id = $this->request->params['id'];
        if ($this->Product->delete($id,false)) {
            $this->Session->setFlash(__('Įrašas pašalintas'), 'default', array(), 'saveMessage');
        } else {
            $this->Session->setFlash(__('Nepavyko pašalinti įrašo'), 'default', array(), 'saveMessage');
        }
        $refUri = $this->request->referer(true);
        if ($refUri && $refUri != '/') {
            $this->redirect(Router::url($refUri, true));
        } else {
            $this->redirect('/products/index');
        }
    }
	
}
