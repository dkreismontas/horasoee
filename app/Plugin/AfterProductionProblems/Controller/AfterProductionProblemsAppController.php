<?php

App::uses('AppController', 'Controller');

class AfterProductionProblemsAppController extends AppController {

    public function beforeFilter() {
        $this->createTable();
        parent::beforeFilter();
    }

    public function index() {
        $this->loadModel('Problem');
        $this->loadModel('AfterProductionProblem');
        $this->AfterProductionProblem->bindModel(array('belongsTo'=>array('Problem')));
        $list = $this->AfterProductionProblem->find('all',array('id','problem_id'));
        $alreadyInListProblems = $this->AfterProductionProblem->find('list',array('fields'=>array('problem_id')));
        $alreadyInListProblems[] = 0;
        $alreadyInListProblems[] = 3;
        $problemOptions = $this->Problem->find('all',array('conditions'=>array('id !='=>$alreadyInListProblems, 'id >'=>Problem::ID_NOT_DEFINED)));
        array_walk($problemOptions, function(&$data){
            $data['Problem']['name'] = Settings::translate($data['Problem']['name']);
        });
        $this->set(array(
            'h1_for_layout'    => __('Pogamybinės prastovos'),
            'list'=>$list,
            'problemOptions'=>$problemOptions,
            'title_for_layout' => _('Pogamybinės prastovos'),
        ));
    }

    public function add() {
        $this->loadModel('AfterProductionProblem');
        $problem_id = $this->request->data['problem_id'];
        $exists = $this->AfterProductionProblem->find('first',array('conditions'=>array('problem_id'=>$problem_id)));
        if(!empty($exists)) {
            $this->redirect('/AfterProductionProblems/');
        }

        $this->AfterProductionProblem->save(array('problem_id'=>$problem_id));
        $this->redirect('/AfterProductionProblems/');
    }

    public function remove() {
        $this->loadModel('AfterProductionProblem');
        $this->AfterProductionProblem->delete($this->request->params['id'],false);
        $this->redirect('/AfterProductionProblems/');
    }


    private function createTable() {
        $this->loadModel('Problem');
        $this->Problem->query('
        CREATE TABLE IF NOT EXISTS `after_production_problems` (
          `id` INT(11) PRIMARY KEY AUTO_INCREMENT,
          `problem_id` INT(11) UNIQUE KEY
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }
}