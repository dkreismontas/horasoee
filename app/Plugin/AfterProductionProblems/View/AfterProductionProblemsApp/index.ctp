<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>

<table class="table">
    <tr>
        <th><?php echo __('Prastova'); ?></th>
        <th><?php echo __('Veiksmai'); ?></th>
    </tr>
    <?php
    foreach ($list as $item) { ?>
        <tr>
            <td><?php echo $item['Problem']['name']; ?></td>
            <td><?php if(!in_array($item['Problem']['id'],array(0,1,2,3))) { ?>
                <a href="<?php echo $item['AfterProductionProblem']['id']?>/remove"><?php echo __('Pašalinti'); ?></a>
                <?php } ?></td>
        </tr>
    <?php } ?>
</table>

<h2><?php echo __('Pridėti prastovą') ?></h2>
<form action="/AfterProductionProblems/add" method="POST">
    <select name="problem_id">
        <?php foreach ($problemOptions as $problem) { ?>
            <option value="<?php echo $problem['Problem']['id']; ?>"><?php echo $problem['Problem']['name']; ?></option>
        <?php } ?>
    </select>
    <input type="submit" value="<?php echo __('Pridėti'); ?>"/>
</form>