<?php

Router::connect('/AfterProductionProblems', array('plugin'=>'AfterProductionProblems','controller' => 'AfterProductionProblemsApp', 'action' => 'index'));
Router::connect('/AfterProductionProblems/add', array('plugin'=>'AfterProductionProblems','controller' => 'AfterProductionProblemsApp', 'action' => 'add'));
Router::connect('/AfterProductionProblems/:id/remove', array('plugin'=>'AfterProductionProblems','controller' => 'AfterProductionProblemsApp', 'action' => 'remove'));