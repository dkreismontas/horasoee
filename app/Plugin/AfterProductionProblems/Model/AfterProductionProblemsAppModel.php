<?php

App::uses('AppModel', 'Model');

class AfterProductionProblemsAppModel extends AppModel {

    public static function getAllAfterProductionProblems() {
        $aprodp = ClassRegistry::init('AfterProductionProblem');
        $aprodp->bindModel(array('belongsTo'=>array('Problem')));
        $all = $aprodp->find('all',array('conditions'=>array('Problem.id IS NOT NULL')));
        $return = array();
        foreach($all as $app) {
            $return[] = array(
                'problem_id'=>$app['Problem']['id'],
                'name'=>$app['Problem']['name']
            );
        }
        return $return;
    }

    public static function getProblemTypeLinks() {
        return '<a href="/AfterProductionProblems" style="float:right;margin-right:10px;color:white" class="btn btn-primary">'.__('Pogamybinės prastovos').'</a>';
    }
}
