<?php
App::uses('AppModel', 'Model');

class StronglasasAppModel extends AppModel {
    public static $navigationAdditions = array( );
    
    public function getWCSidebarLinks(){
        $javascript = '<script type="text/javascript">
        jQuery(document).ready(function($){
           if(window.sensorId == 36){
               $("#timePager div.btn-group").after(\'<ul id="legendsExplanation">\'+         
                    \'<li><span class="tml-gitem-green"></span>Multiglass</li>\'+ 
                    \'<li><span class="tml-gitem-braun"></span>Glassline</li>\'+ 
                    \'<li><span class="tml-gitem-lightblue"></span>Fleishle</li>\'+ 
                    \'<li><span class="tml-gitem-pink"></span>Multiglass + Glassline</li>\'+ 
                    \'<li><span class="tml-gitem-electric"></span>Multiglass + Fleishle</li>\'+ 
                    \'<li><span class="tml-gitem-lightgreen"></span>Glassline + Fleishle</li>\'+ 
                \'</ul>\');
           }else if(window.sensorId == 32){
               $("#timePager div.btn-group").after(\'<ul id="legendsExplanation">\'+         
                    \'<li><span class="tml-gitem-green"></span>Raižymas</li>\'+ 
                    \'<li><span class="tml-gitem-braun"></span>Dangos nuėmimas</li>\'+ 
                \'</ul>\');
           }else if(window.sensorId == 37){
               $("#timePager div.btn-group").after(\'<ul id="legendsExplanation">\'+         
                    \'<li><span class="tml-gitem-green"></span>Lami</li>\'+ 
                    \'<li><span class="tml-gitem-braun"></span>Float</li>\'+ 
                    \'<li><span class="tml-gitem-lightblue"></span>Nuėmimas</li>\'+ 
                \'</ul>\');
           }
        });
        </script>';
        return $javascript;
    }
    //<ul id="legendsExplanation"><li><span class="tml-gitem-green"></span>Multiglass</li></ul>
    //<li><span class="tml-gitem-green"></span>Multiglass</li>
    // <li><span class="tml-gitem-braun"></span>Glassline</li>
            // <li><span class="tml-gitem-lightblue"></span>Fleishle</li>
            // <li><span class="tml-gitem-pink"></span>Multiglass + Glassline</li>
            // <li><span class="tml-gitem-electric"></span>Multiglass + Fleishle</li>
            // <li><span class="tml-gitem-lightgreen"></span>Glassline + Fleishle</li>
}
