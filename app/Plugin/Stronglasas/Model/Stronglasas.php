<?php
App::uses('Problem', 'Model');
class Stronglasas extends AppModel{

    public $useTable = false;
     
     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }
     
     public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
         if($currentPlan){
             $recordModel = ClassRegistry::init('Record');
             $fItems = $recordModel->find('first', array(
                'fields'     => array('SUM(Record.unit_quantity) AS total'),
                'conditions' => array(
                    'Record.approved_order_id' => $currentPlan['ApprovedOrder']['id'],
                    'Record.unit_quantity >' => 0,
                    'Record.shift_id' => $fShift['Shift']['id'],
                )
            ));
            $quantityOutput['quantity'] = 0;
            if($fItems){
                $quantityOutput['quantity'] = $fItems['0']['total'];
            }
         }
     }
     
     //naudojant addDataToVirtualSensor darbas uzdaromas kai tik pasikeicia kieki siuncianciu centru kiekis. Gali naujas centras prisijungti, gali pradeti kitas siusti ir pan, visais atvejais kuriams naujas darbas
     public function addDataToVirtualSensor($sensorId, $combinedSensorsPins, $record){
         static $foundProblemModel = null;
         if($foundProblemModel == null){
             $foundProblemModel = ClassRegistry::init('FoundProblem');
         }
         $combinedSensorsPins = array_flip($combinedSensorsPins);
         foreach($combinedSensorsPins as $pin => $value){
             $combinedSensorsPins[$pin] = isset($record->$pin)?$record->$pin:0;
         }
         $currentActiveSensorPin = array_filter($combinedSensorsPins, function($value){ return $value > 0; });
         if(!empty($currentActiveSensorPin)){
             //Pirma ieskome kokiam sensoriui priklauso paskutinis darbas ir ziurime ar kitas sensorius siuo metu neturi kiekio. Jei kiekis yra, darba reikia uzdaryti, kad irasymo metu susikurti naujas darbas
             $lastActiveWork = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$sensorId,'FoundProblem.state'=>1,'FoundProblem.problem_id'=>Problem::ID_WORK), 'order'=>array('FoundProblem.id DESC')));
             if(!empty($lastActiveWork)){
                 $jsonData = json_decode($lastActiveWork['FoundProblem']['json'],true);
                 $prevWorkSensorPin = isset($jsonData['work_belongs_to'])?explode('_',$jsonData['work_belongs_to']):0;
                 $otherSensorsHasQuantity = array_diff_key($currentActiveSensorPin, array_flip($prevWorkSensorPin));
                 if(!empty($otherSensorsHasQuantity) || sizeof($currentActiveSensorPin) != sizeof($prevWorkSensorPin)){//Jei kitas sensorius nei paskutinio darbo ateina su kiekiu, darba reikia uzdaryti cia, kad irasymo metu susikurti naujas darbas
                     $foundProblemModel->save(array('id'=>$lastActiveWork['FoundProblem']['id'],'state'=>2));
                     //$currentActiveSensorPin = $otherSensorsHasQuantity;
                 }
             }
         }
         $quantity = !empty($currentActiveSensorPin)?current($currentActiveSensorPin):0;
         $foundProblemModel->query('CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');');
         //vel ieskome paskutinio darbo, kadangi po irasymo gali susikurti nauajas darbo irasas, kuriam reikia priskirti sensoriu, jei tai dar neatlikta
         $lastActiveWork = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$sensorId,'FoundProblem.state'=>1,'FoundProblem.problem_id'=>Problem::ID_WORK), 'order'=>array('FoundProblem.id DESC')));
         if(!empty($lastActiveWork)){
             $jsonData = json_decode($lastActiveWork['FoundProblem']['json'],true);
             if(!isset($jsonData['work_belongs_to'])){
                 $jsonData['work_belongs_to'] = implode('_',array_keys($currentActiveSensorPin));
                 $foundProblemModel->save(array('id'=>$lastActiveWork['FoundProblem']['id'],'json'=>json_encode($jsonData)));
             }
         }
     }
    
    //naudojant addDataToTwoVirtualSensor irasyma vienu metu ateina kiekis is abieju centru esamas darbas uzdaromas ir iskart atidaromas darbas naujam darbo centrui, kuris pries tai kiekio dar nesiunte. 
    //Vienu metu gali pareiti kiekis tik perejimo metu is vieno centro i kita t.y. ne ilgiau kaip vieno intervalo ribose 20s
    public function addDataToTwoVirtualSensor($sensorId, $combinedSensorsPins, $record){
         static $foundProblemModel = null;
         if($foundProblemModel == null){
             //$recordModel = ClassRegistry::init('Record');
             $foundProblemModel = ClassRegistry::init('FoundProblem');
         }
         $combinedSensorsPins = array_flip($combinedSensorsPins);
         foreach($combinedSensorsPins as $pin => $value){
             $combinedSensorsPins[$pin] = isset($record->$pin)?$record->$pin:0;
         }
         arsort($combinedSensorsPins);
         $currentActiveSensorPin = key($combinedSensorsPins);
         //Pirma ieskome kokiam sensoriui priklauso paskutinis darbas ir ziurime ar kitas sensorius siuo metu neturi kiekio. Jei kiekis yra, darba reikia uzdaryti, kad irasymo metu susikurti naujas darbas
         $lastActiveWork = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$sensorId,'FoundProblem.state'=>1,'FoundProblem.problem_id'=>Problem::ID_WORK), 'order'=>array('FoundProblem.id DESC')));
         $prevDataSensorId = 0;
         if(!empty($lastActiveWork)){
             $jsonData = json_decode($lastActiveWork['FoundProblem']['json'],true);
             $prevWorkSensorPin = isset($jsonData['work_belongs_to'])?$jsonData['work_belongs_to']:0;
             $otherSensors = array_filter($combinedSensorsPins, function($pin)use($prevWorkSensorPin){
                 return $pin != $prevWorkSensorPin;
             }, ARRAY_FILTER_USE_KEY); 
             if(current($otherSensors) > 0){//Jei kitas sensorius nei paskutinio darbo ateina su kiekiu, darba reikia uzdaryti cia, kad irasymo metu susikurti naujas darbas
                 $foundProblemModel->save(array('id'=>$lastActiveWork['FoundProblem']['id'],'state'=>2));
                 $currentActiveSensorPin = key($otherSensors);
             }
         }
         $quantity = current($combinedSensorsPins);
         $foundProblemModel->query('CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');');
         //vel ieskome paskutinio darbo, kadangi po irasymo gali susikurti nauajas darbo irasas, kuriam reikia priskirti sensoriu, jei tai dar neatlikta
         $lastActiveWork = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$sensorId,'FoundProblem.state'=>1,'FoundProblem.problem_id'=>Problem::ID_WORK), 'order'=>array('FoundProblem.id DESC')));
         if(!empty($lastActiveWork)){
             $jsonData = json_decode($lastActiveWork['FoundProblem']['json'],true);
             if(!isset($jsonData['work_belongs_to'])){
                 $jsonData['work_belongs_to'] = $currentActiveSensorPin;
                 $foundProblemModel->save(array('id'=>$lastActiveWork['FoundProblem']['id'],'json'=>json_encode($jsonData)));
             }
         }
     }

    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$periods){
        foreach($periods as &$period){
            if(isset($period->data['Work']['json'])){
                $jsonData = json_decode($period->data['Work']['json']);
                if($period->data['Work']['sensor_id'] == 32 && isset($jsonData->work_belongs_to) && $jsonData->work_belongs_to == 'i5'){
                    $period->type = 5;
                }
                if($period->data['Work']['sensor_id'] == 37 && isset($jsonData->work_belongs_to)){
                    if($jsonData->work_belongs_to == 'i8'){
                        $period->type = 5;
                    }elseif($jsonData->work_belongs_to == 'i10'){
                        $period->type = 'lightblue';
                    }
                }
                if($period->data['Work']['sensor_id'] == 36 && isset($jsonData->work_belongs_to)){
                    $workBelongsTo = explode('_',$jsonData->work_belongs_to);
                    if(in_array('i12', $workBelongsTo) && sizeof($workBelongsTo) == 1){
                        $period->type = 'green';
                    }elseif(in_array('i11', $workBelongsTo) && sizeof($workBelongsTo) == 1){
                        $period->type = 'braun';
                    }elseif(in_array('i9', $workBelongsTo) && sizeof($workBelongsTo) == 1){
                        $period->type = 'lightblue';
                    }elseif(sizeof(array_intersect($workBelongsTo, array('i11','i12'))) == 2){
                        $period->type = 'pink';
                    }elseif(sizeof(array_intersect($workBelongsTo, array('i9','i12'))) == 2){
                        $period->type = 'electric';
                    }elseif(sizeof(array_intersect($workBelongsTo, array('i9','i11'))) == 2){
                        $period->type = 'lightgreen';
                    }
                }
            }
        }
    }

    public function WorkCenter_AddOrderBuildTimeGraph_Hook(&$fApprovedOrders, &$periods, $fRecords, $fFoundProblems, $fSensor){
        if(in_array($fSensor['Sensor']['id'], array(32,36,37))){
            return true;
        }
    }
    
    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Stronglasas/css/Stronglasas_bare.css';
    }

    public function View_DiffCardsIndex_TableFormation_Hook(&$columns, $li_){
         if(isset($columns['loss_type'])){
             $columns['loss_type'][0] = __('Neatitikties tipas');
         }
    }
     
}
    