<?php
class Technogaja extends AppModel{
        
    public function Record_calculateOee_Hook(&$oeeParams, $data){
        if(isset($data['sensor_id']) && in_array($data['sensor_id'], array(4,5,11,12,13,14))){
            $oeeParams['operational_factor'] = 1;
            $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
            $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
        }
    }
      
}
    