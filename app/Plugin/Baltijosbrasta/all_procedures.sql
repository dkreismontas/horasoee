-- 
DROP FUNCTION IF EXISTS GET_UNIT_QUANTITY;
DELIMITER $$

CREATE  FUNCTION `GET_UNIT_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS decimal(12,4) NO SQL
BEGIN
	DECLARE `product_length` DECIMAL(12,4) DEFAULT NULL;
	SELECT `p`.`product_length` INTO `product_length` FROM `plans` AS `p` WHERE `p`.`id` = `active_plan_id` LIMIT 1;
	SET `product_length` = IFNULL(`product_length`,1);
	SET `product_length` = IF(`product_length` = 0, 1, `product_length`);
	RETURN `quantity` / `product_length`;
END$$

DELIMITER ;
