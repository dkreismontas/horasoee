<?php
App::uses('BaltijosbrastaAppController', 'Baltijosbrasta.Controller');

class PluginsController extends BaltijosbrastaAppController {
	
    public $uses = array('Shift','Record','Sensor','ApprovedOrder');
    public $helpers = array('Barcode');

    public function index(){
        $this->layout = 'ajax';
        $current_page = trim(urldecode($_GET['current_page']),'/');
        if(preg_match('/.*dashboards\/inner.*/i',trim($current_page,'/'))){
            $branchModel = ClassRegistry::init('Branch');
            $this->set(array(
                'branches'=>$branchModel->getAsSelectOptions(false)
            ));
        }
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Baltijosbrasta.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if(trim($ipAndPort)) {
            $shell->args = array(str_replace('_',':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }


    public function print_sticker($orderId){
        $this->layout = 'full_page';
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $this->set(array(
            'order' => $this->ApprovedOrder->findById($orderId)
        ));
    }

//    function save_sensors_data(){
//        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start.txt';
//        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 7200){
//            $this->Sensor->informAboutProblems();
//            die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
//        }
//        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
//        $db = ConnectionManager::getDataSource('default');
//        $branches = array_unique(array_values($sensorsListInDB));
//        foreach($branches as $branchId){
//            //jei reikia importuojame pamainas
//            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
//            if(!$currentShift){
//                App::import('Controller', 'Cron');
//                $CronController = new CronController;
//                $CronController->generateShifts();
//                break;
//            }
//        }
//        //vykdome iraso iterpima i DB
//        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
//        $username = 'horas';
//        $password = '%Q}8d~@Uz$%3kmb6';
//        $context = stream_context_create(array (
//            'http' => array (
//                'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
//            )
//        ));
//        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
//        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
//        //$saveSensors = array_intersect($sensorsListInDB, $saveSensors);
//        $db = ConnectionManager::getDataSource('default');
//        $adressList = array(
//            '5580'=>'http://88.119.196.228:5580/data',
//            '5680'=>'http://88.119.196.228:5680/data',
//            '5780'=>'http://88.119.196.228:5780/data',
//        );
//        $mailOfErrors = array();
//        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
//        fwrite($fh, 1);
//        fclose($fh);
//        $this->Sensor->executeFailedQueries();
//        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
//        $activePlans = $this->ApprovedOrder->find('all',array('fields'=>array('Plan.*','ApprovedOrder.sensor_id'),'conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS),'group'=>array('ApprovedOrder.sensor_id')));
//        $activePlans = Hash::combine($activePlans, '{n}.ApprovedOrder.sensor_id', '{n}.Plan');
//        foreach($adressList as $addressPort => $adress){
//            //$data = json_decode(file_get_contents($adress, false, $context));
//            $ch = curl_init($adress);
//            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
//            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
//            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
//            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
//                'Authorization: Basic ' . base64_encode("$username:$password")
//            ));
//            $data = curl_exec($ch);
//            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
//            if(curl_errno($ch) == 0 AND $http == 200) {
//                $data = json_decode($data);
//            }else{
//                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
//                continue;
//            }
//            foreach($data->periods as $record){
//                foreach($saveSensors as $sensorId => $title){
//                    list($sensorName, $sensorPort) = explode('_',$title);
//                    if($addressPort != $sensorPort) continue;
//                    if(!isset($record->$sensorName)) continue;
//                    $quantity = round($record->$sensorName / 1000, 2);
//                    if(isset($activePlans[$sensorId])){
//                        $quantity = bcmul($quantity, $activePlans[$sensorId]['tuta_factor'],6);
//                    }
//                    try{
//                        if(Configure::read('ADD_FULL_RECORD')){
//                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
//                        }else{
//                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';
//                        }
//                        $db->query($query);
//                    }catch(Exception $e){
//                        $this->Sensor->logFailedQuery($query);
//                        $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
//                    }
//                }
//            }
//        }
//        unlink($dataPushStartMarkerPath);
//        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
//        if(!empty($mailOfErrors)){
//             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
//        }elseif(file_exists($dataNotSendingMarkerPath)){
//            unlink($dataNotSendingMarkerPath);
//        }
//        //sutvarkome atsiustu irasu duomenis per darbo centra
//        $this->Sensor->moveOldRecords();
//        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
//        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
//            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
//            fwrite($fh, 1);
//            fclose($fh);
//            $this->Sensor->informAboutProblems();
//            $this->requestAction('/work-center/update-status?update_all=1');
//            unlink($markerPath);
//        }
//        session_destroy();
//        die();
//    }

}
