<?php
App::uses('Problem', 'Model');
class ReportsController extends BaltijosbrastaAppController{

    public $uses = array('OrderCalculation', 'Baltijosbrasta.Report','PartialQuantity','Problem');

    public function generate_production_report(){
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($dateStart,$dateEnd) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $dateStart = $this->request->data['start_date'];
            $dateEnd = $this->request->data['end_date'];
        }
        $dateStartAdded = date('Y-m-d H:i:s', strtotime('-1 day',strtotime($dateStart)));
        if(empty($this->request->data['branches'])) {
            $this->Session->setFlash(__('Neparinktas padalinys'));
            $this->redirect(array('plugin'=>false,'controller'=>'dashboards','action'=>'inner'));
        }
        $this->OrderCalculation->bindModel(array('belongsTo'=>array(
            'Shift', 'Sensor', 'User', 'ApprovedOrder',
            'Branch'=>array('foreignKey'=>false, 'conditions'=>array('Shift.branch_id = Branch.id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
        )));
        $orders = $this->OrderCalculation->find('all', array(
            'conditions'=>array(
                'Shift.start <' => $dateEnd,
                'Shift.end >' => $dateStartAdded ,
                'Branch.id' => $this->request->data['branches'],
                'OrderCalculation.user_id >'=>0,
                'OrderCalculation.quantity >'=>0,
            ),'order'=>array('Sensor.ps_id', 'Shift.start', 'Shift.ps_id', 'User.id')
        ));
        if(empty($orders)){
            $this->Session->setFlash(__('Nėra duomenų'));
            $this->redirect(array('plugin'=>false,'controller'=>'dashboards','action'=>'inner'));
        }
        $partialQuantities = $this->PartialQuantity->find('all', array('conditions'=>array(
            'PartialQuantity.created BETWEEN \''.$dateStartAdded.'\' AND \''.$dateEnd.'\' ',
        )));
        $this->Report->attachOvertimes($orders, $dateStart, $partialQuantities);
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(array(
            'orders' => $orders,
            'objPHPExcel'=>$objPHPExcel,
            'problemsIdToName'=>$this->Problem->find('list', array('fields'=>array('Problem.id','Problem.name'))),
            'constructData' => array(//pirmas indeksas nusako padaliniui skirtas taisykles. Jei egzistuoja indeksas su 0 indeksu, tinka visiem padaliniam
                __('Operatoriai')=>$this->Report->constuctData($orders, array(
                    0=>array(array('User.first_name', 'User.last_name'), '%1$s %2$s')
                )),
                __('Įrenginiai')=>$this->Report->constuctData($orders, array(
                    0=>array(array('Sensor.ps_id'), '%1$s')
                )),
                __('Gaminiai') => $this->Report->constuctData($orders, array(
                    2=>array(array('Plan.sieneles_plotis_1','Plan.sieneles_plotis_2'),'%1$d x %2$d'),
                    3=>array(array('Plan.tutos_vidinis_diametras_mm'), '%1$s')
                ))
            )
        ));
    }

}