<?php
class Report extends BaltijosbrastaAppModel{

    //Užsakymas prikaluso tai pamainai, kurioje jis buvo pradėtas, nebent užsakymas pradėtas likus 30 minučių iki pamainos pabaigos ir buvo uzbaigtas kitoje pamainoje
    public function attachOvertimes(Array &$orders, String $originalStart, Array $partialQuantities){
        $allowDifference = 1800;
        foreach($orders as &$order){
            if(strtotime($order['Shift']['end'] < strtotime($originalStart))){ continue; }  //kai apskaiciuoto uzsakymo pamainos pabaiga mazesne mazesne uz uzduoto intervalo pradzia, tai yra uzsakymas uz intervalo ribu, skirtas prideti virsvalandi, todel su juo nedirbame
            $orderStartInPrevShift = array_filter($orders, function($filterOrder)use($order, $allowDifference){
                $diff = strtotime($order['Shift']['start']) - strtotime($filterOrder['ApprovedOrder']['created']);
                if($diff <= 0){ return false; } //uzsakymas pradetas veliau nei esamo uzsakymo pamainos pradzia
                return
                    $order['ApprovedOrder']['plan_id'] == $filterOrder['ApprovedOrder']['plan_id'] &&
                    $order['ApprovedOrder']['id'] != $filterOrder['ApprovedOrder']['id'] &&
                    $diff <= $allowDifference;  //nuo uzsakymo pradzios iki esamos uzsakymo pamainos pradzios negali buti praeje daugiau kaip 30min
            });
            $order['OrderCalculation']['problems'] = json_decode($order['OrderCalculation']['problems']);
            if(!is_object($order['OrderCalculation']['problems'])) {
                $order['OrderCalculation']['problems'] = new stdClass();
            }
            $order['OrderCalculation']['shifts_ids'] ??= array($order['OrderCalculation']['shift_id']);
            if(!empty($orderStartInPrevShift)){
                $orderStartInPrevShift = current($orderStartInPrevShift);
                $order['OrderCalculation']['order_start'] = $orderStartInPrevShift['OrderCalculation']['order_start'];
                $order['OrderCalculation']['quantity'] += $orderStartInPrevShift['OrderCalculation']['quantity'];
                $order['OrderCalculation']['time_green'] += $orderStartInPrevShift['OrderCalculation']['time_green'];
                $order['OrderCalculation']['time_yellow'] += $orderStartInPrevShift['OrderCalculation']['time_yellow'];
                $order['OrderCalculation']['time_red'] += $orderStartInPrevShift['OrderCalculation']['time_red'];
                $order['OrderCalculation']['shifts_ids'][] = $orderStartInPrevShift['OrderCalculation']['shift_id'];
                if(!is_object($orderStartInPrevShift['OrderCalculation']['problems'])) {
                    $orderStartInPrevShift['OrderCalculation']['problems'] = json_decode($orderStartInPrevShift['OrderCalculation']['problems']);
                    if(!is_object($orderStartInPrevShift['OrderCalculation']['problems'])){
                        $orderStartInPrevShift['OrderCalculation']['problems'] = new stdClass();
                    }
                }
                foreach($orderStartInPrevShift['OrderCalculation']['problems'] as $problemId => $problem){
                    if(!isset($order['OrderCalculation']['problems']->$problemId)){
                        $order['OrderCalculation']['problems']->$problemId = $problem;
                    }else {
                        $order['OrderCalculation']['problems']->$problemId->length += $problem->length;
                    }
                }
            }
            $partialQuantity = array_filter($partialQuantities, function($partialQuantity)use($order){
                return
//                    strtotime($partialQuantity['PartialQuantity']['created']) > strtotime($order['OrderCalculation']['order_start']) &&
//                    strtotime($partialQuantity['PartialQuantity']['created']) < strtotime($order['OrderCalculation']['order_end']) &&
                    in_array($partialQuantity['PartialQuantity']['shift_id'],$order['OrderCalculation']['shifts_ids']) &&
                    $partialQuantity['PartialQuantity']['approved_order_id'] == $order['OrderCalculation']['approved_order_id'];
            });
            $order['partialPiecesSum'] = Set::extract('/PartialQuantity/pieces_quantity', $partialQuantity);
            $order['partialPiecesSum'] = !empty($order['partialPiecesSum'])?array_sum($order['partialPiecesSum']):0;
            $order['partialQuantitySum'] = Set::extract('/PartialQuantity/quantity', $partialQuantity);
            $order['partialQuantitySum'] = !empty($order['partialQuantitySum'])?array_sum($order['partialQuantitySum']):0;
        }
        //atmetame netinkamus uzsakymus
        $orders = array_filter($orders, function($order)use($originalStart, $allowDifference){
            $diffOrderStartToShiftEnd = strtotime($order['Shift']['end']) - strtotime($order['OrderCalculation']['order_start']);//laikas nuo uzsakymo pradzios iki pamainos galo
            $diffOrderEndToShiftEnd = strtotime($order['Shift']['end']) - strtotime($order['ApprovedOrder']['end']);//laikas nuo uzsakymo pabaigos iki pamainos galo
            return
                strtotime($order['OrderCalculation']['order_end']) > strtotime($originalStart) &&   //uzsakymas priklauso uzduotam intervalui (intervalas buvo prailgintas del galimu darbu, kuriuos reikia prijungti, kurie neieina i uzduota intervala)
                ($diffOrderStartToShiftEnd > $allowDifference || $diffOrderEndToShiftEnd >= 0);    //uzsakymas vykdytas pamainoje ilgiau kaip 30min arba uzbaigtas esamoje pamainoje
        });
    }

    public function constuctData(Array $orders, Array $hashParameters){
        $operatorsData = array();
        foreach($orders as $order){
            if(!in_array($order['Sensor']['branch_id'], array(2,3))){ continue; }   //Kampai ir tutos
            $branchId = $order['Sensor']['branch_id'];
            list($hashPath, $hashFormat) = $hashParameters[0]??$hashParameters[$branchId];
            $combineBy = current(Hash::format($order, $hashPath, $hashFormat));
            $month = (int)date('m', strtotime($order['Shift']['start']));
            if(!isset($operatorsData[$combineBy][$month])){
                $operatorsData[$combineBy][$month] = array(
                    'meters' => 0, 'hours' => 0, 'sensors'=>array(), 'start'=>0, 'end'=>0, 'combiner'=>!$combineBy?'':$combineBy, 'month'=>$month, 'user_id'=>$order['User']['id']
                );
            }
            $combiner = &$operatorsData[$combineBy][$month];
            $madeMetersCount = round(bcmul($order['partialPiecesSum'], $order['Plan']['product_length'],3),2);
            $workDuration = bcdiv((strtotime($order['OrderCalculation']['order_end']) - strtotime($order['OrderCalculation']['order_start'])), 3600, 6);
            $combiner['meters'] = bcadd($combiner['meters'], $madeMetersCount, 6);
            $combiner['hours'] = bcadd($combiner['hours'], $workDuration, 6);
            $combiner['sensors'] = array_unique(array_merge($combiner['sensors'], array($order['Sensor']['ps_id'])));
            if(!$combiner['start'] || $combiner['start'] > strtotime($order['OrderCalculation']['order_start'])){
                $combiner['start'] = strtotime($order['OrderCalculation']['order_start']);
            }
            if($combiner['end'] < strtotime($order['OrderCalculation']['order_end'])){
                $combiner['end'] = strtotime($order['OrderCalculation']['order_end']);
            }
        }
        $operatorsData = array_map(function($data){return array_values($data);}, array_values($operatorsData));
        $operatorsData = Hash::sort($operatorsData, '{n}.0.combiner', 'asc', array('type'=>'natural','ignoreCase'=>true));
        return $operatorsData;
    }

}