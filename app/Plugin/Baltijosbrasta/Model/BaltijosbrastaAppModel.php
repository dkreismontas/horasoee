<?php
App::uses('AppModel', 'Model');

class BaltijosbrastaAppModel extends AppModel {
    public static $navigationAdditions = array( );

    public function getWCSidebarLinks(){
        return '<a data-ng-if="currentPlan && \'baltijosbrasta\'==\''.mb_strtolower(Configure::read('companyTitle')).'\'" target="_blank" href="/baltijosbrasta/plugins/print_sticker/{{currentPlan.ApprovedOrder.id}}" title="'.__('Etiketės spausdinimas').'" class="btn btn-warning">'.__('Etiketė').'</a>';
    }
    
}
