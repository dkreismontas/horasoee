<?php
class Baltijosbrasta extends AppModel{

    public $name = 'Baltijosbrasta';
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     
     public function Products_Edit_Hook(&$productsController){
        $productsController->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Products/edit');
     }
     
    public function Products_Index_Hook(&$productsController){
        $productsController->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Products/index');
    }
    
    public function WorkCenter_BuildStep_Hook(&$fPlan, $product, $fSensor){
        if($product['Product']['tutos_vidinis_diametras'] == 0){
            $fPlan['tuta_factor'] = 1;
        }else{
            $fPlan['tuta_factor'] = round(1 / ((M_PI * $product['Product']['tutos_vidinis_diametras']) / (0.5 + $product['Product']['vidines_juostos_plotis'])), 4);
        }
        if($product['Product']['tutos_vidinis_diametras'] > 0 && $product['Product']['vidines_juostos_plotis'] > 0){
            $fPlan['tuta_factor_quantity'] = bcdiv(bcadd($product['Product']['vidines_juostos_plotis'],0.5,6), bcmul(M_PI,$product['Product']['tutos_vidinis_diametras'],6), 6);
        }else{
            $fPlan['tuta_factor_quantity'] = 1;
        }
    }
    
    public function Record_FindAndSetOrder_Hook($sensor, $approvedOrderId, $plan, $multiplier, &$fields, &$conditions){
        $fields['Record.quantity']='`quantity` * '.floatval($plan[Plan::NAME]['tuta_factor_quantity']);
        $fields['Record.unit_quantity']='`unit_quantity` * '.floatval($plan[Plan::NAME]['tuta_factor']);
    }

    public function ImportPlansXlsPlugin_prepareXlsData_Hook(&$data, $dataPreview=false){
        if(!$dataPreview){
            $planModel = ClassRegistry::init('Plan');
            $moListFromXls = Set::extract('/mo_number', $data);
            $existingPlans = $planModel->find('list', array('fields'=>array('Plan.mo_number', 'Plan.id'), 'conditions'=>array('Plan.mo_number'=>$moListFromXls)));
        }
        foreach($data as &$plan){
            if(!$plan['work_center']) continue;
            $plan['step'] = bcmul($plan['step'],60,6);
            if(strtolower(trim($plan['measuring_unit'])) == 'm'){
                $plan['quantity'] = bcdiv($plan['quantity'],$plan['unit_length'],6);
            }
            if($plan['tutos_vidinis_diametras'] > 0 && $plan['tutos_vidines_juostos_plotis'] > 0 && $plan['unit_length'] > 0){
                $plan['tuta_factor'] = bcdiv(bcadd($plan['tutos_vidines_juostos_plotis'],0.5,6), bcmul(bcmul(M_PI,$plan['tutos_vidinis_diametras'],6), $plan['unit_length'],6), 6);
            }elseif(!$plan['tutos_vidinis_diametras'] && !$plan['tutos_vidines_juostos_plotis'] && $plan['unit_length'] > 0){
                $plan['tuta_factor'] = 1/$plan['unit_length'];
            }else{
                $plan['tuta_factor'] = 1;
            }
            
            if($plan['tutos_vidinis_diametras'] > 0 && $plan['tutos_vidines_juostos_plotis'] > 0){
                $plan['tuta_factor_quantity'] = bcdiv(bcadd($plan['tutos_vidines_juostos_plotis'],0.5,6), bcmul(M_PI,$plan['tutos_vidinis_diametras'],6), 6);
            }else{
                $plan['tuta_factor_quantity'] = 1;
            }
            
            //plano unikaluma nusako parametras mo_number
            if(!$dataPreview){
                if(isset($existingPlans[$plan['mo_number']])){
                    $plan['id'] = $existingPlans[$plan['mo_number']];
                }
            }
        }
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $moNumbers = Set::extract('/Plan/mo_number', $plans);
        if(isset($currentPlan['ApprovedOrder'])){$moNumbers[] = $currentPlan['Plan']['mo_number'];}
        if(!empty($moNumbers)){
            if(!empty($moNumbers)){
                $partialQuantityModel = ClassRegistry::init('PartialQuantity');
                $partialQuantityModel->bindModel(array('belongsTo'=>array('ApprovedOrder', 'Plan'=>array('foreignKey'=>false,'conditions'=>array('ApprovedOrder.plan_id = Plan.id')))));
                $partialQuantities = Hash::combine($partialQuantityModel->find('all', array(
                    'fields'=>array('SUM(PartialQuantity.pieces_quantity) AS quantity', 'TRIM(Plan.id) AS plan_id'),
                    'conditions'=>array('Plan.mo_number'=>array_unique($moNumbers)),
                    'group'=>array('Plan.id')
                )),'{n}.0.plan_id', '{n}.0.quantity');
                if(isset($currentPlan['ApprovedOrder']) && isset($partialQuantities[$currentPlan['Plan']['id']])){
                    $currentPlan['Plan']['partial_quantity'] = $partialQuantities[$currentPlan['Plan']['id']];
                }
                foreach($plans as &$plan){
                    $planId = $plan['Plan']['id'];
                    if(isset($partialQuantities[$planId])){
                        $plan['Plan']['partial_quantity'] = $partialQuantities[$planId];
                    }
                }
            }
        }
        if(isset($currentPlan['ApprovedOrder'])){
            $currentPlan['Plan']['step_unit_divide'] = 1;
            $currentPlan['Plan']['step'] /= 60;
            $currentPlan['Plan']['step_unit_title'] = str_replace('vnt/min','m/min',$currentPlan['Plan']['step_unit_title']);
        }
        foreach($plans as &$plan){
            $plan['Plan']['step'] /= 60;
        }
        $fSensor['Sensor']['declare_using_crates_and_pieces'] = __('Palečių kiekis');
    }
    
    public function Plans_ChangeData_After_Hook(&$plansController){
        $sensorId = $plansController->request->data['Plan']['sensor_id'];
        $fSensor = $plansController->Sensor->withRefs()->findById($sensorId);
        $lineId = $fSensor['Sensor']['line_id'];
        $sensorModel = ClassRegistry::init('Sensor');
        $sameLineSensors = $sensorModel->getRelatedSensors($sensorId,$lineId,false,true);
        foreach($sameLineSensors as $sameLineSensor){
            $sensorId = $sameLineSensor['Sensor']['id'];
            $plansController->request->data['Possibility'][$sensorId] = $sameLineSensor;
        }
        $plansController->set(array(
            'columns'   => array(
                'id'=>array('title'=>'', 'type'=>'hidden'),
            ),
        ));
    }
    
    //perraso jutikliu kiekius is operatoriaus uzfiksuotu daliniu kiekiu
    /*public function OeeMpm_afterGetPlansQuantitiesSearch_Hook(&$plansQuantitiesTmp){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        $plansIdsList = Set::extract('/0/plan_id',$plansQuantitiesTmp);
        $partialQuantityModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $partials = $partialQuantityModel->find('all', array(
            'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'Plan.id'),
            'conditions'=>array('Plan.id'=>$plansIdsList),
            'group'=>array('Plan.id')
        ));
        $partials = Hash::combine($partials, '{n}.Plan.id', '{n}.0');
        foreach($plansQuantitiesTmp as $key => $plansQuantity){
            $planId = $plansQuantity[0]['plan_id'];
            $plansQuantitiesTmp[$key][0]['quantity'] = isset($partials[$planId])?$partials[$planId]['quantity']:0;
        }
    }*/

    //kiekviena uzfiksuota palete turi buti grazinama kaip atskiras irasas su kiekiu ir laiku, kada ivyko registravimas
    public function OeeMpm_afterGetDataFromOee_Hook(&$data){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        $plansIdsList = array();
        foreach($data['plans_quantities'] as $sensorPsId => $planData){
            $plansIdsList = array_merge($plansIdsList, Set::extract('/plan_id',$planData));
        }
        $partialQuantityModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder', 'User',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $piecesQuantitySumByPlanId = array();
        $cratesListByPlanId = array();
        $partials = $partialQuantityModel->find('all', array(
            'fields'=>array(
                'PartialQuantity.quantity', 'PartialQuantity.pieces_quantity', 'PartialQuantity.created', 'Plan.id', 'User.unique_number'
            ),
            'conditions'=>array('Plan.id'=>$plansIdsList),
            'order'=>array('Plan.id')
        ));
        foreach($partials as $partial){
            $planId = $partial['Plan']['id'];
            if(!isset($piecesQuantitySumByPlanId[$planId])){
                $piecesQuantitySumByPlanId[$planId] = 0;
            }
            if($partial['PartialQuantity']['quantity'] > 0) {
                $cratesListByPlanId[$planId][] = array(
                    'quantity' => $partial['PartialQuantity']['quantity'],
                    'created' => $partial['PartialQuantity']['created'],
                    'user_tab_nr' => $partial['User']['unique_number'],
                );
            }
            $piecesQuantitySumByPlanId[$planId] += $partial['PartialQuantity']['pieces_quantity'];
        }
        foreach($data['plans_quantities'] as $sensorPsId => $planData){
            foreach($planData as $key => $plan){
                $planId = $plan['plan_id'];
                $data['plans_quantities'][$sensorPsId][$key]['quantity'] = $piecesQuantitySumByPlanId[$planId]??0;
                $data['plans_quantities'][$sensorPsId][$key]['crates_quantity'] = $cratesListByPlanId[$planId]??array();
            }
        }
    }

    public function OeeMpm_afterGetWorkIntervalsSearch_Hook(&$workIntervals){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        $plansIdsList = Set::extract('/FoundProblem/plan_id',$workIntervals);
        $partialQuantityModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $partials = $partialQuantityModel->find('all', array(
            'fields'=>array('PartialQuantity.*'),
            'conditions'=>array('Plan.id'=>$plansIdsList),
        ));
        foreach($workIntervals as $key => $interval){
            $intervalPartials = array_filter($partials, function($partial)use($interval){
                return $partial['PartialQuantity']['sensor_id'] == $interval['FoundProblem']['sensor_id'] && 
                    strtotime($partial['PartialQuantity']['created']) >= strtotime($interval[0]['start']) && 
                    strtotime($partial['PartialQuantity']['created']) <= strtotime($interval[0]['end']);
            });
            $workIntervals[$key][0]['quantity'] = empty($intervalPartials)?0:array_sum(Set::extract('/PartialQuantity/quantity',$intervalPartials));
        }
    }

    public function Workcenter_BeforeSavePartialQuantity_Hook(&$request, &$item, &$fSensor, &$currentPlan){
         $item['quantity'] = (float) str_replace(',','.',$request->query('declaredQuantity'));
         $item['pieces_quantity'] = (float) str_replace(',','.',$request->query('declaredQuantityPieces'));
    }

    public function Workcenter_changeUpdateStatusVariables_Hook2(&$data){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        if($data['currentPlan']){
//            $piecesQuantity = $partialQuantityModel->find('first', array(
//                'fields'=>array('SUM(PartialQuantity.pieces_quantity) AS pieces_quantity'),
//                'conditions'=>array('PartialQuantity.approved_order_id'=>$data['currentPlan']['ApprovedOrder']['id'])
//            ));
//            $data['orderPartialQuantityPieces'] = (int)$piecesQuantity[0]['pieces_quantity'];
            $partialQuantityModel->bindModel(array('belongsTo'=>array('ApprovedOrder', 'Plan'=>array('foreignKey'=>false,'conditions'=>array('ApprovedOrder.plan_id = Plan.id')))));
            $partialQuantities = $partialQuantityModel->find('first', array(
                'fields'=>array('SUM(PartialQuantity.pieces_quantity) AS pieces_quantity'),
                'conditions'=>array('Plan.mo_number'=>$data['currentPlan']['Plan']['mo_number']),
                'group'=>array('Plan.id')
            ));
            $data['orderPartialQuantityPieces'] = (int)($partialQuantities[0]['pieces_quantity']??0);
        }
    }

    public function WorkCenter_index_Hook(&$data, &$fSensor){
        switch($fSensor['Sensor']['branch_id']){
            case 1://Pjovimas
                $data['structure'] = array(
                    'Plan.serial_number' => __('Užsakymo numeris'),
                    'Plan.client_name' => __('Klientas'),
                    'Plan.kartono_rusos_logo' => __('Kartono rūšis / Logotipas'),
                    'Plan.kartono_gramatura_gsm' => __('Kartono gramatūra, gsm'),
                    'Plan.quantity' => __('Kartono kiekis, kg'),
                    'Plan.formatas_mm' => __('Formatas, mm'),
                    'Plan.tuta' => __('Tūta'),
                    'Plan.rulono_diametras' => __('Rulono diametras'),
                    'Plan.palete' => __('Paletė'),
                    'Plan.pakavimo_tipas' => __('Pakavimo tipas'),
                    'Plan.paletes_aukstis' => __('Paletės aukštis,mm'),
                    'Plan.etikete' => __('Etiketė'),
                    'Plan.pastaba' => __('Pastaba'),
                );
                break;
            case 2: //Kampai
                $data['structure'] = array(
                    'Plan.serial_number' => __('Užsakymo numeris'),
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.tolerancijos4' => __('Tolerancijos'),
                    'Plan.apdail_logo' => __('Apd spalva / Logo'),
                    'Plan.vidines_juostos_plotis_mm' => __('Vid juostos plotis mm'),
                    'Plan.quantity' => __('Užsakymo kiekis, vnt'),
                    'Plan.partial_quantity' => __('Pagamintas kiekis, vnt'),
                    'Plan.palete' => __('Paletė'),
                    'Plan.kiekis_pal' => __('Kiekis pal'),
                    'Plan.kiekis_pake_vnt' => __('Kiekis pake, vnt'),
                    'Plan.ruosinio_ilgis' => __('Ruošinio ilgis'),
                    'Plan.pak_kod_tipas' => __('Pakavimo kodas / tipas'),
                    'Plan.lentu_ilgis_cm' => __('Lentų ilgis, cm'),
                    'Plan.atsparumas_gniuzdymui_n_100mm' => __('Atsparumas gniuždymui'),
                    'Plan.pastaba' => __('Pastaba'),
                    'Plan.sertifikato_zyma' => __('Sertifikato žyma'),
                );
                break;
            case 3: //Tūtos
                $data['structure'] = array(
                    'Plan.serial_number' => __('Užsakymo numeris'),
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.tolerancijos3' => __('Tolerancijos'),
                    'Plan.apdail_logo' => __('Apd spalva / Logo'),
                    'Plan.kompozicija' => __('Kompozicija'),
                    'Plan.quantity' => __('Užsakymo kiekis, vnt'),
                    'Plan.partial_quantity' => __('Pagamintas kiekis, vnt'),
                    'Plan.palete' => __('Paletė'),
                    'Plan.kiekis_pal' => __('Kiekis pal'),
                    'Plan.ruosinio_ilgis' => __('Ruošinio ilgis'),
                    'Plan.deze_kiekis_dezeje_maise' => __('Dėžė/kiekis dėžėje'),
                    'Plan.pak_kod_tipas_palkamp_aukstis' => __('Pakavimo kodas/pal aukštis/kampo aukštis'),
                    'Plan.atsparumas_gniuzdymui_n_100mm' => __('Atsparumas gniuždymui'),
                    'Plan.pastaba' => __('Pastaba'),
                    'Plan.sertifikato_zyma' => __('Sertifikato žyma'),
                );
                break;
            case 4: //Persukimas
                $data['structure'] = array(
                    'Plan.serial_number' => __('Užsakymo numeris'),
                    'Plan.client_name' => __('Klientas'),
                    'Plan.kartono_rusis' => __('Kartono rūšis'),
                    'Plan.kartono_gramatura_gsm' => __('Kartono gramatūra,gsm'),
                    'Plan.quantity' => __('Užsakytas rulonėlių skaičius'),
                    'Plan.formatas_mm' => __('Formatas, mm'),
                    'Plan.metrazas_m2' => __('Metražas, m2'),
                    'Plan.pakavimo_tipas' => __('Pakavimo tipas'),
                    'Plan.etikete' => __('Etiketė'),
                    'Plan.palete' => __('Paletė'),
                    'Plan.kiekis_pal' => __('Kiekis paletėje, vnt'),
                    'Plan.lentu_ilgis_cm' => __('Lentų ilgis, cm'),
                    'Plan.pastaba' => __('Pastaba'),
                );
                if(in_array($fSensor['Sensor']['id'], array(13,14))){
                    $data['structure']['Plan.step'] = str_replace('m/min','kg/h',$data['structure']['Plan.step']);
                }
                break;
        }
    }

    public function changePlanSearchInWorkCenterHook(&$planSearchParams, &$fSensor){
        $planModel = ClassRegistry::init('Plan');
        switch($fSensor['Sensor']['branch_id']) {
            case 1://Pjovimas
                $planModel->virtualFields = array(
                    'kartono_rusos_logo' => 'CONCAT(Plan.kartono_rusis,\' / \', Plan.logotipas_vieta)',
                );
            break;
            case 2://Kampai
                $planModel->virtualFields = array(
                    'tolerancijos4' => 'CONCAT(Plan.sieneles_1_tolerancija,\' x \', Plan.sieneles_2_tolerancija, \' x \', Plan.sieneles_storio_tolerancija, \' x \', Plan.ilgio_tolerancija)',
                    'apdail_logo' => 'CONCAT(Plan.apdailinis,\' / \', Plan.logotipas_vieta)',
                    'pak_kod_tipas' => 'CONCAT(Plan.pakavimo_kodas,\' / \', Plan.pakavimo_tipas)',
                );
            break;
            case 3: //Tūtos
                $planModel->virtualFields = array(
                    'tolerancijos3' => 'CONCAT(Plan.vid_diametro_tolerancija,\' x \', Plan.sieneles_storio_tolerancija, \' x \', Plan.ilgio_tolerancija)',
                    'apdail_logo' => 'CONCAT(Plan.apdailinis,\' / \', Plan.logotipas_vieta)',
                    'deze_kiekis_dezeje_maise' => 'CONCAT(Plan.deze,\' / \', Plan.kiekis_dezeje_maise)',
                    'pak_kod_tipas_palkamp_aukstis' => 'CONCAT(Plan.pakavimo_kodas,\' / \', Plan.paletes_aukstis, \' / \', Plan.kampu_aukstis_m)',
                );
            break;
        }
    }

    public function ApprovedOrdersEditHook(&$controller){
        $controller->PartialQuantity->bindModel(array('belongsTo'=>array('ApprovedOrder', 'Plan'=>array('foreignKey'=>false, 'conditions'=>array('Plan.id = ApprovedOrder.plan_id')))));
        $controller->viewVars['partialQuantities'] = $controller->PartialQuantity->find('all', array(
            'fields'=>array('PartialQuantity.*'),
            'conditions' => array('Plan.mo_number' => $controller->viewVars['item']['Plan']['mo_number']),
            'order'      => array('PartialQuantity.approved_order_id','PartialQuantity.id ASC')
        ));
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/edit');
    }

}
    