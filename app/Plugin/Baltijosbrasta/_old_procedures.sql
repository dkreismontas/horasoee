-- 

DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `ADD_FULL_RECORD`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` DECIMAL(12,4), IN `record_cycle` SMALLINT(6))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `prev_shift_id` INT(11) DEFAULT NULL;
	DECLARE `shift_start` DATETIME DEFAULT NULL;
	DECLARE `active_problem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_end` DATETIME DEFAULT NULL;
	DECLARE `active_fproblem_shift_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_duration` INT(11) DEFAULT NULL;
	DECLARE `product_production_time` INT(11) DEFAULT NULL;
	DECLARE `last_work_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `active_order_id` INT(11) DEFAULT NULL;
	DECLARE `active_plan_id` INT(11) DEFAULT NULL;
	DECLARE `plan_preparation_time` DECIMAL(12,4) DEFAULT NULL;	#plano pasiruosimo laikas
	DECLARE `step` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `box_quantity` INT(11) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
	DECLARE `new_production_start` DATETIME DEFAULT NULL;		#data, nusakanti kada turi prasideti nauja gamyba, irasoma kai nera plano ir pareina reikiamas irasu kiekiais kiekis. 
	DECLARE `setting_prod_starts_on` DECIMAL(12,4) DEFAULT NULL;		#iraso atsiunciamas kiekis, kuris laikomas tinkamu (vyksta gamyba)
	DECLARE `quantity_records_to_start_order` INT(11) DEFAULT NULL;		#Is jutiklio nustatymo papimamas kiekis, nusakantis kiek geru kiekiu irasu turi pareiti, kad prasidetu gamyba.
	DECLARE `quantity_records_count` INT(11) DEFAULT NULL;		#Is eiles einanciu irasiu su gerais kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti. TODO uzbaigiant esama gamyba ji irgi reikia nuresetinti
	DECLARE `setting_short_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi trumpiau nei nurodytas laikas minutėmis. Pvz.: 343_2
	DECLARE `setting_long_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi ilgiau nei nurodytas laikas minutėmis. Pvz.: 343_2. Pakeitimas vykdomas keiciantis pamainai
	DECLARE `exceeded_transition_id` INT(11) DEFAULT NULL;	#Jeigu po užsakymo pasirinkimo yra viršijama derinimo trukmė, kuri nurodoma plano užsakyme, sistema turi sukurti prastovą. Nurodykite šios prastobos ID (jei 0, funkcionalumas neveikia)
	DECLARE `tuta_factor_quantity` DECIMAL(12,4) DEFAULT NULL;	#--Baltijosbrasta
	DECLARE `tuta_factor` DECIMAL(12,4) DEFAULT NULL;			#--Baltijosbrasta
	
    SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	
	SELECT `s`.`branch_id`,`s`.`prod_starts_on`,`s`.`quantity_records_count`,`s`.`quantity_records_to_start_order`,`s`.`new_production_start`,`s`.`short_problem`,`s`.`long_problem`
		INTO `branch_id`,`setting_prod_starts_on`,`quantity_records_count`,`quantity_records_to_start_order`,`new_production_start`,`setting_short_problem`,`setting_long_problem`
		FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id`,`sh`.`start` INTO `shift_id`,`shift_start` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	SELECT `fp`.`id`, TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`), `fp`.`shift_id`,`fp`.`problem_id`, `fp`.`end`
		INTO `active_fproblem_id`,`active_fproblem_duration`,`active_fproblem_shift_id`, `active_problem_id`, `active_fproblem_end`
		FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 LIMIT 1;
	#--Baltijosbrasta pridetas tuta_factor_quantity ir tuta_factor kintamuju reiksmiu perdavimas is plans
	SELECT `ao`.`id`, `ao`.`plan_id`, `plans`.`step`,`plans`.`box_quantity`, `products`.`production_time`,`plans`.`preparation_time`,`plans`.`tuta_factor_quantity`,`plans`.`tuta_factor`
		INTO `active_order_id`,`active_plan_id`,`step`,`box_quantity`, `product_production_time`, `plan_preparation_time`,`tuta_factor_quantity`,`tuta_factor`
		FROM `approved_orders` AS `ao` LEFT JOIN `plans` ON(`ao`.`plan_id` = `plans`.`id`) LEFT JOIN `products` ON(`products`.`id` = `plans`.`product_id`) WHERE `ao`.`sensor_id` = `sensor_id` AND `ao`.`status_id` = 1 LIMIT 1;
	SELECT `s`.`value` INTO `exceeded_transition_id` FROM `settings` AS `s` WHERE `s`.`key` LIKE 'exceeded_transition_id' LIMIT 1;
	
	SET `active_fproblem_duration` = `active_fproblem_duration` + 1;  #+1s reikia, nes problema kuriame su 19s atgal nuo pirmo problemos recordo
	SET `step` = IFNULL(`step`,0); 
	SET `box_quantity` = IFNULL(`box_quantity`,0);
	SET `tuta_factor_quantity` = IFNULL(`tuta_factor_quantity`,0);	#--Baltijosbrasta
	SET `tuta_factor` = IFNULL(`tuta_factor`,0);					#--Baltijosbrasta
	SET `product_production_time` = IFNULL(`product_production_time`,0);
	SET `setting_prod_starts_on` = IF(`setting_prod_starts_on` > 0, `setting_prod_starts_on`, 0.1);
	SET `quantity_records_count` = IF(`quantity` >= `setting_prod_starts_on`, `quantity_records_count` + 1, 0);	#skaiciuojame kiek pagal nauajusius irasus yra is eiles einanciu produkciniu irasu.
	
	IF(`active_fproblem_id` > 0 AND `active_fproblem_shift_id` != `shift_id`) THEN	#KEICIASI PAMAINA: uzdarome buvusios pamainos problema ir atidarome nauja esamai pamainai
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`shift_start`) - 1), '%Y-%m-%d %H:%i:%s');		#sena pamaina turi uzsidaryti 1s anksciau nei prasideda nauja
		UPDATE `found_problems` SET `end` = @subdate, `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;	#uzdarome esama problema, jos pabaiga bus: [new_shift_start - 1s]
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`) VALUES (
			NULL, `shift_start`, `date_time`, `active_problem_id`, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', `active_fproblem_id`);
		SET `active_fproblem_id` = LAST_INSERT_ID();
		IF (`active_order_id` IS NULL) THEN		#Jei keiciasi pamaina ir iki siol dar neparinkta gamyba, tuomet resetiname gamybos pradzia, kad gamyba esamoje pamainoje prasidetu nuo esamoje pamainu atsiustu kiekiu, t.y. jos pradzia butu esamoje pamainoje
			UPDATE `sensors` AS `s` SET `s`.`new_production_start` = '0000-00-00' WHERE `s`.`id`=`sensor_id`;
		END IF;
	END IF;
	
	IF (`quantity` < `setting_prod_starts_on`) THEN		#Jei atsiunciamas probleminis irasas
		IF(`active_problem_id` = -1 OR `active_fproblem_id` IS NULL) THEN	#nauja problema kuriame tik jei iki siol ejo darbas arba nebuvo problemos
			IF(`active_fproblem_id` > 0) THEN	#jei pries tai vyko gamybos problema (darbas), ja reikia uzdaryti
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;	
			END IF;
			SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
			INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`) VALUES (
				NULL, @subdate, `date_time`, '3', '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL);
			SET `active_fproblem_id` = LAST_INSERT_ID();
		ELSEIF (`active_fproblem_id` > 0) THEN	#jei pries tai vyko paprasta problema, ja reikia tiesiog testi
			UPDATE `found_problems` SET `end` = `date_time` WHERE `found_problems`.`id` = `active_fproblem_id`;
		END IF;
		IF(`active_plan_id` > 0 AND `active_problem_id` = 1) THEN
			IF(`exceeded_transition_id` > 0) THEN
				SET `active_fproblem_id` = `CREATE_EXCEEDING_TRANSITION_BY_PLAN`(`sensor_id`, `shift_id`, `active_plan_id`, `plan_preparation_time`, `active_fproblem_id`, `exceeded_transition_id`);
			END IF;
		END IF;
	ELSEIF (`active_fproblem_id` IS NULL AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei siuo metu nera aktyvios problemos ir atsiunciamas geras irasas,kuriame darbo problema
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`) VALUES (
			NULL, @subdate, `date_time`, -1, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL);
	ELSEIF (`active_fproblem_id` > 0 AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei yra problema, bet parejo kiekis
		SET @realProblemDuration = IF(product_production_time > 0, product_production_time*2, 3 * `record_cycle`);	#suskaiciuojame kokio ilgio turi buti problema, kad ja paliktume
		IF(`active_problem_id` = -1) THEN		#jei siuo metu vyksta gamybos problema (problem_id = -1), pratesiame jos laika
			UPDATE `found_problems` SET `end` = `date_time` WHERE `found_problems`.`id` = `active_fproblem_id`;
			SET `active_fproblem_id` = NULL;
		ELSEIF(`active_order_id` > 0 AND `active_problem_id` = 1) THEN		#jei siuo metu yra parinktas uzsakymas ir vyksta derinimas (problem_id = 1):
			IF (`quantity_records_count` >= `quantity_records_to_start_order`) THEN		#derinima uzdaryti tik jei pareina reikiamas geru irasu kiekis
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;		#uzdarome perejima
				UPDATE `records` SET `found_problem_id` = NULL WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on`;	#kol bus pasiektas reikiamas irasu kiekis perejimui uzdaryti, irasai su gerais kiekias eina su problema (nezinoma gal sekantis irasas bus probleminis ir derinimas tesis), todel cia jau galime nuo paskutiniu geru irasu nuimti problemas
				SET `active_fproblem_id` = NULL;
			END IF;
		ELSEIF (`active_fproblem_duration` < @realProblemDuration AND `active_problem_id` >= 0) THEN		#jei esamos problemos trukme mazesne nei maziausias galimas problemos laikas, problema saliname 
			DELETE FROM `found_problems`  WHERE `found_problems`.`id` = `active_fproblem_id`;
			UPDATE `records` SET `records`.`found_problem_id` = NULL WHERE `records`.`found_problem_id` = `active_fproblem_id`;		#nuresetiname recordamas uzdetas problemas
			SELECT MAX(`fp`.`id`) INTO `last_work_fproblem_id` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = -1 AND `fp`.`shift_id` = `shift_id` LIMIT 1;	
			IF (`last_work_fproblem_id` > 0) THEN		#kai tikra problema yra per trumpa, jinai salinama. Tada reikia vel aktyvuoti paskutine darbo problema jei ji egzistuoja
				UPDATE `found_problems` SET `state` = 1, `end` = `date_time` WHERE `found_problems`.`id` = `last_work_fproblem_id`;
			END IF;
			SET `active_fproblem_id` = NULL;
		ELSE
			SET @short_problem_id = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',1));
			SET @short_problem_duration = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',-1));
			IF(@short_problem_id > 0 AND @short_problem_duration > 0 AND @short_problem_duration*60 >= `active_fproblem_duration` AND `active_problem_id` = 3) THEN	#Jei yra jutiklio nustatymas trumpas sustojimas ir problema uzsidaroma su mazesniu laiku nei nustatyme, nepazymeta prastova automatiskai virsta i problema pagal nustatyma
				UPDATE `found_problems` SET `state` = 2, `problem_id` = @short_problem_id WHERE `found_problems`.`id` = `active_fproblem_id`;
			ELSE
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;
			END IF;
			SET `active_fproblem_id` = NULL;
			IF(`active_order_id` > 0) THEN		#Jei patenkama i sia vieta, vadinasi buvo parsiustas kiekis, tikra problema uzsidare, vyksta gmayba, todel kuriame problemini irasa, kuris reiskia vykstanti darba (problem_id = -1)
				SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
				INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`) VALUES (
					NULL, @subdate, `date_time`, '-1', '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL);
			END IF;
		END IF;
	END IF;
	
	#veiksmai, kai keiciasi pamaina (nuo pamainos pradzios nepraejo ilgesnis laikas kaip iraso ciklas)
	IF(UNIX_TIMESTAMP(`date_time`) - UNIX_TIMESTAMP(`shift_start`) <= `record_cycle`) THEN 
		SELECT `shifts`.`id` INTO `prev_shift_id` FROM `shifts` WHERE `shifts`.`start` < `shift_start`  AND `shifts`.`branch_id` = `branch_id` ORDER BY `shifts`.`id` DESC LIMIT 1;
		#Jei jutiklis turi nustatyma keiciantis pamainai pakeisti ilgas nepazymetas prastovas i nurodytas nustatyme, tai darome cia
		SET @long_problem_id = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',1));
		SET @long_problem_duration = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',-1));
		IF(@long_problem_id > 0 AND @long_problem_duration > 0 AND `prev_shift_id` > 0) THEN
			UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = @long_problem_id WHERE TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`) >= @long_problem_duration*60 AND `fp`.`shift_id` = `prev_shift_id` AND `fp`.`problem_id` = 3 AND `fp`.`sensor_id` = `sensor_id`;
		END IF;
	END IF;
	
	#skaiciuojame is eiles einanciu geru irasu kiekius bei nustatome kada prasideda nauja gamyba
	UPDATE `sensors` AS `s` SET `s`.`quantity_records_count` = `quantity_records_count` WHERE `s`.`id`=`sensor_id`; 		
	IF(`quantity_records_count` >= `quantity_records_to_start_order` AND `active_order_id` IS NULL AND `new_production_start` <= `shift_start`) THEN
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle` * `quantity_records_count`)), '%Y-%m-%d %H:%i:%s');	#esamas momentas yra kai pasiektas reikiamas irasu kiekius gamybai pradeti. Nuo dabarties atsokame atgal per irasu kiekiu, tada atejo pirmas geras irasas
		UPDATE `sensors` AS `s` SET `s`.`new_production_start` = @subdate WHERE `s`.`id`=`sensor_id`;
	END IF;
	
	#jei vykdoma gamyba sumuojame kiekius
	IF(`active_order_id` > 0) THEN
		UPDATE `approved_orders` AS `ao` SET `ao`.`end` = `date_time`, `ao`.`quantity` = `ao`.`quantity` + (`quantity`*`box_quantity`),	`ao`.`box_quantity` = `ao`.`quantity` / `box_quantity` WHERE `ao`.`id` = `active_order_id`;
	END IF;
	
	#--BaltijosbrastaPakeistas quantity ir unit_quantity iterpimas per tuta_quantity
	#iterpiame nauja recorda
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`) VALUES (`date_time`, `quantity`*`tuta_factor_quantity`, `quantity`*`box_quantity`*`tuta_factor`, `sensor_id`, `active_plan_id`, `shift_id`, `active_order_id`, `active_fproblem_id`);
END$$

DELIMITER ;
