<?php
$this->Html->css('/baltijosbrasta/css/sticker.css?v=2', array('inline'=>false));
?>
<div style="width: 100%;">
    <table cellspacing="0" cellpadding="0">
        <tr>
            <td>
                <p>MATMENYS / DIMENSIONS / РАЗМЕРЫ</p>
            </td>
        </tr>
        <tr>
            <td>
                <h2>
                    <?php
                    $stringArray = array_filter(array(
                        $order['Plan']['sieneles_plotis_1'],
                        $order['Plan']['sieneles_plotis_2'],
                        $order['Plan']['tutos_vidinis_diametras_mm'],
                        $order['Plan']['sieneles_storis'],
                        $order['Plan']['ilgis_mm'],
                    ));
                    echo implode('x',$stringArray);
                    ?>
                </h2>
            </td>
        </tr>
        <tr>
            <td>
                <p>SPALVA/LOGO / COLOUR/LOGO / ЦВЕТ/ЛОГО</p>
            </td>
        </tr>
        <tr>
            <td>
                <p style="font-size: 16px;"><strong><?php echo $order['Plan']['logotipas_vieta']; ?></strong></p>
            </td>
        </tr>
        <tr>
            <td>
                <p>KIEKIS PAKE / PIECES PER PACKET / КОЛИЧЕСТВО В ПАЧКЕ</p>
            </td>
        </tr>
        <tr>
            <td>
                <h2><?php
                    $stringArray = array_filter(array(
                        $order['Plan']['kiekis_pake_vnt'],
                        $order['Plan']['kiekis_dezeje_maise'],
                    ));
                    echo implode(', ',$stringArray);
                    ?></h2>
            </td>
        </tr>
        <tr>
            <td>
                <p>KIEKIS / QUANTITY / КОЛИЧЕСТВО</p>
            </td>
        </tr>
        <tr>
            <td>
                <h1 id="quantityPal" data-original_quantity="<?php echo $order['Plan']['kiekis_pal']; ?>"><?php echo $order['Plan']['kiekis_pal']; ?></h1>
            </td>
        </tr>
        <tr>
            <td>
                <p>UŽSAKYMO  NUMERIS / CUSTOMER NUMBER / НОМЕР ЗАКАЗА</p>
            </td>
        </tr>
        <tr>
            <td>
                <h1><?php echo $order['Plan']['serial_number']; ?></h1>
            </td>
        </tr>
        <tr>
            <td>
                <p>PAGAMINIMO DATA / DATE OF PRODUCTION / ДАТА</p>
            </td>
        </tr>
        <tr>
            <td>
                <h2><?php echo date('Y-m-d'); ?></h2>
            </td>
        </tr>
        <tr>
            <td>
                <p>OPERATORIUS / OPERATOR / ОПЕРАТОР</p>
            </td>
        </tr>
        <tr>
            <td>
                <h2><?php echo substr($user->first_name,0,3).' '.substr($user->last_name,0,3); ?></h2>
            </td>
        </tr>
        <tr>
            <td>
                <p>KLIENTO PREKĖS KODAS / CUSTOMER PRODUCT CODE / КОД ПРОДУКТА КЛИЕНТА</p>
            </td>
        </tr>
        <tr>
            <td>
                <h3><?php echo $order['Plan']['kliento_prekes_kodas']; ?></h3>
            </td>
        </tr>
        <tr>
            <td>
                <p>KLIENTO UŽSAKYMO NUMERIS / CUSTOMER ORDER CODE / КОД ЗАКАЗА КЛИЕНТА</p>
            </td>
        </tr>
        <tr>
            <td>
                <h3><?php echo $order['Plan']['kliento_uzsakymo_nr']; ?></h3>
            </td>
        </tr>
        <tr>
            <td>
                <?php echo $this->Html->image('/baltijosbrasta/files/sticker_logo.jpg', array('width'=>90,'style'=>'float: left;')); ?>
                <h2 style="position: relative; top: 20px; display: inline;"><?php echo $order['Plan']['se']; ?></h2>
                <?php
                $barcode = $this->Barcode->generateSimpleCode($order['Plan']['serial_number'].'|'.current(explode('_', $order['Plan']['production_code'])), 70, 50, 0, 'horizontal', 'code128');
                ob_start();
                imagejpeg($barcode, null, 100);
                $rawImageBytes = ob_get_clean();
                ?>
                <img style="float: right;" src="data:image/jpeg;base64,<?php echo base64_encode( $rawImageBytes ) ?>" />
            </td>
        </tr>

    </table>
</div>
<div style="display: none">
    <div id="popupContainer">
        <div id="confirmMainForm">
            <div class="text-center">
                <p>Ar paletėje yra standartinis gaminių kiekis? </p>
                <br />
                <button class="btn btn-primary" id="confirmYesButton">Taip</button>
                <button class="btn btn-primary" id="confirmNoButton">Ne</button>
            </div>
        </div>
        <div id="confirmNo" style="display: none;">
            <div class="text-center">
                <p>Įveskite gaminių kiekį paletėje</p>
                <br />
                <input type="number" min="0" class="form-control" id="manualQuantity" />
                <button class="btn btn-primary" id="confirmBackButton">Atgal</button>
                <button class="btn btn-primary" id="confirmManualButton">Patvirtinti</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        popupWindow('#popupContainer', 'auto', '500','auto', "<?php echo __('Gaminių kiekio užklausa'); ?> ", "<?php echo __('Uždaryti langą') ?>");
        $('body').on('click', '#confirmNoButton',function(){
            $('#confirmMainForm').hide();
            $('#confirmNo').show();
        });
        $('body').on('click', '#confirmBackButton',function(){
            $('#confirmMainForm').show();
            $('#confirmNo').hide();
        });
        $('body').on('click', '#confirmYesButton',function(){
            $('#quantityPal').html($('#quantityPal').data('original_quantity'));
            window.print();
        });
        $('body').on('click', '#confirmManualButton',function(){
            if(parseInt($('#manualQuantity').val())<=0){ alert('Įveskite skaičių didesnį už 0'); return false; }
            $('#quantityPal').html($('#manualQuantity').val());
            window.print();
        });

    });
</script>