<?php
$current_page = trim(urldecode($_GET['current_page']),'/');
if(!isset($current_page)) die();
if(false){ ?><script type="text/javascript"><?php }
if(preg_match('/.*dashboards\/inner.*/i',trim($current_page,'/'))){ ?>
    jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#brastaProductionReportForm" aria-controls="brastaProductionReportForm" role="tab" data-toggle="tab"><?php echo __('Gamybos ataskaita'); ?></a></li>');
    let brastaProductionReportForm = jQuery('#four').clone();
    brastaProductionReportForm.find('form').attr('action', '/baltijosbrasta/reports/generate_production_report');
    brastaProductionReportForm.find('.view_angle').remove();
    jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane" id="brastaProductionReportForm">'+brastaProductionReportForm.html()+'</div>');
    jQuery('#brastaProductionReportForm select').parent().parent().replaceWith('<?php echo str_replace("\n",'',$this->Form->input('branches', array(
        'label' => false,
        'options' => $branches,
        'multiple' => true,
        'data-placeholder'=>__('Pasirinkite padalinį'),
        'class' => 'form-control chosen-select', 'before'=>'<label class="col-md-12" style="padding:0">'.__('Padaliniai').'</label><div class="col-md-12" style="padding:0">','after'=>'</div>','div'=>'col-md-6',
    ))); ?>');
<?php } ?>