<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>

<?php
if (!isset($boldLabels)) $boldLabels = true;
if (!function_exists('echoLabel')) {
    function echoLabel($label, $boldLabels = false) {
        if ($boldLabels) {
            echo '<strong>'.$label.'</strong>';
        } else {
            echo $label;
        }
    }
}
?>
    <table class="table table-bordered table-condensed">
        <tbody>
        <tr><td><?php echo __('Padalinys'); ?>: <?php echo Branch::buildName($item); ?></td></tr>
        <tr><td><?php echo __('Darbo centras'); ?>: <?php echo Sensor::buildName($item, false); ?></td></tr>
        <tr>
            <td>
                <?php echoLabel(__('Planas'), $boldLabels); ?>:<br />
                <?php echoLabel(__('Gaminys'), $boldLabels); ?>: <?php echo Plan::buildName($item); ?><br />
                <?php echoLabel(__('Gaminio kodas'), $boldLabels); ?>: <?php echo $item[Plan::NAME]['production_code']; ?><br />
                <?php echoLabel(__('MO Numeris'), $boldLabels); ?>: <?php echo $item[Plan::NAME]['mo_number']; ?><br />
            </td>
        </tr>
        <tr>
            <td>
                <?php echoLabel(__('Faktinė gamyba'), $boldLabels); ?>:<br />
                <?php echoLabel(__('Data ir laikas'), $boldLabels); ?>: <?php
                $ds = new DateTime($item[ApprovedOrder::NAME]['created']);
                $de = new DateTime($item[ApprovedOrder::NAME]['end']);
                echo $ds->format('Y-m-d H:i').' &mdash; '.$de->format('Y-m-d H:i');
                ?><br />
                <?php echoLabel(__('Trukmė'), $boldLabels); ?>: <?php echo number_format(($de->getTimestamp() - $ds->getTimestamp()) / 3600, 1, ',', ''); ?> <?php echo __('val.'); ?><br />
                <?php if (isset($lossItems) && !empty($lossItems)): ?>
                    <?php echoLabel(__('Nuostoliai'), $boldLabels); ?>:<br />
                    <?php foreach ($lossItems as $li): ?>
                        <b><?php echo $li['Loss']['created_at']; ?></b> | <?php echo $li[LossType::NAME]['name']; ?>: <?php echo $li[Loss::NAME]['value'].' '.$li[LossType::NAME]['unit']; ?><br />
                    <?php endforeach; ?>
                <?php endif; ?>
                <?php if (isset($partialQuantities) && !empty($partialQuantities)): ?>
                    <?php echoLabel(__('Deklaruoti kiekiai'), $boldLabels); ?>:<br />
                    <div class="row-fluid"><div class="col-sm-4">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th class="text-center"><?php echo __('Užsakymo ID'); ?></th>
                            <th class="text-center"><?php echo __('Deklaravimo laikas'); ?></th>
                            <th class="text-center"><?php echo __('Palečių kiekis'); ?></th>
                            <th class="text-center"><?php echo __('Vienetų kiekis'); ?></th>
                        </tr>
                        </thead><tbody>
                    <?php foreach ($partialQuantities as $li):
                    if(!$li['PartialQuantity']['quantity'] && !$li['PartialQuantity']['quantity']){continue;}
                    ?>
                        <tr>
                            <td class="text-center"><?php echo $li['PartialQuantity']['approved_order_id']; ?></td>
                            <td class="text-center"><?php echo $li['PartialQuantity']['created']; ?></td>
                            <td class="text-center"><?php echo $li['PartialQuantity']['quantity']; ?></td>
                            <td class="text-center"><?php echo $li['PartialQuantity']['pieces_quantity']; ?></td>
                        </tr>
                    <?php endforeach; ?>
                    </tbody></table></div></div>
                <?php endif; ?>
            </td>
        </tr>
        <?php if(isset($relatedPostponedOrders) && !empty($relatedPostponedOrders)) { ?>
            <tr>
                <td>
                    <?php echoLabel(__('Atidėtas kiekis'), $boldLabels); ?>:<br />
                    <?php foreach($relatedPostponedOrders as $ao):?>
                        <strong><?php echo "&nbsp;".$ao['ApprovedOrder']['end']; ?></strong><br>
                        <div style="margin-left:10px;font-size:12px;margin-bottom:10px;">
                            <?php echoLabel(__('Kiekis').' (vnt)', $boldLabels); ?>: <?php echo $ao[ApprovedOrder::NAME]['quantity']; ?><br />
                            <?php echoLabel(__('Dėžių kiekis').' (vnt)', $boldLabels); ?>: <?php
                            $pqt = ($ao[ApprovedOrder::NAME]['quantity'] - ($ao[ApprovedOrder::NAME]['box_quantity'] * $ao[Plan::NAME]['box_quantity']));
                            echo $ao[ApprovedOrder::NAME]['box_quantity'].(($pqt > 0) ? (' '.__('ir gaminių').' '.$pqt.' vnt') : '');
                            ?>
                        </div>
                    <?php endforeach; ?>
                </td>
            </tr>
        <?php } ?>
        <?php if($item[ApprovedOrder::NAME]['exported_quantity'] != 0) { ?>
            <tr>
                <td>
                    <?php echoLabel(__('Eksportuotas kiekis'), $boldLabels); ?>:<br />
                    <div style="margin-left:10px;font-size:12px;margin-bottom:10px;">
                        <?php echoLabel(__('Kiekis').' (vnt)', $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['exported_quantity']; ?><br />
                        <?php echoLabel(__('Dėžių kiekis').' (vnt)', $boldLabels); ?>:
                        <?php
                        $exported_boxes = floor($item[ApprovedOrder::NAME]['exported_quantity']/$item[Plan::NAME]['box_quantity']);
                        $pqt = ($item[ApprovedOrder::NAME]['exported_quantity'] - ($exported_boxes * $item[Plan::NAME]['box_quantity']));
                        echo $exported_boxes.(($pqt > 0) ? (' '.__('ir gaminių').' '.$pqt.' vnt') : '');
                        ?>
                    </div>
                </td>
            </tr>
        <?php } ?>
        <tr>
            <td>
                <?php echoLabel(__('Kiekis').' (vnt)', $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity']; ?><br />
                <!--                --><?php //echoLabel(__('Dėžių kiekis').' (vnt)', $boldLabels); ?><!--:-->
                <!--                --><?php
                //                    $finished_boxes = "Inf";
                //                    if($item[Plan::NAME]['box_quantity'] != 0) {
                //                        $finished_boxes = floor(($item[ApprovedOrder::NAME]['quantity'] - $item[ApprovedOrder::NAME]['exported_quantity']) / $item[Plan::NAME]['box_quantity']);
                //                    }
                //                    $pqt = (($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity']) - ($finished_boxes * $item[Plan::NAME]['box_quantity']));
                //                    echo $finished_boxes.(($pqt > 0) ? (' '.__('ir gaminių').' '.$pqt.' vnt') : '');
                //                ?>

            </td>
        </tr>
        <?php if (isset($url) && $url): ?>
            <tr><td>&nbsp;</td></tr>
            <tr><td><a href="<?php echo htmlspecialchars($url); ?>"><?php echo $url; ?></a></td></tr>
        <?php endif; ?>
        </tbody>
    </table>


<?php if ($item[ApprovedOrder::NAME]['confirmed'] || ($item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_COMPLETE && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_POSTPHONED && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_PART_COMPLETE)): ?>
<?php if(isset($item[ApprovedOrder::NAME]['modified']) && $item[ApprovedOrder::NAME]['modified'] != $item[ApprovedOrder::NAME]['created'] && (int)$item[ApprovedOrder::NAME]['modified'] != 0){
	echo __('Patvirtinimas atliktas').':'.$item[ApprovedOrder::NAME]['modified'];
} ?>
<div class="form-group">
        <?php
        if($item[ApprovedOrder::NAME]['confirmed']) {
            echo "<b>Kiekis (patvirtintas):</b>" . $item[ApprovedOrder::NAME]['confirmed_quantity'];
            echo "<br><br><b>Nuostoliai:</b>";
            foreach($lossItemsConfirmed as $li) {?>
                <br><b><?php echo $li[LossType::NAME]['name']; ?></b>: <?php echo $li[Loss::NAME]['value'].' '.$li[LossType::NAME]['unit']; ?>
            <?php }
        }?>
</div>
<?php else: ?>

<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
<!--	<div class="col-xs-5">-->
<!--		--><?php
//        $pqt = ($item[ApprovedOrder::NAME]['quantity'] - ($item[ApprovedOrder::NAME]['box_quantity'] * $item[Plan::NAME]['box_quantity']));
//        $finished_boxes = floor(($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity'])/$item[Plan::NAME]['box_quantity']);
//        $box_quantity_label = __('Dėžių kiekis');
//        echo $this->Form->input('confirmed_box_quantity', array('value'=>$finished_boxes,'label' => $box_quantity_label, 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<!--	</div>-->
<!--	<div class="col-xs-1">-->
<!--		<div class="form-group"><label>--><?php //echo __('ir'); ?><!--</label></div>-->
<!--	</div>-->
	<div class="col-xs-12">
		<?php 
		if($editApprovedOrderQuantityPerm){
		  echo $this->Form->input('confirmed_item_quantity', array('label' => __('Gaminių kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'value'=>($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity']))); 
		} ?>
	</div>
</div>
<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
    <div class="col-md-4 col-xs-12">
        <?php foreach($lossTypes as $lt) {
            ?>
            <div class="row">
            <label class="control-label col-xs-6"><?php echo $lt['LossType']['name'] ;?> (<?php echo $lt['LossType']['code'] ;?>)</label>
            <div class="col-xs-6">
                <div class="input-group">
                    <input class="form-control input-default" type="text" name="lt_<?php echo $lt['LossType']['id'] ;?>" value=""/>
                    <span class="input-group-addon"><?php echo $lt['LossType']['unit'] ;?></span>
                </div>
            </div>
            </div>
        <?php } ?>
    </div>
</div>
<?php endif; ?>
<div style="clear:both;"></div>
<div>
	<br />
	<?php if ($canSave && !$item[ApprovedOrder::NAME]['confirmed'] && ($item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_COMPLETE || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_PART_COMPLETE)): ?>
	<input type="submit" class="btn btn-primary" value="<?php echo __('Išsaugoti broką ir tvirtinti užsakymą'); ?>" name="confirm_order" />
	<input type="submit" class="btn btn-primary" value="<?php echo __('Išsaugoti tik broką'); ?>" name="save_loss_only" />
	<?php endif; ?>
	<!--<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a> -->
</div>
<?php echo $this->Form->end();?>