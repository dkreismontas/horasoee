<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>

<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('production_time', array('label' => __('Gamybos trukmė (s)'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('production_code', array('label' => __('Produkto kodas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php //echo $this->Form->input('produkto_svoris', array('label' => __('Produkto svoris'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php //echo $this->Form->input('mase_formoje', array('label' => __('Masė formoje'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('kiekis_formoje', array('label' => __('Kiekis formoje'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('formu_greitis', array('label' => __('Formų greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('work_center', array('label' => __('Darbo centrams'), 'class' => 'form-control', 'div' => array('class' => 'form-group'),'type'=>'select', 'multiple'=>'multiple', 'options'=>$sensorsList)); ?>
<?php echo $this->Form->input('tutos_vidinis_diametras', array('label' => __('Tūtos vidinis diametras'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('vidines_juostos_plotis', array('label' => __('Vidinės juostos plotis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
<?php echo $this->Form->end(); ?>
