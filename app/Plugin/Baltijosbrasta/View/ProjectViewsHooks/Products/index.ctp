<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas planas'); ?></button>
<br /><br /> */ ?>
<div class="clearfix pull-right">
    <form action="/products/import" method="POST" enctype='multipart/form-data'>
        <input style="float:left; margin-top:4px;" type="file" name="doc"/>
        <button class="btn btn-primary"><?php echo __('Importuoti'); ?></button>
        <a class="btn btn-danger" href="<?php echo $this->Html->url('/',true).'files/import_products_example.xml' ?>" class="btn btn-primary"><?php echo __('XML failo pavyzdys'); ?></a>
        <a class="btn btn-warning" href="<?php echo $this->Html->url('/',true).'products/edit' ?>"><?php echo __('Sukurti naują'); ?></a>
    </form>
</div>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Produktas'); ?></th>
        <th><?php echo __('Kodas'); ?></th>
        <th><?php echo __('Kiekis formoje'); ?></th>
        <th><?php echo __('Formų greitis (formos/h)'); ?></th>
        <th><?php echo __('Gamybos ciklas (s)'); ?></th>
        <th><?php echo __('Darbo centrai'); ?></th>
        <th><?php echo __('Tūtos vidinis diametras'); ?></th>
        <th><?php echo __('Vidinės juostos plotis'); ?></th>
        <!--th><?php echo __('Galimi darbo centrai (jei nenurodyta galimi visi)'); ?></th-->
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($list)): ?>
        <tr>
            <td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($list as $li_):
        $li = (object) $li_["Product"]; ?>
        <tr>
            <td><?php echo $li->id; ?></td>
            <td><?php echo $li->name; ?></td>
            <td><?php echo $li->production_code; ?></td>
            <td><?php echo $li->kiekis_formoje; ?></td>
            <td><?php echo $li->formu_greitis; ?></td>
            <td><?php echo $li->production_time; ?></td>
            <td><?php 
                $idsList = explode(',', $li->work_center);
                if(empty($li->work_center)){ echo __('Visi darbo centrai'); }
                else echo implode('<br />', array_filter($sensorsList, function($sensorId)use($idsList){return in_array($sensorId, $idsList); }, ARRAY_FILTER_USE_KEY));
            ?></td>
            <td><?php echo $li->tutos_vidinis_diametras; ?></td>
            <td><?php echo $li->vidines_juostos_plotis; ?></td>
            <!--td><?php echo $li->work_center; ?></td-->
            <td>
                <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span
                        class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a') ?></a><br/>
                <a href="<?php printf($removeUrl, $li->id); ?>"
                   onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');"
                   title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>