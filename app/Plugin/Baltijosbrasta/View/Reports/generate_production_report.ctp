<?php
ob_end_clean();
error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 11,
        'name' => 'Times New Roman',
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);
$sheetNr = 0;
//Pagaminimo registras ------------------------------------
$activeSheet = $objPHPExcel->createSheet($sheetNr++);
$activeSheet->setTitle(__('Pagaminimo registras'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Linija'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Operatorius'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Data'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamaina'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Suplanuotas laikas, min'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Faktinis laikas, min'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pradžia'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pabaiga'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminio pavadinimas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagaminta, m.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletės, vnt.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Brokas, m.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Greitis, m/val'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamyba, min.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Perėjimai, min.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Prastovos, min.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Prastovos'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->getRowDimension(1)->setRowHeight(30);
$rowNr = 2;
$shiftType = array(1=>__('rytas'),2=>__('pietūs'),3=>__('naktis'));
$shiftDurationsPrinted = $problemsMap = array();
foreach($orders as $order){
    $shiftId = $order['Shift']['id'];
    $userId = $order['User']['id'];
    $userName = $order['User']['first_name'].' '.$order['User']['last_name'];
    if(!isset($shiftDurationsPrinted[$shiftId][$userId])){
        $shiftDurationsPrinted[$shiftId][$userId] = 1;
        switch($order['Branch']['id']){
            case 2: $shiftDuration = 690; break;
            default: $shiftDuration = 420;
        }
    }else{ $shiftDuration = ''; }
    $colNr = 0;
    $madeMetersCount = round(bcmul($order['partialPiecesSum'], $order['Plan']['product_length'],3),2);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $order['Sensor']['ps_id'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $userName)->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $order['Shift']['start'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $shiftType[$order['Shift']['ps_id']]??' --- ')->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $shiftDuration)->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, bcdiv((strtotime($order['OrderCalculation']['order_end']) - strtotime($order['OrderCalculation']['order_start'])), 60, 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $order['OrderCalculation']['order_start'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $order['OrderCalculation']['order_end'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $order['Plan']['production_name'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $madeMetersCount)->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($order['partialQuantitySum'],2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcsub($madeMetersCount, $order['OrderCalculation']['quantity'], 3),2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($madeMetersCount, bcdiv($order['OrderCalculation']['time_green'],3600,6), 3),2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($order['OrderCalculation']['time_green'],60,3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($order['OrderCalculation']['time_yellow'],60,3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($order['OrderCalculation']['time_red'],60,3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    foreach($order['OrderCalculation']['problems'] as $problemId => $problem){
        if($problemId <= Problem::ID_NO_WORK){ continue; }
        if(!isset($problemsIdToName[$problemId])){ continue; }
        if(!isset($problemsMap[$problemId])){
            if(empty($problemsMap)){
                $problemsMap[$problemId] = $colNr;
            }else{
                $problemsMap[$problemId] = max($problemsMap);
                $problemsMap[$problemId]++;
            }
            $activeSheet->setCellValueByColumnAndRow($problemsMap[$problemId], 1, Settings::translate($problemsIdToName[$problemId]))->getStyle(cts($problemsMap[$problemId], 1))->applyFromArray($headerStyleArray2);
        }
        $activeSheet->setCellValueByColumnAndRow($problemsMap[$problemId], $rowNr, bcdiv($problem->length,60,2))->getStyle(cts($problemsMap[$problemId], $rowNr))->applyFromArray($borderStyleArray);
    }
    $rowNr++;
}

$colNr = !empty($problemsMap)?max($problemsMap):$colNr;
for ($col = 0; $col != $colNr+1; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}
//Nasumas pagal operatorius, irenginius ir gaminius------------------------------------
foreach($constructData as $combineByName => $operatorsData) {
    $activeSheet = $objPHPExcel->createSheet($sheetNr++);
    $activeSheet->setTitle($combineByName, false);
    $colNr = 0;
    $rowNr = 1;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $combineByName)->getStyle(cts($colNr, $rowNr++))->applyFromArray(Hash::merge($headerStyleArray2, $borderStyleArray));
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Periodas'))->getStyle(cts($colNr, $rowNr))->applyFromArray(Hash::merge($headerStyleArray2, $borderStyleArray));
    $usedMonths = array_unique(Hash::extract($operatorsData, '{n}.{n}.month'));
    sort($usedMonths);
    $startInMonth = $endInMonth = array();
    $totalsByOperator = array();
    $totalsByMonth = array();
    $rowsMap = array();
    foreach ($operatorsData as $monthsData) {
        foreach($monthsData as $operatorData){
    //        if (!trim($operatorData['user'])) {
    //            continue;
    //        }
            $combineBy = $operatorData['combiner'];
            $monthNr = $operatorData['month'];
            if (!isset($rowsMap[$combineBy])) {
                $rowsMap[$combineBy] = empty($rowsMap) ? 4 : max($rowsMap) + 1;
            }
            $rowNr = $rowsMap[$combineBy];
            $headersRowNr = 1;
            $refreshDateInterval = false;
            $colNr = array_search($monthNr, $usedMonths) * 4 + 1;
            $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, $this->App->datePickerConfig()['monthNames'][$monthNr - 1]);
            $activeSheet->mergeCells(nts($colNr) . $headersRowNr . ':' . nts($colNr + 3) . '1')->getStyle(nts($colNr) . $headersRowNr . ':' . nts($colNr + 3) . '1')->applyFromArray(Hash::merge($headerStyleArray2, $borderStyleArray, $centerAlignStyleArray));
            $headersRowNr++;
            //$activeSheet->setCellValueByColumnAndRow($colNr, 3, )->getStyle(cts($colNr, $rowNr))->applyFromArray($borderStyleArray);
            if (!isset($startInMonth[$monthNr]) || $startInMonth[$monthNr] > $operatorData['start']) {
                $startInMonth[$monthNr] = $operatorData['start'];
                $refreshDateInterval = true;
            }
            if (!isset($endInMonth[$monthNr]) || $endInMonth[$monthNr] < $operatorData['end']) {
                $endInMonth[$monthNr] = $operatorData['end'];
                $refreshDateInterval = true;
            }
            if ($refreshDateInterval) {
                $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, date('Y-m-d', $startInMonth[$monthNr]) . ' - ' . date('Y-m-d', $endInMonth[$monthNr]));
                $activeSheet->mergeCells(nts($colNr) . $headersRowNr . ':' . nts($colNr + 3) . $headersRowNr)->getStyle(nts($colNr) . $headersRowNr . ':' . nts($colNr + 3) . $headersRowNr)->applyFromArray(Hash::merge($centerAlignStyleArray, $borderStyleArray));
                $headersRowNr++;
                $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('Metrai'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
                $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('Val.'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
                $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('m/val.'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
                $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('Linijos'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
            }
            $colNr = array_search($monthNr, $usedMonths) * 4 + 1;
            $activeSheet->setCellValueByColumnAndRow(0, $rowNr, $combineBy)->getStyle(cts(0, $rowNr))->applyFromArray($borderStyleArray);
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($operatorData['meters'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($operatorData['hours'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($operatorData['meters'], $operatorData['hours'], 3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, implode(',', $operatorData['sensors']))->getStyle(cts($colNr++, $rowNr))->applyFromArray(Hash::merge($borderStyleArray, $centerAlignStyleArray));
            $rowNr++;
            if (!isset($totalsByOperator[$combineBy])) {
                $totalsByOperator[$combineBy] = array('meters' => 0, 'hours' => 0);
            }
            $totalsByOperator[$combineBy]['meters'] = bcadd($totalsByOperator[$combineBy]['meters'], round($operatorData['meters'], 2), 4);
            $totalsByOperator[$combineBy]['hours'] = bcadd($totalsByOperator[$combineBy]['hours'], round($operatorData['hours'], 2), 4);
            if (!isset($totalsByMonth[$monthNr])) {
                $totalsByMonth[$monthNr] = array('meters' => 0, 'hours' => 0);
            }
            $totalsByMonth[$monthNr]['meters'] = bcadd($totalsByMonth[$monthNr]['meters'], round($operatorData['meters'], 2), 4);
            $totalsByMonth[$monthNr]['hours'] = bcadd($totalsByMonth[$monthNr]['hours'], round($operatorData['hours'], 2), 4);
        }
    }
    $totalColumnsStartPos = sizeof($usedMonths) * 4 + 1;
    $headersRowNr = 1;
    $activeSheet->setCellValueByColumnAndRow($totalColumnsStartPos, $headersRowNr, __('Bendrai'));
    $mergeCellsCoord = nts($totalColumnsStartPos) . $headersRowNr . ':' . nts($totalColumnsStartPos + 2) . ($headersRowNr + 1);
    $activeSheet->mergeCells($mergeCellsCoord)->getStyle($mergeCellsCoord)->applyFromArray(Hash::merge($centerAlignStyleArray, $borderStyleArray, $headerStyleArray2));
    $headersRowNr += 2;
    $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('Metrai'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('Val.'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $headersRowNr, __('m/val.'))->getStyle(cts($colNr++, $headersRowNr))->applyFromArray($borderStyleArray);
    $totalOfTotals = array();
    foreach ($totalsByOperator as $combineBy => $totalByOperator) {
        $colNr = $totalColumnsStartPos;
        $rowNr = $rowsMap[$combineBy];
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalByOperator['meters'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalByOperator['hours'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($totalByOperator['meters'], $totalByOperator['hours'], 3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
        if (!isset($totalOfTotals)) {
            $totalOfTotals = array('meters' => 0, 'hours' => 0);
        }
        $totalOfTotals['meters'] = bcadd($totalOfTotals['meters'], round($totalByOperator['meters'], 2), 4);
        $totalOfTotals['hours'] = bcadd($totalOfTotals['hours'], round($totalByOperator['hours'], 2), 4);
    }
    $activeSheet->setCellValueByColumnAndRow(0, max($rowsMap) + 1, __('Bendrai'))->getStyle(cts(0, max($rowsMap) + 1))->applyFromArray(Hash::merge($borderStyleArray, $headerStyleArray2));
    foreach ($totalsByMonth as $monthNr => $totalByMonth) {
        $colNr = array_search($monthNr, $usedMonths) * 4 + 1;
        $rowNr = max($rowsMap) + 1;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalByMonth['meters'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalByMonth['hours'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($totalByMonth['meters'], $totalByMonth['hours'], 3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    }
    $colNr = $totalColumnsStartPos;
    $rowNr = max($rowsMap) + 1;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalOfTotals['meters'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($totalOfTotals['hours'], 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round(bcdiv($totalOfTotals['meters'], $totalOfTotals['hours'], 3), 2))->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    for ($col = 0; $col != $totalColumnsStartPos + 3; $col++) {
        $length = in_array($col, array(0)) ? 20 : 13;
        $activeSheet->getColumnDimension(nts($col))->setWidth($length);
    }
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Registracijos ataskaita') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
