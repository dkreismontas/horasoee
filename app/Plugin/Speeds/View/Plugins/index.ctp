<div id="mashines" class="{{theme}} columns{{columns}}">
    <!--button v-on:click="loadData()">OK</button>
    <button v-on:click="remove()">Re</button-->
    <div class="row">
        <div class="col-xs-2">
            <label><?php echo __('Atskaitos taškas'); ?></label>
            <input  id="sensorCalendar" v-model="start_date" class="form-control datepicker" type="text" />
        </div>
        <div class="col-xs-1">
            <label><?php echo __('Valandų skaičius'); ?></label>
            <input class="duration form-control" v-model="duration" class="form-control" type="number" value="4" />
        </div>
        <div class="col-xs-9 hours">
            <button class="btn btn-primary hours" v-on:click="changeStartDate(i)" v-for="i in 24" title="{{i}}">{{i}}</button>
        </div>
    </div>
    <div class="row" v-for="(columnNr, row_sensors) in sensors">
        <sensor_comp v-for="(sensor_id, sensor) in row_sensors" :sensor="sensor" :start_date="start_date"></sensor_comp>
    </div>
</div>
<template id="sensorTpl">
    <div class="sensor sensor{{sensor.id}} {{sensor.current_status}} col-xs-{{12/columns}}">
        <div>
            <h1><span><i></i></span>
                <a class="enlarge" target="_blank" href="<?php echo $this->Html->url('/speeds/plugins/index/',true); ?>{{sensor.id}}"></a>
                <a class="sensorTitle" target="_blank" href="<?php echo $this->Html->url('/work-center/',true); ?>{{sensor.line_id}}_{{sensor.id}}">{{sensor.name|shortenString}}</a>
            </h1>
            <div class="box-content">
                <div class="graph" id="graph{{sensor.id}}" style="{{graph_style}}"></div>
            </div>
        </div>
    </div>
</template>
<script type="text/javascript">
    jQuery(document).ready(function($){
        
    });
    setInterval(function(){location.reload();}, 1800000);
    var vm = new Vue({
        el: '#mashines',
        data: {
            sensors: {},
            columns: 0,
            theme: 'white', //white | dark
            counter: 0,
            start_date: '',
            duration: 0, //rodomu valandu default kiekis
        },
        methods: {
            loadData: function(){
                jQuery.ajax({
                    url: "<?php echo $this->Html->url('/speeds/plugins/get_data/'.($sensorId>0?$sensorId:''),true); ?>",
                    data: {start_date:this.start_date, duration:this.duration},
                    type: 'post',
                    context: this,
                    dataType: 'JSON',
                    async: true,
                    success: function(request){
                        this.sensors = request.sensors;
                        this.columns = request.columns;
                    }
                })
            },
            changeStartDate: function(hour){
                hour = hour.toString().length == 1?'0'+hour:hour;
                if(this.start_date == ''){ 
                    this.start_date = new Date().toJSON().slice(0,10);
                }
                this.start_date = this.start_date.substring(0,10);
                this.start_date = this.start_date+' '+hour+':00';
                this.loadData();
            }
        },
        ready: function(){
            jQuery('#sensorCalendar').datepicker({
               dateFormat: 'yy-m-dd',
               firstDay : 1,
            });  
        },
        beforeCompile: function(){ 
            this.loadData();
            setInterval(this.loadData,20000);
        },
        components: {
            'sensor_comp': {
                template: '#sensorTpl',
                props: ['sensor'],
                data: function(){return { columns:0, prepared_data:[], graph_style: '', max_value: 0, satisfactoryLine: false}},
                beforeCompile: function(){
                    this.columns = this.$parent.columns;
                },
                ready: function(){
                    this.buildGraph(); 
                },
                methods: {
                    buildGraph: function(){
                        <?php if($sensorId > 0){ ?>
                            this.graph_style = 'height: '+(jQuery(window).height()-100)+'px;'
                        <?php } ?>
                        this.prepareData();
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(this.drawChart);
                    },
                    prepareData: function(){
                        var i = 0;
                        var prepared_data = [];
                        var max_value = 0;
                        var sensor = this.sensor;
                        var satisfactoryLine = false;
                        this.sensor.history.forEach(function(item){
                            if(sensor.tv_speed_deviation == ''){
                                var tooltipQuantity = sensor.units+': '+item.quantity+"\r\n<?php echo __('Gaminio pavadinimas').': '; ?>"+item.production_name+"\r\n<?php echo __('Gaminio kodas').': '; ?>"+item.production_code+"\r\n<?php echo __('Užsakymo nr.').': '; ?>"+item.mo_number+"\r\n"+item.groupper;
                                prepared_data.push([new Date(item.groupper), parseFloat(item.quantity),tooltipQuantity]);
                            }else{
                                var tooltipQuantity = sensor.units+': '+item.quantity+"\r\n<?php echo __('Gaminio pavadinimas').': '; ?>"+item.production_name+"\r\n<?php echo __('Gaminio kodas').': '; ?>"+item.production_code+"\r\n<?php echo __('Užsakymo nr.').': '; ?>"+item.mo_number+"\r\n"+item.groupper;
                                var tooltipPurpose = '<?php echo __('Tikslas'); ?>'+': '+item.purpose+"\r\n<?php echo __('Gaminio pavadinimas').': '; ?>"+item.production_name+"\r\n<?php echo __('Gaminio kodas').': '; ?>"+item.production_code+"\r\n<?php echo __('Užsakymo nr.').': '; ?>"+item.mo_number+"\r\n"+item.groupper;
                                var singleDotData = [new Date(item.groupper), parseFloat(item.quantity),tooltipQuantity, parseFloat(item.purpose), tooltipPurpose];
                                if(item.satisfactory){
                                    satisfactoryLine = true;
                                    var tooltipSatisfactory = '<?php echo __('Patenkinama riba'); ?>'+': '+item.satisfactory+"\r\n<?php echo __('Gaminio pavadinimas').': '; ?>"+item.production_name+"\r\n<?php echo __('Gaminio kodas').': '; ?>"+item.production_code+"\r\n<?php echo __('Užsakymo nr.').': '; ?>"+item.mo_number+"\r\n"+item.groupper;
                                    singleDotData.push(parseFloat(item.satisfactory));
                                    singleDotData.push(tooltipSatisfactory);
                                }
                                prepared_data.push(singleDotData);
                            }
                            max_value = max_value < parseFloat(item.quantity) ? parseFloat(item.quantity) : max_value;
                        });
                        this.prepared_data = prepared_data;
                        this.max_value = max_value;
                        this.satisfactoryLine = satisfactoryLine;
                    },
                    drawChart: function(){
                        //console.log(this.prepared_data)
                          var data = new google.visualization.DataTable();
                          data.addColumn('datetime', 'X');
                          data.addColumn('number', this.sensor.units);
                          data.addColumn({type:'string',role:'tooltip'});
                          if(this.sensor.tv_speed_deviation != ''){
                              data.addColumn('number', '<?php echo __('Tikslas'); ?>');
                              data.addColumn({type:'string',role:'tooltip'});
                          }
                          if(this.satisfactoryLine){
                              data.addColumn('number', '<?php echo __('Patenkinama riba'); ?>');
                              data.addColumn({type:'string',role:'tooltip'});
                          }
                          data.addRows(this.prepared_data);
                          var options = {
                            hAxis: {
                              //title: '<?php echo __('Laikas'); ?>',
                              gridlines: {color: '#eee', count: this.$parent.duration > 0?this.$parent.duration:5},
                              format: 'H:mm',
                              textStyle: {fontSize: 18},
                              titleTextStyle: {fontSize: 25},
                            },
                            vAxis: {
                              //title: '<?php echo __('Kiekis / min'); ?>',
                              gridlines: {color: '#eee', count: Math.min(parseInt(this.max_value)+1, 10)},
                              textStyle: {fontSize: 18},
                              titleFontSize: 25,
                              position: 'right'
                            },
                            //curveType: 'function',
                            axisTitlesPosition: 'out',
                            chartArea:{left:50,top:30,width:'86%',height:'80%'},
                            legend: { position: 'top' },
                            colors: ['#3f86c4', '#00ff00', '#ff0000'],
                            //this.sensor.current_status
                          };
                    
                          var chart = new google.visualization.LineChart(document.getElementById('graph'+this.sensor.id));
                          chart.draw(data, options);
                    }
                },
                filters: {
                    shortenString: function(string){
                        return string.length > 15?string.substring(0, 15)+'...':string;
                    }
                },
                // watch:{
                    // start_date: function(value){
                        // this.$parent.start_date = value;
                    // }
                // }
            }
        }
    });
</script>
