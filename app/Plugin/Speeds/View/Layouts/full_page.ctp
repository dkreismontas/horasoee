<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?php echo $this->Html->charset(); ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo $appName.($title_for_layout ? (' - '.$title_for_layout) : ''); ?></title>
	
	<?php
	    echo $this->Html->css(array('style.default', 'responsive-tables'));
		echo $this->Html->meta('icon');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
        echo $this->Html->css('Speeds.style.css?v=1');
        echo $this->Html->script('Speeds.vue');
        echo $this->Html->script('Speeds.loader');
	?>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-migrate-1.2.1.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-ui-1.10.3.min.js"></script>
	<script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-ui-timepicker-addon.js"></script>
</head>
<body<?php if (isset($bodyClass) && $bodyClass) { echo ' class="'.htmlspecialchars($bodyClass).'"'; } ?>>
	<?php echo $this->fetch('content'); ?>
</body>
</html>
