<?php
App::uses('AppModel', 'Model');

class SpeedsAppModel extends AppModel {
    public static $navigationAdditions = array( );

    public static function loadNavigationAdditions() {
        self::$navigationAdditions[3] =
            array(
                'name'=>__('Darbo centrų greičiai'),
                'route'=>Router::url(array('plugin' => 'speeds','controller' => 'plugins', 'action' => 'index')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-signal',
                'target'=>'_blank'
            );
    }
}
