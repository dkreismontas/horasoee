<?php
App::uses('Problem','Model');
class Speed extends AppModel{
    public $useTable = false;
    CONST RECORD_AGE = 120; //irasu senumas sekundemis
    CONST RecordDuration = 20; // Kas kiek laiko sukuriamas įrašas, sekundėmis
    public $Record, $Shift, $request, $singleSensorId;
    
    public function combineSensorsIntoColumns($sensors, $columnsCount){
        $rowsCount = ceil(sizeof($sensors) / $columnsCount);
        $data = array();
        for($i = 0; $i < $rowsCount; $i++){
            $data[$i] = array_slice($sensors, $i*$columnsCount, $columnsCount, true);
        }
        return $data;
    }
    
    public function parametersCalculation(&$sensors){
        $histories = $this->getSpeedHistory(array_keys($sensors));
        $currentStatuses = $this->getCurrentStatuses(array_keys($sensors));
        foreach($sensors as &$sensor){
            //$prototype = array('history'=>array());
            //$sensor = array_merge($prototype,$sensor);
            $sensorId = $sensor['id'];
            $sensor['current_status'] = isset($currentStatuses[$sensorId]['status'])?$currentStatuses[$sensorId]['status']:'';
            $sensor['history'] = isset($histories[$sensorId])?$histories[$sensorId]:array();
        }
    }
    
    private function getSpeedHistory($sensorsIds){ 
        $showInterval = isset($this->request['duration']) && intval($this->request['duration'])?min(24, intval($this->request['duration'])):4;
        $time = time();
        $startDate = isset($this->request['start_date']) && trim($this->request['start_date'])?strtotime($this->request['start_date']):$time-(3600*$showInterval);
        $startDate = date('Y-m-d H:i', $startDate);
        //$time = strtotime('2017-10-30 11:59:19');
        $statuses = array();
        $problemsExclusionmodel = ClassRegistry::init('ShiftTimeProblemExclusion');
        $exclusionsList = array_merge(array(0),$problemsExclusionmodel->find('list', array('fields'=>array('id','problem_id'))));
        $bindings = array('belongsTo'=>array('Plan','FoundProblem','Sensor'));
        $searchParameters = array(
            'fields'=>array(
                'GROUP_CONCAT(IF(FoundProblem.problem_id IN ('.implode(',',$exclusionsList).' OR Record.plan_id IS NULL),0,ROUND(Plan.step/60, 2))) AS purpose',
                'GROUP_CONCAT(ROUND(Plan.step/60, 2) - ROUND(Plan.step/60, 2) * CAST(SUBSTRING(REPLACE(tv_speed_deviation,\' \',\'\'), LOCATE(\',\', REPLACE(tv_speed_deviation,\' \',\'\'))+2) AS UNSIGNED) / 100) AS satisfactory',
                'CAST(SUBSTRING(REPLACE(tv_speed_deviation,\' \',\'\'), LOCATE(\',\', REPLACE(tv_speed_deviation,\' \',\'\'))+2) AS UNSIGNED) AS yellow_zone_start',
                'ROUND(SUM(IF(Sensor.plan_relate_quantity = \'quantity\',Record.quantity,Record.unit_quantity)),2) as quantity',
                'TRIM(Plan.production_code) AS production_code',
                'TRIM(Plan.production_name) AS production_name',
                'TRIM(Plan.mo_number) AS mo_number',
                'TRIM(Record.sensor_id) AS sensor_id',
                'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\') AS groupper',
            ),
            'conditions'=>array(
                'Record.created <='=>date('Y-m-d H:i:s', strtotime('+'.$showInterval.' hour',strtotime($startDate))+60), 
                'Record.created >'=>$startDate,
                'Record.sensor_id'=>$sensorsIds
            ),'group'=>array('Record.sensor_id', 'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\')'),
            'order'=>array('Record.sensor_id','Record.created'),
            //'limit'=>120
        );
        $parameters = array(&$searchParameters, &$bindings);
        $pluginData = $this->callPluginFunction('Speed_GetSpeedHistoryBeforeSearch_Hook', $parameters, Configure::read('companyTitle'));  
		$this->Record->bindModel($bindings);
        $historyTmp = $this->Record->find('all', $searchParameters);
        $history = array();
        set_time_limit(3);
        foreach($historyTmp as $historyData){
            $sensorId = $historyData[0]['sensor_id'];
            $historyData[0]['purpose'] = max(explode(',', $historyData[0]['purpose']));
            if($historyData[0]['yellow_zone_start'] > 0) {
                $historyData[0]['satisfactory'] = trim($historyData[0]['satisfactory'])?max(explode(',', $historyData[0]['satisfactory'])):0;
            }else{
                unset($historyData[0]['yellow_zone_start']);
                unset($historyData[0]['satisfactory']);
            }
            $history[$sensorId][] = $historyData[0];
        }
        return $history;
    }

    private function getCurrentStatuses($sensorsIds){
        $time = time();
        //$time = strtotime('2017-10-30 11:59:19');
        $statuses = array();
        $lastStatuses = $this->Record->find('all', array(
            'fields'=>array(
                'MAX(Record.id) as last_id',
                'Record.sensor_id'
            ),'recursive'=>-1,
            'conditions'=>array(
                'Record.created <='=>date('Y-m-d H:i:s', $time-self::RECORD_AGE), 
                'Record.created >'=>date('Y-m-d H:i:s', $time-3600), 
                'Record.sensor_id'=>$sensorsIds
            ),'group'=>array('Record.sensor_id')
        ));
        $this->Record->unbindModel(array('belongsTo'=>array('Sensor')));
        $this->Record->bindModel(array('belongsTo'=>array('FoundProblem', 'Plan','Sensor')));
        $lastStatuses = $this->Record->find('all', array(
            'conditions'=>array('Record.id'=>Set::extract('/0/last_id',$lastStatuses)),
            'group'=>array('Record.sensor_id')
        ));
        foreach($lastStatuses as $data){
            $sensorId = $data['Record']['sensor_id'];
            $statuses[$sensorId]['client_speed'] = $data['Plan']['step'];
            $statuses[$sensorId]['sensor'] = $data['Sensor'];
            if($data['Record']['approved_order_id'] && $data['Record']['plan_id'] && !$data['Record']['found_problem_id']){
                $statuses[$sensorId]['status'] = 'green';
            }elseif($data['Record']['found_problem_id']){
                switch($data['FoundProblem']['problem_id']){
                    case Problem::ID_NEUTRAL: $statuses[$sensorId]['status'] = 'yellow'; break;
                    case Problem::ID_NO_WORK: $statuses[$sensorId]['status'] = 'grey'; break;
                    default: $statuses[$sensorId]['status'] = 'red'; break;
                }
            }elseif($data['Record']['quantity'] > 0){
                $statuses[$sensorId]['status'] = 'green';
            }else{
                $statuses[$sensorId]['status'] = 'red';
            }
        }
        return $statuses;
    }
        
}
    