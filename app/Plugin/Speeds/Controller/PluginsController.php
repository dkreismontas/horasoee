<?php
App::uses('SpeedsAppController', 'Speeds.Controller');

class PluginsController extends SpeedsAppController {
	
    public $uses = array('Sensor','Speeds.Speed','Record','Shift','User');
    
    function index($sensorId=0){
        $this->layout = 'full_page';
		$title_for_layout = __('Greičiai');
        $this->set(compact('sensorId','title_for_layout'));
    }
    
    public function get_data($sensorId=0){
        //$this->allow('*');
        $this->Speed->Record = $this->Record;
        $this->Speed->Shift = $this->Shift;
        $this->Speed->singleSensorId = $sensorId;
        $this->Speed->request = $this->request->data;
        $usersSensors = $this->User->find('list', array('fields'=>array('sensor_id', 'line_id'), 'conditions'=>array('line_id >'=>0)));
        $conditions = array('Sensor.id'=>Configure::read('user')->selected_sensors);
        if($sensorId > 0){
            $conditions['Sensor.id'] = array_intersect(Configure::read('user')->selected_sensors, array($sensorId));
        }
        $sensors = Hash::combine($this->Sensor->find('all', array(
            'fields'=>array(
                'TRIM(Sensor.id) AS id',
                'TRIM(Sensor.name) AS name',
                'TRIM(Sensor.tv_speed_deviation) AS tv_speed_deviation',
                'TRIM(Sensor.branch_id) AS branch_id',
                'TRIM(Sensor.line_id) AS line_id',
                'IF(TRIM(Sensor.units)=\'\',\''.__('Faktas').'\',Sensor.units) AS units'
            ),'conditions'=>array_merge($conditions, array(
                'marked'=>1,
               // 'Sensor.id'=>array(5,6)
            )))),'{n}.0.id', '{n}.0'
        );
        foreach($sensors as $sensorId => &$sensor){ //sis skriptas reikalingas tam, kad galetu isvesti tinkama sensoriaus nuoroda, kuri susideda is linijos ir darbo centro id. Bet linijos id yra ne paties sensoriaus, bet vartotojo linijos, kuriam priklauso sensorius
            $sensor['line_id'] = isset($usersSensors[$sensorId])?$usersSensors[$sensorId]:0;
        }
        $columnsCount = sizeof($sensors) == 1?1:max(min(ceil(sizeof($sensors)/2), 4), 3);
        $this->Speed->parametersCalculation($sensors);
        $sensors = $this->Speed->combineSensorsIntoColumns($sensors, $columnsCount);//fb($sensors);
        //fb($sensors);
        echo json_encode(array(
            'sensors'=> $sensors,
            'columns'=> $columnsCount
        ));
        die();
    }
    
    /*public function beforeFilter(){
        //$this->Auth->allow('Speed/plugins/get_data');
        parent::beforeFilter();
    }*/
	
}
