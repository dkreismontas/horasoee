<?php
App::uses('KitronAppController', 'Kitron.Controller');

class PluginsController extends KitronAppController { 
    
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        $this->layout = 'ajax'; 
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', Configure::read('companyTitle').'.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }
    
    /*function save_sensors_data(){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 7200){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        // $context = stream_context_create(array (
            // 'http' => array (
                // 'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            // )
        // ));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id', 'pin_name')));
        $saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        //$data = json_decode(file_get_contents('http://82.135.222.165:5580/horas/api/v1.0/get', false, $context));
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://82.135.222.165:5580/data',
        );
        $mailOfErrors = array();
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->executeFailedQueries();
        foreach($adressList as $addressPort => $adress){
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $sensorName){
                    $quantity = $record->$sensorName;
                    try{
                        if(Configure::read('ADD_FULL_RECORD')){
                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                        }else{
                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                        }
                        $db->query($query);
                    }catch(Exception $e){
                        $this->Sensor->logFailedQuery($query);
                        $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                    }
                }
            }
        }
        unlink($dataPushStartMarkerPath);
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->Sensor->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->Sensor->moveOldRecords();
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->Sensor->informAboutProblems();
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }*/

    public function sendDiffCardReport(){
        $interval = strtotime(date('Y-m-d',strtotime('-1 day')).' 00:00').' ~ '.strtotime(date('Y-m-d',strtotime('-1 day')).' 23:59:59');
        $attachmentPath = __DIR__.'/../webroot/files/diff_card_report.xls';
        $this->requestAction(array('controller'=>'diff_cards','action'=>'genxls',$interval,'plugin'=>false,'attach_to_file'=>1));
        $userModel = ClassRegistry::init('User');
        $userModel->bindModel(array('belongsTo'=>array('Branch')));
        $emailsList = array_filter($userModel->find('list', array('fields'=>array('User.email'),'recursive'=>1,'conditions'=>array('FIND_IN_SET(User.id, Branch.quality_user_id)'))));
        if(empty($emailsList)){ die('Nera aktyviu emailu padalinyje prie kokybes vadovu'); }
        require_once(ROOT.'/app/Config/email.php');
        $mailConfig = new EmailConfig();
        $mail = new CakeEmail('default');
        $mail->from(isset($mailConfig->default['from_email'])?$mailConfig->default['from_email']:'noreply@horasoee.eu');
        $mail->to($emailsList);
        $mail->subject(__('OEE sistemos neatitikties kortelių ataskaita už praėjusią parą'));
        $mail->emailFormat('both');
        $mail->attachments($attachmentPath);
        $mail->send(__('Ataskaita formuojama laikotarpiui: ').$interval);
        unlink($attachmentPath);
        die();
    }

    public function split_screen_wc(int $sensorId1=0, int $sensorId2=0){
        if(!$sensorId1 || !$sensorId2){
            $this->Session->setFlash(__('Įveskite dviejų sensorių id'));
            $this->redirect('/');
        }
        $sensorModel = ClassRegistry::init('Sensor');
        $sensors = $sensorModel->find('list', array(
            'fields'=>array('Sensor.id', 'Sensor.line_id'),
            'conditions'=>array('Sensor.id'=>array($sensorId1,$sensorId2))
        ));
        $links = array();
        foreach($sensors as $sensorId =>$lineId){
            $links[] = Router::url('/work-center/'.$lineId.'_'.$sensorId, true);
        }
        $this->set(compact('links'));
    }

    
}
