<?php 
ob_end_clean();
$styleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
    );
$row = 4;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, __('Prastovos pavadinimas'));
$sensorColPos = $totalInRow = $totalInCol = array();
//pr($foundProblems);die();
foreach($foundProblems as $problemId => $sensorsHasThisProblem){
    $row++;
    $totalInRow[$row] = 0;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, current($sensorsHasThisProblem)['problem_name']);
    foreach($sensorsHasThisProblem as $sensorId => $foundProblem){
        if(!isset($sensorColPos[$sensorId])){
            $sensorColPos[$sensorId] = !empty($sensorColPos)?max($sensorColPos)+1 : 1; 
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], 4, $foundProblem['sensor_name']);
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], 1, $foundProblem['sensor_name']);
        }
        $duration = bcdiv($foundProblem['duration'], 3600, 2);
        $totalInRow[$row] = bcadd($totalInRow[$row], $duration, 4);
        if(!isset($totalInCol[$sensorColPos[$sensorId]])){ $totalInCol[$sensorColPos[$sensorId]] = 0; }
        $totalInCol[$sensorColPos[$sensorId]] = bcadd($totalInCol[$sensorColPos[$sensorId]], $duration, 4);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], $row, $duration);
    }
}
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, __('Viso'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+1, 4, __('Viso'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+1, $row+1, round(array_sum($totalInCol),2));
foreach($totalInCol as $col => $total){
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+1, round($total,2));
}
for($rowStyle = 4; $rowStyle != $row+2; $rowStyle++){
    for($col = 'A'; $col != PHPExcel_Cell::stringFromColumnIndex(max($sensorColPos)+2); $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.$rowStyle)->applyFromArray($styleArray);
    }
}
foreach($totalInRow as $row => $total){
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+1, $row, round($total,2));
}
$row = 1;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, __('Darbo centrai'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, __('Utilizacija'));
if(isset($this->request->data['fpy_index'])){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, $row, 'FPY');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, $row+1, 'OEE');
}
foreach($sensorColPos as $sensorId => $col){
    $work = isset($foundProblems[-1][$sensorId])?floatval($foundProblems[-1][$sensorId]['duration']):0;
    $downtime223 = isset($foundProblems[223][$sensorId])?floatval($foundProblems[223][$sensorId]['duration']):0;
    $noWork = isset($foundProblems[2][$sensorId])?floatval($foundProblems[2][$sensorId]['duration']):0;
    $downtime240 = isset($foundProblems[240][$sensorId])?floatval($foundProblems[240][$sensorId]['duration']):0;
    $utilisation = bcmul((bcdiv(bcadd($work,$downtime223,6), bcsub(bcsub(bcmul($totalInCol[$col],3600,6),$noWork,6),$downtime240,6) ,4)), 100, 2);
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], $row+1, $utilisation.' %');
    if($utilisation < 60){
        $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex($sensorColPos[$sensorId]).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
    }elseif($utilisation < 65){
        $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex($sensorColPos[$sensorId]).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
    }else{
        $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex($sensorColPos[$sensorId]).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00ff00');
    }
}
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+1, $row, __('Viso'));
for($rowStyle = 1; $rowStyle <= 2; $rowStyle++){
    for($col = 'A'; $col != PHPExcel_Cell::stringFromColumnIndex(max($sensorColPos)+2); $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.$rowStyle)->applyFromArray($styleArray);
    }
}

$work = isset($foundProblems[-1])?floatval(array_sum(Set::extract('/duration',$foundProblems[-1]))):0;
$downtime223 = isset($foundProblems[223])?floatval(array_sum(Set::extract('/duration',$foundProblems[223]))):0;
$noWork = isset($foundProblems[2])?floatval(array_sum(Set::extract('/duration',$foundProblems[2]))):0;
$downtime240 = isset($foundProblems[240])?floatval(array_sum(Set::extract('/duration',$foundProblems[240]))):0;
$utilisation = bcmul((bcdiv(bcadd($work,$downtime223,6), bcsub(bcsub(bcmul(array_sum($totalInCol),3600,6),$noWork,6),$downtime240,6) ,4)), 100, 2);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+1, $row+1, $utilisation.' %');
if($utilisation < 60){
    $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex(($sensorColPos[$sensorId])+1).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('FF0000');
}elseif($utilisation < 65){
    $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex(($sensorColPos[$sensorId])+1).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('ffff00');
}else{
    $objPHPExcel->getActiveSheet()->getStyle(PHPExcel_Cell::stringFromColumnIndex(($sensorColPos[$sensorId])+1).($row+1))->getFill()->setFillType(PHPExcel_Style_Fill::FILL_SOLID)->getStartColor()->setRGB('00ff00');
}
if(isset($this->request->data['fpy_index'])){
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+4, $row, $this->request->data['fpy_index'].' %');
	$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+4, $row+1, bcdiv(bcmul($this->request->data['fpy_index'],$utilisation,4),100,2).' %');
}
for($rowStyle = 1; $rowStyle <= 2; $rowStyle++){
    for($col = PHPExcel_Cell::stringFromColumnIndex(max($sensorColPos)+3); $col != PHPExcel_Cell::stringFromColumnIndex(max($sensorColPos)+5); $col++) {
        $objPHPExcel->getActiveSheet()->getStyle($col.$rowStyle)->applyFromArray($styleArray);
    }
}

$letter = 'A';
for ($c = 0; $c <= sizeof($totalInCol); $c++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($letter++)->setAutoSize(true);
}

header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment;filename="'.__('Įrenginių išnaudojimo ataskaita').'.xls"');
header('Cache-Control: max-age=0');
header('Cache-Control: max-age=1');
header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
header('Cache-Control: cache, must-revalidate');
header('Pragma: public');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit();