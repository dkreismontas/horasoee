<?php
class Grafija extends AppModel{
    
     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }
     
     public function Oeeshell_filterGetFactTimeRecords_Hook(&$time, &$shift, $records, $returnData, $sensors, &$shiftLength){
         if($shift['Shift']['branch_id'] == 6){
             $shiftLength -= (8*60); //atimame 8 valandas, kadangi darbuotjas per 16 pamainos darbo valandu gali ateiti bet kada ir bendrai pamainoje turi pradirbti 8val
         }
     }
}
    