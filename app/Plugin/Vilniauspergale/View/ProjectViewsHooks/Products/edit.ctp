<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>

<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('production_code', array('label' => __('Produkto kodas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('produkto_svoris', array('label' => __('Produkto svoris'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('mase_formoje', array('label' => __('Masė formoje'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('kiekis_formoje', array('label' => __('Kiekis formoje'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('formu_greitis', array('label' => __('Formų greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('aptr_plevele_greitis', array('label' => __('Aptraukimo plėvele greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('sok_vyniojimo_greitis', array('label' => __('Šokolado vyniojimo greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('dez_lankstymo_greitis1', array('label' => __('Dėžučių lankstymo greitis').'1', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('dez_lankstymo_greitis2', array('label' => __('Dėžučių lankstymo greitis').'2', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('pardavimo_vieneto_ilgis', array('label' => __('Produkto ilgis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('eiliu_kiekis', array('label' => __('Eilių kiekis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('linijos_greitis', array('label' => __('Max linijos greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('saldainiu_vyniojimo_greitis', array('label' => __('Saldainių vyniojimo greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('pjautymo_greitis', array('label' => __('Pjaustymo greitis'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
<?php echo $this->Form->end(); ?>
