<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('New Plan'); ?></button>
<br /><br /> */ ?>
<div class="clearfix pull-right">
    <form action="/products/import" method="POST" enctype='multipart/form-data'>
        <input style="float:left; margin-top:4px;" type="file" name="doc"/>
        <button class="btn btn-primary"><?php echo __('Importuoti'); ?></button>
    </form>
</div>
<br/>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Produktas'); ?></th>
        <th><?php echo __('Kodas'); ?></th>
        <th><?php echo __('Linijos kodas'); ?></th>
        <th><?php echo __('Svoris'); ?></th>
<!--        <th>--><?php //echo __('Masė formoje'); ?><!--</th>-->
        <th><?php echo __('Kiekis formoje'); ?></th>
        <th><?php echo __('Formų greitis'); ?></th>
        <th><?php echo __('1'); ?></th>
        <th><?php echo __('2'); ?></th>
        <th><?php echo __('3'); ?></th>
        <th><?php echo __('4'); ?></th>
        <th><?php echo __('5'); ?></th>
        <th><?php echo __('6'); ?></th>
        <th><?php echo __('7'); ?></th>
        <th><?php echo __('8'); ?></th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($list)): ?>
        <tr>
            <td colspan="16"><?php echo __('List is empty.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($list as $li_):
        $li = (object) $li_["Product"]; ?>
        <tr>
            <td><?php echo $li->id; ?></td>
            <td><?php echo $li->name; ?></td>
            <td><?php echo $li->production_code; ?></td>
            <td><?php echo $li->linijos_kodas; ?></td>
            <td><?php echo $li->produkto_svoris; ?></td>
<!--            <td>--><?php //echo $li->mase_formoje; ?><!--</td>-->
            <td><?php echo $li->kiekis_formoje; ?></td>
            <td><?php echo $li->formu_greitis; ?></td>
            <td><?php echo $li->aptr_plevele_greitis; ?></td>
            <td><?php echo $li->sok_vyniojimo_greitis; ?></td>
            <td><?php echo $li->dez_lankstymo_greitis1; ?></td>
            <td><?php echo $li->dez_lankstymo_greitis2; ?></td>
            <td><?php echo $li->pardavimo_vieneto_ilgis; ?></td>
            <td><?php echo $li->eiliu_kiekis; ?></td>
            <td><?php echo $li->linijos_greitis; ?></td>
            <td><?php echo $li->saldainiu_vyniojimo_greitis; ?></td>
            <td>
                <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Edit')); ?>"><span
                        class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a') ?></a><br/>
                <a href="<?php printf($removeUrl, $li->id); ?>"
                   onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');"
                   title="<?php echo htmlspecialchars(__('Remove')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?>
                </a>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
