import MySQLdb
import json
import time
import functions # Custom

sleep_duration = 5 # Seconds

def updateFile():
	with open('main.log', 'a') as fp:
	    fp.write(time.strftime("%Y-%m-%d %H:%M:%S") + ' running\n')

def loop() :
	updateFile()
	print "looping: " + time.strftime("%Y-%m-%d %H:%M:%S")
	# Start db
	db = MySQLdb.connect("localhost","pergale-oee","VzgyLFct","oee")
	cursor = db.cursor(MySQLdb.cursors.DictCursor)

	# Parse partial sensors from DB
	sensors = functions.getSensors(cursor)
	# Build hierarchy of sensors
	hierarchy = functions.sortPartialSensors(sensors)
	if len(hierarchy) == 0 :
		print "No records"
		return
	# Relationship on each partial sensor
	inverted_hierarchy = functions.invertHierarchy(hierarchy)
	# Print hierarchy
	# functions.showHierarchy(hierarchy)
	# Get all partial sensors in a list
	partial_sensors_flat = functions.flattenHierarchyToPartialSensors(hierarchy)
	# Parse all untouched partial sensors records
	records = functions.getUntouchedRecords(cursor,partial_sensors_flat)
	# Join all records to groups of partial sensors and dates
	grouped_records = functions.groupRecords(hierarchy,records)
	# Join records to groups of full records by date
	joined_record_values = functions.joinRecordValues(inverted_hierarchy,grouped_records)
	# Remove not full partial records
	filtered_record_values = functions.removeInvalidRecordValues(hierarchy,joined_record_values)
	if len(filtered_record_values) == 0 :
		print "No records after filtering"
		return
	# Define final values
	final_record_values = functions.finalizeRecordValues(filtered_record_values)
	# Get all record ids to update
	record_ids = functions.getRecordIds(filtered_record_values)
	# Build records for full sensors
	functions.buildFullRecords(cursor,final_record_values)
	# Update partial records
	functions.updateRecords(cursor,json.dumps(record_ids))

	# Commit everything
	db.commit()
	# Close db
	db.close()
	print "Records updated"

while True:
	loop()
	time.sleep(sleep_duration)