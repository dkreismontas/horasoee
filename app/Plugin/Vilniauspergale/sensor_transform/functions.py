from collections import defaultdict
import json
def tree(): return defaultdict(tree)

def getSensors(cursor):
	cursor.execute("SELECT * FROM sensors")
	results = cursor.fetchall()
	return results

def sortPartialSensors(sensors):
	hierarchy = tree()
	for sensor in sensors:
		if sensor["is_partial"] == 1:
			if sensor["full_sensor_id"] not in hierarchy:
				hierarchy[sensor["full_sensor_id"]] = [int(sensor["id"])]
			else:
				hierarchy[sensor["full_sensor_id"]].append(int(sensor["id"]))
	return hierarchy

def invertHierarchy(hierarchy):
	inverted_hierarchy = [0] * 50
	for full_sensor,partial_sensors in hierarchy.iteritems():
		for partial_sensor in partial_sensors:
			inverted_hierarchy.insert(partial_sensor,int(full_sensor))
	return inverted_hierarchy


def showHierarchy(hierarchy):
	for full_sensor,partial_sensors in hierarchy.iteritems():
		print str(full_sensor)
		for partial_sensor in partial_sensors:
			print "-> " + str(partial_sensor)

def flattenHierarchyToPartialSensors(hierarchy):
	partial_sensors_all = [];
	for full_sensor,partial_sensors in hierarchy.iteritems():
		for partial_sensor in partial_sensors:
			partial_sensors_all.append(partial_sensor)
	return partial_sensors_all

def getUntouchedRecords(cursor,partial_sensors_flat):
	partial_sensors_string = str(partial_sensors_flat).strip('[]')
	cursor.execute("SELECT id,created,quantity,sensor_id,shift_id FROM `records` WHERE sensor_id IN (%s) and found_problem_id IS NULL order by created ASC" % partial_sensors_string)
	results = cursor.fetchall()
	return results

def groupRecords(hierarchy,records):
	results_by_sensor = tree()
	for record in records:
		results_by_sensor[record["sensor_id"]][str(record["created"])] = [record["id"],record["quantity"],record["shift_id"]]
	return results_by_sensor

def joinRecordValues(inverted_hierarchy,records):
	results_by_full_sensor = tree()
	partial_record_ids = []
	for partial_sensor_id,inner_content in records.iteritems():
		full_sensor_id =inverted_hierarchy[partial_sensor_id]
		for date, values in inner_content.iteritems():
			if (full_sensor_id not in results_by_full_sensor) or (str(date) not in results_by_full_sensor[full_sensor_id]):
				results_by_full_sensor[full_sensor_id][str(date)] = [[],[],[]]
			results_by_full_sensor[full_sensor_id][str(date)][0].append(values[0])
			results_by_full_sensor[full_sensor_id][str(date)][1].append(values[1])
			results_by_full_sensor[full_sensor_id][str(date)][2] = values[2]
	return results_by_full_sensor

def removeInvalidRecordValues(hierarchy,records):
	results_by_full_sensor = tree()
	for full_sensor_id,inner_content in records.iteritems():
		for date, values in inner_content.iteritems():
			if len(hierarchy[full_sensor_id]) == len(values[0]):
				if (full_sensor_id not in results_by_full_sensor) or (str(date) not in results_by_full_sensor[full_sensor_id]):
					results_by_full_sensor[full_sensor_id][str(date)] = [[],[],[]]
				results_by_full_sensor[full_sensor_id][str(date)][0] = values[0]
				results_by_full_sensor[full_sensor_id][str(date)][1] = values[1]
				results_by_full_sensor[full_sensor_id][str(date)][2] = values[2]
	return results_by_full_sensor

def finalizeRecordValues(records):
	results_by_full_sensor = tree()
	for full_sensor_id,inner_content in records.iteritems():
		for date, values in inner_content.iteritems():
			if full_sensor_id == 11: # Sum this sensor
				results_by_full_sensor[full_sensor_id][str(date)] = [int(getSum(values[1])),int(values[2])]
			else: # Max value of all the other sensors
				results_by_full_sensor[full_sensor_id][str(date)] = [int(max(values[1])),int(values[2])]
	return results_by_full_sensor

def getSum(arr):
	sum = 0
	for element in arr:
		sum += element
	return sum

def getRecordIds(records):
	ids = []
	for full_sensor_id,inner_content in records.iteritems():
		for date, values in inner_content.iteritems():
			ids = ids + values[0]
	return ids

def updateRecords(cursor,record_ids):
	record_ids_string = str(record_ids).strip('[]')
	cursor.execute("UPDATE `records` SET found_problem_id=0 WHERE id IN (%s)" % record_ids_string)
	results = cursor.fetchall()

def buildFullRecords(cursor,records):
	struct = 'created,quantity,unit_quantity,sensor_id,shift_id'
	data = [];
	for full_sensor_id,inner_content in records.iteritems():
		for date, values in inner_content.iteritems():
			single = (date,values[0],0,full_sensor_id,values[1])
			data.append(single)
	stmt = "INSERT INTO `records` (" + struct + ") values (%s,%s,%s,%s,%s)"
	cursor.executemany(stmt,data)