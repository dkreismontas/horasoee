<?php
App::import('Vendor', 'Vilniauspergale.PHPMailer/Exception');
App::import('Vendor', 'Vilniauspergale.PHPMailer/PHPMailer');
App::import('Vendor', 'Vilniauspergale.PHPMailer/SMTP');
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
class Vilniauspergale extends AppModel{
    public $name = 'Vilniauspergale';
    
    public function WorkCenter_BuildStep_Hook(&$fPlan, $product, $fSensor){
        $step_min = 0;
        $sensor_id = $fSensor['Sensor']['id'];
        if($fSensor['Sensor']['plan_relate_quantity'] == 'quantity'){
            $fPlan['box_quantity'] = 1;
        }
        switch ($sensor_id) {
            case 1:
            case 4: // J1
            case 23: //J20
                $step_min = $product['Product']['formu_greitis'] * $product['Product']['kiekis_formoje'];
                break;
            case 6: // J3
            case 7: // J4
                $step_min = $product['Product']['aptr_plevele_greitis'];
                break;
            case 8: // J5
                $step_min = $product['Product']['sok_vyniojimo_greitis'];
                break;
            case 9: // J13
            case 10: // J14
                $step_min = $product['Product']['dez_lankstymo_greitis1'];
                break;
            case 11: // J15, J16 -> sujungtas
                $step_min = $product['Product']['dez_lankstymo_greitis2'];
                break;
            case 17: // J7
            case 18: // J8
            case 19: // J9
            case 20: // J10
            case 21: // J11
            case 28: // J19
                $step_min = $product['Product']['saldainiu_vyniojimo_greitis'];
                break;
            case 14: // J6,J61 -> sujungtas
                $step_min = $product['Product']['linijos_greitis'] / $product['Product']['pardavimo_vieneto_ilgis'] * $product['Product']['kiekis_formoje'];
                break;
            case 27: //J18
                $step_min =  $product['Product']['pjautymo_greitis'];
                break;
            case 29: //J17
                $step_min =  $product['Product']['mase_formoje'];
                break;
            case 24:
                $step_min = $product['Product']['sok_vyniojimo_greitis'];
                break;
            // case 29: //J17
                // $step_min = $product['Product']['kiekis_formoje'] ;
                // break;
        }
        $fPlan['step'] = $step_min * 60;
    }
    
    public function Record_calculateOee_Hook(){
        
    }
    
    public function Products_Index_Hook(&$productsController){
        $productsController->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Products/index');
    }
    public function Products_Edit_Hook(&$productsController){
        $productsController->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Products/edit');
    }
    // public function ReportsContr_BeforeGenerateXLS_Hook(&$reportModel){
        // $reportModel = ClassRegistry::init('Lrytas.PergaleReport');
    // }
    public function ReportsContr_BeforeCalculatePeriodXLS_Hook(&$reportModel, &$templateTitle){
        $templateTitle = '/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Reports/calculate_period_xls';
    }

    public function Mail_BeforeSendMail_Hook(&$mailObj, &$emailsList, &$message, &$subject){
        $config = $mailObj->config();
        $mail = new PHPMailer(true);
        try {
            //Server settings
            //$mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
            $mail->isSMTP();                                            // Send using SMTP
            $mail->Host       = $config['host'];                    // Set the SMTP server to send through
            $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
            $mail->Username   = $config['username'];                     // SMTP username
            $mail->Password   = $config['password'];                               // SMTP password
            //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
            $mail->Port       = $config['port'];                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

            //Recipients
            $mail->setFrom($config['from_email'], 'Vilniaus Pergalė');
            if(is_array($emailsList)){
                foreach($emailsList as $emailsAddr){
                    $mail->addAddress($emailsAddr);
                }
            }else {
                $mail->addAddress($emailsList);     // Add a recipient
            }
            $mail->SMTPSecure = 'tls';

            // Content
            $mail->CharSet = 'UTF-8';
            $mail->isHTML(true);                                  // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            //$mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
