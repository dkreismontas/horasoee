<?php
App::uses('VilniauspergaleAppController', 'Vilniauspergale.Controller');

class PluginsController extends VilniauspergaleAppController {
    
    public $uses = array('Shift','Record','Sensor','DashboardsCalculation','ApprovedOrder');
    
    public function index(){
        $this->layout = 'ajax';
    }
    
    function save_sensors_data(){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 1800){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->informAboutProblems();
        //$this->checkRecordsIsPushing();
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        $tmpRecords = $this->Record->query('SELECT `source`, `date`, `value` FROM `SensorProfileRecords` order by `date`,`source` LIMIT 1000');
        $virtalSensorsQuantities = array();
        foreach($tmpRecords as $tmpRecord){ $tmpRecord = $tmpRecord['SensorProfileRecords'];
            try{
                $date = date('Y-m-d H:i:s',strtotime($tmpRecord['date'].' UTC'));
                $query = 'CALL ADD_FULL_RECORD(\''.$date.'\', '.$tmpRecord['source'].', '.$tmpRecord['value'].', '.Configure::read('recordsCycle').'); ';
                $this->Record->query($query);
                $this->Record->query('DELETE FROM `SensorProfileRecords` WHERE `source` = '.$tmpRecord['source'].' AND `date` = \''.$tmpRecord['date'].'\' ');
                $prevDate = $date;
                if(in_array($tmpRecord['source'], array(12,13))){
                    if(!isset($virtalSensorsQuantities[11][$date])){ $virtalSensorsQuantities[11][$date] = 0; }
                    $virtalSensorsQuantities[11][$date] += $tmpRecord['value'];
                }
                if(in_array($tmpRecord['source'], array(15,16))){
                    if(!isset($virtalSensorsQuantities[14][$date])){ $virtalSensorsQuantities[14][$date] = 0; }
                    $virtalSensorsQuantities[14][$date] += $tmpRecord['value'];
                }
            }catch(Exception $e){
                $this->Log->write('Nepavyko irasyti jutiklio duomenu. Vykdyta uzklausa: '.$query.'. Uzklausos klaida: '.json_encode($e));
                unlink($dataPushStartMarkerPath);
            }
        }
        foreach($virtalSensorsQuantities as $vSensorId => $vQuantities){
            foreach($vQuantities as $date => $vQuantity){
                $query = 'CALL ADD_FULL_RECORD(\''.$date.'\', '.$vSensorId.', '.$vQuantity.', '.Configure::read('recordsCycle').'); ';
                $this->Record->query($query);
                //$this->Log->write('VIRTUALUS QUERY: '.$query);
            }
        }
        unlink($dataPushStartMarkerPath);
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            //sutvarkome atsiustu irasu duomenis per darbo centra
            $this->moveOldRecords();
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }

    public function moveOldRecords() {
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
    }
	
}
