<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
//App::import('Vendor', 'Fernet');
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Record','Bfc.Bfc');

    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
                'http://88.119.154.87:5680/data',
                'http://88.119.154.87:5580/data',
	        );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $ipAndPort);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
	        $mailOfErrors = array();
	        $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
	        if(file_exists($dataPushStartMarkerPath)){
	            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
	                //$this->Sensor->informAboutProblems();
	                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
	            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
	                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
	            }
	        }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
	            $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            if(!$currentShifts[$branchId]){
                    $this->Shift->generateShifts($branchId);
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
	        if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }

			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'\//', $address);
	            });
	        }
            $periodsFound = false;
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
                //$fernetDecoder = new Fernet\Fernet('FdZhRq5lFLmmHmHAEwKPzl7VuO_3Wnsvfjc3ztNUnOg');
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                //$data = json_decode($fernetDecoder->decode($data));
	                $data = json_decode($data);
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
	            foreach($data->periods as $record){
                    $periodsFound = true;
                    $this->fixPairsQuantities($record, array(['i1','i9'], ['i2','i10'], ['i3','i11']), $saveSensors);
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
	                    $quantity = $record->$sensorPinName;

						try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
	                }
	            }
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }

    public function fixPairsQuantities(&$record, $sensorsPairs, $saveSensors){
        $lastRecordsHasProblems = array();
        foreach($sensorsPairs as $pair){
            if(!isset($record->{$pair[0]})){ continue; }
            $values = [];
            foreach($pair as $pin){
                $values[] = $record->$pin;
            }
            if(!array_product($values)){ continue; }    //jei bent vieno is poros jutikliu kiekis yra 0, irasus siunciame iprastai, nieko nedarome
            //jei patenka skriptas cia, vadinasi abu jutikliai siuncia kieki
            if(empty($lastRecordsHasProblems)){
                $this->Record->bindModel(array('belongsTo'=>array('Sensor')));
                $lastRecordsHasProblems = array_filter($this->Record->find('list', array(
                    'recursive'=>1,
                    'fields'=>array('Sensor.pin_name','Record.found_problem_id'),
                    'conditions'=>array('Record.sensor_id'=>array_keys($saveSensors)),
                    'order'=>array('Record.id DESC'),
                    'limit'=>sizeof($saveSensors)
                )));
            }
            $pairHasProblems = array_filter($lastRecordsHasProblems, fn($pin) => in_array($pin, $pair), ARRAY_FILTER_USE_KEY);//pasiliekame tik esamos poros duomenis
            foreach($pair as $key => $pin){
                if(sizeof($pairHasProblems) == sizeof($pair)) { //Jei visi poros jutikliai yra prastovose, kiekis turi nueiti tik pirmam jutikliui is poros saraso, kitiem nunuliname
                    if ($key == 0) {
                        continue;
                    }
                }elseif(!array_key_exists($pin, $pairHasProblems)){ //kai bent vienas poros jutiklis dirba, kiekis nueina prie to, kuris nera prastovoje, kitiem nunuliname
                    continue;
                }
                $record->$pin = 0;
            }
        }
    }
	
}