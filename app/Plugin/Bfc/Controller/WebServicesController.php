<?php
//App::uses('MySoapServer', 'UtenosTrikotazas.Model');
//App::uses('Xml', 'Utility');

class WebServicesController extends BfcAppController
{
    public $name = 'WebServices';
    //public $uses = array('UtenosTrikotazas.UtenosTrikotazas');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('initiate');
    }

    public function curl_test(){
        $order = array(
            'ps_id'=>'test',
            'product_name'=>'gaminys '.time(),
            'mo_number'=>'mo_'.time(),
            'production_time'=>2.5,
        );
        $xml = '<?xml version="1.0" encoding="UTF-8"?><SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="soapserver" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:SOAP-ENC="http://schemas.xmlsoap.org/soap/encoding/" SOAP-ENV:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"><SOAP-ENV:Header><ns1:set_login><username>horas</username><password>oAi+19s1f-</password></ns1:set_login></SOAP-ENV:Header><SOAP-ENV:Body><ns1:start_order><order xsi:type="xsd:string"><![CDATA[
            '.json_encode($order).'
        ]]></order></ns1:start_order></SOAP-ENV:Body></SOAP-ENV:Envelope>';
        $url = Router::url(array('plugin' => 'bfc', 'controller' => 'web_services', 'action' => 'initiate'), true);
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array("Content-Type: text/xml"));
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        if (curl_errno($curl)) {
            throw new Exception(curl_error($curl));
        }
        curl_close($curl);
        echo $result;
        die();
    }

    public function initiate(){
        //file_get_contents('php://input');
        //$this->Log->write('Pradedamas duomenu apsikeitimas per SOAP servisą');
        $this->layout = 'ajax';
        $params = array(
            'location' => Router::url(array('plugin' => 'bfc', 'controller' => 'web_services', 'action' => 'initiate'), true),
            'uri' => ''
        );
        $server = new SoapServer(null, $params);
        $server->setClass('WebService');
        $server->handle();
        $this->Log->write('Užbaigė duomenu apsikeitimą per SOAP servisą');
        $this->render(false);
    }

    //Kreipinys i SoapServisa pasitestavimui
    public function test()
    {
        $params = array(
            'location' => Router::url(array('plugin' => 'utenos_trikotazas', 'controller' => 'server_services', 'action' => 'initiate'), true),
            'uri' => 'soapserver',
            'trace' => 1
        );
        $instance = new SoapClient(null, $params);
        try {
            $headers = array();
            //serveryje visiems hederiams gali buti sukuriamas metodas, kuriame galime nusiskaityti reiksmes. Soap hederiai i SERVER masyva nepatenka
            $headers[] = new SoapHeader('soapserver', 'set_login', (Object)array('username' => 'order_exporter', 'password' => 'Apy^ksX!34'));
            //$headers[] = new SoapHeader('soapserver', 'PASSWORD', 'pavarde');
            pr($instance->__soapCall('export_orders', array(json_encode(array())), null, $headers));
            //pr($instance->__getLastRequest());
        } catch (\Exception $e) {
            throw new \Exception("Soup request failed! Response: " . $instance->__getLastResponse());
        }
        die();
    }

}
