<?php
class PragmaController extends BfcAppController{
	
	public $uses = array('Log');
	
	public function beforeFilter(){
		if(isset($_SERVER['PHP_AUTH_USER']) && $_SERVER['PHP_AUTH_USER'] == 'Pragma' && isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_PW'] == '956dE2yPRVkqKG5u' && $_SERVER['REMOTE_ADDR'] == '213.197.189.242'){
			$this->Auth->allow(array('start_order','end_order'));
		}
		parent::beforeFilter();
	}
	
	public function start_order(){
		$this->Log->write('Gauti duomenys is pragmos: '.file_get_contents('php://input'));
		echo json_encode(array('success'=>1));
		die();
	}
	
	public function end_order(){
		
	}
	
}
