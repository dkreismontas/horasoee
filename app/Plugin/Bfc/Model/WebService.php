<?php
App::uses('ApprovedOrder', 'Model');
App::uses('FoundProblem', 'Model');
App::uses('Problem', 'Model');
class WebService extends BfcAppModel{

    CONST LOGIN = 'horas';
    CONST PASS = 'oAi+19s1f-';
    private static $clientlogin;

    public function __construct(){

    }

    public function start_order($data=''){
        self::check_login();
        $order = json_decode(stripslashes(mb_convert_encoding($data,'UTF-8','US-ASCII')),true);
        $diffkeys = array_diff_key(array_flip(['ps_id','product_name','mo_number','production_time']), $order);
        if(empty($order) || !empty($diffkeys)){ die('Wrong import format. No following parameters provided: '.implode(',', array_keys($diffkeys))); }
        if(!empty($errors = $this->check_order_corrective($order))){
            return implode("\n", $errors);
        }
        $lastApprovedOrder = $this->create_new_order($order);
        if(!empty($lastApprovedOrder)){
            return $this->getLastApprovedOrderData($lastApprovedOrder);
        }
        return 'No previous order found';
    }

    public function set_login($login){
        self::$clientlogin = $login;
    }

    private static function check_login(){
        if(self::$clientlogin->username != self::LOGIN || self::$clientlogin->password != self::PASS){
            die('Login error. Check username and password.');
        }
    }

    private function check_order_corrective($order){
        $sensorModel = ClassRegistry::init('Sensor');
        $errors = [];
        if(empty($sensorModel->findByPsId($order['ps_id']))){
            $errors[] = array('No work center exists with given ps_id value');
        }
        if(isset($order['production_time'])){
            $errors[] = array('Production_time must be greater than 0');
        }
    }

    private function create_new_order($order){
        $planModel = ClassRegistry::init('Plan');
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $sensorModel = ClassRegistry::init('Sensor');
        $fSensor = $sensorModel->findByPsId($order['ps_id']);
        $plan = $planModel->find('first', array('conditions'=>array('Plan.mo_number'=>$order['mo_number'])));
        if(empty($plan)){ //Jei randame egzistuojanti mo, reiskia planas buvo atidetas Pragmoje, dabar paduodamas dar karta. Naujo plano nekuriame, bet pradedame nauja orderi
            $planModel->create();
            $planModel->save(array_merge($order, [  //production_time ir mo_number laukeliu pavadinimai sutampa tarp Pragma ir Plan lenteles
                'sensor_id' => $fSensor['Sensor']['id'],
                'production_name'=>$order['product_name'],
                'box_quantity'=>1,
                'step' => round(3600 / (float)$order['production_time'], 4)
            ]));
            $plan = $planModel->read();
        }
        $approvedOrderModel->findAndComplete($fSensor);
        $approvedOrderModel->create();
        $approvedOrderModel->bindModel(array('belongsTo'=>array('Sensor','Plan')));
        $lastApprovedOrder = $approvedOrderModel->find('first', array(
                'conditions' => array('ApprovedOrder.sensor_id' => $fSensor['Sensor']['id'], 'ApprovedOrder.status_id' => ApprovedOrder::STATE_COMPLETE),
                'order' => array('ApprovedOrder.id DESC'))
        );
        if(empty($lastApprovedOrder)){
            $dateStart = date('Y-m-d H:i:s');
        }else{
            $dateStart = date('Y-m-d H:i:s', strtotime($lastApprovedOrder['ApprovedOrder']['end'])+1);
        }
        $approvedOrderModel->save(array(
            'created' => $dateStart,
            'end' => $dateStart,
            'sensor_id' => $fSensor['Sensor']['id'],
            'plan_id' => $plan['Plan']['id'],
            'status_id' => ApprovedOrder::STATE_IN_PROGRESS,
        ));
        return $lastApprovedOrder;
    }

    private function getLastApprovedOrderData($lastApprovedOrder){
        $recordModel = ClassRegistry::init('Record');
        $settingsModel = ClassRegistry::init('Settings');
        $exclusionsModel = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
        $exclusionList = $exclusionsModel::getExclusionIdList();
        if($exclusionList){$exclusionList = [0];}
        $exceededTransitionId = $settingsModel->getOne('exceeded_transition_id');
        $exceededTransitionId = $exceededTransitionId > 0?$exceededTransitionId:-1000;
        $recordModel->bindModel(array('belongsTo' => array('FoundProblem')));
        $times = $recordModel->find('all', array(
            'fields' => array(
                'SUM('.Configure::read('recordsCycle').') AS total_time',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS green_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id >= '.FoundProblem::STATE_UNKNOWN.' AND FoundProblem.problem_id NOT IN (' . join(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS red_time_no_exclusions',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN ('.implode(',',[$exceededTransitionId,Problem::ID_NEUTRAL]).'),' . (Configure::read('recordsCycle')) . ',0)) as yellow_time',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . join(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS grey_time_only_exclusions',
                'SUM(IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity)) as total_quantity',
            ),'conditions'=>array(
                'Record.sensor_id' => $lastApprovedOrder['Sensor']['id'],
                'Record.approved_order_id'=>$lastApprovedOrder['ApprovedOrder']['id']
            )
        ));
        return array(
            'ps_id'=>$lastApprovedOrder['Sensor']['ps_id'],
            'mo_number'=>$lastApprovedOrder['Plan']['mo_number'],
            'order_duration' => round((float)$times[0]['total_time'] / 60, 2),
            'production_duration'=> round((float)$times[0]['green_time'] / 60, 2),
            'downtime_duration'=> round((float)$times[0]['red_time_no_exclusions'] / 60, 2),
            'changeover_duration'=> round((float)$times[0]['yellow_time'] / 60, 2),
            'order_quantity'=> $times[0]['total_quantity'],
        );
    }

}
