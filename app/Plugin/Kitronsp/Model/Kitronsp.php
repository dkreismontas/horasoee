<?php
class Kitronsp extends AppModel{
     
     /*public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }*/

    public function Workcenter_beforeReturnUserSensor_Hook(&$fUser){
        $fUser['Sensor']['timezone'] = 'Europe/Warsaw';
    }

    public function extendDiffCardDataHook(&$data){
        //prie NK korteles prikabine paskutine sukurta sensoriaus problema
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $lastProblem = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$data['sensor_id'], 'FoundProblem.problem_id >='=>3), 'order'=>array('FoundProblem.id DESC')));
        if($lastProblem){
            $data['found_problem_id'] = $lastProblem['FoundProblem']['id'];
        }
    }

    public function DiffCards_changeReportHeaders_Hook(&$diffCardsController,&$columns,&$searchParameters){
        if(isset($diffCardsController->params['named']['attach_to_file'])){
            $searchParameters['conditions'][User::NAME.DiffCard::PFX_RESPONSIVE_SECTION.'.section'] = 'Kokybės skyrius';
        }
        array_splice($columns, 7, 0, array(array('name'=>__('Užsakymo numeris'),'width'=>14)));
    }

    public function DiffCards_changeReportValues_Hook(&$values, &$dataContainer){
        $additionalData = isset($dataContainer['ApprovedOrder'])?json_decode($dataContainer['ApprovedOrder']['additional_data'],true):array();
        $orderNr = isset($additionalData['add_order_number'])?$additionalData['add_order_number']:'';
        array_splice($values, 7, 0, array($orderNr));
    }

    public function DiffCards_beforeXlsOutput_Hook(&$diffCardsController,&$objPHPExcel){
        if(isset($diffCardsController->params['named']['attach_to_file'])){
            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
            $objWriter->save(__DIR__.'/../webroot/files/diff_card_report.xls');
            return true;
        }else{
            return false;
        }
    }

    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Kitronsp/css/Kitron_bare.css';
    }

    public function Reports_AftergenerateAttributesUsage_Hook(&$ReportsController){
        $ReportsController->set($ReportsController->viewVars);
        $ReportsController->render('/../Plugin/Kitronsp/View/ProjectViewsHooks/Reports/get_attributes_usage_xls');
    }
     
}
    