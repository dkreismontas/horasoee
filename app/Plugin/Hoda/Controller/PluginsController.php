<?php
App::uses('HodaAppController', 'Hoda.Controller');
class PluginsController extends HodaAppController {
	
    public $uses = array('Shift','Record','Sensor');

    public function index(){
        die();
        $this->layout = 'ajax';
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Hoda.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

}
