<?php
class Omniteksas extends AppModel{
     
//     public function Record_calculateOee_Hook(&$oeeParams, $data){
//        $oeeParams['operational_factor'] = 1;
//        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
//        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
//     }
     
     public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
        $settingsModel = ClassRegistry::init('Settings');
        $testDowntimeId = $settingsModel->getOne('test_production_downtime_id');
        if(!$testDowntimeId){ return null; }
        $planModel = ClassRegistry::init('Plan');
        $planModel->bindModel(array('belongsTo'=>array('Product')));
        $plan = $planModel->findById($appOrder['ApprovedOrder']['plan_id']);
        $planName = isset($plan['Product']['name']) && trim($plan['Product']['name'])?$plan['Product']['name']:$plan['Plan']['production_name'];
        if(!preg_match('/^test.+/i',trim($planName))){ return null; }
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $logModel = ClassRegistry::init('Log');
        $shiftModel = ClassRegistry::init('Shift');
        $recordModel = ClassRegistry::init('Record');
        $sensorModel = ClassRegistry::init('Sensor');
        $foundProblemModel->deleteAll(array('FoundProblem.end >'=>$appOrder['ApprovedOrder']['created'], 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
        $fShift = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $start = new DateTime($appOrder['ApprovedOrder']['created']);
        $quantity = $recordModel->find('first', array('fields'=>array('SUM(Record.quantity) AS quantity_sum'), 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$start->format('Y-m-d H:i:s'))));
        $currentProblem = $foundProblemModel->newItem($start, new DateTime(),
            $testDowntimeId, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $plan[Plan::NAME]['id'], array(), $quantity[0]['quantity_sum']);
        $recordModel->updateAll(
            array('Record.found_problem_id'=>$currentProblem['FoundProblem']['id']),
            array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$currentProblem['FoundProblem']['start'])
        );
        $sensorModel->updateAll(
            array('Sensor.tmp_prod_starts_on'=>$fSensor['Sensor']['prod_starts_on'], 'Sensor.prod_starts_on'=>9999),
            array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
        );
    }

	public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
        if($fSensor['Sensor']['prod_starts_on'] >= 9999){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(
                array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
                array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
            );
        }
    }
	
	public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $plansIds = Set::extract('/Plan/id', $plans);
        if(isset($currentPlan['ApprovedOrder'])){$plansIds[] = $currentPlan['Plan']['id'];}
        if(!empty($plansIds)){
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $approvedOrders = $approvedOrderModel->find('list', array('fields'=>array('plan_id', 'id'),'conditions'=>array('ApprovedOrder.plan_id'=>$plansIds)));
            if(!empty($approvedOrders)){
                $partialQuantityModel = ClassRegistry::init('PartialQuantity');
                $partialQuantities = Hash::combine($partialQuantityModel->find('all', array(
                    'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'TRIM(PartialQuantity.approved_order_id) AS approved_order_id'),
                    'conditions'=>array('PartialQuantity.approved_order_id'=>$approvedOrders),
                    'group'=>array('PartialQuantity.approved_order_id')
                )),'{n}.0.approved_order_id', '{n}.0.quantity');
                if(isset($currentPlan['ApprovedOrder']) && isset($partialQuantities[$currentPlan['ApprovedOrder']['id']])){
                    $currentPlan['Plan']['partial_quantity'] = $partialQuantities[$currentPlan['ApprovedOrder']['id']];   
                }
                foreach($plans as &$plan){
                    $planId = $plan['Plan']['id'];
                    if(!isset($approvedOrders[$planId])){ continue; }
                    if(isset($partialQuantities[$approvedOrders[$planId]])){
                        $plan['Plan']['partial_quantity'] = $partialQuantities[$approvedOrders[$planId]];
                    }
                }
            }
        }
        foreach($plans as &$plan){
            $plan['Plan']['step'] = round($plan['Plan']['step'] / 60, 2);
        }
        if($currentPlan){
            $currentPlan['Plan']['step'] = round($currentPlan['Plan']['step'] / 60, 2);
            $currentPlan['Plan']['step_unit_divide'] = 1;
        }
		
    }
     
}
    