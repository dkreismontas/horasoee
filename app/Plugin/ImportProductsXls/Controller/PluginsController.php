<?php
App::uses('ImportProductsXlsAppController', 'ImportProductsXls.Controller');
App::uses('Xml', 'Utility');
class PluginsController extends ImportProductsXlsAppController {
    CONST basePath = '/app/Plugin/ImportProductsXls/files/';
	public $uses = array('ImportProductsXls.Import');
    public $name = 'Plugins';
	private $info;

    public function index(){
        $this->layout = 'empty';
		/*$attributesReceptivity = ClassRegistry::init('AttributesReceptivity');
		$attributesReceptivity->save(array(
			'attribute_identifier'	=> 'a',
			'process_identifier'	=> 'b',
			'unit_code'				=> 'c',
			'duration' 				=> round(10.45478745,4)
		));*/
    }
	
	public function select_settings(){}
	
	public function start_upload(){
		$this->Import->readXlsFile();
		if(empty($this->Import->rawData)){
			echo __('Nepavyko gauti duomenų. Patikrinkite failą.');	die();
		}
		if($this->request->data['selector'] == 0){ //failo duomenu isvedimas i ekrana
            echo __('Duomenys rasti. Galima pradėti importavimą.');
			$this->create_preview_table();
		}else{ //uzsakymu importavimas
			$this->Import->moveDataToDb();
		}
		die();
	}
	
	private function create_preview_table(){
		$this->layout = 'ajax';
		$view = new View($this, false);
		$view->set(array(
			'data'=>$this->Import->rawData,
			'dataColumnsPos'=>$this->Import->dataColumnsPos
		));
		echo $view->render(__FUNCTION__);
	}
	
	
}
