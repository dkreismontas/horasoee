<?php

class Import{
    var $name = __CLASS__;
    public $fileName;
    public $rawData;
    public $fileBasePath;
    public $dataColumnsPos = array();
    
    public function __construct(){
        $this->dataColumnsPos = array(
            0 => array('name'=>__('Produkto pavadinimas')),
            1 => array('production_code'=>__('Produkto kodas')),
            2 => array('kiekis_formoje'=>__('Kiekis formoje')),
            3 => array('formu_greitis'=>__('Formų greitis (formos/h)')),
            4 => array('production_time'=>__('Gamybos ciklas (s)')),
            5 => array('work_center'=>__('Darbo centrai')),
        );
    }

    public function readXlsFile()
    {
        $this->fileName = basename($_FILES["file"]["name"]);
        $this->fileBasePath = $_SERVER['DOCUMENT_ROOT'] . '/app/Plugin/ImportProductsXls/files' . '/' . $this->fileName;
        $fileType = pathinfo($this->fileName, PATHINFO_EXTENSION);
        if (!in_array($fileType, array('xls', 'xlsx'))) {
            die(__('Netinkamas failo formatas. '.$fileType));
        }
        if ($_FILES['file']['error'] == UPLOAD_ERR_OK             //checks for errors
            && is_uploaded_file($_FILES['file']['tmp_name'])
        ) { //checks that file is uploaded
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $this->fileBasePath)) {
                try {
                    set_time_limit(200);
                    App::import('Vendor', 'mexel/PHPExcel');
                    App::import('Vendor', 'mexel/PHPExcel/IOFactory');
                    $objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');
                    try {
                        $objPHPExcel = $objPHPExcel->load($this->fileBasePath);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($this->fileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $sheetData = $objPHPExcel->getSheet(0);
                    $this->rawData = $this->getDataSheet($sheetData);
                } catch (Exception $e) {
                    return false;
                }
            }
        }
        @unlink($this->fileBasePath);
    }

    

    public function getDataSheet($sheet){
        $fullData = array();
        $highestRow = $this->highestRow($sheet, 1);
        $highestColumn = $this->highestColumn($sheet, 2, 0);
        $i = 1;
        $currentRow = 1;
        $colsPosTitles = array();
        foreach($this->dataColumnsPos as $key => $col){
            $colsPosTitles[$key] = key($col);
        }
        while ($i < $highestRow) { //  skaitome tol, kol rasime tuscia eilute
            $i += 1;
            $data = $sheet->rangeToArray('A' . $currentRow . ':' . $highestColumn . $currentRow, '0', TRUE, FALSE);
            $data = array_filter($data[0], 'strlen');
            if(sizeof($data)>sizeof($colsPosTitles)){
                $colsCountDiff = sizeof($data) - sizeof($colsPosTitles);
                for($c = 1; $c <= $colsCountDiff; $c++){
                    $colsPosTitles[] = $data[sizeof($colsPosTitles)];//array($data[sizeof($colsPosTitles)]=>$data[sizeof($colsPosTitles)].'{'.__('Papildomas parametras %d',$c).'}');
                }
            }
            $data = array_combine($colsPosTitles, $data);
            $currentRow++;
            if (empty($data)) break;
            $fullData[] = $data;
        }
        return $fullData;
    }

    

    public function highestRow($sheet, $row = 1){
        $currentRow = $row;
        do {
            $currentRow++;
            $index = "A" . $currentRow;
            $content = trim($sheet->getCell($index)->getValue());
        } while ($content != "");
        return $currentRow;
    }

    public function highestColumn($sheet, $row = 1, $offset = 0){
        $currentColumn = 0;
        do {
            $index = PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ($row); 
            $content = trim($sheet->getCell($index)->getValue());
            $currentColumn++;
        } while ($content != "");
        $currentColumn -= 2;
        return PHPExcel_Cell::stringFromColumnIndex($currentColumn - $offset);
    }

    public function moveDataToDb(){
        array_shift($this->rawData);
        $productModel = ClassRegistry::init('Product');
        $productModel->virtualFields = array('unique'=>'CONCAT(Product.work_center,\'_\',Product.production_code)');
        $allProducts = $productModel->find('list', array('fields'=>array('unique', 'Product.id')));
		foreach($this->rawData as $rawData){
			$uniqueKey = $rawData['work_center'].'_'.$rawData['production_code'];
            if(isset($allProducts[$uniqueKey])){
                $productModel->id = $allProducts[$uniqueKey];
            }else{
            	$productModel->create();
            }
			$productModel->save($rawData);
			$allProducts[$uniqueKey] = $productModel->id;
		}
        die();
    }

}
