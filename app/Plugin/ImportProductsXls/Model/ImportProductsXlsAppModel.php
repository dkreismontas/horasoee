<?php
App::uses('AppModel', 'Model');

class ImportProductsXlsAppModel extends AppModel {
    
    public static $navigationAdditions = array( );
    
    public static function loadNavigationAdditions() {
        self::$navigationAdditions[1] =
            array(
                'name'=>__('Gaminių importavimas iš xls failo'),
                'route'=>Router::url(array('plugin' => 'import_products_xls','controller' => 'plugins/select_settings', 'action' => 'index')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
}
