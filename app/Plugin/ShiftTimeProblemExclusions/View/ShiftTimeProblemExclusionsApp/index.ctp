<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif;  ?>
<?php //print_r($problemOptions);die(); ?>
<form action="/ShiftTimeProblemExclusions/add" method="POST">
    <div class="row-fluid">
        <div class="col-sm-3">
            <select name="problem_id" class="form-control">
                <?php
                foreach ($problemOptions as $problem) { ?>
                    <option value="<?php echo $problem['Problem']['id']; ?>"><?php echo 'ID: '.$problem['Problem']['id'].'. '.Settings::translate(Problem::buildName($problem, true)); ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-sm-3">
            <input type="submit" value="<?php echo __('Įtraukti į planinių prastovų sąrašą'); ?>" class="btn btn-primary" />
        </div>
    </div>
</form>

<table class="table">
    <tr>
        <th><?php echo __('Prastova'); ?></th>
        <th><?php echo __('Veiksmai'); ?></th>
    </tr>
    <?php
    foreach ($list as $item) { ?>
        <tr>
            <td><?php echo 'ID: '.$item['Problem']['id'].'. '.Problem::buildName($item, true); ?></td>
            <td>
                <?php echo $item['Problem']['id'] >= 3?$this->Html->link(__('Pašalinti'), array('controller' => 'ShiftTimeProblemExclusionsApp', 'action' => 'remove', 'id'=>$item['ShiftTimeProblemExclusion']['id'])):''; ?>
            </td>
        </tr>
    <?php } ?>
</table>