<?php

App::uses('AppModel', 'Model');

class ShiftTimeProblemExclusionsAppModel extends AppModel {

    public static function getProblemTypeLinks() {
        return '<a href="/ShiftTimeProblemExclusions" style="float:right;margin-right:10px;color:white" class="btn btn-primary">'.__('Prastovos, mažinančios pamainų laiką').'</a>';
    }

    public static function getExclusionIdList() {
        $stpe = ClassRegistry::init('ShiftTimeProblemExclusion');
        $all = $stpe->find('list',array('fields'=>array('problem_id')));
        return $all;
    }
}
