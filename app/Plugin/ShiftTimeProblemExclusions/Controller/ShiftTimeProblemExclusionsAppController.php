<?php

App::uses('AppController', 'Controller');

class ShiftTimeProblemExclusionsAppController extends AppController {
    public $uses = array('Line','ShiftTimeProblemExclusion','Log');

    public function beforeFilter() {
        $this->createTable();
        parent::beforeFilter();
    }

    public function index() {
        $this->loadModel('Problem');
        $this->loadModel('ShiftTimeProblemExclusion');
        $this->ShiftTimeProblemExclusion->bindModel(array('belongsTo'=>array('Problem')));
        $list = $this->ShiftTimeProblemExclusion->find('all',array('conditions'=>array('Problem.line_id'=>$this->Line->getAsSelectOptions(true,true))));
        $alreadyInListProblems = $this->ShiftTimeProblemExclusion->find('list',array('fields'=>array('problem_id')));
        $alreadyInListProblems[] = -1;
        //$alreadyInListProblems[] = 1;
        //$alreadyInListProblems[] = 3;
        $this->Problem->bindModel(array('belongsTo'=>array('Line')));
        $problemOptions = $this->Problem->find('all',array('conditions'=>array('Problem.id >'=>Problem::ID_NOT_DEFINED, 'Problem.id !='=>$alreadyInListProblems, 'Problem.line_id'=>$this->Line->getAsSelectOptions(true,true))));
        $this->set(array(
            'h1_for_layout'    => __('Problemos, mažinančios pamainos laiką'),
            'list'=>$list,
            'problemOptions'=>$problemOptions,
            'title_for_layout' => __("Problemos, mažinančios pamainos laiką"),
        ));
    }

    public function add() {
        $this->loadModel('ShiftTimeProblemExclusion');
        $problem_id = $this->request->data['problem_id'];
        $exists = $this->ShiftTimeProblemExclusion->find('first',array('conditions'=>array('problem_id'=>$problem_id)));
        if(!empty($exists)) {
            $this->redirect('/ShiftTimeProblemExclusions/');
        }
        $this->ShiftTimeProblemExclusion->save(array('problem_id'=>$problem_id));
        $this->Log->write('Itrauke prastova i pamainos laika mazinanciu prastovu sarasa: '.$problem_id);
        $this->redirect('/ShiftTimeProblemExclusions/');
    }

    public function remove() {
        $this->loadModel('ShiftTimeProblemExclusion');
        $this->ShiftTimeProblemExclusion->delete($this->request->params['id'],false);
        $this->redirect('/ShiftTimeProblemExclusions/');
    }

    private function createTable() {
        $this->loadModel('Problem');
        $this->Problem->query('
        CREATE TABLE IF NOT EXISTS `shift_time_problem_exclusions` (
          `id` INT(11) PRIMARY KEY AUTO_INCREMENT,
          `problem_id` INT(11) UNIQUE KEY
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;');
    }
}
