<?php

Router::connect('/ShiftTimeProblemExclusions', array('plugin'=>'ShiftTimeProblemExclusions','controller' => 'ShiftTimeProblemExclusionsApp', 'action' => 'index'));
Router::connect('/ShiftTimeProblemExclusions/add', array('plugin'=>'ShiftTimeProblemExclusions','controller' => 'ShiftTimeProblemExclusionsApp', 'action' => 'add'));
Router::connect('/ShiftTimeProblemExclusions/:id/remove', array('plugin'=>'ShiftTimeProblemExclusions','controller' => 'ShiftTimeProblemExclusionsApp', 'action' => 'remove'));