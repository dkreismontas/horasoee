<div class="row">
    <div class="col-md-3" style="border: 1px solid #ccc; border-right: 0;">
        <?php
        echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control',
            )
        ));
        echo __('Ieškoti pagal datą');
        ?>
        <div class="row">
            <div class="col-md-8">
                <?php echo $this->Form->input('start_end_date', array('label' => __('Nuo - iki'), 'class'=>'form-control daterange')); ?>
            </div>
            <!--div class="col-md-4">
                <?php echo $this->Form->input('end_date', array('label' => __('Pabaiga'), 'class'=>'form-control datepicker')); ?>
            </div-->
            <div class="col-md-4">
                <?php echo $this->Form->input('product_code', array('label' => __('Gaminio kodas'))); ?>
            </div>
        </div>

        <?php echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
    <div class="col-md-2" style="border: 1px solid #ccc; ">
        <?php echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control',
            )
        ));
        echo __("Ieškoti pagal užsakymo nr.");
        echo $this->Form->input('mo', array(
            'label' => __('Įveskite užsakymo nr'),
        ));
        echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
</div>
<div class="mo-picker">
    <?php if(isset($mo_list)) echo $this->element('mo_picker'); ?>
</div>


<script>
    jQuery("document").ready(function ($) {
        jQuery(".datepicker").datetimepicker({
            dateFormat: "yy-mm-dd"
        });
        if(jQuery.uniform)
            jQuery('input:checkbox, input:radio, .uniform-file').uniform();


        jQuery("div.mo-picker .date-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");

            var checker =  $(this).closest("li");
//            console.log(checker.find("ul").find(".checker"));
            var input = checker.find("ul").find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
        jQuery("div.mo-picker .line-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");
            var checker =  $(this).closest('span.mo-line').next();
            console.log($(this));
            console.log(checker);
            var input = checker.find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
        $('input.daterange').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD HH:mm', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ " 
            },
            forceUpdate: true,
            timePicker: true,
            timePicker24Hour: true,
            timePickerIncrement: 5,
            opens: 'right',
          }, function(start, end, label) {
            //console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));
        });
    });

</script>
<style>.ui-state-active {background-color:#ccc;}</style>
<?php echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon'); ?>

<?php if (!empty($mo_number))
{
    $i = 0;?>
    <br/>
    <div><b><?php echo __("Rezultatų kiekis") . ": " . (is_array($mo_number)?count($mo_number):'1'); ?></b></div>
    <table class="table table-bordered mo-stats">

        <?php foreach ($recordsFormatted as $mo_number => $record)
        {
            if ($i != 0)
            { ?>
                <tr>
                    <td>&nbsp;</td>
                </tr>

                <?php
            }
            $i ++;
            $data = $record['data'];
            $nr = isset($data['additional_data']['add_order_number'])?' (nr: '.$data['additional_data']['add_order_number'].')':'';
            ?>
            <tr class="mo-name">
                <td>&nbsp;</td>
                <td><?php echo $mo_number; ?></td>
                <td colspan="3"><?php echo $data['name'].' ('.$data['production_code'].')'.$nr ?></td>
                <td colspan="2"><?php echo __("Pradžia") . " " . $data['start'] ?></td>
                <td colspan="2"><?php echo __("Pabaiga") . " " . $data['end'] ?></td>
            </tr>

            <tr class="mo-header">
                <th>&nbsp;</th>
                <th><?php echo __('Supakuotas kiekis, vnt.'); ?></th>
                <th><?php echo __('Supakuotas kiekis, kg.'); ?></th>
                <th><?php echo __('Linijos vidurys, vnt'); ?></th>
                <th><?php echo __('Formavimas, vnt'); ?></th>
                <th><?php echo __('Patvirtintas kiekis, vnt'); ?></th>
                <th><?php echo __('Praradimai linijos vid., vnt'); ?></th>
                <th><?php echo __('Praradimai formavime, vnt'); ?></th>
                <th><?php echo __('Visas praradimas, vnt'); ?></th>


                <th class="mo-border-left"><?php echo __('NK deklaruotas brokas visas (kg)'); ?></th>

                <?php foreach ($loss_types as $loss_type)
                { ?>
                    <th><?php echo $loss_type['loss_name'] ?></th>
                <?php } ?>

                <th><?php echo __('Norma (vnt/h)') ?></th>

                <th class="mo-green mo-border-left"><?php echo __('Gamybos laikas'); ?></th>

                <?php foreach ($problems as $key => $problem)
                {
                    $problem_id = $problem['problem_id'];
                    ?>
                    <th class="<?php echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?>"><?php echo __($problem['problem_name']); ?></th>
                <?php } ?>

                <th class="mo-border-left"><?php echo __('OEE %'); ?></th>
                <th><?php echo __('Prieinamumo koeficientas %'); ?></th>
                <th><?php echo __('Efektyvumo koeficientas %'); ?></th>
                <!--                <th>Kokybės koeficientas</th>-->

                <th><?php echo __('Pradžia'); ?></th>
                <th><?php echo __('Pabaiga'); ?></th>

            </tr>

            <tr class="mo-combined">
                <td>&nbsp;</td>
                <td><?php if (isset($data['sum_supakuotas'])) echo $data['sum_supakuotas']; ?></td>
                <td><?php if (isset($data['sum_supakuotas'])) echo bcmul($data['sum_supakuotas'],bcdiv($data['step_kg'], $data['step'],6),2); ?></td>
                <td><?php if (isset($data['sum_pries_raikyma'])) echo $data['sum_pries_raikyma'] ?></td>
                <td><?php if (isset($data['sum_pries_kepima'])) echo $data['sum_pries_kepima'] ?></td>
                <td>&nbsp;</td>

                <td <?php if (isset($data['sum_pries_kepima']) && isset($data['praradimai_del_kepimo'])) echo ($data['praradimai_del_kepimo'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_del_kepimo'])) echo $data['praradimai_del_kepimo'] ?></td>

                <td <?php if (isset($data['praradimai_del_raikymo'])) echo ($data['praradimai_del_raikymo'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_del_raikymo'])) echo $data['praradimai_del_raikymo'] ?></td>
                <td <?php if (isset($data['praradimai_visas'])) echo ($data['praradimai_visas'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_visas'])) echo $data['praradimai_visas'] ?></td>


                <td class="mo-border-left"><?php echo $data['sum_consequences'] ?></td>

                <?php foreach ($loss_types as $loss_type)
                {
                    $loss_type_id = $loss_type['loss_type_id'];
                    ?>
                    <td><?php if (isset($data['losses'][$loss_type_id])) echo number_format($data['losses'][$loss_type_id], 3); else echo "0.000"; ?></td>
                <?php } ?>

                <td class="mo-border-left">&nbsp;</td>
                <td class="mo-border-left">&nbsp;</td>

                <?php foreach ($problems as $key => $problem)
                { ?>
                    <td>&nbsp;</td>
                <?php } ?>

                <td class="mo-border-left">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <!--                <td>&nbsp;</td>-->

                <td>&nbsp;</td>
                <td>&nbsp;</td>

            </tr>


            <?php foreach ($record['sensors'] as $sensor){
            foreach($sensor as $id=>$approvedOrder) {
                ?>
                <tr>
                    <td><?php echo $approvedOrder['ps_id'] ?></td>
                    <td><?php if ($approvedOrder['type'] == 3) echo $approvedOrder['sum_quantity']; ?></td>
                    <td><?php if ($approvedOrder['type'] == 3) echo bcmul($approvedOrder['sum_quantity'],bcdiv($data['step_kg'], $data['step'],6),2); ?></td>
                    <td><?php if ($approvedOrder['type'] == 2) echo bcmul($approvedOrder['sum_quantity'],bcdiv($data['step_kg'], $data['step'],6),2); ?></td>
                    <td><?php if ($approvedOrder['type'] == 1) echo bcmul($approvedOrder['sum_quantity'],bcdiv($data['step_kg'], $data['step'],6),2); ?></td>

                    <td><?php if (isset($approvedOrder['confirmed_quantity'])) echo $approvedOrder['confirmed_quantity'] ?></td>

                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>


                    <td class="mo-border-left"><?php echo $approvedOrder['sum_consequences'] ?></td>

                    <?php foreach ($loss_types as $loss_type)
                    {
                        $loss_type_id = $loss_type['loss_type_id'];
                        ?>
                        <td><?php if (isset($approvedOrder['losses'][$loss_type_id])) echo number_format($approvedOrder['losses'][$loss_type_id]['loss_value'],3); ?></td>
                    <?php } ?>

                    <td>
                        <?php
                        if(array_key_exists($approvedOrder['id'],$plans_stepsFormatted[$mo_number])) {
                            echo $plans_stepsFormatted[$mo_number][$approvedOrder['id']];
                        }?></td>

                    <td class="mo-green mo-border-left"><?php echo $this->App->changeUnixSecondsToHumanTime($approvedOrder['sum_gamybos_laikas'], true) ?></td>
                    <!--                <td class="mo-green mo-border-left">--><?php //echo $approvedOrder['sum_gamybos_laikas']; ?><!--</td>-->

                    <?php foreach ($problems as $key => $problem)
                    {
                        $problem_id = $problem['problem_id'];
                        ?>
                        <!--                    <th class="--><?php //echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?><!--">--><?php //if (isset($approvedOrder['found_problems'][$problem_id])) echo $approvedOrder['found_problems'][$problem_id]['problem_length']; ?><!--</th>-->
                        <th class="<?php echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?>"><?php if (isset($approvedOrder['found_problems'][$problem_id])) echo $this->App->changeUnixSecondsToHumanTime($approvedOrder['found_problems'][$problem_id]['problem_length'], true); ?></th>
                    <?php } ?>
                    <td class="mo-border-left"><?php if (isset($approvedOrder['oee']['oee'])) echo round($approvedOrder['oee']['oee'], 0)?></td>
                    <td><?php if (isset($approvedOrder['oee']['avail'])) echo round($approvedOrder['oee']['avail']*100,0); ?></td>
                    <td><?php if (isset($approvedOrder['oee']['perf'])) echo round($approvedOrder['oee']['perf']*100, 0);?></td>
                    <!--                <td>--><?php //if (isset($approvedOrder['oee']['quality'])) echo $approvedOrder['oee']['quality']?><!--</td>-->

                    <td><?php echo $this->app->changeToNonBreakingSpaces($approvedOrder['jutiklio_gamybos_pradzia']); ?></td>
                    <td><?php echo $this->app->changeToNonBreakingSpaces($approvedOrder['jutiklio_gamybos_pabaiga']); ?></td>

                </tr>

            <?php } }?>
            <?php
            if(!empty($record['verifications'])){
                 echo '<tr>';
                echo '<td><strong>'.__('Patikros').'</strong></td>';
                echo '</tr>';
            }
            foreach($record['verifications'] as $verifications){
                echo '<tr>';
                echo '<td>'.__('Kiekis').': '.$verifications['Verification']['current_count'].'</td>';
                echo '<td>'.__('Data').': '.$verifications['Verification']['created'].'</td>';
                echo '<td>'.__('Kodas').': '.$verifications['Plan']['production_code'].'</td>';
                echo '<td>'.__('Gamybos pradžia').': '.$verifications['ApprovedOrder']['created'].'</td>';
                echo '<td>'.__('Gamybos pabaiga').': '.$verifications['ApprovedOrder']['end'].'</td>';
                echo '</tr>';
            }
            ?>

        <?php } ?>

    </table>

<?php } ?>