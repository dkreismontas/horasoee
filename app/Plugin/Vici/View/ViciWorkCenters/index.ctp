<?php
echo $this->Html->css(Configure::read('companyTitle').'.work_center.css?v=1');
echo $this->Html->script(Configure::read('companyTitle').'.vue');
?>
<div id="headerPart">
    <p>UAB "Plungės kooperatyvinė prekyba"</p>
    <h1><?php echo implode(' ', array_slice(explode(' ', $fSensor['Sensor']['name']),0,2)); ?></h1>
</div>
<div id="workCenter">
    <div class="row-fluid">
        <div class="col-lg-8" id="periodsBlock">
            <div v-for="hour in request.periods" class="hours">
                <div class="legends">
                    <div v-for="h in 61" v-if="h%5==0" class="hourLegend"><span>{{h}}</span></div>
                </div>
                <div class="hour">
                    <div class="hourTitle">{{hour.value}}</div>
                    <div class="intervalsContainer">
                        <div v-for="interval in hour.periods" class="interval type-{{interval.type}}" style="width: {{interval|getIntervalWidth}}%; left: {{interval|getLeftPos}}%;" ></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4" id="periodsColorsLegends">
            <div>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td class="goodSpeed">Darbas<br />(geras greitis)</td>
                        <td class="slowSpeed">Darbas<br />(lėtas greitis)</td>
                        <td class="downtime">Prastova</td>
                        <td class="changeover">Perėjimas</td>
                    </tr>
                </table>
            </div>
        </div>
        <br class="clear" />
    </div>
    <div class="row-fluid">
        <div class="col-lg-5 col-md-12" id="speedBlock">
            <div>
                <div id="speedStatus" class="arrow{{request.arrow}}">
                    <div class="speed">{{request.lastMinuteSpeed}}</div>
                </div>
                <ul id="plansBlock">
                    <li class="activePlan" v-if="request.activeOrder"><marquee behavior="scroll" direction="left">{{request.activeOrder.Plan.production_name}}</marquee></li>
                    <li v-for="plan in request.nextPlans"><marquee behavior="scroll" direction="left">{{plan.Plan.production_name}}</marquee></li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-md-6" id="oeeBlock">
            <div>
                <p>OEE</p>
                <div class="face {{request.oeeFace}}"></div>
                <p>{{request.oee}} %</p>
                <p>{{request.oeeComment}}</p>
            </div>
        </div>
        <div class="col-lg-4 col-md-6" id="goalBlock">
            <div>
                <p>Pamainos tikslas</p>
                <p class="weightGoal">{{request.theoricalWeightInShift}} t</p>
                <div class="shiftHour" v-for="hour in request.shiftHoursCount">
                    <label>{{hour.hourNr}}</label>
                    <div class="{{hour.status}} hourRectangle">
                        <div class="factWeight" v-if="hour.factWeight">{{hour.factWeight}} t</div>
                    </div>
                    <br class="clear" />
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
var vm = new Vue({
    el: '#workCenter',
    data: {
        request: {},
    },
    methods: {
        loadData: function(){
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/'.strtolower(Configure::read('companyTitle')).'/vici_work_centers/update_status/'.$sensorId,true); ?>",
                type: 'post',
                context: this,
                dataType: 'JSON',
                async: true,
                success: function(request){
                    this.request = request;
                }
            })
        }
    },
    ready: function(){
         
    },
    beforeCompile: function(){ 
        this.loadData();
        setInterval(this.loadData,120000);
    },
    components: {
        
    },
    computed: {
        intervalWidth: function () {
          return Math.random();
        }
    },
    filters: {
        getIntervalWidth: function(interval){
            return (interval.end - interval.start) / 3600 * 100 + 0.1;
        },
        getLeftPos: function(interval){
            return interval.start / 3600 * 100;
        }
    }
});
        
</script>