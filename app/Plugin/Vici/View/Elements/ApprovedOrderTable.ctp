<?php
	if (!isset($boldLabels)) $boldLabels = false;
	if (!function_exists('echoLabel')) {
		function echoLabel($label, $boldLabels = false) {
			if ($boldLabels) {
				echo '<strong>'.$label.'</strong>';
			} else {
				echo $label;
			}
		}
	}
?>
<table class="table table-bordered table-condensed">
	<tbody>
		<tr><td><?php echo __('Padalinys'); ?>: <?php echo Branch::buildName($item); ?></td></tr>
		<tr><td><?php echo __('Darbo centras'); ?>: <?php echo Sensor::buildName($item, false); ?></td></tr>
		<tr>
			<td>
				<?php echoLabel(__('Planas'), $boldLabels); ?>:<br />
				<?php echoLabel(__('Gaminys'), $boldLabels); ?>: <?php echo Plan::buildName($item); ?><br />
				<?php echoLabel(__('Gaminio kodas'), $boldLabels); ?>: <?php echo $item[Plan::NAME]['production_code']; ?><br />
				<?php echoLabel(__('MO Numeris'), $boldLabels); ?>: <?php echo $item[Plan::NAME]['mo_number']; ?><br />
<!--				--><?php //echoLabel(__('Data ir laikas'), $boldLabels); ?><!--: --><?php
//					$pds = new DateTime($item[Plan::NAME]['start']);
//					$pde = new DateTime($item[Plan::NAME]['end']);
//					echo $pds->format('Y-m-d H:i').' &mdash; '.$pde->format('Y-m-d H:i');
//				?><!--<br />-->
<!--				--><?php //echoLabel(__('Trukmė'), $boldLabels); ?><!--: --><?php //echo number_format(($pde->getTimestamp() - $pds->getTimestamp()) / 3600, 1, ',', ''); ?><!-- --><?php //echo __('val.'); ?><!--<br />-->
<!--				--><?php //echoLabel(__('Kiekis'), $boldLabels); ?><!--: --><?php //echo $item[Plan::NAME]['quantity']; ?>
<!--                --><?php //if($item['Sensor']['type']==1) {
//                    $tesl_kiekis = 0;
//                    if($item['Plan']['qty_1']!=0) {
//                        $tesl_kiekis=round($item['Plan']['quantity']*$item['Plan']['yield']/$item['Plan']['qty_1'],2);
//                    }
//                ?>
<!--                <br/>-->
<!--                    --><?php // echo "<b>Tešlinukų kiekis</b>: ".$tesl_kiekis; ?>
<!--                --><?php //}?>
			</td>
		</tr>
		<tr>
			<td>
				<?php echoLabel(__('Faktinė gamyba'), $boldLabels); ?>:<br />
				<?php echoLabel(__('Data ir laikas'), $boldLabels); ?>: <?php
					$ds = new DateTime($item[ApprovedOrder::NAME]['created']);
					$de = new DateTime($item[ApprovedOrder::NAME]['end']);
					echo $ds->format('Y-m-d H:i').' &mdash; '.$de->format('Y-m-d H:i');
				?><br />
				<?php echoLabel(__('Trukmė'), $boldLabels); ?>: <?php echo number_format(($de->getTimestamp() - $ds->getTimestamp()) / 3600, 1, ',', ''); ?> <?php echo __('val.'); ?><br />
				<?php if (isset($lossItems) && !empty($lossItems)): ?>
				<?php echoLabel(__('Nuostoliai'), $boldLabels); ?>:<br />
					<?php foreach ($lossItems as $li): ?>
				<b><?php echo $li['Loss']['created_at']; ?></b> | <?php echo $li[LossType::NAME]['name']; ?>: <?php echo round($li[Loss::NAME]['value']).' '.$li[LossType::NAME]['unit']; ?><br />
					<?php endforeach; ?>
				<?php endif; ?>
                <?php if (isset($partialQuantities) && !empty($partialQuantities)): ?>
				<?php echoLabel(__('Deklaruoti kiekiai'), $boldLabels); ?>:<br />
					<?php foreach ($partialQuantities as $li): ?>
				<b><?php echo $li['PartialQuantity']['created']; ?></b> | <?php echo $li['PartialQuantity']['quantity']; ?><br />
					<?php endforeach; ?>
				<?php endif; ?>
			</td>
		</tr>
        <?php if(isset($relatedPostponedOrders) && !empty($relatedPostponedOrders)) { ?>
        <tr>
            <td>
                <?php echoLabel(__('Atidėtas kiekis'), $boldLabels); ?>:<br />
                <?php foreach($relatedPostponedOrders as $ao):?>
                    <strong><?php echo "&nbsp;".$ao['ApprovedOrder']['end']; ?></strong><br>
                    <div style="margin-left:10px;font-size:12px;margin-bottom:10px;">
                    <?php echoLabel(__('Kiekis (kg)'), $boldLabels); ?>: <?php echo $unitWeight>0?bcdiv($ao[ApprovedOrder::NAME]['quantity'],$unitWeight,2):__('Nepaduotas svoris (%s) vnt.', $ao[ApprovedOrder::NAME]['quantity']); ?><br />
                    <?php //echoLabel(__('Dėžių kiekis (vnt)'), $boldLabels); ?><?php
                    //$pqt = ($ao[ApprovedOrder::NAME]['quantity'] - ($ao[ApprovedOrder::NAME]['box_quantity'] * $ao[Plan::NAME]['box_quantity']));
                    //echo $ao[ApprovedOrder::NAME]['box_quantity'].(($pqt > 0) ? (' '.__('ir gaminių').' '.$pqt.' vnt') : '');
                    ?>
                    </div>
                <?php endforeach; ?>
            </td>
        </tr>
        <?php } ?>
        <?php if($item[ApprovedOrder::NAME]['exported_quantity'] != 0) { ?>
        <tr>
            <td>
                <?php echoLabel(__('Eksportuotas kiekis'), $boldLabels); ?>:<br />
                <div style="margin-left:10px;font-size:12px;margin-bottom:10px;">
                    <?php echoLabel(__('Kiekis').' (vnt)', $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['exported_quantity']; ?><br />
                    <?php echoLabel(__('Dėžių kiekis').' (vnt)', $boldLabels); ?>:
                    <?php
                    $exported_boxes = floor($item[ApprovedOrder::NAME]['exported_quantity']/$item[Plan::NAME]['box_quantity']);
                    $pqt = ($item[ApprovedOrder::NAME]['exported_quantity'] - ($exported_boxes * $item[Plan::NAME]['box_quantity']));
                    echo $exported_boxes.(($pqt > 0) ? (' '.__('ir gaminių').' '.$pqt.' vnt') : '');
                    ?>
                </div>
            </td>
        </tr>
        <?php } ?>
		<tr>
			<td>
				<?php echoLabel(__('Kiekis (kg)'), $boldLabels); ?>: 
				<?php 
    				$count = $item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity'];
    				echo $unitWeight>0?bcdiv($count,$unitWeight,2):__('Nepaduotas svoris (%s) vnt.', $count);
    		    ?><br />
			</td>
		</tr>
		<?php if (isset($url) && $url): ?>
		<tr><td>&nbsp;</td></tr>
		<tr><td><a href="<?php echo htmlspecialchars($url); ?>"><?php echo $url; ?></a></td></tr>
		<?php endif; ?>
	</tbody>
</table>
