<?php
App::uses('FoundProblem', 'Model');
App::uses('Problem', 'Model');
class ViciWorkCenter extends AppModel {
    
    public $name = 'ViciWorkCenter';
    public $useTable = 'vici_shifts_goals';
    
     public function generateShiftHours($fShift, $factWeightInShift){
        $hoursCount = ceil((strtotime($fShift['Shift']['end']) - strtotime($fShift['Shift']['start'])) / 3600);
        $start = strtotime($fShift['Shift']['start']);
        $end = strtotime($fShift['Shift']['end'])+1;
        $hoursList = array();
        while($end > $start){
            $hourStart = $end-3600;
            if(time() < $hourStart){
                $status = 'future';
            }elseif(time() >= $hourStart && time() <= $end){
                $status = 'current';
            }else{
                $status = 'past';
            }
            $hourData = array('status'=>$status,'hourNr'=>$hoursCount--);
            if($status == 'current'){
                $hourData['factWeight'] = bcdiv($factWeightInShift[0]['fact_weight'],1000,2);
            }
            $hoursList[] = $hourData;
            $end -= 3600;
        }
        return $hoursList;
    }

    public function getTheoricalWeightInShift($activeOrder, $nextPlans, $fSensor, $fShift){
        $goalExist = $this->find('first', array('conditions'=>array('ViciWorkCenter.shift_id'=>$fShift['Shift']['id'], 'ViciWorkCenter.sensor_id'=>$fSensor['Sensor']['id'])));
        if($goalExist){ return $goalExist['ViciWorkCenter']['goal']; }
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $shiftDuration = strtotime($fShift['Shift']['end']) - strtotime($fShift['Shift']['start']); 
        $plans = array($activeOrder) + $nextPlans;
        $totalTheoricalDuration = 0;
        $plansInGoal = array();
        $shiftIFull = false;
        foreach($plans as $plan){
            $plan['Plan']['preparation_time'] = $plan['Plan']['preparation_time'] > 0?$plan['Plan']['preparation_time']*60:300;
            if($plan['Plan']['step_kg'] > 0){
                if(isset($plan['ApprovedOrder']['id']) && $plan['ApprovedOrder']['id'] > 0){//jei yra siuo metu vykdomas uzsakymas, reikia priskaiciuoti jo derinimo ir kiekio likuti
                    $activeProblem = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS, 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id'])));
                    if(!empty($activeProblem) && $activeProblem['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL){//jei siuo metu vyksta derinimas, reikia priskaiciuoti derinimo likuti
                        $changeoverDurationsInPrevShifts = $foundProblemModel->find('first', array(
                            'fields'=>array('TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end) AS changeover_duration'),
                            'conditions'=>array('FoundProblem.problem_id'=>Problem::ID_NEUTRAL, 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id'], 'FoundProblem.shift_id <>'=>$fShift['Shift']['id'])
                        ));
                        if(!empty($changeoverDurationsInPrevShifts)){
                            $plan['Plan']['preparation_time'] = max(0, $plan['Plan']['preparation_time'] - $changeoverDurationsInPrevShifts[0]['changeover_duration']);
                        }
                    }
                    $quantityType = $fSensor['Sensor']['plan_relate_quantity']=='quantity'?'box_quantity':'quantity';
                    $unitWeight = bcdiv($plan['Plan']['step_kg'], $plan['Plan']['step'], 4);
                    $plan['Plan']['theorical_duration'] = max(0,bcmul(bcdiv(($plan['Plan']['quantity'] - bcmul($plan['ApprovedOrder'][$quantityType],$unitWeight,4)), $plan['Plan']['step_kg'],4), 3600));
                }else{
                    $plan['Plan']['theorical_duration'] = bcmul(bcdiv($plan['Plan']['quantity'], $plan['Plan']['step_kg'],4), 3600);
                }
            }else{
                $plan['Plan']['theorical_duration'] = 0;
            }
            $totalTheoricalDuration = bcadd($totalTheoricalDuration, $plan['Plan']['preparation_time'], 4);
            if($totalTheoricalDuration > $shiftDuration){ break; } //Jei pridejus plano derinima jau gauname daugiau laiko nei pamaina, sio plano nebetraukiame
            $totalTheoricalDuration = bcadd($totalTheoricalDuration, $plan['Plan']['theorical_duration'], 4);
            if($totalTheoricalDuration > $shiftDuration){ //jei pridejus sio plano numatyta laika gauname daugiau nei pamaina, atskaiciuosiume pervisi ir pridesime tik likuti, kad tilptume i pamaina
                $surplusTime = bcsub($totalTheoricalDuration, $shiftDuration, 4);
                $plan['Plan']['theorical_duration'] = bcsub($plan['Plan']['theorical_duration'], $surplusTime, 4);
                $shiftIFull = true;
            }
            $plansInGoal[] = $plan['Plan'];
            if($shiftIFull){ break; }
        }
        $goalWeight = 0;
        foreach($plansInGoal as $plan){
            $goalWeight = bcadd($goalWeight, bcmul(bcdiv($plan['theorical_duration'],3600,4), $plan['step_kg'], 4));
        }
        $goalWeight = bcdiv($goalWeight,1000,2);
        $this->save(array('shift_id'=>$fShift['Shift']['id'], 'sensor_id'=>$fSensor['Sensor']['id'], 'goal'=>$goalWeight));
        return $goalWeight;
    }
    
}
