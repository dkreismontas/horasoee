<?php
class Vici extends AppModel{
    public $name = 'Vici';
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     
     public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, $fShift,&$fTransitionProblemTypes, &$allPlans){
        if(!empty($currentPlan)){
            $currentPlan['Plan']['step_vnt_min'] = bcdiv($currentPlan['Plan']['step'],60,2);
        } 
        foreach($plans as &$plan){
            $plan['Plan']['step_vnt_min'] = bcdiv($plan['Plan']['step'],60,2); 
        }
        foreach($allPlans as &$allPlan){
            //if($fSensor['Sensor']['id'] == 7){
                $allPlan['Plan']['name'] = $allPlan['Plan']['production_code'].' | '.$allPlan['Plan']['so_number'];
            // }else{
                // $allPlan['Plan']['name'] = $allPlan['Plan']['production_name'].' | '.$allPlan['Plan']['so_number'];
            // }
        }
    }
     
    public function setTimeGraphHoverProduct(&$obj){
        static $ApprovedOrder = null;
        if($ApprovedOrder == null){ $ApprovedOrder = ClassRegistry::init('ApprovedOrder'); }
        $obj->title .= '<br/><b>'.__('Gaminys').'</b>: '.$obj->data[Plan::NAME]['production_code'].' ('.$obj->data[Plan::NAME]['production_name'].')';
        $obj->title .= '<br/><b>'.__('Užsakymo nr.').'</b>: '.$obj->data[Plan::NAME]['mo_number'];
        $unitWeight = $obj->data[Plan::NAME]['step_kg'] > 0?bcdiv($obj->data[Plan::NAME]['step'], $obj->data[Plan::NAME]['step_kg'],6):1;
        if(isset($obj->data[ApprovedOrder::NAME])) {
            $relatedCount = bcdiv($ApprovedOrder->getRelatedApprovedOrders($obj->data[Plan::NAME]['id'], $obj->data[ApprovedOrder::NAME]['id']),$unitWeight,2);             
            $madeCount = bcdiv(($obj->data[ApprovedOrder::NAME]['quantity'] + $relatedCount),$unitWeight,2);
            $obj->title .= '<br/><b>' . __('Pagaminta').':</b> '.$madeCount.' kg.'.($obj->data[Plan::NAME]['quantity']>0?' '.__('iš').' '.$obj->data[Plan::NAME]['quantity']:'').' kg' ;
            $obj->title .= '<br/><b>' . __('Intervalo kiekis').':</b> '. bcdiv($obj->intervalQuantity,$unitWeight,2).' kg';
            if($obj->data[Plan::NAME]['quantity'] > 0){
                $obj->title .= '<br/><b>' . __('Likęs kiekis').':</b> '.max(0, $obj->data[Plan::NAME]['quantity'] - $madeCount). 'kg';
            }
            if($relatedCount > 0){
                $obj->title .= '<br/><b>' . __('Nuo paskutinio atidėjimo pagaminta').':</b> '.$relatedCount.' kg';
            }
            if(isset($obj->data['Work']) && !empty($obj->data['Work']) && $obj->data['Work']['problem_id'] == -1){
                $obj->title .= '<br/><b>' . __('Komentaras').':</b> '.$obj->data['Work']['comments'];
            }
        }
    }

    public function ApprovedOrdersIndexHook(&$controller){
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/index');
    }
    
    public function ApprovedOrdersEditHook(&$controller){
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/edit');
    }

    public function DashboardsGetMoStatsHook(&$controller){
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Dashboards/get_mo_stats');
    }
    
    public function SlidesAfterEditHook(&$viewVariables){
        $viewVariables['viewTypes'][99554] = __('Vici darbo centras');
    }
    
    public function SlidesAfterShowHook(&$viewVariables){
        $links = explode(',',$viewVariables['slidesCollection']['SlidesCollection']['links']);
        $recreatedLinks = array();
        foreach($viewVariables['slidesCollection']['SlidesCollection']['slides_json'] as $slideData){
            if($slideData->type == 99554){
                $recreatedLinks[] = '\''.Router::url(array('plugin'=>'vici', 'controller'=>'vici_work_centers', 'action'=>'index', current($slideData->sensors)),true).'\'';
            }else{
                $recreatedLinks[] = current($links);
                next($links);
            }
        }
        $viewVariables['slidesCollection']['SlidesCollection']['links'] = implode(',',$recreatedLinks);
    }
    
    public function App_beforeBeforeFilter_Hook(&$appController){
        if(in_array($_SERVER['REMOTE_ADDR'], Configure::read('ServerIP'))){
            $appController->Auth->allow(array(
                'index','update_status', 'buildTimeGraph'
            ));
        }
    }

    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$hPeriods){
        foreach($hPeriods as &$period){
            if(isset($period->data['FoundProblem']) && $period->data['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL){
                $period->title = preg_replace_callback('/(<b>Derinimo norma<\/b>:\s*[^>]+>)/', function($match){
                    return '';
                }, $period->title);
            }
        }
    }
}
    