<?php
App::uses('ApprovedOrder', 'Model');
class ViciWorkCentersController extends ViciAppController{ 
    
    public $uses = array('Sensor','Record','ApprovedOrder','Plan','Shift','Vici.ViciWorkCenter','WorkCenter');
    public $components = array('Help');
    
    // public function beforeFilter(){
        // if(in_array($_SERVER['REMOTE_ADDR'], array('195.14.180.130'))){
            // $this->Auth->allow(array(
                // 'index','update_status'
            // ));
        // }
        // parent::beforeFilter();
    // }
    
    public function index($sensorId){
        $this->layout = 'bare';
        $fSensor = $this->Sensor->findById($sensorId);
        $this->set(compact('sensorId','fSensor'));
    }
    
    public function update_status($sensorId){
        $this->layout = 'empty';
        $fSensor = $this->Sensor->findById($sensorId);
        $fShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        Configure::write('graphHoursDefaultLinesCount',2);
        $data = ob_start('ob_gzhandler');
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $activeOrder = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.status_id'=>1, 'ApprovedOrder.sensor_id'=>$sensorId)));
        $theoricalSpeed = !empty($activeOrder)?bcdiv($activeOrder['Plan']['step'],60,4):0;
        $lastMinuteSpeed = round($this->Record->getLastMinuteSpeed($fSensor),2);
        $speedPercentage = $theoricalSpeed>0?bcdiv($lastMinuteSpeed,$theoricalSpeed,2) * 100:0;
        if($speedPercentage < 70){
            $arrow = 1;
        }elseif($speedPercentage < 90){
            $arrow = 2;
        }elseif($speedPercentage < 95){
            $arrow = 3;
        }else{
            $arrow = 4;
        }
        $condForAppOrd = array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE));
        $planSearchParams = array(
            'conditions' => array(
               'Plan.sensor_id' => $fSensor[Sensor::NAME]['id'],
               'Plan.has_problem'=>0,
               'Plan.disabled'=>0,
               'ApprovedOrder.id' => null,
               'Plan.id <>'=>!empty($activeOrder)?$activeOrder['Plan']['id']:0
            ),
            'order' => array('IFNULL(ApprovedOrder.status_id, 999) ASC', 'Plan.position ASC', 'Plan.end ASC'),
            'group' => 'Plan.id',
            'limit' => 100
        );
        $nextPlans = $this->Plan->withRefs(true, $condForAppOrd)->find('all', $planSearchParams);
        $oee = $this->Record->calcOee2($fShift, $fSensor);
        if($oee < 70){
            $oeeComment = 'Blogai!';
            $oeeFace = 'bad';
        }elseif($oee < 89){
            $oeeComment = 'Prastai!';
            $oeeFace = 'good';
        }else{
            $oeeComment = 'Puiku!';
            $oeeFace = 'perfect';
        }
        $this->Record->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $factWeightInShift = $this->Record->find('first', array(
            'fields'=>array('SUM(Plan.step_kg / Plan.step * IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity,Record.unit_quantity)) AS fact_weight'),
            'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'])
        ));
        $data = array(
            //'periods'=>$this->requestAction(array('plugin'=>false,'controller'=>'WorkCenter','action'=>'buildTimeGraph'), array('pass'=>compact('fSensor'))),
            'periods'=>$this->WorkCenter->buildTimeGraph($fSensor),
            'lastMinuteSpeed'=>$lastMinuteSpeed,
            'arrow'=>$arrow,
            'nextPlans'=>array_slice($nextPlans, 0, 2),
            'activeOrder'=>$activeOrder,
            'oee'=>$oee,
            'oeeComment'=>$oeeComment,
            'oeeFace'=>$oeeFace,
            'shiftHoursCount'=>$this->ViciWorkCenter->generateShiftHours($fShift, $factWeightInShift),
            'theoricalWeightInShift'=>$this->ViciWorkCenter->getTheoricalWeightInShift($activeOrder, $nextPlans, $fSensor, $fShift)
        );
        echo json_encode($data);
        ob_end_flush();
        die();
    }
    
}
