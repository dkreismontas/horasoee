<?php

App::uses('Plan', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('Loss', 'Model');
App::uses('LossType', 'Model');
App::uses('Branch', 'Model');

class ExporterComponent extends Component {

    /**
     * Tada kai pamainos vadovas patvirtina deziu kiekius
     * @param array $ord
     */
    public static function exportCompleted($ord,$exportPath,$type='csv') {
        CakeLog::write('debug','overriding exportCompleted');
        $date = new DateTime();
        $extension = '.txt';
        if($type=='xml') {
            $extension = '.xml';
        }
        $filePath = $exportPath.DS.'MORPT_'.$date->format('Ymd').'_'.$date->format('His').$ord[Plan::NAME]['mo_number'].$extension;
        if($ord[Plan::NAME]['production_code'] == 39000) {
            $ord[ApprovedOrder::NAME]['confirmed_quantity'] = $ord[ApprovedOrder::NAME]['confirmed_quantity'] * 0.5;
        }
        $production_code_value = $ord[Plan::NAME]['production_code'];
        $confirmed_quantity_value = $ord[ApprovedOrder::NAME]['confirmed_quantity'];
        if($ord['Sensor']['type'] == 1) {
            $production_code_value = $ord[Plan::NAME]['paste_code'];
            $confirmed_quantity_value *= $ord[Plan::NAME]['yield'];
            $confirmed_quantity_value = round($confirmed_quantity_value,2);
        }
        if($type) {
            $data = ''
                . $ord[Branch::NAME]['ps_id'] . ';'
                . $production_code_value . ';'
                . $ord[Plan::NAME]['mo_number'] . ';'
                . (isset($ord[Plan::NAME]['report_no'])?$ord[Plan::NAME]['report_no'].';':'')
                . round($confirmed_quantity_value,2) . ';'
                . number_format(((strtotime($ord[ApprovedOrder::NAME]['end']) - strtotime($ord[ApprovedOrder::NAME]['created'])) / 3600), 2, '.', '') . ';'
                . $date->format('Ymd') . ';'
                . '1' . "\r\n";
        }
        if($type=='xml') {
            $data = '<?xml version="1.0" encoding="UTF-8"?>
    <dataroot xmlns:od="urn:schemas-microsoft-com:officedata" generated="'.$date->format("Y-m-d\TH:i:s").'">
        <ExportDough>
            <Item>'.$production_code_value.'</Item>
            <Amount>'.round($confirmed_quantity_value,2).'</Amount>
            <Prod_unit>'.$ord[Branch::NAME]['ps_id'].'</Prod_unit>
            <Reporting_Date>'.$date->format('Ymd').'</Reporting_Date>
            <Reporting_Time>'.$date->format('His').'</Reporting_Time>
        </ExportDough>
    </dataroot>';
        }

        file_put_contents($filePath, $data);
    }

}
