<?php
App::uses('JususprendimasAppController', 'Jususprendimas.Controller');

class PluginsController extends JususprendimasAppController {
    public $uses = array('Shift', 'FoundProblem', 'Problem');
    CONST PROPABILITY = 30; //santykis, su kuriuo 1/propability gamyba virs i problema ir atvirksciai
    
    function save_sensors_data(){
        $sensorModel = ClassRegistry::init('Sensor');
        $recordModel = ClassRegistry::init('Record');
        $recordModel->deleteAll(array('Record.created <'=>date('Y-m-d H:i', strtotime('-2 WEEKS'))));
        $sensorsListInDB = $sensorModel->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        foreach($sensorsListInDB as $sensorId => $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId);
            if(!$currentShift){             
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
            //vykdome iraso iterpima i DB
            $lastRecord = $recordModel->find('first', array('conditions'=>array('Record.sensor_id'=>$sensorId), 'order'=>array('Record.id DESC')));
            if(!$lastRecord){
                $lastQuantity = rand(5, 10);
                $lastDate = time()-1;
            }else{
                $opositeNr = $lastRecord['Record']['quantity'] > 0?0:rand(5, 10);
                //$quantityChooser = array_merge(array_fill(0, 20, $lastRecord['Record']['quantity']), array($opositeNr));
                $lastQuantity = $lastRecord['Record']['quantity'];
                $lastDate = strtotime($lastRecord['Record']['created']);
            }           
            while($lastDate < time()){
                $lastDate += Configure::read('recordsCycle');
                $lastQuantity = $this->getQuantity($lastQuantity);
                $db->query('CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $lastDate).'\', '.$sensorId.', '.$lastQuantity.'); '); 
            }
            $this->addTransition($sensorId);
            $this->attachProblems($sensorId);
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        die();
    }

    private function getQuantity($lastQuantity){
        $opositeNr = $lastQuantity > 0?0:rand(5, 10);
        $quantityChooser = array_merge(array_fill(0, self::PROPABILITY, $lastQuantity), array($opositeNr)); //proc tikimybe kiekio pasikeitimui is 0 į >0
        $quantity = $quantityChooser[array_rand($quantityChooser, 1)];
        return $quantity;
    }
    
    private function addTransition($sensorId){
        $lastTransition = $this->FoundProblem->find('first', array(
            'conditions'=>array('FoundProblem.sensor_id'=>$sensorId, 'FoundProblem.problem_id'=>1, 'FoundProblem.start >'=>date('Y-m-d H:i:s', strtotime('-4 hour'))),
            'order'=>array('FoundProblem.start DESC')
        ));
        if(!$lastTransition){
            $time = date('Y-m-d H:i:s', strtotime('-5 hour'));
        }else{
            $time = $lastTransition['FoundProblem']['end'];
        }
        if(time()-strtotime($time) > 3600*4){
            $time = date('Y-m-d H:i:s',strtotime('+2 hour',strtotime($time)));
            $longestTime = $this->FoundProblem->find('first', array(
                'fields'=>array('MAX(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) AS max_diff'),
                'conditions'=>array('FoundProblem.sensor_id'=>$sensorId, 'FoundProblem.start >'=>$time)
            ));
            $this->FoundProblem->updateAll(array('problem_id'=>1, 'while_working'=>0), 
                array('FoundProblem.sensor_id'=>$sensorId, 'FoundProblem.start >'=>$time, 'TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)'=>$longestTime[0]['max_diff'])
            );
        }
    }

    private function attachProblems($sensorId){
        $problemsList = $this->Problem->find('list',array('fields'=>array('Problem.id'), 'conditions'=>array('Problem.id >'=>3)));
        if(empty($problemsList)) return;
        $lastFoundProblemSet = $this->FoundProblem->find('first', array(
            'conditions'=>array('FoundProblem.sensor_id'=>$sensorId, 'FoundProblem.problem_id >'=>3),
            'order'=>array('FoundProblem.start DESC')
        ));
        if($lastFoundProblemSet){
            if(time() - strtotime($lastFoundProblemSet['FoundProblem']['start']) < 3600) return; //jei paskutinei priskirtai problemai nera daugiau kaip valanda, nevykdom priskyrimo, kad einamosios problemos butu nepazymetos 1val
            $lastSetedId = $lastFoundProblemSet['FoundProblem']['id'];
        }else $lastSetedId = 0;
        $this->FoundProblem->updateAll(array('FoundProblem.problem_id'=>'IF(RAND() <= 0.7, ELT(1 + FLOOR(RAND()*'.sizeof($problemsList).'),\''.implode('\',\'', $problemsList).'\'), FoundProblem.problem_id)'),
            array('FoundProblem.problem_id'=>3, 'FoundProblem.id >'=>$lastSetedId, 'FoundProblem.sensor_id'=>$sensorId)
        ); 
    }
    
}
