<?php
class UtenosTrikotazas extends AppModel{
     
    public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
    }
    
    public function recalcOeeHook(&$val=array()){
         $val['operational_factor'] = 1;
     }
     
     public function recalcGetFactTimeHook(&$record, $shift_length){
        //perskaiciuojame prieinamumo koef.
        $record['operational_factor'] = 1;
    }

    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$periods){
        foreach($periods as &$period){
            if(isset($period->data['Work']['problem_id']) && $period->data['Work']['problem_id'] == -1 && in_array($period->data['Sensor']['id'],array(3,4))){
                $period->title = str_replace(__('vnt'),'m', $period->title);
            }
        }
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        if(!empty($currentPlan) && in_array($fSensor['Sensor']['id'], array(3,4))){
            $currentPlan['Plan']['step_unit_title'] = 'm/min';
        }
    }
      
}
    