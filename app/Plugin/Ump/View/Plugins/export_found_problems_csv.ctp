<?php
$previous_date = null;
foreach ($foundProblems as $fp) {
    $csvLine = array();
    try{
        $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
    }catch(Exeption $e){
        $approvedAddData = null;
    }
    $fp['Sensor']['name'] = $fp['Sensor']['name'].' '.(isset($fp['Factory']['name'])?$fp['Factory']['name']:'');
    $csvLine[] = $fp['Sensor']['name'];
    $csvLine[] = isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number'];
    $csvLine[] = $fp['Plan']['production_name'];
    $csvLine[] = $fp['Plan']['production_code'];
    $csvLine[] = $fp['DiffCard']['description'];
    $parsedProblemsTree = $fp['Problem']['id'] > 0 && isset($problemsTreeList[$fp['Problem']['id']])?array_reverse($problemsTreeList[$fp['Problem']['id']]):array();
    for($i=0; $i <= $maxDowntimeLevel; $i++){
        if(isset($parsedProblemsTree[$i])){
            $csvLine[] = Settings::translate($parsedProblemsTree[$i]);
        }else{
            $csvLine[] = Settings::translate($fp['Problem']['name']);
        }
    }
    $csvLine[] = $fp['FoundProblem']['start'];
    $csvLine[] = $fp['FoundProblem']['end'];
    $csvLine[] = round($fp[0]['found_problem_duration'] / 60,2);
    if ($previous_date == null && strtotime($fp['FoundProblem']['start']) >= strtotime($shift['Shift']['start'])) {
        $previous_date = $shift['Shift']['start'];
    }
    if($fp['FoundProblem']['problem_id'] >= 3 && !in_array($fp['FoundProblem']['problem_id'], $downtimesExclusionsIds)) {
        if ($previous_date != null) {
            $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date);
            $date_diff = $date_diff > 0?$date_diff:0;
        }
        $previous_date = $fp['FoundProblem']['end'];
    }else{
        $date_diff = 0;
    }
    $csvLine[] = $date_diff > 0 ? round($date_diff / 60, 2) : "";
    $csvLine[] = $fp['FoundProblem']['comments'];
    $csvLine[] = $fp['Plan']['step'];
    $csvLine[] = getIntervalQuantity($fp);
    $csvLine[] = $fp['Plan']['box_quantity'];
    $csvLine[] = $fp['User']['unique_number'];
    $csvLine[] = round((strtotime($fp['Shift']['end'])-strtotime($fp['Shift']['start'])) / 3600);
    echo implode(';', $csvLine)."\n";
}
header("Content-type: text/plain; charset=utf-8");
header(sprintf("Content-Disposition: attachment; filename=Ivykiu_zurnalas_%s-%s.csv",date('Y-m-d',strtotime($shift['Shift']['start'])),date('Y-m-d',strtotime($shift['Shift']['end']))));
die();

function getIntervalQuantity($data){
    if($data['Sensor']['plan_relate_quantity'] == 'quantity'){ return $data['FoundProblem']['quantity']; }
    $multiplier = 1;
    if(isset($data['ApprovedOrder']) && !empty($data['ApprovedOrder']) && $data['ApprovedOrder']['box_quantity'] > 0){
        $multiplier = bcdiv($data['ApprovedOrder']['quantity'],$data['ApprovedOrder']['box_quantity'],6);
    }
    return bcmul($data['FoundProblem']['quantity'],$multiplier,2);
}