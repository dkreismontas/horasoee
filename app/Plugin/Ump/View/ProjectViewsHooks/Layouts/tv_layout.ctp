<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <?php echo $this->Html->charset(); ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title><?php echo $appName.($title_for_layout ? (' - '.$title_for_layout) : ''); ?></title>

    <?php
    echo $this->Html->css(array('style.default', 'responsive-tables'));
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
    echo $this->fetch('css');
    echo $this->fetch('script');
    echo $this->Html->css('Tv.style2.css?v=6');
    echo $this->Html->script('Tv.vue');
    echo $this->Html->script('Tv.loader');
    echo $this->Html->script('Tv.gauge.min');
    ?>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-migrate-1.2.1.min.js"></script>
    <style type="text/css">
        .sensor.super_parent_problem_id_21,
        .sensor.super_parent_problem_id_25{
            border: 10px solid #ff0000;
            padding: 0;
        }
    </style>
</head>
<body<?php if (isset($bodyClass) && $bodyClass) { echo ' class="'.htmlspecialchars($bodyClass).'"'; } ?>>
<?php echo $this->fetch('content'); ?>
</body>
</html>
