<?php
class Ump extends AppModel{

    public $name = __CLASS__;
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     public function changeUpdateStatusVariablesHook(&$fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        if(!empty($currentPlan)){
            $currentPlan['Plan']['step_unit_title'] = 'm/min';
        }
    }
     
    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$hPeriods){
        foreach($hPeriods as &$period){
            if(isset($period->data['Work']['quantity'])){
                //PAZYMETO INTERVALO
                $durationInMin = bcdiv(strtotime($period->dtEnd->format('Y-m-d H:i:s')) - strtotime($period->dtStart->format('Y-m-d H:i:s')),60,4);
                $avgSpeedInCurrentInterval = $durationInMin > 0?bcdiv($period->intervalQuantity, $durationInMin,2):0;
                $period->title .= '<br/><b>'.__('Vid. pažymėto intervalo greitis').'</b>: '.$avgSpeedInCurrentInterval.' '.__('m/min');
                //VISO INTERVALO
                $multiplier = $period->data['Sensor']['plan_relate_quantity'] == 'quantity'?1:$period->data['Plan']['box_quantity'];
                $durationInMin = bcdiv(strtotime($period->data['Work']['end']) - strtotime($period->data['Work']['start']),60,4);
                $avgSpeedInAllInterval = bcdiv(bcmul($period->data['Work']['quantity'], $multiplier,4),$durationInMin,2);
                $period->title .= '<br/><b>'.__('Vid. viso intervalo greitis').'</b>: '.$avgSpeedInAllInterval.' '.__('m/min');
            }
        }
    }

    public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
        $settingsModel = ClassRegistry::init('Settings');
        $testDowntimeId = $settingsModel->getOne('test_production_downtime_id');
        if(!$testDowntimeId){ return null; }
        $planModel = ClassRegistry::init('Plan');
        $planModel->bindModel(array('belongsTo'=>array('Product')));
        $plan = $planModel->findById($appOrder['ApprovedOrder']['plan_id']);
        $planName = isset($plan['Product']['name']) && trim($plan['Product']['name'])?$plan['Product']['name']:$plan['Plan']['production_name'];
        if(!preg_match('/^test.+/i',trim($planName))){ return null; }
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $logModel = ClassRegistry::init('Log');
        $shiftModel = ClassRegistry::init('Shift');
        $recordModel = ClassRegistry::init('Record');
        $sensorModel = ClassRegistry::init('Sensor');
        $foundProblemModel->deleteAll(array('FoundProblem.end >'=>$appOrder['ApprovedOrder']['created'], 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
        $fShift = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $start = new DateTime($appOrder['ApprovedOrder']['created']);
        $quantity = $recordModel->find('first', array('fields'=>array('SUM(Record.quantity) AS quantity_sum'), 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$start->format('Y-m-d H:i:s'))));
        $currentProblem = $foundProblemModel->newItem($start, new DateTime(),
            $testDowntimeId, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $plan[Plan::NAME]['id'], array(), $quantity[0]['quantity_sum']);
        $recordModel->updateAll(
            array('Record.found_problem_id'=>$currentProblem['FoundProblem']['id']),
            array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$currentProblem['FoundProblem']['start'])
        );
        $sensorModel->updateAll(
            array('Sensor.tmp_prod_starts_on'=>$fSensor['Sensor']['prod_starts_on'], 'Sensor.prod_starts_on'=>9999),
            array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
        );
    }

    public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
        if($fSensor['Sensor']['prod_starts_on'] >= 9999){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(
                array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
                array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
            );
        }
    }

    public function TvPlugin_AfterIndex_Hook(&$layout){
        $layout = '../../../'.$this->name.'/View/ProjectViewsHooks/Layouts/tv_layout';
    }

    public function WorkCenter_BeforeUpdateStatus_Hook(&$fSensor, $user){
        if(isset($user['sensor_id']) && !trim($user['sensor_id'])){
            $shiftModel = ClassRegistry::init('Shift');
            $currentShift = $shiftModel->findCurrent($fSensor['Sensor']['branch_id']);
            if(!empty($currentShift)){
                $duration = time() - strtotime($currentShift['Shift']['start']);
                Configure::write('graphHoursDefaultLinesCount', (int)ceil($duration/3600));
            }
        }
    }
     
}
    