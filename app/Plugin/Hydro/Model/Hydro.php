<?php
class Hydro extends AppModel{

    private static $shiftSearchBy = 'end';

    public function Speed_GetSpeedHistoryBeforeSearch_Hook(&$searchParameters, &$bindings){
        $searchParameters['group'] = array('Record.sensor_id', 'Record.id');
        $searchParameters['fields'][6] = 'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i:%s\') AS groupper';
    }
    
    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $fSensor['Sensor']['speed_gauge_colors'] = array('#F03E3E','#30B32D','#FFDD00');//sukeiciame spalvas vietomis        
    }
    
    public function Tv_parametersCalculationAfterValuesSet_Hook(&$fSensor){
        $fSensor['sensor_settings']['speed_gauge_colors'] = array('#F03E3E','#30B32D','#FFDD00');//sukeiciame spalvas vietomis     
    }

    /*public function Oeeshell_beforeStartDurationCalculation_Hook(&$shiftsSearchParams, &$start, &$end){
        $shiftsSearchParams['conditions'] = array('Shift.'.self::$shiftSearchBy.' >=' => $start, 'Shift.'.self::$shiftSearchBy.' <' => $end, 'Shift.disabled'=>0);
    }

    public function ReportsContr_AfterCalculatePeriodXLS_Hook(&$calculation, &$shiftsExtremumsParams, &$requestData){
        $shiftsExtremumsParams['conditions'] = array('Shift.'.self::$shiftSearchBy.' >=' => $requestData['start_date'], 'Shift.'.self::$shiftSearchBy.' <' => $requestData['end_date']);
    }

    public function HelpComponent_beforeDatePatternFormation_Hook(&$shiftSearchBy){
        $shiftSearchBy = self::$shiftSearchBy;
    }*/
    
    
    
    
    // public function Tv_getCurrentStatusesBeforeSearch_Hook(&$searchParameters, &$bindings, &$sensorsIds){
        // $searchParameters = array(
            // 'fields'=>array(
                // 'MAX(Record.id) as last_id',
                // 'SUM(IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity)) AS last_minute_quantities',
                // 'Record.sensor_id'
            // ),'recursive'=>1,
            // 'conditions'=>array(
                // 'Record.created >'=>date('Y-m-d H:i:s', strtotime('-1 hour')), 
                // 'Record.sensor_id'=>$sensorsIds
            // ),'group'=>array('Record.sensor_id')
        // );
    // }
    
    //public function Tv_parametersCalculationAfterValuesSet_Hook(&$sensor){
        //$sensor['client_speed_vnt_min'] *= 60;
   // }
     
}
    