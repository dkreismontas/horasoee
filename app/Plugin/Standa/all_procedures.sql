DROP PROCEDURE IF EXISTS HOOK_AFTER_RECORD_INSERT;
DELIMITER $$

CREATE PROCEDURE `HOOK_AFTER_RECORD_INSERT`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `branch_id` INT(11), IN `quantity` DECIMAL(12,4), IN `shift_id` INT(11), IN `active_fproblem_id` INT(11), IN `active_order_id` INT(11), IN `active_plan_id` INT(11), IN `record_cycle` SMALLINT(6)) NO SQL
BEGIN
	DECLARE `shift_start` DATETIME DEFAULT NULL;
	DECLARE `prev_shift_id` INT(11) DEFAULT NULL;
	DECLARE `prev_shift_duration` INT(11) DEFAULT NULL;
	DECLARE `not_defined_downtimes_duration_in_prev_shift` INT(11) DEFAULT NULL;
	DECLARE `shift_is_disabled` SMALLINT(2) DEFAULT 0;
	#veiksmai, kai keiciasi pamaina (nuo pamainos pradzios nepraejo ilgesnis laikas kaip iraso ciklas)
	#Jei praejusios pamainos nepazymeta prastova bendra trukme yra daugiau kaip 90proc pamainos laiko, visas tas prastovas reikia paversti i nera darbo
	SELECT `sh`.`start` INTO `shift_start` FROM `shifts` AS `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	IF(UNIX_TIMESTAMP(`date_time`) - UNIX_TIMESTAMP(`shift_start`) <= `record_cycle`) THEN 
		SELECT `shifts`.`id`, TIMESTAMPDIFF(SECOND, `shifts`.`start`, `shifts`.`end`),`shifts`.`disabled` INTO `prev_shift_id`,`prev_shift_duration`,`shift_is_disabled` FROM `shifts` WHERE `shifts`.`start` < `shift_start`  AND `shifts`.`branch_id` = `branch_id` ORDER BY `shifts`.`id` DESC LIMIT 1;
		IF(`shift_is_disabled` = 1) THEN
            SELECT SUM(TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`)) INTO `not_defined_downtimes_duration_in_prev_shift` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`shift_id` = `prev_shift_id` AND `fp`.`problem_id` = 3 LIMIT 1;
            IF(`not_defined_downtimes_duration_in_prev_shift` / `prev_shift_duration` * 100 > 90) THEN
                UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = 2 WHERE `fp`.`shift_id` = `prev_shift_id` AND `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = 3;
                CALL `ADD_LOG`(`sensor_id`, CONCAT('Keiciantis pamainai pamainos id: ',  `shift_id`,' nepazymetos prastovos automatiskai verciamos i nera darbo, kadangi bendra ju suma viršyja 90% pamainos laiko. Prastovu suam lygi (s):', `not_defined_downtimes_duration_in_prev_shift`));
            END IF;
        END IF;
	END IF;
END$$

DELIMITER ;
