<?php
class Standa extends AppModel{
     
     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }
     
     // public function changeUpdateStatusVariablesHook($fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        // if(!empty($currentPlan)){
            // $recordModel = ClassRegistry::init('Record');
            // $quantity = $recordModel->find('first', array(
                // 'fields'=>array('SUM(Record.unit_quantity) AS total'), 
                // 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
            // ));
            // $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.','').' '.__('vnt'); 
        // }
     // }

    public function WorkCenter_index_Hook(&$data, &$fSensor){
         $data['logoutUrl'] = Router::url(array('controller' => 'users', 'action' => 'logout', (int)$fSensor['User']['sensor_id']>0?$fSensor['Sensor']['id']:''));
    }

    public function Users_beforeLogin_Hook(Object &$usersController, Int $sensorId=0){
        $ldapCredentials = array( //LDAP (linux) arba ACTIVE_DIRECTORY (windows) prisijungimas
            'domain'=>'ldap://centras.standa.lt',
            'admin_dn'=>'HorasLDAP@centras.standa.lt',
            'admin_password'=>'86v8JjKwqUDsmbTZdKL234QDFKgEz6nN',
            'base_dn'=>'OU=Standa_users,DC=centras,DC=standa,DC=lt'
        );
        $usersController->Auth->login();
        $user = $usersController->Auth->user();
        if(in_array($user['id'], array(1,2,57))){ return true; }
        $requestData = $usersController->request->data;
        if(!is_array($ldapCredentials) || empty($ldapCredentials)){ return null; }
        $ldapConn = ldap_connect($ldapCredentials['domain']);
        if (!$ldapConn){
            return array('errors'=>__('Nepavyko prisijungti prie LDAP serviso'));
        }
        //The first step is to bind the administrator so that we can search user info
        $ldapBindAdmin = ldap_bind($ldapConn, $ldapCredentials['admin_dn'], $ldapCredentials['admin_password']);
        if ($ldapBindAdmin){
            $filter = '(sAMAccountName='.$requestData['User']['username'].')';
            //$filter = '(&(sAMAccountName='.$username.')(memberOf=CN=118 HAAS SMM-4,OU=CNC,OU=Standa_users,DC=centras,DC=standa,DC=lt))';
            //$filter = '(CN=*)';
            $attributes = array("*");
            $result = ldap_search($ldapConn, $ldapCredentials['base_dn'], $filter, $attributes);
            $entries = ldap_get_entries($ldapConn, $result);
            if($entries['count'] == 0){
                return array('errors'=>__('Nepavyko aptikti vartotojo %s LDAP sistemoje', $requestData['User']['username']));
            }
            $userDN = $entries[0]["name"][0];
            //Okay, we're in! But now we need bind the user now that we have the user's DN
            $ldapBindUser = @ldap_bind($ldapConn, $userDN, $requestData['User']['password']);
            if($ldapBindUser){
                $groupModel = ClassRegistry::init('Group');
                $availableGroups = $groupModel->find('list', array('fields'=>array('id','name'), 'order'=>array('Group.priority')));
                $horasGroupsInLDAP = array_filter(array_map(function($groupCN){
                    if(preg_match('/horas_([^,]+)/i', $groupCN, $match)){return $match[1]; }else{ return null; }
                }, $entries[0]['memberof']));
                $availableGroups = array_intersect($availableGroups, $horasGroupsInLDAP);
                if(!empty($availableGroups)){
                    $group = $groupModel->find('first', array('conditions'=>array('LOWER(Group.name)'=>mb_strtolower(current($availableGroups)))));
                    $userExist = $usersController->User->find('first', array('conditions'=>array(
                        'User.username'=>$requestData['User']['username'],
                        'User.password'=>Security::hash($requestData['User']['password'], null, true),
                    )));
                    $saveData = array( //sie duomenys visada atnaujinami, nes gali pasikeisti LDAPE
                        'group_id'=>$group['Group']['id'],
                        'password'=>Security::hash($requestData['User']['password'], null, true),
                    );
                    if(!$userExist){
                        //sukuriamas naujas vartotojas su siais duomenimis
                        $saveData = array_merge($saveData, array(
                            'username'=>$requestData['User']['username'],
                            'email'=>isset($entries[0]['mail'])?(is_array($entries[0]['mail'])?implode(',',$entries[0]['mail']):$entries[0]['mail']):'',
                            'first_name'=>$entries[0]['givenname'][0]??'', //displayname Vardas Pavarde
                            'last_name'=>$entries[0]['sn'][0]??'',
                            'active'=>1,
                            'language'=>Configure::read('defaultLanguage'),
                            'operator'=>1
                        ));
                        $usersController->Session->setFlash(__('LDAP autorizacija patvirtinta. OEE sistemoje sukurtas naujas vartotojas. Susisiekite su sistemos administratoriumi, kad jam būtų priskirti darbo centrai.'), 'default', array());
                        $usersController->User->create();
                    }else{
                        $usersController->User->id = $userExist['User']['id'];
                    }
                    $usersController->User->save($saveData);
                }else{
                    return array('errors'=>__('Vartotojas aptiktas LDAP sistemoje, tačiau nepriskirtas Horas grupei'));
                }
            } else {
                return array('errors'=>__('Nepavyko prijungti vartotojo %s prie LDAP sistemos, patikrinkite slaptažodį', $requestData['User']['username']));
            }
        } else {
            return array('errors'=>__('Nepavyko prijungti admin vartotojo prie LDAP sistemos'));
        }
    }

    //kiekviena uzfiksuota palete turi buti grazinama kaip atskiras irasas su kiekiu ir laiku, kada ivyko registravimas
    public function OeeMpm_afterGetDataFromOee_Hook(&$data){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        $plansIdsList = array();
        foreach($data['plans_quantities'] as $sensorPsId => $planData){
            $plansIdsList = array_merge($plansIdsList, Set::extract('/plan_id',$planData));
        }
        $partialQuantityModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder', 'User',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $piecesQuantitySumByPlanId = array();
        $partials = $partialQuantityModel->find('all', array(
            'fields'=>array(
                'PartialQuantity.quantity', 'PartialQuantity.created', 'Plan.id', 'User.unique_number'
            ),
            'conditions'=>array('Plan.id'=>$plansIdsList),
            'order'=>array('Plan.id')
        ));
        foreach($partials as $partial){
            $planId = $partial['Plan']['id'];
            if(!isset($piecesQuantitySumByPlanId[$planId])){
                $piecesQuantitySumByPlanId[$planId] = 0;
            }
            $piecesQuantitySumByPlanId[$planId] += $partial['PartialQuantity']['quantity'];
        }
        foreach($data['plans_quantities'] as $sensorPsId => $planData){
            foreach($planData as $key => $plan){
                $planId = $plan['plan_id'];
                $data['plans_quantities'][$sensorPsId][$key]['quantity'] = $piecesQuantitySumByPlanId[$planId]??0;
            }
        }
    }

    public function OeeMpm_afterGetWorkIntervalsSearch_Hook(&$workIntervals){
        static $partialQuantityModel = null;
        if($partialQuantityModel == null){ $partialQuantityModel = ClassRegistry::init('PartialQuantity'); }
        $plansIdsList = Set::extract('/FoundProblem/plan_id',$workIntervals);
        $partialQuantityModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $partials = $partialQuantityModel->find('all', array(
            'fields'=>array('PartialQuantity.*'),
            'conditions'=>array('Plan.id'=>$plansIdsList),
        ));
        foreach($workIntervals as $key => $interval){
            $intervalPartials = array_filter($partials, function($partial)use($interval){
                return $partial['PartialQuantity']['sensor_id'] == $interval['FoundProblem']['sensor_id'] &&
                    strtotime($partial['PartialQuantity']['created']) >= strtotime($interval[0]['start']) &&
                    strtotime($partial['PartialQuantity']['created']) <= strtotime($interval[0]['end']);
            });
            $workIntervals[$key][0]['quantity'] = empty($intervalPartials)?0:array_sum(Set::extract('/PartialQuantity/quantity',$intervalPartials));
        }
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $moNumbers = Set::extract('/Plan/mo_number', $plans);
        if(isset($currentPlan['ApprovedOrder'])){$moNumbers[] = $currentPlan['Plan']['mo_number'];}
        if(!empty($moNumbers)){
            if(!empty($moNumbers)){
                $partialQuantityModel = ClassRegistry::init('PartialQuantity');
                $partialQuantityModel->bindModel(array('belongsTo'=>array('ApprovedOrder', 'Plan'=>array('foreignKey'=>false,'conditions'=>array('ApprovedOrder.plan_id = Plan.id')))));
                $partialQuantities = Hash::combine($partialQuantityModel->find('all', array(
                    'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'TRIM(Plan.id) AS plan_id'),
                    'conditions'=>array('Plan.mo_number'=>array_unique($moNumbers)),
                    'group'=>array('Plan.id')
                )),'{n}.0.plan_id', '{n}.0.quantity');
                if(isset($currentPlan['ApprovedOrder'])){
                    if(isset($partialQuantities[$currentPlan['Plan']['id']])){
                        $currentPlan['Plan']['remaining_quantity'] = (int)($currentPlan['Plan']['quantity'] - $partialQuantities[$currentPlan['Plan']['id']]);
                    }
                    $currentPlan['Plan']['short_end'] = date('Y-m-d', strtotime($currentPlan['Plan']['end']));
                    $currentPlan['Plan']['quantity'] = (int)$currentPlan['Plan']['quantity'];
                }
                foreach($plans as &$plan){
                    $planId = $plan['Plan']['id'];
                    if(isset($partialQuantities[$planId])){
                        $plan['Plan']['remaining_quantity'] = (int)($plan['Plan']['quantity'] - $partialQuantities[$planId]);
                    }
                    $plan['Plan']['short_end'] = date('Y-m-d', strtotime($plan['Plan']['end']));
                    $plan['Plan']['quantity'] = (int)$plan['Plan']['quantity'];
                }
            }
        }
    }

    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../'.Configure::read('companyTitle').'/css/'.Configure::read('companyTitle').'_bare.css';
    }

    public function Record_calculateMainFactorsInHourAfterLoop_Hook(&$dataInHour, &$fSensor, &$oeeParams, &$dcHoursInfoBlocks, $hourNr){
        $workTime = $dataInHour['fact_prod_time'] + $dataInHour['transition_time'];
        $dcHoursInfoBlocks['fact_plus_transition_time']['value'][(int)$hourNr] = number_format(round(bcdiv($workTime, 60,2)),0,'.','');
        $dcHoursInfoBlocks['downtime_time']['value'][(int)$hourNr] = number_format(round(bcdiv($dataInHour['shift_length'] - $workTime, 60,2)),0,'.','');
        $dcHoursInfoBlocks['exploitation_factor']['title'] = __('Prieina mumas %');
    }

    public function Workcenter_changeUpdateStatusVariables_Hook2(&$val, $fSensor){
        $val['availableSensors'] = array();
    }

    public function Users_beforeChooseSensor_Hook(&$usersController){
        $usersController->redirect(array('controller'=>'users', 'action'=>'login'));
    }
    
    public function WorkCenter_buildPlanName_Hook(&$plan){
        return __('Gaminys').': '.$plan['Plan']['production_name'] .
            ($plan['Plan']['production_code'] ? (' ('.__('gaminio kodas').': ' . $plan['Plan']['production_code'] . ')') : '') .
            ($plan['Plan']['mo_number'] ? (' ('.__('Užsakymo nr.').': '.$plan['Plan']['mo_number'] . ')') : '').
            ($plan['Plan']['process_name'] ? (' ('.__('Procesas').': ' . $plan['Plan']['process_name'] . ')') : '');
    }

    public function Tv_parametersCalculationAfterValuesSet_Hook(&$sensor){
        if(isset($sensor['operational_factor'])) {
            unset($sensor['operational_factor']);
        }
    }

    public function Slides_beforeBeforeFilter_Hook(SlidesController &$slidesController){
        if(in_array($_SERVER['REMOTE_ADDR'], Configure::read('ServerIP'))){
            $slidesController->Auth->allow(array(
                'show'
            ));
        }
    }

    public function TvPlugin_beforeBeforeFilter_Hook(&$controller){
        if(in_array($_SERVER['REMOTE_ADDR'], Configure::read('ServerIP'))){
            $controller->Auth->allow(array(
                'index','get_data'
            ));
        }
    }

}
    