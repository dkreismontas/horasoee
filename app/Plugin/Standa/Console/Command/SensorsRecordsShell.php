<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','DashboardsCalculation');
    
    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
	            'http://172.33.1.242:5580/data',
	            'http://172.33.1.243:5580/data',
	            'http://172.33.1.244:5580/data',
	        );
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $this->args[0]);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
	        $mailOfErrors = array();
	        $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
	        if(file_exists($dataPushStartMarkerPath)){
	            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
	                $this->Sensor->informAboutProblems();
	                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
	            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
	                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
	            }
	        }
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $db = ConnectionManager::getDataSource('default');
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
	            $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            if(!$currentShifts[$branchId]){
                    App::uses('CakeRequest', 'Network');
                    App::uses('CakeResponse', 'Network');
                    App::uses('Controller', 'Controller');
                    App::uses('AppController', 'Controller');
                    App::import('Controller', 'Cron');
                    $CronController = new CronController;
                    $CronController->generateShifts();
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'/', $address);
	            });
	        }
	        $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                $data = json_decode($data);
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
	            foreach($data->periods as $record){
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
	                    $quantity = $record->$sensorPinName;
	                    try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
	                }
	            }
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath)){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->calcOee($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }

	private function calcOee(Array $currentShifts, Array $sensorsListInDB){
        $prevShifts = array();
        foreach($currentShifts as $branchId => $shift){
        	if(time() - strtotime($shift['Shift']['start']) > Configure::read('recordsCycle')*2){ continue; }
        	$prevShift = $this->Shift->getPrev($shift['Shift']['start'], $branchId);
			if(empty($prevShift)){ continue; }
			$prevShifts[$prevShift['Shift']['id']] = $prevShift;
        }
		if(empty($prevShifts)){ return null; }
		$prevShiftsIds = array_keys($prevShifts);
		$oeeCalculatedShiftsIds = $this->DashboardsCalculation->find('list', array(
			'fields'=>array('DashboardsCalculation.shift_id','DashboardsCalculation.shift_id'),
			'conditions'=>array('DashboardsCalculation.shift_id'=>$prevShiftsIds),
			'group'=>array('DashboardsCalculation.shift_id')
		));
		$unCalculatedShiftsIds = array_diff($prevShiftsIds, $oeeCalculatedShiftsIds);
		if(empty($unCalculatedShiftsIds)){ return null; }
		
        set_time_limit(2000);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $shell = new OeeShell();
		
		foreach($unCalculatedShiftsIds as $shiftId){
			$this->Log->write('Apskaiciuojamas OEE praejusiai pamainai. Pamainos ID: '.$shiftId);
            $shift = $prevShifts[$shiftId];
			$shell->shift = $shift;
			$sensorsIdsInCurrentShift = Set::extract('/Sensor/id', array_filter($sensorsListInDB, function($sensor)use($shift){
				return $sensor['Sensor']['branch_id'] == $shift['Shift']['branch_id'];
			}));
            $shell->start_sensors_calculation($sensorsIdsInCurrentShift,false);
		}
    }
	
	
}