<?php
ob_end_clean();
$styleArray = array(
    'borders' => array('outline' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN,
            'color' => array('argb' => '000000'),
        ), ),
);
$bgColorYellow = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'fdff5d')
    )
);
$bgColorBlue = array(
    'fill' => array(
        'type' => PHPExcel_Style_Fill::FILL_SOLID,
        'color' => array('rgb' => 'a4d9fa')
    )
);
$unusedEquipmentProblemsIds = array(
    3,19,20,21,22,24,26,28,29,30,46,66
);
$unusedEquipmentTotal = array();
$row = 2;
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, __('Prastovos pavadinimas'));
$sensorColPos = $totalInRow = $totalInCol = array();
foreach($foundProblems as $problemId => $sensorsHasThisProblem){
    $row++;
    $totalInRow[$row] = 0;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, Settings::translate(current($sensorsHasThisProblem)['problem_name']));
    foreach($sensorsHasThisProblem as $sensorId => $foundProblem){
        if(!isset($sensorColPos[$sensorId])){
            $sensorColPos[$sensorId] = !empty($sensorColPos)?max($sensorColPos)+2 : 1;
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], 2, $foundProblem['sensor_name'].', '.__('val.'));
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId]+1, 2, $foundProblem['sensor_name'].', %');
        }
        $duration = bcdiv($foundProblem['duration'], 3600, 2);
        $totalInRow[$row] = bcadd($totalInRow[$row], $duration, 4);
        if(!isset($totalInCol[$sensorColPos[$sensorId]])){ $totalInCol[$sensorColPos[$sensorId]] = 0; }
        if(!isset($unusedEquipmentTotal[$sensorColPos[$sensorId]])){ $unusedEquipmentTotal[$sensorColPos[$sensorId]] = 0; }
        $totalInCol[$sensorColPos[$sensorId]] = bcadd($totalInCol[$sensorColPos[$sensorId]], $duration, 4);
        if(in_array($problemId, $unusedEquipmentProblemsIds)){
            $unusedEquipmentTotal[$sensorColPos[$sensorId]] = bcadd($unusedEquipmentTotal[$sensorColPos[$sensorId]], $duration, 4);
        }
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId], $row, $duration);
    }
}
$row = 2;
foreach($foundProblems as $problemId => $sensorsHasThisProblem){
    $row++;
    //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row, Settings::translate(current($sensorsHasThisProblem)['problem_name']));
    foreach($sensorsHasThisProblem as $sensorId => $foundProblem){
        $duration = bcdiv($foundProblem['duration'], 3600, 2);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($sensorColPos[$sensorId]+1, $row, $totalInCol[$sensorColPos[$sensorId]] > 0? round(bcdiv($duration, $totalInCol[$sensorColPos[$sensorId]], 5) * 100, 2) : 0);
    }
}
if(empty($sensorColPos)){$sensorColPos[] = 0;}
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+1, __('Viso'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(0, $row+2, __('Neišnaudoti įrengimai'))->getStyleByColumnAndRow(0, $row+2)->applyFromArray(Hash::merge($styleArray, $bgColorYellow));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+2, 2, __('Viso, val.'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, 2, __('Viso, %'));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+2, $row+1, round(array_sum($totalInCol),2));
foreach($totalInCol as $col => $total){
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+1, round($total,2));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+1, 100);
    $unusedEquipmentTotal[$col] ??= 0;
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row+2, round($unusedEquipmentTotal[$col],2))->getStyleByColumnAndRow($col, $row+2)->applyFromArray(Hash::merge($styleArray, $bgColorYellow));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col+1, $row+2, $totalInCol[$col] > 0? round(bcdiv($unusedEquipmentTotal[$col], $totalInCol[$col], 5) * 100, 2) : 0)->getStyleByColumnAndRow($col+1, $row+2)->applyFromArray(Hash::merge($styleArray, $bgColorYellow));
}
foreach($totalInRow as $row => $total){
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+2, $row, round($total,2));
    $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, $row, round(bcdiv($total,array_sum($totalInRow),5)*100,2));
}
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, $row+1, 100);
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+2, $row+2, round(!empty($unusedEquipmentTotal)?array_sum($unusedEquipmentTotal):0,2))->getStyleByColumnAndRow(max($sensorColPos)+2, $row+2)->applyFromArray(Hash::merge($styleArray, $bgColorYellow));
$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow(max($sensorColPos)+3, $row+2, round(!empty($unusedEquipmentTotal) && !empty($totalInCol) && array_sum($totalInCol) > 0 ? bcdiv(array_sum($unusedEquipmentTotal), array_sum($totalInCol),5) * 100 : 0, 2))->getStyleByColumnAndRow(max($sensorColPos)+3, $row+2)->applyFromArray(Hash::merge($styleArray, $bgColorYellow));
for($rowStyle = 2; $rowStyle != $row+2; $rowStyle++){
    $i = 1;
    for($col = 'A'; $col != PHPExcel_Cell::stringFromColumnIndex(max($sensorColPos)+4); $col++) {
        $styleToApply = $i % 2 != 0 && $i > 1?Hash::merge($styleArray,$bgColorBlue):$styleArray; $i++;
        $objPHPExcel->getActiveSheet()->getStyle($col.$rowStyle)->applyFromArray($styleToApply);
    }
}
$letter = 'A';
for ($c = 0; $c <= max($sensorColPos)+4; $c++) {
    $objPHPExcel->getActiveSheet()->getColumnDimension($letter++)->setAutoSize(true);
}
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header('Content-Type: application/vnd.ms-excel');
    header('Content-Disposition: attachment;filename="'.__('Įrenginių išnaudojimo ataskaita').'.xls"');
    header('Cache-Control: max-age=0');
    header('Cache-Control: max-age=1');
    header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
    header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
    header('Cache-Control: cache, must-revalidate');
    header('Pragma: public');
    $objWriter->save('php://output');
    exit();
}