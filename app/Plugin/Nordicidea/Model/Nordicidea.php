<?php
class Nordicidea extends AppModel{

    public function WorkCenter_BeforeUpdateStatus_Hook(&$fSensor, $user){
        if(in_array($fSensor['Sensor']['id'], array(3,5,7,11,12))) {
            $dcHoursInfoBlocks = is_array(Configure::read('workcenter_hours_info_blocks')) ? Configure::read('workcenter_hours_info_blocks') : array();
            $dcHoursInfoBlocks = array('count_in_hour_quantity' => array('title' => __('Laikas'), 'addSpace' => 50, 'value' => array(), 'round' => 2)) + $dcHoursInfoBlocks;
            Configure::write('workcenter_hours_info_blocks', $dcHoursInfoBlocks);
        }
    }

    public function Reports_AftergenerateAttributesUsage_Hook(&$ReportsController){
        $ReportsController->set($ReportsController->viewVars);
        $ReportsController->render('/../Plugin/Nordicidea/View/ProjectViewsHooks/Reports/get_attributes_usage_xls');
    }

    public function Reports_BeforeSearchGenerateAttributesUsage_Hook(&$bindModel, &$searchParameters, &$requestData){
        $bindModel = array('belongsTo'=>array('Sensor','Problem'));
        if(isset($searchParameters['conditions']['Problem.parent_id'])){
            unset($searchParameters['conditions']['Problem.parent_id']);
        }
    }

}
    