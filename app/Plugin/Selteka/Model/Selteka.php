<?php
class Selteka extends AppModel{
     
      public function Record_calculateOee_Hook(&$oeeParams, $data){
          $oeeParams['operational_factor'] = $data['shift_length'] > 0?$data['count_delay'] / $data['shift_length']:0;
          $oeeParams['oee'] = 100 * bcmul(bcmul($oeeParams['exploitation_factor'],$oeeParams['operational_factor'],6), $oeeParams['quality_factor'],6);
          $oeeParams['oee_exclusions'] = 100 * bcmul(bcmul($oeeParams['exploitation_factor_exclusions'],$oeeParams['operational_factor'],6),$oeeParams['quality_factor'],6);
      }
     
     // public function changeUpdateStatusVariablesHook($fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        // if(!empty($currentPlan)){
            // $recordModel = ClassRegistry::init('Record');
            // $quantity = $recordModel->find('first', array(
                // 'fields'=>array('SUM(Record.unit_quantity) AS total'), 
                // 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
            // ));
            // $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.','').' '.__('vnt'); 
        // }
     // }
    public function changePlanSearchInWorkCenterHook(&$planSearchParams){
        unset($planSearchParams['conditions']['Plan.sensor_id']);
    }
     
}
    