setInterval(function(){location.reload();}, 1800000);
Vue.directive('click-outside', {
    priority: 700,
    bind () {
        let self  = this;
        this.event = function (event) {
            self.vm.$emit(self.expression,event)
        };
        this.el.addEventListener('click', this.stopProp);
        document.body.addEventListener('click',this.event);
    },
    unbind() {
        this.el.removeEventListener('click', this.stopProp);
        document.body.removeEventListener('click',this.event);
    },
    stopProp(event) {event.stopPropagation(); }
});
var vm = new Vue({
    el: '#corridors',
    data: {
        corridors: {}, totals: {}, productChoosePopup: false, currentCorridor: {}, currentCorridorProducts: [], productsFilterString: '',
        productSelected: {}, productChooseConfirmPopup: false
    },
    methods: {
        loadData: function(){
            jQuery.ajax({
                url: load_corridors_url,
                type: 'post',
                context: this,
                dataType: 'JSON',
                async: true,
                success: function(request){
                    this.corridors = request.corridors;
                    this.totals = request.totals;
                }
            })
        },
        selectProduct: function(product){
            this.productsFilterString = '';
            this.currentCorridorProducts = [];
            this.productSelected = product;
        },
        closeProductSelector: function(){
            this.productChoosePopup = false;
        },
        filterAllProducts: function(){
            if(!this.productsFilterString.length) {
                this.currentCorridorProducts = this.currentCorridor.Products;
            }
        },
        confirmProduct: function(){
            if(!Object.keys(this.productSelected).length){
                alert('Pasirinkite ką gaminate');
                return false;
            }
            this.productChoosePopup = false;
            this.productChooseConfirmPopup = true;
        },
        confirmConfirmProduct: function(){
            jQuery.ajax({
                url: '/hampidjan/corridors_operations/start_production',
                type: 'post',
                context: this,
                data: {corridor: this.currentCorridor.Sensor, product: this.productSelected},
                success: function(request){
                    this.productChooseConfirmPopup = false;
                    this.loadData();
                }
            })
        },
        goBackToProductSelect: function(){
            this.productChoosePopup = true;
            this.productChooseConfirmPopup = false;
        }
    },
    events: {
        hideProductsSelection: function(){
            this.currentCorridorProducts = [];
        }
    },
    watch: {
        productsFilterString: function(string){
            if(!this.currentCorridor.hasOwnProperty('Products') || !string.length){ return false; }
            this.currentCorridorProducts = this.currentCorridor.Products.filter(function(product){
                return product.name.toLowerCase().indexOf(string.toLowerCase()) >= 0;
            });
        }
    },beforeCompile: function(){
        this.loadData();
        setInterval(this.loadData,60000);
    },
    components: {
        'corridor_comp': {
            template: '#corridorTpl',
            props: ['corridor'],
            data: function(){return { columns:0}},
            beforeCompile: function(){
                this.columns = this.$parent.columns;
            },
            methods: {
                selectCorridor: function(corridor){
                    this.$parent.productSelected = {}
                    jQuery.ajax({
                        url: '/hampidjan/corridors_operations/get_products',
                        type: 'post',
                        context: this,
                        dataType: 'JSON',
                        data: corridor,
                        success: function(products){
                            this.$parent.productChooseConfirmPopup = false;
                            this.$parent.productChoosePopup = true;
                            corridor.Products = products;
                            this.$parent.productsFilterString = '';
                            this.$parent.currentCorridorProducts = new Array();
                            this.$parent.currentCorridor = corridor;
                        }
                    })
                }
            },
            filters: {

            }
        }
    }
});