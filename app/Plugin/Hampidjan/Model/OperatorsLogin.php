<?php
class OperatorsLogin extends HampidjanAppModel{

    public function orderCorridors(Array $corridors){
        $corridorsGroups = array();
        foreach($corridors as $corridor){
            if(!preg_match('/(\d+)(\w+)/i', $corridor['Sensor']['name'], $match)){ continue; }
            $corridorsGroups[$match[2]][(int)$match[1]] = $corridor;
        }
        krsort($corridorsGroups);
        foreach($corridorsGroups as &$corridorsGroup){
            ksort($corridorsGroup);
        }
        return $corridorsGroups;
    }

}