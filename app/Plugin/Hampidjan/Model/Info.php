<?php
class Info extends HampidjanAppModel{

    public $useTable = false;
    public $totalsInfo = array();

    public function attachShiftsQuantities(Array &$corridors, $currShiftOnly = false){
        $recordModel = ClassRegistry::init('Record');
        $shiftModel = ClassRegistry::init('Shift');
        $dashboardCalcModel = ClassRegistry::init('DashboardsCalculation');
        $branchId = current(current($corridors))['Sensor']['branch_id'];
        $currentShift = $shiftModel->findCurrent($branchId);
        $currentShiftQuantities = Hash::combine($recordModel->find('all', array(
            'fields'=>array(
                'ROUND(SUM(Record.unit_quantity),2) AS quantity', 'Record.sensor_id',
                'MAX(Record.created) AS last_record_created'
            ),'conditions'=>array('Record.shift_id'=>$currentShift['Shift']['id']),
            'group'=>array('Record.sensor_id')
        )), '{n}.Record.sensor_id', '{n}.0');
        $shiftDurationInHour = bcdiv(1 + strtotime($currentShift['Shift']['end']) - strtotime($currentShift['Shift']['start']), 3600, 6);
            foreach($corridors as &$corridorsRow) {
                array_walk($corridorsRow, function (&$corridor) use ($currentShiftQuantities, $shiftDurationInHour) {
                    $corridorId = $corridor['Sensor']['id'];
                    $corridor['Vars'] = $corridor[0]??array();
                    unset($corridor[0]);
                    $corridor['current_shift_quantity'] = isset($currentShiftQuantities[$corridorId]['quantity']) ? round($currentShiftQuantities[$corridorId]['quantity']) : 0;
                    $step = (float)$corridor['Plan']['step'];
                    $corridor['intent_count'] = round($step * $shiftDurationInHour);
                    $corridor['remaining_count'] = round($corridor['intent_count'] - $corridor['current_shift_quantity']);
                    $corridor['is_connected'] = isset($currentShiftQuantities[$corridorId]['last_record_created'])?time() - strtotime($currentShiftQuantities[$corridorId]['last_record_created']) <= Configure::read('recordsCycle')*2:false;
                });
            }
        if($currShiftOnly) {
            return null;
        }
        $lastThreeShiftsIds = $shiftModel->find('list', array(
            'fields' => array('Shift.id', 'Shift.id'),
            'conditions' => array('Shift.id <' => $currentShift['Shift']['id']),
            'order' => array('Shift.id DESC'),
            'limit' => 3
        ));
        $dashboardCalcModel->virtualFields = array('sensor_shift' => 'CONCAT(DashboardsCalculation.sensor_id,\'_\',DashboardsCalculation.shift_id)');
        $dashboardCalcModel->bindModel(array('belongsTo' => array('Shift')));
        $prevShiftsCalculations = Hash::combine($dashboardCalcModel->find('all', array(
            'fields' => array(
                'sensor_shift',
                'ROUND(DashboardsCalculation.total_quantity,2) AS total_quantity',
                'TRIM(Shift.type) AS shift_name',
            ), 'conditions' => array('DashboardsCalculation.shift_id' => $lastThreeShiftsIds),
            'order' => array('DashboardsCalculation.shift_id DESC')
        )), '{n}.DashboardsCalculation.sensor_shift', '{n}.0');
        $dashboardCalcModel->virtualFields = array();
        $this->totalsInfo = array(__('Einama pamaina') => 0);
        foreach($corridors as &$corridorsRow) {
            array_walk($corridorsRow, function (&$corridor) use ($currentShiftQuantities, $prevShiftsCalculations, &$totals) {
                $corridorId = $corridor['Sensor']['id'];
                $corridor['last_shifts_dashboards'] = array_filter($prevShiftsCalculations, function($corridorShift)use($corridorId){
                    return current(explode('_',$corridorShift)) == $corridorId;
                },ARRAY_FILTER_USE_KEY);
                $this->totalsInfo[__('Einama pamaina')] += $corridor['current_shift_quantity'];
                foreach($corridor['last_shifts_dashboards'] as $dash){
                    $shiftName = $dash['shift_name'];
                    if(!isset($this->totalsInfo[$shiftName])){ $this->totalsInfo[$shiftName] = 0; }
                    $this->totalsInfo[$shiftName] += $dash['total_quantity'];
                }
            });
        }
    }

    public function attachPins(Array &$corridors, Array $pins){
        $this->totalsInfo = array(__('Pagamintas kiekis')=>0, __('Likęs kiekis')=>0, __('Tikslo kiekis')=>0);
        foreach($corridors as &$corridorsRow){
            foreach($corridorsRow as &$corridor){
                $this->totalsInfo[__('Pagamintas kiekis')] += $corridor['current_shift_quantity'];
                $this->totalsInfo[__('Tikslo kiekis')] += $corridor['intent_count'];
                $port = $corridor['Sensor']['port'];
                $corridorPins = array_filter($pins,function($pin)use($port){
                    return $port == $pin['Sensor']['port'];
                });
                foreach($corridorPins as $pin){
                    $pinNumber = (int)substr(trim($pin['Sensor']['name']),-2);
                    $sensorName = $pin['Sensor']['name'];
                    if($pinNumber % 2 == 0){
                        $corridor['right_pins'][] = $pin;
                    }else{
                        $corridor['left_pins'][] = $pin;
                    }
                }
                if(isset($corridor['right_pins']) && isset($corridor['left_pins'])){
                    $defaultPin = array('Sensor'=>array('marked'=>0,'connection_status'=>0));
                    $diff = sizeof($corridor['right_pins']) - sizeof($corridor['left_pins']);
                    $type = $diff < 0?'right_pins':'left_pins';
                    for($i = 1; $i <= abs($diff); $i++){
                        $corridor[$type][] = $defaultPin;
                    }
                }
            }
        }
        $this->totalsInfo[__('Likęs kiekis')] = $this->totalsInfo[__('Tikslo kiekis')] - $this->totalsInfo[__('Pagamintas kiekis')];
    }

}