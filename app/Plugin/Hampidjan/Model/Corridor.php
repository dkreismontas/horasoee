<?php
class Corridor extends HampidjanAppModel{

    public function generateCorridorsJsons(Array $corridors, Array $currentShift, Array $prevShift, Array $prevShiftOrders, Array $recordsCounts, Array $pins, Array $rotationsCountInShift){
        foreach($corridors as $corridor){
            $corridorId = $corridor['Sensor']['id'];
            $corridorPort = $corridor['Sensor']['port'];
            $counters = $this->getCounters($corridorId, $recordsCounts, $currentShift, $prevShiftOrders);
            $pinsStatuses = $this->getMashines($corridor, $pins, $rotationsCountInShift, $recordsCounts, $currentShift);
            $data = array(
                'corridor' => ltrim($corridor['Sensor']['name'],'0'),
                'shift' => date('H',strtotime($currentShift['Shift']['start'])).' - '.date('H',strtotime($currentShift['Shift']['end'])+1),
                'twine' => $corridor['Product']['production_code']??'',
                'operator' => isset($corridor['User']['id'])?mb_substr(($corridor['User']['first_name'].' '.$corridor['User']['last_name'].' '.$corridor['User']['username']),0,21):'',
                'previous_shift'=> $this->getPreviousShiftInfo($corridorId, $prevShiftOrders, $prevShift),
                'counters' => $counters,
                'results' => $this->getTotalResults($corridorId, $recordsCounts, $counters, $pinsStatuses),
                'machine' => $pinsStatuses['pins']
            );
           if(trim($corridor['Sensor']['device_ip'])){
               //pr($data);
           }
           $this->sendJsonThrouSocket(json_encode($data), $corridor['Sensor']);
        }
    }

    private function getPreviousShiftInfo(Int $corridorId, Array $prevShiftOrders, Array $prevShift){
        $defaultStructure = array('twine' => '','shift' => '','operator' => '','oee' => '');
        if(!isset($prevShiftOrders[$corridorId])){ return $defaultStructure; }
        $order = $prevShiftOrders[$corridorId];
        return array(
            'twine'=>$order['Product']['production_code']??'',
            'shift' => date('H',strtotime($prevShift['Shift']['start'])).' - '.date('H',strtotime($prevShift['Shift']['end'])+1),
            'operator' => isset($order['User']['id'])?mb_substr(($order['User']['first_name'].' '.$order['User']['last_name'].' '.$order['User']['username']),0,13):'',
            'oee'=>isset($order['DashboardsCalculation']['oee'])?round($order['DashboardsCalculation']['oee']):0
        );
    }

    private function getCounters(Int $corridorId, Array $recordsCounts, Array $currentShift, Array $prevShiftOrders){
        $defaultStructure = array('now' => 0,'expected' =>0,'goal' =>0,'previous_shift' =>0);
        if(!isset($recordsCounts[$corridorId])){ return $defaultStructure; }
        static $hoursFromShiftStart,$shiftDurationInHours = null;
        if($hoursFromShiftStart == null){
            $hoursFromShiftStart = bcdiv(time() - strtotime($currentShift['Shift']['start']), 3600, 6);
            $shiftDurationInHours = bcdiv(strtotime($currentShift['Shift']['end']) - strtotime($currentShift['Shift']['start']), 3600, 6);
        }
        $order = $recordsCounts[$corridorId];
        return array(
            'now' => number_format($order['unit_quantity'], 1,'.',''),
            'expected' =>number_format(bcmul(bcdiv($order['unit_quantity'], $hoursFromShiftStart, 6),$shiftDurationInHours, 2), 1,'.',''),
            'goal' =>number_format(bcmul($order['FirstPlanInShift']['Plan']['step'], $shiftDurationInHours, 2), 1,'.',''),
            'previous_shift' =>number_format(($prevShiftOrders[$corridorId]['DashboardsCalculation']['total_quantity']??0) ,1,'.','')
        );
    }

    private function getTotalResults(Int $corridorId, Array $recordsCounts, Array $counters, Array $pinsStatuses){
        $defaultStructure = array(
            'border' => 'grey',
            'last_hour'=>array('status'=>'', 'value'=>0),
            'expected'=>array('status'=>'', 'value'=>0),
            'shift'=>array('status'=>'', 'value'=>0)
        );
        if(!isset($recordsCounts[$corridorId])){ return $defaultStructure; }
        $order = $recordsCounts[$corridorId];
        $shiftEffectivity = $counters['goal'] > 0? bcdiv($counters['expected'], $counters['goal'], 2) : 0;
        $atLeastOnePinActive = array_filter($pinsStatuses['pins'], function($pin){
            return $pin['status'] == 'green';
        });
        return array(
            'border' => !empty($atLeastOnePinActive)?'green':'red',
            'last_hour'=>array(
                'status'=>$order['last_hour_effectivity'] < 0.75?'red':($order['last_hour_effectivity'] < 0.85?'yellow':'green'),
                'value'=>round($order['last_hour_effectivity'] * 100)
            ),
            'expected'=>array(
                'status'=>$shiftEffectivity < 0.8?'red':($shiftEffectivity < 1?'yellow':'green'),
                'value'=>round($shiftEffectivity * 100)
            ),
            'shift'=>array(
                'status'=>$pinsStatuses['total_oee'] < 0.75?'red':($pinsStatuses['total_oee'] < 0.85?'yellow':'green'),
                'value'=>round($pinsStatuses['total_oee'] * 100)
            )
        );
    }

    public function getMashines(Array $corridor, Array $pins, Array $rotationsCountInShift, Array $recordsCounts, Array $currentShift){
        static $shiftDurationInMin, $minutesFromShiftStart = null;
        if($shiftDurationInMin == null){
            $minutesFromShiftStart = bcdiv(1+ time() - strtotime($currentShift['Shift']['start']), 60, 6);
            $shiftDurationInMin = bcdiv(1 + strtotime($currentShift['Shift']['end']) - strtotime($currentShift['Shift']['start']), 60, 6);
        }
        $corridorPort = $corridor['Sensor']['port'];
        $corridorId = $corridor['Sensor']['id'];
        $pins = array_filter($pins, function($pin)use($corridorPort){
            return strtolower($pin['Sensor']['port']) == strtolower($corridorPort);
        });
        $pinStatuses = array('pins'=>array(), 'total_oee'=>0);
        $totalFactCount = 0;
        if(isset($recordsCounts[$corridorId])){
            $plan = $recordsCounts[$corridorId]['FirstPlanInShift'];
            $theoricalRotatitonsCount = $plan['Product']['picks'] > 0 && $corridor['Sensor']['perimeter'] > 0? bcdiv(bcdiv(bcmul($corridor['Sensor']['picks_speed'], $shiftDurationInMin, 6), ($plan['Product']['picks'] * 10), 6), $corridor['Sensor']['perimeter'], 6):0;
        }else{
            $theoricalRotatitonsCount = 0;
        }
        $activePinsCount = 0;
        foreach ($pins as $pin){
            $pinTitle = 'M'.(int)mb_substr(trim($pin['Sensor']['name']),-2);
            if($pin['Sensor']['marked'] == 0){
                $pinStatuses['pins'][$pinTitle] = array(
                    'status' => -1,
                    'availability'=> -1
                );
                continue;
            }
            $activePinsCount++;
            $factRotationsCount = $rotationsCountInShift[trim($pin['Sensor']['name'])]??0;
            $totalFactCount += $factRotationsCount;
            $pinStatuses['pins'][$pinTitle] = array(
                'status' => isset($pin['FoundProblemsTemp']['id']) && $pin['FoundProblemsTemp']['id'] > 0?'red':'green',
                'availability'=> $theoricalRotatitonsCount > 0? round(bcdiv($factRotationsCount, $theoricalRotatitonsCount, 4)*100):0
            );
        }
        if($theoricalRotatitonsCount > 0){//perskaiciuojamas teorinis kiekis visiems irenginiams nuo pamainos pradzios iki dabar
            $theoricalRotatitonsCount = bcmul($theoricalRotatitonsCount, $activePinsCount, 6);
            $theoricalRotatitonsCount = bcdiv(bcmul($theoricalRotatitonsCount, $minutesFromShiftStart, 6), $shiftDurationInMin,6);
            $pinStatuses['total_oee'] = $theoricalRotatitonsCount > 0? bcdiv($totalFactCount, $theoricalRotatitonsCount, 2) : 0;
        }
        return $pinStatuses;
    }

    public function sendJsonThrouSocket(String $jsonData, Array $corridor){
        static $issueRegister,$settingModel,$emailsList,$mail,$minutes = null;
        $deviceIp = $corridor['device_ip'];
        $corridorId = $corridor['id'];
        if(sizeof(explode(':',$deviceIp)) != 2){ return false; }
        list($address,$port) = explode(':',$deviceIp);
        $socketError = $out = '';
        if (($sock = @socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) === false) {
            $socketError .= "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
        }
        if (@socket_connect($sock, $address, $port) === false){
            $socketError .= "socket_connect() failed: reason: " . socket_strerror(socket_last_error($sock)) .' '.$address.':'.$port. "\n";
        }
        $jsonData .= "#END\n";
        $stateOk = false;
        if(!trim($socketError)) {
            socket_write($sock, $jsonData, strlen($jsonData));
            socket_set_option($sock, SOL_SOCKET, SO_RCVTIMEO, array("sec" => 5, "usec" => 0));
            while ($out = socket_read($sock, 2048)) {
                //echo 'KoridoriusID '.$corridorId.':'.$out."\n";
                if(strtolower(trim($out)) == 'ok'){$stateOk = true; }
            }
        }
        socket_close($sock);
        if(!$stateOk || trim($socketError)){
            if($issueRegister == null){
                $settingModel = ClassRegistry::init('Settings');
                $warnSetting = $settingModel->getOne('warn_about_no_records');
                if(!trim($warnSetting)) return null;
                $warnSetting = explode(':', $warnSetting);
                if(sizeof($warnSetting)!=2){ return null; }
                $minutes = isset($warnSetting[1]) && is_numeric(trim($warnSetting[1]))?intval(trim($warnSetting[1])):0;
                $minutes = max(5, $minutes);
                $emailsList = explode('|',current($warnSetting));
                array_walk($emailsList, function(&$data){$data = trim($data);});
                if(empty($emailsList))return null;
                $issueRegister = $settingModel->find('first', array('conditions'=>array('Settings.key'=>'mailsend_on_screen_issue')));
                $issueRegister = json_decode($issueRegister['Settings']['description'],true);
                App::uses('CakeEmail', 'Network/Email');
                $mail = new CakeEmail('default');
            }
            if(!isset($issueRegister[$corridorId]) || time() - $issueRegister[$corridorId] >= $minutes*60){
                $mail->from('noreply@horasoee.eu');
                $mail->to($emailsList);
                $mail->subject(__('HorasOEE pranešimas apie nepavykusį duomenų perdavimą į koridoriaus ekraną'));
                $mail->emailFormat('both');
                $mail->send('Koridoriaus informacija: '.json_encode($corridor)."\n\n".$socketError);
                $issueRegister[$corridorId] = time();
                $settingModel->updateAll(array('description'=>'\''.json_encode($issueRegister).'\''), array('Settings.key'=>'mailsend_on_screen_issue'));
            }
        }
    }

}