<?php
App::uses('Problem', 'Model');
App::uses('FoundProblem', 'Model');
class Hampidjan extends AppModel{

    private $approvedOrder = null;
    private static $ftpRemote = array(
        'host'=>'',
        'port'=>21,
        'username'=>'',
        'password'=>'',
        'path'=>''
    );
    private $indicators = array();

    public function __construct(){
        $this->indicators = array(
            1 => __('Darbo laikai'),
            2 => __('Gamybos laikai'),
            3 => __('Kiekis'),
            4 => __('OEE'),
            5 => __('Prastovų kiekis'),
            6 => __('Prastovų laikas'),
            7 => __('Prieinamumo koeficientas'),
            8 => __('Veiklos efektyvumo koeficientas'),
        );
        parent::__construct();
    }

    public function extendTmpProblem(Array $tmpProblems, Object $sensor, Array $currentShift, Object $corridor){
        if(!isset($tmpProblems[$sensor->id])){ return false; }
        static $foundProblemsTempModel,$shiftModel,$approvedOrderModel,$foundProblemModel = null;
        if($tmpProblems[$sensor->id]['shift_id'] != $currentShift['Shift']['id']){
            if($foundProblemModel === null) {
                $foundProblemModel = ClassRegistry::init('FoundProblem');
                $foundProblemsTempModel = ClassRegistry::init('FoundProblemsTemp');
                $shiftModel = ClassRegistry::init('Shift');
                $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            }
            if($this->approvedOrder === null) {
                $this->approvedOrder = $approvedOrderModel->findCurrent($corridor->id);
            }
            $prevShift = $shiftModel->findById($tmpProblems[$sensor->id]['shift_id']);
            if(empty($prevShift)){
                $prevShift = $shiftModel->find('first',array(
                    'conditions'=>array('Shift.id >'=>$tmpProblems[$sensor->id]['shift_id'], 'Shift.branch_id'=>$sensor->branch_id),
                    'order'=>array('Shift.start')
                ));
            }
            $foundProblemModel->newItem(
                new DateTime($tmpProblems[$sensor->id]['problem_start']),
                new DateTime($prevShift['Shift']['end']),
                Problem::ID_NOT_DEFINED,
                $sensor->id,
                $prevShift['Shift']['id'],
                FoundProblem::STATE_COMPLETE,
                null, false,
                !empty($this->approvedOrder)?$this->approvedOrder['ApprovedOrder']['plan_id']:null,
                array(),
                0,
                $corridor->active_user_id
            );
            $foundProblemsTempModel->deleteAll(array('sensor_id'=>$sensor->id));
            return false;
        }
        return true;
     }

    public function completeTmpProblem(Array $tmpProblems, Object $sensor, Object $corridor, DateTime $endDate = null){
        if(!isset($tmpProblems[$sensor->id])){ return false; }
        static $approvedOrderModel,$foundProblemModel,$foundProblemsTempModel = null;
        if($foundProblemModel === null) {
            $foundProblemModel = ClassRegistry::init('FoundProblem');
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $foundProblemsTempModel = ClassRegistry::init('FoundProblemsTemp');
        }
        if($this->approvedOrder === null) {
            $this->approvedOrder = $approvedOrderModel->findCurrent($corridor->id);
        }
        $foundProblemModel->newItem(
            new DateTime($tmpProblems[$sensor->id]['problem_start']),
            $endDate == null? new DateTime($tmpProblems[$sensor->id]['problem_end']): $endDate,
            Problem::ID_NOT_DEFINED,
            $sensor->id,
            $tmpProblems[$sensor->id]['shift_id'],
            FoundProblem::STATE_COMPLETE,
            null, false,
            !empty($this->approvedOrder)?$this->approvedOrder['ApprovedOrder']['plan_id']:null,
            array(),
            0,
            $corridor->active_user_id
        );
        $foundProblemsTempModel->deleteAll(array('sensor_id'=>$sensor->id));
    }

	public function App_createMenu_Hook(&$menu, $params){
        $userId = Configure::read('user')->id??0;
		$settingsMenu = array(
            new MainMenuItem(__('Nustatymai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'index')), (strtolower($params['controller']) == 'settings' &&  $params['action'] == 'index'), 'iconfa-cog'),
            //new MainMenuItem(__('Linijos'), Router::url(array('plugin'=>'','controller' => 'lines', 'action' => 'index')), (strtolower($params['controller']) == 'lines' &&  $params['action'] == 'index'), 'iconfa-code-fork'),
            new MainMenuItem(__('Koridoriai'), Router::url(array('plugin'=>'','controller' => 'sensors', 'action' => 'index')), (strtolower($params['controller']) == 'sensors' &&  $params['action'] == 'index'), 'iconfa-signal'),
            //new MainMenuItem(__('Prastovų tipai'), Router::url(array('plugin'=>'','controller' => 'problems', 'action' => 'index')), (strtolower($params['controller']) == 'problems' &&  $params['action'] == 'index'), 'iconfa-warning-sign'),
            new MainMenuItem(__('Nuostolių tipai'), Router::url(array('plugin'=>'','controller' => 'loss_types', 'action' => 'index')), (strtolower($params['controller']) == 'losstypes' &&  $params['action'] == 'index'), 'iconfa-trash'),
            new MainMenuItem(__('Neatitikties tipai'), Router::url(array('plugin'=>'','controller' => 'diff_types', 'action' => 'index')), (strtolower($params['controller']) == 'difftypes' &&  $params['action'] == 'index'), 'iconfa-tag'),
            //new MainMenuItem(__('Fabrikai'), Router::url(array('plugin'=>'','controller' => 'factories', 'action' => 'index')), (strtolower($params['controller']) == 'factories' &&  $params['action'] == 'index'), 'iconfa-briefcase'),
            new MainMenuItem(__('Padaliniai'), Router::url(array('plugin'=>'','controller' => 'branches', 'action' => 'index')), (strtolower($params['controller']) == 'branches' &&  $params['action'] == 'index'), 'iconfa-home'),
            new MainMenuItem(__('Pamainos'), Router::url(array('plugin'=>'','controller' => 'shifts', 'action' => 'index')), (strtolower($params['controller']) == 'shifts' &&  $params['action'] == 'index'), 'iconfa-retweet'),
            //new MainMenuItem(__('Atnaujinimai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'check_updates')), (strtolower($params['controller']) == 'settings' && $params['action'] == 'check_updates'), 'iconfa-home'),
        );
        $usersMenu = array(
            new MainMenuItem(__('Sukurti vartotoją'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'edit')), (strtolower($params['controller']) == 'users' &&  $params['action'] == 'edit'), 'iconfa-group'),
            new MainMenuItem(__('Vartotojų sąrašas'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'index')), (strtolower($params['controller']) == 'users' && $params['action'] == 'index'), 'iconfa-group'),
        );
		if($userId == 1){
			$settingsMenu[] = new MainMenuItem(__('Įskiepiai'), Router::url(array('plugin'=>'','controller' => 'settings', 'action' => 'controll_plugins')), (strtolower($params['controller']) == 'settings' && $params['action'] == 'controll_plugins'), 'iconfa-home');
            $usersMenu[] = new MainMenuItem(__('Teisių paskirstymas'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'check_permissions')), (strtolower($params['controller']) == 'users' &&  $params['action'] == 'check_permissions'), 'iconfa-group');
		}
		$menu = array(
            //new MainMenuItem(__('Info skydas'), Router::url(array('plugin'=>'','controller' => 'dashboard', 'action' => 'index')), (strtolower($params['controller']) == 'dashboard' &&  $params['action'] == 'index'), 'iconfa-laptop'),
            new MainMenuItem(__('Info skydas'), '#', false, 'iconfa-laptop', null, array(
                new MainMenuItem(__('Pamainų peržiūra'), Router::url(array('plugin'=>'hampidjan','controller' => 'infos', 'action' => 'get_states_and_counts')), (strtolower($params['controller']) == 'infos' &&  $params['action'] == 'get_states_and_counts'), 'iconfa-check'),
                new MainMenuItem(__('Būsenos ir kiekiai'), Router::url(array('plugin'=>'hampidjan','controller' => 'infos', 'action' => 'get_states_and_counts_currshift')), (strtolower($params['controller']) == 'infos' &&  $params['action'] == 'get_states_and_counts_currshift'), 'iconfa-check'),
                new MainMenuItem(__('Skaidrių rinkiniai'), Router::url(array('plugin'=>'','controller' => 'slides', 'action' => 'display')), (strtolower($params['controller']) == 'slides' &&  $params['action'] == 'display'), 'iconfa-laptop'),
                new MainMenuItem(__('24h grafikas'), Router::url(array('plugin'=>'','controller' => 'work_center_24h', 'action' => 'index')), (strtolower($params['controller']) == 'slides' &&  $params['action'] == 'display'), 'iconfa-bar-chart'),
            )),
            new MainMenuItem(__('Užsakymų statusas'), Router::url(array('plugin'=>'','controller' => 'approved_orders', 'action' => 'index')), (strtolower($params['controller']) == 'approvedorders' &&  $params['action'] == 'index'), 'iconfa-book'),
            //new MainMenuItem(__('Gaminiai'), Router::url(array('plugin'=>'','controller' => 'products', 'action' => 'index')), (strtolower($params['controller']) == 'products'), 'iconfa-check'),
            new MainMenuItem(__('Gaminiai'), '#', false, 'iconfa-check', null, array(
                new MainMenuItem(__('Gaminiai'), Router::url(array('plugin'=>'','controller' => 'products', 'action' => 'index')), (strtolower($params['controller']) == 'products' &&  $params['action'] == 'index'), 'iconfa-check')
            )),
            new MainMenuItem(__('Statistika'), '#', false, 'iconfa-signal', null, array(
                new MainMenuItem(__('Neatitikties kortelės'), Router::url(array('plugin'=>'','controller' => 'diff_cards', 'action' => 'index')), (strtolower($params['controller']) == 'diffcards' &&  $params['action'] == 'index'), 'iconfa-tags'),
                new MainMenuItem(__('Įrašai'), Router::url(array('plugin'=>'','controller' => 'records', 'action' => 'index')), (strtolower($params['controller']) == 'records' &&  $params['action'] == 'index'), 'iconfa-pencil'),
                new MainMenuItem(__('Skydelis'), Router::url(array('plugin'=>'','controller' => 'dashboards', 'action' => 'inner')), (strtolower($params['controller']) == 'dashboards' &&  $params['action'] == 'inner'), 'iconfa-signal'),
                //new MainMenuItem(__('Užsakymo duomenys'), Router::url(array('plugin'=>'','controller' => 'dashboards', 'action' => 'get_mo_stats')), (strtolower($params['controller']) == 'dashboards' &&  $params['action'] == 'get_mo_stats'), 'iconfa-signal')
            )),
            new MainMenuItem(__('Nustatymai'), '#', false, 'iconfa-cog', null, $settingsMenu),
            new MainMenuItem(__('Vartotojai'), '#', false, 'iconfa-group', null, $usersMenu),
            new MainMenuItem(__('Atsijungti'), Router::url(array('plugin'=>'','controller' => 'users', 'action' => 'logout')), false, 'iconfa-signout')
            );
		return true;
    }

    public function Oeeshell_beforeMain_Hook(&$sensorsSearchParams){
        $sensorsSearchParams['conditions'] = array('Sensor.pin_name'=>'');
    }

    public function App_afterBeforeFilter_Hook(Object &$appController, Object $user){
        if(!empty((Array)$user)) {
            $corridors = $appController->Sensor->find('list', array('fields' => array('Sensor.id'), 'conditions' => array('Sensor.pin_name' => '', 'Sensor.device_ip <>'=>'')));
            $user->selected_sensors = array_intersect($user->selected_sensors, $corridors);
            Configure::write('user', $user);
        }
    }

    public function SensorsController_afterIndex_Hook(&$sensorsController, &$viewVars){
        $sensorsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Sensors/index');
    }

    public function SensorsController_afterEdit_Hook(&$sensorsController, &$viewVars){
    	if(isset($sensorsController->request->params['id']) && $sensorsController->request->params['id'] == 0 && $sensorsController->Auth->user()['id'] != 1){
    		$sensorsController->Session->setFlash(__('Naujų koridorių kurti neleidžiama. Prašome kreiptis į sistemos administratorių.'), 'default', array(), 'saveMessage');
			$sensorsController->redirect(array('controller'=>'sensors','action'=>'index'));
    	}
        $title = isset($sensorsController->request->data['Sensor']['name']) ? sprintf(__('Koridorius %s (ID: %d)'), $sensorsController->request->data['Sensor']['name'], $sensorsController->request->data['Sensor']['id']) : __('Naujas koridorius');
        $viewVars['h1_for_layout'] = $viewVars['title_for_layout'] = $title;
        $sensorsController->set($viewVars);
        $sensorsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Sensors/edit');
    }

    public function Sensor_afterCheckDataCorrectivity_Hook(&$errors, &$requestData){
        $errors = array();
        $sensorModel = ClassRegistry::init('Sensor');
        $sensor = $sensorModel->find('first', array('conditions'=>array('Sensor.name'=>$requestData['Sensor']['name'], 'Sensor.id <>'=>$requestData['Sensor']['id'])));
        if(!empty($sensor)){
            $errors[] = __('Koridorius su tokiu pavadinimu jau yra sukurtas');
        }
        if(!trim($requestData['Sensor']['name'])){
            $errors[] = __('Įrašykite koridoriaus pavadinimą');
        }
        if((float)$requestData['Sensor']['picks_speed'] <= 0){
            $errors[] = __('Įveskite mašinos greitį');
        }
        if((float)$requestData['Sensor']['perimeter'] <= 0){
            $errors[] = __('Įveskite mašinos būgno perimetrą');
        }
    }

    public function Products_Index_Hook(&$productsController){
        $productsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Products/index');
    }

    public function Products_Edit_Hook(&$productsController, &$viewVars){
        $viewVars['sensorsList']=$productsController->Sensor->getAsSelectOptions(false, array('Sensor.pin_name'=>''));
        $productsController->set($viewVars);
        $productsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Products/edit');
    }

    public function Product_afterCheckDataCorrectivity_Hook(&$errors, &$requestData){
        if(isset($requestData['Product']['dens']) && (int)$requestData['Product']['dens'] == 0){
            $errors[] = __('Nurodykite denų skaičių');
        }
        if(isset($requestData['Product']['picks']) && (int)$requestData['Product']['picks'] == 0){
            $errors[] = __('Nurodykite piksų skaičių');
        }
        if(isset($requestData['Product']['utilization']) && (float)$requestData['Product']['utilization'] == 0){
            $errors[] = __('Įveskite gaminio normą');
        }
    }

    public function transferShiftRotations($prevShift){
    	$rotationsExportMarkerPath = __dir__.'/../webroot/files/rotations_export.txt';
        if(file_exists($rotationsExportMarkerPath) && time() - filemtime($rotationsExportMarkerPath) < 3600){ //vis dar vyksta eksportavimas is senos sesijos
        	return null;
		}
		//$localFileDir = __dir__.'/../webroot/files/rotations/';
		$localFileDir = '/home/mashine_rotations/';
		$fileName = $prevShift['start'] . ' | ' . $prevShift['end'] . '.txt';
        $localFilePath = $localFileDir.$fileName;
		if(file_exists($localFilePath) || time() - strtotime($prevShift['end']) < Configure::read('recordsCycle')*2){	//failas jau eksportuotas arba nauja pamaina katik prasidejo, todel palaukiame pora minuciu, kad tikrai sueitu visu koridoriu irasai
			return null;
		}
		$fh = fopen($rotationsExportMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $rotationsModel = ClassRegistry::init('MachinesRotationsLog');
        set_time_limit(30);
        $localDump = fopen($localFilePath, "w");
        $pageNr = 0; $limit = 500000;
        while(true){
            $rotations = $rotationsModel->find('all', array(
                'conditions'=>array('created <='=>$prevShift['end'])
            ,'order'=>array('MachinesRotationsLog.corridor_name', 'MachinesRotationsLog.machine_sensor_name', 'MachinesRotationsLog.created'),
                'limit'=>$limit,
                'offset'=>$pageNr * $limit
            ));
            if(empty($rotations)){
                break;
            }
            foreach ($rotations as $rotation){ $rotation = $rotation['MachinesRotationsLog'];
                fwrite($localDump, $rotation['corridor_name'].','.$rotation['machine_sensor_name'].','.$rotation['created']."\n");
            }
			unset($rotations);
            $pageNr++;
        }
        fclose($localDump);
        $rotationsModel->query('DELETE FROM `'.$rotationsModel->useTable.'` WHERE `created` <= \''.$prevShift['end'].'\' ');
        // if(trim(self::$ftpRemote['host'])) {
            // $localDump = fopen($localFilePath, "r");
            // $ftpConn = ftp_connect(self::$ftpRemote['host'], self::$ftpRemote['port'], 10) or die("Could not connect to remote FTP server");
            // $login = ftp_login($ftpConn, self::$ftpRemote['username'], self::$ftpRemote['password']);
            // ftp_set_option($ftpConn, FTP_TIMEOUT_SEC, 1800);
            // ftp_pasv($ftpConn, true);
            // ftp_fput($ftpConn, $fileName, $localDump, FTP_BINARY);
            // ftp_close($ftpConn);
            // $this->removeOldLocalFiles($localFileDir, 3600*24*7);
        // }
        unlink($rotationsExportMarkerPath);
		$this->removeOldLocalFiles($localFileDir, 3600*24*14);
    }

    private function removeOldLocalFiles($dir, $deleteIfOlder){
        $files = scandir($dir);
        foreach($files as $f) {
            if($f == '..' || $f == '.') {
                continue;
            }
            $file_date = filemtime($dir.$f);
            if($file_date < (time() - $deleteIfOlder)) {
                unlink($dir.$f);
            }
        }
    }

    public function Dashboards_AfterInner_Hook(&$dashboardsController){
        $dashboardsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Dashboards/inner');
    }

    public function Dashboards_widget_Hook(&$dashboardsController, &$indicators){
        $sensorsList = $dashboardsController->Sensor->getAsSelectOptions(false, array('Sensor.pin_name'=>'', 'Sensor.device_ip <>'=>''));
        $indicators = $this->indicators;
        $dashboardsController->set(compact('sensorsList','indicators'));
        return $dashboardsController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Dashboards/widget1');
    }

    public function Dashboards_BeforeChart_Hook(&$dashboardModel){
        $dashboardModel = ClassRegistry::init('Hampidjan.Dashboard');
        $dashboardModel->indicators = $this->indicators;
    }

    public function Oeeshell_filterGetFactTimeRecords_Hook(&$time, &$shift, &$records, &$returnData, &$sensors, &$shift_length){
        $sensorModel = ClassRegistry::init('Sensor');
        $corridors = $sensorModel->find('list', array(
            'fields'=>array('Sensor.id', 'Sensor.port'),
            'conditions'=>array('Sensor.pin_name'=>'','Sensor.device_ip <>'=>'','Sensor.id'=>$sensors),
        ));
		$corridorPins = Hash::combine($sensorModel->find('all', array(
            'fields'=>array('COUNT(Sensor.id) AS count', 'GROUP_CONCAT(Sensor.id) AS sensors_ids', 'Sensor.port'),
            'conditions'=>array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1, 'Sensor.port'=>array_unique($corridors)),
            'group'=>array('Sensor.port')
        )),'{n}.Sensor.port', '{n}.0');
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $foundProblemTmpModel = ClassRegistry::init('FoundProblemsTemp');
		$conditions['FoundProblem.plan_id'] = $conditionsTmp['FoundProblemsTemp.plan_id'] = Set::extract('/Plan/id',$time);
		$conditions['Sensor.pin_name <>'] = $conditionsTmp['Sensor.pin_name <>'] = '';
        if($shift['Shift']['id'] == 0){
        	$conditions['FoundProblem.end >='] = $shift['Shift']['start'];
        	$conditionsTmp['FoundProblemsTemp.problem_start >='] = $shift['Shift']['start'];
        }else{
        	$conditions['FoundProblem.shift_id'] = $shift['Shift']['id'];
        	$conditionsTmp['FoundProblemsTemp.shift_id'] = $shift['Shift']['id'];
        }
		$foundProblemModel->bindModel(array('belongsTo'=>array('Sensor')));
        $downtimes = Hash::combine($foundProblemModel->find('all', array(
            'fields'=>array(
            	'SUM(TIMESTAMPDIFF(SECOND,
                    CASE WHEN FoundProblem.start > \''.$shift['Shift']['start'].'\' THEN FoundProblem.start ELSE \''.$shift['Shift']['start'].'\' END,
                    CASE WHEN FoundProblem.end < \''.$shift['Shift']['end'].'\' THEN FoundProblem.end ELSE \''.$shift['Shift']['end'].'\' END)) AS true_problem_time',
                'COUNT(FoundProblem.id) AS true_problem_count',
                'Sensor.port',
                'FoundProblem.plan_id'
            ),'conditions'=>$conditions,
            'group'=>array('FoundProblem.plan_id')
        )),'{n}.FoundProblem.plan_id', '{n}.0');
		$foundProblemTmpModel->bindModel(array('belongsTo'=>array(
			'Sensor',
		)));
        $downtimesTmp = Hash::combine($foundProblemTmpModel->find('all', array(
            'fields'=>array(
            	'SUM(TIMESTAMPDIFF(SECOND,
                    CASE WHEN FoundProblemsTemp.problem_start > \''.$shift['Shift']['start'].'\' THEN FoundProblemsTemp.problem_start ELSE \''.$shift['Shift']['start'].'\' END,
                    \''.date('Y-m-d H:i:s').'\')) AS true_problem_time',
                'COUNT(FoundProblemsTemp.id) AS true_problem_count',
                'Sensor.port',
                'FoundProblemsTemp.plan_id'
            ),'conditions'=>$conditionsTmp,
            'group'=>array('FoundProblemsTemp.plan_id')
        )),'{n}.FoundProblemsTemp.plan_id', '{n}.0');
		foreach($downtimes as $planId => &$downtimeData){
			if(!isset($downtimesTmp[$planId])){ continue; }
			$downtimeData['true_problem_time'] += $downtimesTmp[$planId]['true_problem_time'];
			$downtimeData['true_problem_count'] += $downtimesTmp[$planId]['true_problem_count'];
		}
        foreach ($time as &$timeSingle) {
            $planId = $timeSingle['Plan']['id'];
            $corridorId = $timeSingle['Record']['sensor_id'];
            $port = $corridors[$corridorId]??0;
            $sensorsCount = $corridorPins[$port]['count']??0;
			$timeSingle[0]['sensors_count'] = $sensorsCount;
			$timeSingle[0]['total_time'] *= $sensorsCount;
            if(!isset($downtimes[$planId])){
                $timeSingle[0]['true_problem_time'] = 0;
                $timeSingle[0]['true_problem_time_exclusions'] = 0;
                $timeSingle[0]['true_problems_ids'] = '';
                $timeSingle[0]['problems_ids_without_exclusions'] = '';
                $timeSingle[0]['fact_prod_time'] = $shift_length * $sensorsCount * 60;
                $timeSingle['Plan']['step'] /= $sensorsCount;
                continue;
            }
            $timeSingle[0]['true_problem_time'] = $downtimes[$planId]['true_problem_time'];
            $timeSingle[0]['true_problem_time_exclusions'] = $downtimes[$planId]['true_problem_time'];
            $timeSingle[0]['true_problems_ids'] = implode(',',range(0,$downtimes[$planId]['true_problem_count'],1));
            $timeSingle[0]['problems_ids_without_exclusions'] = implode(',',range(0,$downtimes[$planId]['true_problem_count'],1));
            $timeSingle[0]['fact_prod_time'] = $shift_length * $sensorsCount * 60 - $downtimes[$planId]['true_problem_time'];
			$timeSingle['Plan']['step'] /= $sensorsCount;
        }
    }
	
	//Kiekvienas koridorius turi kitoki sesnoriu kieki, todel perskaiciuojame atskirai pamainos trukme 
	public function Oeeshell_appendGetFactTimeLoop_Hook(&$records, &$val, $corridorId){
		static $corridorPins, $corridors = null;
		if($corridorPins == null){
			$sensorModel = ClassRegistry::init('Sensor');			
			$corridorPins = Hash::combine($sensorModel->find('all', array(
	            'fields'=>array('COUNT(Sensor.id) AS count', 'Sensor.port'),
	            'conditions'=>array('Sensor.pin_name <>'=>'', 'Sensor.marked'=>1),
	            'group'=>array('Sensor.port')
	        )),'{n}.Sensor.port', '{n}.0.count');
			$corridors = $sensorModel->find('list', array(
	            'fields'=>array('Sensor.id', 'Sensor.port'),
	            'conditions'=>array('Sensor.pin_name'=>'','Sensor.device_ip <>'=>''),
	        ));
		}
		if(!$corridorPins){ return null; }
		$port = $corridors[$corridorId]??0;
		$sensorsCount = $corridorPins[$port]??0;
		$records['sensors'][$corridorId]['shift_length'] *= $sensorsCount;
		$records['sensors'][$corridorId]['shift_length_with_exclusions'] = $records['sensors'][$corridorId]['shift_length'];
	}

    public function WorkCenter24_BeforeRecordSearch_Hook(&$searchQuery, &$bindModel,&$shift, &$corridorsList){
        array_walk($corridorsList, function(&$corridor){
            $corridor['tv_legends'] = '<div class="legends"><ul>
                <li><span></span> '.__('faktinis kiekis / teorinis kiekis, %').'</li>
                <li><span></span> '.__('100 - (faktinis kiekis / teorinis kiekis), %').'</li>
            </ul></div>';
        });
        $bindModel = array();
        $recordModel = ClassRegistry::init('Record');
        $shiftModel = ClassRegistry::init('Shift');
        $db = $recordModel->getDataSource();
        $fields = array(
            'SUM(' . Configure::read('recordsCycle') . ') / 3600 * Plan.step AS planned_quantity',
            'SUM(Record.unit_quantity) AS fact_quantity',
            'TRIM(Record.sensor_id) AS sensor_id',
            'DATE_FORMAT(Record.created,\'%Y-%m-%d %H:%i\') AS groupper',
        );
        $subQuery = $db->buildStatement(
            array(
                'fields' => $fields,
                'alias' => 'Record',
                'table' => $db->fullTableName($recordModel),
                'conditions' => $searchQuery['conditions'],
                'order' => $searchQuery['order'],
                'group' => array('Record.sensor_id', 'DATE_FORMAT(Record.created,\'%Y-%m-%d %H\')', 'Record.plan_id'),
                'joins' => array(array('table' => 'plans', 'alias' => 'Plan', 'type' => 'LEFT', 'conditions' => array('Plan.id = Record.plan_id')))
            ),
            $recordModel
        );
        $fields = array(
            'LEAST(3600, ROUND(SUM(Record.fact_quantity) / SUM(Record.planned_quantity) * 3600)) AS green_time',
            '3600 - LEAST(3600, ROUND(SUM(Record.fact_quantity) / SUM(Record.planned_quantity) * 3600)) AS red_time',
            'TRIM(Record.sensor_id) AS sensor_id',
            'TRIM(Record.groupper) AS groupper',
        );
        $searchQuery = $db->buildStatement(
            array(
                'fields' => $fields,
                'alias' => 'Record',
                'table' => '(' . $subQuery . ')',
                'order' => array('Sensor.name', 'Record.groupper'),
                'group' => array('Record.sensor_id', 'Record.groupper'),
                'joins' => array(array('table' => 'sensors', 'alias' => 'Sensor', 'type' => 'LEFT', 'conditions' => array('Record.sensor_id = Sensor.id')))
            ),
            $recordModel
        );
    }

	public function SlidesAfterEditHook(&$viewVariables){
		$sensorModel = ClassRegistry::init('Sensor');
		$viewVariables['sensors'] = $sensorModel->getAsSelectOptions(false, array('Sensor.pin_name'=>'', 'Sensor.device_ip <>'=>''));
		$viewVariables['viewTypes'] = array(2=>__('24h grafikas'));
		$viewVariables['sensorsLabel'] = __('Koridoriai');
	}

	public function ApprovedOrdersIndexHook(&$appOrdersController){
		$appOrdersController->set(array(
			'sensorsLabel'=>__('Koridoriai'),
			'returnOrderPerm' => false
		));
	}
	
	public function Records_AfterIndex_Hook(&$recordsController){
		$recordsController->set(array(
			'sensorsLabel'=>__('Koridorius'),
		));
	}

	public function LossTypes_AfterIndex_Hook(&$lossTypesController){
		$lossTypesController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/LossTypes/index');
	}

	public function LossTypes_AfterEdit_Hook(&$lossTypesController){
		$lossTypesController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/LossTypes/edit');
	}

	public function Branches_AfterIndex_Hook(&$branchesController){
		$branchesController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Branches/index');
	}

	public function Branches_AfterEdit_Hook(&$branchesController){
		$branchesController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Branches/edit');
	}

	public function Users_AfterEdit_Hook(&$usersController){
		$usersController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Users/edit');
	}

	public function Users_AfterIndex_Hook(&$usersController){
		$usersController->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/Users/index');
	}

	public function Sensor_beforeInformAboutProblems_Hook(&$queryParameters){
        $queryParameters['conditions'] = array(
            'Sensor.pin_name'=>'', 'Sensor.device_ip <>'=>'','Sensor.marked'=>1
        );
    }

    public function ApprovedOrdersEditHook(&$controller){
        $controller->render('/../Plugin/Hampidjan/View/ProjectViewsHooks/ApprovedOrders/edit');
    }
}
    