<?php
class Report extends HampidjanAppModel{

    public function getWorkersReport(Array $shifts){
        $orderCalcModel = ClassRegistry::init('OrderCalculation');
        $orderCalcModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = OrderCalculation.approved_order_id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
            'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
            'User',
            'Sensor',
            'Shift'
        )));
        $orders = $orderCalcModel->find('all', array(
            'fields'=>array(
                'Shift.start',
                'Shift.type',
                'TRIM(Product.production_code) AS production_code',
                'CONCAT(User.first_name,\' \',User.last_name) AS user',
                'TRIM(OrderCalculation.quantity) AS quantity',
                'OrderCalculation.sensor_id',
                'Sensor.name'
            ),'conditions'=>array('OrderCalculation.shift_id'=>array_keys($shifts)),
            'order'=>array('OrderCalculation.shift_id','Sensor.name','OrderCalculation.id')
        ));
//        $ordersByShift = array();
//        foreach($orders as $order){
//            $corridorName = $order['Sensor']['name'];
//            $shiftId = $order['OrderCalculation']['shift_id'];
//            $ordersByShift[$corridorName][$shiftId][] = $order[0];
//        }
//        unset($orders);
        return $orders;
    }

    public function getOrdersReport(Array $shifts){
        $orderCalcModel = ClassRegistry::init('OrderCalculation');
        $orderCalcModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = OrderCalculation.approved_order_id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
            'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
            'Shift'
        )));
        $orders = $orderCalcModel->find('all', array(
            'fields'=>array(
                'Product.production_code',
                'Plan.id',
                'SUM(OrderCalculation.quantity) AS quantity',
                'Shift.start',
                'Shift.type',
            ),'conditions'=>array('OrderCalculation.shift_id'=>array_keys($shifts)),
            'order'=>array('OrderCalculation.shift_id','Product.production_code'),
            'group'=>array('Shift.id','Product.production_code')
        ));
//        $ordersByShift = array();
//        foreach($orders as $order){
//            $productCode = $order['Product']['production_code'];
//            $shiftId = $order['OrderCalculation']['shift_id'];
//            $ordersByShift[$productCode][$shiftId] = $order['OrderCalculation']['quantity'];
//        }
//        unset($orders);
        return $orders;
    }

    public function getSensorsDowntimes(Array $shifts){
        $orderCalcModel = ClassRegistry::init('OrderCalculation');
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $sensorModel = ClassRegistry::init('Sensor');
        $orderCalcModel->bindModel(array('belongsTo'=>array(
            'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = OrderCalculation.approved_order_id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
            'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
            'Sensor',
        )));
        $orders = $orderCalcModel->find('all', array(
            'fields'=>array(
                'Plan.id',
                'OrderCalculation.shift_id',
                'Product.production_code AS production_code',
                'OrderCalculation.quantity',
                'Sensor.name','Sensor.port'
            ),'conditions'=>array('OrderCalculation.shift_id'=>array_keys($shifts)),
            'order'=>array('OrderCalculation.shift_id','Sensor.name'),
        ));
        $plansIds = Set::extract('/Plan/id', $orders);
        $foundProblems = $foundProblemModel->find('all', array(
            'fields'=>array(
                'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)) as downtime_duration',
                'COUNT(FoundProblem.id) AS downtimes_count',
                'FoundProblem.plan_id',
                'FoundProblem.sensor_id',
                'FoundProblem.shift_id',
            ),'conditions'=>array(
                'FoundProblem.plan_id'=>$plansIds,
                'FoundProblem.shift_id'=>array_keys($shifts),
            ),'group'=>array('FoundProblem.shift_id','FoundProblem.plan_id', 'FoundProblem.sensor_id')
        ));
        $sortedDowntimes = array();
        $sortedTotalDowntimes = array();
        foreach($foundProblems as $foundProblem){
            $sensorId = $foundProblem['FoundProblem']['sensor_id'];
            $planId = $foundProblem['FoundProblem']['plan_id'];
            $shiftId = $foundProblem['FoundProblem']['shift_id'];
            $sortedDowntimes[$shiftId][$planId][$sensorId] = $foundProblem[0];
            if(!isset($sortedTotalDowntimes[$planId][$sensorId])){
                $sortedTotalDowntimes[$planId][$sensorId] = $foundProblem[0];
            }else{
                $sortedTotalDowntimes[$planId][$sensorId]['downtime_duration'] += $foundProblem[0]['downtime_duration'];
                $sortedTotalDowntimes[$planId][$sensorId]['downtimes_count'] += $foundProblem[0]['downtimes_count'];
            }
        }
        $sensors = $sensorModel->find('all', array(
            'fields'=>array('Sensor.name','Sensor.port','Sensor.id'),
            'conditions'=>array('Sensor.marked'=>1, 'Sensor.pin_name <>'=>'', 'Sensor.device_ip'=>'')
        ));
        $corridorSensors = array();
        foreach($sensors as $sensor){ $sensor = $sensor['Sensor'];
            $corridorSensors[$sensor['port']][$sensor['id']] = (int)substr($sensor['name'],-2);
        }
        $ordersByShift = array();
        $totalOrders = array();
        foreach($orders as $order){
            $corridorName = $order['Sensor']['name'];
            $corridorPort = $order['Sensor']['port'];
            $shiftId = $order['OrderCalculation']['shift_id'];
            $productCode = $order['Product']['production_code'];
            $planId = $order['Plan']['id'];
            if(!isset($ordersByShift[$corridorName][$shiftId][$productCode])) {
                $ordersByShift[$shiftId][$corridorName][$productCode] = array();
            }
            if(!isset($totalOrders[$corridorName][$productCode])) {
                $totalOrders[$corridorName][$productCode] = array();
            }
            if(isset($sortedDowntimes[$shiftId][$planId])){
                foreach($sortedDowntimes[$shiftId][$planId] as $sensorId => $downtimeData){
                    if(!isset($corridorSensors[$corridorPort][$sensorId])){ continue; }
                    $pinNr = $corridorSensors[$corridorPort][$sensorId];
                    $ordersByShift[$shiftId][$corridorName][$productCode][$pinNr] = $downtimeData;
                }
            }
            if(isset($sortedTotalDowntimes[$planId])){
                foreach($sortedTotalDowntimes[$planId] as $sensorId => $downtimeData){
                    if(!isset($corridorSensors[$corridorPort][$sensorId])){ continue; }
                    $pinNr = $corridorSensors[$corridorPort][$sensorId];
                    $totalOrders[$corridorName][$productCode][$pinNr] = $downtimeData;
                }
            }
        }
        return array($ordersByShift, $totalOrders);
    }

}