<?php
App::uses('ApprovedOrder', 'Model','Record');
class Dashboard extends AppModel {
	const RecordDuration = 20; // Kas kiek laiko sukuriamas įrašas, sekundėmis

    var $name = 'Dashboard';
    var $useTable = false;
    var $components = array();
	var $indicator = 'indicator1';

	private $primaryData;
	private $modelSearch,$model;
	private $start,$end;
	private $conditions;
	private $fieldToSearch,$groupByDate,$xLegend,$angle;
	private $xLabel='xLabel';	//pagal si laukeli isvedame greafiko x asi
	private $operationField,$averageField,$operationFieldAs;
    private $sumMultiplier = 1;
	private $showType = 'single'; //grafiko rodymo tipas (single-paprastas, multiple-sugrupuotas pagal datas ir reiksme)
	private $dataStructure;
	private $axis;
	private $attr;
	private $shiftsToSearch; //pamainos, kuriose bus ieskomi duomenys; Jei bus pazymeta visos pamainos, ieskosim visose
	private $mergeData=array(); //jei vienos uzklausos metu nepavyksta visko suskaiciuot, vykdomas papildomas skaiciavimas ir ant galo duomenys sumerginami i kruva
	private $globalGroupBy = array(); //sarasas, pagal kuri visados grupuosime irasus
	private $bind; //aprasytos per masyva su kokiom lentelem vyksta sajungos
	private $allowPSIdGrouping=true;	//nurodo ar galima grupuoti pagal sensoriu. Kai skaiciuojame bendra laika tokio grupavimo nereikia (18 grafikas pvz)
	private $orderBy = array();
	private $colors = array();
    private $dateTypes = array(2=>array('z', 'day'), 3=>array('W','week'), 4=>array('m','month'));
    public $timeGroupsForSubcribsions = array('','0', '1', '2', '3', '4','9','10','11');
    private $problemsTreeList = null;
    public $time_patterns = array();
    public $splitColumns = array(); //jei istraukus vienos eilutes duomenis yra dvi vertes, kurias reikia isvesti i atskirus stulpleius, jas aprasome cia
    public $indicators = array();

   /* <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="0"><?php echo __('Praėjusi pamaina'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="1"><?php echo __('Praėjusi para'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="2"><?php echo __('Praėjusi savaitė'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="10"><?php echo __('Praėjusi savaitė iki dabar'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="3"><?php echo __('Praėjęs mėnuo'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="9"><?php echo __('Praėjęs mėnuo iki dabar'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="4"><?php echo __('Praėję metai'); ?></a></li>*/

    public function getTimePatterns(){
        return array(
            0 => __('Praėjusi pamaina'),
            1 => __('Praėjusi para'),
            2 => __('Praėjusi savaitė'),
            10 => __('Praėjusi savaitė iki dabar'),
            3 => __('Praėjęs mėnuo'),
            9 => __('Praėjęs mėnuo iki dabar'),
            4 => __('Praėję metai'),
            6 => __('Ši savaitė'),
            7 => __('Šis mėnuo'),
            8 => __('Šie metai'),
        );
    }

    public function getStatisticalSubscriptionsPeriodicityTypes(){
        return array(
            0=>__('Pamaininis'),
            1=>__('Paromis'),
            2=>__('Savaitinis'),
            3=>__('Mėnesinis')
        );
    }

    private function GetLineParameters($data){
        $graphData = array();
        if(isset($data[$this->indicator]) && trim($data[$this->indicator])!==''){
            $graphData = $this->start_calculation($data,true);
//            fb($graphData,'graphgraphdata');
        }
        return $graphData;
    }

	private function setModel(){
		$bind = array('Sensor','Shift'); //duomenis visados jungiame su Sensor lentele
		$this->orderBy = array();
		switch($this->primaryData[$this->indicator]){ //atranka pagal rodikli
            //case 1: //darbo laikai
//				$this->model = 'DashboardsCalculation';
//                $this->modelSearch = ClassRegistry::init($this->model);
			//break;
            default:
                $this->model = 'DashboardsCalculation';
                $this->modelSearch = ClassRegistry::init($this->model);
            break;
		}
		if(in_array('Sensor',$bind) || isset($bind['Sensor'])){ //visur jei tik yra sensor lentele prijungiame branch lentele
			$bind['Branch'] = array('className'=>'Branch', 'foreignKey'=>false, 'conditions'=>array('Sensor.branch_id = Branch.id'));
		}

		$this->bind = $bind;
		$this->modelSearch->bindModel(array('belongsTo'=>$bind),false);
        
        if($this->problemsTreeList === null){
            $problemModel = ClassRegistry::init('Problem');
            $this->problemsTreeList = $problemModel->parseThreadedProblemsTitles($problemModel->find('threaded'));
        }
	}

	private function setConditions(){
		if(isset($this->primaryData['time_pattern']) && $this->primaryData['time_pattern'] >= 0 && is_numeric($this->primaryData['time_pattern'])){
			list($this->start,$this->end) = $this->components['help']->setStartEndByPattern($this->primaryData);
		}else{
		    if(isset($this->primaryData['date_start_end'])){
                @list($this->primaryData['date_start'], $this->primaryData['date_end']) = explode(' ~ ', $this->primaryData['date_start_end']);
            }
			if(preg_match('/\d{4}-/', trim($this->primaryData['date_start'])) || !trim($this->primaryData['date_start'])){
	        	$this->start = trim($this->primaryData['date_start'])?trim($this->primaryData['date_start']):current(current($this->modelSearch->find('first',array('fields'=>array('MIN(Shift.start) as min_start'),'conditions'=>array('Shift.start <>'=>0)))));
	            $this->end = trim($this->primaryData['date_end'])?trim($this->primaryData['date_end']):date('Y-m-d H:i');
	        }elseif(is_numeric(trim($this->primaryData['date_start']))){
	            $this->start = date('Y-m-d H:i',strtotime('-'.trim($this->primaryData['date_start'].' days')));
				$this->end = date('Y-m-d H:i');
			}else{
				//die('Neteisingai paduotas pradžios laikas');
			}
		}
        $this->start = date('Y-m-d H:i:s',strtotime($this->start));
        $this->end = date('Y-m-d H:i:s',strtotime($this->end));
		$from = array('Shift.start < \''.$this->end.'\'');
        $till = array('Shift.end >= \''.$this->start.'\'');
//		if(in_array($this->primaryData[$this->indicator],array(4,9,10,11,12,13))){ //record
//			$from = array($this->model.'.created > \''.$this->start.'\'');
//        	$till = array($this->model.'.created <= \''.$this->end.'\'');
//		}
		if(in_array($this->primaryData[$this->indicator],array(14,16,18))){ 
			$from = array($this->model.'.start < \''.$this->end.'\'');
        	$till = array($this->model.'.start >= \''.$this->start.'\'');
		}
		if(in_array($this->primaryData[$this->indicator],array(15,17))){ //Loss
			$from = array('ApprovedOrder.end <= \''.$this->end.'\'');
        	$till = array('ApprovedOrder.end > \''.$this->start.'\'');
		}
        if($this->model == 'FoundProblem'){//2019-07-10 salyga reikalinga, kai intervalai nepatenka pilnai i pamaina, kad nesigautu minusiniu laiku, nes tada pakanka, kad prastova patenka i pamaina, bet gali nepatekti i intervala
		//if(in_array($this->primaryData[$this->indicator],array(18,19))){ //FoundProblem
			$from = array($this->model.'.start < \''.$this->end.'\'');
       	    $till = array($this->model.'.end > \''.$this->start.'\'');
		}
        $sensorCondition = $shiftCondition = array();
		if(!empty($this->primaryData['sensors'])){
			$sensorCondition = array('Sensor.id'=>$this->primaryData['sensors']);
		}
        if($this->model == 'FoundProblem' && (!isset($this->primaryData['add_downtime_exclusions']) || $this->primaryData['add_downtime_exclusions'] == 0)){
            $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
            $exclusionList = $plugin_model::getExclusionIdList();
            if(!empty($exclusionList)) {
                $sensorCondition[$this->model . '.problem_id NOT'] = $exclusionList;
            }
        }
        $shiftCondition[] = array('Shift.disabled'=>0);
		if(!empty($this->shiftsToSearch) && !in_array('-1',$this->primaryData['shifts'])){
			$shiftCondition[] = array('Shift.type'=>$this->shiftsToSearch);
		}
		$this->conditions = array('conditions'=>array_merge($from,$till,$sensorCondition, $shiftCondition));
        /*if(in_array($this->primaryData['time_group'], array(2,3,4)) && in_array($this->primaryData[$this->indicator], array(0,1,2,23,24))){ //savaitinis bei menesinis grupavimas ir grafiko tipas yra OEE, prieinamumo bei efektyvumo koef
            $type = $this->dateTypes[$this->primaryData['time_group']][1];
            $this->conditions = array('conditions'=>array_merge($sensorCondition, array("CONCAT($this->model.year,'-',$this->model.$type)" => $this->getPeriodsList())));
        }*/
	}
    
    //gaunamas nuo start iki end metai-menuo arba metai-savaites nr sarasas, pvz 2017-22, 2017-23, ...
    private function getPeriodsList(){
        $start = strtotime($this->start);
        $periodsList = array();
        while($start < strtotime($this->end)){
            $periodsList[] = date('Y', $start).'-'.(int)date($this->dateTypes[$this->primaryData['time_group']][0], $start);
            $start = strtotime('+1 '.$this->dateTypes[$this->primaryData['time_group']][1], $start);
        }
        return $periodsList;
    }

	//nustatoma kokiu laukeliu ieskosime ir kieno atzvilgiu pamainos ar sensoriaus isvesime grafika
	private function setSearchParameters(){
        $this->groupByDate='';
        $this->angle = array('field'=>'Sensor.name','column'=>'Sensor','as'=>'');
		$this->xLegend = __('Koridoriai');
		$this->showType = 'single';
        switch($this->primaryData['time_group']){
        	case 0://suminis, grupujuojamas pagal jutiklius, concat naudojamas tik tam, kad cakePHP imestu irasa i ta pacia vieta kaip ir sum
				if($this->angle['column']=='Sensor'){
				    if(isset($this->primaryData['combine_all_sensors']) && $this->primaryData['combine_all_sensors']){
					    $this->fieldToSearch = array('\''.__('Bendra').'\' AS xLabel');
                    }else{
                        $this->fieldToSearch[] = 'CONCAT("",'.$this->angle['field'].') AS xLabel';
                    }
					$this->globalGroupBy[] = 'xLabel';
				}else $this->fieldToSearch[] = 'CONCAT('.$this->angle['field'].') AS xLabel';
			break;
			case 1: //pamaina
				$this->groupByDate = 'Shift.start';
//				$this->fieldToSearch[] = 'CONCAT(Shift.id," ", DATE_FORMAT(Shift.start,\'%d %H:%i\')) AS xLabel';
                $this->fieldToSearch[] = 'CONCAT(DATE_FORMAT(Shift.start,\'%Y-%m-%d %H:%i\'),\' \',Shift.type) AS xLabel';
				$this->showType = 'multiple';
			break;
            case 2: //vidurkis dienoje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m-%d\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%m%d\')';
				$this->showType = 'multiple';
            break;
            case 3: //vidurkis savaiteje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%v\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%v\')';
				$this->showType = 'multiple';
            break;
            case 4: //vidurkis menesyje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%m\')';
				$this->showType = 'multiple';
            break;
            case 5: //vidurkis metuose
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m-%d\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y\')';
				$this->showType = 'multiple';
            break;
        }
        /*if(in_array($this->primaryData['time_group'], array(2,3,4)) && in_array($this->primaryData[$this->indicator], array(0,1,2,23,24))){ //savaitinis bei menesinis grupavimas ir grafiko tipas yra OEE, prieinamumo bei efektyvumo koef
            $this->fieldToSearch = array();
            $type = $this->dateTypes[$this->primaryData['time_group']][1];
            if($this->primaryData['time_group'] == 2){
                $this->fieldToSearch[] = "MAKEDATE($this->model.year, $this->model.day+1) AS xLabel";
            }else{
                $this->fieldToSearch[] = "CONCAT($this->model.year,'-',$this->model.$type) AS xLabel";
            }
            $this->groupByDate = $type;
        }*/
		//pagal grafiko laiko grupavima nurodom koks bus grupavimas bei pagal ka isvesime grafika (pamainos ar sensoriai)
		//visi nesuminiai grafikai issiveda sensoriu padalinio ir sensoriaus id atzvilgiu, todel 18 grafika ignoruojame
		if(($this->showType == 'multiple' || $this->angle['column'] == 'Shift') && $this->allowPSIdGrouping){
			if($this->angle['column'] == 'Sensor' ){
				$this->fieldToSearch[] = 'CONCAT(SUBSTR(TRIM(LEADING \'0\' FROM Sensor.name),1,12)) AS ps_id';
				$this->angle['column'] = array(0,'ps_id');
				$this->globalGroupBy[] = 'ps_id';
			}elseif($this->angle['column'] == 'Shift'){
				$this->fieldToSearch[] = 'Shift.ps_id';
				//$this->globalGroupBy[] = 'Shift.ps_id';
			}elseif($this->angle['column'] == 'Problem'){
				$this->fieldToSearch[] = 'CONCAT(Problem.name) AS problem_name';
				$this->angle['column'] = array(0,'problem_name');
				$this->globalGroupBy[] = 'problem_name';
			}
		}
        if(isset($this->primaryData['combine_all_sensors']) && $this->primaryData['combine_all_sensors']){
            $this->globalGroupBy = array();
        }
	}

	private function setGraphType(){
		$this->averageField = '/ COUNT(distinct('.$this->model.'.shift_id))';
        $this->sumMultiplier = 1;
        $this->operationFieldAs = 'sum';
		$graphId = $this->primaryData[$this->indicator];
        $this->yLegend = $this->indicators[$graphId];
		$this->angle['column'] = array(0,'ps_id');
        $this->splitColumns = array();
		switch($graphId){  //rakursas pagal ka isvedame grafika
		 	case 1:	//Darbo laikai
                $this->fieldToSearch[] = 'SUM('.$this->model.'.fact_prod_time) AS gamybos_laikas';
                $this->fieldToSearch[] = 'SUM('.$this->model.'.true_problem_time) AS prastovu_laikas';
                $this->operationField = 1;
                $this->splitColumns = array('gamybos_laikas'=>__('Gamybos laikas'), 'prastovu_laikas'=>__('Prastovų laikas'));
                $this->colors = array(__('Gamybos laikas')=>'#5AB65A', __('Prastovų laikas')=>'#D64D49');
                $this->globalGroupBy = array();
                if($this->primaryData['time_group'] == 0) {
                    $this->xLabel = 'ps_id';
                }
			break;
			case 2:	//Gamybos laikai
                $this->operationField = $this->model.'.fact_prod_time';
			break;
			case 3:	//Kiekis
                $this->operationField = $this->model.'.total_quantity';
			break;
            case 4:	//OEE
                $this->operationField = $this->model.'.oee';
                $this->fieldToSearch[] = '(SUM('.$this->model.'.count_delay) / SUM('.$this->model.'.fact_prod_time)) * (SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length)) * 100 AS sum';
			break;
			case 5:	//Prastovu kiekis
                $this->operationField = $this->model.'.true_problem_count';
			break;
			case 6:	//Prastovu laikas
                $this->operationField = $this->model.'.true_problem_time';
			break;
			case 7:	//Prieinamumo koeficientas
				$this->fieldToSearch[] = 'SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length) * 100 AS sum';
			break;
			case 8:	//Efektyvumo koeficientas
                $this->fieldToSearch[] = 'SUM('.$this->model.'.count_delay) / SUM('.$this->model.'.fact_prod_time) * 100 AS sum';
			break;
		}
	}

	private function executeSearch(){
	    $params = array(&$this->primaryData, &$this->indicator, &$this->fieldToSearch, &$this->groupByDate, &$this->model, &$this->colors);
        $this->callPluginFunction('Dashboard_beforeExecuteSearch_Hook', $params, Configure::read('companyTitle'));
		if(!array_filter($this->fieldToSearch, function($field){ return strpos(strtolower($field), 'as sum');})){
		    $averageAction = isset($this->primaryData['action']) && $this->primaryData['action']==2 && trim($this->averageField)?$this->averageField:'';
		    $this->fieldToSearch[] = '(SUM('.$this->operationField.')*'.$this->sumMultiplier.' '.$averageAction.') AS '.$this->operationFieldAs;
		}
		//$averageAction = isset($this->primaryData['action']) && $this->primaryData['action']==2 && trim($this->averageField)?"/ COUNT(distinct({$this->averageField}))":'';
		$this->attr = array(
            'fields'=>$this->fieldToSearch,
            'group' => $this->components['help']->strip_empty_elements(array_merge(array($this->groupByDate),$this->globalGroupBy)),
            'order' => $this->orderBy,
            'y_legend' => $this->yLegend,
            'x_legend' => $this->xLegend,
        );
        //fb($this->components['help']->strip_empty_elements(array_merge(array($this->groupByDate),$this->globalGroupBy)));
       // $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
        //fb($this->dataStructure);die();
//fb(array_merge($this->conditions,$this->attr));
//die();
// $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));die();
// fb($this->fieldToSearch);
// die();
//fb($this->attr);
//fb($this->conditions);
 $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
        //fb($this->dataStructure,'duomenu stuktura neapdorota');
// die();
        try{
		    $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
        }catch(Exception $e){
            pr($e);
            $this->dataStructure = array();
        }

	}

	private function setAxis(){
		$this->axis = array(
            'xAxis' => array(0=>array($this->xLabel)),
            'columns' => $this->angle['column'],
            'valueColumn'=>0,
            'showType'=>$this->showType,
        );
	}

	//Nurodo is kokiu pamainu imti duomenis, bet nenusako valdikliu ar pamainu atzvilgiu bus isvedami duomenys
	private function setShifts(){
		//if(in_array('-1',$this->primaryData['shifts']) && $this->primaryData['action'] > 0){
		if(in_array('-1',$this->primaryData['shifts'])){
			//$shiftsModel = ClassRegistry::init('Shift');
			//$this->shiftsToSearch = $shiftsModel->find('list',array('fields'=>array('Shift.ps_id','Shift.ps_id'),'order'=>array('Shift.id DESC'), 'limit'=>100, 'group'=>'Shift.ps_id'));
		}else{
			$this->shiftsToSearch = $this->primaryData['shifts'];
		}
	}

    public function start_calculation($data,$onlyData=false){
            $lineParameters = array();
			// if(!$onlyData){
                // //$this->indicator = 'indicator2';
// //                fb($data,'datada');
                // $lineParameters = $this->GetLineParameters($data);
// //                fb($lineParameters,'lineparams');
                // $this->indicator = 'indicator1';
            // }
            $this->xLabel = 'xLabel';
			$this->modelSearch = $this->fieldToSearch=$this->groupByDate=$this->xLegend=$this->angle = $this->attr= null;
			$this->conditions = $this->bind = null;
			$this->mergeData = $this->globalGroupBy = array();
			$this->allowPSIdGrouping = true;
			$this->averageField = '';
            $this->colors = array();
			$this->primaryData = $data;
            $this->setModel();
			$this->setShifts();
			$this->setConditions();
			$this->setSearchParameters();
			$this->setGraphType();
			$this->executeSearch();
			$this->setAxis();
            $options = array(
                'start' => $this->start,
                'end' => $this->end,
            );
            $pareto = 0;
            $columnsType = 0;
            $step = 0;
            $step2 = 0;
            $graphWidth = 1000;
            $legends = array();
            $legends = array(
                //'widget1Tooltip' => isset($legends['widget1Tooltip'])?$legends['widget1Tooltip']:__('Valandos')
            );
            $this->reconstructData();
           // fb($this->dataStructure);die();
            $this->set(array('data'=>$this->primaryData, 'start'=>$this->start, 'end'=>$this->end));

            $title= $this->primaryData['graph_name']."\n".$this->start.' - '.$this->end;
            $sumValues = $this->primaryData['type'] == 1?false:true;
			$logModel = ClassRegistry::init('Log');
			if(empty($this->dataStructure)){
                $logModel->write(__('no data on $this->dataStructure on table: '.$this->modelSearch->useTable).' CONDITIONS: '.json_encode(array_merge($this->conditions,$this->attr)));
                echo __('Nėra duomenų. '); return;
            }
//
//            $records = array();
//            $paretoData = NULL;
//            fb($this->mergeData,"records outer");
            if($this->primaryData['export_type'] == 0){
                $timeGroup = isset($this->primaryData['time_group'])?$this->primaryData['time_group']:0;
                if(!empty($this->dataStructure[0]) && !empty($this->dataStructure[0][0]) && is_numeric($this->dataStructure[0][0]['sum']) && $this->dataStructure[0][0]['sum'] <= -1000000) {
                    $dataStructure0 = $this->components['help']->splitMulticolumnData($this->dataStructure,$timeGroup);
                    foreach($dataStructure0 as $key=>$struct) {
                        $this->axis['columns'][1] = $key;
                        list($records,$paretoData) = $this->components['help']->collectData($struct,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues,$timeGroup);
                        $this->adjustAction($records);
                        if(!empty($this->mergeData)){
                            if($this->showType == 'single') {
                                $records[0] = array_merge($records[0],$this->mergeData);
                                $this->mergeData = $records[0];
                            }
                            else {
                                $records = array_merge($records,$this->mergeData);
                                $this->mergeData = $records;
                            }
                        }
                    }
                } else {
                    list($records,$paretoData) = $this->components['help']->collectData($this->dataStructure,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues,$timeGroup);
                    $this->adjustAction($records);
                    if(!empty($this->mergeData)){
                        if($this->showType == 'single') $records[0] = array_merge($records[0],$this->mergeData);
                        else $records = array_merge($records,$this->mergeData);
                    }
                }
                $sensorModel = ClassRegistry::init('Sensor');
                $sensorsList = implode(', ', $sensorModel->find('list', array('fields'=>array('id','name'), 'conditions'=>array('Sensor.id'=>$this->primaryData['sensors']))));
                $options = array_merge($options,array(
                    'data' => $records,
                    'selectedInfo' => $this->primaryData,
                    'paretoData' => $paretoData,
                    'columnsType' => $columnsType,
                    'step' => $step,
                    'step2' => $step2,
                    'graph_title' => $title.' ['.$sensorsList.']',
                    'y_legend' => __($this->attr['y_legend']),
                    'y2_legend' => '%',
                    'x_legend' => $this->attr['x_legend'],
                    'root_path'=> ROOT.'/app/webroot/img/graphs/',
                    'graphWidth' => $graphWidth,
                    //'limit' => sizeof($this->primaryData[key($data)]),
                    'steps' => $step,
                    'legends'=>$legends,
                    'showType'=>$this->showType,
                    'colors'=>$this->colors
                ));
                if($onlyData) return $options;
				//isvalome tuscius duomenis
                if(!isset($options['data'][0])){ //jei ne grupiniai duomenys
                	$tempData = $options['data'];
					$options['data'] = $tempData; unset($tempData);
				}else{
					$tempData = $options['data'][0];
                	array_walk($options['data'][0],function($val,$key)use(&$tempData){if($val==0){unset($tempData[$key]);}});
					$options['data'][0] = $tempData; unset($tempData);
				}
//                fb($options['data'],'optsdata');
                if(empty($options['data'])){
                	$logModel->write(__('no data on $options[data]').json_encode($options));
                    echo __('Nėra duomenų '); return true;
                }
                switch($this->primaryData['graph_type']){
                    case 0: //linijinis
                        return $this->components['googlechart']->constructGraph($options,$lineParameters);
                        //return $this->components['opengraph']->constructGraphLine($options,$lineParameters);
                    break;
                    case 1://stulpelinis
                        //return  $this->components['opengraph']->constructGraph($options,$lineParameters); //flashinis grafikas
                        return  $this->components['googlechart']->constructGraph($options,$lineParameters); //svg grafikas
                        //return  $this->components['opengraph']->constructGraph($options);
                    break;
                    case 2://skritulinis
                    	if(isset($options['data'][1])) return __('Skritulinis grafikas gali būti atvaizduojamas tik pagal suminį laiko grupavimą');
                        return  $this->components['googlechart']->constructGraph($options,$lineParameters);
                        //return  $this->components['opengraph']->constructGraphPie($options);
                    break;
                }
                die();
            }else{
                 switch($this->primaryData['export_type']){
                    case 2: //grafiko ir duomenu eksportavimas
                        list($records,$paretoData) = $this->components['help']->collectData($this->dataStructure,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues);
                        return array('data'=>$records, 'title'=>$title, 'file_name'=>$this->primaryData['file_name']);
                    break;
                    /*case 3: //duomenu strukturos eksportas
                        $title = __('Duomenų struktūros').' '.$start.' - '.$end;
                        $this->_exportDataStructures($dataStrucure,$title,$file_name,$dataTableHeaders);
                    break;*/
                }
            }
        }

	private function adjustAction(&$records){
		if(isset($this->primaryData['summarize']) && $this->primaryData['summarize']==1){
			switch($this->primaryData['action']){
				case 1:
					foreach($records as $key => $record){
						$records[$key][__('suma')] = array_sum($record);
					}
				break;
				case 2:
					foreach($records as $key => $record){
						$records[$key][''.__('vidurkis')] = round(array_sum($record) / sizeof($record),3);
					}
				break;
			}
			//pasaliname nereikalingus irasus jei parinktas "Visos pamainos" bet ne pati pamaina konkreciai ir tik tada, kai rakursas yra pamaina
			if(in_array('-1',$this->primaryData['shifts']) && $this->primaryData['action'] > 0 && sizeof($this->primaryData['shifts']) >= 2){
				foreach($records[0] as $shift_id => $record){
					if(!in_array($shift_id,$this->primaryData['shifts']) && is_numeric($shift_id)) unset($records[0][$shift_id]);
				}
			}
		}

	}

    public static function getRecordDuration()
    {
        return self::RecordDuration;
    }

	public function validateData($data){
		$errors = array();
//        fb($data,'datatata');
		if($data['indicator1'] == null){
			$errors['NodeIndicator1'] = __('Pasirinkite rodiklį y1');
		}
		if($data['graph_type'] == null){
			$errors['NodeGraphType'] = __('Būtina pasirinkti grafiko tipą');
		}
		if(empty($data['sensors'])){
			$errors['NodeSensors'] = __('Parinkite bent vieną darbo centrą');
		}
		if(empty($data['shifts'])){
			$errors['NodeShifts'] = __('Parinkite bent vieną pamainą');
		}
		if($data['date_start'] == null){
			$errors['field_start'] = __('Nurodykite laiko intervalo pradžios tašką "Nuo"');
		}
		if($data['date_end'] == null){
			$errors['field_end'] = __('Nurodykite laiko intervalo pabaigos tašką "Iki"');
		}
		if($data['indicator1'] > 0 &&  isset($data['indicator2']) && $data['indicator2'] > 0 && $data['graph_type'] == 0){
			$errors['graph_type'] = __('Jei parinkti abu rodikliai, grafiko tipas privalo būti stulpelinis');
		}
        //if($data['indicator1'] == 18 && sizeof($data['sensors'])>1){
            //$errors['only_one_shift'] = __('Darbo laikų grafike galima pasirinkti tik vieną darbo centrą');
        //}
        if($data['indicator1'] == 1 && sizeof($data['shifts'])>1){
            $errors['only_one_shift'] = __('Darbo laikų grafike galima pasirinkti tik vieną pamainą');
        }
        if($data['indicator1'] == 1 && $data['graph_type'] == 2 && $data['time_group'] != 0){
            $errors['only_one_shift'] = __('Darbo laikų grafike skritulinis tipas galimas tik su laiko grupavimu "Suminis"');
        }
        if($data['time_pattern'] == 4 && $data['time_group'] == 1){
            $errors['to_many_data'] = __('Praėjusių metų laikotarpio negalima derinti su pamaininiu grupavimu dėl per didelio duomenų kiekio');
        }
        
		return $errors;
	}

    private function reconstructData(){
        if(!empty($this->splitColumns)){
            $newDataStructure = array();
            foreach($this->dataStructure as $dataStructure){
                foreach($this->splitColumns as $splitColumnKey => $splitColumnName){
                    $dataStructure[0]['sum'] = $dataStructure[0][$splitColumnKey];
                    $dataStructure[0]['ps_id'] = $splitColumnName;
                    $newDataStructure[] = $dataStructure;
                }
            }
            $this->dataStructure = $newDataStructure;
        }
    }

}