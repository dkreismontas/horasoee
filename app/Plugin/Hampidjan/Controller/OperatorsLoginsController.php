<?php
class OperatorsLoginsController extends HampidjanAppController{

    public $uses = array('Sensor','Hampidjan.OperatorsLogin');

    public function beforeFilter(){
        $this->Auth->allow(array(
            'login'
        ));
        parent::beforeFilter();
    }

    public function login(){
        $this->layout = 'full_page';
        if ($this->request->is('post')) {
            if ($this->Auth->login()) {
                $this->redirect(array('plugin'=>'hampidjan','controller'=>'operators_logins','action'=>'login_corridors'));
            }else{
                $this->set(array('loginError'=>__('Prisijungti nepavyko, įveskite teisingus prisijungimo duomenis')));
            }
        }
    }

    public function login_corridors(){
    	$userId = $this->Auth->user()['id'];
    	if($this->request->is('post')){
    		$this->Sensor->updateAll(array('Sensor.active_user_id'=>null), array('Sensor.active_user_id'=>$userId));
    		$this->Sensor->updateAll(array('Sensor.active_user_id'=>$userId), array('Sensor.id'=>explode(',',$this->request->data['sensors']), 'Sensor.active_user_id IS NULL'));
			$this->logout();
    	}
        $this->layout = 'full_page';
		$this->Sensor->bindModel(array('belongsTo'=>array('User'=>array('foreignKey'=>false, 'conditions'=>array('User.id = Sensor.active_user_id')))));
        $corridors = $this->Sensor->find('all', array(
            'fields'=>array('Sensor.id','Sensor.name','Sensor.active_user_id','User.*','CONCAT(User.first_name, " ", User.last_name) AS user'),
            'conditions'=>array('Sensor.pin_name'=>''),
            'order'=>array('Sensor.name')
        ));
        $corridors = $this->OperatorsLogin->orderCorridors($corridors);
        $this->set(compact('corridors','userId'));
    }

	public function cancel_login(){
		$this->logout();	
		
	}
	
	private function logout(){
		$this->Session->delete('Auth.User');
        $this->Cookie->delete('login');
		$this->Auth->logout();	
		$this->redirect(array('plugin'=>'hampidjan','controller'=>'operators_logins', 'action'=>'login'));
	}

}