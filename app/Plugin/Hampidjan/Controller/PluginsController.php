<?php
App::uses('HampidjanAppController', 'Hampidjan.Controller');

class PluginsController extends HampidjanAppController {
	
    public $uses = array('Shift','Record','Sensor','Branch','MachinesRotationsLog','FoundProblemsTemp','Hampidjan.Hampidjan','DashboardsCalculation','ApprovedOrder');
    
    public function index(){
        die();
    }

    public function beforeFilter(){
        if(isset($_SERVER['PHP_AUTH_PW']) && $_SERVER['PHP_AUTH_PW'] == Configure::read('PHP_AUTH_PW')){
            $this->Auth->allow(array(
                'save_sensors_data'
            ));
        }
        parent::beforeFilter();
    }
    
    function save_sensors_data($bypass = 0){
        //if(!$bypass){ die(); }
        ob_start();
        //$dataSet = file_get_contents(__dir__.'/../webroot/files/periods.txt');
        $dataSet = file_get_contents('php://input');
        //$this->Log->write($dataSet);
        //$dataSet = $this->getPeriods();
        $dataSet = json_decode($dataSet);
        if(!is_object($dataSet)){ die(); }
//$this->Log->write($corridor->id.': '.json_encode(current($dataSet->periods)));
        $device =  trim(current(current($dataSet))->device);
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start'.$device.'.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 3600){
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
        }
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $sensors = Hash::combine($this->Sensor->find('all', array(
            'fields'=>array('Sensor.id', 'Sensor.pin_name', 'Sensor.name','Sensor.branch_id','Sensor.active_user_id','Sensor.device_ip'),
            'conditions'=>array('Sensor.port'=>$device, 'Sensor.marked'=>1),
            'order'=>array('Sensor.pin_name')
        )),'{n}.Sensor.pin_name','{n}.Sensor');
        $corridor = current(array_filter($sensors, function($sensor){
            return trim($sensor['device_ip']) && !trim($sensor['pin_name']);
        }));
        if(empty($corridor) || trim($corridor['pin_name'])){
            $this->Log->write('Nesukurtas koridoriaus sensorius dezutei: '.$device); die();
        }
        $corridor = (object)$corridor;
        $sensors = (Object)array_slice($sensors,1,null,true);
        $tmpProblems = Hash::combine($this->FoundProblemsTemp->find('all', array(
            'conditions'=>array('FoundProblemsTemp.sensor_id'=>array_column(get_object_vars($sensors),'id'))
        )),'{n}.FoundProblemsTemp.sensor_id', '{n}.FoundProblemsTemp');
        $db = ConnectionManager::getDataSource('default');
        $mailOfErrors = array();
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $confirm=array();
        $tz = new DateTimeZone('Europe/Vilnius');
        $this->Record->bindModel(array('belongsTo'=>array('Shift')));
        $lastRecord = $this->Record->find('first', array('conditions'=>array('Record.sensor_id'=>$corridor->id),'order'=>array('Record.id DESC')));
        $shiftChanges = 0;
		if(!empty($lastRecord)){
			$prevShift = $this->Shift->find('first',array('conditions'=>array('Shift.id <'=>$lastRecord['Record']['shift_id']), 'order'=>array('Shift.start DESC')));
			$this->Hampidjan->transferShiftRotations($prevShift['Shift']);
		}
        foreach($dataSet->periods as $period){
            $quantity = 0;
            $rotations = array();
            $tmpProblemsUpdate = array();
            $newTmpProblemsCreate = array();
            $createdFrom = new DateTime($period->time_from);
            $createdFrom->setTimezone($tz);
            $createdTo = new DateTime($period->time_to);
            $createdTo->setTimezone($tz);
            if($createdTo < new DateTime(date('Y-m-d H:i:s', strtotime('-1 WEEK')))){ continue; }
            if(!empty($this->Record->find('first',array('conditions'=>array('Record.created'=>$createdTo->format('Y-m-d H:i:s'), 'Record.sensor_id'=>$corridor->id))))){
                $confirm[] = $period->time_to;
                continue;
            }
            $currentShift = $this->Shift->findCurrent($corridor->branch_id, $createdTo);

            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                $currentShift = $this->Shift->findCurrent($corridor->branch_id, $createdTo);
            }
            if(!empty($lastRecord) && $lastRecord['Record']['shift_id'] != $currentShift['Shift']['id']){
                $shiftChanges = $lastRecord['Record']['shift_id'];
                $this->Sensor->updateAll(array('active_user_id'=>null), array('Sensor.id'=>$corridor->id));
            }
            foreach($period as $pin => $record){
                if(!isset($sensors->$pin)) continue;
                $sensor = (object)$sensors->$pin;
				
                if(!empty($record->timestamps)){
                    $this->Hampidjan->completeTmpProblem($tmpProblems,$sensor,$corridor);
                    foreach($record->timestamps as $rotationTime) {
                        $date = new DateTime($rotationTime);
                        $date->setTimezone($tz);
                        $rotationTime = $date->format('Y-m-d H:i:s');
                        $rotations[] = array(
                            'corridor_name' => $corridor->name,
                            'machine_sensor_name' => $sensor->name,
                            'created' => $rotationTime
                        );
                    }
                }else{//kuriame/pratesiame prastova per tarpine lentele
                    if($this->Hampidjan->extendTmpProblem($tmpProblems,$sensor,$currentShift,$corridor)){
                        $tmpProblemsUpdate[$sensor->id] = $createdTo->format('Y-m-d H:i:s');
                    }elseif(empty($this->FoundProblemsTemp->find('first', array('conditions'=>array('FoundProblemsTemp.sensor_id'=>$sensor->id))))){
                    	$currentOrder = $this->ApprovedOrder->findCurrent($corridor->id);
                        $newData = array(
                            'sensor_id'=>$sensor->id,
                            'shift_id'=>$currentShift['Shift']['id'],
                            'problem_start'=>$createdFrom < new DateTime($currentShift['Shift']['start'])?$currentShift['Shift']['start']:$createdFrom->format('Y-m-d H:i:s'),
                            'problem_end'=>$createdTo->format('Y-m-d H:i:s'),
                            'plan_id' => $currentOrder['ApprovedOrder']['plan_id']
                        );
                        $newTmpProblemsCreate[] = $newData;
                        $tmpProblems[$sensor->id] = $newData;
                    }
                }
                $quantity += $record->count;
            }
            try{
                $query = 'CALL ADD_FULL_RECORD(\''.$createdTo->format('Y-m-d H:i:s').'\', '.$corridor->id.', '.$quantity.','.Configure::read('recordsCycle').');';
                $db->query($query);
                if(!empty($rotations)){ $this->MachinesRotationsLog->saveAll($rotations); }
                if(!empty($newTmpProblemsCreate)){ $this->FoundProblemsTemp->saveAll($newTmpProblemsCreate); }
                if(!empty($tmpProblemsUpdate)){
                    $tmpProblemsUpdateString = '';
                    foreach($tmpProblemsUpdate as $sensorId => $end){
                        $tmpProblemsUpdateString .= ' WHEN '.$sensorId.' THEN \''.$end.'\'';
                    }
                    $this->FoundProblemsTemp->updateAll(
                        array('FoundProblemsTemp.problem_end'=>'CASE FoundProblemsTemp.sensor_id'.$tmpProblemsUpdateString.' END'),
                        array('FoundProblemsTemp.sensor_id'=>array_keys($tmpProblemsUpdate))
                    );
                }
                $confirm[] = $period->time_to;
            }catch(Exception $e){
                $this->Log->write('Nepavyko irasyti jutiklio duomenu. Vykdyta uzklausa: '.$query.'. Uzklausos klaida: '.json_encode($e));
                $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
            }
        }
        unlink($dataPushStartMarkerPath);
        $this->Sensor->moveOldRecords();
        if($shiftChanges > 0){
            $this->calcOee($shiftChanges, $corridor->id);
        }
        session_destroy();
        $systemWarnings = ob_get_clean();
        if(trim($systemWarnings)){
            $this->Log->write('Duomenu irasymo metu sistemoje gauti pranesimai: '.$systemWarnings);
        }
        echo json_encode(array('confirm'=>$confirm));
        die();
    }

    private function calcOee($shiftId, $corridorId){
        $markerpath = __dir__.'/../webroot/files/dashboard_calculations_started_'.$corridorId.'.txt';
        if(file_exists($markerpath) && time() - filemtime($markerpath) < 600 ){ return false; }
        $oeeCalculated = $this->DashboardsCalculation->find('first', array('conditions'=>array('DashboardsCalculation.shift_id'=>$shiftId)));
        if(!$oeeCalculated){
            $this->Log->write('Apskaiciuojamas OEE praejusiai pamainai. Pamainos ID: '.$shiftId);
            $myfile = @fopen($markerpath, "w");
            set_time_limit(2000);
            App::import('Console/Command', 'AppShell');
            App::import('Console/Command', 'OeeShell');
            $shell = new OeeShell();
            $shell->sensorsIds = array($corridorId);
            $out = $shell->main();
            fclose($myfile);
            unlink($markerpath);
        }
    }

    public function check_records_is_pushing(){
        $this->Sensor->informAboutProblems();
        die();
    }

}
