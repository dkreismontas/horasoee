<?php
App::uses('ApprovedOrder','Model');
class CorridorsController extends HampidjanAppController{

    public $uses = array('Shift','OrderCalculation', 'Record','ApprovedOrder','MachinesRotationsLog','Hampidjan.Corridor');

    public function generate_corridors_jsons(){
        $this->Sensor->bindModel(array(
            'belongsTo'=>array(
                'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.status_id = '.ApprovedOrder::STATE_IN_PROGRESS.' AND ApprovedOrder.sensor_id = Sensor.id')),
                'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
                'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
                'User'=>array('foreignKey'=>false, 'conditions'=>array('User.id = Sensor.active_user_id'))
            )
        ));
        $corridors = $this->Sensor->find('all', array(
            'fields'=>array(
                'Sensor.id', 'Sensor.branch_id', 'Sensor.name', 'Sensor.port', 'Sensor.picks_speed', 'Sensor.device_ip', 'Sensor.perimeter',
                'Product.production_code',
                'User.id', 'User.first_name', 'User.last_name', 'User.username'
            ),'conditions'=>array('Sensor.pin_name'=>'', 'Sensor.device_ip <>'=>''),
            'order'=>array('Sensor.name')
        ));
        $corridorsIds = Set::extract('/Sensor/id', $corridors);
        $branchId = current($corridors)['Sensor']['branch_id'];
        $currentShift = $this->Shift->findCurrent($branchId);
        $prevShift = $this->Shift->getPrev($currentShift['Shift']['start'],$branchId);
        $prevShiftOrdersIds = Set::extract('/0/order_id',$this->OrderCalculation->find('all', array(
            'fields'=>array('MAX(OrderCalculation.approved_order_id) as order_id'),
            'conditions'=>array('OrderCalculation.sensor_id'=>$corridorsIds, 'OrderCalculation.shift_id'=>$prevShift['Shift']['id']??0),
            'group'=>array('OrderCalculation.sensor_id')
        )));
        $this->OrderCalculation->bindModel(array('belongsTo'=>array(
            'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = OrderCalculation.approved_order_id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
            'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
            'User'=>array('foreignKey'=>false, 'conditions'=>array('User.id = OrderCalculation.user_id')),
            'DashboardsCalculation'=>array('foreignKey'=>false, 'conditions'=>array('DashboardsCalculation.shift_id = '.$prevShift['Shift']['id'].' AND ApprovedOrder.sensor_id = DashboardsCalculation.sensor_id'))
        )));
        $prevShiftOrders = Hash::combine($this->OrderCalculation->find('all', array(
            'fields'=>array(
                'Product.production_code',
                'OrderCalculation.sensor_id',
                'User.id', 'User.first_name', 'User.last_name', 'User.username',
                'DashboardsCalculation.oee',
                'DashboardsCalculation.total_quantity',
            ),
            'conditions'=>array(
                'OrderCalculation.approved_order_id'=>$prevShiftOrdersIds,
                'OrderCalculation.sensor_id'=>$corridorsIds,
                'OrderCalculation.shift_id'=>$prevShift['Shift']['id']
            ),'order'=>array('OrderCalculation.order_start DESC')
        )),'{n}.OrderCalculation.sensor_id', '{n}');
        $this->Record->bindModel(array(
            'belongsTo'=>array(
                'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = Record.approved_order_id')),
                'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
            )
        ));
        $recordsCounts = Hash::combine($this->Record->find('all', array(
            'fields'=>array(
                'SUM(IF(Record.created > \''.date('Y-m-d H:i:s', strtotime('-1 hour')).'\', Record.unit_quantity / Plan.step, 0)) AS last_hour_effectivity',
                'MIN(IF(Record.approved_order_id > 0, Record.approved_order_id, 9999999999999)) AS first_approved_order_id',
                'SUM(Record.unit_quantity) AS unit_quantity',
                'Record.sensor_id'
            ),'conditions'=>array(
                'Record.sensor_id'=>$corridorsIds,
                'Record.shift_id'=>$currentShift['Shift']['id'],
            ),'group'=>array('Record.sensor_id')
        )),'{n}.Record.sensor_id', '{n}.0');
        $this->ApprovedOrder->bindModel(array(
            'belongsTo'=>array('Plan', 'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')))
        ));
        $firstApprovedOrders = Hash::combine($this->ApprovedOrder->find('all', array(
            'fields'=>array('ApprovedOrder.sensor_id', 'Plan.*', 'Product.*'),
            'conditions'=>array(
                'ApprovedOrder.id'=>Set::extract('/first_approved_order_id', $recordsCounts)
            )
        )),'{n}.ApprovedOrder.sensor_id', '{n}');
        array_walk($recordsCounts, function(&$record, $corridorId)use($firstApprovedOrders){
            $record['FirstPlanInShift'] = $firstApprovedOrders[$corridorId]??array();
        });
        $this->Sensor->bindModel(array('belongsTo'=>array(
            'FoundProblemsTemp' => array('foreignKey'=>false, 'conditions'=>array('Sensor.id = FoundProblemsTemp.sensor_id'))
        )));
        $pins = $this->Sensor->find('all', array(
            'fields'=>array(
                'Sensor.id', 'Sensor.port', 'Sensor.name', 'Sensor.marked',
                'FoundProblemsTemp.id'
            ),'conditions'=>array('Sensor.pin_name <>'=>''),
            'order'=>array('Sensor.name')
        ));
        $rotationsCountInShift = Hash::combine($this->MachinesRotationsLog->find('all', array(
            'fields'=>array(
                'COUNT(MachinesRotationsLog.machine_sensor_name) AS rotations_count',
                'MachinesRotationsLog.machine_sensor_name'
            ),
            'conditions'=>array(
                'MachinesRotationsLog.created >=' => $currentShift['Shift']['start']
            ),'group'=>array('MachinesRotationsLog.machine_sensor_name')
        )),'{n}.MachinesRotationsLog.machine_sensor_name', '{n}.0.rotations_count');
        $jsonViews = $this->Corridor->generateCorridorsJsons($corridors, $currentShift, $prevShift, $prevShiftOrders, $recordsCounts, $pins, $rotationsCountInShift);
        die();
    }

}