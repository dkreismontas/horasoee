<?php
App::uses('ApprovedOrder', 'Model');
class CorridorsOperationsController extends HampidjanAppController{

    public $uses = array('Sensor','ApprovedOrder','Product', 'Plan','FoundProblemsTemp','Hampidjan.Hampidjan');

    public function get_products($returnAsJson = true){
        $producModel = ClassRegistry::init('Product');
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $approvedOrder = $this->ApprovedOrder->findCurrent($this->request->data['Sensor']['id']);
        $allProducts = $producModel->find('all', array(
            'fields'=>array(
                'CONCAT(Product.name,\' (\',Product.production_code,\')\') AS name',
                'TRIM(Product.id) as id',
            ),'conditions'=>array(
                'Product.id <>'=>!empty($approvedOrder)?$approvedOrder['Plan']['product_id']:0,
                'OR'=>array(
                    'TRIM(Product.work_center)'=>'',
                    'TRIM(Product.work_center)'=>0,
                    'FIND_IN_SET('.$this->request->data['Sensor']['id'].', work_center)'
                )
            )));
        array_walk($allProducts, function(&$product){ $product = $product[0]; });
        if($returnAsJson){
            echo json_encode($allProducts);die();
        }else{ return $allProducts; }
    }

    public function start_production(){
        if(!$this->request->is('ajax')){ die(); }
        $corridor = $this->Sensor->findById($this->request->data['corridor']['id']);
        $corridorPins = $this->Sensor->find('all', array(
            'fields'=>array(
                'Sensor.id'
            ),'conditions'=>array('Sensor.pin_name <>'=>'', 'Sensor.port'=>ltrim($corridor['Sensor']['name'],'0'), 'Sensor.marked'=>1),
            'order'=>array('Sensor.name')
        ));
        $tmpProblems = Hash::combine($this->FoundProblemsTemp->find('all', array(
            'conditions'=>array('FoundProblemsTemp.sensor_id'=>Set::extract('/Sensor/id', $corridorPins))
        )),'{n}.FoundProblemsTemp.sensor_id', '{n}.FoundProblemsTemp');
        $endDate = new DateTime();
        foreach($corridorPins as $corridorPin){
            $this->Hampidjan->completeTmpProblem($tmpProblems,(Object)$corridorPin['Sensor'],(Object)$corridor['Sensor'], $endDate);
        }
        $product = $this->Product->findById($this->request->data['product']['id']);
        $step = bcmul(
                    bcmul(
                        bcmul(
                            bcmul(
                                bcdiv($corridor['Sensor']['picks_speed'], $product['Product']['picks'] * 10, 6),
                                60,
                                6),
                            bcdiv($product['Product']['dens'], 100000, 6),
                            6),
                        sizeof($corridorPins),
                        6),
                    $product['Product']['utilization'],
                    6
                );
        $fPlan = array(
            'sensor_id'=>$corridor['Sensor']['id'],
            'production_name'=>$product['Product']['name'],
            'production_code'=>$product['Product']['production_code'],
            'mo_number'=>$corridor['Sensor']['id']."_".uniqid(),
            'product_id'=>$product['Product']['id'],
            'box_quantity'=>bcmul($corridor['Sensor']['perimeter'], bcdiv($product['Product']['dens'], 100000, 6), 6),
            'disabled'=>1,
            'step'=>$step,
            'preparation_time'=>isset($product['Product']['preparation_time'])?$product['Product']['preparation_time']:0
        );
        // $approvedOrder = $this->ApprovedOrder->findCurrent($this->request->data['Sensor']['id']);
        $this->ApprovedOrder->findAndComplete($corridor, $endDate);
        $this->Plan->save($fPlan);
        $this->ApprovedOrder->create();
        $appOrder = array(
            'created' => $endDate->format('Y-m-d H:i:s'),
            'end' => $endDate->format('Y-m-d H:i:s'),
            'sensor_id' => $corridor['Sensor']['id'],
            'plan_id' => $this->Plan->id,
            'status_id' => ApprovedOrder::STATE_IN_PROGRESS,
        );
        $this->ApprovedOrder->save($appOrder);
        die();
    }

}