<?php
App::uses('ApprovedOrder','Model');
class InfosController extends HampidjanAppController {

    public $uses = array('Sensor','OperatorsLogin','Hampidjan.Info','Shift');

    public function get_states_and_counts(){
        if($this->request->is('ajax')){
            $this->Sensor->bindModel(array(
                'belongsTo'=>array(
                    'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.status_id = '.ApprovedOrder::STATE_IN_PROGRESS.' AND ApprovedOrder.sensor_id = Sensor.id')),
                    'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
                    'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id'))
                )
            ));
            $corridors = $this->Sensor->find('all', array(
                'fields'=>array(
                    'Sensor.id','Sensor.name','Product.production_code','Sensor.branch_id','Plan.step',
                    'CONCAT(Product.name,\' (\',Product.production_code,\')\') AS product_full_name'
                ),
                'conditions'=>array('Sensor.pin_name'=>''),
                'order'=>array('Sensor.name')
            ));
            $corridors = $this->OperatorsLogin->orderCorridors($corridors);
            $this->Info->attachShiftsQuantities($corridors);
            echo json_encode(array(
                'corridors'=>$corridors,
                'totals'=>$this->Info->totalsInfo,
            ));
            die();
        }
        $this->set(array('loadCorridorsUrl'=>'/hampidjan/infos/get_states_and_counts'));
    }

    public function get_states_and_counts_currshift(){
        if($this->request->is('ajax')){
            $this->Sensor->bindModel(array(
                'belongsTo'=>array(
                    'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.status_id = '.ApprovedOrder::STATE_IN_PROGRESS.' AND ApprovedOrder.sensor_id = Sensor.id')),
                    'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
                    'Product'=>array('foreignKey'=>false, 'conditions'=>array('Plan.product_id = Product.id')),
                    'User'=>array('foreignKey'=>false, 'conditions'=>array('User.id = Sensor.active_user_id'))
                )
            ));
            $corridors = $this->Sensor->find('all', array(
                'fields'=>array(
                    'Sensor.id','Sensor.name','Product.production_code','Sensor.branch_id','Sensor.port','Plan.step',
                    'SUBSTR(CONCAT(SUBSTR(User.first_name,1,1), \'. \',User.last_name),1, 16) AS user_name',
                    'CONCAT(Product.name,\' (\',Product.production_code,\')\') AS product_full_name'
                ),'conditions'=>array('Sensor.pin_name'=>''),
                'order'=>array('Sensor.name ASC')
            ));
            $this->Sensor->bindModel(array('belongsTo'=>array(
                'FoundProblemsTemp' => array('foreignKey'=>false, 'conditions'=>array('Sensor.id = FoundProblemsTemp.sensor_id'))
            )));
            $pins = $this->Sensor->find('all', array(
                'fields'=>array(
                    'Sensor.id', 'Sensor.port', 'Sensor.name', 'Sensor.marked', 'Sensor.pin_name',
                    'FoundProblemsTemp.id','FoundProblemsTemp.problem_end'
                ),'conditions'=>array('Sensor.pin_name <>'=>''),
                'order'=>array('Sensor.name'),
                'group'=>array('Sensor.id')
            ));
            $corridors = $this->OperatorsLogin->orderCorridors($corridors);
            $this->Info->attachShiftsQuantities($corridors,true);
            $this->Info->attachPins($corridors, $pins);
            echo json_encode(array(
                'corridors'=>$corridors,
                'totals'=>$this->Info->totalsInfo,
            ));
            die();
        }
        $this->set(array('loadCorridorsUrl'=>'/hampidjan/infos/get_states_and_counts_currshift'));
    }

}