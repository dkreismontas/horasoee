<?php
class ReportsController extends HampidjanAppController{

    public $uses = array('Hampidjan.Report','Shift');

    public function generate(){
        if(!$this->request->is('post')){ die(); }
        //$date = $this->request->data['date'];

        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        $type = $this->request->data['view_angle'];
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()->setTitle(__("Gamyba pagal darbuotoją").' '.$date_start.' - '.$date_end);
        $objPHPExcel->setActiveSheetIndex(0);
        $shifts = Hash::combine($this->Shift->find('all', array(
            'fields'=>array('Shift.id', 'Shift.start', 'Shift.type'),
            //'conditions'=>array('DATE_FORMAT(Shift.start,"%Y-%m-%d")'=>$date),
            'conditions'=>array(
                'Shift.start >='=>$date_start,
                'Shift.start <'=>$date_end.' 23:59:59',
            ),'order'=>array('Shift.start')
        )),'{n}.Shift.id', '{n}.Shift');
        $date_start = date('Y-m-d', strtotime($date_start));
        $date_end = date('Y-m-d', strtotime($date_end));
        switch($type){
            case 0: //gamyba pagal darbuotoja
                $data = $this->Report->getWorkersReport($shifts);
                $this->set(compact('data','objPHPExcel','shifts','date_start','date_end'));
                $this->render('workers_report');
            break;
            case 1: //gamyba pagal gamini
                $data = $this->Report->getOrdersReport($shifts);
                $this->set(compact('data','objPHPExcel','shifts','date_start','date_end'));
                $this->render('orders_report');
            break;
            case 2: //irenginiu sustojimai
                list($ordersByShift, $totalOrders) = $this->Report->getSensorsDowntimes($shifts);
                $this->set(compact('ordersByShift','totalOrders','objPHPExcel','shifts','date_start','date_end'));
                $this->render('downtimes_report');
            break;
        }
    }

}