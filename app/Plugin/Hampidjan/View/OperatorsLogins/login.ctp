<?php
$this->Html->css('Hampidjan.operators_logins',null,array('inline'=>false));
?>
<div class="container">
    <div class="padding-top-bottom">

    </div>
    <div class="row">
        <div class="col-xs-5" id="buttons">
            <div class="row">
                <div class="col-xs-4"><button value="1">1</button></div>
                <div class="col-xs-4"><button value="2">2</button></div>
                <div class="col-xs-4"><button value="3">3</button></div>
            </div>
            <div class="row">
                <div class="col-xs-4"><button value="4">4</button></div>
                <div class="col-xs-4"><button value="5">5</button></div>
                <div class="col-xs-4"><button value="6">6</button></div>
            </div>
            <div class="row">
                <div class="col-xs-4"><button value="7">7</button></div>
                <div class="col-xs-4"><button value="8">8</button></div>
                <div class="col-xs-4"><button value="9">9</button></div>
            </div>
            <div class="row">
                <div class="col-xs-4"><button class="smaller-items" value="del">Del</button></div>
                <div class="col-xs-4"><button value="0">0</button></div>
                <div class="col-xs-4"><button class="smaller-items" value="ok">Ok</button></div>
            </div>
        </div>
        <div class="col-xs-7" id="inputs">
            <form action="" method="post" id="login-form">
            <table>
                <tr><td></td></tr>
                <tr><td><label><?php echo __('Tabelio numeris'); ?>:</label><input type="text" id="tab" name="data[User][username]" /></td></tr>
                <tr><td><label><?php echo __('PIN'); ?>:</label><input type="password" id="pin" name="data[User][password]" /></td></tr>
                <tr><td><?php if(isset($loginError)): ?><h2 class="warning"><?php echo $loginError; ?></h2><?php endif; ?></td></tr>
            </table>
                <input type="submit" style="display:none;" />
            </form>
        </div>
    </div>
    <div class="padding-top-bottom"></div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function($){
        let focusInput;
        $('#inputs input').bind('click', function(e){
            focusInput = $(this);
        });
        $('input#tab').trigger('click');
        $('#buttons button').bind('click', function(e){
            e.preventDefault();
            if($(this).val() == 'ok'){
                $('form#login-form').submit();
            }else if($(this).val() == 'del'){
                focusInput.val(focusInput.val().slice(0,-1));
            }else {
                focusInput.val(focusInput.val() + $(this).val());
            }
        });
    });
</script>