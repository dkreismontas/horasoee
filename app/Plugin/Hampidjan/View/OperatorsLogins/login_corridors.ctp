<?php
$this->Html->css('Hampidjan.corridors_login',null,array('inline'=>false));
?>
<div class="container">
    <div class="row">
        <div class="col-xs-2" id="actionButtons">
            <?php echo $this->Html->link(_('Atšaukti'), array('plugin'=>'hampidjan','controller'=>'operators_logins', 'action'=>'cancel_login'), array('class'=>'btn btn-primary')); ?>
            <form action="<?php echo Router::url(array('plugin'=>'hampidjan','controller'=>'operators_logins', 'action'=>'login_corridors')); ?>" method="post">
            	<input type="hidden" name="sensors" value="" />
            	<button class="btn btn-primary" id="confirm"><?php echo _('OK'); ?></button>
            </form>
        </div>
        <div class="col-xs-10">
            <?php 
            $userOwnsCorridors = array();
            foreach($corridors as $letter => $sameLetterGroup){ ?>
                <div class="row corridorsGroup">
                    <?php foreach($sameLetterGroup as $corridor){
                    $class = $corridor['Sensor']['active_user_id'] > 0?'used':'';
					if($corridor['Sensor']['active_user_id'] == $userId){
						$class = 'reserved';
						$userOwnsCorridors[] = $corridor['Sensor']['id'];
					} 
                    ?>
                        <div class="col-xs-1 corridor <?php echo $class; ?>" data-sensor_id="<?php echo $corridor['Sensor']['id']; ?>">
                            <p><?php echo $corridor['Sensor']['name']; ?></p>
                            <p><?php echo $corridor[0]['user']; ?></p>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
<script type="text/javascript">
	jQuery(document).ready(function($){
		let reservedSensors = [<?php echo implode(',', $userOwnsCorridors) ?>];
		console.log(reservedSensors)
		$('.corridor').bind('click', function(){
			if($(this).hasClass('used')){ return false; }
			$(this).toggleClass('reserved');
			if($(this).hasClass('reserved')){
				reservedSensors.push($(this).data('sensor_id'));
			}else{
				let index = reservedSensors.indexOf($(this).data('sensor_id'));
				if (index != -1) {
				    reservedSensors.splice(index, 1);
				}
			}
		});
		
		$('button#confirm').bind('click', function(e){
			e.preventDefault();
			if(reservedSensors.length == 0){
				alert('<?php echo __('Pasirinkite bent vieną koridorių'); ?>');
				return false;
			}
			$(this).closest('form').find('input[name="sensors"]').val(reservedSensors);
			$(this).closest('form').submit();
		});
	});
</script>
