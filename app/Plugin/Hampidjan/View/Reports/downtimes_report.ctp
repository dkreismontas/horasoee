<?php
ob_end_clean();
error_reporting(0);
$styleArray = array(
    'borders' => array('outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => '000000'),
    ), ),
    'font' => array(
        'size' => 9,
        'name' => 'Times New Roman'
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
$headerStyleArray = array(
    'borders' => array('outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => '000000'),
    ), ),
    'font' => array(
        'size' => 9,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

//HEADERS
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Visi duomenys'), false);
$colNr = 0; $rowNr = 2;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Įrenginių sustojimai'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $date)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
$rowNr += 2; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Data'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamaina'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Koridorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Įrenginio nr.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
for($i = 1; $i <= 22; $i++){
    $pinsColumns[$i] = $colNr;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $i)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
}
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->mergeCells(nts(0).'2:'.nts($colNr-1).'2');
$activeSheet->mergeCells(nts(0).'3:'.nts($colNr-1).'3');
$rowNr++;
foreach($ordersByShift as $shiftId => $shiftsIds) {
    foreach($shiftsIds as $corridorName => $productsNames) {
        foreach($productsNames as $productName => $pins) {
            $colNr = 0;
            $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, date('Y-m-d',strtotime($shifts[$shiftId]['start'])))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
            $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $shifts[$shiftId]['type'])->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
            $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $corridorName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
            $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $productName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Sustojimų kiekis, vnt.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr+1, __('Sustojimų trukmė, min.'))->getStyle(cts($colNr, $rowNr+1))->applyFromArray($styleArray);$colNr++;
            $totals['downtimes_count'] = $totals['downtime_duration'] = 0;
            foreach ($pins as $pinNr => $downtimes) {
                $downtimesDuration = bcdiv($downtimes['downtime_duration'],60,2);
                $activeSheet->setCellValueByColumnAndRow($pinsColumns[$pinNr], $rowNr, $downtimes['downtimes_count'])->getStyle(cts($pinsColumns[$pinNr], $rowNr))->applyFromArray($styleArray);
                $activeSheet->setCellValueByColumnAndRow($pinsColumns[$pinNr], $rowNr+1, $downtimesDuration)->getStyle(cts($pinsColumns[$pinNr], $rowNr+1))->applyFromArray($styleArray);
                $totals['downtime_duration'] = bcadd($downtimesDuration, $totals['downtime_duration'], 6);
                $totals['downtimes_count'] += $downtimes['downtimes_count'];
                $colNr++;
            }
            $activeSheet->setCellValueByColumnAndRow(max($pinsColumns)+1, $rowNr, $totals['downtimes_count'])->getStyle(cts(max($pinsColumns)+1, $rowNr))->applyFromArray($styleArray);
            $activeSheet->setCellValueByColumnAndRow(max($pinsColumns)+1, $rowNr+1, $totals['downtime_duration'])->getStyle(cts(max($pinsColumns)+1, $rowNr+1))->applyFromArray($styleArray);
            $rowNr += 2;
        }
    }
}
$col = 0;
$activeSheet->getColumnDimension(nts($col++))->setWidth(15);
$activeSheet->getColumnDimension(nts($col++))->setWidth(10);
$activeSheet->getColumnDimension(nts($col++))->setWidth(10);
$activeSheet->getColumnDimension(nts($col++))->setWidth(20);
$activeSheet->getColumnDimension(nts($col++))->setWidth(20);


$activeSheet = $objPHPExcel->createSheet(1);
$activeSheet->setTitle(__('Bendras'), false);
$colNr = 0; $rowNr = 2;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Įrenginių sustojimai'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $date)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
$rowNr += 2; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Koridorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Įrenginio nr.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
for($i = 1; $i <= 22; $i++){
    $pinsColumns[$i] = $colNr;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $i)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
}
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->mergeCells(nts(0).'2:'.nts($colNr-1).'2');
$activeSheet->mergeCells(nts(0).'3:'.nts($colNr-1).'3');
$rowNr++;
foreach($totalOrders as $corridorName => $productsNames) {
    foreach($productsNames as $productName => $pins) {
        $colNr = 0;
        $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $corridorName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
        $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr).($rowNr+1));
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $productName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Sustojimų kiekis, vnt.'))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr+1, __('Sustojimų trukmė, min.'))->getStyle(cts($colNr, $rowNr+1))->applyFromArray($styleArray);$colNr++;
        $totals['downtimes_count'] = $totals['downtime_duration'] = 0;
        foreach ($pins as $pinNr => $downtimes) {
            $downtimesDuration = bcdiv($downtimes['downtime_duration'],60,2);
            $activeSheet->setCellValueByColumnAndRow($pinsColumns[$pinNr], $rowNr, $downtimes['downtimes_count'])->getStyle(cts($pinsColumns[$pinNr], $rowNr))->applyFromArray($styleArray);
            $activeSheet->setCellValueByColumnAndRow($pinsColumns[$pinNr], $rowNr+1, $downtimesDuration)->getStyle(cts($pinsColumns[$pinNr], $rowNr+1))->applyFromArray($styleArray);
            $totals['downtime_duration'] = bcadd($downtimesDuration, $totals['downtime_duration'], 6);
            $totals['downtimes_count'] += $downtimes['downtimes_count'];
            $colNr++;
        }
        $activeSheet->setCellValueByColumnAndRow(max($pinsColumns)+1, $rowNr, $totals['downtimes_count'])->getStyle(cts(max($pinsColumns)+1, $rowNr))->applyFromArray($styleArray);
        $activeSheet->setCellValueByColumnAndRow(max($pinsColumns)+1, $rowNr+1, $totals['downtime_duration'])->getStyle(cts(max($pinsColumns)+1, $rowNr+1))->applyFromArray($styleArray);
        $rowNr += 2;
    }
}
$col = 0;
$activeSheet->getColumnDimension(nts($col++))->setWidth(10);
$activeSheet->getColumnDimension(nts($col++))->setWidth(20);
$activeSheet->getColumnDimension(nts($col++))->setWidth(20);

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Įrenginių sustojimai') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}