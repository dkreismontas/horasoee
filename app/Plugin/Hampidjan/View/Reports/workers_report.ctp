<?php
ob_end_clean();
error_reporting(0);
$styleArray = array(
    'borders' => array('outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => '000000'),
    ), ),
    'font' => array(
        'size' => 9,
        'name' => 'Times New Roman'
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    )
);
$headerStyleArray = array(
    'borders' => array('outline' => array(
        'style' => PHPExcel_Style_Border::BORDER_THIN,
        'color' => array('argb' => '000000'),
    ), ),
    'font' => array(
        'size' => 9,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

//HEADERS
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Visi duomenys'), false);
$colNr = 0; $rowNr = 2;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamyba pagal darbuotoją'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $date_start.' - '.$date_end)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
$rowNr += 2; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Data'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamaina'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Koridorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Operatorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagamintas kiekis'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->mergeCells(nts(0).'2:'.nts($colNr-1).'2');
$activeSheet->mergeCells(nts(0).'3:'.nts($colNr-1).'3');
$rowNr++;
$totals = array();
foreach($data as $rec) {
    //if($rec[0]['quantity'] == 0){ continue; }
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, date('Y-m-d',strtotime($rec['Shift']['start'])))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $rec['Shift']['type'])->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $rec['Sensor']['name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $rec[0]['production_code'])->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $rec[0]['user'])->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($rec[0]['quantity'], 4))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray);$colNr++;
    $rowNr++;
    $totals[$rec['Sensor']['name']][$rec[0]['production_code']][$rec[0]['user']] = $totals[$rec['Sensor']['name']][$rec[0]['production_code']][$rec[0]['user']]??0;
    $totals[$rec['Sensor']['name']][$rec[0]['production_code']][$rec[0]['user']] += $rec[0]['quantity'];
}
for ($col = 1; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth($col%3==0?10:20);
}

//Bendras
$activeSheet = $objPHPExcel->createSheet(1);
$activeSheet->setTitle(__('Bendras'), false);
$colNr = 0; $rowNr= 2;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamyba pagal darbuotoją'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $date_start.' - '.$date_end)->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$colNr = 0; $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Koridorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Operatorius'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagamintas kiekis'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->mergeCells(nts(0).'2:'.nts($colNr-1).'2');
$activeSheet->mergeCells(nts(0).'3:'.nts($colNr-1).'3');
$totalQuantity = 0;
ksort($totals);
foreach($totals as $corridorName => $corridorsData){
    foreach($corridorsData as $productName => $productsData) {
        foreach ($productsData as $workerName => $quantity) {
            $colNr = 0; $rowNr++;
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $corridorName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $productName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $workerName)->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, round($quantity,4))->getStyle(cts($colNr, $rowNr))->applyFromArray($styleArray); $colNr++;
            $totalQuantity = bcadd($totalQuantity,$quantity,6);
        }
    }
}
$rowNr++;
$activeSheet->setCellValueByColumnAndRow(2, $rowNr, __('Viso'))->getStyle(cts(2, $rowNr))->applyFromArray($styleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow(3, $rowNr, round($totalQuantity,2))->getStyle(cts(3, $rowNr))->applyFromArray($styleArray); $colNr++;
for ($col = 1; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth($col%3==0?10:20);
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Gamyba pagal darbuotoją') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}