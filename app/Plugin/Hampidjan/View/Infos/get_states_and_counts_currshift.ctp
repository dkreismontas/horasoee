<?php
$this->Html->script('vue', array('inline'=>false));
$this->Html->css('Hampidjan.style',null,array('inline'=>false));
?>
<div id="corridors">
    <div class="row currshift totals">
        <div v-for="(shiftName, count) in totals">
            <div class="corridor key"><div><h3>{{shiftName}}</h3></div></div>
            <div class="corridor value"><div>{{count.toFixed(2)}} kg</div></div>
        </div>
    </div>
    <div class="row" v-for="corridorsLetters in corridors">
        <corridor_comp v-for="(corridorId, corridor) in corridorsLetters" :corridor="corridor"></corridor_comp>
    </div>
    <div id="productChoose" class="popup" :class="{hidden: !productChoosePopup}">
        <h3><?php echo __('Šiuo metu koridoriuje'); ?> {{currentCorridor.Sensor.name}} <?php echo __('gaminama'); ?>: {{currentCorridor.Vars.product_full_name}}</h3>
        <h4><?php echo __('Parinktas produktas'); ?>: {{productSelected.name}}</h4>
        <div class="select-list">
            <input class="form-control" type="text" value="" v-model="productsFilterString" v-on:dblclick="filterAllProducts" placeholder="<?php echo __('Pasirinkite produktą'); ?>" />
            <ul v-click-outside="hideProductsSelection"><li v-for="product in currentCorridorProducts" v-on:click="selectProduct(product)">{{product.name}}</li></ul>
        </div>
        <div class="row">
            <div class="col-xs-6">
                <button class="btn btn-primary pull-right" v-on:click="confirmProduct()"><?php echo __('Patvirtinti gaminį'); ?></button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-default" v-on:click="closeProductSelector()"><?php echo __('Uždaryti'); ?></button>
            </div>
        </div>
    </div>
    <div id="productChooseConfirmPopup" class="popup" :class="{hidden: !productChooseConfirmPopup}">
        <h3><?php echo __('Ar tikrai norite pakeisti gaminį?'); ?></h3>
        <h4><?php echo __('Keičiama iš'); ?>: {{currentCorridor.Vars.product_full_name}}</h4>
        <h4><?php echo __('Keičiama į'); ?>: {{productSelected.name}}</h4>
        <br />
        <div class="row">
            <div class="col-xs-6">
                <button class="btn btn-primary pull-right" v-on:click="confirmConfirmProduct()"><?php echo __('Taip'); ?></button>
            </div>
            <div class="col-xs-6">
                <button class="btn btn-default" v-on:click="goBackToProductSelect()"><?php echo __('Ne'); ?></button>
            </div>
        </div>
    </div>
</div>
<template id="corridorTpl">
    <div class="corridor pins">
        <div :class="{no_connection: !corridor.is_connected}">
            <div class="left-side">
                <div v-for="pin in corridor.left_pins" class="pinContainer" :class="[pin.FoundProblemsTemp.id > 0?'hasProblem':'', pin.Sensor.name, pin.Sensor.marked==1?'active':'disabled']"><p class="pin"></p></div>
            </div>
            <div class="center">
                <p v-on:click="selectCorridor(corridor)" class="corridor-name">{{corridor.Sensor.name}}</p>
                <p class="bg" style="background: #c6e0b4;">{{corridor.current_shift_quantity}}</p>
                <p class="bg" style="background: #{{corridor.remaining_count > 0?'bdd7ee':'8ea9db'}};">{{corridor.remaining_count}}</p>
                <p class="bg" style="background: #ffe699;">{{corridor.intent_count}}</p>
            </div>
            <div class="right-side">
                <div v-for="pin in corridor.right_pins" class="pinContainer" :class="[pin.FoundProblemsTemp.id > 0?'hasProblem':'', pin.Sensor.name, pin.Sensor.marked==1?'active':'disabled']"><p class="pin"></p></div>
            </div>
            <br class="clear" />
            <p>{{corridor.Product.production_code}}&nbsp;</p>
            <p class="user">{{corridor.Vars.user_name?corridor.Vars.user_name:'<?php echo __('---'); ?>'}}</p>
        </div>
    </div>
</template>
<script type="text/javascript">
    var load_corridors_url = '<?php echo $loadCorridorsUrl; ?>';
</script>
<?php
echo $this->Html->script('Hampidjan.states_and_counts_currshift', array('inline'=>true));