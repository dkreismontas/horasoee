<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('code', array('label' => __('Kodas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('unit', array('label' => __('Mato vnt.'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php //echo $this->Form->input('line_ids', array('label' => __('Priskiriama linijoms'), 'type' => 'select', 'multiple'=>true, 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'options'=>$lines, 'size'=>sizeof($lines)<10?sizeof($lines):10)); ?>
<?php //echo $this->Form->input('sensor_types', array('label' => __('Priskiriama jutiklių tipams'), 'type' => 'select', 'multiple'=>true, 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'options'=>$typeOptions)); ?>
<div>
	<br />
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>

