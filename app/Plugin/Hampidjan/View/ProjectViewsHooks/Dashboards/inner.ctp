<style>
    .nav-tabs li:hover a{
        color:white !important;
    }
    .nav-tabs li.active:hover a{
        color:#3b6c8e !important;
    }
</style>
<div id="dialog"></div>
<div id="subscriptionPeriod" style="display: none;">
    <?php
    echo $this->Form->create('Subscription', array('id'=>'addSubscription'));
    echo $this->Form->input('form_data', array('id'=>'mainFormData', 'type'=>'hidden'));
    echo $this->Form->input('form_action', array('id'=>'mainFormAction', 'type'=>'hidden'));
    echo $this->Form->input('period_type', array('options'=>$statSubscrPeriodTypes, 'class'=>'form-control', 'label'=>false));
    echo $this->Form->button(__('Išsaugoti'), array('class'=>'btn btn-primary pull-left save', 'label'=>false));
    echo $this->Form->button(__('Uždaryti langą'), array('class'=>'btn btn-default pull-right cancel', 'label'=>false));
    echo $this->Form->end();
?>
</div>
<div id="infoContent">
	<div class="row-fluid">
		<div  class="span12">
			<h4 class="widgettitle"><?php echo __('Veiksmai'); ?></h4>
			<div class="widgetcontent">
				<?php
				echo $this->Form->button(__('Sukurti naują skydelį'), array(
					'type' => 'button',
					'data-type'=> 1,
					'class'=>'addWidget btn btn-info'
				));
				echo $this->Form->button(__('Skydelių prenumerata'), array(
					'type' => 'button',
					'data-type'=> 1,
					'class'=>'subscription btn btn-info'
				));
//				echo $this->Form->button(__('Ataskaitų prenumerata'), array(
//					'type' => 'button',
//					'data-type'=> 1,
//					'class'=>'statisticalSubscription btn btn-info'
//				));

			?>
                <!--ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#one" aria-controls="one" role="tab" data-toggle="tab"><?php echo __('Statistikos ataskaita') ?></a></li>
                    <li role="presentation"><a href="#two" aria-controls="two" role="tab" data-toggle="tab"><?php echo __('Perėjimų tipų ataskaita'); ?></a></li>
                    <li role="presentation"><a href="#three" aria-controls="two" role="tab" data-toggle="tab"><?php echo __('Darbuotojų ataskaita'); ?></a></li>
                    <li role="presentation"><a href="#four" aria-controls="two" role="tab" data-toggle="tab"><?php echo __('Įrenginių išnaudojimo ataskaita'); ?></a></li>
                </ul-->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="hampidjan">
                        <?php $errorMessage = $this->Session->flash(); echo trim($errorMessage)?'<h3 class="widgettitle title-danger">'.$errorMessage.'</h3>':''; ?>
                        <div class="row">
                            <div class="col-md-6">
                                <br>
                                <?php         echo $this->Form->create(false, array(
                                'url'           => '/hampidjan/reports/generate',
                                'class'         => 'form form-horizontal',
                                'role'          => 'form',
                                'autocomplete'  => 'off',
                                'inputDefaults' => array(
                                    'class' => 'form-control datepicker',
                                )
                                ));?>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-6">
                                            <?php //echo $this->Form->input('date', array('label' => __('Data'), 'class'=>'date form-control', 'value'=>date('Y-m-d'))); ?>
                                            <?php echo $this->Form->input('start_end_date', array('label' => __('Nuo - iki'), 'class'=>'daterange start_end_date form-control')); ?>
                                            <?php echo $this->Form->input('time_pattern', array('type'=>'hidden','class'=>'time_pattern','id'=>false)); ?>
                                            <?php
                                            echo $this->Form->input('view_angle', array(
                                                'label' => false,
                                                'options' => array(0=>__('Gamyba pagal darbuotoją'),1=>__('Gamyba pagal gaminį'), 2=>__('Įrenginių sustojimai')),
                                                'class' => 'form-control', 'before'=>'<label class="col-md-12" style="padding:0">'.__('Ataskaitos tipas').'</label><div class="col-md-12" style="padding:0">','after'=>'</div>','div'=>'view_angle',
                                            ));
                                            ?>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="timePattern dropdown col-xs-1" style="padding-left: 0;">
                                                <span class="btn glyphicon glyphicon-calendar" data-toggle="dropdown"></span>
                                                <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                                                    <?php foreach($timePatterns as $key => $patternName): ?>
                                                        <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="<?php echo $key; ?>"><?php echo $patternName; ?></a></li>
                                                    <?php endforeach; ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php echo $this->Form->submit(__('Siųstis'), array('class' => 'btn btn-primary','style' => 'margin-left:15px;margin-top:10px; float: left;', 'div'=>false));?>
                                <?php //echo $this->Form->button(__('Prenumeruoti'), array('class' => 'btn btn-primary add-subscription','style' => 'float: left;font-size: 13px; margin-left:15px;margin-top:10px;'));?>
                                <?php echo $this->Form->end();?>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
</div>
<div id="widgetsWrap">
	<div class="row" id="dashboardItems">
		<?php
		foreach ($widgets as $key => $widget) {
			echo $this->requestAction('/dashboards/widget/'.$widget['DashboardsSetting']['type'].'/subscription:'.$subscription,array('widget'=>$widget,'key'=>'key_'.$key));
		}
		 ?>
	</div>
</div>
<?php
    //$this->Html->script('ofc-scripts/json2',array('inline' => false));
    // $this->Html->script('ofc-scripts/swfobject',array('inline' => false));
    // $this->Html->script('ofc-scripts/swf_bg_functions',array('inline' => false));
    $this->Html->script('ofc-scripts/jquery.extractor.min',array('inline' => false));
    $this->Html->script('https://www.gstatic.com/charts/loader.js',array('inline' => false));
	echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon');
?>
<script type="text/javascript" charset="utf-8">
    var branchesShifts = <?php echo json_encode($branchesShifts); ?>;
    var sensorBranches = <?php echo json_encode($sensorBranches); ?>;
    var downtimes = <?php echo json_encode($downtimes); ?>;
    var lineSensors = <?php echo json_encode($lineSensors); ?>;
    var typeSensors = <?php echo json_encode($typeSensors); ?>;
    jQuery("document").ready(function ($) {
        // jQuery(".onlydatepicker").datepicker({
            // dateFormat: "yy-mm-dd"
        // });
        // jQuery(".datepicker").datetimepicker({
            // dateFormat : 'yy-mm-dd',
            // timeFormat : 'HH:mm',
            // firstDay : 1,
            // closeText: 'Uždaryti',
            // currentText: 'Dabar',
            // timeText: 'Laikas',
            // hourText: 'Valandos',
            // minuteText: 'Minutės',
            // alwaysSetTime: true,
            // autoClose: false,
            // onSelect: function(dateText,inst){
                // //$('#innerForm ').trigger('click');
                // $('#innerForm input.time_pattern').val('');
            // }
        // });
        $('.add-subscription').bind('click', function(e){
            e.preventDefault();
            $("#subscriptionPeriod h4").remove();
            var form = $(this).closest('form');
            var warnings = new Array();
            if(form.find('input.time_pattern').val().length <= 0){
                warnings.push('<?php echo __('Pasirinkite tinkamą ataskaitos laikotarpį'); ?>');
            }
            if(!form.find('#sensors').val() && !form.find('#operators').val()){
                warnings.push('<?php echo __('Pasirinkite darbo centrą/darbuotoją'); ?>');
            }
            if(warnings.length > 0){
                $("#dialog").html('<h4 class="widgettitle title-danger">'+warnings.join('<br />')+'</h4>');
                $("#dialog").dialog({
                    title: '<?php echo __('Periodiškumas'); ?>'
                });
                return false;
            }
            $("#subscriptionPeriod #mainFormData").val(form.serialize());
            $("#subscriptionPeriod #mainFormAction").val(form.attr('action'));
            $("#subscriptionPeriod").dialog({
                title: '<?php echo __('Periodiškumas'); ?>'
            });
        });
        $('body').on('click', 'form#addSubscription button.cancel', function(e){
            e.preventDefault();
            $(this).closest('div.ui-dialog').find('.ui-dialog-titlebar-close').trigger('click');
        });
        $('body').on('click', 'form#addSubscription button.save', function(e){
            e.preventDefault();
            $.ajax({
                url: "<?php echo $this->Html->url('/'.$this->params['controller'].'/save_statistical_reports_subscription/',true); ?>",
                type: 'post',
                data: $(this).closest('form').serialize(),
                context: $(this),
                success: function(){
                    $('#subscriptionPeriod').prepend('<h4 class="widgettitle title-success"><?php echo __('Sėkmingai sukurta'); ?></h4>');
                    var form = $(this).closest('form');
                    setTimeout(function(){
                        form.find('button.cancel').trigger('click');
                    },2000);
                }
            });
        });
        $('.timePattern li a').bind('click',function(){
            $(this).closest('form').find('.start_end_date').val($(this).html());
            $(this).closest('form').find('input.time_pattern').val($(this).data('timeformat'));
        });

        $('#mainFormTimePatterns li a').bind('click',function(){
             $('#innerForm #start_end_date').val($(this).html());
            $('#innerForm input.time_pattern').val($(this).data('timeformat'));
        });
        $('#transTimePatterns li a').bind('click',function(){
            $('#transForm .start_end_date').val($(this).html());
            $('#transForm input.time_pattern').val($(this).data('timeformat'));
        });
        $('#workersFormTimePatterns li a').bind('click',function(){
            $('#workersForm .start_end_date').val($(this).html());
            $('#workersForm input.time_pattern').val($(this).data('timeformat'));
        });
        $('#attrTimePatterns li a').bind('click',function(){
            $('#attributesUsageForm .start_end_date').val($(this).html());
            $('#attributesUsageForm input.time_pattern').val($(this).data('timeformat'));
        });
        $( "#dashboardItems").sortable({
            //helper: fixHelper,
            containment: "parent",
            items: "> div",
            update: function(event, ui) {
                var items = new Array();
                $.each($('#dashboardItems > div'),function() {
                    items.push($(this).data('widget_id'));
                });
                $.post("<?php echo $this->Html->url('/'.$this->params['controller'].'/change_dashboard_position/',true); ?>", { items: items } );
            }
        });
    });
	 (function($, window) {
        $.fn.replaceOptions = function(options) {
            var self, $option;
            this.empty();
            this.attr('disabled', 'disabled');
            self = this;
            $.each(options, function(index, option) {
                $option = $("<option></option>").attr("value", index).text(option);
                self.append($option);
                self.removeAttr('disabled');
            });
        };
    })(jQuery, window);
    (function($, window) {
        $.fn.setCheckboxes = function(options,delimet) {
            var self, $option;
            this.empty();
            this.attr('disabled', 'disabled');
            self = this;
            $.each(options, function(index, option) {
                $option = $('<input type="checkbox" name="Node['+delimet+'][]">').attr("value", index);

               $option = '<label>'+$option[0].outerHTML+' '+option+'</label>'
                self.append($option);
                self.removeAttr('disabled');
            });
        };
    })(jQuery, window);

    var currentFormObj,loopCounter=0;
	jQuery(document).ready(function($) {
	    //Widgeto uzdarymas
	    $('#dashboardItems').on('click','.closeWidget',function(){
	        var thisOne = $(this);
	        var conf = confirm('<?php echo __('Ar tikrai norite uždaryti šį grafiką?'); ?>');
	        if(!conf) return false;
	        $.ajax({
	            url: "<?php echo $this->Html->url('/'.$this->params['controller'].'/remove_widget/',true); ?>",
                type: 'post',
                data: {widget_id: thisOne.attr('widget_id')},
                success: function(){
                    thisOne.parents('.widget_block').remove();
                }
	        });
	    });
	    $('#dashboardItems').on('click','.shareWidget',function(){
	        var widgetId = $(this).attr('widget_id');
	        $('#usersHavingMailList li').each(function(){
	            $(this).show();
	            $(this).children('button').attr('widget_id',widgetId).attr('action','share_widget');
	        });
	        popupWindow('#usersHavingMailList', 'auto', '500','auto', "<?php echo __('Vartotojai'); ?> ", "<?php echo __('Uždaryti langą') ?>");
	    });
	    $('body').on('click','#usersHavingMailList ul li button',function(){
	        $.ajax({
                url: "<?php echo $this->Html->url('/'.$this->params['controller'].'/',true); ?>"+$(this).attr('action'),
                type: 'post',
                data: {user_id: $(this).attr('user_id'), 'widget_id': $(this).attr('widget_id')},
                context: $(this),
                success: function(){
                    $(this).closest('li').hide('fast');
                }
            });
	    });
	    //grafiko eksportas
        /*$('.export').live('click',function(e){
            $(this).parent().find('.iconsweets-fullscreen').trigger('click');
            currentFormObj = $(this).parents('form');
            if(!currentFormObj.find('.grafikas').is(':visible')){
                alert('<?php echo __('Norėdami eksportuoti grafiką pirmiausia išsaugokite nuostatas'); ?>');
                return false;
            }
            currentFormObj.find(".export_type").attr('value',$(this).attr('export_type'));
            var swf_id = currentFormObj.find(".keyIdent").val();
            var file_name = '<?php echo date('Y-m-d H-i-s') ?>.png';
            currentFormObj.find(".file_name").attr('value',file_name);
            post_image(false,swf_id,file_name);
            $('.preloader').hide('fast');
            return false;
        });*/

		$('.widget_block:nth-child(3n+1)').addClass('no_pad_left');
		$(document).on('click','.addWidget', function(){
			//var sitas = $(this);
			var tipas = $(this).data('type');
			$.post('<?php echo Router::url('/dashboards/widget/',true) ?>'+tipas,function(data){
				$('#widgetsWrap > div').prepend(data);
				$('.widget_block').removeClass('no_pad_left');
				$('.widget_block:nth-child(3n+1)').addClass('no_pad_left');
				jQuery('.popoversample').popover({selector: 'i[rel=popover]', trigger: 'hover'});
			});

		});
		jQuery('.popoversample').popover({selector: 'i[rel=popover]', trigger: 'hover'});

		$('.subscription').bind('click',function(){
			 $.ajax({
			 	url: "<?php echo $this->Html->url('/dashboards/get_subscription_graphs',true); ?>",
	            type: 'post',
	            success: function(data){
					$('#dialog').html(data);
					$("#dialog" ).dialog({
						title: '<?php echo __('Prenuomeruojami grafikai'); ?>'
					})
	            }
			 })
		});
		$('.statisticalSubscription').bind('click',function(){
			 $.ajax({
			 	url: "<?php echo $this->Html->url('/dashboards/get_statistical_subscription_graphs',true); ?>",
	            type: 'post',
	            success: function(data){
					$('#dialog').html(data);
					$("#dialog" ).dialog({
                        width: 1000,
						title: '<?php echo __('Ataskaitų prenumerata'); ?>'
					})
	            }
			 })
		});
		$('#dialog').on('click','#addSubscriptions input[type="checkbox"]',function(){
			$.ajax({
			 	url: "<?php echo $this->Html->url('/dashboards/save_subscription_graphs',true); ?>",
	            type: 'post',
	            data: {graph_id:$(this).val(), action: $(this).is(':checked')?1:0}
			 })
		});
		$('input.daterange').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD', //affiche sous forme 17/09/2018 14:00
                firstDay: 1, //to start week by Monday
                daysOfWeek: ["<?php echo __('Se') ?>","<?php echo __('Pi') ?>","<?php echo __('An') ?>","<?php echo __('Tr') ?>","<?php echo __('Ke') ?>","<?php echo __('Pe') ?>","<?php echo __('Še') ?>"],
                monthNames: ["<?php echo __('Sausis') ?>","<?php echo __('Vasaris') ?>","<?php echo __('Kovas') ?>","<?php echo __('Balandis') ?>","<?php echo __('Gegužė') ?>","<?php echo __('Birželis') ?>","<?php echo __('Liepa') ?>","<?php echo __('Rugpjūtis') ?>","<?php echo __('Rugsėjis') ?>", "<?php echo __('Spalis') ?>","<?php echo __('Lapkritis') ?>","<?php echo __('Gruodis') ?>"],
                applyLabel: "<?php echo __('Išsaugoti') ?>",
                cancelLabel: "<?php echo __('Atšaukti') ?>",
                fromLabel: "<?php echo __('Nuo') ?>",
                toLabel: "<?php echo __('Iki') ?>",
                separator: " ~ " 
            },
            forceUpdate: true,
            timePicker: false,
            timePicker24Hour: false,
            timePickerIncrement: 1,
            opens: 'right',
        }).on('show.daterangepicker', function(ev, picker) {
            $('.time_pattern').attr('value','');
        });
        jQuery(".date").datepicker({
            dateFormat: "yy-mm-dd"
        });
	});

	function submitForm(){
        //jQuery('.preloader').hide('fast');
        //currentFormObj.find('button.saveData').trigger('click');
        currentFormObj.submit();
        jQuery('.export_type').val(0);
        //jQuery('#TimeIntervals').attr('action',formAction);
    }

    function downloadFile(){
        clearInterval(downl);
        jQuery('body').append('<iframe style="display: none;" src="<?php echo $this->Html->url('files/filedownload.php'); ?>?download_file=tmp.png"></iframe>');
    }

    function post_image(debug,swf_id,file_name){
        var url = "<?php echo $this->Html->url('/files/ofc_upload_image.php',true); ?>?name="+file_name;
        var ofc = findSWF("my_chart"+swf_id);
        ofc.post_image( url, 'done', debug );
        loop = setInterval(function(){check_file_created(file_name)},1000);
    }

    function check_file_created(file_name){
         jQuery.ajax({
            url: "<?php echo $this->Html->url('/dashboards/check_file_created',true); ?>",
            type: 'post',
            data: {file_name: file_name},
            complete: function(data){
                loopCounter++;
                try{
                   var response = jQuery.parseJSON(jQuery.trim(data.responseText));
                }catch(e){
                    alert(e);
                }
                if(response.status == '1') {
                    loopCounter = 0;
                    clearInterval(loop);
                    submitForm();
                }
                if(loopCounter >= 10) {
                    alert('<?php echo __('Problema saugant grafiką.'); ?>');
                    clearInterval(loop);
                    loopCounter = 0;
                    return false;
                }
            }
        });
    }
</script>