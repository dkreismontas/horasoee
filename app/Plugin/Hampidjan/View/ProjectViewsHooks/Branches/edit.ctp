<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php //echo $this->Form->input('factory_id', array('label' =>__('Fabrikas'), 'options' => $factoryOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('ps_id', array('label' => __('PS ID'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('quality_user_id', array('label' => __('Kokybės atstovai'),'multiple'=>true, 'options' => $userOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('production_user_id', array('label' => __('Gamybos vadovas'), 'options' => $userOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('shift_manager_email', array('label' => __('Pamainos vadovo el. paštas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<div>
	<br />
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?> 

