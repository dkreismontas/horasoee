<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas padalinys'); ?></button>
<br /><br />
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Pavadinimas'); ?></th>
			<th><?php echo __('PS ID'); ?></th>
			<th><?php echo __('Kokybės atstovai'); ?></th>
			<th><?php echo __('Gamybos vadovas'); ?></th>
			<th><?php echo __('Pamainos vadovo el. paštas'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; 
            $branchQualityUsers = array_intersect_key($userOptions,array_flip(explode(',',$li->quality_user_id)));
        ?>
		<tr>
			<td><?php echo $li->id; ?></td>
			<td><?php echo $li->name; ?></td>
			<td><?php echo $li->ps_id; ?></td>
			<td><?php echo implode(', ',$branchQualityUsers) ?></td>
			<td><?php echo isset($userOptions[$li->production_user_id]) ? $userOptions[$li->production_user_id] : $li->production_user_id; ?></td>
			<td><?php echo $li->shift_manager_email ? $li->shift_manager_email : '&mdash;'; ?></td>
			<th>
				<a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				&nbsp;
				<a href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
			</th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
