<?php 
echo $this->Session->flash();
echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); 
$this->request->data[User::NAME]['password'] = '';
?>
<div class="row-fluid clearfix">
    <div class="col-md-4">
        <?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
        <?php echo $this->Form->input('username', array('label' => __('Vartotojo vardas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('password', array('label' => __('Slaptažodis'), 'type' => 'password', 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'required'=>false)); ?>
        <?php echo $this->Form->input('email', array('type'=>'text','label' => __('El.paštas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'required'=>false)); ?>
        <?php echo $this->Form->input('first_name', array('label' => __('Vardas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('last_name', array('label' => __('Pavardė'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('section', array('label' => __('Skyrius'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('shift_title', array('label' => __('Pamainos pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
    </div>
    <div class="col-md-4">
        <?php echo $this->Form->input('group_id', array('label' => __('Teisių grupė'), 'options' => $permGroups, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php //echo $this->Form->input('branch_id', array('label' => __('Padalinys'), 'options' => $branchOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php //echo $this->Form->input('sensor_id', array('label' => __('Darbo centras'),'multiple'=>true, 'options' => $sensorOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php //echo $this->Form->input('line_id', array('label' => __('Linija'), 'options' => $lineOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('language',array('label'=>__('Kalba'),'type' => 'select','class' => 'form-control','options'=>$possibleLanguages,'default'=>'lt_LT'));?>
        <?php //if(in_array('WorksList',CakePlugin::loaded())): ?>
        <?php //echo $this->Form->input('tech_code', array('label' => __('Techniko kodas'),'type'=>'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php //endif;?>
        <?php echo $this->Form->input('default_start_path', array('label' => __('Pirminis puslapis po prisijungimo: (pvz. /work-center/1_1)'),'type'=>'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
     </div>
     <div class="col-md-4">
        <div class="checkbox">
            <label for="UserResponsive">
                <?php echo $this->Form->input('responsive', array('label' => false, 'type' => 'checkbox', 'div' => null)); ?>
                <?php echo __('Atsakingas'); ?>
            </label>
        </div>
        <div class="checkbox">
            <label for="UserActive">
                <?php echo $this->Form->input('active', array('label' => false, 'type' => 'checkbox', 'div' => null)); ?>
                <?php echo __('Aktyvus'); ?>
            </label>
        </div>
        <!--button class="btn btn-primary" id="attachSensors"><?php echo __('Jutiklių priskyrimas'); ?></button>
        <div style="display: none;">
            <div id="attachSensorsContainer"><?php echo $this->Form->input('selected_sensors', array('class'=>'form-controll', 'type'=>'select', 'multiple'=>'checkbox','label'=>false, 'options'=>array_diff_key($sensorOptions,array_flip(array(0))))); ?></div>
        </div-->
        <?php echo $this->Form->input('additional_emails', array('label' => __('Papildomi el. adresai, kai vartotojas pasirinktas kaip atsakovas. Pirmasis el. adresas - kokybės kontrolė. Vienas adresas - viena eilutė.'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php //echo $this->Form->input('unique_number', array('label' => __('Darbuotojo "Tab" numeris'),'type'=>'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <div>
            <br />
            <button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript"> 
    $(function() { 
        $('input:checkbox, input:radio, .uniform-file').uniform();
        $('#selectAll').bind('click', function(){
            if($(this).is(':checked')){
                $('.checkbox input').attr('checked', true);
                $('.checker span').addClass('checked');
            }else{
                $('.checkbox input').attr('checked', false);
                $('.checker span').removeClass('checked');
            }
        });
        
        $('#attachSensors').bind('click', function(e){
            e.preventDefault();
            popupWindow('#attachSensorsContainer', 'auto', '800','auto', "<?php echo __('Jutiklių priskyrimas'); ?> ", "<?php echo __('Uždaryti langą') ?>");
        });
    }); 
    
</script>
