<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off', 'type' => 'file')); ?>
<div class="row">
    <div class="col-md-6 col-xs-12">
        <?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
        <?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('ps_id', array('label' => __('PS ID'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('branch_id', array('label' => __('Padalinys'), 'options' => $branchOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('picks_speed', array('label' => __('Mašinos greitis (piksai/min)'), 'type'=>'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('perimeter', array('label' => __('Mašinos būgno perimetras (m)'), 'type'=>'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
    </div>
</div>
<div>
	<br /> 
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
</div>
<?php echo $this->Form->end(); ?>