<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php if($user->id == 1){ ?>
<button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas koridorius'); ?></button><br /><br />
<?php } ?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Identifikatorius'); ?></th>
			<th><?php echo __('Pavadinimas'); ?></th>
			<th><?php echo __('Mašinos greitis'); ?></th>
			<th><?php echo __('Perimetras'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="4"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; $liBranch = (isset($li_[Branch::NAME]) ? ((object) $li_[Branch::NAME]) : null); ?>
		<tr>
			<td><?php echo $li->id; ?></td>
			<td><?php echo $li->ps_id; ?></td>
			<td><?php echo $li->name; ?></td>
			<td><?php echo $li->picks_speed; ?></td>
			<td><?php echo $li->perimeter; ?></td>
			<th>
				<a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				&nbsp;
				<!--button class="btn btn-default removeSensor" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></button-->
			</th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.removeSensor').bind('click',function(){
            var conf = confirm('<?php echo htmlspecialchars($removeMessage); ?>');
            if(conf){
                window.location.href = '<?php printf($removeUrl, $li->id); ?>';
            }
            return false;
        });
    });
</script>
