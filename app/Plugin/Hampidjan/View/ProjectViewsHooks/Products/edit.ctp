<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<div class="row">
    <div class="col-xs-4">
        <?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
        <?php echo $this->Form->input('production_code', array('label' => __('Gaminio kodas'), 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('name', array('label' => __('Gaminio pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('dens', array('label' => __('Denų skaičius'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('picks', array('label' => __('Piksai/10cm'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
        <?php echo $this->Form->input('utilization', array('label' => __('Norma, %'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
    </div>
    <div class="col-xs-2">
        <?php echo $this->Form->input('work_center', array('label' => __('Koridoriams'), 'class' => 'form-control', 'div' => array('class' => 'form-group'),'type'=>'select', 'multiple'=>'multiple', 'options'=>$sensorsList, 'size'=>17)); ?>
    </div>
</div>
<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a>
<?php echo $this->Form->end(); ?>
