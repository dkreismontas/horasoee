<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
    <div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas planas'); ?></button>
<br /><br /> */ ?>
    <div class="clearfix pull-right">
            <a class="btn btn-warning" href="<?php echo $this->Html->url('/',true).'products/edit' ?>"><?php echo __('Sukurti naują'); ?></a>
    </div>
    <br/>
    <br/>
    <table class="table table-bordered table-striped" id="productsTable">
        <thead>
        <tr>
            <th><?php echo __('ID'); ?></th>
            <th><?php echo __('Kodas'); ?></th>
            <th><?php echo __('Pavadinimas'); ?></th>
            <th><?php echo __('Denai'); ?></th>
            <th><?php echo __('Piksai (10cm)'); ?></th>
            <th><?php echo __('Norma, %'); ?></th>
            <th><?php echo __('Koridoriai'); ?></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php if (empty($list)): ?>
            <tr>
                <td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td>
            </tr>
        <?php endif; ?>
        <?php foreach ($list as $li_):
            $li = (object) $li_["Product"]; ?>
            <tr>
                <td><?php echo $li->id; ?></td>
                <td><?php echo $li->production_code; ?></td>
                <td><?php echo $li->name; ?></td>
                <td><?php echo $li->dens; ?></td>
                <td><?php echo $li->picks; ?></td>
                <td><?php echo $li->utilization; ?></td>
                <td><?php
                    $idsList = explode(',', $li->work_center);
                    if(empty($li->work_center)){ echo __('Visi koridoriai'); }
                    else echo implode('<br />', array_filter($sensorsList, function($sensorId)use($idsList){return in_array($sensorId, $idsList); }, ARRAY_FILTER_USE_KEY));
                    ?></td>
                <td>
                    <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span
                            class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a') ?></a><br/>
                    <a href="<?php printf($removeUrl, $li->id); ?>"
                       onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');"
                       title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?>
                    </a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
        jQuery('#productsTable').dataTable({
            "sPaginationType": "full_numbers",
            //"bScrollInfinite" : true,
            "aLengthMenu": [[100, 200, 500, -1], [100, 200, 500, "<?php echo __('Visi'); ?>"]],
            "aaSorting": [],
            "bScrollCollapse" : true,
            "sScrollY" : "800px",
            "iDisplayLength": -1,
            "paging": true,
            "scrollY": 500,
            "oLanguage":{
                'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
                'sSearch':'<?php echo __('Paieška');?>',
                'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
                'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
                'sInfoEmpty':"",
                'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
                'oPaginate':{
                    'sFirst': '<?php echo __('Pirmas');?>',
                    'sPrevious': '<?php echo __('Ankstesnis');?>',
                    'sNext': '<?php echo __('Kitas');?>',
                    'sLast': '<?php echo __('Paskutinis');?>'
                }
            },
            "oSearch": {"sSearch": "<?php echo $this->Session->read('FilterText.'.$this->params['controller']); ?>"}
        });
        $('.maincontentinner').on('keyup','#productsTable_filter input', function(){
            $.ajax({
                url: '<?php echo Router::url(array('controller'=>'settings','action'=>'set_filter_text'),true); ?>',
                type: 'POST',
                data: {filter: 'FilterText.<?php echo $this->params['controller']; ?>', value: $(this).val()}
            });
        });
    </script>
<?php
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>