<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
App::import('Vendor', 'Fernet');
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Record','Malsena.Malsena');

    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
                'http://88.119.11.135:5580/data/encrypted',
                'http://88.119.11.135:5680/data/encrypted',
	        );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $ipAndPort);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
	        $mailOfErrors = array();
	        $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
	        if(file_exists($dataPushStartMarkerPath)){
	            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
	                //$this->Sensor->informAboutProblems();
	                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
	            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
	                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
	            }
	        }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
	            $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            if(!$currentShifts[$branchId]){
                    App::uses('CakeRequest', 'Network');
                    App::uses('CakeResponse', 'Network');
                    App::uses('Controller', 'Controller');
                    App::uses('AppController', 'Controller');
                    App::import('Controller', 'Cron');
	                $CronController = new CronController;
	                $CronController->generateShifts(false, $branchId);
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
	        if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }

			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'\//', $address);
	            });
	        }
            $periodsFound = false;
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
                $fernetDecoder = new Fernet\Fernet('FdZhRq5lFLmmHmHAEwKPzl7VuO_3Wnsvfjc3ztNUnOg');
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                $data = json_decode($fernetDecoder->decode($data));
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
	            foreach($data->periods as $record){
                    $periodsFound = true;
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
	                    $quantity = $record->$sensorPinName;
						try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
	                }
	            }
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }
	
}