<?php
App::uses('LrytasImportPlansFromXMLAppController', 'LrytasImportPlansFromXML.Controller');
App::uses('Xml', 'Utility');
class PluginsController extends LrytasImportPlansFromXMLAppController {
    CONST basePath = '/app/Plugin/LrytasImportPlansFromXML/files/';
	private $xmlFile;
	public $uses = array('Order','UsageOrder','OrderProcess','OrdersFact','ExportedOrder','TechProcess','Node', 'LrytasImportPlansFromXML.XmlImport','Log');
	private $exportedOrders = array();	//uzsakymu sarasas, kurie bus isvedami i xml
	private $exportedOrdersInDb = array();	//eksportuojamu uzsakymu saraas, kurie jau buvo issaugoti duomenu bazeje, tikrinant kad i xml neptektu jau issiusti irasai
    public $name = 'Plugins';
	private $nodeIdentId;
	private $currentFilterOrder;
	private $currentProcessesList; //template_structure parametro atveju xml procesai sukisami i vienamti sarasa [ident] = array() kiekvieno uzsakymo atveju is naujo
	private $templateprocesses; //pagal sablona istrauktru threaded procesu sarasas, pakeiciantis esama procesu medi
	private $exportStamp;
    
    private $ordersToDisable = array();

    public function index(){
        //$this->requestAuth(true);
        //$this->layout = '';
    }

	public function start_upload(){
		$this->XmlImport->xmlFile = ROOT.self::basePath.basename($_FILES["file"]["name"]);
		$imageFileType = pathinfo($this->XmlImport->xmlFile,PATHINFO_EXTENSION);
		$log = array('errors'=>array());
		if(strtolower($imageFileType) != 'xml'){
			$info['errors'] = __('Netinkamas failo formatas');
			echo json_encode($info);
			die();
		}
		if ($_FILES['file']['error'] == UPLOAD_ERR_OK               //checks for errors
		      && is_uploaded_file($_FILES['file']['tmp_name'])) { //checks that file is uploaded
			if(move_uploaded_file($_FILES["file"]["tmp_name"], $this->XmlImport->xmlFile)){
				$info = $this->XmlImport->import_orders($this->XmlImport->xmlFile); 
				if($info === true){
					$log['success'] = __('Užsakymai sėkmingai importuoti');
				}elseif(is_array($info) && !empty($info)){
					$log['errors'] = '<ul style="list-style-type:none;"><li>'.implode('</li><li>',$info).'</li></ul>';
				}
				@unlink($this->XmlImport->xmlFile);
			}
		}
		echo json_encode($log);
		die();
	}
	

	/*public function start_cron_upload(){       
        $ftp_server = "ftp.Lrytas.bttcloud.com";
        $ftp_conn = ftp_connect($ftp_server) or die("Could not connect to $ftp_server");
        $login = ftp_login($ftp_conn, 'horasmpm', 'Lapkritis.22');
        $xmlFiles = ftp_nlist($ftp_conn, '');
        $dates = array();
        array_walk($xmlFiles, function($file, $key)use(&$dates, $ftp_conn){ $dates[] = ftp_mdtm($ftp_conn,$file); });
        sort($dates); 
        $dates = array_unique($dates);
        $sortedFiles = array();
        foreach($dates as $date){
            foreach($xmlFiles as $key => $file){
                if(ftp_mdtm($ftp_conn,$file) == $date){
                    $sortedFiles[] = $file;
                    unset($xmlFiles[$key]);
                }
            }
        }
        
        foreach($sortedFiles as $file){
            $imageFileType = pathinfo($file,PATHINFO_EXTENSION);
            if(strtolower($imageFileType) != 'xml'){
                $this->Log->write(__('Iš ftp serverio importuotas failas %s neturi xml plėtinio', $file));
                continue;
            }
            $this->XmlImport->xmlFile = tempnam(sys_get_temp_dir(), "ftp");
            ftp_get($ftp_conn, $this->XmlImport->xmlFile, $file, FTP_BINARY);
            $info = $this->XmlImport->import_orders($file); 
            ftp_get($ftp_conn, ROOT.self::basePath.'history/'.str_replace('.xml','-'.date('Y.m.d-H.i.s').'.xml',$file), $file, FTP_BINARY); 
            if($info === true){
                 $this->Log->write(__('Iš ftp serverio importuotas failas sėkmingai', $file));  
            }
            @ftp_delete($ftp_conn, $file); 
        }
        ftp_close($ftp_conn); 
        // --uzsakymu rikiavimas START
        $arr = array('conditions'=>array(
            'Plan.disabled'=>0,
            'Sensor.pin_name <> \'\'',
            'ApprovedOrder.status_id IS NULL'
        ));
        App::import( 'Model', 'Lrytas.LrytasPlan');
        $planModel = new LrytasPlan($arr);
        //--uzsakymu rikiavimas END
        die();
    }*/

	
}
