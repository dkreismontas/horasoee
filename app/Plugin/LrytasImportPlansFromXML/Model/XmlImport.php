<?php
class XmlImport extends AppModel{
	
	private static $importStatusFile, $stopImportFile;
	public $totalOrdersCount = 0;
	private $plans;
	public $xmlFile;
	private $info = array();
	private $Log = array();
	
	private $planModel, $possibilityModel;
	private $sensors;
	
	public function __construct(){
		$sensorModel = ClassRegistry::init('Sensor');
		$this->sensors = $sensorModel->find('list', array('fields'=>array('ps_id', 'id')));
		$this->planModel = ClassRegistry::init('Plan');
		$this->possibilityModel = ClassRegistry::init('Possibility');
		$this->Log = ClassRegistry::init('Log');
	}
	
	public function updateStatusDuringFilter($serialNumber, $rowNumber){
		$this->checkForStopMarker();
		if(!trim(self::$importStatusFile)) return;
        $handle = fopen(self::$importStatusFile, 'w+');
        fwrite($handle, 
        	'<h3>'.__('Filtruojami užsakymai').'</h3>'."\n".
        	'<p>'.__('Serijos numeris').': '.$serialNumber.'</p>'."\n".
        	'<p>'.__('Išfiltruota').': '.$rowNumber.' '.__('iš').' '.$this->totalOrdersCount.'</p>'."\n"
		);
        fclose($handle);
	}
	
	public function createStopMarker(){
		$handle = fopen(self::$stopImportFile, 'w+');
		fclose($handle);
	}
	
	public function checkForStopMarker(){
		if(file_exists(self::$stopImportFile)){
			echo josn_encode(array('1=1'));
			die();
		}
	}
	
	public function import_orders($file=''){
		try{
			$this->plans = Xml::toArray(Xml::build($this->xmlFile));
			$this->plans = isset($this->plans['PData']['oee_plan'][0])?$this->plans['PData']['oee_plan']:array($this->plans['PData']['oee_plan']);
			$this->totalOrdersCount = sizeof($this->plans);
			$this->create_orders();
            //$this->planModel->reorder();
			if(empty($this->info)) return true;
			else return $this->info;
		}catch(Exception $e){
		    $this->Log->write(__('Iš ftp serverio importuotas failas %s neatitiko xml formato', $file)); 
			return false;
		}
	}

	//kuria uzsakymus is paduoto uzsakymu masyvo pagal sistemoje esanciu sablonu pavyzdi
	public function create_orders(){
		$lastMo = $this->planModel->find();
        //$this->planModel->virtualFields(array('max_pos'=>'MAX(position)+1'));
        //$maxPos = $this->planModel->find('list', array('fields'=>array('sensor_id', 'max_pos'), 'group'=>array('sensor_id')));
		$pos = time();
		foreach($this->plans as $plan){
			$saveData = $plan;
            if(!isset($this->sensors[$plan['work_center']])){
                continue;
            }
			$saveData['sensor_id'] = $this->sensors[$plan['work_center']];
			$saveData['production_code'] = $plan['unit_code'];
			$saveData['production_name'] = $plan['unit_title'];
			$saveData['order_part'] = $plan['order_part'];
			$saveData['signature_number'] = $plan['signature_number'];
			$saveData['signature_side'] = $plan['signature_side'];
			$saveData['quantity'] = $plan['imprints'];
			$saveData['box_quantity'] = $plan['nest'];
            $saveData['step'] = $plan['speed'];
            $saveData['attach_speed'] = $plan['attach_speed'];
            $saveData['sm_number'] = $plan['sm_number'];
            $saveData['mo_number'] = $this->sensors[$plan['work_center']].'_'.uniqid();
            $saveData['preparation_time'] = $plan['preparation_time'];
            $saveData['attach_time'] = $plan['attach_time'];
            $saveData['production_time'] = $plan['production_time'];
            $saveData['start'] = $plan['start'];
            $saveData['end'] = $plan['end'];
            $saveData['position'] = $pos+1;
            $saveData['paper_code'] = isset($saveData['serialized_data']['paper_code'])?$saveData['serialized_data']['paper_code']:'';
            $saveData['serialized_data'] = isset($saveData['serialized_data'])?json_encode($saveData['serialized_data']):'';
			
			$saveData = array_filter($saveData, function($val){ return !is_array($val); });
			$planExistsCond = array(
                'Plan.sm_number'=>$saveData['sm_number'],
                'Plan.sensor_id'=>$saveData['sensor_id'],
                'Plan.order_part'=>$saveData['order_part'],
                'Plan.signature_number'=>$saveData['signature_number'],
                'Plan.signature_side'=>$saveData['signature_side'],
            );
            if(isset($plan['serialized_data']['paper_code']) && trim($plan['serialized_data']['paper_code'])){
                $planExistsCond['Plan.paper_code'] = $plan['serialized_data']['paper_code']; 
            }
            $planExist = $this->planModel->find('first', array('conditions'=>$planExistsCond));
			if(!empty($planExist)){
			    unset($saveData['position']);
				$this->planModel->id = $planExist['Plan']['id'];
				//$this->possibilityModel->deleteAll(array('plan_id'=>$this->planModel->id));
			}else{
				$this->planModel->create();
			}
			$this->planModel->save($saveData);
		}
	}

}
