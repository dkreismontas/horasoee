<?php
App::uses('AppModel', 'Model');

class LrytasImportPlansFromXMLAppModel extends AppModel {
	public static $navigationAdditions = array( );

    public static function loadNavigationAdditions() {
        self::$navigationAdditions[2] =
            array(
                'name'=>'Lrytas Plano importavimas',
                'route'=>Router::url(array('plugin' => 'lrytas_import_plans_from_x_m_l','controller' => 'Plugins/index', 'action' => 'index')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
}
