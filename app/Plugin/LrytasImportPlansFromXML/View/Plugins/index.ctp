<form id="uploadXML" enctype="multipart/form-data">
	<input style="display:none;" name="file" type="file" />
	<a id="orderImportButton" class="btn btn-rounded btn-primary" href="" style="margin-right: 4px;"> <i class="iconsweets-link"></i> &nbsp; <?php echo __('Pasirinkite XML failą'); ?></a>
</form>

<script type="text/javascript">
    
	//Uzsakymu importas
	jQuery('#orderImportButton').bind('click',function(){
		jQuery('form#uploadXML input[type="file"]').trigger('click');
		return false;
	});
	jQuery('form#uploadXML input[type="file"]').live('change',function(){
		jQuery('form#uploadXML').trigger('submit');
	});
	
	
	jQuery('form#uploadXML').live('submit',function(){  
		$('#startPlaning').remove();
		jQuery('body').append('<div id="startPlaning"><div class="planWrapper"><div><div class="info"><h1><?php echo __('Vykdomas importavimas'); ?></h1><br /></div><div class="loaders"></div></div></div><div class="planScreen"></div></div>');
		var formData = new FormData(jQuery(this)[0]);
		jQuery.ajax({
			type: 'POST',
			url: '<?php echo $this->Html->url('/lrytas_import_plans_from_x_m_l/Plugins/start_upload',true); ?>',
			data: formData,
			dataType: 'JSON',
			success: function(json){
				if(!json){
					alert('<?php echo __('Problema su serveriu. Bandykite iš naujo.'); ?>');
				}
				if(json != null && jQuery.trim(json.errors)){
					jQuery('#startPlaning .info').append('<p>'+json.errors+'</p>')
				}
				if(json.success){
					jQuery('#startPlaning .info').append('<p>'+json.success+'</p>')
				}
				jQuery('#startPlaning .info').append('<button class="btn btn-primary" id="ok">OK</button>')
				//else alert(ordersImportFail);
				//else alert(ordersImportFail);
				//location.reload();
			},
			cache: false,
            contentType: false,
            processData: false
		});
		return false;
	});
	
	jQuery('#ok').live('click', function(){
		jQuery('#startPlaning').remove();
	});
</script>