<?php
class Vitabaltic extends AppModel{
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     
     public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$periods){
        foreach($periods as &$period){
            if(isset($period->data['Work']['problem_id']) && $period->data['Work']['problem_id'] == -1){
                $period->title = str_replace(__('vnt.'),'m.', $period->title);
            }
        }
    }
     
    public function Workcenter_changeUpdateStatusVariables_Hook2(&$data){
        if(isset($data['workcenterHoursInfoBlocks']['operational_factor'])){
            array_walk($data['workcenterHoursInfoBlocks']['operational_factor']['value'], function(&$value){
               $value = min(100, $value); 
            });
        }
    }
    
    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $plansIds = Set::extract('/Plan/id', $plans);
        if(isset($currentPlan['ApprovedOrder'])){$plansIds[] = $currentPlan['Plan']['id'];}
        if(!empty($plansIds)){
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $approvedOrders = $approvedOrderModel->find('list', array('fields'=>array('plan_id', 'id'),'conditions'=>array('ApprovedOrder.plan_id'=>$plansIds)));
            if(!empty($approvedOrders)){
                $partialQuantityModel = ClassRegistry::init('PartialQuantity');
                $partialQuantities = Hash::combine($partialQuantityModel->find('all', array(
                    'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'TRIM(PartialQuantity.approved_order_id) AS approved_order_id'),
                    'conditions'=>array('PartialQuantity.approved_order_id'=>$approvedOrders),
                    'group'=>array('PartialQuantity.approved_order_id')
                )),'{n}.0.approved_order_id', '{n}.0.quantity');
                if(isset($currentPlan['ApprovedOrder']) && isset($partialQuantities[$currentPlan['ApprovedOrder']['id']])){
                    $currentPlan['Plan']['partial_quantity'] = $partialQuantities[$currentPlan['ApprovedOrder']['id']];   
                }
                foreach($plans as &$plan){
                    $planId = $plan['Plan']['id'];
                    if(!isset($approvedOrders[$planId])){ continue; }
                    if(isset($partialQuantities[$approvedOrders[$planId]])){
                        $plan['Plan']['partial_quantity'] = $partialQuantities[$approvedOrders[$planId]];
                    }
                }
            }
        }
    }
     
}
    