<?php
App::uses('KggroupAppController', 'Kggroup.Controller');

class PluginsController extends KggroupAppController {
        
    public $uses = array('Shift','Record','Sensor','Plan','ApprovedOrder');

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Kggroup.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

    public function index(){
        $this->layout = 'ajax';
    }

    function test_import($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Kggroup.VitomekImportPlansShell');
        $shell = new VitomekImportPlansShell();
        $shell->start_import();
        die();
    }

}
