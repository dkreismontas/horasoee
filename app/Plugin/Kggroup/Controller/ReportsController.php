<?php
class ReportsController extends KggroupAppController{

    public $uses = array('Kggroup.LastShiftReportPdf','Shift','Sensor','Problem','FoundProblem','Record');

    public function generate_shifts_report_pdf(){
        $this->layout = 'ajax';
        $sensorId = $this->request->data['sensors']??0;
        $settings = $this->LastShiftReportPdf->getSensorDowntimesSettings($sensorId);
        if(empty($settings)){
            if(!isset($this->request->params['named']['exportToFile'])) {
                $this->Session->setFlash(__('Neaprašyti jutiklio prastovų laikai sistemos nustatymuose'));
                $this->redirect(array('plugin' => false, 'controller' => 'dashboards', 'action' => 'inner'));
            }else{ return null; }
        }
        $sensorData = $this->Sensor->withRefs()->findById($sensorId);
        $lastShift = $this->Shift->find('first',array('conditions'=>array(
            'Shift.id'=>24653703,//TODO
            'Shift.end <'=>date('Y-m-d H:i'),
            'Shift.branch_id'=>$sensorData['Sensor']['branch_id']??0
        ), 'order'=>array('Shift.end DESC')));
        if(empty($lastShift)) {
            if(!isset($this->request->params['named']['exportToFile'])){
                $this->Session->setFlash(__('Neparinktas jutiklis'));
                $this->redirect(array('plugin' => false, 'controller' => 'dashboards', 'action' => 'inner'));
            }else{ return null; }
        }
        $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        $downtimesTypes = array(
            'technical_parents' => __('Techninės prastovos'),
            'technological_parents' => __('Technologinės prastovos'),
            'exploitation_parents' => __('Eksplotacinės prastovos'),
            'changeovers' => __('Perėjimai tarp rūšių')
        );
        $quantityType = $sensorData['Sensor']['plan_relate_quantity']=='quantity'?'Record.quantity':'Record.unit_quantity';
        $this->Record->bindModel(array('belongsTo'=>array('Plan','Sensor','FoundProblem',
            'Problem'=>array('foreignKey'=>false, 'conditions'=>array('FoundProblem.problem_id = Problem.id')),
            'TransitionProblem'=>array('foreignKey'=>false,'className'=>'Problem','conditions'=>array('FoundProblem.transition_problem_id = TransitionProblem.id AND FoundProblem.transition_problem_id > 0'))
        )));
        $problemsData = $this->Record->find('all', array(
            'fields'=>array(
                'SUM('.Configure::read('recordsCycle').') AS duration',
                'SUM('.$quantityType.') as quantity',
                'Problem.id',
                'Problem.name',
                'Plan.step',
                //'SUM(Plan.step * (TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)/3600)) AS theorical_quantity',
                'ROUND(SUM('.Configure::read('recordsCycle').' / 3600 * Plan.step)) AS theorical_quantity',
                'TransitionProblem.id',
                'TransitionProblem.name',
                'COUNT(DISTINCT(FoundProblem.id)) AS transitions_count'
            ),'conditions'=>array(
                'Record.sensor_id' => $sensorId,
                'Record.shift_id' => $lastShift['Shift']['id'],
                //'FoundProblem.start != FoundProblem.end'
            ),'group'=>array(
                'FoundProblem.problem_id',
                'FoundProblem.transition_problem_id',
            )
        ));
        $problemSettings = $this->LastShiftReportPdf->getSensorDowntimesSettings($sensorData['Sensor']['id']);
        $dismissProblemsForTheoricalQuantity = array_merge(
            isset($problemSettings['measuring'])?Set::extract('/id',$problemSettings['measuring']):array(),
            isset($problemSettings['experiments'])?$problemSettings['experiments']['id']:array(),
            Set::extract('/id',$problemSettings['changeovers']),
            array($problemSettings['pam_start']['id']),
            array($problemSettings['pam_end']['id']),
            array($problemSettings['cleaning']['id']),
            );
        $finalData = $this->LastShiftReportPdf->combineProblemsData($problemsData, $sensorData, $problemsTreeList, $downtimesTypes, $dismissProblemsForTheoricalQuantity);
        $finalData['shift_start_end'] = date('Y/m/d H:i:s',strtotime($lastShift['Shift']['start'])).' - '.date('Y/m/d H:i:s',strtotime($lastShift['Shift']['end']) +1);
        $finalData['shift_duration'] = strtotime($lastShift['Shift']['end']) - strtotime($lastShift['Shift']['start']) + 1;
        $this->Record->bindModel(array('belongsTo'=>array('Plan','FoundProblem')));
        $hoursRecords = $this->Record->find('all', array(
            'fields'=>array(
                'ROUND(SUM(IF(Record.found_problem_id IS NULL, Record.'.$sensorData['Sensor']['plan_relate_quantity'].', 0))) AS fact_quantity',
                'ROUND(SUM(IF(FoundProblem.problem_id IN ('.implode(',', $dismissProblemsForTheoricalQuantity).') OR FoundProblem.transition_problem_id IN ('.implode(',', $dismissProblemsForTheoricalQuantity).'), 0, ('.Configure::read('recordsCycle').' / 3600 * Plan.step)))) AS plan_quantity',
                'DATE_FORMAT(Record.created,\'%H\') AS hours'
            ),'conditions'=>array(
                'Record.sensor_id' => $sensorId,
                'Record.shift_id' => $lastShift['Shift']['id']
            ),'group'=>array(
                'DATE_FORMAT(Record.created,\'%Y-%m-%d %H\')'
            )
        ));
        //suformuojamas
        $graphName = $this->LastShiftReportPdf->createGraph($hoursRecords);
        if(isset($this->request->params['named']['exportToFile'])){
            App::import("Vendor", "mexel/PHPExcel");
            $objPHPExcel = new PHPExcel();
            $objDrawing = new PHPExcel_Worksheet_MemoryDrawing();
            $xlsView = new View($this, false);
            $xlsReportPath = APP.'Plugin/Kggroup/webroot/files/';
            $uniquePart = array('shifts_report_xls',$sensorData['Factory']['name'], $sensorData['Sensor']['name']);
            foreach($uniquePart as $catalog){
                $xlsReportPath .= mb_strtolower(Inflector::slug($catalog)).'/';
                if (!file_exists($xlsReportPath)) {
                    mkdir($xlsReportPath);
                }
            }
            $this->LastShiftReportPdf->removeOldLocalFiles($xlsReportPath);
            $xlsReportPath .= Inflector::slug(__('pamainos ataskaita')) . '_' . date('Y-m-d|H:i',strtotime($lastShift['Shift']['start'])).'_'.date('Y-m-d|H:i',strtotime($lastShift['Shift']['end']) +1) . '.xlsx';
            $xlsView->set(compact('objPHPExcel', 'objDrawing', 'finalData', 'sensorData', 'downtimesTypes', 'hoursRecords', 'graphName', 'xlsReportPath'));
            $xlsView->render('generate_shifts_report_xls');
            //$this->LastShiftReportPdf->sendXlsReportToFtp($xlsReportPath);
        }
        $pdfView = new View($this, false);
        $pdfView->set(compact('graphName','finalData','sensorData', 'downtimesTypes','hoursRecords','settings'));
        $html2pdf = new Spipu\Html2Pdf\Html2Pdf('P', 'A4', 'pl');
        $html2pdf->pdf->SetDisplayMode('real');
        $html2pdf->writeHTML($pdfView->render());
        if(file_exists(WWW_ROOT.$graphName)){
            unlink(WWW_ROOT.$graphName);
        }
        if(isset($this->request->params['named']['exportToFile'])){
            $html2pdf->output(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile'],'F');
        }else{
            $html2pdf->output();
        }
        $this->render(false);
    }

}