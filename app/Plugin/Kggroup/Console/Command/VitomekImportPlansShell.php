<?php
App::uses('Xml', 'Utility');
class VitomekImportPlansShell extends Shell {

    public $uses = array('Plan');

    public function start_import(){
        $ftpServer = '84.42.9.254';
        $ftpPort = 21;
        $ftpLogin = 'Vitomek';
        $ftpPass = 'qyp4BLz7gzb7kG2ek4xM';
        $xmlFilePath = 'Рецепт';
        $ftpConn = ftp_connect($ftpServer, $ftpPort, 10) or die("Could not connect to $ftpServer");
        $login = ftp_login($ftpConn, $ftpLogin, $ftpPass);
        ftp_set_option($ftpConn, FTP_TIMEOUT_SEC, 1800);
        ftp_pasv($ftpConn, true);
        //$xmlFilePath = implode('/',array_slice(explode('/', trim($xmlFilePath,'/')),0,-1));
        $handle = tempnam(sys_get_temp_dir(), "ftp");
        $this->Plan->bindModel(array('belongsTo'=>array(
            'ApprovedOrder'=>array(
                'foreignKey'=>false,
                'conditions'=>array('Plan.id = ApprovedOrder.plan_id')
            )
        )), false);
        foreach(ftp_nlist($ftpConn, $xmlFilePath) as $remoteFile){
            ftp_get($ftpConn, $handle, $remoteFile, FTP_BINARY, 0);
            $xml = Xml::toArray(Xml::build($handle));
            $plan = $xml['TASK']['RECIPE']??'';
            if(!trim($plan)){ $this->moveFileTo('errors', $ftpConn, $remoteFile); continue; }
            $plan = self::parsePlanIntoParts($plan);
            if(empty($plan)){ $this->moveFileTo('errors', $ftpConn, $remoteFile); continue; }
            $plan['step'] = 220;
            $planExist = $this->Plan->find('first', array('conditions'=>array(
                'Plan.mo_number'=>$plan['mo_number'],
                'Plan.sensor_id'=>$plan['sensor_id'],
            )));
            if(isset($planExist['ApprovedOrder']['id']) && $planExist['ApprovedOrder']['id'] > 0){ //planu su orderiais nenaujiname
                $this->moveFileTo('success', $ftpConn, $remoteFile); continue;
            }
            if(!empty($planExist)){
                $this->Plan->id = $planExist['Plan']['id'];
            }else{
                $this->Plan->create();
            }
            $this->Plan->save($plan);
            $this->moveFileTo('success', $ftpConn, $remoteFile);
        }
    }

    private static function parsePlanIntoParts($plan){
        if(preg_match('/№([^\s]+)\s+[кК]+од:\s*([^\s|(]+)\s*\((.+?(?=\)\s*[кК]+)).+:\s*(.+?(?=\s\d*[\.,]*\d*т))\s+(\d*[\.,]*\d*)т/i', $plan, $planParts)){
            $plan = array_combine(
                array('all_string', 'mo_number', 'production_code', 'production_name', 'client', 'quantity'),
                $planParts
            );
            $plan['sensor_id'] = 42;
            $plan['step'] = 200;
            $plan['box_quantity'] = 1;
            return $plan;
        }
        return array();
    }

    private function moveFileTo($catalog, $ftpConn, $filePath){
        $filePathParts = explode('/', $filePath);
        $fileName = array_pop($filePathParts);
        $moveToPath = $catalog.'/'.$fileName;
        if(ftp_size($ftpConn, $moveToPath)){
            @ftp_delete($ftpConn, $moveToPath);
        }
        ftp_rename($ftpConn, $filePath, $moveToPath);
    }

}