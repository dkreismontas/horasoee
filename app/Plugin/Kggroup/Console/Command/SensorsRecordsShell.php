<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Plan','ApprovedOrder','Record');
    private static $sensorsInOtherTimezones = array(
       'Europe/Minsk' => array(43, 44, 55),
       'Europe/Moscow' => array(42),
    );

    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
                'http://88.119.209.195:5580/data',//alytus
                'http://88.119.209.198:5580/data',//kedainiai
                'http://88.119.209.199:5580/data', //malunas
                'http://88.119.209.199:55801/data', //kgpasarai
                'http://146.120.14.95:5580/data', //Belfidagro - Baltarusija
                'http://88.119.209.195:55801/data', //KgSlaituva
                'http://88.119.209.195:55802/data', //Kgagppgp
                //'http://84.42.2.179:5580/data', //Vitomek
                'http://84.42.9.254:5580/data', //Vitomek
                'http://88.119.209.198:55801/data', //Vpaukstynas
                'http://88.119.209.198:55802/data', //Vpaukstynas
                'http://88.119.209.199:55802/data', //Vpaukstynas
                'http://88.119.209.195:55803/data', //Vpaukstynas
                'http://88.119.209.198:55803/data', //Vpaukstynas
	        );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $this->args[0]);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
	        $mailOfErrors = array();
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
                $sensorsInBranch = Set::extract('/Sensor/id',array_filter($sensorsListInDB, function($sensor)use($branchId){ return $sensor['Sensor']['branch_id'] == $branchId; }));
                $timeZone = array_filter(self::$sensorsInOtherTimezones, function($sensorsIds)use($sensorsInBranch){
                    return !empty(array_intersect($sensorsIds, $sensorsInBranch));
                });
                $date = new DateTime();
                if(!empty($timeZone)){
                    $tz = new DateTimeZone(key($timeZone));
                    $date->setTimezone($tz);
                }
                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, $date);
	            if(!$currentShifts[$branchId] || ($currentShifts[$branchId]['Branch']['shift_change_init']==1)){
                    $this->Shift->generateShifts($branchId);
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
	        if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }
            $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
            if(file_exists($dataPushStartMarkerPath)){
                if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
                    //$this->Sensor->informAboutProblems();
                    die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
                }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
                    $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
                }
            }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'\//', $address);
	            });
	        }
            $periodsFound = false;
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                $data = json_decode($data);
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
                if($address == 'http://88.119.209.198:5580/data'){
                    $this->copyPlan(array(4),6);//Kedainiai id:6 jutiklis turi gauti toki pati plana kaip ir vienas is id:4 arba ... jutikliu. Tai darome cia, kadangi su id:6 niekas nedirbs
                }
	            foreach($data->periods as $record){
                    $periodsFound = true;
                    $kedainiaiId3Id4Total = 0;
                    $belfidagroId43Id44Total = 0;
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }

						if($sensorId == 2){//Kgalytus
                            $quantity = $record->$sensorPinName / 10;
                        }elseif($sensorId == 13){//Kgpasarai //jei jutiklio i5 įrašai lygus 0, i6 jutiklio įrašai turi būti padaromi į 0, jei i5 įrašai ne 0, tada i6 tokie kokie ateina
                            $quantity = isset($record->i5) && $record->i5 == 0?0:$record->$sensorPinName;
                        }elseif($sensorId == 14){//Kgpasarai //jei jutiklio i7 įrašai lygus 0, i8 jutiklio įrašai turi būti padaromi į 0, jei i7 įrašai ne 0, tada i8 tokie kokie ateina
                            $quantity = isset($record->i7) && $record->i7 == 0?0:$record->$sensorPinName;
                        }elseif($sensorId == 22){ //KgAgppgp
                            $quantity = $record->$sensorPinName * 2;
                        }else{
                            $quantity = $record->$sensorPinName;
                        }
						$timeZone = array_filter(self::$sensorsInOtherTimezones, function($sensorsIds)use($sensorId){
						    return in_array($sensorId, $sensorsIds);
                        });
						if(!empty($timeZone)){
                            $tz = new DateTimeZone(key($timeZone));
                            $date = new DateTime(gmdate('Y-m-d H:i:s', $record->time_to).' UTC');
                            $date->setTimezone($tz);
                            $created = $date->format('Y-m-d H:i:s');
                        }else{
                            $created = date('Y-m-d H:i:s', $record->time_to);
                        }
						if($sensorId == 42){ //Vitomek DC42 turi būti fiksuojami jo paties siunčiami duomenys ir papildomai prijungiami DC45 duomenys. TASK ID 5087
						    $quantity += $record->i2;
                        }
	                    try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.$created.'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
                        if(in_array($sensorId, array(3,4))){//Makaronu pakavimo centrai
                            $kedainiaiId3Id4Total += $quantity;
                        }
                        if(in_array($sensorId, array(43,44))){//Belfidagro centrai
                            $belfidagroId43Id44Total += $quantity;
                        }
	                }
                    //Kgkedainiu procedura susumuoti dvieju darbo centru ID3 ir ID4 kiekius ir irasyti i bendra ID6
                    if($address == 'http://88.119.209.198:5580/data'){
                        try{
                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', 6, '.$kedainiaiId3Id4Total.','.Configure::read('recordsCycle').');';
                            $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
                            fwrite($fh, $queryLog);
                        }catch(Exception $e){
                            $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
                        }
                    }
                    //Belfidagro procedura susumuoti dvieju darbo centru ID43 ir ID44 kiekius ir irasyti i bendra ID59
                    if($addessIpAndPort == '146.120.14.95:5580'){
                        try{
                            $query = 'CALL ADD_FULL_RECORD(\''.$created.'\', 59, '.$belfidagroId43Id44Total.','.Configure::read('recordsCycle').');';
                            $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
                            fwrite($fh, $queryLog);
                        }catch(Exception $e){
                            $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
                        }
                    }
	            }
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }

    private function copyPlan($sensorFrom, $sensorTo){
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $activeOrderTo = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.sensor_id'=>$sensorTo, 'ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS)));
        if(!empty($activeOrderTo)){
            $copyFromSensorId = (int)current(explode('_',$activeOrderTo['Plan']['mo_number'])); //mo numeris sudarytas is sensorID_uniqueNr, todel ziurime is ko vyko aktyvaus plano klonavimas
            if($copyFromSensorId == $sensorTo){//jei planas uzdetas tiesiogiai sensorTo, tada nieko nedarome, patys plana ir tures uzdaryti, nes jis nebuvo klonuotas o sukurtas tiesiai per darbo centra
                return true;
            }
        }
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $activeOrderFrom = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.sensor_id'=>$sensorFrom, 'ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS)));
        if(!empty($activeOrderFrom) && !empty($activeOrderTo) && $activeOrderFrom['Plan']['mo_number'] == $activeOrderTo['Plan']['mo_number']){//planai sutampa, nieko nedarome
            return true;
        }elseif(!empty($activeOrderFrom) && empty($activeOrderTo)){//jei $sensorFrom sensoriaus planas yra, bet nera $sensorTo, klonuojame plana bei orderi
            $planExist = $this->Plan->find('first', array('conditions'=>array('Plan.sensor_id'=>$sensorTo, 'Plan.mo_number'=>$activeOrderFrom['Plan']['mo_number'])));
            if(empty($planExist)){
                $this->Log->write('Klonuojamas planas. Originalus duomenys: '.json_encode($activeOrderFrom));
                $activeOrderFrom['Plan']['step'] *= 2;
                $activeOrderFrom['Plan']['sensor_id'] = $sensorTo;
                unset($activeOrderFrom['Plan']['id']);
                $this->Plan->create();
                $this->Plan->save($activeOrderFrom['Plan']);
                $activeOrderFrom['ApprovedOrder']['sensor_id'] = $sensorTo;
                $activeOrderFrom['ApprovedOrder']['plan_id'] = $this->Plan->id;
                unset($activeOrderFrom['ApprovedOrder']['id']);
                $this->ApprovedOrder->create();
                $this->ApprovedOrder->save($activeOrderFrom['ApprovedOrder']);
                $this->Log->write('Klonuojamas planas. Pakeisti duomenys: '.json_encode($activeOrderFrom));
            }
        }elseif(!empty($activeOrderTo)){ //jei mo numeriai nesutampa bet iš ir į sensoriai turi planus, arba uždarytas iš planas bet vis dar yra į planas, plana uždarome
        //elseif(empty($activeOrderFrom) && !empty($activeOrderTo)){ //vadinas $sensorFrom planas uzsidares, o $sensorTo dar aktyvus, todel uzdarome ji, kad viskas sutaptu
            $this->ApprovedOrder->id = $activeOrderTo['ApprovedOrder']['id'];
            $this->Log->write('Uzdaromas klonuotas planas: '.json_encode($this->ApprovedOrder->read()));
            $this->ApprovedOrder->save(array('status_id'=>ApprovedOrder::STATE_COMPLETE));
            $this->ApprovedOrder->create();
        }
    }
	
	
}