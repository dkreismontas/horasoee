<?php
App::import('Vendor', 'pChart/autoload');
use pChart\pCharts;
use pChart\pColor;
use pChart\pDraw;

Class LastShiftReportPdf extends KggroupAppModel{

    CONST ROUNDING = 2;

    private static $downtimes = array(//pirmas key yra sensor_id
        24 => array(
            "pam_start" => array('id'=>1063, 'duration'=>10),
            "pam_end" => array('id'=>1064, 'duration'=>15),
            "technical_parents" => array('id'=>array(480)),
            "technological_parents" => array('id'=>array(483)),
            "exploitation_parents" => array('id'=>array(477)),
            "cleaning" => array('id'=>471, 'duration'=>120),
            "changeovers" => array(
                array('id'=>473,'duration'=>10),
                array('id'=>1121, 'duration'=>10)
            ),
            "measuring" => array('id'=> 494, 'duration'=>3)
        ),
        23 => array(
            "pam_start" => array('id'=>1063, 'duration'=>10),
            "pam_end" => array('id'=>1064, 'duration'=>15),
            "technical_parents" => array('id'=>array(480)),
            "technological_parents" => array('id'=>array(483)),
            "exploitation_parents" => array('id'=>array(477)),
            "cleaning" => array('id'=>471, 'duration'=>120),
            "changeovers" => array(
                array('id'=>473, 'duration'=>10),
                array('id'=>1121, 'duration'=>10)
            ),
            "measuring" => array('id'=> 494, 'duration'=>3)
        ),
        21 => array(
            "pam_start" => array('id'=>1204, 'duration'=>10),
            "pam_end" => array('id'=>1205, 'duration'=>15),
            "technical_parents" => array('id'=>array(478)),
            "technological_parents" => array('id'=>array(481)),
            "exploitation_parents" => array('id'=>array(475)),
            "cleaning" => array('id'=>472, 'duration'=>120),
            "changeovers" => array(
                array('id'=>474, 'duration'=>10),
            ),
            "experiments" => array('id'=>array(485)),
        ),
        22 => array(
            "pam_start" => array('id'=>1061, 'duration'=>10),
            "pam_end" => array('id'=>1062, 'duration'=>15),
            "technical_parents" => array('id'=>array(479)),
            "technological_parents" => array('id'=>array(482)),
            "exploitation_parents" => array('id'=>array(476)),
            "cleaning" => array('id'=>484, 'duration'=>120),
            "changeovers" => array(
                array('id'=>1043, 'duration'=>30),
                array('id'=>1120, 'duration'=>10),
                array('id'=>1089, 'duration'=>180),
                array('id'=>1042, 'duration'=>120),
                array('id'=>1090, 'duration'=>360),
            ),
            "measuring" => array('id'=> 452, 'duration'=>3),
            "experiments" => array('id'=>array(486))
        ),
        3 => array(
            "pam_start" => array('id'=>168, 'duration'=>10),
            "pam_end" => array('id'=>832, 'duration'=>15),
            "technical_parents" => array('id'=>array(157,154)),
            "technological_parents" => array('id'=>array(158,155)),
            "exploitation_parents" => array('id'=>array(156)),
            "cleaning" => array('id'=>101, 'duration'=>360),
            "changeovers" => array(
                array('id'=>918,'duration'=>90),
                array('id'=>103, 'duration'=>15)
            )
        ),
        4 => array(
            "pam_start" => array('id'=>168, 'duration'=>10),
            "pam_end" => array('id'=>832, 'duration'=>15),
            "technical_parents" => array('id'=>array(157,154)),
            "technological_parents" => array('id'=>array(158,155)),
            "exploitation_parents" => array('id'=>array(156)),
            "cleaning" => array('id'=>101, 'duration'=>360),
            "changeovers" => array(
                array('id'=>918,'duration'=>90),
                array('id'=>103, 'duration'=>15)
            )
        ),
        6 => array(
            "pam_start" => array('id'=>168, 'duration'=>10),
            "pam_end" => array('id'=>832, 'duration'=>15),
            "technical_parents" => array('id'=>array(157,154)),
            "technological_parents" => array('id'=>array(158,155)),
            "exploitation_parents" => array('id'=>array(156)),
            "cleaning" => array('id'=>101, 'duration'=>360),
            "changeovers" => array(
                array('id'=>918,'duration'=>90),
                array('id'=>103, 'duration'=>15)
            )
        ),
        5 => array(
            "pam_start" => array('id'=>169, 'duration'=>15),
            "pam_end" => array('id'=>1202, 'duration'=>15),
            "technical_parents" => array('id'=>array(163)),
            "technological_parents" => array('id'=>array(164)),
            "exploitation_parents" => array('id'=>array(162)),
            "cleaning" => array('id'=>100, 'duration'=>660),
            "changeovers" => array(
                array('id'=>178,'duration'=>60),
                array('id'=>177, 'duration'=>120),
                array('id'=>176, 'duration'=>25),
                array('id'=>179, 'duration'=>60),
            )
        ),
        7 => array(
            "pam_start" => array('id'=>173, 'duration'=>30),
            "pam_end" => array('id'=>1203, 'duration'=>15),
            "technical_parents" => array('id'=>array(171)),
            "technological_parents" => array('id'=>array(172)),
            "exploitation_parents" => array('id'=>array(170)),
            "cleaning" => array('id'=>175, 'duration'=>180),
            "changeovers" => array(
                array('id'=>138,'duration'=>30),
            )
        ),
    );

    public function getSensorDowntimesSettings($sensorId){
        return self::$downtimes[$sensorId]??array();
    }

    public function extractDeeperIds($parentIds, Array $tree){
        $parentIds = is_array($parentIds)?$parentIds:array($parentIds);
        $ids = array();
        foreach ($parentIds as $parentId) {
            $childs = array_filter($tree, function ($array) use ($parentId) {
                return array_key_exists($parentId, $array);
            });
            $ids = array_merge($ids, array_keys($childs));
        }
        return array_unique($ids);
    }

    public function combineProblemsData(Array $problemsData, Array $sensorData, Array $problemsTreeList, Array $downtimesTypes, Array $dismissProblemsForTheoricalQuantity){
        $problemSettings = $this->getSensorDowntimesSettings($sensorData['Sensor']['id']);
        $theoryQuantity = array_filter($problemsData, function($data)use($dismissProblemsForTheoricalQuantity){
            return !in_array($data['Problem']['id'], $dismissProblemsForTheoricalQuantity) && !in_array($data['TransitionProblem']['id'], $dismissProblemsForTheoricalQuantity);
        });
        $workIntervals = array_filter($problemsData, function($data){
            return !$data['Problem']['id'];
        });
        $finalData['theorical_quantity'] = !empty($theoryQuantity)?number_format(round(array_sum(Set::extract('/0/theorical_quantity', $theoryQuantity)), self::ROUNDING),0,'.',''):0;
        $finalData['fact_quantity'] = !empty($workIntervals)?round(current($workIntervals)[0]['quantity'],self::ROUNDING):0;
        $shiftStartData = self::getParametrsThroughNorm($problemSettings['pam_start']['id'], $sensorData, $problemsData, 'exploitation_parents', $problemSettings);
        if(!empty($shiftStartData)){
            $finalData['shift_start_duration'] = bcdiv($shiftStartData['duration'], 60, 2);
            $finalData['shift_start_quantity'] = $shiftStartData['theorical_quantity'];
            $finalData['shift_start_norm'] = bcdiv($shiftStartData['norm'], 60,2);
        }
        $shiftEndData = self::getParametrsThroughNorm($problemSettings['pam_end']['id'], $sensorData, $problemsData, 'exploitation_parents', $problemSettings);
        if(!empty($shiftEndData)){
            $finalData['shift_end_duration'] = bcdiv($shiftEndData['duration'], 60, 2);
            $finalData['shift_end_quantity'] = $shiftEndData['theorical_quantity'];
            $finalData['shift_end_norm'] = bcdiv($shiftEndData['norm'], 60,2);
        }
        //susirasome kokios prastovos skaiciuosis per norma
        $calculateThrouNormIds = array_merge(
            is_array($problemSettings['cleaning']['id'])?$problemSettings['cleaning']['id']:array($problemSettings['cleaning']['id']),
            Set::extract('/id', $problemSettings['changeovers']),
            isset($problemSettings['measuring'])?Set::extract('/id',$problemSettings['measuring']):array(),
        );
        $exceedingIds = array();
        //susirandame sarasa prastovu, skaiciuojamu per norma, kurios yra virsijimai
        foreach($calculateThrouNormIds as $calculateThrouNormId){
            if(preg_match('/'.$calculateThrouNormId.'_(\d+_\d+)/', $sensorData['Sensor']['new_fproblem_when_current_exceeds'], $match)){
                $exceedingIds[] = explode('_', $match[1])[1];
            }
        }
        foreach($downtimesTypes as $downtimesType => $downtimeLabel){
            if(!isset($problemSettings[$downtimesType]['id'])){
                $idsList = Set::extract('/id', $problemSettings[$downtimesType]);
            }else {
                $idsList = is_array($problemSettings[$downtimesType]['id']) ? $problemSettings[$downtimesType]['id'] : array($problemSettings[$downtimesType]['id']);
            }
            $downtimesIds = $this->extractDeeperIds($idsList, $problemsTreeList);
            $downtimes = array_filter($problemsData, function ($data) use ($downtimesIds) {
                return in_array($data['Problem']['id'], $downtimesIds) || in_array($data['TransitionProblem']['id'], $downtimesIds);
            });
            if(empty($downtimes)){ continue; }
            foreach ($downtimes as $downtime) {
                $problemId = $downtime['TransitionProblem']['id'] > 0? $downtime['TransitionProblem']['id'] : $downtime['Problem']['id'];
                if(in_array($problemId,$exceedingIds)){ continue; }
                $problemName = trim($downtime['TransitionProblem']['name'])?$downtime['TransitionProblem']['name']:$downtime['Problem']['name'];
                if(in_array($problemId, $calculateThrouNormIds)){//sio tipo prastovos skaiciuojamas kaip pamainos pradzios/pabaigos
                    $calculateThrouNormData = self::getParametrsThroughNorm($problemId, $sensorData, $problemsData, $downtimesType, $problemSettings);
                    if(!empty($calculateThrouNormData)){
                        $calculateThrouNormData['problem_name'] = Settings::translate($problemName);
                        $finalData[$downtimesType][] = $calculateThrouNormData;
                    }
                    continue;
                }
                $finalData[$downtimesType][] = array(
                    'problem_id' => $problemId,
                    'problem_name' => Settings::translate($problemName),
                    'duration'=>$downtime[0]['duration'],
                    'theorical_quantity'=>number_format(round($downtime[0]['theorical_quantity'], self::ROUNDING),0,'.',''),
                    'transitions_count' => (int)$downtime[0]['transitions_count']
                );
            }
            if(!isset($finalData[$downtimesType])){ continue; }
            usort($finalData[$downtimesType], function ($a, $b) {
                return ($a['duration'] < $b['duration']);
            });
        }
//        if(isset($finalData['changeovers'])){ //sumaziname pamainos kieki per tiek, kiek leidziama normoje (perejimu kiekis padauginamas is normos)
//            $finalData['theorical_quantity'] -= array_sum(array_map('intval', Set::extract('/norm', $finalData['changeovers'])));
//        }
        return $finalData;
    }

    private static function getParametrsThroughNorm(Int $problemId, Array $sensorData, Array $problemsData, String $downtimesType, Array $problemSettings){
        if(isset($problemSettings['measuring']) && $problemSettings['measuring']['id'] == $problemId){//34. Ilgiau nei nurodytas laiko intervalas trunkanti prastova virsta į kitą, nurodytą prastovą
            $problemExceedsSetting = $sensorData['Sensor']['new_fproblem_when_current_exceeds2'];
            $finalData['measuring'] = 1;
        }else{//32. Pamainos mastu ilgiau nei nurodytas laiko intervalas trunkanti prastova virsta į kitą...
            $problemExceedsSetting = $sensorData['Sensor']['new_fproblem_when_current_exceeds'];
        }

        if(preg_match('/'.$problemId.'_(\d+_\d+)/', $problemExceedsSetting, $match)){
            $normS = explode('_', $match[1])[0] * 60;
            $exceedingId = explode('_', $match[1])[1];
            $includeIds = array($problemId, $exceedingId);
            $shiftIntervals = array_filter($problemsData, function($data)use($includeIds){
                return in_array($data['Problem']['id'], $includeIds) || in_array($data['TransitionProblem']['id'], $includeIds);
            });
            $step = !empty($shiftIntervals)?bcdiv(current($shiftIntervals)['Plan']['step'], 3600, 6):0;
            $transitionsCount = !empty($shiftIntervals)?array_sum(array_map('intval',Set::extract('/0/transitions_count', $shiftIntervals))):0;
            $transitionsCount = $transitionsCount > 0? $transitionsCount:1;
            if($downtimesType != 'exploitation_parents' || (isset($problemSettings['measuring']) && $problemSettings['measuring']['id'] == $problemId)) {
                $normS *= $transitionsCount;
            }
            $finalData['duration'] = !empty($shiftIntervals)?round(array_sum(Set::extract('/0/duration', $shiftIntervals)), self::ROUNDING):0;
            $finalData['theorical_quantity'] = number_format(round(bcmul(($normS - $finalData['duration']), $step, 6), self::ROUNDING),0,'.','');
            $finalData['norm'] = $normS;
            $finalData['norm_quantity'] = number_format(round(bcmul($normS, $step, 6), self::ROUNDING), 0,'.','');
            $finalData['transitions_count'] = $transitionsCount;
            $finalData['problem_id'] = $problemId;
            return $finalData;
        };
        return array();
    }

    public function createGraph($hoursRecords){
        $myPicture = new pDraw(800,230);
        $myPicture->myData->addPoints(array_map('intval',Set::extract('/0/fact_quantity', $hoursRecords)),__('Faktinis kiekis'));
        $myPicture->myData->addPoints(array_map('intval',Set::extract('/0/plan_quantity', $hoursRecords)),__('Planinis kiekis'));
        $myPicture->myData->setPalette(__('Faktinis kiekis'),new pColor(241, 195, 35, 100));
        $myPicture->myData->setPalette(__('Planinis kiekis'),new pColor(132, 132, 132, 100));
        $myPicture->myData->setSerieProperties(__('Planinis kiekis'), ["Weight" => 1]);
        $myPicture->myData->setSerieProperties(__('Faktinis kiekis'), ["Weight" => 1]);
        $myPicture->myData->setAxisName(0,__('Kiekis'));
        $myPicture->myData->setAbscissaName(__('Valandos'));
        $myPicture->myData->addPoints(Set::extract('/0/hours', $hoursRecords),"Labels");
        $myPicture->myData->setSerieDescription("Labels","Months");
        $myPicture->myData->setAbscissa("Labels");
        /* Turn off Anti-aliasing */
        $myPicture->setAntialias(FALSE);
        /* Add a border to the picture */
        //$myPicture->drawRectangle(0,0,699,229,["Color"=>new pColor(0)]);
        /* Write the chart title */
        $myPicture->setFontProperties(["FontName"=>APP.'Vendor/pChart/fonts/Cairo-Regular.ttf',"FontSize"=>14]);
        $myPicture->drawText(50,28,__('Valandinės tendencijos grafikas'),["FontSize"=>20,"Align"=>TEXT_ALIGN_MIDDLELEFT]);
        //$myPicture->drawText(670,210,__('Valandos'),["FontSize"=>12,"Align"=>TEXT_ALIGN_MIDDLELEFT]);
        /* Set the default font */
        $myPicture->setFontProperties(["FontSize"=>12]);
        /* Define the chart area */
        $myPicture->setGraphArea(60,40,750,200);
        /* Draw the scale */
        $myPicture->drawScale(["XMargin"=>10,"YMargin"=>10,"Floating"=>TRUE,"GridColor"=>new pColor(200),"DrawSubTicks"=>TRUE,"CycleBackground"=>TRUE]);
        /* Turn on Anti-aliasing */
        $myPicture->setAntialias(TRUE);
        /* Draw the line chart */
        (new pCharts($myPicture))->drawLineChart(
            //array("DisplayValues"=>TRUE)
        );
        /* Write the chart legend */
        $myPicture->drawLegend(450,20,["Style"=>LEGEND_NOBORDER,"Mode"=>LEGEND_HORIZONTAL]);
        $graphName = sprintf('files/%s.png',uniqid('pgraph'));
        //$graphName = sprintf('files/%s.png','nuotrauka');
        $myPicture->render(WWW_ROOT.$graphName);
        return $graphName;
    }

    public function sendXlsReportToFtp($xlsReportPath){
        $ftpLogin = 'shifts_report_xls';
        $ftpPass = 'FUqnRqucWPYMg7Q8Uyut';
        $ftpServer = '185.193.26.239';
        $ftpPort = '21';
        $filePath = '/';
        $ftpConn = ftp_connect($ftpServer, $ftpPort, 10) or die("Could not connect to $ftpServer");
        $login = ftp_login($ftpConn, $ftpLogin, $ftpPass) or die("Could not login");;
        ftp_set_option($ftpConn, FTP_TIMEOUT_SEC, 1800);
        ftp_pasv($ftpConn, true);
        if (ftp_nlist($ftpConn, $filePath) == false) {
            @ftp_mkdir($ftpConn, $filePath);
        }
        $file = fopen($xlsReportPath, "r+");
        ftp_fput($ftpConn, $filePath . current(array_reverse(explode('/', $xlsReportPath))), $file, FTP_BINARY);
        fclose($file);
        ftp_close($ftpConn);
        unlink($xlsReportPath);
    }

    public function removeOldLocalFiles($dir){
        $deleteIfOlder = (60 * 60 * 24) * 60; // (60 * 60 * 24) = 1 day
        $files = scandir($dir);
        foreach($files as $f) {
            if($f == '..' || $f == '.') {
                continue;
            }
            $file_date = filemtime($dir.$f);
            if($file_date < (time() - $deleteIfOlder)) {
                unlink($dir.$f);
            }
        }
    }

}