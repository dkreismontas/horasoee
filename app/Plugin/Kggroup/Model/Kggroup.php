<?php
class Kggroup extends AppModel{
     
     public function Record_calculateOee_Hook(&$oeeParams, $data){
         if(isset($data['sensor_id']) && in_array($data['sensor_id'], array(21,22,23,24))){
             $oeeParams['operational_factor'] = $data['shift_length']>0?bcdiv($data['count_delay'], $data['shift_length'], 6):0; 
             $oeeParams['oee'] = bcmul(bcmul($oeeParams['exploitation_factor'],$oeeParams['operational_factor'],6), $oeeParams['quality_factor'],6) * 100;
             $oeeParams['oee_exclusions'] = 100 * bcmul(bcmul($oeeParams['exploitation_factor_exclusions'],$oeeParams['operational_factor'],6),$oeeParams['quality_factor'],6);
         }
     }
     
     public function Record_getLastMinuteSpeed_Hook($fSensor){
        if($fSensor['Sensor']['id'] == 2){ //kgalytus
	        $time = time();
	        $quantity = 'quantity';
	        $recordModel = ClassRegistry::init('Record');
	        $speed = $recordModel->find('first', array(
	            'fields'=>array(
	                'SUM(IF(Record.created >= \''.date('Y-m-d H:i:s', $time-$recordModel::RECORD_AGE-59).'\','.$quantity.',0)) / 3 AS last_minute_quantities',
	            ),'recursive'=>-1,
	            'conditions'=>array(
	                'Record.created <='=>date('Y-m-d H:i:s', $time-$recordModel::RECORD_AGE), 
	                'Record.created >'=>date('Y-m-d H:i:s', $time-300), 
	                'Record.sensor_id'=>$fSensor['Sensor']['id']
	            )
	        ));
	        return isset($speed[0]['last_minute_quantities'])?number_format($speed[0]['last_minute_quantities'],2,'.',''):0;
	    }
		return false;
    }
     
    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        if($fSensor['Sensor']['id'] == 2){ //kgalytus
	        if(isset($currentPlan['ApprovedOrder'])){
	            $currentPlan['Plan']['performance'] = $this->Record_getLastMinuteSpeed_Hook($fSensor)*1.25*60;
	            $currentPlan['Plan']['step'] /= 3;
	        }
	        foreach($allPlans as &$allPlan){
	            $allPlan['Plan']['step_no_avg'] = $allPlan['Plan']['step']/60;
	        }
	    }
        if(in_array($fSensor['Sensor']['id'], array(13,14)) && !empty($currentPlan)){//Kgpasarai
            $currentPlan['Plan']['step_unit_title'] = str_replace(array('vnt/min','vnt/h'),array('kg/min','kg/h'),$currentPlan['Plan']['step_unit_title']);
        }
        if(!empty($currentPlan) && isset($fSensor['Factory']) && $fSensor['Factory']['id'] == 5){ //KgSlaituva
            if($fSensor['Sensor']['type'] == 1){
                $recordModel = ClassRegistry::init('Record');
                $quantity = $recordModel->find('first', array(
                    'fields'=>array('SUM(Record.unit_quantity) AS total'), 
                    'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
                ));
                if($quantity[0]['total'] > 0){
                    $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.','').' kg'; 
                } 
            }else{
                $quantityOutput['quantity'] = str_replace('vnt.',' kg', $quantityOutput['quantity']);
            }
            $currentPlan['Plan']['step_unit_title'] = 'kg/min';
        }
        if(!empty($currentPlan) && isset($fSensor['Factory']) && in_array($fSensor['Factory']['id'],array(7,8))){ //7 Vpaukstynas ir 8 Vitomek
            $recordModel = ClassRegistry::init('Record');
            $quantity = $recordModel->find('first', array(
                'fields'=>array('SUM(Record.unit_quantity) AS total'), 
                'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
            ));
            $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.','').' '.__('vnt'); 
        }
        if(in_array($fSensor['Sensor']['id'], array(42,45))){
            $dashboardModel = ClassRegistry::init('DashboardsCalculation');
            $prevShiftCalc = $dashboardModel->find('first', array(
                //'fields'=>array('DashboardsCalculation.total_quantity'),
                'conditions'=>array(
                    'DashboardsCalculation.sensor_id'=>$fSensor['Sensor']['id'],
                    'DashboardsCalculation.shift_id >'=>0
                ),'order'=>array('DashboardsCalculation.shift_id DESC')
            ));
            if(!empty($prevShiftCalc)) {
                $fSensor['Sensor']['KGprevShiftCount'] = __('Praėjusios pam kiekis') . ': ' . round($prevShiftCalc['DashboardsCalculation']['total_quantity']);
            }
        }
    }

    public function Record_calculateMainFactorsInHourAfterLoop_Hook(&$dataInHour, &$fSensor, &$oeeParams, &$dcHoursInfoBlocks, $hourNr){
        if(in_array($fSensor['Sensor']['id'], array(21,22,23,24))){//Kgagppgp
            if(!isset($dcHoursInfoBlocks['operational_factor'])){
                $dcHoursInfoBlocks['operational_factor'] = array('title'=>__('Efekt. koef'), 'addSpace'=>70,'value'=>array());
            }
            $operationalFactor = $dataInHour['shift_length']>0?bcdiv($dataInHour['count_delay'], $dataInHour['shift_length'], 6):0;
            $dcHoursInfoBlocks['operational_factor']['value'][(int)$hourNr] = number_format($operationalFactor*100,0,'.','');
        }
    }
    
    public function Speed_GetSpeedHistoryBeforeSearch_Hook(&$searchParameters, &$bindings){
        $kgAlytusSensorId = array(2);
        $searchParameters['fields'][0] = '
        	GROUP_CONCAT(
        		IF(Record.plan_id IS NULL,0,
        			IF(Record.sensor_id IN ('.implode(',',$kgAlytusSensorId).'), ROUND(Product.formu_greitis/60, 2), ROUND(Plan.step/60, 2))
				)
			) AS purpose';
        $searchParameters['fields'][3] = '
        	ROUND(
        		IF(Record.sensor_id IN ('.implode(',',$kgAlytusSensorId).'), SUM(Record.quantity), SUM(IF(Sensor.plan_relate_quantity = \'quantity\',Record.quantity,Record.unit_quantity)))
        	,2) as quantity';
		$bindings['belongsTo']['Product'] = array('foreignKey'=>false,'conditions'=>array('Plan.product_id = Product.id'));
    }
    
    public function Workcenter_afterBeforeRender_Hook(&$bodyClass, &$fSensor){
    	if($fSensor['Sensor']['id'] == 2){ //kgalytus
        	$bodyClass[] = '/../Kggroup/css/Kgalytus_bare.css';
		}
     }
    
    public function WorkCenter_index_Hook(&$data, &$fSensor){
        switch($fSensor['Factory']['id']){
            case 1://KGALYTUS
                $data['structure'] = array(
                    'Plan.production_name' => __('Gaminio pavadinimas'),
                    'Plan.production_code' => __('Gaminio kodas'),
                    'Plan.performance' => __('Našumas kg/val.'),
                );
            break;
            case 2: //KG Kėdainiai
                $data['structure'] = array(
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, vnt/val'),
                );
            break;
            case 3: //KG Malūnas
                $data['structure'] = array(
                    'Plan.mo_number' => __('Užsakymo numeris'), 
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, vnt/h'),
                    'Plan.quantity' => __('Kiekis, vnt.'),
                    'Plan.partial_quantity' => __('Deklaruotas kiekis, vnt.'),
                );
            break;
            case 4: //KG Pašarai
                $data['structure'] = array(
                    'Plan.mo_number' => __('Užsakymo numeris'), 
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, m/min'),
                    'Plan.quantity' => __('Kiekis, vnt.'),
                    'Plan.partial_quantity' => __('Deklaruotas kiekis, vnt.'),
                );
                if(in_array($fSensor['Sensor']['id'], array(13,14))){
                    $data['structure']['Plan.step'] = str_replace('m/min','kg/h',$data['structure']['Plan.step']);
                }
            break;
            case 5: //KG Šlaituva
                $data['structure'] = array(
                    'Plan.mo_number' => __('Užsakymo numeris'), 
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, kg/h'),
                    'Plan.quantity' => __('Kiekis, vnt.'),
                    'Plan.partial_quantity' => __('Deklaruotas kiekis, vnt.'),
                );
            break;
            case 6: //KG AGPPGP
                $data['structure'] = array(
                    'Product.production_code' => __('Gaminio kodas'), 
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, vnt/h')
                );
            break;
            case 7: //Vilniaus Paukštynas
                $data['structure'] = array(
                    'Plan.mo_number' => __('Užsakymo numeris'), 
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, vnt/h'),
                    'Plan.quantity' => __('Kiekis, vnt.'),
                    'Plan.partial_quantity' => __('Deklaruotas kiekis, vnt.'),
                );
            break;
            case 8: //Vitomek
                $data['structure'] = array(
                    'Plan.mo_number' => __('Užsakymo numeris'),
                    'Plan.production_code' => __('Kodas'),
                    'Plan.production_name' => __('Gaminys'),
                    'Plan.step' => __('Norma, vnt/h'),
                    'Plan.quantity' => __('Kiekis, t.'),
                    'Plan.client' => __('Klientas'),
                );
                break;
        }
        if(in_array($fSensor['Sensor']['id'], array(42,45))){
            $data['pluginScripts'] = 'jQuery(document).ready(function(){
                $("#gaugeBar div.oee-gauge-wrap").append("<h2 style=\"position:absolute; bottom: 0;\" class=\"gaugeTitle\">{{sensor.KGprevShiftCount}}</h2>");
            });';
        }
    }

    public function WorkCenter_BeforeUpdateStatus_Hook(&$fSensor, $user){
        if((($fSensor['Sensor']['id'] >= 25 && $fSensor['Sensor']['id'] <= 49) || in_array($fSensor['Sensor']['id'], array(13,14)))  && in_array($user['Group']['id'], array(4,5,6))){//tik operatoriu grupems
            Configure::write('graphHoursDefaultLinesCount', 12);
        }
    }
    
    public function Workcenter_beforeReturnUserSensor_Hook(&$fUser){
        if(in_array($fUser['Sensor']['id'], array(43,44,55,59))){
            $fUser['Sensor']['timezone'] = 'Europe/Minsk';
        }
        if(in_array($fUser['Sensor']['id'], array(42,45))){
            $fUser['Sensor']['timezone'] = 'Europe/Moscow';
        }
    }

    public function Dashboard_beforeExecuteSearch_Hook(&$primaryData, &$indicator, &$fieldToSearch, &$groupByDate, &$model, &$colors){
        if($primaryData['indicator1'] == 18){
            $colors['Pertrauka'] = '#647ff5';
            $problemModel = ClassRegistry::init('Problem');
            $problems = $problemModel->find('list', array('fields'=>array('Problem.name'), 'conditions'=>array('Problem.id'=>array(814,815))));
            foreach($problems as $problemName){
                $problemName = Settings::translate($problemName);
                if(isset($colors[$problemName])){
                    $colors[$problemName] = '#e8970c';
                }
            }
        }
        $transitionText = __('Perėjimas');
        if(isset($colors[$transitionText])){
            $colors[$transitionText] = 'FDEE00';
        }
        if(sizeof($primaryData['sensors']) == sizeof(array_intersect($primaryData['sensors'], array(21,22,23,24)))){
            switch($primaryData[$indicator]){
            case 0: //OEE be isimciu skaiciavimas
                $qualitypart = '(IF(Sensor.calculate_quality > 0, SUM('.$model.'.good_quantity), 1) / IF(Sensor.calculate_quality > 0, SUM('.$model.'.all_quantity), 1))';
                $fieldToSearch[] = $qualitypart.' * (SUM('.$model.'.count_delay) / SUM('.$model.'.shift_length)) * (SUM('.$model.'.fact_prod_time) / SUM('.$model.'.shift_length)) * 100 AS sum';
            break;
            case 23:    //OEE su išimtimis skaiciavimas
                $qualitypart = '(IF(Sensor.calculate_quality > 0, SUM('.$model.'.good_quantity), 1) / IF(Sensor.calculate_quality > 0, SUM('.$model.'.all_quantity), 1))';
                $fieldToSearch[] = $qualitypart.' * (SUM('.$model.'.count_delay) / SUM('.$model.'.shift_length_with_exclusions)) * (SUM('.$model.'.fact_prod_time) / SUM('.$model.'.shift_length_with_exclusions)) * 100 AS sum';
            break;
            case 2: //Veiklos efektyvumo koeficientas
                if($primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $fieldToSearch[] = 'SUM('.$model.'.count_delay) / SUM('.$model.'.shift_length_with_exclusions) * 100 AS sum';
                }
            break;
        }
        }
    }
    
    // public function FoundProblem_beforeAddDowntimeTypeToOtherSameGroupSensors_Hook(&$fSensor,&$sensorsDowntimesGroupsString,&$fProblemType,&$fFoundProblem){
        // if(isset($fSensor['Factory']) && $fSensor['Factory']['id'] == 6){
            // $problemModel = ClassRegistry::init('Problem');
            // $problemsTreeList = $problemModel->parseThreadedProblemsTitles($problemModel->find('threaded'));
            // $problemTypeId = $fProblemType['Problem']['id'];
            // if(isset($problemsTreeList[$problemTypeId])){
                // if(!in_array(current(array_reverse(array_keys($problemsTreeList[$problemTypeId]))), array(481,482,483))){ //Technologinės Prastovos
                    // $sensorsDowntimesGroupsString = '';
                // }
            // }
        // }
    // }
     
}
    