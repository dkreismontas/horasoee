<style type="text/css">

</style>
<page style="font-family: freeserif; font-size: 16px;">
    <br />
    <h1 style="text-align: center;"><?php echo $sensorData['Sensor']['name'].' '.__('pamainos ataskaita') ?></h1>
    <table style="border: 0; width: 90%; margin-left: 20px;" cellpadding="0" cellspacing="2">
        <tr>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Pradžia - pabaiga'); ?>:</td>
            <td colspan="3" style="width: 75%; text-align: center"><?php echo $finalData['shift_start_end']; ?></td>
        </tr>
        <tr>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Pamainos trukmė'); ?>:</td>
            <td colspan="3" style="width: 75%; text-align: center"><?php echo gmdate("H:i:s", $finalData['shift_duration']); ?></td>
        </tr>
        <tr>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Faktinis kiekis'); ?>:</td>
            <td style="width: 25%; text-align: center"><?php echo $finalData['fact_quantity']??__('nežinoma'); ?></td>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Planinis kiekis'); ?>:</td>
            <td style="width: 25%; text-align: center"><?php
                echo $finalData['theorical_quantity']??__('nežinoma'); ?>
            </td>
        </tr>
        <tr>
            <td style="width: 25%;"></td>
            <td style="width: 25%; vertical-align: bottom; font-weight: bold; text-align: center;"><br /><?php echo __('Trukmė min.'); ?></td>
            <td style="width: 25%; vertical-align: bottom; font-weight: bold; text-align: center;"><br /><?php echo __('Norma min.'); ?></td>
            <td style="width: 25%; font-weight: bold; vertical-align: bottom; text-align: center;"><br /><?php echo __('Sutaupytas(+)/prarastas(-) kiekis'); ?></td>
        </tr>
        <tr>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Pamainos pradžia'); ?>:</td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_start_duration']??__('nežinoma'); ?></td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_start_norm']??__('nežinoma'); ?></td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_start_quantity']??__('nežinoma'); ?></td>
        </tr>
        <tr>
            <td style="width: 25%; font-weight: bold;"><?php echo __('Pamainos pabaiga'); ?>:</td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_end_duration']??__('nežinoma'); ?></td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_end_norm']??__('nežinoma'); ?></td>
            <td style="width: 25%; text-align: center; vertical-align: top;"><?php echo $finalData['shift_end_quantity']??__('nežinoma'); ?></td>
        </tr>
    </table>
    <br />
    <table style="border: 0; width: 90%; margin-left: 20px; text-align: center;" cellpadding="0" cellspacing="2">
    <?php
    foreach($downtimesTypes as $downtimesType => $downtimeLabel) {
        if(!isset($finalData[$downtimesType])){ continue; } ?>
            <tr>
                <td style="width: 40%; font-weight: bold; text-align: left;"><?php echo $downtimeLabel; ?></td>
                <td style="width: 20%; font-weight: bold;"><?php echo __('Trukmė'); ?></td>
                <td style="width: 20%; font-weight: bold;"><?php if($downtimesType == 'changeovers'){ ?><?php echo __('Norma | registruota kartų'); } ?></td>
                <td style="width: 20%; font-weight: bold;"><?php echo $downtimesType != 'changeovers'?__('Prarastas kiekis'):__('Sutaupytas(+)/prarastas(-) kiekis'); ?></td>
            </tr>
            <?php
            $sum = array('duration'=>0, 'theorical_quantity'=>0);
            foreach($finalData[$downtimesType] as $key => $downtime){
                $sum['duration'] += $downtime['duration'];
                if($downtimesType != 'changeovers' && !in_array($downtime['problem_id'], array(100)) && !isset($downtime['measuring'])){//Plovimui ir perejimams nereikia minuso
                    $downtime['theorical_quantity'] = 0 - $downtime['theorical_quantity'];
                }
                $sum['theorical_quantity'] += $downtime['theorical_quantity'];
                if($downtimesType == 'changeovers' && $key == 3){ break; }
                if($key == 5){ break; }
                if(isset($settings['experiments']) && in_array($downtime['problem_id'], $settings['experiments']['id'])){
                    $downtime['theorical_quantity'] = 0;
                }
            ?>
                <tr>
                    <td style="width: 40%; font-weight: bold; text-align: left;"><?php echo $downtime['problem_name']; ?></td>
                    <td style="width: 20%;"><?php echo gmdate('H:i:s',$downtime['duration']); ?></td>
                    <td style="width: 20%;"><?php if($downtimesType == 'changeovers'){ echo isset($downtime['norm'])?gmdate('H:i:s',$downtime['norm']).' | '.$downtime['transitions_count'].'x':''; } ?></td>
                    <td style="width: 20%; text-align: center;"><?php echo $downtime['theorical_quantity']; ?></td>
                </tr>
            <?php } ?>
            <tr>
                <td style="font-weight: bold; text-align: right;"><?php echo __('Suma'); ?>:</td>
                <td style="font-weight: bold;"><?php echo gmdate('H:i:s',$sum['duration']); ?></td>
                <td></td>
                <td style="font-weight: bold; text-align: center;"><?php echo $sum['theorical_quantity']; ?></td>
            </tr>
            <tr><td></td></tr>
    <?php } ?>
    </table>
    <br />
    <br />
    <img src="<?php echo Router::url('/'.$graphName, true); ?>" />
    <br />
    <br />
    <table style="border: 1px solid #000000; width: 90%; margin-left: 20px; border-collapse: collapse; position:relative; left: -30px;">
    <?php
    foreach(array('hours'=>__('Val.'), 'fact_quantity'=>__('Fakt. kiekis'), 'plan_quantity'=>__('Plan. kiekis')) as $rowKey => $rowName){
        ?>
        <tr>
        <td style="border: 1px solid #000000;"><?php echo $rowName; ?></td>
        <?php
        foreach($hoursRecords as $hourRecord){ ?>
            <td style="border: 1px solid #000000; padding: 5px; text-align: center;"><?php echo $hourRecord[0][$rowKey] ?></td>
        <?php } ?>
        </tr>
    <?php } ?>
    </table>
</page>