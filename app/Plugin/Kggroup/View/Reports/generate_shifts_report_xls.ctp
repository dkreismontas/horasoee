<?php
//ob_end_clean();
//error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
    ),
);
$centerAlignBoldStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$rightAlignBoldStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$borderStyleArrayBold = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);
if(!function_exists('nts')){
    function nts($col) {// number to letter cordinates
        return PHPExcel_Cell::stringFromColumnIndex($col);
    }
    function cts($row, $col) {// number cordinates to letter
        return nts($row) . $col;
    }
}
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Pamaininė ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $sensorData['Sensor']['name'].' '.__('pamainos ataskaita'));
$activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr+4).$rowNr)->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
$rowNr += 2; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pradžia - pabaiga').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_start_end']);
$activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr+3).$rowNr)->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$rowNr ++; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamainos trukmė').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, gmdate("H:i:s", $finalData['shift_duration']));
$activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr+3).$rowNr)->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$rowNr ++; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Faktinis kiekis').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['fact_quantity']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Planinis kiekis').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['theorical_quantity']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);;
$rowNr ++; $colNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Trukmė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Norma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Sutaupytas(+)/prarastas(-) kiekis'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
$rowNr ++; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamainos pradžia').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_start_duration']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_start_norm']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_start_quantity']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$rowNr ++; $colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamainos pabaiga').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_end_duration']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_end_norm']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $finalData['shift_end_quantity']??__('nežinoma'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
foreach($downtimesTypes as $downtimesType => $downtimeLabel) {
    $rowNr += 2; $colNr = 0;
    if (!isset($finalData[$downtimesType]) || empty($finalData[$downtimesType])) {
        continue;
    }
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtimeLabel)->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Trukmė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtimesType == 'changeovers'?__('Norma | registruota kartų (x)'):'')->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtimesType != 'changeovers'?__('Prarastas kiekis'):__('Sutaupytas(+)/prarastas(-) kiekis'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
    $sum = array('duration'=>0, 'theorical_quantity'=>0);
    foreach($finalData[$downtimesType] as $key => $downtime){
        $colNr = 0; $rowNr++;
        $sum['duration'] += $downtime['duration'];
        if($downtimesType != 'changeovers'){
            $downtime['theorical_quantity'] = 0 - $downtime['theorical_quantity'];
        }
        $sum['theorical_quantity'] += $downtime['theorical_quantity'];
        if($downtimesType == 'changeovers' && $key == 3){ break; }
        if($key == 5){ break; }
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtime['problem_name'])->getStyle(cts($colNr++, $rowNr));
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, gmdate('H:i:s',$downtime['duration']))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtimesType == 'changeovers' && isset($downtime['norm'])?gmdate('H:i:s',$downtime['norm']).' | '.$downtime['transitions_count'].'x':'')->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $downtime['theorical_quantity'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
    }
    $rowNr ++; $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Suma').':')->getStyle(cts($colNr++, $rowNr))->applyFromArray($rightAlignBoldStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, gmdate('H:i:s',$sum['duration']))->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
    $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $sum['theorical_quantity'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignBoldStyleArray);
}
$rowNr += 2;
for ($col = 0; $col != 4; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}
$activeSheet->getColumnDimension(nts(0))->setWidth(30);

foreach(array('hours'=>__('Valandos'), 'fact_quantity'=>__('Faktinis kiekis'), 'plan_quantity'=>__('Planinis kiekis')) as $rowKey => $rowName) {
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $rowName)->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArrayBold);
    foreach ($hoursRecords as $hourRecord) {
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $hourRecord[0][$rowKey])->getStyle(cts($colNr++, $rowNr))->applyFromArray($borderStyleArray);
    }
    $rowNr++;
}
$objDrawing->setName('Sample image');
$objDrawing->setImageResource(@imagecreatefrompng(WWW_ROOT.$graphName));
$objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
$objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
$objDrawing->setHeight(230);
$objDrawing->setWidth(800);
$objDrawing->setCoordinates('A'.($rowNr += 2));
$objDrawing->setWorksheet($activeSheet);
$rowNr += 2;

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save($xlsReportPath);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . current(array_reverse(explode('/', $xlsReportPath))));
    $objWriter->save('php://output');
    die();
}

