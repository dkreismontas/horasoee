<?php

App::uses('AppController', 'Controller');

class WorksListController extends AppController {
    public $components = array('Session', 'Paginator');
    public $uses = array('WorkEntry','Line');

    /** @requireAuth Technikų registras: peržiūrėti sukurtus darbus */ 
    public function entries() {
        $this->requestAuth(true);
        $this->loadModel('WorksList.WorkEntry');
        $this->loadModel('WorksList.WorkType');
//        $this->WorkEntry->insertPermissions();
        try {
            $this->WorkEntry->getInfo();
        } catch(Exception $e) {
            if($e->getCode() == 500) {
                $this->WorkEntry->createSchema();
            }
        }

        $limit = isset($this->request->params['named']['limit']) ? $this->request->params['named']['limit'] : null;
        if (!$limit) $limit = 100;
        $this->WorkEntry->bindModel(array('belongsTo'=>array('User','Plan','WorkType','Sensor')));
        $arr = array('limit' => $limit, 'order' => array('id'=>'DESC'), 'conditions'=>array('WorkEntry.sensor_id'=>Configure::read('user')->selected_sensors));
        $this->Paginator->settings = $arr;
        $list = $this->Paginator->paginate('WorkEntry');

        $this->set(array(
            'h1_for_layout'=>__('Technikų darbų registras'),
            'title_for_layout' => __("Darbų sąrašas"),
            'list' => $list,
            'typeOptions' => Sensor::getTypes(),
        ));

    }


    /** @requireAuth Technikų registras: sąrašas */
    public function types() {
        $this->requestAuth(true);
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('Line');
        try {
            $this->WorkType->getInfo();
        } catch(Exception $e) {
            if($e->getCode() == 500) {
                $this->WorkType->createSchema();
            }
        }
        $availableLines = $this->Line->getAsSelectOptions(false, true);
        $list = $this->WorkType->find('all',array('conditions'=>array('WorkType.line_id'=>$availableLines)));

        $this->set(array(
            'h1_for_layout'=>__('Technikų darbų tipai'),
            'title_for_layout' => __("Darbų sąrašas"),
            'list' => $list,
            'typeOptions' => Sensor::getTypes(),
            'parentOptions' => $this->WorkType->getAsSelectOptions(true),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
        ));
    }

    /** @requireAuth Technikų registras: sukurti */
    public function create() {
        $this->requestAuth(true);
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('Line');

        if(!empty($this->request->data) && !empty($this->request->data['WorkType']['name'])) {
            if($this->WorkType->save(array(
                'WorkType'=>array(
                    'name'=>$this->request->data['WorkType']['name'],
                    'sensor_type'=>$this->request->data['WorkType']['sensor_type'],
                    'parent_id'=>$this->request->data['WorkType']['parent_id'],
                    'line_id'=>$this->request->data['WorkType']['line_id'],
                    )
            ))) {
                $this->redirect('/WorksList/types');
            }
        }

        $this->set(array(
            'h1_for_layout'=>__('Technikų darbų tipai'),
            'title_for_layout' => __("Sukurti"),
            'parentOptions' => $this->WorkType->getAsSelectOptions(true),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
            'typeOptions' => Sensor::getTypes(),
        ));

    }

    /** @requireAuth Technikų registras: redaguoti */
    public function edit() {
        $this->requestAuth(true);
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('Line');

        if(!empty($this->request->data) && !empty($this->request->data['WorkType']['name'])) {
            $this->WorkType->create(false);
            if($this->WorkType->save(array(
                'WorkType'=>array(
                    'id'=>$this->request->data['WorkType']['id'],
                    'name'=>$this->request->data['WorkType']['name'],
                    'sensor_type'=>$this->request->data['WorkType']['sensor_type'],
                    'parent_id'=>$this->request->data['WorkType']['parent_id'],
                    'line_id'=>$this->request->data['WorkType']['line_id'],
                )
            ))) {
                $this->redirect('/WorksList/types');
            }
        }

        $this->request->data = $this->WorkType->findById($this->request->params['id']);
        $this->set(array(
            'h1_for_layout'=>__('Technikų darbų tipai'),
            'title_for_layout' => "Redaguoti",
            'typeOptions' => Sensor::getTypes(),
            'parentOptions' => $this->WorkType->getAsSelectOptions(true,array($this->request->params['id'])),
            'lineOptions' => $this->Line->getAsSelectOptions(true),
            'deleteUrl' => "/WorksList/".$this->request->params['id']."/delete",
        ));
    }

    /** @requireAuth Technikų registras: ištrinti */
    public function delete() {
        $this->requestAuth(true);
        $this->loadModel('WorksList.WorkType');
        $this->WorkType->delete($this->request->params['id'],false);
        $this->redirect('/WorksList/types');
    }
    
    /** @requireAuth Technikų registras: generuoti xls ataskaitą */
    public function generateXLSreport() {
        $this->requestAuth(true);
        $start_date = "1970-01-01";
        if(isset($this->request->data['start_date']) && !empty($this->request->data['start_date'])) {
            $start_date = $this->request->data['start_date'];
        }
        $end_date = "2050-01-01";
        if(isset($this->request->data['end_date']) && !empty($this->request->data['end_date'])) {
            $end_date = $this->request->data['end_date'];
        }
        $this->loadModel('WorksList.WorkEntry');
        $report = $this->WorkEntry->generateReport($start_date,$end_date);
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Ataskaita.xls"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT');
        header ('Cache-Control: cache, must-revalidate');
        header ('Pragma: public');
        $objWriter = PHPExcel_IOFactory::createWriter($report, 'Excel5');
        $objWriter->save('php://output');
        exit();
    }

    /** @requireAuth Technikų registras: atverti darbo puslapį */
    public function works() {
        $this->requestAuth(true);
        $this->layout = 'works';
//        $this->loadModel('WorksList.WorkEntry');
        $this->set(array(
            'bodyClass'=>'',
            'title_for_layout' => __('Įrašai'),
            'newUrl' => Router::url('0/edit'),
            'editUrl' => Router::url('%d/edit'),
            'removeUrl' => Router::url('%d/remove'),
        ));
    }

    public function tryAuth() {
        $this->layout = false;
        if(empty($this->request->query)) {
            die();
        }
        $code = $this->request->query['code'];
        $results = $this->getUserByTechCode($code);
        if(empty($code) || empty($results)) {
            echo "{\"hasErrors\":1,\"errors\":\"Techniko kodas neatpažintas\"}";
            die();
        }

        echo json_encode($results['User']);
        die();
    }

    private function getUserByTechCode($code) {
        $this->loadModel('User');
        return $this->User->findByTechCode($code,array('id','first_name','last_name'));
    }

    public function startWork() {
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('WorksList.WorkEntry');
        if(!array_key_exists('type',$this->request->query) || !array_key_exists('code',$this->request->query) || !array_key_exists('wc',$this->request->query)) {
            echo "{\"hasErrors\":1,\"errors\":\"Something's not defined\"}";
            die();
        }
        $techCode = $this->request->query['code'];
        $sensorType = $this->request->query['type'];
        $workCenterId = $this->request->query['wc'];

        $user = $this->getUserByTechCode($techCode);
        if(empty($user)) {
            echo "{\"hasErrors\":1,\"errors\":\"Technikas nerastas\"}";
            die();
        }

        $this->WorkEntry->bindModel(array('belongsTo'=>array('WorkType')));
        $busy = $this->WorkEntry->find('first',array(
            'fields'=>array('id','WorkType.sensor_type','status','user_id','WorkType.name'),
            'conditions' => array(
                'status'=>WorkEntry::$STATUS_CURRENT,
                'sensor_id'=>$workCenterId
            )
        ));
        if(!empty($busy)) {
            echo "{\"hasErrors\":1,\"errors\":\"Darbas jau vykdomas\"}";
            die();
        };

        //$workEntry['WorkEntry']['work_type_id'] = $workTypeId;
        $workEntry['WorkEntry']['status'] = WorkEntry::$STATUS_CURRENT;
        $workEntry['WorkEntry']['started_at'] = date("Y-m-d H:i:s");
        $workEntry['WorkEntry']['user_id'] = $user['User']['id'];
        $workEntry['WorkEntry']['sensor_id'] = $workCenterId;
        $workEntry['WorkEntry']['plan_id'] = 0;

        $planClass = ClassRegistry::init('Plan');
        $attachedPlan = WorkEntry::findAttachedPlan($workEntry,$planClass);
        if($attachedPlan != null && !empty($attachedPlan)) {
            $workEntry['WorkEntry']['plan_id'] = $attachedPlan['Plan']['id'];
        }

        $this->WorkEntry->save($workEntry);


        $workEntry['User'] = $user;
        echo json_encode($workEntry);

        die();
    }

    public function selectWorkType() {
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('WorksList.WorkEntry');
        if(!array_key_exists('workType',$this->request->query) || !array_key_exists('type',$this->request->query) || !array_key_exists('code',$this->request->query) || !array_key_exists('wc',$this->request->query)) {
            echo "{\"hasErrors\":1,\"errors\":\"Something's not defined\"}";
            die();
        }
        $techCode = $this->request->query['code'];
        $sensorType = $this->request->query['type'];
        $workTypeId = $this->request->query['workType'];
        $workCenterId = $this->request->query['wc'];

        $user = $this->getUserByTechCode($techCode);
        if(empty($user)) {
            echo "{\"hasErrors\":1,\"errors\":\"Technikas nerastas\"}";
            die();
        }

        $workType = $this->WorkType->findById($workTypeId);
        if(empty($workType)) {
            echo "{\"hasErrors\":1,\"errors\":\"Darbas nerastas\"}";
            die();
        }
        if($workType['WorkType']['sensor_type'] != $sensorType) {
            echo "{\"hasErrors\":1,\"errors\":\"Neteisingas jutiklio tipas\"}";
            die();
        }

        $this->WorkEntry->bindModel(array('belongsTo'=>array('WorkType')));
        $busy = $this->WorkEntry->find('first',array(
            'fields'=>array('id','WorkType.sensor_type','status','user_id','WorkType.name'),
            'conditions' => array(
                'status'=>WorkEntry::$STATUS_CURRENT,
                'sensor_id'=>$workCenterId
            )
        ));
        if(!empty($busy)) {
            echo "{\"hasErrors\":1,\"errors\":\"Darbas jau vykdomas\"}";
            die();
        };

        $workEntry['WorkEntry']['work_type_id'] = $workTypeId;
        $workEntry['WorkEntry']['status'] = WorkEntry::$STATUS_CURRENT;
        $workEntry['WorkEntry']['started_at'] = date("Y-m-d H:i:s");
        $workEntry['WorkEntry']['user_id'] = $user['User']['id'];
        $workEntry['WorkEntry']['sensor_id'] = $workCenterId;
        $workEntry['WorkEntry']['plan_id'] = 0;

        $planClass = ClassRegistry::init('Plan');
        $attachedPlan = WorkEntry::findAttachedPlan($workEntry,$planClass);
        if($attachedPlan != null && !empty($attachedPlan)) {
            $workEntry['WorkEntry']['plan_id'] = $attachedPlan['Plan']['id'];
        }

        $this->WorkEntry->save($workEntry);



        $workEntry['User'] = $user;
        echo json_encode($workEntry);

        die();
    }

    public function finishWorkEntry() {
        $this->loadModel('WorksList.WorkEntry');
        if(!array_key_exists('workEntry',$this->request->query) || !array_key_exists('code',$this->request->query) || !array_key_exists('workType',$this->request->query)) {
            echo "{\"hasErrors\":1,\"errors\":\"Something's not defined\"}";
            die();
        }
        $techCode = $this->request->query['code'];
        $workEntryId = $this->request->query['workEntry'];
        $workTypeId = $this->request->query['workType'];
        $comment = $this->request->query['comment'];


        $user = $this->getUserByTechCode($techCode);
        if(empty($user)) {
            echo "{\"hasErrors\":1,\"errors\":\"User not found\"}";
            die();
        }

        $workEntry = $this->WorkEntry->findById($workEntryId);
        if(empty($workEntry)) {
            echo "{\"hasErrors\":1,\"errors\":\"Work entry not found\"}";
            die();
        }

        if($workEntry['WorkEntry']['user_id'] != $user['User']['id']) {
            echo "{\"hasErrors\":1,\"errors\":\"Work entry does not belong to user\"}";
            die();
        }

        $workEntry['WorkEntry']['ended_at'] = date("Y-m-d H:i:s");
        $workEntry['WorkEntry']['status'] = WorkEntry::$STATUS_FINISHED;
        $workEntry['WorkEntry']['work_type_id'] = $workTypeId;
        $workEntry['WorkEntry']['comment'] = $comment;

        $this->WorkEntry->save($workEntry);

        die();

    }

    public function getWorksList() {
        $this->layout = false;
        $this->loadModel('WorksList.WorkType');
        $this->loadModel('WorksList.WorkEntry');
        if(empty($this->request->query)) {
            echo "No query";
            die();
        }
        $code = $this->request->query['code'];
        $user = $this->getUserByTechCode($code);
        if(empty($user)) {
            echo "no user";
            die();
        }

        $this->WorkEntry->bindModel(array('belongsTo'=>array('WorkType')));
        $busy = $this->WorkEntry->find('first',array(
            'fields'=>array('id','WorkType.sensor_type','status','user_id','WorkType.name'),
            'conditions' => array(
                'status'=>WorkEntry::$STATUS_CURRENT,
                'sensor_id'=>$this->request->query['wc']
            )
        ));
        if(!empty($busy)) {
            $this->loadModel('User');
            $user = $this->User->findById($busy['WorkEntry']['user_id'],array('id','first_name','last_name'));
            if(empty($user)) {
                echo "{\"hasErrors\":1,\"errors\":\"Undefined user is doing work\"}";
                die();
            }
            $busy['User'] = $user['User'];

            $this->loadModel('Sensor');
            $sensor = $this->Sensor->findById($this->request->query['wc']);
            $res = $this->WorkType->find('threaded',array(
                'fields'=>array('id','parent_id','name','sensor_type'),
                'conditions' => array(
                    'sensor_type'=>$this->request->query['type'],
                    'line_id'=>$sensor['Sensor']['line_id']
                )
            ));
            $busy['WorksList'] = $res;
            echo json_encode($busy);
            die();
        };

        echo "{}";


        die();

    }
    public function getWorkCenter() {
        $this->layout = false;
        $this->loadModel('Sensor');
        if(empty($this->request->query)) {
            die();
        }
        $res = $this->Sensor->findById($code = $this->request->query['wc'],array('id','ps_id','type'));
        echo json_encode($res['Sensor']);

        die();
    }

}
