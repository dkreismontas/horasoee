<?php

App::uses('AppModel', 'Model');

class WorksListAppModel extends AppModel {

    public static $navigationAdditions = array( );

    public static function loadNavigationAdditions() {
        self::$navigationAdditions[3] =
            array(
                'name'=>__('Technikų darbų registras'),
                'route'=>Router::url(array('plugin' => 'WorksList','controller' => 'WorksList', 'action' => 'entries')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
        self::$navigationAdditions[4] =
            array(
                'name'=>__("Technikų darbų tipai"),
                'route'=>Router::url(array('plugin' => 'WorksList','controller' => 'WorksList', 'action' => 'types')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );

    }

    public static function getWCSidebarLinks() {
        return "<a class=\"btn btn-info\" data-ng-if=\"sensor.time_interval_technicians_button==1\" target=\"_blank\" href=\"/works/#?wc={workCenterId}\">".__('Technikai')."</a>";
    }
}
