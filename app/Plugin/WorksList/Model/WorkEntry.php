<?php

App::uses('AppModel', 'Model');

class WorkEntry extends AppModel {

    public static $STATUS_NONE = 0;
    public static $STATUS_CURRENT = 1;
    public static $STATUS_FINISHED = 2;


    public static $max_problems = 0;


    public function generateReport($start_date,$end_date) {
        $settings = ClassRegistry::init('Settings');
        $sysName = $settings->getOne(Settings::S_SYSTEM_EMAIL_NAME);

        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->getProperties()
            ->setCreator($sysName)
            ->setLastModifiedBy($sysName)
            ->setTitle("Ataskaita");

        $workSheet = $objPHPExcel->setActiveSheetIndex(0);

//        $header = array(
//            array('name' => "Teorinis planas", 'width' => 8, 'start' => 'I', 'merge' => 'I1:M1', 'bgColor' => 'ffff00'),
//            array('name' => "Faktas", 'width' => 8, 'start' => 'N', 'merge' => 'N1:T1', 'bgColor' => '00ff00'),
//            array('name' => "Nuokrypis (min.)", 'width' => 8, 'start' => 'U', 'merge' => 'U1:Z1', 'bgColor' => 'ff0000'),
//            array('name' => "Problemų sąrašas", 'width' => 8, 'start' => 'AA', 'merge' => 'AA1:AB1', 'bgColor' => '777700'),
//        );

        $subheader = array(
            array('name' => "ID", 'width' => 8),
            array('name' => __("Pavadinimas"), 'width' => 26),
            array('name' => __("Darbuotojas"), 'width' => 26),
            array('name' => __("Nuo"), 'width' => 20),
            array('name' => __("Iki"), 'width' => 20),
            array('name' => __("Trukmė (min.)"), 'width' => 18),
            array('name' => __("DC"), 'width' => 10),
            array('name' => __("Komentaras"), 'width' => 26),
//            array('name' => "Užsakymas (MO)", 'width' => 46),
//
//            array('name' => "Pradžia", 'width' => 20),
//            array('name' => "Pabaiga", 'width' => 20),
//            array('name' => "Trukmė (min.)", 'width' => 20),
//            array('name' => "Derinimo pradžia", 'width' => 20),
//            array('name' => "Derinimo pabaiga", 'width' => 20),
//
//            array('name' => "Pradžia", 'width' => 20),
//            array('name' => "Pabaiga", 'width' => 20),
//            array('name' => "Trukmė (min.)", 'width' => 20),
//            array('name' => "Derinimo pradžia", 'width' => 20),
//            array('name' => "Derinimo pabaiga", 'width' => 20),
//            array('name' => "Derinimo trukmė (min.)", 'width' => 24),
//            array('name' => "Bendra prastovų trukmė (min.)", 'width' => 30),
//
//            array('name' => "Nuo užsakymo pradžios", 'width' => 22),
//            array('name' => "Nuo užsakymo pabaigos", 'width' => 22),
//            array('name' => "Trukmės skirtumas", 'width' => 20),
//            array('name' => "Nuo derinimo pradžios", 'width' => 22),
//            array('name' => "Nuo derinimo pabaigos", 'width' => 22),
//            array('name' => "Derinimo skirtumas", 'width' => 20),
        );

        $centerStyle = array(
            'fill'      => array(
                'type'  => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ffff00')
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            )
        );
        $rightBorderStyle = array(
            'borders' => array(
                'right' => array(
                    'style' => PHPExcel_Style_Border::BORDER_MEDIUM
                )
            )
        );

//        // Build main header
//        foreach ($header as $idx => $col) {
//            $colId = $col['start'];
//            $workSheet->setCellValue($colId . '1', $col['name']);
//            $centerStyle['fill']['color']['rgb'] = $col['bgColor'];
//            $workSheet->getStyle($col['merge'])->applyFromArray($centerStyle);
//            $workSheet->mergeCells($col['merge']);
//        }

        // Build subheader
        foreach ($subheader as $idx => $col) {
            $colId = chr(ord('A') + $idx);
            $workSheet->setCellValue($colId . '2', $col['name']);
            $workSheet->getColumnDimension($colId)->setWidth($col['width']);
        }
        $centerStyle['fill']['color']['rgb'] = 'cccccc';
        $workSheet->getStyle('A2:' . (chr(ord('A') + count($subheader) - 1)) . '2')->applyFromArray($centerStyle);

        $this->bindModel(array('belongsTo' => array('User','WorkType')));
        $we = $this->find('all', array(
            'conditions' => array(
                'user_id !='            =>   0,
                'ended_at IS NOT NULL',
                'started_at >='              =>   $start_date,
                'ended_at <='                =>   $end_date,
            )
        ));
        foreach ($we as $key => $w) {
            $this->fillSingleWorkReport($workSheet, $key, $w);
        }
        // Fill subheaders for problems
        $col = "AA";
        for ($i = 0; $i < (self::$max_problems); $i ++) {
            $workSheet->setCellValue($col . '2', __("Prastovos pavadinimas"));
            $workSheet->getColumnDimension($col)->setWidth(30);
            $workSheet->setCellValue(++$col . '2', __("Prastovos pradžia"));
            $workSheet->getColumnDimension($col)->setWidth(30);
            $workSheet->setCellValue(++$col . '2', __("Prastovos pabaiga"));
            $workSheet->getColumnDimension($col)->setWidth(30);
            $objPHPExcel->getActiveSheet()->getStyle($col.'2:'.$col.(count($we)+2))->applyFromArray($rightBorderStyle);
            if($i != (self::$max_problems - 1)) {
                $col ++;
            }
        }
        $workSheet->getStyle('AA2:' . $col . '2')->applyFromArray($centerStyle);
        $workSheet->mergeCells("AA1:".$col."1");
        return $objPHPExcel;
    }

    private function fillSingleWorkReport($workSheet, $i, $w) {
        $sensor = ClassRegistry::init('Sensor');
        $planClass = ClassRegistry::init('Plan');

        $sens = $sensor->findById($w['WorkEntry']['sensor_id']);

        $line = $i + 3;
        $workSheet->setCellValue('A' . $line, $w['WorkEntry']['id']);
        $workSheet->setCellValue('B' . $line, $w['WorkType']['name']);
        $workSheet->setCellValue('C' . $line, $w['User']['first_name'] . " " . $w['User']['last_name']);
        $workSheet->setCellValue('D' . $line, $w['WorkEntry']['started_at']);
        $workSheet->setCellValue('E' . $line, $w['WorkEntry']['ended_at']);
        $workSheet->setCellValue('F' . $line, ($w['WorkEntry']['ended_at'] != null) ? $this->changeUnixToMinutes(strtotime($w['WorkEntry']['ended_at']) - strtotime($w['WorkEntry']['started_at'])) : "");
        $workSheet->setCellValue('G' . $line, ($w['WorkEntry']['sensor_id'] == 0) ? "" : $sens['Sensor']['ps_id']);
        $workSheet->setCellValue('H' . $line, $w['WorkEntry']['comment']);

        $p = self::findAttachedPlan($w, $planClass);
        if ($p == null) {
            return;
        }
//
//        // Save plan id for future if we don't have it
//        if ($w['WorkEntry']['plan_id'] == 0) {
//            $w['WorkEntry']['plan_id'] = $p['Plan']['id'];
//            $this->save($w);
//        }
//
//        $workSheet->setCellValue('H' . $line, $p['Plan']['production_name'] . " (" . $p['Plan']['mo_number'] . ")");
//        $workSheet->setCellValue('I' . $line, $p['Plan']['start']);
//        $workSheet->setCellValue('J' . $line, $p['Plan']['end']);
//        $workSheet->setCellValue('K' . $line, $this->changeUnixToMinutes(strtotime($p['Plan']['end']) - strtotime($p['Plan']['start'])));
//
//        $previousPlan = $planClass->findPrevious($p);
//
//        if ($previousPlan != null && !empty($previousPlan)) {
//            $workSheet->setCellValue('L' . $line, $previousPlan['Plan']['end']);
//            $workSheet->setCellValue('M' . $line, $p['Plan']['start']);
//        }
//
//        $approvedOrderClass = ClassRegistry::init('ApprovedOrder');
//        $ao = self::findRelatedApprovedOrder($w, $p, $approvedOrderClass); // (Finished!)
//
//        if ($ao != null && !empty($ao)) {
//
//            $workSheet->setCellValue('N' . $line, $ao['ApprovedOrder']['created']);
//            if ($ao['ApprovedOrder']['status_id'] == $approvedOrderClass::STATE_COMPLETE) {
//                $workSheet->setCellValue('O' . $line, $ao['ApprovedOrder']['end']);
//                $workSheet->setCellValue('P' . $line, $this->changeUnixToMinutes(strtotime($ao['ApprovedOrder']['end']) - strtotime($ao['ApprovedOrder']['created'])));
//
//                $previousAO = $approvedOrderClass->findPrevious($ao);
//                if (!empty($previousAO)) {
//                    $workSheet->setCellValue('Q' . $line, $previousAO['ApprovedOrder']['end']);
//                    $workSheet->setCellValue('R' . $line, $ao['ApprovedOrder']['created']);
//                    $workSheet->setCellValue('S' . $line, $this->changeUnixToMinutes(strtotime($ao['ApprovedOrder']['created']) - strtotime($previousAO['ApprovedOrder']['end'])));
//                }
//
//                $foundProblemClass = ClassRegistry::init('FoundProblem');
//                $problems = $foundProblemClass->getAllByPlanId($p['Plan']['id']);
//                $timeSum = $foundProblemClass::getTimeSum($problems);
//
//                $workSheet->setCellValue('T' . $line, $this->changeUnixToMinutes($timeSum));
//
//                // Nuokrypis
//                $workSheet->setCellValue('U' . $line, $this->changeUnixToMinutes(strtotime($ao['ApprovedOrder']['created']) - strtotime($p['Plan']['start'])));
//                $workSheet->setCellValue('V' . $line, $this->changeUnixToMinutes(strtotime($ao['ApprovedOrder']['end']) - strtotime($p['Plan']['end'])));
//
//                $workSheet->setCellValue('W' . $line, $this->changeUnixToMinutes((strtotime($ao['ApprovedOrder']['end']) - strtotime($ao['ApprovedOrder']['created'])) - (strtotime($p['Plan']['end']) - strtotime($p['Plan']['start']))));
//                if (!empty($previousAO)) {
//                    $workSheet->setCellValue('X' . $line, $this->changeUnixToMinutes(strtotime($previousPlan['Plan']['end']) - strtotime($previousAO['ApprovedOrder']['end'])));
//                }
//                $workSheet->setCellValue('Y' . $line, $this->changeUnixToMinutes(strtotime($p['Plan']['start']) - strtotime($ao['ApprovedOrder']['created'])));
//
//                $workSheet->setCellValue('Z' . $line, $this->changeUnixToMinutes((strtotime($p['Plan']['start']) - strtotime($previousPlan['Plan']['end'])) - (strtotime($ao['ApprovedOrder']['created']) - strtotime($previousAO['ApprovedOrder']['end']))));
//
//                $col = "AA";
//                if (count($problems) > self::$max_problems) {
//                    self::$max_problems = count($problems);
//                }
//                foreach ($problems as $problem) {
//                    $workSheet->setCellValue($col . $line, $problem['Problem']['name']);
//                    $workSheet->setCellValue(++ $col . $line, $problem['FoundProblem']['start']);
//                    $workSheet->setCellValue(++ $col . $line, $problem['FoundProblem']['end']);
//
//                    $col ++;
//                }
//
//            }
//
//
//        }


    }


    public static function findAttachedPlan($w, $planObj) {
        if ($w['WorkEntry']['started_at'] == null) return;

        if ($w['WorkEntry']['plan_id'] != 0) {
            return $planObj->find('first', array(
                'fields'     => array(
                    'id', 'production_name', 'mo_number', 'start', 'end', 'sensor_id'
                ),
                'conditions' => array(
                    'id' => $w['WorkEntry']['plan_id']
                )
            ));
        }

        // Find plan where dates intersect.
        $p = $planObj->find('first', array(
            'fields'     => array(
                'id', 'production_name', 'mo_number', 'start', 'end', 'sensor_id'
            ),
            'conditions' => array(
                'start <='  => $w['WorkEntry']['started_at'],
                'end >'     => $w['WorkEntry']['started_at'],
                'sensor_id' => $w['WorkEntry']['sensor_id']
            )
        ));
        if (!empty($p)) {
            return $p;
        }

        // Find next plan
        $p = $planObj->find('first', array(
            'fields'     => array(
                'id', 'production_name', 'mo_number', 'start', 'end', 'sensor_id'
            ),
            'conditions' => array(
                'start >'   => $w['WorkEntry']['started_at'],
                'sensor_id' => $w['WorkEntry']['sensor_id']
            ),
            'order'      => array('start' => 'ASC')
        ));

        if (!empty($p)) {
            return $p;
        }

        return null;
    }

    private static function findRelatedApprovedOrder($w, $plan, $approvedOrderClass) {

        $plan_id = $plan['Plan']['id'];
        $ao = $approvedOrderClass->find('first', array(
            'conditions' => array(
                'plan_id' => $plan_id
            )
        ));
        if (!empty($ao)) {
            return $ao;
        }

        return null;
    }


    public function createSchema() {
        $this->query("

CREATE TABLE IF NOT EXISTS `work_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `work_type_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `sensor_id` int(11) NOT NULL DEFAULT '0',
  `plan_id` int(11) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ");        $this->query("

CREATE TABLE IF NOT EXISTS `work_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `sensor_type` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;
        ");
        $this->insertPermissions();
    }

    public function insertPermissions() {
        $perm = ClassRegistry::init('Perm');
        $data = array(
            'Perm' => array(
                array('key' => 'WorksListController::entries', 'description' => 'Darbų sąrašas: Matyti darbų sąrašą', 'updated' => 1),
                array('key' => 'WorksListController::generateXLSreport', 'description' => 'Darbų sąrašas: Generuoti ataskaitą', 'updated' => 1),
                array('key' => 'WorksListController::types', 'description' => 'Darbų sąrašas: Matyti darbų tipus', 'updated' => 1),
                array('key' => 'WorksListController::create', 'description' => 'Darbų sąrašas: Sukurti naują tipa', 'updated' => 1),
                array('key' => 'WorksListController::edit', 'description' => 'Darbų sąrašas: Redaguoti tipą', 'updated' => 1),
                array('key' => 'WorksListController::delete', 'description' => 'Darbų sąrašas: Ištrinti tipą', 'updated' => 1),
                array('key' => 'WorksListController::works', 'description' => 'Darbų sąrašas: Pasiekti technikų puslapį', 'updated' => 1),
            )
        );

        $perm->saveAll($data['Perm']);
    }

    public function changeUnixToMinutes($unix) {
        return round($unix/60,2);
    }

    public function changeUnixSecondsToHumanTime($unix, $remove_last_space = false) {
        $mult = "";
        if ($unix < 0) {
            $mult = "-";
            $unix = - 1 * $unix;
        }
        if ($unix == 0) {
            return "0 s";
        }
        $result = "";
        $points = array(
            __('val')   => 3600,
            __('min') => 60,
            __('s')   => 1
        );

        foreach ($points as $point => $value) {
            if ($unix >= $value) {
                $fl = floor($unix / $value);
                $result .= $fl . " " . $point . " ";
                $unix -= $fl * $value;
            }
        }

        if ($remove_last_space) {
            $result = trim($result);
        }


        return $mult . $result;
    }

}