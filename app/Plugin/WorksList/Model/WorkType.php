<?php

App::uses('AppModel', 'Model');

class WorkType extends AppModel {

    public static $STATUS_NONE = 0;
    public static $STATUS_CURRENT = 1;
    public static $STATUS_FINISHED = 2;


    public function getAsSelectOptions($addEmpty = false, $exclude = array()) {
        $conds = array();
        $conds['id !='] = $exclude;
        $items = $this->find('all', (empty($conds) ? array() : array('conditions' => $conds)));
        $options = array();
        if ($addEmpty) {
            $options[0] = __('-- None --');
        }
        foreach ($items as $li) {
            $options[$li['WorkType']['id']] = $li['WorkType']['name'];
        }
        return $options;
    }

}