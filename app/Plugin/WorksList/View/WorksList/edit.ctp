<h3><?php echo __('Redaguoti'); ?></h3>
<?php echo $this->Form->create("WorkType", array('autocomplete' => 'off')); ?>

<?php echo $this->Form->input('id', array('type' => 'hidden', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('name', array('label' => __('Pavadinimas'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('parent_id', array('label' => __('Tėvinis'), 'options' => $parentOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('sensor_type', array('label' => __('Tipas'), 'options' => $typeOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
<?php echo $this->Form->input('line_id', array('label' => __('Linija'), 'options' => $lineOptions, 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>

<div>
    <br />
    <button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
    <a href="<?php echo $deleteUrl ?>" class="btn btn-warning"><?php echo __("Ištrinti"); ?></a>
</div>
<?php echo $this->Form->end(); ?>

