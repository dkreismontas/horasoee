<div ng-controller="MainController">
    <button ng-click="refreshPage()" class="refresh-button btn btn-primary" style="float:right;"><?php echo __('Atnaujinti puslapį'); ?>
    </button>
    <div class="container" style="margin:0 auto; max-width:700px;">
        <h1><b><?php echo __('Darbo centras'); ?>: {{workCenter.ps_id}}</b></h1>

        <div ng-show="isLoading">
            <img src="/images/loaders/loader12.gif"/> <?php echo __('Kraunama'); ?>...
        </div>
        <div ng-show="hasErrors" style="color:red;">
            {{errors}}
        </div>

        <div ng-show="!isLoading">
            <div ng-show="!isAuthed">
                <h2><?php echo __('Prisijungimas'); ?></h2>

                <p><b><?php echo __('Įveskite techniko kodą'); ?>:</b></p>

                <form ng-submit="tryAuth(code)">
                    <div class="form-group">
                        <input type="text" id="code" class="form-control" ng-model="code" style="height:auto;"/>
                    </div>
                    <button class="btn btn-success full-width"><?php echo __('Prisijungti'); ?></button>
                </form>
            </div>

            <div ng-show="isAuthed">
                <?php echo __('Prisijungta kaip'); ?>: <b>{{technician.full_name}}</b>

                <div ng-show="isBusy">
                    <div ng-show="!confirmEndWork" style="text-align:center;">
                        <h2><b><?php echo __('Šiuo metu vykdomas darbas'); ?></b></h2>

                        <h3><?php echo __('Vykdo'); ?>: <b>{{busyWorkEntryUser.full_name}}</b></h3>
                    </div>

                    <div ng-show="busyWorkEntryUser.id == technician.id">

                        <ul ng-show="!confirmEndWork" style="text-align:center;">
                            <br>
                            <h2><?php echo __('Pasirinkite darbą, kad jį pabaigti'); ?></h2>
                            <li class="list-unstyled" ng-repeat="work in worksList">
                                <button class="btn btn-primary full-width" ng-click="endWorkConfirm(work)">
                                    {{work.WorkType.name}}
                                </button>
                            </li>
                        </ul>
                        <div ng-show="confirmEndWork">
                            <h1><?php echo __('Ar tikrai norite pabaigti darbą ir pasirinkti'); ?> <b>{{selectedWorkType.name}}</b>?</h1>
                            <label><?php echo __('Papildomas komentaras'); ?></label>
                            <textarea ng-model="comment" class="form-control"></textarea>
                            <button class="btn btn-warning full-width" ng-click="finishWork(selectedWorkType,busyWorkEntry)"><?php echo __('Pabaigti'); ?></button>
                            <button class="btn btn-primary full-width" ng-click="cancelEndWork()"><?php echo __('Atšaukti'); ?></button>
                        </div>

                        <!--                        <button class="btn btn-warning full-width" ng-click="finishWork(busyWorkEntry)">Pabaigti darbą-->
                        <!--                        </button>-->
                    </div>
                </div>
                <div ng-show="!isBusy">
                    <h2>{{currentMessage}}</h2>

                </div>
            </div>
        </div>
    </div>


</div>