<?php echo $this->Html->css('/WorksList/css/workslist'); ?>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas įrašas'); ?></button>
<br /><br /> */ ?>

<!--<a href="/WorksList/report">Ataskaita</a>-->

<div class="col-md-3">
<div class="row">
    <?php echo __('Atsisiųsti ataskaitą'); ?>:
    <?php         echo $this->Form->create(false, array(
        'url'           => '/WorksList/report',
        'class'         => 'form form-horizontal',
        'role'          => 'form',
        'inputDefaults' => array(
            'class' => 'form-control datepicker',
        )
    ));?>
    <div class="col-md-6">
        <?php echo $this->Form->input('start_date', array('label' => __('Pradžia'))); ?>
    </div>
    <div class="col-md-6">
        <?php echo $this->Form->input('end_date', array('label' => __('Pabaiga'))); ?>
    </div>

</div>
    <?php echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));?>
    <?php echo $this->Form->end();?>
</div>


<br/>
<table class="table table-bordered work-table">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Pavadinimas'); ?></th>
			<th><?php echo __('Jutiklio tipas'); ?></th>
			<th><?php echo __('Vartotojas'); ?></th>
			<th><?php echo __('Darbo centras'); ?></th>
			<th><?php echo __('Būsena'); ?></th>
			<th><?php echo __('Komentaras'); ?>
			<th><?php echo __('Pradžia'); ?>
			<th><?php echo __('Pabaiga'); ?>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li):
			?>
		<tr class="
		    <?php
                switch($li['WorkEntry']['status']) {
                    case 1:
                        echo "work-current";
                        break;
                    case 2:
                        echo "work-finished";
                        break;
                }
            ?>

		">
			<td><?php echo $li['WorkEntry']['id'];?></td>
			<td><?php echo $li['WorkType']['name']; ?></td>
			<td><?php echo isset($typeOptions[$li['Sensor']['type']])?$typeOptions[$li['Sensor']['type']]:''; ?></td>
			<td><?php echo ($li['WorkEntry']['user_id'] == 0) ? "-" : "<a target=\"_blank\" href=\"/users/edit/{$li['User']['id']}\">".$li['User']['first_name']." ".$li['User']['last_name']."</a>"; ?></td>
			<td><?php echo ($li['WorkEntry']['sensor_id'] == 0) ? "-" : $li['Sensor']['ps_id']; ?></td>
<!--			<td>--><?php //echo ($li['WorkEntry']['plan_id'] == 0) ? "-" : "<a target=\"_blank\" href=\"/plans/".$li['WorkEntry']['plan_id']."/edit\">".$li['Plan']['production_name']." (".$li['Plan']['mo_number'].")"."</a>"; ?><!--</td>-->
			<td><?php
                 switch($li['WorkEntry']['status']) {
                     case 0:
                         echo __("Nepradėtas");
                         break;
                     case 1:
                         echo __("Vykdomas");
                         break;
                     case 2:
                         echo __("Pabaigtas");
                         break;
                 }
                ?>
            </td>
			<td><?php echo $li['WorkEntry']['comment']; ?></td>
			<td><?php echo $li['WorkEntry']['started_at']; ?></td>
			<td><?php echo $li['WorkEntry']['ended_at']; ?></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<!--<div>--><?php //echo $this->App->properPaging(true, array()); ?><!--</div>-->

<script>
    jQuery("document").ready(function () {
        jQuery(".datepicker").datepicker({
            dateFormat: "yy-mm-dd"
        });
    });
</script>