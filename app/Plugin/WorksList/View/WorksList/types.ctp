<?php echo $this->Html->css('/WorksList/css/workslist'); ?>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>

<?php
echo $this->Html->link(
    __('Sukurti naują'),
    array('plugin' => 'WorksList', 'controller' => 'WorksList', 'action' => 'create'),
    array('class'=>'btn btn-info')
)

?>
<br/>
<table class="table table-bordered work-table">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Tėvinis'); ?></th>
			<th><?php echo __("Pavadinimas"); ?></th>
			<th><?php echo __("Jutiklio tipas"); ?></th>
			<th><?php echo __("Linija"); ?></th>
			<th><?php echo __("Veiksmai"); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="7"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li):
			?>
		<tr>
			<td><?php echo $li['WorkType']['id'];?></td>
            <td><?php echo (($li['WorkType']['parent_id'] != 0 && isset($parentOptions[$li['WorkType']['parent_id']])) ? $parentOptions[$li['WorkType']['parent_id']] : ''); ?></td>
			<td><?php echo $li['WorkType']['name']; ?></td>
			<td><?php echo $typeOptions[$li['WorkType']['sensor_type']]; ?></td>
            <td><?php echo ($li['WorkType']['line_id'] != 0 && isset($lineOptions[$li['WorkType']['line_id']])) ? $lineOptions[$li['WorkType']['line_id']] : ''; ?></td>
			<td><a class="btn btn-default" href="/WorksList/<?php echo $li['WorkType']['id']; ?>/edit"><span class="glyphicon glyphicon-pencil"></span> <?php echo __('Redaguoti'); ?></a></td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<!--<div>--><?php //echo $this->App->properPaging(true, array()); ?><!--</div>-->
