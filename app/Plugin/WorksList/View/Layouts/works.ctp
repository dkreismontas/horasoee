<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<title><?php echo "Darbų sąrašas" ?></title>
	<?php
		echo $this->Html->meta('icon');
//		echo $this->Html->css(array('style.default', 'style','bootstrap-datepicker.min'));
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
		echo $this->Html->css('/css/bootstrap.min');

	?>
    <style>
        .btn {
            margin-bottom:20px;
            border-radius:0;
            padding:10px;
            font-size:20px;
        }
        .form-control {
            border-radius:0;
            padding:20px;
            font-size:20px;
        }
        .full-width {
            width:100%;
        }
    </style>



</head>
<body ng-app="WorksListApp">
    <?php echo $this->fetch('content'); ?>

    <script type="text/javascript" src="<?php echo $baseUri; ?>js/soundjs-0.5.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/raphael.2.1.0.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/justgage.1.0.1.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/bootstrap-datepicker.lt.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/angular.min.js"></script>
    <script type="text/javascript" src="<?php echo $baseUri; ?>js/polyfill.js"></script>
    <?php echo $this->Html->script('/WorksList/js/workslist'); ?>
    <script>
        jQuery(document).ready(function() {
            setTimeout(function(){
                $("#code").focus();
            }, 0);
        });
    </script>
</body>
</html>
