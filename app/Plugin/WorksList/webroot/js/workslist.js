var app = angular.module('WorksListApp', []);
//app.config(['$locationProvider', function($locationProvider){
//    $locationProvider.html5Mode(true);
//}]);


app.controller('MainController', function ($scope, $location, $http, $window) {
    $scope.workCenter = null;
    load();

    $scope.isLoading = false;
    $scope.isAuthed = false;
    $scope.techCode = "";
    $scope.code = ""; // Test variable.
    $scope.comment = "";

    $scope.isBusy = false;
    $scope.busyWorkEntry = null;
    $scope.busyWorkEntryUser = null;

    $scope.hasErrors = false;
    $scope.errors = "";

    $scope.technician = null;
    $scope.worksList = null;
    $scope.worksListMain = null;

    $scope.confirmEndWork = false;
    $scope.selectedWorkType = null;

    $scope.currentMessage = "";

    // Try to authenticate with entered technician code.
    $scope.tryAuth = function(code) {
        if(!$scope.workCenter) {
            return;
        }
        $scope.isLoading = true;
        $scope.techCode = code;
        $http.get('/WorksList/tryAuth?code='+code).
            success(function(data) {
                $scope.isLoading = false;
                $scope.hasErrors = 0;
                if('hasErrors' in data) {
                    $scope.hasErrors = data.hasErrors;
                    $scope.errors = data.errors;
                    return;
                }
                if('id' in data) {
                    $scope.technician = data;
                    $scope.technician.full_name = data.first_name + " " + data.last_name;
                    $scope.isAuthed = true;
                }
            });
    };
    $scope.endWorkConfirm = function(work) {
        if(work.children && work.children.length != 0) {
            $scope.worksList = work.children;
            return;
        }
        $scope.confirmEndWork = true;
        $scope.selectedWorkType = work.WorkType;
    };
    $scope.cancelEndWork = function() {
        $scope.confirmEndWork = false;
        $scope.selectedWorkType = null;
        $scope.worksList = $scope.worksListMain;
    };
    $scope.startWork = function() {
        console.log("starting work!");
        $scope.isLoading = true;
        $http.get('/WorksList/startWork?code=' + $scope.techCode + '&type=' + $scope.workCenter.type + "&wc=" + $scope.workCenter.id).
            success(function(data) {
                $scope.hasErrors = false;
                $scope.isLoading = false;
                if(data.length <= 5) {
                    console.log(data);
                    return;
                }
                if('hasErrors' in data) {
                    $scope.hasErrors = data.hasErrors;
                    $scope.errors = data.errors;
                    return;
                }

                if('WorkEntry' in data) {
                    getWorksListIfBusy();
                }
            });
    };
    $scope.finishWork = function(selectedWorkType,workEntry) {
        $scope.isLoading = true;
        $http.get('/WorksList/finishWorkEntry?code=' + $scope.techCode + '&workEntry=' + workEntry.id + '&workType=' + selectedWorkType.id+'&comment='+$scope.comment).
            success(function(data) {
                $scope.hasErrors = false;
                $scope.isLoading = false;
                if(data.length <= 5) {
                    console.log(data);
                    //$window.close();
                    $scope.refreshPage();
                    return;
                }
                else if('hasErrors' in data) {
                    $scope.hasErrors = data.hasErrors;
                    $scope.errors = data.errors;
                    return;
                }


            });
    };

    function getWorksListIfBusy() {
        console.log("getWorksListIfBusy");
        $scope.isLoading = true;
        $http.get('/WorksList/getWorksList?code=' + $scope.techCode + "&type=" + $scope.workCenter.type + "&wc=" + $scope.workCenter.id).
            success(function(data) {
                $scope.isLoading = false;
                if('hasErrors' in data) {
                    $scope.hasErrors = data.hasErrors;
                    $scope.errors = data.errors;

                    return;
                }

                if(('WorkEntry' in data) && ('status' in data.WorkEntry) && ('WorksList' in data)) {
                    $scope.isBusy = true;
                    $scope.busyWorkEntry = data.WorkEntry;
                    $scope.busyWorkEntryUser = data.User;
                    $scope.busyWorkEntryUser.full_name = data.User.first_name + " " + data.User.last_name;

                    $scope.worksList = data.WorksList;
                    $scope.worksListMain = data.WorksList;


                } else {
                    $scope.isBusy = false;
                    $scope.busyWorkEntry = null;

                    $scope.currentMessage = "Pradedamas naujas darbas";
                    $scope.startWork();

                    $scope.worksList = data;
                }

            });
    }


    // Wait for auth changes to reload works list.
    $scope.$watch('isAuthed',function() {
        if($scope.isAuthed) {
            setTimeout(function() {
                getWorksListIfBusy();
            },10);

        }
    });
    function load() {
        //$scope.workCenter.id = $location.search()['wc'];
        if($location.search()['wc']) {
            $http.get('/WorksList/getWorkCenter?wc=' + $location.search()['wc']).
                success(function(data) {
                    $scope.workCenter = data;
                });
        }
        //console.log();
    }
    $scope.refreshPage = function() {
        $window.location.reload();
    }
});