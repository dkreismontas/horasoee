<?php
//Router::connect('/WorksList', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'index'));
Router::connect('/WorksList/types', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'types'));
Router::connect('/WorksList/entries', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'entries'));
Router::connect('/WorksList/create', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'create'));

Router::connect('/WorksList/tryAuth', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'tryAuth'));
Router::connect('/WorksList/getWorksList', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'getWorksList'));
Router::connect('/WorksList/getWorkCenter', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'getWorkCenter'));
Router::connect('/WorksList/selectWorkType', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'selectWorkType'));
Router::connect('/WorksList/startWork', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'startWork'));
Router::connect('/WorksList/finishWorkEntry', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'finishWorkEntry'));
Router::connect('/WorksList/:id/edit', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'edit'));
Router::connect('/WorksList/:id/delete', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'delete'));
Router::connect('/WorksList/report', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'generateXLSreport'));


Router::connect('/works', array('plugin'=>'WorksList','controller' => 'WorksList', 'action' => 'works'));