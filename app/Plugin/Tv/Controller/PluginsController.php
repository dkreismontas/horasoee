<?php
App::uses('TvAppController', 'Tv.Controller');

class PluginsController extends TvAppController {
	
    public $uses = array('Sensor','Tv.Tv','Record','Shift','User','DashboardsCalculation');
    
    function index($workCenters = ''){
    	$title_for_layout = __('Darbo centrai');
        $layout = 'full_page';
        $parameters = array(&$layout);
        $this->Help->callPluginFunction('TvPlugin_AfterIndex_Hook', $parameters, Configure::read('companyTitle'));
        $this->layout = $layout;
		$this->set(compact('title_for_layout','workCenters'));
        $this->layout = $layout;
    }
    
    public function get_data($workCenters = ''){
        //$this->allow('*');
        $this->Tv->Record = $this->Record;
        $this->Tv->Shift = $this->Shift;
        $this->Tv->DashboardsCalculation = $this->DashboardsCalculation;
        $usersSensors = $this->User->find('list', array('fields'=>array('sensor_id', 'line_id'), 'conditions'=>array('line_id >'=>0)));
        $this->Sensor->bindModel(array('belongsTo'=>array(
            'Line'
        )));
        $conditions = array('Sensor.marked'=>1);
        if(isset(Configure::read('user')->selected_sensors)){
            $conditions['Sensor.id'] = Configure::read('user')->selected_sensors;
        }
        if(trim($workCenters)){
            if(!empty(Configure::read('user')->selected_sensors)) {
                $conditions['Sensor.id'] = array_intersect(Configure::read('user')->selected_sensors, explode('_', $workCenters));
            }else{
                $conditions['Sensor.id'] = explode('_', $workCenters);
            }
        }
        $searchParameters = array('fields'=>array(
            'TRIM(Sensor.id) AS id',
            'TRIM(Sensor.name) AS name',
            'TRIM(Sensor.speedometer_type) AS speedometer_type',
            'TRIM(Sensor.calculate_quality) AS calculate_quality',
            'TRIM(Sensor.image) AS image',
            'TRIM(Sensor.tv_speed_deviation) AS tv_speed_deviation',
            'TRIM(Sensor.branch_id) AS branch_id',
            'TRIM(Sensor.line_id) AS line_id',
            'TRIM(Line.name) AS line_title'
        ), 'conditions'=>$conditions, 'order'=>array('Sensor.position','Line.name', 'Sensor.name'));
        $parameters = array(&$searchParameters);
        $pluginData = $this->Help->callPluginFunction('Tv_BeforeGetDataSensorsSearch_Hook', $parameters, Configure::read('companyTitle'));
        $sensors = Hash::combine($this->Sensor->find('all', $searchParameters),'{n}.0.id', '{n}.0');
        $this->Tv->parametersCalculation($sensors);
        foreach($sensors as $sensorId => &$data){ //sis skriptas reikalingas tam, kad galetu isvesti tinkama sensoriaus nuoroda, kuri susideda is linijos ir darbo centro id. Bet linijos id yra ne paties sensoriaus, bet vartotojo linijos, kuriam priklauso sensorius
            $data['line_id'] = isset($usersSensors[$sensorId])?$usersSensors[$sensorId]:$data['line_id'];
        }
        $columnsCount = sizeof($sensors) <= 2?2:max(min(ceil(sizeof($sensors)/2), 4), 3);
        $sensors = $this->Tv->combineSensorsIntoColumns($sensors, $columnsCount);
        echo json_encode(array(
            'sensors'=> $sensors, 
            'columns'=> $columnsCount
        ));
        die();
    }
    
    public function beforeFilter(){
        $parameters = array(&$this);
        $this->Help->callPluginFunction('TvPlugin_beforeBeforeFilter_Hook', $parameters, Configure::read('companyTitle'));
        parent::beforeFilter();
    }
	
}
