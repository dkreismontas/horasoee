<?php
App::uses('Problem','Model');
class Tv extends AppModel{
    public $useTable = false;
    CONST RECORD_AGE = 120; //irasu senumas sekundemis
    CONST RecordDuration = 20; // Kas kiek laiko sukuriamas įrašas, sekundėmis
    public $Record, $Shift, $DashboardsCalculation;
    
    public function combineSensorsIntoColumns($sensors, $columnsCount){
        foreach($sensors as &$sensor){
            if(trim($sensor['image']) && file_exists(ROOT.'/app/webroot/img/'.$sensor['image'])){
                $sensor['sensor_tvplugin_img'] = 'background-image: url(/img/'.$sensor['image'].')';
            }elseif(file_exists(__DIR__.'/../webroot/img/single_sprites/'.Configure::read('companyTitle').'-'.$columnsCount.'c-id'.$sensor['id'].'.png')){
                $sensor['sensor_tvplugin_img'] = 'background-image: url('.Router::url('/tv/img/single_sprites/'.Configure::read('companyTitle').'-'.$columnsCount.'c-id'.$sensor['id'].'.png', true).')';
            }else{
                $sensor['sensor_tvplugin_img'] = '';
            }
        }
        $rowsCount = ceil(sizeof($sensors) / $columnsCount);
        $data = array();
        for($i = 0; $i < $rowsCount; $i++){
            $data[$i] = array_values(array_slice($sensors, $i*$columnsCount, $columnsCount, true));
        }
        return $data;
    }
    
    public function parametersCalculation(&$sensors){
        $currentStatuses = $this->getCurrentStatuses(array_keys($sensors));
        $this->attachAllStatuses($sensors);
        foreach($sensors as &$sensor){
            $prototype = array('total_time'=>0, 'transition_time'=>0, 'no_work_time'=>0, 'true_problem_time'=>0);
            $sensor = array_merge($prototype,$sensor);
            $sensorId = $sensor['id'];
            $sensor['oee'] = isset($sensor['oee'])?round($sensor['oee']):0;
            $sensor['current_status'] = isset($currentStatuses[$sensorId]['status'])?$currentStatuses[$sensorId]['status']:'';
            $sensor['running_line_txt'] = isset($currentStatuses[$sensorId]['running_line_txt'])?$currentStatuses[$sensorId]['running_line_txt']:'';
            $sensor['last_minute_quantities'] = isset($currentStatuses[$sensorId]['last_minute_quantities'])?round($currentStatuses[$sensorId]['last_minute_quantities'],2):0;
            $div = $sensor['speedometer_type'] == 0?60:1;
            $sensor['client_speed_vnt_min'] = isset($currentStatuses[$sensorId]['client_speed'])?number_format($currentStatuses[$sensorId]['client_speed'] / $div, 2, '.', ''):0;
            $sensor['sensor_settings'] = isset($currentStatuses[$sensorId]['sensor'])?$currentStatuses[$sensorId]['sensor']:array();
            $sensor['speedometer_title'] = $sensor['speedometer_type'] == 0?__('Greitis vnt/min'):__('Pam greitis vnt/h');
            $parameters = array(&$sensor);
            $pluginData = $this->callPluginFunction('Tv_parametersCalculationAfterValuesSet_Hook', $parameters, Configure::read('companyTitle'));
        }
    }
    
    private function getCurrentStatuses($sensorsIds){
        $time = time();
        //$time = strtotime('2017-10-30 11:59:19');
        $statuses = array();
        $now = new DateTime();
        $fShiftsIds = ClassRegistry::init('Shift')->findAllCurrentShifts();
        $exclusionList = array();
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
            $exclusionList = $plugin_model::getExclusionIdList();
            $shiftDuration = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . join(',', $exclusionList) . '),0,' . Configure::read('recordsCycle') . ')) AS shift_duration';
        }else{
            $shiftDuration = 'SUM('.Configure::read('recordsCycle').') AS shift_duration';
        }
        $bindings = array('belongsTo'=>array('Sensor','FoundProblem'));
        $searchParameters = array(
            'fields'=>array(
                'MAX(Record.id) as last_id',
                'SUM(IF(Sensor.speedometer_type = 0,
                    IF(Record.created >= \''.date('Y-m-d H:i:s', $time-self::RECORD_AGE-59).'\' AND Record.created <= \''.date('Y-m-d H:i:s', $time-self::RECORD_AGE).'\', IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity) ,0),
                    IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity)
                )) AS quantities',
                //'SUM(IF(Record.created >= \''.date('Y-m-d H:i:s', $time-self::RECORD_AGE-59).'\', IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity) ,0)) AS last_minute_quantities',
                'Record.sensor_id',
                'Sensor.speedometer_type',
                $shiftDuration
            ),'recursive'=>1,
            'conditions'=>array(
                // 'Record.created <='=>date('Y-m-d H:i:s', $time-self::RECORD_AGE), 
                // 'Record.created >'=>date('Y-m-d H:i:s', $time-3600), 
                'Record.sensor_id'=>$sensorsIds,
                'Record.shift_id'=>$fShiftsIds,
            ),'group'=>array('Record.sensor_id')
        );
        $this->Record->bindModel($bindings);
        $parameters = array(&$searchParameters, &$bindings, &$sensorsIds);
        $pluginData = $this->callPluginFunction('Tv_getCurrentStatusesBeforeSearch_Hook', $parameters, Configure::read('companyTitle'));
        $lastStatuses = $this->Record->find('all', $searchParameters);
        foreach($lastStatuses as $data){
            $sensorId = $data['Record']['sensor_id'];
            if($data['Sensor']['speedometer_type'] == 0){
                $statuses[$sensorId]['last_minute_quantities'] = $data[0]['quantities'];
            }else{
                $statuses[$sensorId]['last_minute_quantities'] = $data[0]['shift_duration'] > 0?bcdiv($data[0]['quantities'], bcdiv($data[0]['shift_duration'],3600,6),2):0;
            }
        }
        $this->Record->unbindModel(array('belongsTo'=>array('Sensor')));
        $this->Record->bindModel(array('belongsTo'=>array('FoundProblem','Plan','Sensor','Problem'=>array('foreignKey'=>false, 'conditions'=>array('Problem.id = FoundProblem.problem_id')))));
        $lastStatuses = $this->Record->find('all', array(
            'conditions'=>array('Record.id'=>Set::extract('/0/last_id',$lastStatuses)),
            'group'=>array('Record.sensor_id')
        ));
        foreach($lastStatuses as $data){
            $sensorId = $data['Record']['sensor_id'];
            $statuses[$sensorId]['client_speed'] = $data['Plan']['step'];
            $statuses[$sensorId]['sensor'] = $data['Sensor'];
            $runningLineTxt = isset($data['Plan']['production_name'])?$data['Plan']['production_name']:'';
            if($data['Record']['approved_order_id'] && $data['Record']['plan_id'] && !$data['Record']['found_problem_id']){
                $statuses[$sensorId]['status'] = 'green';
            }elseif($data['Record']['found_problem_id']){
                switch($data['FoundProblem']['problem_id']){
                    case Problem::ID_NEUTRAL: $statuses[$sensorId]['status'] = 'yellow'; break;
                    case Problem::ID_NO_WORK: $statuses[$sensorId]['status'] = 'grey'; break;
                    default:
                        if(in_array($data['FoundProblem']['problem_id'], $exclusionList)){
                            $statuses[$sensorId]['status'] = 'grey';
                        }else {
                            $statuses[$sensorId]['status'] = 'red problem_id_'.$data['FoundProblem']['problem_id'].' super_parent_problem_id_'.$data['FoundProblem']['super_parent_problem_id'];
                        }
                    break;
                }
                $runningLineTxt .= (trim($runningLineTxt)?' - ':'').Settings::translate($data['Problem']['name']);
            }elseif($data['Record']['quantity'] > 0){
                $statuses[$sensorId]['status'] = 'green';
            }else{
                $statuses[$sensorId]['status'] = 'red';
            }
            $statuses[$sensorId]['running_line_txt'] = $runningLineTxt;
        }
        return $statuses;
    }

    private function attachAllStatuses(&$sensors){
        $branches = array_unique(Set::extract($sensors,'/branch_id'));
        $exclusionList = array();
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
            $exclusionList = $plugin_model::getExclusionIdList();
        }
        $exclusionList[] = Problem::ID_NO_WORK;
        $exclusionList = array_unique($exclusionList);
        foreach($branches as $branchId){
            $branchSensors = array_filter($sensors, function($sensor)use($branchId){ return $sensor['branch_id'] == $branchId; });
            $fShift = $this->Shift->findCurrent($branchId, new DateTime);
            $prevShiftsId = $this->Shift->find('list', array('fields'=>array('Shift.id','Shift.id'),'conditions'=>array('Shift.id < '=>$fShift['Shift']['id'], 'Shift.branch_id'=>$branchId, 'Shift.disabled'=>0), 'order'=>array('Shift.id DESC'), 'limit'=>2 ));
            $this->Record->bindModel(array('belongsTo'=>array('FoundProblem')));
            $times = $this->Record->find('all', array(
                'fields' => array(
                    'TRIM(Record.sensor_id) AS sensor_id',
                    'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (self::RecordDuration) . ',0)) AS total_time',
                    'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id='.Problem::ID_NEUTRAL.',' . (self::RecordDuration) . ',0)) as transition_time',
                    'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN('.implode(',', $exclusionList).'),' . (self::RecordDuration) . ',0)) as no_work_time',
                    'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id>='.Problem::ID_NOT_DEFINED.' AND FoundProblem.problem_id NOT IN('.implode(',', $exclusionList).'),' . (self::RecordDuration) . ',0)) as true_problem_time',
                 ), 'conditions' => array(
                    'Record.shift_id' => $fShift['Shift']['id'], 
                    'Record.sensor_id' => array_keys($branchSensors)
                 ),'group'  => array('Record.sensor_id'),
                 'order'  => array('Record.sensor_id ASC')
            ));
            $prevShiftsOEE = $this->DashboardsCalculation->find('all', array(
                'fields'=>array('DashboardsCalculation.shift_id','ROUND(DashboardsCalculation.oee_with_exclusions) AS oee','DashboardsCalculation.sensor_id'),
                'conditions'=>array(
                    'DashboardsCalculation.sensor_id'=>array_keys($branchSensors), 
                    'DashboardsCalculation.shift_id'=>$prevShiftsId,
                ), 'order'=>array('DashboardsCalculation.sensor_id','DashboardsCalculation.shift_id DESC'), 
            ));
            $oee = $this->calcOee($fShift, array_keys($branchSensors));
            foreach($times as $time){ $time = current($time);
                $sensorId = $time['sensor_id']; unset($time['sensor_id']);
                $sensors[$sensorId]['prev_shifts_oee'] = array_filter($prevShiftsOEE, function($data)use($sensorId){ return $data['DashboardsCalculation']['sensor_id'] == $sensorId; });
                $sensors[$sensorId]['prev_shifts_oee'] = Hash::combine($sensors[$sensorId]['prev_shifts_oee'], '{n}.DashboardsCalculation.shift_id', '{n}.0.oee');
                krsort($sensors[$sensorId]['prev_shifts_oee']);
                $sensors[$sensorId]['prev_shifts_oee'] = array_values($sensors[$sensorId]['prev_shifts_oee']);
                $total = array_sum($time);
                if($total > 0){
                    array_walk($time, function(&$param)use($total){ $param = round($param/$total*100); });
                }
                $sensors[$sensorId] = array_merge($sensors[$sensorId], $time);
                //$sensors[$sensorId]['oee'] = min(100, (isset($oee[$sensorId]['oee'])?round($oee[$sensorId]['oee'],1):0));
                if(isset($oee[$sensorId]['oee_with_exclusions'])){
                    $sensors[$sensorId]['oee'] =  min(100, round($oee[$sensorId]['oee_with_exclusions'],0));
                    $sensors[$sensorId]['exploitation_factor'] =  round($oee[$sensorId]['exploitation_factor_with_exclusions'],2);
                }elseif(isset($oee[$sensorId]['oee'])){
                    $sensors[$sensorId]['oee'] = min(100, round($oee[$sensorId]['oee'],0));
                    $sensors[$sensorId]['exploitation_factor'] =  round($oee[$sensorId]['exploitation_factor'],2);
                }else $sensors[$sensorId]['oee'] = 0;
                $sensors[$sensorId]['operational_factor'] = round($oee[$sensorId]['operational_factor'],2);
                $sensors[$sensorId]['quality_factor'] = round($oee[$sensorId]['quality_factor'],2);
            }
        }
    }

    private function calcOee($fShift,$fSensor){
        if(!$fShift){ return array(); }
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $oeeObj = new OeeShell;
        $fShift[Shift::NAME]['end'] = date('Y-m-d H:i:s');
        $oeeObj->shift = $fShift;
        $oeeObj->workWithMo = false;
        $data = $oeeObj->start_sensors_calculation($fSensor,true);
        return $data;
    }
        
}
    