<div id="mashines" class="{{theme}} columns{{columns}}">
    <!--button v-on:click="loadData()">OK</button>
    <button v-on:click="remove()">Re</button-->
    <div class="row" v-for="(columnNr, row_sensors) in sensors">
        <sensor_comp v-for="(sensor_id, sensor) in row_sensors" :sensor="sensor"></sensor_comp>
    </div>
</div>
<template id="sensorTpl">
    <div class="sensor sensor_{{sensor.id}} {{sensor.current_status}} col-xs-{{12/columns}}">
        <div>
            <h1><span><i></i></span>{{sensor.name|shortenString}}</h1>
            <div class="box-content">
                <div id="leftSide">
                    <p class="currentSpeed" v-if="sensor.client_speed_vnt_min > 0 && sensor.tv_speed_deviation != ''">{{sensor.speedometer_title}}<br />{{sensor.last_minute_quantities}} <?php //echo __('vnt/min'); ?></p>
                    <canvas height="{{canvasHeight}}" width="{{canvasWidth}}" class="speed" id="speed{{sensor.id}}"></canvas>
                    <a style="{{sensor.sensor_tvplugin_img}};" href="<?php echo Router::url('/work-center'); ?>/{{sensor.line_id}}_{{sensor.id}}" class="mashineStatus {{sensor.current_status}}" target="_blank"></a>
                    <div class="prevShiftsOee">
                        <p><?php echo __('Praėjusios pam OEE'); ?>: {{sensor.prev_shifts_oee[0]; }}</p>
                        <p><?php echo __('Užpraėjusios pam OEE'); ?>: {{sensor.prev_shifts_oee[1]; }}</p>
                    </div>
                </div>
                <div class="rightContainer">
                    <div class="graph" id="graph{{sensor.id}}"></div>
                    <div class="factors">
                        <div v-if="sensor.exploitation_factor">
                            <p><?php echo __('Prieinamumas') ?> - <b>{{(sensor.exploitation_factor * 100).toFixed(2)}}</b> %</p>
                            <div class="exploitationIndicator indicator"><div style="width: {{Math.min(sensor.exploitation_factor*100,100)}}%;"></div></div>
                        </div>
                        <div v-if="sensor.operational_factor">
                            <p><?php echo __('Efektyvumas') ?> - <b>{{(sensor.operational_factor * 100).toFixed(2)}}</b> %</p>
                            <div class="operationalIndicator indicator"><div style="width: {{Math.min(sensor.operational_factor*100,100)}}%;"></div></div>
                        </div>
                        <div v-if="sensor.calculate_quality > 0 AND sensor.quality_factor">
                            <p><?php echo __('Kokybė') ?> - <b>{{(sensor.quality_factor * 100).toFixed(2)}}</b> %</p>
                            <div class="qualityIndicator indicator"><div style="width: {{Math.min(sensor.quality_factor*100,100)}}%;"></div></div>
                        </div>
                    </div>
                </div>
                <br class="clear" />
                <div class="marquee"><p>{{sensor.running_line_txt}}</p></div>
            </div>
            <div class="box-footer row">
                <div class="footerStatus green col-xs-3">
                    
                    <div class="textData">
                        <p class="statusText"><span style="white-space: nowrap;"><?php echo __('Darbas'); ?></span></p>
                        <p class="statusPercent">{{sensor.total_time}}%</p>
                    </div>
                </div>
                <div class="footerStatus red col-xs-3">
                    <div class="textData">
                        <p class="statusText"><span style="white-space: nowrap;"><?php echo __('Prastova'); ?></span></p>
                        <p class="statusPercent">{{sensor.true_problem_time}}%</p>
                    </div>
                </div>
                <div class="footerStatus yellow col-xs-3">
                    <div class="textData">
                        <p class="statusText"><span style="white-space: nowrap;"><?php echo __('Derinimas'); ?></span></p>
                        <p class="statusPercent">{{sensor.transition_time}}%</p>
                    </div>
                </div>
                <div class="footerStatus grey col-xs-3">
                    <div class="textData">
                        <p class="statusText"><span style="white-space: nowrap;"><?php echo __('Išjungta'); ?></span></p>
                        <p class="statusPercent">{{sensor.no_work_time}}%</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>



<script type="text/javascript">
    setInterval(function(){location.reload();}, 1800000);
    var vm = new Vue({
        el: '#mashines',
        data: {
            sensors: {},
            columns: 0,
            theme: 'white', //white | dark
            counter: 0,
        },
        methods: {
            loadData: function(){
                jQuery.ajax({
                    url: "<?php echo $this->Html->url('/tv/plugins/get_data/'.$workCenters,true); ?>",
                    type: 'post',
                    context: this,
                    dataType: 'JSON',
                    async: true,
                    success: function(request){
                        this.sensors = request.sensors;
                        this.columns = request.columns;
                        this.canvasHeight = 200;
                        this.canvasWidth = 500;
                    }
                })
            }
        },
        beforeCompile: function(){ 
            this.loadData();
            setInterval(this.loadData,500000);
        },
        components: {
            'sensor_comp': {
                template: '#sensorTpl',
                props: ['sensor'],
                data: function(){return { columns:0, canvasHeight: 0, canvasWidth: 0 }},
                beforeCompile: function(){
                    this.columns = this.$parent.columns;
                    this.canvasHeight = this.$parent.columns==2?200:150;
                    this.canvasWidth = this.$parent.columns==2?500:300;
                },
                ready: function(){
                    this.buildGraph(); 
                },
                methods: {
                    buildGraph: function(){
                        if(this.sensor.tv_speed_deviation != ''){
                            this.drawSpeedGraph();
                        }
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(this.drawChart);
                    },
                    drawChart: function(){
                        //donut grafikas
                        var data = google.visualization.arrayToDataTable([
                          ['TV', 'Percentage'],
                          ['active', this.sensor.total_time],
                          ['downtime', this.sensor.true_problem_time],
                          ['prep', this.sensor.transition_time],
                          ['inactive', this.sensor.no_work_time],
                        ]);
                        var options = {
                          pieHole: 0.7,
                          pieSliceTextStyle: {
                            color: 'black',
                          },
                          tooltip : {
                             trigger: 'none'
                          },
                          pieSliceText: 'none',
                          legend: 'none',
                          'chartArea': {'width': '95%', 'height': '95%'},
                          'backgroundColor': (this.$parent.theme == 'dark'?'#27282c':'#fff'),
                          slices: {
                            0: { color: '#18a418' },
                            1: { color: '#df0404' },
                            2: { color: '#ffd800' },
                            3: { color: '#b7b7b7' },
                          }
                        };
                        var chart = new google.visualization.PieChart(document.getElementById('graph'+this.sensor.id));
                        google.visualization.events.addListener(chart, 'ready', this.myReadyHandler);
                        chart.draw(data, options);
                    },
                    myReadyHandler: function(){
                        $("#graph"+this.sensor.id).append('<div class="JSFiddle"><div class="chart_div"><big>'+this.sensor.oee+'%</big><br />OEE</div></div>');
                    },
                    drawSpeedGraph: function(){
                         if(parseFloat(this.sensor.client_speed_vnt_min) <= 0) return;
                         var speederSettings = this.sensor.sensor_settings.tv_speed_deviation.split(',');
                         var minDeviationPerc,maxDeviationPerc = 30;
                         var minYellowDeviationPerc = 0;
                         var maxYellowDeviationPerc = 5;
                         if(speederSettings[0]){
                             var minMaxSetting = speederSettings[0].replace('[','').replace(']','').split('-');
                             if(minMaxSetting.length == 2){
                                 minDeviationPerc = parseFloat(minMaxSetting[0]);
                                 maxDeviationPerc = parseFloat(minMaxSetting[1]);
                             }
                         }
                         if(speederSettings[1]){
                             var minMaxSetting = speederSettings[1].replace('[','').replace(']','').split('-');
                             if(minMaxSetting.length == 2){
                                 minYellowDeviationPerc = parseFloat(minMaxSetting[0]);
                                 maxYellowDeviationPerc = parseFloat(minMaxSetting[1]);
                             }
                         }
                         var minDeviation = parseFloat(this.sensor.client_speed_vnt_min * (1 - minDeviationPerc/100));
                         var maxDeviation = parseFloat(this.sensor.client_speed_vnt_min * (1 + maxDeviationPerc/100));
                         var minYellowDeviation = parseFloat(this.sensor.client_speed_vnt_min * (1 - minYellowDeviationPerc/100));
                         var maxYellowDeviation = parseFloat(this.sensor.client_speed_vnt_min * (1 + maxYellowDeviationPerc/100));
                         var staticZones = new Array();
                         if(typeof this.sensor.sensor_settings.speed_gauge_colors == 'object' && this.sensor.sensor_settings.speed_gauge_colors.length >= 3){
                            var intervalsV = !this.sensor.sensor_settings.speed_gauge_intervals?[minDeviation, minYellowDeviation, maxYellowDeviation, maxDeviation]:this.sensor.sensor_settings.speed_gauge_intervals;
                            for(var i in this.sensor.sensor_settings.speed_gauge_colors){
                                staticZones.push({strokeStyle: this.sensor.sensor_settings.speed_gauge_colors[i], min: intervalsV[i], max: intervalsV[parseInt(i)+1]});
                            }
                         }else{
                            staticZones = [
                               {strokeStyle: "#F03E3E", min: minDeviation, max: minYellowDeviation}, // Red from 100 to 130
                               {strokeStyle: "#FFDD00", min: minYellowDeviation, max: maxYellowDeviation}, // Yellow
                               {strokeStyle: "#30B32D", min: maxYellowDeviation, max: maxDeviation}, // Green
                            ];
                         }
                         var opts = {
                          angle: 0, // The span of the gauge arc
                          lineWidth: 0.25, // The line thickness
                          radiusScale: 0.8, // Relative radius
                          pointer: {
                            length: 0.55, // // Relative to gauge radius
                            strokeWidth: 0.031, // The thickness
                            color: '#000000' // Fill color
                          },
                          limitMax: true,     // If false, max value increases automatically if value > maxValue
                          limitMin: true,     // If true, the min value of the gauge will be fixed
                          strokeColor: '#E0E0E0',  // to see which ones work best for you
                          generateGradient: true,
                          highDpiSupport: true,     // High resolution support
                          staticZones: staticZones,
                          staticLabels: {
                              font: "20px sans-serif",  // Specifies font
                              labels: [minDeviation, maxDeviation],  // Print labels at these values
                              color: "#000000",  // Optional: Label text color
                              fractionDigits: 0  // Optional: Numerical precision. 0=round off.
                            }
                        };
                        var target = document.getElementById('speed'+this.sensor.id); // your canvas element
                        var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
                        gauge.maxValue = maxDeviation; // set max gauge value
                        gauge.setMinValue(minDeviation);  // Prefer setter over gauge.minValue = 0
                        gauge.animationSpeed = 5; // set animation speed (32 is default value)
                        gauge.set(this.sensor.last_minute_quantities); // set actual value
                    }
                },
                filters: {
                    shortenString: function(string){
                        return string.length > 18?string.substring(0, 18)+'...':string;
                    }
                }
            }
        }
    });
</script>
