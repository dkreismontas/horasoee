<?php
App::uses('AppModel', 'Model');

class SeverstalAppModel extends AppModel {
    public static $navigationAdditions = array( );

    //public function getWCSidebarLinks(){
        //return '<button type="button" class="btn btn-info" data-ng-click="endProductionProc(); prodRegisterIssue();">'.__('Pamainos pabaiga').'</button>';
    //}

    public static function loadNavigationAdditions() {
        self::$navigationAdditions[1] =
            array(
                'name'=>__('Severstal plans'),
                'route'=>Router::url(array('plugin' => 'severstal','controller' => 'import_plans', 'action' => 'start_import')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }

    public function getWCSidebarLinks(){
        return '<a class="oee-btn center bg-orange">Mano plugin button </a>';
    }

}
