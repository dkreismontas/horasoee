<?php
App::uses('SeverstalAppController', 'Severstal.Controller');

class PluginsController extends SeverstalAppController {
	
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        $this->layout = 'ajax';
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Severstal.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

}
