<?php
class Axioma extends AppModel{

    private static $shiftSearchBy = 'end';

     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }

    public function Oeeshell_beforeStartDurationCalculation_Hook(&$shiftsSearchParams, &$start, &$end){
        $shiftsSearchParams['conditions'] = array('Shift.'.self::$shiftSearchBy.' >=' => $start, 'Shift.'.self::$shiftSearchBy.' <' => $end, 'Shift.disabled'=>0);
    }

    public function ReportsContr_AfterCalculatePeriodXLS_Hook(&$calculation, &$shiftsExtremumsParams, &$requestData){
        $shiftsExtremumsParams['conditions'] = array('Shift.'.self::$shiftSearchBy.' >=' => $requestData['start_date'], 'Shift.'.self::$shiftSearchBy.' <' => $requestData['end_date']);
    }

    public function HelpComponent_beforeDatePatternFormation_Hook(&$shiftSearchBy){
        $shiftSearchBy = self::$shiftSearchBy;
    }
     
     // public function changeUpdateStatusVariablesHook($fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        // if(!empty($currentPlan)){
            // $recordModel = ClassRegistry::init('Record');
            // $quantity = $recordModel->find('first', array(
                // 'fields'=>array('SUM(Record.unit_quantity) AS total'), 
                // 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
            // ));
            // $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.','').' '.__('vnt'); 
        // }
     // }
     
}
    