DROP FUNCTION IF EXISTS GET_UNIT_QUANTITY;

DELIMITER $$

CREATE  FUNCTION `GET_UNIT_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS decimal(12,4) NO SQL
BEGIN
	DECLARE `sensor_type` INT(2) DEFAULT NULL;
	DECLARE `multiplier` DECIMAL(12,4) DEFAULT 1;
	DECLARE `line_industrial` TINYINT(1) DEFAULT 0;
	DECLARE `qty_pack` MEDIUMINT(8) DEFAULT 1;
	SELECT `s`.`type` INTO `sensor_type` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `l`.`industrial` INTO `line_industrial` FROM `line_sensors` AS `ls` LEFT JOIN `lines` AS `l` ON (`ls`.`line_id` = `l`.`id`) WHERE `ls`.`sensor_id`=`sensor_id` LIMIT 1;
	IF(`sensor_type` = 3 AND `line_industrial` = 1) THEN
		SET `multiplier` = `box_quantity`;
	ELSE
		SET `multiplier` = 1;
	END IF;
	RETURN `quantity` * `multiplier`;
END$$

DELIMITER ;