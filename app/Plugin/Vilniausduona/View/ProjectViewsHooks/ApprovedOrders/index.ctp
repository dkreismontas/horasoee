<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas patvirtintas užsąkymas'); ?></button>
<br /><br /> */ ?>

<?php if(!$forced_sensorId) {?>
    <div class="clearfix">
        <div class="btn-group pull-left">
            <h3><i class="iconsweets-trashcan2"></i> <?php echo __('Paieška'); ?></h3>
            <?php
            echo $this->Form->create('ApprovedOrder', array('class'=>'row-fluid', 'type'=>'get'));
            echo $this->Form->input('order_number', array('type'=>'text', 'label'=>__('Įveskite užsakymo nr.'),  'class'=>'form-control', 'div'=>'pull-left col-xs-5 nopadding', 'value'=>isset($this->request->query['order_number'])?$this->request->query['order_number']:''));
            echo $this->Form->input('product_code', array('type'=>'text', 'label'=>__('Gaminio kodas'),  'class'=>'form-control', 'div'=>'pull-left col-xs-4', 'value'=>isset($this->request->query['product_code'])?$this->request->query['product_code']:''));
            echo $this->Form->input(__('Ieškoti'), array('type'=>'submit', 'label'=>'&nbsp;',  'class'=>'form-control btn btn-default', 'div'=>'pull-left col-xs-3 nopadding'));
            echo $this->Form->end();
            ?>
        </div>
        <div class="btn-group pull-right">
            <button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo $sensorsLabel; ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
            <ul class="dropdown-menu">
                <?php foreach ($sensorOptions as $lid => $li): ?>
                    <li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <br/>
<?php } ?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Data ir laikas'); ?></th>
			<th><?php echo __('Darbo centrai'); ?></th>
			<th><?php echo __('Planas'); ?></th>
			<th><?php echo __('Kiekis'); ?></th>
			<th><?php echo __('Dėžių / kubilų kiekis'); ?></th>
			<th><?php echo __('Patvirtintas'); ?></th>
			<th><?php echo __('Būsena'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; ?>
		<tr>
			<td><?php echo $li->id; ?></td>
			<td><?php $ds = new DateTime($li->created); echo $ds->format('Y-m-d H:i'); ?></td>
			<td><?php echo Sensor::buildName($li_); ?></td>
			<td><?php echo Plan::buildName($li_); ?></td>
			<td><?php 
			if($li->confirmed && $li_['Sensor']['type'] == 1){
			    echo '0';
            }else{
                echo $li->quantity;
            }
			?></td>
			<td>
			    <?php 
			    if($li->confirmed && $li_['Sensor']['type'] == 1){
                    echo round($li_[ApprovedOrder::NAME]['confirmed_quantity'],2);
			    }else{
			        $bq = $li->confirmed ? ($li_[Plan::NAME]['box_quantity'] ? floor($li->confirmed_quantity / $li_[Plan::NAME]['box_quantity']) : 0) : $li->box_quantity;
                    $iq = ($li->confirmed ? $li->confirmed_quantity : $li->quantity) - ($bq * $li_[Plan::NAME]['box_quantity']);
                    echo $bq;//.(($iq > 0) ? (' ('.$iq.')') : '');
			    }
			?></td>
			<td <?php if(isset($li->prev_shift_no_quantities) && $li->prev_shift_no_quantities){ echo 'style="background: #ffe612;"'; } ?>>
			    <?php echo ($li->status_id == ApprovedOrder::STATE_COMPLETE || $li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE) ? ($li->confirmed ? __('Taip') : __('Ne')) : '&mdash;'; ?>
			    <?php if(isset($li->prev_shift_no_quantities) && $li->prev_shift_no_quantities){
                    echo '<br />'.__('Užsakymas buvo gamintas kitoje pamainoje nei buvo patvirtintas, tačiau už tą pamainą nėra deklaruotų kiekių');
                } ?>
			</td>
			<td><?php echo __(Status::buildName($li_)); ?></td>
			<th>
				<?php if ($li->status_id == ApprovedOrder::STATE_COMPLETE): ?>
				<a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				<?php endif; ?>
				<?php if ($li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE): ?>
                    <?php if(!$li->confirmed): ?>
                        <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                    <?php endif; ?>
                    &nbsp;
                    <?php if($li->status_id != ApprovedOrder::STATE_PART_COMPLETE) : ?>
                        <a href="<?php printf($finishUrl, $li->id); ?>"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;Pabaigti</a>
                    <?php endif; ?>
				<?php endif; ?>
			</th>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
