<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->element('Vilniausduona.ApprovedOrderTable', array('relatedPostponedOrders'=>$rpo,'item' => $item, 'lossItems' => $lossItems, 'boldLabels' => true)); ?>
<?php if ($item[ApprovedOrder::NAME]['confirmed'] || ($item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_COMPLETE && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_POSTPHONED && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_PART_COMPLETE)): ?>
<?php if(isset($item[ApprovedOrder::NAME]['modified']) && $item[ApprovedOrder::NAME]['modified'] != $item[ApprovedOrder::NAME]['created'] && (int)$item[ApprovedOrder::NAME]['modified'] != 0){
	echo __('Patvirtinimas atliktas').':'.$item[ApprovedOrder::NAME]['modified'];
} ?>
<div class="form-group">
	<label>
        <?php if($item['Sensor']['type'] == 1) : ?>
            <?php echo 'Kubilų kiekis'.($item[ApprovedOrder::NAME]['confirmed'] ? (' ('.__('patvirtintas').')') : ''); ?>:
            <?php echo round($item[ApprovedOrder::NAME]['confirmed_quantity'],2); ?>
        <?php else: ?>
            <?php echo __('Dėžių kiekis').($item[ApprovedOrder::NAME]['confirmed'] ? (' ('.__('patvirtintas').')') : ''); ?>:
            <?php
            $bq = ($item[Plan::NAME]['box_quantity'] ? floor($item[ApprovedOrder::NAME]['confirmed_quantity']/$item[Plan::NAME]['box_quantity']) : 0);
            $pq = ($item[ApprovedOrder::NAME]['confirmed_quantity'] - ($bq * $item[Plan::NAME]['box_quantity']));
            echo $bq.(($pq > 0) ? (' '.__('ir gaminių').' '.$pq.' vnt') : '');
            ?>
        <?php endif; ?>
    </label>
	<span></span>
</div>
<?php else: ?>
<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
	<div class="col-xs-5">
		<?php
        $pqt = ($item[ApprovedOrder::NAME]['quantity'] - ($item[ApprovedOrder::NAME]['box_quantity'] * $item[Plan::NAME]['box_quantity']));
        $finished_boxes = $item[Plan::NAME]['box_quantity'] > 0?floor(($item[ApprovedOrder::NAME]['quantity']-$item[ApprovedOrder::NAME]['exported_quantity'])/$item[Plan::NAME]['box_quantity']):0;
        $box_quantity_label = __('Dėžių kiekis');
        if($item['Sensor']['type'] == 1) {
            $finished_boxes = $item[Plan::NAME]['quantity'];
            $box_quantity_label = 'Kubilų kiekis';
        }

        echo $this->Form->input('confirmed_box_quantity', array('value'=>$finished_boxes,'label' => $box_quantity_label, 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
	</div>
	<div class="col-xs-1">
		<div class="form-group"><label><?php echo __('ir'); ?></label></div>
	</div>
	<div class="col-xs-6">
		<?php echo $this->Form->input('confirmed_item_quantity', array('label' => __('Gaminių kiekis'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'))); ?>
	</div>
</div>
<?php endif; ?>
<div>
	<br />
    <?php if(!$canSave): ?>
        <p>Palaukite 2 minutes po užsakymo pabaigos.</p>
    <?php endif;?>
	<?php if ($canSave && !$item[ApprovedOrder::NAME]['confirmed'] && ($item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_COMPLETE || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED || $item[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_PART_COMPLETE)): ?>
	   <input type="submit" class="btn btn-primary" value="<?php echo __('Išsaugoti'); ?>" name="confirm_order" />
	<?php endif; ?>
	<!--<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a> -->
</div>
<?php echo $this->Form->end();