<?php
App::uses('VilniausduonaAppController', 'Vilniausduona.Controller');
App::uses('ApprovedOrder', 'Model');
class PluginsController extends VilniausduonaAppController {
    
    public $uses = array('Shift','Record','Sensor','DashboardsCalculation','ApprovedOrder','Settings','Vilniausduona.Vilniausduona');
    
    public function index(){
        $this->layout = 'ajax';
        $schema = $this->DashboardsCalculation->schema();
        if(!isset($schema['sensor_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `sensor_quantity` DECIMAL(20,8) NOT NULL COMMENT 'sensoriaus uzfiksuotas kiekis' AFTER `total_quantity`;");
        }
        if(!isset($schema['planned_stops_duration'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `planned_stops_duration` INT(11) NOT NULL COMMENT 'planuotu sustojimu trukme sekundemis' AFTER `total_quantity`;");
        }
        if(!isset($schema['planned_stops_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `planned_stops_quantity` INT(11) NOT NULL COMMENT 'planuotu sustojimu kiekis' AFTER `total_quantity`;");
        }
        if(!isset($schema['non_planned_stops_duration'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `non_planned_stops_duration` INT(11) NOT NULL COMMENT 'neplanuotu sustojimu trukme sekundemis' AFTER `total_quantity`;");
        }
        if(!isset($schema['non_planned_stops_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `non_planned_stops_quantity` INT(11) NOT NULL COMMENT 'neplanuotu sustojimu kiekis' AFTER `total_quantity`;");
        }
        $schema = $this->ApprovedOrder->schema();
        if(!isset($schema['confirmation_shift_id'])){
            $this->DashboardsCalculation->query("ALTER TABLE `approved_orders` ADD `confirmation_shift_id` INT NOT NULL COMMENT 'Pamainos id, kurioje uzsakymas buvo patvirtintas' AFTER `additional_data`;");
        }
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Vilniausduona.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

    /** @requireAuth Vilniausduona: redaguoti kiekius užsakymo tvirtinimo metu  */
    public function edit_approved_order_quantities(){
        die();//reikia del teisiu tvirtinant uzsakymu kiekius
    }

	/*function save_sensors_data(){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 1800){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->informAboutProblems();
		//$this->checkRecordsIsPushing();
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        $tmpRecords = $this->Record->query('SELECT `source`, `date`, `value` FROM `SensorProfileRecords` order by `date`,`source` LIMIT 1000');
        foreach($tmpRecords as $tmpRecord){ $tmpRecord = $tmpRecord['SensorProfileRecords'];
            try{
                $date = date('Y-m-d H:i:s',strtotime($tmpRecord['date'].' UTC'));
				$quantity = $tmpRecord['source'] == 14?round($tmpRecord['value']/3):$tmpRecord['value'];
				$query = 'CALL ADD_FULL_RECORD(\''.$date.'\', '.$tmpRecord['source'].', '.$quantity.', '.Configure::read('recordsCycle').'); ';
                $this->Record->query($query);
                $this->Record->query('DELETE FROM `SensorProfileRecords` WHERE `source` = '.$tmpRecord['source'].' AND `date` = \''.$tmpRecord['date'].'\' ');
            }catch(Exception $e){
                $this->Log->write('Nepavyko irasyti jutiklio duomenu. Vykdyta uzklausa: '.$query.'. Uzklausos klaida: '.json_encode($e));
                unlink($dataPushStartMarkerPath);
            }
        }
        unlink($dataPushStartMarkerPath);
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            //sutvarkome atsiustu irasu duomenis per darbo centra
            $this->moveOldRecords();
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }

    public function moveOldRecords() {
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        die();
    }
	
    private function checkRecordsIsPushing(){
        $this->render(false);
        $settings = $this->Settings->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $minutes = explode(':', $settings);
        $minutes = isset($minutes[1]) && is_numeric(trim($minutes[1]))?floatval(trim($minutes[1])):0;
        if(!$minutes) $minutes = 60;
        $markerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > $minutes*60 ){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, 1);
            fclose($fh);
            $emailsList = explode('|',current(explode(':', $settings)));
            array_walk($emailsList, function(&$data){$data = trim($data);});
            if(empty($emailsList))return;
            $this->Record->bindModel(array('belongsTo'=>array('Sensor')));
            $lastRecords = $this->Record->find('all', array(
                'fields'=>array('Sensor.*','MAX(Record.created) as last_created',), 
                'conditions'=>array('Sensor.is_partial'=>0, 'Sensor.pin_name <>'=>''),
                'group'=>array('Record.sensor_id')
            ));
            $message = '';
            if(empty($lastRecords)){
                $message .= 'Į duomenų bazės records lentelę nepareina duomenys iš sensorių, lentelė tuščia.';
            }
            foreach($lastRecords as $record){
                if(time() - strtotime($record[0]['last_created']) > $minutes*60){
                    $message .= sprintf('Negaunami duomenys iš sensoriaus, kurio identifikatorius: %s, pavadinimas: %s, kojelės nr.: %s'."\r\n", $record['Sensor']['ps_id'],$record['Sensor']['name'],$record['Sensor']['pin_name']);
                }
            }
            if(trim($message)){
                $headers = "Content-Type: text/plain; charset=UTF-8";
                mail(implode(',',$emailsList),"Įrašų siuntimo problema ".Configure::read('companyTitle'),$message,$headers);
                $profileRecordModel = ClassRegistry::init('sensorprofilerecord');
                $oldestRecord = $profileRecordModel->query('SELECT MIN(Date) as min_date FROM `SensorProfileRecords`');
                if($oldestRecord){
                    $recordSameDate = $profileRecordModel->query('DELETE FROM `SensorProfileRecords` WHERE `Date` = \''.$oldestRecord[0][0]['min_date'].'\' ');
                    $this->Log->write('Is SensorProfileRecords lenteles del irasu siuntimo sustojimo pasalinti irasai, kuriu Date = '.$oldestRecord[0][0]['min_date']);
                }
            }
        }
    }*/
	
}
