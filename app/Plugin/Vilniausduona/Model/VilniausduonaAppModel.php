<?php
App::uses('AppModel', 'Model');

class VilniausduonaAppModel extends AppModel {

    public function getWCSidebarLinks(){
        $url = Router::url(array('controller'=>'plans'),true);
        return '<div ng-if="!needTimePager && sensor.id == 12"><a target="_blank" href="'.$url.'/index/sensorId:{{sensor.id}}" title="'.__('Tvirtinti planus').'" class="btn btn-info ng-scope">'.__('Tvirtinti planus').'</a></div>';
    }

}
