<?php
class HisteelPlan{
    public $useTable = false;
    private $orders = array();
    
    public function __construct(Array $arr){
        $this->orders = $this->createOrders($arr);
        $this->startPlaning();
        $this->updatePlansData();
    }
    
    private function startPlaning(){
        (int)$now = time();
        settype($calendar, 'array');
        foreach($this->orders as $sensorId => &$orders){
            (int)$startPoint = $now;
            $calendar = array();
            foreach($orders as &$order){
                if($order['Plan']['client_speed'] == 0) continue; //TODO issiaiskinti ka daryti tokiais atvejais
                unset($order['Plan']['plan_start']);
                unset($order['Plan']['plan_end']);
                $quantityMade = array_sum(Set::extract('/quantity', $order['ApprovedOrder']));
                (int)$orderDuration = round(($order['Plan']['preparation_time'] + ($order['Plan']['quantity'] - $quantityMade) / ($order['Plan']['client_speed'] / 60)) * 60); //trukme sekundemis
                if($orderDuration <= 0){ $orderDuration = 1; }
                while($orderDuration > 0){
                    if(empty($calendar)) $calendar = $this->constructCalendar($startPoint);
                    $interval = current($calendar);
                    if(!isset($order['Plan']['plan_start'])){ $order['Plan']['plan_start'] = date('Y-m-d H:i', $interval['start']); }
                    $intervalDuration = $interval['end'] - $interval['start'];
                    if($intervalDuration < $orderDuration){
                        array_shift($calendar);
                        $orderDuration -= $intervalDuration;
                        //next($calendar);
                    }else{
                        // pr(date('Y-m-d H:i', $interval['start']));
                        // pr(date('Y-m-d H:i', $interval['end']));
                        $interval['start'] += $orderDuration;
                        $order['Plan']['plan_end'] = date('Y-m-d H:i', $interval['start']);
                        $calendar[key($calendar)] = $interval;
                        $orderDuration = 0;
                    }
                    $startPoint = $interval['end'];
                }
            }
        }
    }
    
    private function addEntry($start, $end, &$calendar){
        if($start < $end){
            $calendar[] = array(
                //'start'=>date('Y-m-d H:i', $start), 
                //'end'=>date('Y-m-d H:i', $end),
                // unix laikas
                'start'=>$start, 
                'end'=>$end,
            );
            return true;
        }
        return false;
    }
    
    private function constructCalendar($startPoint){
        $availableWeekdays = array(1,2,3,4,5); //date('N') savaites dienos reprezentacija, kur 6 - sestad, 7 - sekmad ir t.t.
        $availableHoures = array(
            array(
                'start'=>'06:00',
                'end'=>'22:00', 
                'breaks'=>array(
                    array('start'=>'09:00','end'=>'09:15'),
                    array('start'=>'12:00','end'=>'12:30'),
                    array('start'=>'16:00','end'=>'16:15'),
                    array('start'=>'18:00','end'=>'18:30'),
    
                )
            ),
        );
        $holidays = array('01-01', '02-16', '03-11', '05-01', '05-06', '06-03', '06-24', '07-06', '08-15', '11-01', '12-24', '12-25', '12-26');
        (int)$reachPoint = strtotime('+2 week', $startPoint);
        $calendar = array();
        while($startPoint < $reachPoint){
            //patikriname ar esama diena nera tarp atostogu
            if(in_array(date('m-d', $startPoint), $holidays)){
                GOTO START_LOOP; //sokame i kita diena su laiku 00:00
            }
            //patikriname ar esama diena nera tarp savaites dienu, kuriomis nedirbama
            if(!in_array(date('N', $startPoint), $availableWeekdays)){
                GOTO START_LOOP; //sokame i kita diena su laiku 00:00
            }
            // pildome darbo laiku kalendoriu ir einam per pamainas
            foreach($availableHoures as $interval){
                //prasortina pertrauku masyva
                usort($interval['breaks'], function($a, $b) {
                    return strtotime($a['start']) - strtotime($b['start']);
                });
                $start = max($startPoint, strtotime(date('Y-m-d',$startPoint).' '.$interval['start']));
                $end = strtotime(date('Y-m-d',$startPoint).' '.$interval['end']);
                if($start >= $end) { continue; }
                // einamee per pertrakas
                foreach($interval['breaks'] as $breaksInterval){
                    if(intval(date('Hi', $start)) >= intval(str_replace(':','',$breaksInterval['start'])) && intval(date('Hi', $start)) < intval(str_replace(':','',$breaksInterval['end']))){
                        $start = strtotime(date('Y-m-d',$start).' '.$breaksInterval['end']);
                        continue;
                    }
                    $end = strtotime(date('Y-m-d',$start).' '.$breaksInterval['start']);
                    if(!$this->addEntry($start, $end, $calendar)) { continue; }
                    $start = strtotime(date('Y-m-d',$end).' '.$breaksInterval['end']);
                }
                $end = strtotime(date('Y-m-d',$startPoint).' '.$interval['end']);
                $this->addEntry($start, $end, $calendar);
            }
            START_LOOP:
            $startPoint = strtotime(date('Y-m-d',strtotime('+1 day', $startPoint))); continue; //sokame i kita diena su laiku 00:00
        }
        return $calendar;
    }
    
    //atnaujinsime planu suplanuotus laikus i DB
    private function updatePlansData(){
        $planModel = ClassRegistry::init('Plan');
        $schema = $planModel->schema();
        if(!array_key_exists('plan_start', $schema)){
            $planModel->query('ALTER TABLE `'.$planModel->tablePrefix.$planModel->useTable.'` ADD `plan_start` DATETIME NOT NULL AFTER `created`;');
            $planModel->query('ALTER TABLE `'.$planModel->tablePrefix.$planModel->useTable.'` ADD `plan_end` DATETIME NOT NULL AFTER `plan_start`;');
            die('Plans lenteleje prideti papildomi stulpeliai. Perkrauti puslapi.');
        }
        // /;
        foreach($this->orders as $sensorId => $orders){
            foreach($orders as $order){
                if(!isset($order['Plan']['plan_start'])) continue;
                $planModel->id = $order['Plan']['id'];
                $planModel->save(
                    array('plan_start'=>$order['Plan']['plan_start'], 'plan_end'=>$order['Plan']['plan_end'])
                );
            }
        }
    }
    
    public function createOrders($arr){
        if(!isset($arr['conditions'])) return array();
        $planModel = ClassRegistry::init('Plan');
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $condForAppOrd = array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE, 'ApprovedOrder.sensor_id = Plan.sensor_id'));
        $planModel->withRefs(true, $condForAppOrd);
        //pirmiausia randame uzsakymus kurie ne karto neturejo uzbaigimo statuso
        $plans = $planModel->find('list', array(
            'fields'=>array('Plan.id'), 'recursive'=>1, 'conditions'=>array_merge(array('Plan.has_problem'=>0),$arr['conditions']),
            //'order'=>array('Plan.sensor_id', 'FIELD(ApprovedOrder.status_id, 1)',)
        ));
        $planModel->bindModel(array('hasMany'=>array('ApprovedOrder')));
        //uzsakymams, kurie niekad neturejo uzbaigimo, ieskome paskutinio statuso 
        $plans = $planModel->find('all', array(
            //'fields'=>array('Plan.*','ApprovedOrder.*'),
            'conditions'=>array(
                'Plan.id'=>$plans
            ),
            'order'=>array('Plan.sensor_id', 'Plan.position_frozen DESC', 'Plan.position')
        ));
        $sensorsPlan = array();
        //sugrupuojame uzsakymus i sensorius bei susidedame viska taip, kad siuo metu vykdomas uzsakymas butu virsuje
        array_walk($plans, function(&$plan, $key)use(&$plansInProgress, &$sensorsPlan){
            $sensorId = $plan['Plan']['sensor_id'];
            if(array_filter($plan['ApprovedOrder'], function($appOrd){ return isset($appOrd['status_id']) && $appOrd['status_id'] == 1; })){
                if(isset($sensorsPlan[$sensorId])){
                    $sensorsPlan[$sensorId] = array_merge(array($plan), $sensorsPlan[$sensorId]);
                }else{
                    $sensorsPlan[$sensorId][] = $plan;
                }
            }else{
                $sensorsPlan[$sensorId][] = $plan;
            }
        });
        //Suskaiciuojame kiekvieno sensoriaus pirmo uzsakymo likusio derinimo laika
        foreach($sensorsPlan as $sensorId => &$plans){
            $usedTransition = round($this->calculateRemainingTransition($plans[0], $approvedOrderModel, $foundProblemModel)/60);
            $plans[0]['Plan']['preparation_time'] = max(0, $plans[0]['Plan']['preparation_time'] - $usedTransition);
            //$sensorsPlan[$sensorId][] = 
        }
        return $sensorsPlan;
    }

    //Ieskome uzsakymui likusio derinimo, kai tarp jo gamybos buvo iterptas kitas uzsakymas
    private function calculateRemainingTransition($plan, $approvedOrderModel, $foundProblemModel){
        //ieskome kada pirma karta sis uzsakymas buvo pradetas gaminti
        $firstCurrentOrder = $approvedOrderModel->find('first', array(
            'conditions'=>array(
                'ApprovedOrder.plan_id'=>$plan['Plan']['id']
            ),'order'=>array('ApprovedOrder.created')
        ));
        if(!$firstCurrentOrder) return 0;
        //surandame kita uzsakyma, kuris buvo gamintas veliau (buvo isiterpes)
        $lastOtherOrder = $approvedOrderModel->find('first', array(
            'conditions'=>array(
                'ApprovedOrder.plan_id <>'=>$plan['Plan']['id'], 
                'ApprovedOrder.created >'=>$firstCurrentOrder['ApprovedOrder']['end'],
                'ApprovedOrder.sensor_id'=>$plan['Plan']['sensor_id'],
            ),'order'=>array('ApprovedOrder.end DESC')
        ));
        if($lastOtherOrder){
            $startSearch = $lastOtherOrder['ApprovedOrder']['end'];
        }else $startSearch = '0000-00-00';
        //nuo isiterpusio uzsakymo pabaigos ieskome visu vykdytu perejimu esamam uzsakymui
        $transitionTime = $foundProblemModel->find('first',array(
            'fields'=>array('SUM(TIMESTAMPDIFF(SECOND, start, end)) AS sum'),
            'conditions'=>array(
                'FoundProblem.sensor_id'=>$plan['Plan']['sensor_id'],
                'FoundProblem.plan_id'=>$plan['Plan']['id'],
                'FoundProblem.start >='=>$startSearch,
                'FoundProblem.problem_id'=>Problem::ID_NEUTRAL
            )
        ));
        return intval($transitionTime[0]['sum']);
    }
}
