<?php
App::uses('ApprovedOrder', 'Model');
App::uses('Problem', 'Model');
class Histeel extends AppModel{
     
     private $recordsIds = array();
     public $useTable = false;
    
    //Workcenter kontroleris updateStatus metodas.
    public function changePlanSearchInWorkCenterHook(&$planSearchParams, &$fSensor){
        $sensorId = $fSensor['Sensor']['id'];
        if(in_array($sensorId, array(21,23,24,25,26))){
            $planModel = ClassRegistry::init('Plan');
            $planModel->bindModel(array('belongsTo'=>array('Possibility'=>array('foreignKey'=>false, 'conditions'=>array('Possibility.plan_id = Plan.id AND Possibility.sensor_id = '.$sensorId)))));
            if(isset($planSearchParams['conditions']['Plan.sensor_id'])) {
                unset($planSearchParams['conditions']['Plan.sensor_id']);
            }
            $planSearchParams['conditions'][] = 'Possibility.id IS NOT NULL';
        }
        $planSearchParams['order'] = array('IFNULL('.ApprovedOrder::NAME.'.status_id, 999) ASC', Plan::NAME.'.plan_start ASC');
    }
    
    public function verificationHook($recordModel, $currentPlan){
        if(!$currentPlan || !$currentPlan['Plan']['quantity'] || !$currentPlan['Plan']['client_speed']){
            return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>false);
        }
        $promptAfterSpecificCountReached = 20;
        //$promptEveryCountFormula = round($currentPlan['Plan']['quantity'] / ceil($currentPlan['Plan']['quantity'] / ($currentPlan['Plan']['client_speed'] * 2)));
        $madedCount = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.unit_quantity) AS sum'),
            'conditions'=>array('Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id'], 'Record.found_problem_id IS NULL')
        ));
        $verificationModel = ClassRegistry::init('Verification');
        $verificationsCount = $verificationModel->find('first', array(
            'fields'=>array('COUNT(Verification.id) AS count'), 
            'conditions'=>array('Verification.approved_order_id'=>$currentPlan['ApprovedOrder']['id'])
        ));
        $verificationsCount = (int)$verificationsCount[0]['count'];
        //1. ismesti lentele patikrai jei pagamino reikiama kieki nuo gamybos pradzios     
        if($verificationsCount == 0 && $madedCount[0]['sum'] >= $promptAfterSpecificCountReached){
            $verifOnEnd = $currentPlan['Plan']['production_time'] >= 120?false:true;
            $text = __('Gamybos pradžia. Atlikite detalių patikrą.');
            return array('promptVerification'=>true, 'countdown'=>0, 'verificationOnProductionEnd'=>$verifOnEnd, 'text'=>$text);
        }
        if($currentPlan['Plan']['production_time'] >= 120){
            $promptEveryCountFormula = round($currentPlan['Plan']['quantity'] / ceil($currentPlan['Plan']['production_time'] / 120)); 
            $promptEveryCountFormula = $promptEveryCountFormula > 0?$promptEveryCountFormula:1;
            $neededVerificationsCount = floor($madedCount[0]['sum'] / $promptEveryCountFormula);
            if($verificationsCount > 0 && $promptEveryCountFormula > 0 && $neededVerificationsCount >= $verificationsCount ){ //2 Ismesti lentele patikrai jei pagamintu gaminiu kiekis pasieke nauja patikra nuo paskutiniosios patikros
                $text = $currentPlan['Plan']['quantity'] <= $madedCount[0]['sum']?__('Gamybos pabaiga. Atlikite detalių patikrą.'):__('Tarpinė patikra. Atlikite detalių patikrą (atlikta %s iš %s).', $verificationsCount-1, $neededVerificationsCount);
                return array('promptVerification'=>true, 'countdown'=>0, 'verificationOnProductionEnd'=>false, 'text'=>$text);
            }else{ return array(
                    'promptVerification'=>false, 
                    'countdown'=>__('Iki kitos patikros liko').': '.($promptEveryCountFormula - $madedCount[0]['sum'] % $promptEveryCountFormula).' '.__('vnt.'),
                    'verificationOnProductionEnd'=>false
                );
            }
        }elseif($verificationsCount > 0){
            $text = __('Gamybos pabaiga. Atlikite detalių patikrą.');
            return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>true, 'text'=>$text);
        }
    }

    public function beforePlanPaginateHook(&$arr, &$requestParams){
    	if(isset($requestParams->data['start_planing'])){
    		App::import( 'Model', 'Histeel.HisteelPlan');
        	$planModel = new HisteelPlan($arr);
    	}
        $arr['order'] = array('Plan.plan_start');
        $arr['plan_exist'] = true;
    }

    public function Oeeshell_filterGetFactTimeRecords_Hook(&$approvedOrdersData, $currentShift, &$records, &$returnData){
        foreach($approvedOrdersData as &$approvedOrderData){
            if(!$approvedOrderData['Plan']['step']) continue;
            $approvedOrderData['Plan']['step'] *= $approvedOrderData['Plan']['box_quantity'];
        }
    }

    public function Record_calculateOee_Hook(&$oeeParams, $data){
        if(!isset($data['exceeded_transition_time'])){ $data['exceeded_transition_time'] = 0; }
        $greenYellowTimes =  $data['fact_prod_time']+$data['transition_time'] + $data['exceeded_transition_time'];
        $oeeParams['exploitation_factor'] = $data['shift_length'] > 0?round($greenYellowTimes / $data['shift_length'], 3):0;
        $oeeParams['exploitation_factor_exclusions'] = isset($data['shift_length_with_exclusions']) && $data['shift_length_with_exclusions'] > 0?round($greenYellowTimes / $data['shift_length_with_exclusions'], 3):0;
        $vardiklis = ($data['fact_prod_time'] + $data['transition_time'] + $data['exceeded_transition_time']);
        $oeeParams['operational_factor'] = $vardiklis > 0?($data['count_delay'] + $data['transition_time']) / $vardiklis : 0;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
    }
    
    public function Dashboard_beforeExecuteSearch_Hook(&$primaryData, &$indicator, &$fieldToSearch, &$groupByDate, &$model){
        switch($primaryData[$indicator]){
            case 0: //OEE be isimciu skaiciavimas
                //if($primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $fieldToSearch = array_filter($fieldToSearch, function($field){ return !strpos(strtolower($field), 'as sum'); });
                    $fieldToSearch[] = '(SUM('.$model.'.count_delay + '.$model.'.transition_time) / SUM('.$model.'.fact_prod_time + '.$model.'.transition_time + '.$model.'.exceeded_transition_time)) * (SUM('.$model.'.fact_prod_time + '.$model.'.transition_time) / SUM('.$model.'.shift_length)) * 100 AS sum';
                //}
            break;
            case 23:    //OEE su išimtimis skaiciavimas
                //if($primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $fieldToSearch = array_filter($fieldToSearch, function($field){ return !strpos(strtolower($field), 'as sum'); });
                    $fieldToSearch[] = '(SUM('.$model.'.count_delay + '.$model.'.transition_time) / SUM('.$model.'.fact_prod_time + '.$model.'.transition_time + '.$model.'.exceeded_transition_time)) * (SUM('.$model.'.fact_prod_time + '.$model.'.transition_time) / SUM('.$model.'.shift_length_with_exclusions)) * 100 AS sum';
               // }
            break;
            case 1://Prieinamumo koeficientas be isimciu
                //if($primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo pridedami perejima
                    $fieldToSearch = array_filter($fieldToSearch, function($field){ return !strpos(strtolower($field), 'as sum'); });
                    $fieldToSearch[] = 'SUM('.$model.'.fact_prod_time + '.$model.'.transition_time) / SUM('.$model.'.shift_length) * 100 AS sum';
                //}
            break;
            case 24: //Prieinamumo su išimtimis koeficientas
                //if($this->primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $fieldToSearch = array_filter($fieldToSearch, function($field){ return !strpos(strtolower($field), 'as sum'); });
                    $fieldToSearch[] = 'SUM('.$model.'.fact_prod_time + '.$model.'.transition_time) / SUM('.$model.'.shift_length_with_exclusions) * 100 AS sum';
                //}
            break;
            case 2: //Veiklos efektyvumo koeficientas
                //if($this->primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $fieldToSearch = array_filter($fieldToSearch, function($field){ return !strpos(strtolower($field), 'as sum'); });
                    $fieldToSearch[] = '(SUM('.$model.'.count_delay + '.$model.'.transition_time) / SUM('.$model.'.fact_prod_time + '.$model.'.transition_time + '.$model.'.exceeded_transition_time)) * 100 AS sum';
                //}
            break;
        }
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        foreach($plans as &$plan){
            $plan['Plan']['step'] = round($plan['Plan']['step'] / 60, 2);
        }
        if($currentPlan){
            $currentPlan['Plan']['step'] = round($currentPlan['Plan']['step'] / 60, 2);
            $currentPlan['Plan']['step_unit_divide'] = 1; 
        }
		foreach($allPlans as &$allPlan){
			$allPlan['Plan']['name'] .= ' ('.__('Operacijos kodas').': '.$allPlan['Plan']['paste_code'].')';
		}
    }
    
    public function WorkCenter_afterChoosePlan_Hook(&$fPlan, $fSensor, &$completedApprovedOrderExist){
        if(!empty($fPlan) && empty($completedApprovedOrderExist) && in_array($fSensor['Sensor']['id'], array(21,23,24,25,26))){
            $fPlan['Plan']['sensor_id'] = $fSensor['Sensor']['id'];
        }
    }
	
	public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
		$planModel = ClassRegistry::init('Plan');
		$planModel->updateAll(array('sensor_id'=>$fSensor['Sensor']['id']), array('Plan.id'=>$ord['ApprovedOrder']['plan_id']));
	}

	public function trySendOrdersXmlForPrevShift($currentShift){
        $markerPath = __dir__.'/../webroot/files/orders_export_in_progress.txt';
        if((file_exists($markerPath) && time() - filemtime($markerPath) <= 300) || $currentShift['Shift']['prev_shift_orders_exported']){
            return null;
        }
        $fh = fopen($markerPath, 'w');
	    fclose($fh);
	    $shiftModel = ClassRegistry::init('Shift');
	    $recordModel = ClassRegistry::init('Record');
	    $logModel = ClassRegistry::init('Log');
	    $prevShift = $shiftModel->getPrev($currentShift['Shift']['start'], $currentShift['Shift']['branch_id']);
	    if(empty($prevShift) || $prevShift['Shift']['disabled']){
            $shiftModel->updateAll(array('prev_shift_orders_exported'=>1), array('Shift.id'=>$currentShift['Shift']['id']));
	        return null;
	    }
	    $recordModel->bindModel(array('belongsTo'=>array('Sensor','Plan')));
	    $orders = $recordModel->find('all', array(
	        'fields'=>array(
	            'SUM(Record.quantity) AS quantity',
                'MAX(Record.created) AS order_end',
                'Sensor.name',
                'Plan.mo_number',
                'Plan.production_code',
                'Plan.paste_code',
                'Record.plan_id',
            ),'conditions'=>array(
	            'Record.shift_id'=>$prevShift['Shift']['id'],
                'Record.plan_id IS NOT NULL'
            ),'group'=>array(
                'Record.sensor_id', 'Record.plan_id'
            )
        ));
        $fileName = $prevShift['Shift']['id'].'_'.date('YmdHis',strtotime($prevShift['Shift']['start'])).'.xml';
        $xml = '<?xml version="1.0" encoding="utf-8"?><orders>';
        foreach($orders as $order){
            $xml .= '<order>';
            $xml .= sprintf('<work_center><![CDATA[%s]]></work_center>', $order['Sensor']['name']);
            $xml .= sprintf('<mo_number><![CDATA[%s]]></mo_number>', $order['Plan']['mo_number']);
            $xml .= sprintf('<production_code><![CDATA[%s]]></production_code>', $order['Plan']['production_code']);
            $xml .= sprintf('<paste_code><![CDATA[%s]]></paste_code>', $order['Plan']['paste_code']);
            $xml .= sprintf('<order_end><![CDATA[%s]]></order_end>', $order[0]['order_end']);
            $xml .= sprintf('<quantity><![CDATA[%s]]></quantity>', $order[0]['quantity']);
            $xml .= '</order>';
        }
        $xml .= '</orders>';
        $pathToFTP = 'ftp://hsftp1:6IlxBXLkGU5P@185.82.94.56:21/';
        if(preg_match('/^ftp:\/\/([^:]+):([^@]+)@([^:]+):([^\/]+)(\/.*)/i', $pathToFTP, $match)){
            if(sizeof($match) == 6){
                array_shift($match);
                list($ftpLogin, $ftpPass, $ftpServer, $ftpPort, $xmlFilePath) = $match;
            }
        }
        $ftpConn = ftp_connect($ftpServer,$ftpPort,10) or die("Could not connect to $ftpServer");
        $login = ftp_login($ftpConn, $ftpLogin, $ftpPass);
        ftp_set_option($ftpConn,FTP_TIMEOUT_SEC,1800);
        ftp_pasv($ftpConn, true);
        if(ftp_nlist($ftpConn, $xmlFilePath) == false) {
            @ftp_mkdir($ftpConn, $xmlFilePath);
        }
        $stream = fopen('php://memory', 'r+');
        fputs($stream, $xml);
        rewind($stream);
        if(ftp_fput($ftpConn, $xmlFilePath.'/'.$fileName, $stream, FTP_BINARY)){
            $shiftModel->updateAll(array('prev_shift_orders_exported'=>1), array('Shift.id'=>$currentShift['Shift']['id']));
        }else{
            $logModel->write('Nepavyko uzsakymu xml eksportas i FTP serveri');
        }
        fclose($stream);
        ftp_close($ftpConn);
        unlink($markerPath);
    }

}