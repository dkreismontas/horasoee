<?php
App::uses('AppModel', 'Model');

class HisteelAppModel extends AppModel {
    
    public static $navigationAdditions = array( );
    
    public static function loadNavigationAdditions() {
        self::$navigationAdditions[2] =
            array(
                'name'=>__('Darbo centrų apkrautumas'),
                'route'=>Router::url(array('plugin' => 'histeel','controller' => 'Graph/display', 'action' => 'index')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
}
