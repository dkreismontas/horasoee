<?php
App::uses('ApprovedOrder', 'Model');
class GraphController extends HisteelAppController{
    
    public $uses = array('Plan');
    
    public function display(){
        $condForAppOrd = array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE, 'ApprovedOrder.sensor_id = Plan.sensor_id')); //jungiame tik su baigtais uzsakymais
        $this->Plan->withRefs(false, $condForAppOrd); 
        $plans = $this->Plan->find('all', array(
            'fields'=>array(
                'SUM(TIMESTAMPDIFF(SECOND, Plan.plan_start, Plan.plan_end)) AS plan_length',
                'MAX(Plan.plan_end) AS plan_end',
                'Plan.sensor_id',
                'Plan.position_frozen',
                'Sensor.name'
            ),'conditions'=>array(
                'Sensor.pin_name <>'=>'',
                'ApprovedOrder.id IS NULL',
                'Plan.disabled'=>0,
                'Plan.has_problem'=>0,
            ),'group'=>array(
                'Plan.sensor_id',
                'Plan.position_frozen'
            ),'order'=>array(
                'Plan.sensor_id',
                'Plan.position_frozen DESC'
            )
        ));
        $sensorsPlan = array();
        $plansDurations = array();
        foreach($plans as $plan){
            $sensorId = $plan['Plan']['sensor_id'];
            if(!isset($plansDurations[$sensorId])){ $plansDurations[$sensorId] = 0; }
            $plansDurations[$sensorId] += $plan[0]['plan_length'];
            $sensorsPlan[$sensorId][] = $plan;
        }
        arsort($plansDurations);
        $maxDuration = current($plansDurations); 
        $this->set(compact('maxDuration', 'sensorsPlan'));
    }
    
}
