<style type="text/css">
    table tbody{
        font-size: 18px;   
    }
    table th{
        vertical-align: middle !important;
        text-align: center;
    }
    .frozen0, .nofrozenLegend{
        background: #84bfe9;
    }
    .frozen1, .frozenLegend{
        background: #3b6c8e;
    }
    .ilustration div{
        height: 35px;
        display: block;
        float: left;
    }
    .legend{
        color: #fff;
        padding: 3px;
        display: inline-block;
        text-shadow: 1px 1px #000;
    }
</style>
<div class="frozenLegend legend"><?php echo __('Užšaldytų užsakymų spalvinimas'); ?></div>
<div class="nofrozenLegend legend"><?php echo __('Paprastų užsakymų spalvinimas'); ?></div>
<table class="table table-bordered" id="graph">
    <thead>
        <tr>
            <th rowspan="2"><?php echo __('Darbo centras'); ?></th>
            <th colspan="2"><?php echo __('Apkrautumas') ?></th>
        </tr>
        <tr>
            <th><?php echo __('Užšaldytų užsakymų pabaigos data') ?></th>
            <th><?php echo __('Paskutinio užsakymo pabaigos data'); ?></th>
        </tr>
    </thead>
    <tbody>
        <?php //pr($sensorsPlan);
        foreach($sensorsPlan as $sensorId => $plans){ ?>
            <tr>
               <td style="width: 200px;"><?php echo current($plans)['Sensor']['name']; ?></td> 
               <td colspan="2" class="ilustration">
                   <?php
                    foreach($plans as $plan){
                        ?><div length="<?php echo $plan['0']['plan_length']; ?>" class="frozen<?php echo intval($plan['Plan']['position_frozen']); ?>"></div><?php
                    }  
                   ?>
               </td>
            </tr>
            <tr>
               <td></td>
               <td><?php 
                    $planFrozen = array_filter($plans, function($data){ return intval($data['Plan']['position_frozen']) == 1; });
                    echo $planFrozen?current($planFrozen)[0]['plan_end']:'';
               ?></td>
               <td><?php 
                    $planSimple = array_filter($plans, function($data){ return intval($data['Plan']['position_frozen']) == 0; });
                    echo $planSimple?current($planSimple)[0]['plan_end']:'';
               ?></td>
            </tr>
        <?php } ?>
    </tbody>
</table>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var scaling = $('tbody tr:first-child td.ilustration').width() / <?php echo $maxDuration; ?>;
        $('.ilustration div').each(function(){
            $(this).css({width: Math.round($(this).attr('length')*scaling)+'px'});
        })
    });
</script>
