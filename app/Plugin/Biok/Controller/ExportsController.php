<?php
class ExportsController extends AppController{
    
    public $uses = array('Shift','Record','Sensor','ApprovedOrder','FoundProblem','Problem','Biok.Biok');
    public $components = array('Help');
    
    public function xls_export(){
        $this->Sensor->virtualFields = array('full_name' => 'CONCAT(Sensor.name," ",Sensor.ps_id)');
        $sensorsList = $this->Sensor->find('list', array('fields' => array('Sensor.id', 'Sensor.full_name'), 'conditions'=>array(
            //'Sensor.pin_name <>'=>''
        )));
        $title_for_layout = __('Užsakymų eksportas');
        $this->set(compact('sensorsList','title_for_layout'));
    }
    
    public function generate_xls(){
        $errors = array();
        $this->layout = 'ajax';
        $timePattern = isset($this->request->data['time_pattern'])?$this->request->data['time_pattern']:'';
        $sensors = isset($this->request->data['sensors'])?$this->request->data['sensors']:array();
        if(empty($sensors)){ $errors[] = __('Neparinkti darbo centrai'); }
        if(!$timePattern){
            if(!isset($this->request->data['start_date']) || !trim($this->request->data['start_date'])){
                $errors[] = __('Neįvesta laiko intervalo pradžia');
            }else{
                $start = $this->request->data['start_date'];
            }
            if(!isset($this->request->data['end_date']) || !trim($this->request->data['end_date'])){
                $errors[] = __('Neįvesta laiko intervalo pradžia');
            }else{
                $end = $this->request->data['end_date'];
            }
        }else{
            $this->request->data['sensors'] = $sensors;
            list($start,$end) = $this->Help->setStartEndByPattern($this->request->data);
        }
        if(!empty($errors)){
            $this->Session->setFlash('<ul><li>'.implode('</li><li>',$errors).'</li></ul>');
            $this->redirect(array('plugin'=>'biok', 'controller'=>'exports', 'action'=>'xls_export'));
        }
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $orders = $this->ApprovedOrder->find('all', array(
            'conditions'=>array(
                'ApprovedOrder.created <' => $end,
                'ApprovedOrder.end >' => $start,
                'ApprovedOrder.sensor_id' => $sensors,
            ),'order'=>array('ApprovedOrder.created')
        ));
        $this->FoundProblem->bindModel(array('belongsTo'=>array('Problem')), false);
        $availableProblems = array();
        foreach($orders as &$order){
            $order['Problems'] = Hash::combine($this->FoundProblem->find('all', array(
                'fields'=>array(
                    'SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) AS duration',
                    'TRIM(Problem.name) AS title',
                    'TRIM(Problem.id) AS id',
                    'COUNT(FoundProblem.id) AS count', 
                    'TRIM(FoundProblem.transition_problem_id) AS transition_problem_id', 
                    'TRIM(FoundProblem.json) AS json', 
                ),'conditions'=>array(
                    'FoundProblem.plan_id'=>$order['Plan']['id'],
                    'Problem.id IS NOT NULL'
                ),'group'=>array(
                    'FoundProblem.problem_id'
                ),'order'=>array('FoundProblem.id')
            )), '{n}.0.id', '{n}.0');
            $availableProblems = array_unique(array_merge($availableProblems, array_keys($order['Problems'])));
            $shifts = $this->Shift->find('all', array('conditions'=>array('OR'=>array(
                '\''.$order['ApprovedOrder']['created'].'\' BETWEEN Shift.start AND Shift.end',
                '\''.$order['ApprovedOrder']['end'].'\' BETWEEN Shift.start AND Shift.end',
            )),'order'=>array('Shift.start')));
            $order['Shift'] = current($this->Biok->getLongestShift($shifts, $order));
        }
        $availableProblems = array_filter($availableProblems, function($data){ return $data >= 2; });
        sort($availableProblems); 
        $availableProblems = array_combine($availableProblems, $availableProblems);
        $transitionProblems = Hash::combine($this->Problem->find('all', array('conditions'=>array('transition_problem'=>1))),'{n}.Problem.id','{n}.Problem');
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(compact('orders','objPHPExcel','availableProblems','transitionProblems'));
    }
    
}
