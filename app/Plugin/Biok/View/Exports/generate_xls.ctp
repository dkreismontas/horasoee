<?php
    ob_end_clean();
    error_reporting(0);
    $styleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman'
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
    );
    $headerStyleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman',
            'bold'=>true
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'wrap'=>true,
            'rotation'   => 0,
        ),
    );

//HEADERS
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Ataskaita'));
$rowNr = 1;
$problemColStart = 17;
$activeSheet->setCellValueByColumnAndRow($problemColStart, $rowNr, __('Prastovos'))->mergeCells(cts($problemColStart, $rowNr).':'.cts(($problemColStart+sizeof($availableProblems)*2)-1, $rowNr))->getStyle(cts($problemColStart, $rowNr))->applyFromArray($headerStyleArray);
$rowNr = 2;
$activeSheet->setCellValueByColumnAndRow(3, $rowNr, __('Gamybos pradžia'))->mergeCells(cts(3, $rowNr).':'.cts(4, $rowNr))->getStyle(cts(3, $rowNr))->applyFromArray($headerStyleArray);
$activeSheet->setCellValueByColumnAndRow(5, $rowNr, __('Gamybos pabaiga'))->mergeCells(cts(5, $rowNr).':'.cts(6, $rowNr))->getStyle(cts(5, $rowNr))->applyFromArray($headerStyleArray);
$colNr = $problemColStart+sizeof($availableProblems);
$activeSheet->setCellValueByColumnAndRow($problemColStart, $rowNr, __('Pagal laiką (min.)'))->mergeCells(cts($problemColStart, $rowNr).':'.cts($colNr-1, $rowNr))->getStyle(cts($problemColStart, $rowNr))->applyFromArray($headerStyleArray);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagal kiekį'))->mergeCells(cts($colNr, $rowNr).':'.cts($colNr+sizeof($availableProblems)-1, $rowNr))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray);
$rowNr = 3;$colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbo centras'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Barkodas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkto pavadinimas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Data'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Laikas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Data'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Laikas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Kiekis'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Partija'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojas1'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojas2'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojas3'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojas4'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojų kiekis'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pamaina'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Greitis planas vnt/h'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Greitis faktas vnt/h'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;
$colNr = $problemColStart+sizeof($availableProblems)*2;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Perėjimo tipas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo perėjimo trukmė tikslas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo perėjimo trukmė faktas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Perėjimo viršijimas (min)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Perėjimo viršijimo pavadinimas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(20); $colNr++;

$colNr = 0; $rowNr = 4;
foreach($orders as $key => $order){
    //pr($availableProblems);
    //pr($order);die();
    $additionalData = json_decode($order['ApprovedOrder']['additional_data'],true);
    if(isset($additionalData->worker1)){ $worker1 = $additionalData->worker1; $workersCount++; }else{};
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Sensor']['name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['production_code']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['production_name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, date('Y-m-d',strtotime($order['ApprovedOrder']['created'])));
    $activeSheet->getStyleByColumnAndRow($colNr-1,$rowNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, date('H:i',strtotime($order['ApprovedOrder']['created'])));
    $activeSheet->getStyleByColumnAndRow($colNr-1,$rowNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, date('Y-m-d',strtotime($order['ApprovedOrder']['end'])));
    $activeSheet->getStyleByColumnAndRow($colNr-1,$rowNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, date('H:i',strtotime($order['ApprovedOrder']['end'])));
    $activeSheet->getStyleByColumnAndRow($colNr-1,$rowNr)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_DATE_DATETIME);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($additionalData['election_number'])?$additionalData['election_number']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($additionalData['worker1'])?$additionalData['worker1']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($additionalData['worker2'])?$additionalData['worker2']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($additionalData['worker3'])?$additionalData['worker3']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($additionalData['worker4'])?$additionalData['worker4']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, sizeof(array_filter($additionalData, function($value,$key){ return preg_match('/^worker\d+/i',$key) && trim($value); },ARRAY_FILTER_USE_BOTH)));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Shift']['type']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['step']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($order['Problems'][-1])?round($order['ApprovedOrder']['quantity'] / ($order['Problems'][-1]['duration'] / 3600 )):0);
    foreach($availableProblems as $problemId => &$problemTitle){
        if(!isset($order['Problems'][$problemId])){ $colNr++; continue; }
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, round($order['Problems'][$problemId]['duration'] / 60, 2));
        $problemTitle = $order['Problems'][$problemId]['title'];
    }
    unset($problemTitle);
    foreach($availableProblems as $problemId => $problemTitle){
        if(!isset($order['Problems'][$problemId])){ $colNr++; continue; }
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Problems'][$problemId]['count']);
    }
    $transitions = array_filter($order['Problems'], function($data){ return $data['transition_problem_id'] > 0; });
    if(!empty($transitions)){
        $transitionId = current($transitions)['transition_problem_id'];
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($transitionProblems[$transitionId])?$transitionProblems[$transitionId]['name']:'');
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($transitionProblems[$transitionId])?$transitionProblems[$transitionId]['transition_time']:'');
    }else{ $colNr += 2; }
    //$activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['preparation_time']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($order['Problems'][1])?round($order['Problems'][1]['duration'] / 60, 2):0);
    
    $exceededTransitions = array_filter($order['Problems'], function($data){ $jsonData = json_decode($data['json']); return isset($jsonData->transition_exceeded); });
    if(!empty($exceededTransitions)){
        $totalDuration = 0; $exceededTransitionsTitles = array();
        foreach($exceededTransitions as $exceededTransition){
            $totalDuration += $exceededTransition['duration'];
            $exceededTransitionsTitles[] = $exceededTransition['title'];
        }
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, round($totalDuration/60,2));
        $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, implode(',', array_unique($exceededTransitionsTitles)));
    }else{ $colNr += 2; }
    
    for($i = 0; $i < $colNr; $i++){
        $activeSheet->getStyleByColumnAndRow($i, $rowNr)->applyFromArray($styleArray);
    }
    $rowNr++; $colNr = 0;
}
$maxColNr = $colNr;
$rowNr = 3;

foreach($availableProblems as $problemTitle){
    $activeSheet->setCellValueByColumnAndRow($problemColStart, $rowNr, $problemTitle)->getStyle(cts($problemColStart, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($problemColStart))->setWidth(15); $problemColStart++;
}
foreach($availableProblems as $problemTitle){
    $activeSheet->setCellValueByColumnAndRow($problemColStart, $rowNr, $problemTitle)->getStyle(cts($problemColStart, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($problemColStart))->setWidth(15); $problemColStart++;
}

for ($col = 0; $col != $maxColNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setAutoSize(true);
}
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . date('Y-m-d') . '_' . time() . '.xlsx"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}