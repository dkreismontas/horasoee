<div class="pageheader">
    <div class="pageicon"><span class="iconfa-book"></span></div>               <div class="pagetitle">
        <h1><?php echo $title_for_layout; ?></h1>
    </div>
</div>
<div role="tabpanel" class="tab-pane active" id="one">
<br>
<?php $errorMessage = $this->Session->flash(); echo trim($errorMessage)?'<h3 class="widgettitle title-danger">'.$errorMessage.'</h3>':''; ?>
<div class="row">
    <div class="col-md-6">
        <br>
        <?php         echo $this->Form->create(false, array(
        'url'           => '/biok/plugins/generate_xls',
        'class'         => 'form form-horizontal',
        'role'          => 'form',
        'inputDefaults' => array(
        'class' => 'form-control datepicker',
        )
        ));?>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-3">
                    <?php echo $this->Form->input('start_date', array('label' => __('Nuo'))); ?>
                </div>
                <div class="col-md-3">
                    <?php echo $this->Form->input('end_date', array('label' => __('Iki'))); ?>
                    <?php echo $this->Form->input('time_pattern', array('type'=>'hidden','class'=>'time_pattern','id'=>false)); ?>
                </div>
                <div class="col-md-3">
                    <div id="mainFormTimePatterns" class="dropdown col-xs-1" style="padding-left: 0;">
                        <span class="btn glyphicon glyphicon-calendar" data-toggle="dropdown"></span>
                        <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="0"><?php echo __('Praėjusi pamaina'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="1"><?php echo __('Praėjusi para'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="2"><?php echo __('Praėjusi savaitė'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="10"><?php echo __('Praėjusi savaitė iki dabar'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="3"><?php echo __('Praėjęs mėnuo'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="9"><?php echo __('Praėjęs mėnuo iki dabar'); ?></a></li>
                          <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="4"><?php echo __('Praėję metai'); ?></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="col-md-6">
                    <?php       echo $this->Form->input('sensors', array(
                    'label' => false,
                    'options' => $sensorsList,
                    'multiple' => true,
                    'class' => 'form-control', 'before'=>'<label class="col-md-12" style="padding:0">'.__('Darbo centras').'</label><div class="col-md-12" style="padding:0">','after'=>'</div>','div'=>'',
                    ));
                    ?>
                </div>
            </div>
        </div>
        <?php echo $this->Form->submit(__('Siųstis'), array('class' => 'btn btn-primary','style' => 'margin-left:15px;margin-top:10px;'));?>
        <?php echo $this->Form->end();?>
    </div>
</div>
</div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('#mainFormTimePatterns li a').bind('click',function(){
            $('#xls_exportForm #start_date').val($(this).html());
            $('#xls_exportForm #end_date').val($(this).html());
            $('#xls_exportForm input.time_pattern').val($(this).data('timeformat'));
        });
        jQuery(".datepicker").datetimepicker({
            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',
            firstDay : 1,
            closeText: 'Uždaryti',
            currentText: 'Dabar',
            timeText: 'Laikas',
            hourText: 'Valandos',
            minuteText: 'Minutės',
            alwaysSetTime: true,
            autoClose: false,
            onSelect: function(dateText,inst){
                $('#xls_exportForm input.time_pattern').val('');
            }
        });
    });
</script>
