<?php
class Biok extends AppModel{
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     
     public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Biok/css/Biok_bare.css';
     }
     
     public function getLongestShift($shifts, $order){
         if(sizeof($shifts) == 1){ return current($shifts); }
         $workInShift = array();
         foreach($shifts as $key => $shift){
            if(strtotime($order['ApprovedOrder']['created']) >= strtotime($shift['Shift']['start']) && strtotime($order['ApprovedOrder']['end']) <= strtotime($shift['Shift']['end'])){//uzsakymas buvo gamintas visoje pamainoje
                return $shift; //jei uzsakymas prasidejo ir baigesi sioje pamainoje, tai ja ir graziname
            }elseif(strtotime($order['ApprovedOrder']['created']) >= strtotime($shift['Shift']['start'])){ //uzsakymas prasidejo sioje pamainoje
                $workInShift[$key] = strtotime($shift['Shift']['end']) - strtotime($order['ApprovedOrder']['created']);
            }else{ //uzsakymas baigesi sioje pamainoje
                $workInShift[$key] = strtotime($order['ApprovedOrder']['end']) - strtotime($shift['Shift']['start']);
            }
         }
         if($workInShift){
             arsort($workInShift); //susirikiuojame taip, kad pirma butu pamaina su didziausia trukme issaugant masyvo raktus. 
             return $shifts[key($workInShift)]; //graziname ta pamaina, kurios raktas turi didziausia trukme
         }
     }

     public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes){
         $problemModel = ClassRegistry::init('Problem');
         $recordModel = ClassRegistry::init('Record');
         $foundProblemModel = ClassRegistry::init('FoundProblem');
         $fTransitionProblemTypes = $problemModel->find('threaded', array(
            'conditions' => array(
                Problem::NAME.'.sensor_type' => array(0, $fSensor[Sensor::NAME]['type']),
                Problem::NAME.'.line_id'=>array(0,$fSensor[Line::NAME]['id']),
                Problem::NAME.'.disabled'=>0,
                Problem::NAME.'.transition_problem'=>1,
                Problem::NAME.'.transition_time >'=>0,
            ),
            'order' => array(Problem::NAME.'.name ASC')
        ));
        $fzeroTimeTransitionProblemTypes = $problemModel->find('threaded', array(
            'conditions' => array(
                Problem::NAME.'.sensor_type' => array(0, $fSensor[Sensor::NAME]['type']),
                Problem::NAME.'.line_id'=>array(0,$fSensor[Line::NAME]['id']),
                Problem::NAME.'.disabled'=>0,
                Problem::NAME.'.transition_problem'=>1,
                Problem::NAME.'.transition_time'=>0,
            ),
            'order' => array(Problem::NAME.'.name ASC')
        ));
        if($currentPlan){
            $productionStarted = $recordModel->find('first', array('conditions'=>array(
                'Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id'],
                'Record.found_problem_id IS NULL',
                'Record.quantity >' => 0
            )));
            if(!$productionStarted){ //jei gamyba neprasidejusi, isvedame paskutini derinima kuris turejo derinimo laika (derinimo virsijimai neturi laiko)
                $foundProblemModel->bindModel(array('belongsTo'=>array('Problem'=>array('foreignKey'=>'transition_problem_id'))));
                $transition = $foundProblemModel->find('first', array(
                    'fields'=>array('Problem.transition_time', 'Problem.name'),
                    'conditions'=>array(
                        'FoundProblem.plan_id' => $currentPlan['Plan']['id'],
                        'FoundProblem.sensor_id' => $fSensor['Sensor']['id'],
                        'Problem.transition_time >' => 0
                    ),'order'=>array('FoundProblem.start DESC')
                ));
                if($transition){
                    $currentPlan['Plan']['transition_title'] = $transition['Problem']['name']; 
                    $currentPlan['Plan']['transition_time'] = $transition['Problem']['transition_time']; 
                }
            }else{
                $currentPlan['Plan']['transition'] = ''; 
            }
            $currentPlan['Plan']['step'] /= 60; 
            $currentPlan['Plan']['step_unit_divide'] = 1; 
         }
     }

}
    