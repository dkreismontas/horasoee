<?php
App::uses('AppModel', 'Model');

Class BiokAppModel extends AppModel {
    public static $navigationAdditions = array( );
    
    public static function loadNavigationAdditions() {
        self::$navigationAdditions[3] =
            array(
                'name'=>'Biok xls ataskaita',
                'route'=>Router::url(array('plugin' => 'biok','controller' => 'exports/xls_export')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
    
}
