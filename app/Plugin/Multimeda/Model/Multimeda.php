<?php
class Multimeda extends AppModel{
     private $recordsIds = array();
     private $recordHavingQuantityDetected = false;
     
    //Multimeda hooksas, kurio pagalba uzsakymas bus laikomas pradetu (manager aplinkoj ismes lentele kas dabar gaminama) kai tarp irasu su kiekias atejusiu nuliniu irasu kiekis nebus didensis kaip 5 irasai
    /*public function detectEventsHook(&$record, &$siblingRowsHavingQuantity, $currentPlan, &$mustReachZerosCountToStartProblem){
        
    }*/
    public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
    }
    
    public function changeUpdateStatusVariablesHook($fSensor, $plans, $currentPlan, &$quantityOutput, $fShift){
        $recordModel = ClassRegistry::init('Record');
        $quantity = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.unit_quantity) AS total'), 
            'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
        ));
        if($quantity[0]['total'] > 0){
            //$currentPlan['ApprovedOrder']['quantity'] = $quantity[0]['total'];
            //$currentPlan['ApprovedOrder']['box_quantity'] = $quantity[0]['total'];
            $quantityOutput['quantity'] = number_format($quantity[0]['total'],2,'.',''); 
        } 
    }
      
}
    