<?php
App::uses('MultimedaAppController', 'Multimeda.Controller');

class PluginsController extends MultimedaAppController {
	
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        die();
    }
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        //vykdome iraso iterpima i DB
        //$saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $context = stream_context_create(array (
            'http' => array (
                'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            )
        ));
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
        //$saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://192.168.0.10:5580/horas/api/v1.0/get',
            '5680'=>'http://192.168.0.222:5680/data',
        );
        foreach($adressList as $addressPort => $adress){
            $data = json_decode(file_get_contents($adress, false, $context));
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $title){
                    list($sensorName, $sensorPort) = explode('_',$title);
                    if($addressPort != $sensorPort) continue;
                    if(!isset($record->$sensorName)) continue;
                    if(in_array($sensorId, array(3,4))){
                        $quantity = $record->$sensorName / 1000;
                    }elseif(in_array($sensorId, array(7,8,9))){
                        $quantity = bcmul($record->$sensorName,0.22,6);
                    }else{
                        $quantity = $record->$sensorName;
                    }
                    $quantity = abs($quantity);
                    $db->query('CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ');
                }
            }
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->moveOldRecords();
        
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }



    public function moveOldRecords() {
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        die();
    }
	
}
