<?php
class Vildeta extends AppModel{

     public function Record_calculateOee_Hook(&$oeeParams, $data){
         $oeeParams['operational_factor'] = 1;
         $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
         $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }

    public function Tv_parametersCalculationAfterValuesSet_Hook(&$sensor){
        if(isset($sensor['operational_factor'])) {
            unset($sensor['operational_factor']);
        }
    }

    public function WorkCenter24_AfterGetData_Hook(&$sensorsData){
        unset($sensorsData['oee_data']['operational_factor']);
    }

    public function WorkCenter_BeforeUpdateStatus_Hook(&$fSensor, $user){
        if(isset($user['sensor_id']) && trim($user['sensor_id'])){
            Configure::write('graphHoursDefaultLinesCount', 8);
        }
    }

}
    