<?php
App::uses('ApprovedOrder', 'Model');
App::uses('Sensor', 'Model');
App::uses('Plan', 'Model');
App::uses('Exporter', 'Controller/Component');
class Leibur extends AppModel{
    public $name = 'Leibur';
    private static $virtualSensors = array(0,13,25);
     
    //Leibur hooksas, kurio pagalba uzsakymas bus laikomas pradetu (manager aplinkoj ismes lentele kas dabar gaminama) kai tarp irasu su kiekias atejusiu nuliniu irasu kiekis nebus didensis kaip 5 irasai
    /*public function detectEventsHook(&$record, &$siblingRowsHavingQuantity, $currentPlan, &$mustReachZerosCountToStartProblem){
        
    }*/
    /*public function recalcOeeHook(&$val=array()){
        
    }*/
    
    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $this->calculateHoursInfo($dcHoursInfoBlocks, $hours, $fSensor);
        //planuojamu vykdyti uzsakymu kiekio apasakiciavimas dezemis
        foreach($plans as &$plan){
            if($fSensor['Sensor']['type'] == 3){
	            if($plan['Plan']['box_quantity'] == 0){$plan['Plan']['box_quantity'] = 1;}
	            $totalBoxQuantity = floor($plan['Plan']['quantity'] / $plan['Plan']['box_quantity']);
	            $plan['Plan']['quantity'] = sprintf(__('%d dėžių (%d vnt.)'), $totalBoxQuantity, $plan['Plan']['quantity']);
			}else{
				$plan['Plan']['quantity'] = sprintf(__('%d vienetai leibur'), $plan['Plan']['quantity']);
			}
        }
        //siuo metu vykdomo uzsakymo kiekio apsakiciavimas paletemis bei dezemis (burbuliukas prie paskutinio intervalo darbo centro grafike)
        if($currentPlan && !empty($quantityOutput)) {
        	if($fSensor['Sensor']['type'] == 3){
	            if($currentPlan['Plan']['box_quantity'] == 0){$currentPlan['Plan']['box_quantity'] = 1;}
	            $totalBoxQuantity = floor($currentPlan['Plan']['quantity'] / $currentPlan['Plan']['box_quantity']);
	            $currentPlan['Plan']['quantity'] = sprintf(__('%d dėžių (%d vnt.)'), $totalBoxQuantity, $currentPlan['Plan']['quantity']);
			}else{
				$currentPlan['Plan']['quantity'] = sprintf(__('%d vienetai leibur'), $currentPlan['Plan']['quantity']);
			}
			
            if($fSensor['Sensor']['type'] == 3){
				$quantityOutput['quantity'] =  sprintf(__('%d dėžių (%d vnt.)'), $currentPlan['ApprovedOrder']['box_quantity'], $currentPlan['ApprovedOrder']['quantity']);
            }elseif(in_array($fSensor[Sensor::NAME]['id'], array(4))){
            	$quantityOutput['quantity'] =  sprintf(__('%d vienetai leibur'), $currentPlan['ApprovedOrder']['box_quantity']);
            }else{
            	$quantityOutput['quantity'] =  sprintf(__('%d vienetai leibur'), $currentPlan['ApprovedOrder']['quantity']);
            }
            //logika, pagal kuria nusakoma ar atvaizduoti Deklaruoti kiekius mygtuka: deklaruoti kiekius atitinkamam uzsakymui vartotojas gali tik viena karta
            // $partialQuantityModel = ClassRegistry::init('PartialQuantity');
            // $hasPartialQuantity = $partialQuantityModel->find('first', array('conditions'=>array('PartialQuantity.sensor_id'=>$fSensor['Sensor']['id'], 'PartialQuantity.approved_order_id'=>$currentPlan['ApprovedOrder']['id'])));
            // if($hasPartialQuantity){
                // $fSensor['Sensor']['time_interval_declare_count_button'] = 0;
            // }
        }
        if($fSensor['Sensor']['type'] == 3){
            $fSensor['Sensor']['declare_using_crates_and_pieces'] = __('Dėžės');
        }
    }

    public function Record_FindAndSetOrder_Hook($sensor, $approvedOrderId, $plan, $multiplier, &$fields, &$conditions){
        $multiplier = 1;
        if ($sensor[Sensor::NAME]['type'] == Sensor::TYPE_PACKING && $sensor[Line::NAME]['industrial'] && !in_array($sensor[Sensor::NAME]['id'], array(3))) {
            $multiplier = intval($plan[Plan::NAME]['box_quantity']);
        }
        if(in_array($sensor[Sensor::NAME]['id'], array(2,4,9))){
            //$fields = array('Record.quantity'=>'quantity / '.$plan['Plan']['qty_pack']) + $fields;
            $multiplier = bcdiv(1,$plan['Plan']['qty_pack'],6);
        }
        $fields['Record.unit_quantity']='`quantity` * '.$multiplier;
    }
    
    public function WorkCenter_ExtendProdUpdateOrderQuantities_Hook(&$approvedOrder, &$recordModel, &$currentPlan){
        $approvedOrder[ApprovedOrder::NAME]['quantity'] = $recordModel->findAndSumOrder($approvedOrder[ApprovedOrder::NAME]['id']);
        if(in_array($approvedOrder[ApprovedOrder::NAME]['sensor_id'], array(2,4,9))){
            $currentPlan['Plan']['box_quantity'] = 0;
            $approvedOrder[ApprovedOrder::NAME]['box_quantity'] = bcmul($approvedOrder[ApprovedOrder::NAME]['quantity'], $currentPlan[Plan::NAME]['qty_pack'], 6);
        }
        return true;
    }
    
    public function Workcenter_BeforeSavePartialQuantity_Hook(&$request, &$item, &$fSensor, &$currentPlan){
        if($fSensor['Sensor']['type'] == 3 && $currentPlan && $currentPlan['Plan']['box_quantity'] > 0){
            $crates = (int) $request->query('declaredQuantity');
            $pieces = (int) $request->query('declaredQuantityPieces');
            $item['quantity'] = $crates * $currentPlan['Plan']['box_quantity'] + $pieces;
            if($item['quantity'] > 0) {
                $exporterModel = ClassRegistry::init($this->name . '.Exporter');
                $currentPlan['ApprovedOrder']['confirmed_quantity'] = $item['quantity'];
                $currentPlan['ApprovedOrder']['end'] = date('Y-m-d H:i:s');
                $exporterModel->exportCompleted($currentPlan);
            }
        }elseif($fSensor['Sensor']['type'] == 1 && $currentPlan && $currentPlan['Plan']['box_quantity'] > 0){
            $crates = (int) $request->query('declaredQuantity');
            $item['quantity'] = $crates * $currentPlan['Plan']['box_quantity'];
        }
    }
    
    public function Workcenter_changeUpdateStatusVariables_Hook2(&$data){
        static $recordModel = null;
        if($recordModel == null){ $recordModel = ClassRegistry::init('Record'); }
        if($data['currentPlan']){
            $unitQuantitySum = $recordModel->findAndSumOrder($data['currentPlan']['ApprovedOrder']['id']);
            $data['currentPlan']['Plan']['production_code'] .= ' '.$data['currentPlan']['Plan']['mo_number'];
            if($data['sensor']['type'] == 3){
                $pieces = $data['orderPartialQuantity'];
                $data['orderPartialQuantity'] = (int)floor($pieces / $data['currentPlan']['Plan']['box_quantity']);//"Jau ivesta" deziu reiksme
                $data['orderPartialQuantityPieces'] = (int) $pieces % $data['currentPlan']['Plan']['box_quantity'];//"Jau ivesta" vienetu reiksme
                $data['partialQuantity'] = max(0,(int)(($unitQuantitySum - $pieces) / $data['currentPlan']['Plan']['box_quantity']));//Deziu ivesties laukelio default reiksme (dar neuzdeklaruotas kiekis dezemis)
                $data['partialQuantityPieces'] = max(0, ($unitQuantitySum - $pieces) % $data['currentPlan']['Plan']['box_quantity']);//Vienetu ivesties laukelio default reiksme (dar neuzdeklaruotas kiekis vienetais atskaicius pilnas dezes)
            }elseif(in_array($data['sensor']['type'], array(1,2))){
                $pieces = $data['orderPartialQuantity'];
                $data['partialQuantity'] = max(0,($unitQuantitySum - $pieces));
            }
        }
        if(isset($data['texts']['confirmProblemChoose'])){
            $data['texts']['confirmProblemChoose'] = '';
        }
        //Jei siuo metu atidarytas vienas sis id:13 arba id:25 virtualiu sensoriu ir jam rasomi duomenys, visi mygtukai turi buti nematomi operatoriui, nes ji neturi galeti nei pradeti naujo plano nei uzbaigti kol nesubegs visi irasai
        if(in_array($data['sensor']['id'], self::$virtualSensors)){
            $markerPath = __dir__.sprintf('/../webroot/files/virtual_sensors_update_%d.txt', $data['sensor']['id']);
            if(file_exists($markerPath) || !$data['sensor']['id']){
                array_walk($data['sensor'], function(&$value,$key){ if(preg_match('/.+button/i', $key)){ $value = 0; } });
                $data['sensor']['has_access_to_register_events'] = false;
                $data['event'] = new TimelineEvent(TimelineEvent::TYPE_NONE, null, null, null);
            }
        }
    }

    public function ApprovedOrdersIndexHook(&$controller){
        $controller->PartialQuantity->virtualFields = array('quantity_sum'=>'SUM(PartialQuantity.quantity)');
        $partialQuantities = $controller->PartialQuantity->find('list', array(
            'fields' => array('PartialQuantity.approved_order_id','quantity_sum'),
            'conditions' => array('PartialQuantity.approved_order_id' =>  Set::extract('/ApprovedOrder/id', $controller->viewVars['list'])),
            'group'      => array('PartialQuantity.approved_order_id')
        ));
        $controller->PartialQuantity->virtualFields = array();
        foreach($controller->viewVars['list'] as &$list){
            $appOrderId = $list['ApprovedOrder']['id'];
            $list['ApprovedOrder']['partial_quantities_sum'] = isset($partialQuantities[$appOrderId])?$partialQuantities[$appOrderId]:0;
        }

        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/index');
    }
    public function ApprovedOrdersEditHook(&$controller){
        if(!empty($controller->request->data) && !isset($controller->request->data['Sensor'])){//vykdomas uzsakymo patvirtinimas
            $id = $controller->request->data['ApprovedOrder']['id'];
            $item = $controller->ApprovedOrder->withRefs()->findById($id);
            $shiftModel = ClassRegistry::init('Shift');
            $currentTime = date('Y-m-d H:i:s');
            $currentShift = $shiftModel->find('first', array('conditions'=>array('Shift.start <='=>$currentTime, 'Shift.end >='=>$currentTime, 'Shift.branch_id'=>$item['Branch']['id'])));
            if($currentShift){
                $updateData = array('confirmation_shift_id'=>$currentShift['Shift']['id']);
                //papildomai is records irasu ieskome ar uzsakymas buvo gamintas kitoje pamainoje nei vykdomas patvirtinimas. Jei buvo, tuomet turi buti daliniu kiekiu is partial_quantities lenteles tai pamainai. Jei nera, vesime pranesima
                $recordModel = ClassRegistry::init('Record');
                $prevShiftsIdsList = $recordModel->find('list', array(
                    'fields'=>array('Record.shift_id'),
                    'conditions'=>array(
                        'Record.sensor_id'=>$item['ApprovedOrder']['sensor_id'],
                        'Record.shift_id <>'=>$currentShift['Shift']['id'],
                        'Record.approved_order_id'=>$id,
                    ),'group'=>array('Record.shift_id')
                ));
                if(!empty($prevShiftsIdsList)){
                    $partialQuantities = $controller->PartialQuantity->find('all', array(
                        'conditions'=>array(
                            'PartialQuantity.sensor_id'=>$item['ApprovedOrder']['sensor_id'],
                            'PartialQuantity.shift_id'=>$prevShiftsIdsList,
                            'PartialQuantity.approved_order_id'=>$id,
                        )
                    ));
                    if(empty($partialQuantities)){
                        $updateData['prev_shift_no_quantities'] = 1;
                    }
                }
                $controller->ApprovedOrder->updateAll($updateData, array('ApprovedOrder.id'=>$id));
            }
        }
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/edit');
    }

    public function DashboardsGetMoStatsHook(&$controller){
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Dashboards/get_mo_stats');
    }
    
    /*public function Report_UpdateGlobalSheetColumns_Hook(&$generatedGlobalData, &$workSheet, &$currentRow, &$startRow){
        $columParameters = array('theorical_time'=>__('Teorinis gamybos laikas (min)'));
        foreach($columParameters as $paramKey => $paramTitle){
            $currentColumn = 'A';
            $currentRow++;
            $workSheet->setCellValue($currentColumn++ . $currentRow, $paramTitle);
            $total = 0;
            foreach($generatedGlobalData as $singleShift){          
                if(!isset($singleShift[$paramKey])){
                    $currentColumn++; continue;
                }else{
                    $total += $singleShift[$paramKey];
                    $workSheet->setCellValue($currentColumn++ . $currentRow, $singleShift[$paramKey]);
                }
            }
            $workSheet->setCellValue($currentColumn . $currentRow, $total);
            $startRow++;
        }
        $currentColumn = 'A';
    }*/
    
    /*public function Report_UpdateShiftSheetColumns_Hook($shift, &$workSheet, &$currentRow, $sensorId, &$return_arr){
        $return_arr['theorical_time'] = $this->getTheoryTimeOfConfirmedQuantity($shift['Shift']['start'],$shift['Shift']['end'], $sensorId);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Teorinis gamybos laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, $return_arr['theorical_time']);
    }
    
    private function getTheoryTimeOfConfirmedQuantity($start, $end, $sensorId){
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $approvedOrderModel->bindModel(array('belongsTo'=>array('Plan')));
        $confirmedQuantities = $approvedOrderModel->find('all', array(
            'fields'=>array(
                'SUM(ApprovedOrder.confirmed_quantity) AS total_confirmed_quantity',
                'TRIM(Plan.step) AS step',
                'TRIM(Plan.id) AS plan_id',
            ),
            'conditions'=>array(
                'ApprovedOrder.sensor_id'=>$sensorId,
                'ApprovedOrder.confirmed'=>1,
                'ApprovedOrder.confirmed_quantity >'=>0,
                'ApprovedOrder.end >='=>$start,
                'ApprovedOrder.end <'=>$end,
            ),'group'=>array('ApprovedOrder.plan_id')
        ));
        $time = 0;
        foreach($confirmedQuantities as $confirmedQuantity){
            $quantityMade =  isset($confirmedQuantity[0]['total_confirmed_quantity'])?floatval($confirmedQuantity[0]['total_confirmed_quantity']):0;
            $step =  isset($confirmedQuantity[0]['step'])?floatval($confirmedQuantity[0]['step']):0;
            $time += $step > 0 ? ($quantityMade * 3600 / $step) : 0;
        }
        return round($time / 60);
    }*/
    
    //deklaruotas kiekis yra paciame darbo centre paspaudus mygtuka "Deklaruoti kieki" i forma ivestas kiekis
    //Patvirtintas kiekis yra per "Uzsakymu statusas" Meniu redaguojamas konkretus approvedOrder ir ten ivestas kiekis. Po ivedimo uzsakymas igauna statusa: patvirtinta  (confirmed=1)
    public function Oeeshell_filterGetFactTimeRecords_Hook(&$approvedOrdersData, $currentShift, &$records, &$returnData){
        if($returnData) return; //jei OEE skaiciuojamas realiu laiku, perskaciuoti kiekiu nereikia
        //nuresetinamas teorinio laiko paskaiciavimas, kadangi jis apsakiciuojamas pagal formule veliau
        foreach($records['sensors'] as $sensorId => &$recordData){
            $recordData['theory_prod_time'] = 0;
        }
        foreach($approvedOrdersData as &$approvedOrderData){
            $approvedOrderData[0]['sensor_quantity'] = $approvedOrderData[0]['total_quantity'];
            if(!$approvedOrderData['Record']['approved_order_id']) continue;
            $approvedOrderData[0]['total_quantity'] = $this->getOrderCountInShift($approvedOrderData['Record']['approved_order_id'], $currentShift['Shift'], $approvedOrderData['Sensor']['type'], $approvedOrderData['Plan']['id']);
            $approvedOrderData[0]['theory_prod_time'] = $this->getTheoryTime($approvedOrderData[0]['total_quantity'], $approvedOrderData['Plan']['step']);
        }
    }

    public function Oeeshell_beforeSearchGetFactTime_Hook(&$additional_fields, &$recordBindModels){
        $recordBindModels['belongsTo']['SuperParentProblem'] = array('className'=>'Problem', 'foreignKey'=>false, 'conditions'=>array('SuperParentProblem.id = FoundProblem.super_parent_problem_id'));
        $additional_fields[] = 'SUM(IF(POSITION(LOWER("non planned stop") IN SuperParentProblem.name) > 0, ' . Configure::read('recordsCycle') . ', 0)) AS non_planned_stops_duration';
        $additional_fields[] = 'SUM(IF(POSITION(LOWER("]planned stop") IN SuperParentProblem.name) > 0, ' . Configure::read('recordsCycle') . ', 0)) AS planned_stops_duration';
        $additional_fields[] = 'GROUP_CONCAT(DISTINCT CASE WHEN POSITION(LOWER("non planned stop") IN SuperParentProblem.name) > 0 THEN FoundProblem.id END ) AS non_planned_stops_quantity';
        $additional_fields[] = 'GROUP_CONCAT(DISTINCT CASE WHEN POSITION(LOWER("]planned stop") IN SuperParentProblem.name) > 0 THEN FoundProblem.id END ) AS planned_stops_quantity';
    }

    public function Oeeshell_appendGetFactTimeLoop_Hook(&$records, &$val, $sensor_id){
        //return false;
        if(!isset($records['sensors'][$sensor_id]['sensor_quantity'])){
            $records['sensors'][$sensor_id]['sensor_quantity'] = 0;
            $records['sensors'][$sensor_id]['non_planned_stops_duration'] = 0;
            $records['sensors'][$sensor_id]['non_planned_stops_quantity'] = 0;
            $records['sensors'][$sensor_id]['planned_stops_duration'] = 0;
            $records['sensors'][$sensor_id]['planned_stops_quantity'] = 0;
        }
        $records['sensors'][$sensor_id]['non_planned_stops_duration'] += isset($val[0]['non_planned_stops_duration'])?$val[0]['non_planned_stops_duration']:0;
        $records['sensors'][$sensor_id]['planned_stops_duration'] += isset($val[0]['planned_stops_duration'])?$val[0]['planned_stops_duration']:0;
        $records['sensors'][$sensor_id]['non_planned_stops_quantity'] += isset($val[0]['non_planned_stops_quantity'])? sizeof(explode(',',$val[0]['non_planned_stops_quantity'])):0;
        $records['sensors'][$sensor_id]['planned_stops_quantity'] += isset($val[0]['planned_stops_quantity'])? sizeof(explode(',',$val[0]['planned_stops_quantity'])):0;
        $records['sensors'][$sensor_id]['sensor_quantity'] += isset($val[0]['sensor_quantity'])?$val[0]['sensor_quantity']:0;
    }
    
    //grazinima teorine plano trukme
    public function getTheoryTime($count, $planStep){
        if($planStep > 0){
            return $count * 60 / $planStep;
        }else return 0;
    }
    
    //grazinamas atitinkamo uzsakymo pagamintas kiekis nurodytoje pamainoje. jei uzsakymo jutiklis yra maisymo tipo, papildomai kiekis perskaiciuojamas per formule
    /*public function getOrderCountInShift($orderId, $shiftId, $sensorType, $planId){
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $planModel = ClassRegistry::init('Plan');
        $approvedOrderModel->bindModel(array('hasMany'=>array('PartialQuantity')));
        $approvedOrder = $approvedOrderModel->findById($orderId);
        if(empty($approvedOrder)) return 0;          
        $count = 0;
        if($approvedOrder['ApprovedOrder']['confirmation_shift_id'] != $shiftId){ //jei uzsakymas nebuvo patvirtintas esamoje pamainoje, naudojame tos pamainos deklaruota kieki
            //ieskome ar esamoje pamainoje buvo deklaruotu kiekiu
            if(isset($approvedOrder['PartialQuantity'])){
                $currentShiftDeclarations = array_filter($approvedOrder['PartialQuantity'], function($decl)use($shiftId){ return $decl['shift_id'] == $shiftId; });
                $count = !empty($currentShiftDeclarations)?current($currentShiftDeclarations)['quantity']:0;   
            }else return 0; //jei uzsakymas neturi deklaruoti kiekiu apskritai, graziname 0
        }else{
            if(isset($approvedOrder['PartialQuantity'])){
                $previosShiftDeclarations = array_filter($approvedOrder['PartialQuantity'], function($decl)use($shiftId){ return $decl['shift_id'] != $shiftId; });
                $previosShiftDeclarationsCount = !empty($previosShiftDeclarations)?current($previosShiftDeclarations)['quantity']:0;
            }else{ $previosShiftDeclarationsCount = 0; }
            $count = $approvedOrder['ApprovedOrder']['confirmed_quantity'] - $previosShiftDeclarationsCount;
        }            
        //jei sensorius yra maisymo tipo (patvirtinti kiekiai gaunami kubilais), galutini kieki verciame per papildoma formule
        if($sensorType == Sensor::TYPE_MIXING){
            $plan = $planModel->findById($planId);
            if($plan && $plan['Plan']['qty_1'] > 0){
                $count = round($count * $plan['Plan']['yield'] / $plan['Plan']['qty_1']);
            }
        }
        return $count;
    }*/
    
    //grazinamas atitinkamo uzsakymo pagamintas kiekis nurodytoje pamainoje. jei uzsakymo jutiklis yra maisymo tipo, papildomai kiekis perskaiciuojamas per formule
    public function getOrderCountInShift($orderId, $shift, $sensorType, $planId){
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $planModel = ClassRegistry::init('Plan');
        $shiftModel = ClassRegistry::init('Shift');
        $approvedOrderModel->bindModel(array('hasMany'=>array('PartialQuantity')));
        $shiftId = $shift['id'];
        $approvedOrder = $approvedOrderModel->findById($orderId);
        if(empty($approvedOrder)) return 0;          
        $count = 0;
        $partialQuantitiesInCurrentShift = array_filter($approvedOrder['PartialQuantity'], function($partial)use($shiftId){
            return $partial['shift_id'] == $shiftId;
        });
        $shiftOrderEnd = null;
        if($approvedOrder['ApprovedOrder']['confirmed']){
            $shiftOrderEnd = $shiftModel->find('first', array('conditions'=>array(
                'Shift.start <='=>$approvedOrder['ApprovedOrder']['end'],
                'Shift.end >'=>$approvedOrder['ApprovedOrder']['end'],
                'Shift.branch_id'=>$shift['branch_id'],
            )));
        }
        if(!$approvedOrder['ApprovedOrder']['confirmed'] || !$shiftOrderEnd || $shiftOrderEnd['Shift']['id'] != $shiftId){
            $count = !empty($partialQuantitiesInCurrentShift)?Set::extract('/quantity', $partialQuantitiesInCurrentShift):0;
            return is_array($count) && !empty($count)?array_sum($count):$count;
        }else{
            $partialQuantitiesInOtherShifts = array_filter($approvedOrder['PartialQuantity'], function($partial)use($shiftId){
                return $partial['shift_id'] != $shiftId;
            });
            $countByPartial = !empty($partialQuantitiesInOtherShifts)?Set::extract('/quantity', $partialQuantitiesInOtherShifts):0;
            $countByPartial = is_array($countByPartial) && !empty($countByPartial)?array_sum($countByPartial):$countByPartial;
            $count = bcsub($approvedOrder['ApprovedOrder']['confirmed_quantity'], $countByPartial, 2);
        }
        
        //jei sensorius yra maisymo tipo (patvirtinti kiekiai gaunami kubilais), galutini kieki verciame per papildoma formule
        if($sensorType == Sensor::TYPE_MIXING){
            $plan = $planModel->findById($planId);
            if($plan && $plan['Plan']['qty_1'] > 0){
                $count = round($count * $plan['Plan']['yield'] / $plan['Plan']['qty_1']);
            }
        }          
        return $count;
    }
    
    //per sia vieta pakeiciame kiekius kiekvienam uzsakymui atskirai
    public function Oeeshell_filterOrderStatsRecords_Hook(&$approvedOrderData,$currentShift){
        $records['sensors'] = array();
        $approvedOrdersData = array(array(
            'Record'=>array('approved_order_id'=>$approvedOrderData['approved_order_id']),
            'Plan'=>array('id'=>$approvedOrderData['plan_id'], 'step'=>1000),
            'Sensor'=>array('type'=>$approvedOrderData['sensor_type'])
        ));
        $this->Oeeshell_filterGetFactTimeRecords_Hook($approvedOrdersData, $currentShift, $records);
        $approvedOrderData['total_quantity'] = isset($approvedOrdersData[0][0]['total_quantity'])?$approvedOrdersData[0][0]['total_quantity']:$approvedOrderData['total_quantity'];
        if(isset($approvedOrdersData[0][0]['theory_prod_time'])){
            $approvedOrderData['theory_prod_time'] = $approvedOrdersData[0][0]['theory_prod_time'];
        }
    }

    public function ReportsContr_BeforeGenerateXLS_Hook(&$reportModel){
        $reportModel = ClassRegistry::init('Leibur.LeiburReport');
    }

    public function ReportsContr_BeforeCalculatePeriodXLS_Hook(&$reportModel, &$templateTitle){
        $templateTitle = '/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Reports/calculate_period_xls';
    }

    public function Oeeshell_afterSaveOrderData_Hook(&$saveData, &$order, &$approvedOrder){
        return false;
        $saveData['confirmed_quantity'] = $order[0]['total_quantity'];
        $saveData['quantity'] = $order[0]['sensor_quantity'];
    }
    
    public function ApprovedOrders_Edit_AfterSave_Hook(&$approvedOrdersController, &$item, &$lossItems){
        $exporterModel = ClassRegistry::init($this->name.'.Exporter');
        $partialQuantityModel = ClassRegistry::init('PartialQuantity');
        if($item['ApprovedOrder']['status_id'] == 4 || $item['ApprovedOrder']['status_id'] == 6) {
            $approvedOrdersController->Log->write(__('1. Patvirtintas atidetas arba dalinai uzbaigtas užsakymas su duomenimis').': '.json_encode($item));
            $exporterModel->exportCompleted($item);
        } else {
//            if($item['Sensor']['type'] != 1) {
//                if($item['Sensor']['type'] == 3) {
//                    $related_export_item = $approvedOrdersController->ApprovedOrder->withRefs()->find('first',array(
//                        'conditions'=>array(
//                            'Plan.mo_number'=>$item['Plan']['mo_number'],
//                            'Sensor.type'=>1,
//                            'ApprovedOrder.confirmed'=>true
//                        )
//                    ));
//                    if($related_export_item != null && !empty($related_export_item)) {
//                        $approvedOrdersController->Log->write(__('2. Patvirtintas užsakymas su duomenimis').': '.json_encode($item));
//                        $exporterModel->exportCompleted($related_export_item);
//                    }
//                }
                $partialQuantity = $partialQuantityModel->find('first', array(
                    'fields'=>array('SUM(PartialQuantity.quantity) AS quantity'),
                    'conditions'=>array('PartialQuantity.approved_order_id' => $item['ApprovedOrder']['id'])
                ));
                if(!empty($partialQuantity) && $partialQuantity[0]['quantity'] > 0 && $item['Sensor']['type'] == 3){
                    $item['ApprovedOrder']['confirmed_quantity'] = max(1, $item['ApprovedOrder']['confirmed_quantity'] - $partialQuantity[0]['quantity']);
                }
                $exporterModel->exportCompleted($item);
            //}
        }
        $hasLoss = false;
        $losses = array();
        foreach ($lossItems as $loss) {
            $approvedOrdersController->Log->write('Loss Found : ' .$loss['LossType']['code'] ." amount: " . $loss['Loss']['value']);
            if(!array_key_exists($loss['LossType']['code'],$losses)) {
                $losses[$loss['LossType']['code']] = 0;
            }
            $losses[$loss['LossType']['code']] += $loss['Loss']['value'];
        }
        $approvedOrdersController->Log->write("Total losses: " . json_encode($losses).";");
        $index = 0;
        foreach($losses as $key=>$loss) {
            $exporterModel->exportLosses($item,$key,$loss,$index);
            $index++;
        }
    }

    public function calculateHoursInfo(&$dcHoursInfoBlocks, $hours, $fSensor){
        //Planuotas kiekis (vnt.) ir Pagamintas kiekis (vnt.)
        if(empty($hours)){return;}
        $recordModel = ClassRegistry::init('Record');
        $founProblemModel = ClassRegistry::init('FoundProblem');
        $hoursToWorkOn = Set::extract('/value', $hours);
        $recordModel->bindModel(array('belongsTo'=>array('Plan','FoundProblem')));
        $lastFullHour = array_reverse($hours);
        $firstDate = current($hours)['date'].' '.sprintf('%01d',current($hours)['value']).':00';
        $lastDate = current($lastFullHour)['date'].' '.sprintf('%01d',current($lastFullHour)['value']).':00';
        if(strtotime('+1 hour',strtotime($lastDate)) <= time()){
            $lastDate = date('Y-m-d H:i', strtotime('+1 hour',strtotime($lastDate)));
        }
        $quantityField = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        $planCountData = $recordModel->find('all',array(
            'fields'=>array(
                'SUM(IF((Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL) OR (FoundProblem.problem_id >= 3),' . (Configure::read('recordsCycle')) . ',0)) AS work_problem_time',
                'SUM(IF(FoundProblem.problem_id = 1,' . (Configure::read('recordsCycle')) . ',0)) AS fact_transition_time',
                'GROUP_CONCAT(DISTINCT(IF(FoundProblem.problem_id = 1, FoundProblem.id, 0))) AS transition_problem_id',
                'MAX(IF(FoundProblem.problem_id = 1, Record.created, \'0000-00-00\')) AS transition_end',
                //'MAX(Record.created) AS hour_date',
                
                //'SUM('.Configure::read('recordsCycle').') AS work_problem_time',
                'SUM(Record.'.$quantityField.') AS fact_quantity',
                'IF(Record.approved_order_id > 0,Plan.step / 60,0) AS step',
                'Record.approved_order_id',
                'Plan.preparation_time',
                'DATE_FORMAT(Record.created, \'%H\') AS hour'
            ),'conditions'=>array(
                'Record.created >='=>$firstDate,
                'Record.created <'=>$lastDate,
                'Record.sensor_id'=>$fSensor['Sensor']['id']
            ),'group'=>array(
                'DATE_FORMAT(Record.created, \'%Y-%m-%d %H\')',
                'Record.approved_order_id'
            )
        ));
        $plannedHoursQuantity = $factHoursQuantity = array();
        foreach($planCountData as $data){
            if(!$data['Record']['approved_order_id']){ continue; }
            $currentHour = (int)$data[0]['hour'];             
            $transitionOverflow = 0;
            $transitionsProblemId = array_filter(explode(',', $data[0]['transition_problem_id']));          
            if(!empty($transitionsProblemId)){
                $transitionProblem = $founProblemModel->findById(current($transitionsProblemId)); //imame pirma nari, nes vienas uzsakymas gali tureti tik viena derinima               
                if(!empty($transitionProblem)){
                    $factTransitionDuration = strtotime($data[0]['transition_end']) - strtotime($transitionProblem['FoundProblem']['start']);
                    $transitionOverflow = max(0, $factTransitionDuration - $data['Plan']['preparation_time']*60);
                    if($transitionOverflow > $data[0]['fact_transition_time']){
                        $transitionOverflow = min($data[0]['fact_transition_time'], $transitionOverflow);
                    }                
                }   
            }          
            if(!isset($plannedHoursQuantity[$currentHour])){ $plannedHoursQuantity[$currentHour] = 0; }
            if(!isset($factHoursQuantity[$currentHour])){ $factHoursQuantity[$currentHour] = 0; }
            $plannedHoursQuantity[$currentHour] += bcmul(bcdiv($data[0]['work_problem_time']+$transitionOverflow,60,6), $data[0]['step'], 6);
            $factHoursQuantity[$currentHour] += round($data[0]['fact_quantity']);           
        }
        array_walk($plannedHoursQuantity, function(&$data){ $data = round($data); });
        $dcHoursInfoBlocks['plan_count']['value'] = $plannedHoursQuantity;
        $dcHoursInfoBlocks['fact_count']['value'] = $factHoursQuantity;
        
        //Pamainos atsilikimas nuo plano (min.)
        $delayTimes = $this->getHourFactFromPlanDelayTimes($fSensor, strtotime($firstDate), strtotime($lastDate), 'shift');
        $dcHoursInfoBlocks['delay_in_shift']['value'] = $delayTimes;
        $delayTimes = $this->getHourFactFromPlanDelayTimes($fSensor, strtotime($firstDate), strtotime($lastDate), 'day');
        $dcHoursInfoBlocks['delay_in_day']['value'] = $delayTimes;
    }

    private function getHourFactFromPlanDelayTimes($fSensor, $currentHour, $lastHour, $startPointType){
        static $shiftModel=null,$planModel=null,$recordModel=null;
        $shiftModel = $shiftModel===null?ClassRegistry::init('Shift'):$shiftModel;
        $planModel = $planModel===null?ClassRegistry::init('Plan'):$planModel;
        $recordModel = $recordModel===null?ClassRegistry::init('Record'):$recordModel;
        $hoursDelayList = array();
        $quantityField = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        while($currentHour < $lastHour){
            $currentHour += 3600;
            $currentShift = $shiftModel->find('first',array('conditions'=>array('Shift.branch_id'=>$fSensor['Sensor']['branch_id'], 'Shift.start <='=>date('Y-m-d H:i:s', $currentHour-60), 'Shift.end >'=>date('Y-m-d H:i:s', $currentHour-60))));
            if(!$currentShift) continue;
            if($startPointType == 'shift'){//skaiciuojama satsilikimas nuo pamainos, kuri yra esamoje valandoje
                $startPoint = $currentShift['Shift']['start'];
            }else{//skaiciuojama satsilikimas nuo paros, kuri yra esamoje valandoje
                //jei esamos pamainos pradzia yra vienoje dienoje, o pabaiga kitoje, vadinasi esame naktineje pamainoje, todel para prasideda praejuios dienos pirmoje pamainoje
                $dayStart = date('d',strtotime($currentShift['Shift']['start'])) != date('d',strtotime($currentShift['Shift']['end']))?date('Y-m-d',strtotime($currentShift['Shift']['start'])) : date('Y-m-d',strtotime($currentShift['Shift']['end']));
                $dayStartShift = $shiftModel->find('first',array('conditions'=>array('Shift.branch_id'=>$fSensor['Sensor']['branch_id'], 'Shift.start >='=>$dayStart)));
                $startPoint = $dayStartShift['Shift']['start'];
            }
            $plans = $planModel->find('all', array(
                'conditions'=>array(
                    'Plan.start <='=>date('Y-m-d H:i:s', $currentHour),
                    'Plan.end >='=>$startPoint,
                    'Plan.sensor_id'=>$fSensor['Sensor']['id']
                )
            ));
            if(empty($plans)){ continue; }
            $recordModel->bindModel(array('belongsTo'=>array('Plan')));
            $factProduction = $recordModel->find('all', array(
                'fields'=>array(
                    'SUM(Record.'.$quantityField.') AS quantity',
                    'TRIM(Record.plan_id) AS plan_id',
                    'TRIM(Plan.step) AS step',
                ),'conditions'=>array(
                    'Record.sensor_id'=>$fSensor['Sensor']['id'],
                    'Record.created >='=>$startPoint,
                    'Record.created <'=>date('Y-m-d H:i:s', $currentHour)
                ),'group'=>array('Record.plan_id')
            ));
            $plansIdsList = Set::extract('/Plan/id', $plans);
            $factsMadeNotByPlan = array_filter($factProduction, function($data)use($plansIdsList){ return !in_array($data[0]['plan_id'], $plansIdsList); });
            $timeDelay = 0;
            foreach($factsMadeNotByPlan as $factMadeNotByPlan){
                if(!floatval($factMadeNotByPlan[0]['step']))continue;
                $timeDelay = bcadd($timeDelay, bcdiv($factMadeNotByPlan[0]['quantity'],bcdiv($factMadeNotByPlan[0]['step'],60,6),6), 6);
            }
            foreach($plans as $plan){
                $step = floatval(bcdiv($plan['Plan']['step'],60,6));//kiek vnt pagamina per minute
                $planStart = max(strtotime($currentShift['Shift']['start']), strtotime($plan['Plan']['start']));
                $planEnd = min($currentHour, strtotime($plan['Plan']['end']));
                $factData = array_filter($factProduction, function($data)use($plan){ return $plan['Plan']['id'] == $data[0]['plan_id']; });
                $factCount = !empty($factData)?current($factData)[0]['quantity']:0;
                $planCount = $step>0?bcmul(bcdiv(($planEnd - $planStart),60,6), $step, 6):0;
                //sumuojame bendra kiekvieno plano fakta nuo teorijos neatitikima minutemis
                $timeDelay = $step>0?bcadd($timeDelay, bcdiv(bcsub(min($factCount, $planCount), $planCount, 6), $step, 6), 6):0;
            }
            $hoursDelayList[(int)date('H', $currentHour-3600)] = round($timeDelay);
        }
        return $hoursDelayList;
    }

    public function addQuantitiesToVirtualSensors($savedSensorsQty,$date,$activePlans){
        static $recordModel = null, $db = null, $foundProblemModel = null;
        if($recordModel == null){ $recordModel = ClassRegistry::init('Record'); }
        if($foundProblemModel == null){ $foundProblemModel = ClassRegistry::init('FoundProblem'); }
        if($db == null){ $db = ConnectionManager::getDataSource('default'); }
        $virtualSensors = array();
        if(sizeof(array_intersect(array_keys($savedSensorsQty), array(6,7,8)))==3){
            if(isset($savedSensorsQty[31]) && $savedSensorsQty[31] > 10){
                $virtualSensors[4] = 0;
            }else{
                $virtualSensors[4] = $savedSensorsQty[6] + $savedSensorsQty[7] + $savedSensorsQty[8];
            }
        }
        if(sizeof(array_intersect(array_keys($savedSensorsQty), array(10,11,12)))==3){
            $virtualSensors[9] = $savedSensorsQty[10] + $savedSensorsQty[11] + $savedSensorsQty[12];
        }
        if(sizeof(array_intersect(array_keys($savedSensorsQty), array(20,21)))==2){
            $virtualSensors[19] = $savedSensorsQty[20] + $savedSensorsQty[21];
        }
        if(sizeof(array_intersect(array_keys($savedSensorsQty), array(23,24)))==2){
            $virtualSensors[22] = $savedSensorsQty[23] + $savedSensorsQty[24];
        }
        foreach($virtualSensors as $sensorId => $quantity){
            $db->query('CALL ADD_FULL_RECORD(\''.$date.'\', '.$sensorId.', '.$quantity.', '.Configure::read('recordsCycle').'); ');  
        }
        $virtualSensorId = 13;
        if(isset($activePlans[$virtualSensorId]) && sizeof(array_intersect(array_keys($savedSensorsQty), array(14,15)))==2){
            $markerPath = __dir__.'/../webroot/files/virtual_sensors_update_'.$virtualSensorId.'.txt';
            if(!file_exists($markerPath) || time() - filemtime($markerPath) >  3600){
                $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
                fwrite($fh, 1);
                fclose($fh);
                $lastRecord = $recordModel->find('first', array('conditions'=>array('Record.sensor_id'=>$virtualSensorId),'order'=>array('Record.id DESC')));
                $lastRecordDate = !empty($lastRecord)?$lastRecord['Record']['created']:'0000-00-00';
                $newRecords = $recordModel->find('all', array(
                    'fields'=>array(
                        'TRIM(Record.quantity) AS quantity',
                        'TRIM(Record.created) AS created'
                    ),'conditions'=>array(
                        'Record.created >'=>$lastRecordDate,
                        'Record.sensor_id'=>strtolower(trim($activePlans[$virtualSensorId]['crate_type'])) == 'kst'?14:15,
                    ),'order'=>array('Record.id')
                ));
                $lastFoundProblem = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId), 'order'=>array('FoundProblem.id DESC')));
                foreach($newRecords as $newRecord){ $newRecord = $newRecord[0];
                    if(!empty($lastFoundProblem) && strtotime($newRecord['created']) < strtotime($lastFoundProblem['FoundProblem']['start'])){//jei puciami senesni recordai nei sukurta aktyvi prastova
                        $foundProblemModel->updateAll(array('FoundProblem.state'=>FoundProblem::STATE_COMPLETE), array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS));
                        $activeProblemInPast = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.start <'=>$newRecord['created']), 'order'=>array('FoundProblem.id DESC')));
                        if(!empty($activeProblemInPast)){
                            $foundProblemModel->updateAll(array('FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS), array('FoundProblem.id'=>$activeProblemInPast['FoundProblem']['id']));
                        }
                        $lastFoundProblem = array();
                    }
                    $db->query('CALL ADD_FULL_RECORD(\''.$newRecord['created'].'\', '.$virtualSensorId.', '.$newRecord['quantity'].', '.Configure::read('recordsCycle').'); '); 
                }
                unlink($markerPath);
            }
        }
        $virtualSensorId = 25;
        if(isset($activePlans[$virtualSensorId]) && sizeof(array_intersect(array_keys($savedSensorsQty), array(26,29,27,30)))==4){
            $markerPath = __dir__.'/../webroot/files/virtual_sensors_update_'.$virtualSensorId.'.txt';
            if(!file_exists($markerPath) || time() - filemtime($markerPath) >  3600){
                $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
                fwrite($fh, 1);
                fclose($fh);
                $lastRecord = $recordModel->find('first', array('conditions'=>array('Record.sensor_id'=>$virtualSensorId),'order'=>array('Record.id DESC')));
                $lastRecordDate = !empty($lastRecord)?$lastRecord['Record']['created']:'0000-00-00';
                $newRecords = $recordModel->find('all', array(
                    'fields'=>array(
                        'SUM(Record.quantity) AS quantity',
                        'TRIM(Record.created) AS created'
                    ),'conditions'=>array(
                        'Record.created >'=>$lastRecordDate,
                        'Record.sensor_id'=>strtolower(trim($activePlans[$virtualSensorId]['crate_type'])) == 'kst'?array(26,29):array(27,30),
                    ),'group'=>array('Record.created')    
                ));
                $lastFoundProblem = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId), 'order'=>array('FoundProblem.id DESC')));
                foreach($newRecords as $newRecord){ $newRecord = $newRecord[0];
                    if(!empty($lastFoundProblem) && strtotime($newRecord['created']) < strtotime($lastFoundProblem['FoundProblem']['start'])){//jei puciami senesni recordai nei sukurta aktyvi prastova
                        //uzdarome visas aktyvias prastovas ir surandame ta prastova, kuri yra veliausia is prastovu, turinciu mazesni sukurimo laika nei esamas iraso laikas. Sia prastova darysime aktyvia
                        $foundProblemModel->updateAll(array('FoundProblem.state'=>FoundProblem::STATE_COMPLETE), array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS));
                        $activeProblemInPast = $foundProblemModel->find('first', array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.start <'=>$newRecord['created']), 'order'=>array('FoundProblem.id DESC')));
                        if(!empty($activeProblemInPast)){
                            $foundProblemModel->updateAll(array('FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS), array('FoundProblem.id'=>$activeProblemInPast['FoundProblem']['id']));
                        }
                        $lastFoundProblem = array();
                    }
                    $db->query('CALL ADD_FULL_RECORD(\''.$newRecord['created'].'\', '.$virtualSensorId.', '.$newRecord['quantity'].', '.Configure::read('recordsCycle').'); '); 
                }
                unlink($markerPath);
            }
        }
    }

    public function ApprovedOrders_Index_BeforeSearch_Hook(&$searchParameters, &$request, &$ApprovedOrderModel){
        $userModel = ClassRegistry::init('User');
        $userliveData = $userModel->findById(Configure::read('user')->id);
        if(Configure::read('user')->sensor_id > 0){
            $searchParameters['conditions']['ApprovedOrder.confirmed'] = 0;
            $searchParameters['conditions']['ApprovedOrder.status_id'] = ApprovedOrder::STATE_COMPLETE;
            $searchParameters['conditions']['ApprovedOrder.sensor_id'] = $userliveData['User']['active_sensor_id'] > 0?$userliveData['User']['active_sensor_id']:Configure::read('user')->sensor_id;
        }
    }
    
    public function Workcenter_beforeReturnUserSensor_Hook(&$fSensor){
        //Jei siuo metu atidarytas vienas sis id:13 arba id:25 virtualiu sensoriu ir jam bandoma uzdeti plana irasu irasymo metu, reikia stabdyti bet kokia funkcija, keiciant jutikli i neegzsituoajnti
        if(in_array($fSensor['Sensor']['id'], self::$virtualSensors)){
            $markerPath = __dir__.sprintf('/../webroot/files/virtual_sensors_update_%d.txt', $fSensor['Sensor']['id']);
            if(file_exists($markerPath)){
                $fSensor['Sensor']['id'] = 0;
            }
        }
    }
    
    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Leibur/css/Leibur_bare.css?v=1';
    }

	public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
		$settingsModel = ClassRegistry::init('Settings');
		$testDowntimeId = $settingsModel->getOne('test_production_downtime_id');
		if(!$testDowntimeId){ return null; }
		$planModel = ClassRegistry::init('Plan');
		$planModel->bindModel(array('belongsTo'=>array('Product')));
		$plan = $planModel->findById($appOrder['ApprovedOrder']['plan_id']);
		$planName = isset($plan['Product']['name']) && trim($plan['Product']['name'])?$plan['Product']['name']:$plan['Plan']['production_name'];
		if(!preg_match('/^test.+/i',trim($planName))){ return null; }
		$foundProblemModel = ClassRegistry::init('FoundProblem');
		$logModel = ClassRegistry::init('Log');
		$shiftModel = ClassRegistry::init('Shift');
		$recordModel = ClassRegistry::init('Record');
		$sensorModel = ClassRegistry::init('Sensor');	
		$foundProblemModel->deleteAll(array('FoundProblem.end >'=>$appOrder['ApprovedOrder']['created'], 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
		$fShift = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
		$start = new DateTime($appOrder['ApprovedOrder']['created']);
		$quantity = $recordModel->find('first', array('fields'=>array('SUM(Record.quantity) AS quantity_sum'), 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$start->format('Y-m-d H:i:s'))));
		$currentProblem = $foundProblemModel->newItem($start, new DateTime(),
                            $testDowntimeId, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $plan[Plan::NAME]['id'], array(), $quantity[0]['quantity_sum']);
		$recordModel->updateAll(
			array('Record.found_problem_id'=>$currentProblem['FoundProblem']['id']), 
			array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$currentProblem['FoundProblem']['start'])
		);
		$sensorModel->updateAll(
			array('Sensor.tmp_prod_starts_on'=>$fSensor['Sensor']['prod_starts_on'], 'Sensor.prod_starts_on'=>9999),
			array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
		);
	}

	public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
		if($fSensor['Sensor']['prod_starts_on'] >= 9999){
			$sensorModel = ClassRegistry::init('Sensor');
			$sensorModel->updateAll(
				array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
				array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
			);
		}
	}

	public function Messages_beforeCheckProjectAlive_Hook(&$projectList){
        $projectList = array(
            'HanzasMaiznica'=>'http://172.30.8.151/messages/get_latest_record',
            'DuonaVilnius'=>'http://10.131.145.25/messages/get_latest_record',
            'DuonaPanevezys'=>'http://172.30.6.97/messages/get_latest_record'
        );
    }

    public function FoundProblem_beforeManipulateCycleStart_Hook(&$fSensor,&$fFoundProblem){
        static $foundProblemModel, $approvedOrderModel, $planModel, $sensorModel, $productionMovementTimeModel, $logModel = null;
        if($foundProblemModel == null){
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $planModel = ClassRegistry::init('Plan');
            $foundProblemModel = ClassRegistry::init('FoundProblem');
            $sensorModel = ClassRegistry::init('Sensor');
            $productionMovementTimeModel = ClassRegistry::init('ProductionMovementTime');
            $logModel = ClassRegistry::init('Log');
        }
        if($fSensor['Sensor']['type'] == Sensor::TYPE_PACKING && !empty($fFoundProblem) && $fFoundProblem['FoundProblem']['problem_id'] == Problem::ID_NOT_DEFINED && isset($fSensor['ApprovedOrder']['id']) && $fSensor['ApprovedOrder']['id'] > 0){ //pakavimas ir vyksta nepazymeta prastova vykdant gamyba
            $problemDuration = strtotime($fFoundProblem['FoundProblem']['end']) - strtotime($fFoundProblem['FoundProblem']['start']);
            if($problemDuration >= $fSensor['Sensor']['records_count_to_start_problem'] * Configure::read('recordsCycle')){
                $sameLineSensorsIds = $sensorModel->getRelatedSensors($fSensor['Sensor']['id'],null,true);
                if(!empty($sameLineSensorsIds)) {
                    $foundProblemModel->bindModel(array('belongsTo' => array('Sensor','Plan')));
                    $productionMovementTime = $productionMovementTimeModel->find('first', array(
                        'conditions'=>array('ProductionMovementTime.production_code'=>$fSensor['Plan']['production_code'])
                    ));
                    if(!empty($productionMovementTime)) {
                        $productionMovementTime = $productionMovementTime['ProductionMovementTime']['time'] * 60;
                        $timeBack = strtotime($fFoundProblem['FoundProblem']['start']) - $productionMovementTime;
                        $queryParams = array(
                            'fields' => array(
                                'ABS(TIMESTAMPDIFF(SECOND, \'' . date('Y-m-d H:i:s', $timeBack) . '\', FoundProblem.start)) AS deviation',
                                'FoundProblem.id',
                                'FoundProblem.problem_id',
                                'FoundProblem.super_parent_problem_id',
                            ), 'conditions' => array(
                                'Sensor.type' => Sensor::TYPE_MIXING,
                                'Sensor.id' => $sameLineSensorsIds,
                                'Plan.paste_code' => $fSensor['Plan']['paste_code'],
                                sprintf('FoundProblem.start BETWEEN \'%s\' AND \'%s\'', date('Y-m-d H:i:s', $timeBack - 120), date('Y-m-d H:i:s', $timeBack + 120)),
                                'FoundProblem.problem_id <>' => Problem::ID_NOT_DEFINED
                            ), 'order' => array(
                                'ABS(TIMESTAMPDIFF(SECOND, \'' . date('Y-m-d H:i:s', $timeBack) . '\', FoundProblem.start))'
                            )
                        );
                        $lineStartProblem = $foundProblemModel->find('first', $queryParams);
                        if(!empty($lineStartProblem)){
                            $logModel->write('Pakavimo darbo centro prastova automatiskai keiciama i tos pacios linijos pradzios jutiklio prastova. Keiciama prastova: '.json_encode($fFoundProblem).' Linijos pradzios prastova: '.json_encode($lineStartProblem).' Vykdyta uzklausa su parametrais: '.json_encode($queryParams), $fSensor);
                            $foundProblemModel->updateAll(
                                array('problem_id'=>$lineStartProblem['FoundProblem']['problem_id'], 'super_parent_problem_id'=>$lineStartProblem['FoundProblem']['super_parent_problem_id']),
                                array('FoundProblem.id'=>$fFoundProblem['FoundProblem']['id'])
                            );
                        }
                    }
                }
            }
        }
        $planDiffAction = explode('_', trim($fSensor['Sensor']['duona_plan_difference_action']??''));
        if(sizeof($planDiffAction) == 3){//prastovos ID, prastovos atšokimo trukmę minutėmis ir minimalų laiko tarpą tarp planų minutėmis, pvz.: 123_60_300
            list($problemId, $timeJumpDuration, $durationBetweenPlans) = $planDiffAction;
            $approvedOrderModel->bindModel(array('belongsTo'=>'Plan'));
            $lastApprovedOrder = $approvedOrderModel->find('first', array(
                'conditions'=>array('ApprovedOrder.sensor_id'=>$fSensor['Sensor']['id']),
                'order'=>array('ApprovedOrder.created DESC')
            ));
            $problemWasAlreadyCreated = $foundProblemModel->find('first', array(
                'conditions'=>array('FoundProblem.plan_id'=>$lastApprovedOrder['Plan']['id'], 'FoundProblem.problem_id'=>$problemId)
            ));
            if($lastApprovedOrder && !empty($lastApprovedOrder['Plan']) && empty($problemWasAlreadyCreated)) {
                $nextOrderByPlan = $planModel->find('first', array('conditions' => array('Plan.start >=' =>$lastApprovedOrder['Plan']['end'])));
                if($nextOrderByPlan){
                    $timeDiffBetweenPlans = strtotime($nextOrderByPlan['Plan']['start']) - strtotime($lastApprovedOrder['Plan']['end']);
                    if($timeDiffBetweenPlans > $durationBetweenPlans*60){//Jei skirtumas tarp planu yra didesnis nei nurodytas minimalus laiko tarpas tarp planų jutiklio nustatymuose
                        //turi būti iš antrojo plano pradžios (plans.start) atimama nustatyme nurodyta prastovos atšokimo reikšmė.
                        //Jei apskaičiuota reikšmė yra mažesnė už dabartinį laiko momentą, sistema turi užbaigti prieš tai vykusią prastovą ir pradėti naują, nurodyto tipo, prastovą.
                        if(strtotime($nextOrderByPlan['Plan']['start']) - $timeJumpDuration*60 < time()){
                            if(!empty($fFoundProblem)){//Jei tuo metu kai turės būti pradėta prastova automatiškai jutiklis fiksuos gamybą, prastova nebus pradėta (gamybos metu $currentProblem = null)
                                $time = new DateTime();
                                $foundProblemModel->findAndComplete($fSensor, $time);
                                $time = $time->add(new DateInterval('PT1S'));
                                $shift = $this->Shift->findCurrent($fSensor['Sensor']['branch_id']);
                                $newFoundProblem = $foundProblemModel->newItem($time, $time, $problemId, $fSensor['Sensor']['id'], $shift['Shift']['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $lastApprovedOrder['Plan']['id']);
                                $logModel->write('Uzdaroma esama problema, kadangi tarp esamo ir busimo plano gaunamas per didelis laiko tarpas (pagal jutiklio nustatyma duona_plan_difference_action). Uzdaroma esama prastova '.json_encode($fFoundProblem).' Sukurta nauja prastova: '.json_encode($newFoundProblem), $fSensor);
                                $logModel->write('Esamas planas '.json_encode($lastApprovedOrder).' Busimas planas: '.json_encode($nextOrderByPlan), $fSensor);
                            }
                        }
                    }
                }
            }
        }
    }

    public function View_EditSensorLeftBlock_Hook(&$form){
        echo $form->input('duona_plan_difference_action', array('label' => __('Pamainos pradėjimas. Nurodykite prastovos ID, prastovos atšokimo trukmę minutėmis ir minimalų laiko tarpą tarp planų minutėmis, pvz.: 123_60_300'), 'type'=>'text','class' => 'form-control', 'div' => array('class' => 'form-group')));
    }
}