<?php
App::uses('ApprovedOrder', 'Model');
class Exporter extends LeiburAppModel{

    private static function getExportPath($createIfNotExists = false) {
        $settingsModel = ClassRegistry::init('Settings');
        $path = $settingsModel->getOne('plan_export_path');
        if(!trim($path)){
            $path = APP.DS.'external'.DS.'out';
        }
        if ($createIfNotExists && !is_dir($path)){
            $old = umask(0);
            mkdir($path, 0755);
            umask($old);
        }
        return $path;
    }

    public static function exportCompleted($ord){
        $xmlTags = array();
        $sensorModel = ClassRegistry::init('Sensor');
        $sensorModel->bindModel(array('belongsTo'=>array('Line')));
        $sensor = $sensorModel->findById($ord['Sensor']['id']);
        $rootTagName = 'product';
        $xmlTags['facility'] = 100;
        $xmlTags['finished'] = $ord['ApprovedOrder']['status_id'] == ApprovedOrder::STATE_COMPLETE?1:0;
        $xmlTags['finish_date'] = date('Y-m-d',strtotime($ord['ApprovedOrder']['end']));
        $xmlTags['finish_time'] = date('H:i:s',strtotime($ord['ApprovedOrder']['end']));
        $xmlTags['production_line'] = $sensor['Line']['ps_id']??'';
        $xmlTags['work_center'] = $ord['Sensor']['name'];
        $xmlTags['unique_ID'] = $ord['Plan']['uniqueid']??'';
        if($ord['Sensor']['type'] == 1) {
            $rootTagName = 'dough';
            $xmlTags['item'] = $ord['Plan']['paste_code'];
            $xmlTags['amount'] = bcmul($ord['ApprovedOrder']['confirmed_quantity'], $ord['Plan']['yield'], 2);
            $xmlTags['unit'] = 'kg';
        }elseif($ord['Sensor']['type'] == 3){
            $xmlTags['product_number'] = $ord['Plan']['production_code'];
            $xmlTags['mo_number'] = $ord['Plan']['mo_number'];
            $xmlTags['report_no'] = $ord['Plan']['report_no'];
            $xmlTags['amount'] = $ord['ApprovedOrder']['confirmed_quantity'];
            $xmlTags['unit'] = 'pcs';
        }
        $xmlString = '<?xml version="1.0" encoding="UTF-8"?><OEEExport><'.$rootTagName.'>';
        foreach($xmlTags as $xmlTag => $tagValue){
            $xmlString .= sprintf('<%s>%s</%s>', $xmlTag, $tagValue, $xmlTag);
        }
        $xmlString .= '</'.$rootTagName.'></OEEExport>';
        $date = new DateTime();
        $sensorType = $ord['Sensor']['type'] == 1?'T':'P';
        $lineName = $sensor['Line']['name']??'';
        $fileName = $lineName.'_'.$sensorType.'_'.$ord['Sensor']['id'].'_'.$ord['Plan']['mo_number'].'_'.$date->format('Ymd').'_'.$date->format('His').'.xml';
        $filePath = self::getExportPath(true).DS.'Orders'.DS.$fileName;
        file_put_contents($filePath, $xmlString);
    }

    public static function exportLosses($ord,$loss_code,$loss_value,$addSeconds) {
        $date = new DateTime();
        if($addSeconds != 0) {
            $date->add(new DateInterval('PT'.$addSeconds.'S'));
        }
        $fileName = 'ORDLESS_'.$ord[Plan::NAME]['mo_number'].'_'.$date->format('Ymd').'_'.$date->format('His').'.txt';
        $filePath = self::getExportPath(true).DS.'Losses'.DS.$fileName;
        $data = ''
            .$ord[Branch::NAME]['ps_id'].';'
            .$loss_code.';' // broko kodas
            .number_format(floatval($loss_value), 2, '.', '').';' // broko kiekis
            .$date->format('Ymd').';'."\r\n";
        file_put_contents($filePath, $data);
    }

}