DROP PROCEDURE IF EXISTS HOOK_AFTER_RECORD_INSERT;
DROP FUNCTION IF EXISTS GET_UNIT_QUANTITY;
DROP FUNCTION IF EXISTS GET_APPROVED_ORDER_BOX_QUANTITY;

DELIMITER $$

CREATE  FUNCTION `GET_UNIT_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS DECIMAL(12,4) NO SQL
BEGIN
	DECLARE `sensor_type` INT(2) DEFAULT NULL;
	DECLARE `multiplier` DECIMAL(12,4) DEFAULT 1;
	DECLARE `line_industrial` TINYINT(1) DEFAULT 0;
	DECLARE `qty_pack` MEDIUMINT(8) DEFAULT 1;
	SELECT `s`.`type` INTO `sensor_type` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `l`.`industrial` INTO `line_industrial` FROM `line_sensors` AS `ls` LEFT JOIN `lines` AS `l` ON (`ls`.`line_id` = `l`.`id`) WHERE `ls`.`sensor_id`=`sensor_id` LIMIT 1;
	IF(`sensor_type` = 3 AND `line_industrial` = 1 AND `sensor_id` NOT IN (3)) THEN
		SET `multiplier` = `box_quantity`;
	ELSEIF(`sensor_id` IN (2,4,9)) THEN
		SELECT `p`.`qty_pack` INTO `qty_pack` FROM `plans` AS `p` WHERE `p`.`id`=`active_plan_id` LIMIT 1;
		SET `qty_pack` = IF(`qty_pack`=0 OR `qty_pack` IS NULL, 1, `qty_pack`);
		SET `multiplier` = 1 / `qty_pack`;
	END IF;
RETURN `quantity` * `multiplier`;
END$$

CREATE  FUNCTION `GET_APPROVED_ORDER_BOX_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS DECIMAL(12,4) NO SQL
BEGIN
	DECLARE `qty_pack` MEDIUMINT(8) DEFAULT 1;
	DECLARE `boxes` DECIMAL(12,4) DEFAULT 1;
	SET `boxes` = `quantity` / `box_quantity`;
	IF(`sensor_id` IN (2,4,9)) THEN
		SELECT `p`.`qty_pack` INTO `qty_pack` FROM `plans` AS `p` WHERE `p`.`id`=`active_plan_id` LIMIT 1;
		SET `qty_pack` = IF(`qty_pack`=0 OR `qty_pack` IS NULL, 1, `qty_pack`);
		SET `boxes` = `quantity` * `qty_pack`;
	END IF;
RETURN `boxes`;
END$$

CREATE PROCEDURE `HOOK_AFTER_RECORD_INSERT`(IN `date_time` DATETIME, IN `sensor_id` INT(11),  IN `branch_id` INT(11), IN `quantity` DECIMAL(12,4), IN `shift_id` INT(11), IN `active_fproblem_id` INT(11), IN `active_order_id` INT(11), IN `active_plan_id` INT(11), IN `record_cycle` SMALLINT(6) ) NO SQL
BEGIN
	DECLARE `shift_start` DATETIME DEFAULT NULL;
	DECLARE `prev_shift_id` INT(11) DEFAULT NULL;
	DECLARE `prev_shift_duration` INT(11) DEFAULT NULL;
	DECLARE `not_defined_downtimes_duration_in_prev_shift` INT(11) DEFAULT NULL;
	#veiksmai, kai keiciasi pamaina (nuo pamainos pradzios nepraejo ilgesnis laikas kaip iraso ciklas)
	#Jei praejusios pamainos nepazymeta prastova bendra trukme yra daugiau kaip 90proc pamainos laiko, visas tas prastovas reikia paversti i nera darbo
	SELECT `sh`.`start` INTO `shift_start` FROM `shifts` AS `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	IF(UNIX_TIMESTAMP(`date_time`) - UNIX_TIMESTAMP(`shift_start`) <= `record_cycle`) THEN 
		SELECT `shifts`.`id`, TIMESTAMPDIFF(SECOND, `shifts`.`start`, `shifts`.`end`) INTO `prev_shift_id`,`prev_shift_duration` FROM `shifts` WHERE `shifts`.`start` < `shift_start`  AND `shifts`.`branch_id` = `branch_id` ORDER BY `shifts`.`id` DESC LIMIT 1;
		SELECT SUM(TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`)) INTO `not_defined_downtimes_duration_in_prev_shift` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`shift_id` = `prev_shift_id` AND `fp`.`problem_id` = 3 LIMIT 1;
		IF(`not_defined_downtimes_duration_in_prev_shift` / `prev_shift_duration` * 100 > 90) THEN
			UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = 2 WHERE `fp`.`shift_id` = `prev_shift_id` AND `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = 3;
			CALL `ADD_LOG`(`sensor_id`, CONCAT('Keiciantis pamainai pamainos id: ',  `shift_id`,' nepazymetos prastovos automatiskai verciamos i nera darbo, kadangi bendra ju suma viršyja 90% pamainos laiko. Prastovu suam lygi (s):', `not_defined_downtimes_duration_in_prev_shift`));			
		END IF;
	END IF;
#Automatinis uzsakymo uzdarymas ir atidarymas
#	DECLARE `sensor_plan_relate_quantity` VARCHAR(50) DEFAULT NULL;
#	DECLARE `sensor_leibur_plan_difference` MEDIUMINT(8) DEFAULT 0;
#	DECLARE `compare_quantity` DECIMAL(12,4) DEFAULT 0;
#	DECLARE `plan_quantity` DECIMAL(12,4) DEFAULT 0;
#	DECLARE `seconds_till_next_plan` INT(11) DEFAULT NULL;
#	DECLARE `next_plan_id` INT(11) DEFAULT NULL;
#	DECLARE `branch_id` INT(11) DEFAULT NULL;
#	DECLARE `active_fproblem_duration` INT(11) DEFAULT NULL;
#	DECLARE `ao_quantity` DECIMAL(12,4) DEFAULT NULL;
#	DECLARE `ao_box_quantity` DECIMAL(12,4) DEFAULT NULL;
#	IF(false AND `active_plan_id` > 0 AND `sensor_id` NOT IN(2,3,4,13,16,18,19,25,31)) THEN 
#		SELECT `s`.`plan_relate_quantity`, `s`.`leibur_plan_difference`*60, `s`.`branch_id` INTO `sensor_plan_relate_quantity`,`sensor_leibur_plan_difference`,`branch_id` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
#		SELECT `p`.`quantity` INTO `plan_quantity` FROM `plans` AS `p` WHERE `p`.`id`=`active_plan_id` LIMIT 1;
#		SELECT TIMESTAMPDIFF(SECOND,NOW(),`p`.`start`), `p`.`id` INTO `seconds_till_next_plan`, `next_plan_id` FROM `plans` AS `p` LEFT JOIN `approved_orders` AS `ao` ON (`p`.`id` = `ao`.`plan_id`) WHERE `p`.`sensor_id`=`sensor_id` AND `p`.`disabled` = 0 AND `ao`.id IS NULL ORDER BY `p`.`start` LIMIT 1;
#		SELECT `ao`.`quantity`,`ao`.`box_quantity` INTO `ao_quantity`,`ao_box_quantity`	FROM `approved_orders` AS `ao` WHERE `ao`.`id` = `active_order_id` LIMIT 1;
#		SET `compare_quantity` = IF(`sensor_plan_relate_quantity`='unit_quantity', `ao_quantity`, `ao_box_quantity`);
#		IF(`compare_quantity` >= `plan_quantity` AND `seconds_till_next_plan` IS NOT NULL AND `seconds_till_next_plan` <= `sensor_leibur_plan_difference`) THEN #UZDAROME ESAMA IR ATIDAROME SEKANTI PLANA 
#			UPDATE `approved_orders` AS `ao` SET `ao`.`status_id` = 2 WHERE `ao`.`id` = `active_order_id`;
#			UPDATE `found_problems` AS `fp` SET `fp`.`state` = 2 WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1;
#			SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) + 1), '%Y-%m-%d %H:%i:%s');
#			INSERT INTO `approved_orders` (`id`, `created`, `sensor_id`, `plan_id`, `status_id`, `end`, `quantity`, `box_quantity`, `confirmed_quantity`, `confirmed`, `swap_time`, `exported_quantity`, `additional_data`, `confirmation_shift_id`, `prev_shift_no_quantities`) VALUES 
#					(NULL, @adddate, `sensor_id`, `next_plan_id`, 1, @adddate, '0.0000', '0.0000', '0.00', '0', NULL, '0', NULL, 0, 0);
#			INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`) VALUES (
#					NULL, @adddate, @adddate, 1, 0, 0, `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL);
#		ELSEIF(`compare_quantity` >= `plan_quantity` AND (`seconds_till_next_plan` IS NULL OR `seconds_till_next_plan` > `sensor_leibur_plan_difference`)) THEN #UZDAROME ESAMA PLANA TIK KAI ATSIRANDA NEPAZYMETA PRASTOVA, TRUNKANTI ILGIAU KAIP 80S 
#			SELECT TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`) INTO `active_fproblem_duration` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 AND `fp`.`problem_id` > 0 LIMIT 1;
#			IF(`active_fproblem_duration` >= 79) THEN
#				UPDATE `approved_orders` AS `ao` SET `ao`.`status_id` = 2 WHERE `ao`.`id` = `active_order_id`;
#				UPDATE `found_problems` AS `fp` SET `fp`.`state` = 2 WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1;
#			END IF;
#		END IF;
#	END IF;
END$$

DELIMITER ;
