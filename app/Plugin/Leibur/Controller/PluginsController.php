<?php
App::uses('LeiburAppController', 'Leibur.Controller');
App::uses('ApprovedOrder', 'Model');
class PluginsController extends LeiburAppController {
    
    public $uses = array('Shift','Record','Sensor','DashboardsCalculation','ApprovedOrder','Settings','Leibur.Leibur');



    public function index(){
        $this->layout = 'ajax';
        $schema = $this->DashboardsCalculation->schema();
        if(!isset($schema['sensor_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `sensor_quantity` DECIMAL(20,8) NOT NULL COMMENT 'sensoriaus uzfiksuotas kiekis' AFTER `total_quantity`;");
        }
        if(!isset($schema['planned_stops_duration'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `planned_stops_duration` INT(11) NOT NULL COMMENT 'planuotu sustojimu trukme sekundemis' AFTER `total_quantity`;");
        }
        if(!isset($schema['planned_stops_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `planned_stops_quantity` INT(11) NOT NULL COMMENT 'planuotu sustojimu kiekis' AFTER `total_quantity`;");
        }
        if(!isset($schema['non_planned_stops_duration'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `non_planned_stops_duration` INT(11) NOT NULL COMMENT 'neplanuotu sustojimu trukme sekundemis' AFTER `total_quantity`;");
        }
        if(!isset($schema['non_planned_stops_quantity'])){
            $this->DashboardsCalculation->query("ALTER TABLE `dashboards_calculations` ADD `non_planned_stops_quantity` INT(11) NOT NULL COMMENT 'neplanuotu sustojimu kiekis' AFTER `total_quantity`;");
        }
        $schema = $this->ApprovedOrder->schema();
        if(!isset($schema['confirmation_shift_id'])){
            $this->DashboardsCalculation->query("ALTER TABLE `approved_orders` ADD `confirmation_shift_id` INT NOT NULL COMMENT 'Pamainos id, kurioje uzsakymas buvo patvirtintas' AFTER `additional_data`;");
        }
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Leibur.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

    /*function save_sensors_data($port = ''){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start'.$port.'.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 1800){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
        $activePlans = $this->ApprovedOrder->find('all',array('fields'=>array('Plan.*','ApprovedOrder.sensor_id'),'conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS),'group'=>array('ApprovedOrder.sensor_id')));
		$activePlans = Hash::combine($activePlans, '{n}.ApprovedOrder.sensor_id', '{n}.Plan');
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            'HUB01R2' => array(
                'http://10.131.33.144:5580/data',
                'http://10.131.33.145:5580/data',
            ),'HUB02R4'=>array(
            	'http://10.131.33.145:5580/data',
            	'http://10.131.33.148:5580/data'
            ),'HUB03R5'=>array(
            	'http://10.131.33.146:5580/data',
            	'http://10.131.33.149:5580/data',
            ),
        );
		if(trim($port)){
            $adressList = array_filter($adressList, function($addressPort)use($port){
                return $addressPort == $port;
            },ARRAY_FILTER_USE_KEY);
        }
        $mailOfErrors = array();
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->executeFailedQueries();
        foreach($adressList as $box => $adressesInBox){
            $dataPass = false;
            foreach($adressesInBox as $adress){
                if($dataPass){ break; } //jei praejo pirmas adresas, antro neskaitome
                $ch = curl_init($adress);
                curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                    'Authorization: Basic ' . base64_encode("$username:$password")
                ));
                $data = curl_exec($ch);
                $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                if(curl_errno($ch) == 0 AND $http == 200) {
                    $data = json_decode($data);
                    $dataPass = true;
                }else{
                    $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                    continue;
                }
                foreach($data->periods as $record){
                    $savedSensorsQty = array();
                    foreach($saveSensors as $sensorId => $title){
                        list($sensorName, $sensorPort) = explode('_',$title);
                        if($box != $sensorPort) continue;
                        if(!isset($record->$sensorName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
                        $quantity = $record->$sensorName;
                        $savedSensorsQty[$sensorId] = $quantity;
                        try{
                            if(Configure::read('ADD_FULL_RECORD')){
                                $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                            }else{
                                $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                            }
                            $db->query($query);
                        }catch(Exception $e){
                            $this->Sensor->logFailedQuery($query);
                            $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                        }
                    }
                    $this->Leibur->addQuantitiesToVirtualSensors($savedSensorsQty,date('Y-m-d H:i:s', $record->time_to),$activePlans);
                }
            }
        }
        unlink($dataPushStartMarkerPath);
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->Sensor->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) >  60){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        $this->Sensor->moveOldRecords();
        session_destroy();
        die();
    }*/

	
	/** @requireAuth Leibur: redaguoti kiekius užsakymo tvirtinimo metu  */
	public function edit_approved_order_quantities(){
		die();//reikia del teisiu tvirtinant uzsakymu kiekius
	}

    /*private function checkRecordsIsPushingTalinas($message = ''){
    	pr($message);
        $settings = $this->Settings->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList))return;
        $minutes = explode(':', $settings);
        $minutes = isset($minutes[1]) && is_numeric(trim($minutes[1]))?floatval(trim($minutes[1])):0;
        if(!$minutes) $minutes = 60;
        $markerPath = __dir__.'/../webroot/files/last_push_check2.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > $minutes*60){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, 1);
            fclose($fh);
            $headers = "Content-Type: text/plain; charset=UTF-8";
            mail(implode(',',$emailsList),__("Įrašų siuntimo problema ").Configure::read('companyTitle'),implode(";\n",$message),$headers);
        }
    }*/
    
    // public function confirm_order($orderId=0){
        // if(empty($this->request->data) || !isset($this->request->data['ApprovedOrder']['sensor_id'])){
            // $this->redirect('/');
        // }
        // $item = $this->ApprovedOrder->withRefs()->findById($orderId);
        // $redirectUrl = array('controller'=>'approvedOrders', 'action'=>'index', 'sensorId'=>$this->request->data['ApprovedOrder']['sensor_id']);
        // if(empty($item)){ $this->redirect($redirectUrl); }
        // $piecesCount = $this->request->data['ApprovedOrder']['boxes'] * $item['Plan']['box_quantity'] + 
    // }
	
}
