<div class="row">
    <div class="col-md-2">
        <?php echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control',
            )
        ));
        echo __("Ieškoti pagal mo");
        echo $this->Form->input('mo', array(
            'label' => __('Įveskite mo numerį'),
        ));
        echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
    <div class="col-md-3">
        <?php
        echo $this->Form->create('MoStat', array(
            'class'         => 'form form-horizontal',
            'role'          => 'form',
            'inputDefaults' => array(
                'class' => 'form-control datepicker',
            )
        ));
        echo __('Ieškoti pagal datą');
        ?>
        <div class="row">
            <div class="col-md-6">
                <?php echo $this->Form->input('start_date', array('label' => __('Pradžia'))); ?>
            </div>
            <div class="col-md-6">
                <?php echo $this->Form->input('end_date', array('label' => __('Pabaiga'))); ?>
            </div>

        </div>

        <?php echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary pull-right'));
        echo $this->Form->end();
        ?>
    </div>
</div>
<div class="mo-picker">
    <?php if(isset($mo_list)) echo $this->element('mo_picker'); ?>
</div>


<script>
    jQuery("document").ready(function () {
        jQuery(".datepicker").datetimepicker({
            dateFormat: "yy-mm-dd"
        });
        if(jQuery.uniform)
            jQuery('input:checkbox, input:radio, .uniform-file').uniform();


        jQuery("div.mo-picker .date-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");

            var checker =  $(this).closest("li");
//            console.log(checker.find("ul").find(".checker"));
            var input = checker.find("ul").find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
        jQuery("div.mo-picker .line-toggle").click(function() {
            var isChecked = $(this).parent("span").hasClass("checked");
            var checker =  $(this).closest('span.mo-line').next();
            console.log($(this));
            console.log(checker);
            var input = checker.find(".checker span input");
            if(isChecked) {
                input.prop("checked",true);
            } else {
                input.prop("checked",false);
            }
            jQuery.uniform.update(input);
        });
    });

</script>
<style>.ui-state-active {background-color:#ccc;}</style>
<?php echo $this->Html->script('ofc-scripts/jquery-ui-timepicker-addon'); ?>

<?php if (!empty($mo_number))
{
    $i = 0;?>
    <br/>
    <div><b><?php echo __("Rezultatų kiekis") . ": " . count($mo_number); ?></b></div>
    <table class="table table-bordered mo-stats">

        <?php foreach ($recordsFormatted as $mo_number => $record)
        {
            if ($i != 0)
            { ?>
                <tr>
                    <td>&nbsp;</td>
                </tr>

            <?php
            }
            $i ++;
            $data = $record['data'];
            ?>
            <tr class="mo-name">
                <td>&nbsp;</td>
                <td><?php echo $mo_number; ?></td>
                <td colspan="3"><?php echo $data['name'] ?></td>
                <td colspan="2"><?php echo __("Pradžia") . " " . $data['start'] ?></td>
                <td colspan="2"><?php echo __("Pabaiga") . " " . $data['end'] ?></td>
            </tr>

            <tr class="mo-header">
                <th>&nbsp;</th>
                <th><?php echo __('Supakuotas kiekis, vnt'); ?></th>
                <th><?php echo __('Kiekis prieš raikymą, vnt'); ?></th>
                <th><?php echo __('Kiekis prieš kepimą, vnt'); ?></th>
                <th><?php echo __('Patvirtintas kiekis, vnt'); ?></th>

                <th><?php echo __('Praradimai dėl kepimo, vnt'); ?></th>
                <th><?php echo __('Praradimai dėl raikymo, vnt'); ?></th>
                <th><?php echo __('Visas praradimas, vnt'); ?></th>


                <th class="mo-border-left"><?php echo __('NK deklaruotas brokas visas (kg)'); ?></th>

                <?php foreach ($loss_types as $loss_type)
                { ?>
                    <th><?php echo $loss_type['loss_name'] ?></th>
                <?php } ?>

                <th>Step</th>

                <th class="mo-green mo-border-left"><?php echo __('Gamybos laikas'); ?></th>

                <?php foreach ($problems as $key => $problem)
                {
                    $problem_id = $problem['problem_id'];
                    ?>
                    <th class="<?php echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?>"><?php echo $problem['problem_name'] ?></th>
                <?php } ?>

                <th class="mo-border-left">OEE</th>
                <th><?php echo __('Prieinamumo koeficientas'); ?></th>
                <th><?php echo __('Efektyvumo koeficientas'); ?></th>
<!--                <th>Kokybės koeficientas</th>-->

                <th><?php echo __('Pradžia'); ?></th>
                <th><?php echo __('Pabaiga'); ?></th>

            </tr>

            <tr class="mo-combined">
                <td>&nbsp;</td>
                <td><?php if (isset($data['sum_supakuotas'])) echo $data['sum_supakuotas'] ?></td>
                <td><?php if (isset($data['sum_pries_raikyma'])) echo $data['sum_pries_raikyma'] ?></td>
                <td><?php if (isset($data['sum_pries_kepima'])) echo $data['sum_pries_kepima'] ?></td>
                <td>&nbsp;</td>

                <td <?php if (isset($data['sum_pries_kepima']) && isset($data['praradimai_del_kepimo'])) echo ($data['praradimai_del_kepimo'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_del_kepimo'])) echo $data['praradimai_del_kepimo'] ?></td>

                <td <?php if (isset($data['praradimai_del_raikymo'])) echo ($data['praradimai_del_raikymo'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_del_raikymo'])) echo $data['praradimai_del_raikymo'] ?></td>
                <td <?php if (isset($data['praradimai_visas'])) echo ($data['praradimai_visas'] < 0) ? "class='mo-red'" : "" ?>><?php if (isset($data['praradimai_visas'])) echo $data['praradimai_visas'] ?></td>


                <td class="mo-border-left"><?php echo $data['sum_consequences'] ?></td>

                <?php foreach ($loss_types as $loss_type)
                {
                    $loss_type_id = $loss_type['loss_type_id'];
                    ?>
                    <td><?php if (isset($data['losses'][$loss_type_id])) echo number_format($data['losses'][$loss_type_id], 3); else echo "0.000"; ?></td>
                <?php } ?>

                <td class="mo-border-left">&nbsp;</td>
                <td class="mo-border-left">&nbsp;</td>

                <?php foreach ($problems as $key => $problem)
                { ?>
                    <td>&nbsp;</td>
                <?php } ?>

                <td class="mo-border-left">&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
<!--                <td>&nbsp;</td>-->

                <td>&nbsp;</td>
                <td>&nbsp;</td>

            </tr>


            <?php foreach ($record['sensors'] as $sensor)
            {       
                foreach($sensor as $id=>$approvedOrder) {
                ?>
            <tr>
                <td><?php echo $approvedOrder['ps_id'] ?></td>
                <td><?php if ($approvedOrder['type'] == 3) echo $approvedOrder['sum_quantity'] ?></td>
                <td><?php if ($approvedOrder['type'] == 2) echo $approvedOrder['sum_quantity'] ?></td>
                <td><?php if ($approvedOrder['type'] == 1) echo $approvedOrder['sum_quantity'] ?></td>

                <td><?php if (isset($approvedOrder['confirmed_quantity'])) echo $approvedOrder['confirmed_quantity'] ?></td>

                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>


                <td class="mo-border-left"><?php echo $approvedOrder['sum_consequences'] ?></td>

                <?php foreach ($loss_types as $loss_type)
                {
                    $loss_type_id = $loss_type['loss_type_id'];
                    ?>
                    <td><?php if (isset($approvedOrder['losses'][$loss_type_id])) echo number_format($approvedOrder['losses'][$loss_type_id]['loss_value'],3); ?></td>
                <?php } ?>

                <td>
                    <?php
                    if(array_key_exists($approvedOrder['id'],$plans_stepsFormatted[$mo_number])) {
                        echo $plans_stepsFormatted[$mo_number][$approvedOrder['id']];
                    }?></td>

                <td class="mo-green mo-border-left"><?php echo $this->App->changeUnixSecondsToHumanTime($approvedOrder['sum_gamybos_laikas'], true) ?></td>
<!--                <td class="mo-green mo-border-left">--><?php //echo $approvedOrder['sum_gamybos_laikas']; ?><!--</td>-->

                <?php foreach ($problems as $key => $problem)
                {
                    $problem_id = $problem['problem_id'];
                    ?>
<!--                    <th class="--><?php //echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?><!--">--><?php //if (isset($approvedOrder['found_problems'][$problem_id])) echo $approvedOrder['found_problems'][$problem_id]['problem_length']; ?><!--</th>-->
                    <th class="<?php echo ($problem_id == 1) ? "mo-yellow" : (($problem_id > 2) ? "mo-red" : "") ?>"><?php if (isset($approvedOrder['found_problems'][$problem_id])) echo $this->App->changeUnixSecondsToHumanTime($approvedOrder['found_problems'][$problem_id]['problem_length'], true); ?></th>
                <?php } ?>

                <td class="mo-border-left"><?php if (isset($approvedOrder['oee']['oee'])) echo $approvedOrder['oee']['oee'] * 100?></td>
                <td><?php if (isset($approvedOrder['oee']['avail'])) echo $approvedOrder['oee']['avail'] * 100?></td>
                <td><?php if (isset($approvedOrder['oee']['perf'])) echo $approvedOrder['oee']['perf'] * 100?></td>
<!--                <td>--><?php //if (isset($approvedOrder['oee']['quality'])) echo $approvedOrder['oee']['quality']?><!--</td>-->

                <td><?php echo $this->app->changeToNonBreakingSpaces($approvedOrder['jutiklio_gamybos_pradzia']); ?></td>
                <td><?php echo $this->app->changeToNonBreakingSpaces($approvedOrder['jutiklio_gamybos_pabaiga']); ?></td>

            </tr>

        <?php } }?>


        <?php } ?>

    </table>

<?php } ?>