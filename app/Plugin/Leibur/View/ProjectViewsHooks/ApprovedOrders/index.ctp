<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas patvirtintas užsąkymas'); ?></button>
<br /><br /> */ ?>

<?php if(!$forced_sensorId) {?>
<div class="clearfix">
	<div class="btn-group pull-right">
		<button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Darbo centras') ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
		<ul class="dropdown-menu">
			<?php foreach ($sensorOptions as $lid => $li): ?>
			<li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
			<?php endforeach; ?>			
		</ul>
	</div>
</div>
<br/>
<?php }else{ ?>
<div class="clearfix">
    <div class="btn-group pull-right">
        <button id="backToWc" class="btn btn-info" href=""><?php echo __('Grįžti į darbo centrą'); ?></button>
        <script type="text/javascript">
            jQuery('#backToWc').bind('click', function(){
                window.close();
            });
        </script>
    </div>
</div>
<?php } ?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Data ir laikas'); ?></th>
			<th><?php echo __('Darbo centrai'); ?></th>
			<th><?php echo __('Planas'); ?></th>
			<th><?php echo __('Kiekis'); ?></th>
			<?php if($forced_sensorId == null) { ?><th><?php echo __('Dėžių / kubilų kiekis'); ?></th><?php } ?>
			<?php if($forced_sensorId == null) { ?><th><?php echo __('Patvirtintas'); ?></th><?php } ?>
			<?php if($forced_sensorId > 0) { ?><th><?php echo __('Kiekis patvirtinimui'); ?></th><?php } ?>
			<th><?php echo __('Būsena'); ?></th>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; ?>
		<tr>
			<td><?php echo $li->id; ?></td>
			<td><?php $ds = new DateTime($li->created); echo $ds->format('Y-m-d H:i'); ?></td>
			<td><?php echo Sensor::buildName($li_); ?></td>
			<td><?php echo Plan::buildName($li_); ?></td>
			<?php if($forced_sensorId == null) { ?><td><?php 
    			if($li->confirmed && $li_['Sensor']['type'] == 1){
    			    echo '0';
                }else{
                    echo $li->quantity;
                }
    			?></td>
    			<td>
    			    <?php 
    			    if($li->confirmed && $li_['Sensor']['type'] == 1){
                        echo round($li_[ApprovedOrder::NAME]['confirmed_quantity'],2);
    			    }else{
    			        $bq = $li->confirmed ? ($li_[Plan::NAME]['box_quantity'] ? floor($li->confirmed_quantity / $li_[Plan::NAME]['box_quantity']) : 0) : $li->box_quantity;
                        $iq = ($li->confirmed ? $li->confirmed_quantity : $li->quantity) - ($bq * $li_[Plan::NAME]['box_quantity']);
                        echo $bq;//.(($iq > 0) ? (' ('.$iq.')') : '');
    			    }
    			?></td>
    			<td <?php if(isset($li->prev_shift_no_quantities) && $li->prev_shift_no_quantities){ echo 'style="background: #ffe612;"'; } ?>>
    			    <?php echo ($li->status_id == ApprovedOrder::STATE_COMPLETE || $li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE) ? ($li->confirmed ? __('Taip') : __('Ne')) : '&mdash;'; ?>
    			    <?php if(isset($li->prev_shift_no_quantities) && $li->prev_shift_no_quantities){
                        echo '<br />'.__('Užsakymas buvo gamintas kitoje pamainoje nei buvo patvirtintas, tačiau už tą pamainą nėra deklaruotų kiekių');
                    } ?>
    			</td>
			<?php }else{ ?>
			    <td><?php echo $li_['Plan']['box_quantity']>0?__('%d dėžės (%d vnt.)',(int)bcdiv($li_['Plan']['quantity'],$li_['Plan']['box_quantity']),$li_['Plan']['quantity']):$li_['Plan']['quantity'].' '.__('vnt.'); ?></td>
			<?php } ?>
			<?php if($forced_sensorId > 0){ ?>
			     <td style="min-width: 300px;"><div class="row-fluid"><?php 
			         if($li_['Sensor']['type'] == 3 && !empty($li_['Plan'])){
			             $piecesQuantity = $li_['ApprovedOrder']['partial_quantities_sum'] > 0?$li_['ApprovedOrder']['partial_quantities_sum']:$li_['ApprovedOrder']['quantity'];
                         $boxesCount = $li_['Plan']['box_quantity'] == 0?0:(int)($piecesQuantity / $li_['Plan']['box_quantity']);
                         $piecesCount = $li_['Plan']['box_quantity'] == 0?$piecesQuantity:$piecesQuantity % $li_['Plan']['box_quantity'];
                         echo $this->Form->create('ApprovedOrder', array('url'=>array('controller'=>'approved_orders','action'=>'edit',$li->id)));
                         echo $this->Form->input('confirmed_box_quantity', array('type'=>'text', 'label'=>__('Dėžės'), 'value'=>$boxesCount, 'class'=>'form-control input-default', 'div'=>'col-xs-3'));
                         echo $this->Form->input('confirmed_item_quantity', array('type'=>'text', 'label'=>__('Vienetai'), 'value'=>$piecesCount, 'class'=>'form-control input-default', 'div'=>'col-xs-3'));
                         echo $this->Form->input('sensor_id', array('type'=>'hidden', 'value'=>$li->sensor_id));
                         echo $this->Form->input('id', array('type' => 'hidden', 'value'=>$li->id));
                         echo $this->Form->input(__('Patvirtinti'), array('type'=>'submit', 'class'=>'form-control btn btn-primary input-default', 'label'=>'&nbsp;', 'div'=>'col-xs-6'));
                         echo $this->Form->end();
			         }else{
			             $piecesQuantity = $li_['ApprovedOrder']['partial_quantities_sum'] > 0?$li_['ApprovedOrder']['partial_quantities_sum']:$li_['ApprovedOrder']['box_quantity'];
                         echo $this->Form->create('ApprovedOrder', array('url'=>array('controller'=>'approved_orders','action'=>'edit',$li->id)));
                         echo $this->Form->input('confirmed_box_quantity', array('type'=>'hidden', 'value'=>0));
                         echo $this->Form->input('confirmed_item_quantity', array('type'=>'text', 'label'=>__('Vienetai'), 'value'=>$piecesQuantity, 'class'=>'form-control input-default', 'div'=>'col-xs-5'));
                         echo $this->Form->input('sensor_id', array('type'=>'hidden', 'value'=>$li->sensor_id));
                         echo $this->Form->input('id', array('type' => 'hidden', 'value'=>$li->id));
                         echo $this->Form->input(__('Patvirtinti'), array('type'=>'submit', 'class'=>'form-control btn btn-primary input-default', 'label'=>'&nbsp;', 'div'=>'col-xs-7'));
                         echo $this->Form->end();
			         }
			     ?></div></td>
			<?php } ?>
			<td><?php echo __(Status::buildName($li_)); ?></td>
			<td class="full_width">
				<?php if ($li->status_id == ApprovedOrder::STATE_COMPLETE): ?>
				<a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
				<?php endif; ?>
				<?php if ($li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE): ?>
                    <?php //if(!$li->confirmed): ?>
                        <a class="btn btn-default" href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                    <?php //endif; ?>
                    &nbsp;
                    <?php if($li->status_id != ApprovedOrder::STATE_PART_COMPLETE) : ?>
                        <a class="btn btn-default" href="<?php printf($finishUrl, $li->id); ?>"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;<?php echo __('Pabaigti'); ?></a>
                    <?php endif; ?>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
