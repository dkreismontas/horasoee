<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
App::import('Vendor', 'Fernet');
App::uses('LeiburAppController', 'Leibur.Controller');
App::uses('ApprovedOrder', 'Model');
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Record','Leibur.Leibur','ApprovedOrder');

    function save_sensors_data(){
        ob_start();
        try{
            $adressList = array(
                'HUB01R2' => array(
                    'http://10.131.33.144:5580/data',
                    'http://10.131.33.145:5580/data',
                ),'HUB02R4'=>array(
                    'http://10.131.33.145:5580/data',
                    'http://10.131.33.148:5580/data'
                ),'HUB03R5'=>array(
                    'http://10.131.33.146:5580/data',
                    'http://10.131.33.149:5580/data',
                ),
            );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
            $ip = $port = $uniqueString = '';
            $ipAndPort = $this->args[0]??'';
            if(trim($ipAndPort)){
                $ipAndPort = explode(':', $ipAndPort);
                if(sizeof($ipAndPort) == 2){
                    list($ip, $port) = $ipAndPort;
                    $uniqueString = str_replace('.','_',$ip).'|'.$port;
                }else{
                    $uniqueString = $this->args[0]??'';
                }
            }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
            $mailOfErrors = array();
            $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
            if(file_exists($dataPushStartMarkerPath)){
                if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
                    //$this->Sensor->informAboutProblems();
                    die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
                }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
                    $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
                }
            }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
            $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
            $sensorsConditions = array('Sensor.pin_name <>'=>'');
            if(trim($uniqueString)){
                $sensorsConditions['Sensor.port'] = $uniqueString;
            }
            $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan')));
            $activePlans = $this->ApprovedOrder->find('all',array('fields'=>array('Plan.*','ApprovedOrder.sensor_id'),'conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS),'group'=>array('ApprovedOrder.sensor_id')));
            $activePlans = Hash::combine($activePlans, '{n}.ApprovedOrder.sensor_id', '{n}.Plan');
            $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
            $sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id','port'), 'conditions'=>$sensorsConditions));
            if(trim($uniqueString)){
                $adressList = array_filter($adressList, function($addressPort)use($uniqueString){
                    return $addressPort == $uniqueString;
                },ARRAY_FILTER_USE_KEY);
            }
            $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
            $currentShifts = array();
            foreach($branches as $branchId){
                //jei reikia importuojame pamainas
                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
                if(!$currentShifts[$branchId]){
                    App::uses('CakeRequest', 'Network');
                    App::uses('CakeResponse', 'Network');
                    App::uses('Controller', 'Controller');
                    App::uses('AppController', 'Controller');
                    App::import('Controller', 'Cron');
                    $CronController = new CronController;
                    $CronController->generateShifts();
                    $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
                }
            }
            if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }

            $saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
            $this->Sensor->virtualFields = array();
            $periodsFound = false;
            foreach($adressList as $box => $adressesInBox){
                $dataPass = false;
                foreach($adressesInBox as $address){
                    if($dataPass){ break; } //jei praejo pirmas adresas, antro neskaitome
                    if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
                    $addessIpAndPort = current($ipAndPort);
                    $ch = curl_init($address);
                    curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
                    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                        'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
                    ));
                    $data = curl_exec($ch);
                    //$fernetDecoder = new Fernet\Fernet('FdZhRq5lFLmmHmHAEwKPzl7VuO_3Wnsvfjc3ztNUnOg');
                    $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                    if(curl_errno($ch) == 0 AND $http == 200) {
                        //$data = json_decode($fernetDecoder->decode($data));
                        $data = json_decode($data);
                        $dataPass = true;
                    }else{
                        $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
                        continue;
                    }
                    if(!isset($data->periods)){ continue; }
                    foreach($data->periods as $record){
                        $periodsFound = true;
                        $savedSensorsQty = array();
                        foreach($saveSensors as $sensorId => $title){
                            list($pin, $sensorBox) = explode('_',$title);
                            if($box != $sensorBox) continue;
                            if(!isset($record->$pin)) continue;
                            if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
                            $quantity = $record->$pin;
                            $savedSensorsQty[$sensorId] = $quantity;
                            try{
                                $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                                $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
                                fwrite($fh, $queryLog);
                            }catch(Exception $e){
                                $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
                            }
                        }
                        $this->Leibur->addQuantitiesToVirtualSensors($savedSensorsQty,date('Y-m-d H:i:s', $record->time_to),$activePlans);
                    }
                }
            }
            fclose($fh);
            $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
            $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
            if(!empty($mailOfErrors)){
                $this->Sensor->checkRecordsIsPushing($mailOfErrors);
            }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
                unlink($dataNotSendingMarkerPath);
            }
            //sutvarkome atsiustu irasu duomenis per darbo centra
            $this->Sensor->moveOldRecords();
            $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
            if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
                $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
                fclose($fh);
                $this->FoundProblem->manipulate(array_keys($saveSensors));
                $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
                unlink($markerPath);
            }
        }catch(Exception $e){
            $systemWarnings = $e->getMessage();
        }
        $systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
            pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }
	
	
}