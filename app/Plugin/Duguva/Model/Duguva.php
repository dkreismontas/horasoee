<?php
class Duguva extends AppModel{
     
     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }
     
     public function Oeeshell_skipFixFoundProblemsTimesByRecords_Hook(){
         return true;   //OEE duomenu skaiciavimo metu nepertvarko recordu bei found_problems laiku
     }
     
     //kadangi aktyvi problema gali buti per kelias pamainas, ieksome per rekursija kiekvienos problemos pradzios
     public function getThreadedActiveProblems($problemId, $state, $fProblemId = null){
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $foundProblemModel->bindModel(array('belongsTo'=>array('Sensor')));
        $conditions = array('Sensor.id IS NOT NULL', 'TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end) >= 60');
        if($problemId){ $conditions['FoundProblem.problem_id'] = $problemId; }
        if($state){ $conditions['FoundProblem.state'] = $state; }
        if($fProblemId){ $conditions['FoundProblem.id'] = $fProblemId; }
        $problems = $foundProblemModel->find('all', array(
            'fields'=>array(
                'CONCAT(\'ID: \',Sensor.id,\' \',Sensor.name) AS name', 
                'FoundProblem.start',
                'FoundProblem.prev_shift_problem_id',
                'FoundProblem.id',
            ),'conditions'=>$conditions,
            'order'=>array('FoundProblem.start DESC')
        ));
        foreach($problems as &$problem){
            if($problem['FoundProblem']['prev_shift_problem_id']){
                $parentProblem = $this->getThreadedActiveProblems($problemId, null, $problem['FoundProblem']['prev_shift_problem_id']);
                if(!empty($parentProblem)){
                    $problem = current($parentProblem);
                }
            }
        }
        return $problems;
     }
}
    