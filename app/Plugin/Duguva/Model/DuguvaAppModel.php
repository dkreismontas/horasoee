<?php
App::uses('AppModel', 'Model');

class DuguvaAppModel extends AppModel {
    public static $navigationAdditions = array( );
    
    public static function loadNavigationAdditions() {
        self::$navigationAdditions[] =
            array(
                'name'=>__('Darbo centrai, kuriems baigėsi špūlė'),
                'route'=>Router::url(array('plugin' => strtolower(Configure::read('companyTitle')),'controller' => 'plugins', 'action' => 'show_wc_in_problem')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-th-list',
                'menu_pos'=>3,
                'target'=>'_blank'
            );
        self::$navigationAdditions[] =
            array(
                'name'=>__('Įrenginių bendras OEE'),
                'route'=>Router::url(array('plugin' => strtolower(Configure::read('companyTitle')),'controller' => 'plugins', 'action' => 'show_combined_oee', '0_1_2_3')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-th-list',
                'menu_pos'=>3,
                'target'=>'_blank'
            );
        self::$navigationAdditions[] =
            array(
                'name'=>__('Audimo įrenginių bendras OEE'),
                'route'=>Router::url(array('plugin' => strtolower(Configure::read('companyTitle')),'controller' => 'plugins', 'action' => 'show_combined_oee', '4_5')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-th-list',
                'menu_pos'=>3,
                'target'=>'_blank'
            );
    }
    
}
