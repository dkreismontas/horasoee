<?php
App::import('Vendor', 'Fernet');
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Record','Duguva.Duguva');

    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
                '5580'=>'http://78.60.165.178:5580/data',
                '5680'=>'http://78.60.165.178:5680/data',
                '5780'=>'http://78.60.165.178:5780/data',
                '5880'=>'http://78.60.165.178:5880/data',
                '5980'=>'http://78.60.165.178:5980/data',
                '6080'=>'http://78.60.165.178:6080/data',
                '6180'=>'http://78.60.165.178:6180/data',
                '6280'=>'http://78.60.165.178:6280/data',
                '6380'=>'http://78.60.165.178:6380/data',
                '6480'=>'http://78.60.165.178:6480/data',
                '6580'=>'http://78.60.165.178:6580/data',
	        );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $ipAndPort);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
	        $mailOfErrors = array();
	        $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
	        if(file_exists($dataPushStartMarkerPath)){
	            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
	                //$this->Sensor->informAboutProblems();
	                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
	            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
	                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
	            }
	        }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
	            $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            if(!$currentShifts[$branchId]){
                    App::uses('CakeRequest', 'Network');
                    App::uses('CakeResponse', 'Network');
                    App::uses('Controller', 'Controller');
                    App::uses('AppController', 'Controller');
                    App::import('Controller', 'Cron');
	                $CronController = new CronController;
	                $CronController->generateShifts(false, $branchId);
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
	        if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }

			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'\//', $address);
	            });
	        }
            $periodsFound = false;
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
                //$fernetDecoder = new Fernet\Fernet('FdZhRq5lFLmmHmHAEwKPzl7VuO_3Wnsvfjc3ztNUnOg');
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                //$data = json_decode($fernetDecoder->decode($data));
	                $data = json_decode($data);
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
                //$timeAddConfirmation = array();
	            foreach($data->periods as $record){
                    //$timeAddConfirmation[] = $record->time_to;
                    $periodsFound = true;
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
                        $quantity = $this->getPerimeter($sensorId) * $record->$sensorPinName;
						try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
	                }
	            }
                //$this->sendAddConfirmation($timeAddConfirmation, $address);
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }

    private function getPerimeter($sensorId){
        $sensorsPerimeterList = array(
            376 => 308,377 => 308,378 => 308,379 => 275,380 => 273,381 => 273,382 => 273,383 => 275,384 => 275,385 => 270,386 => 270,387 => 275,388 => 275,389 => 278,390 => 275,391 => 275,
            392 => 280,393 => 280,394 => 280,395 => 135,396 => 135,397 => 135,398 => 275,399 => 275,400 => 275,401 => 272,402 => 272,403 => 272,404 => 272,405 => 275,406 => 272,407 => 278,
            408 => 275,409 => 275,410 => 280,411 => 275,412 => 275,413 => 275,414 => 275,415 => 275,416 => 275,417 => 275,418 => 272,419 => 272,420 => 272,421 => 275,422 => 275,423 => 275,
            424 => 735,425 => 735,426 => 1000,427 => 306,428 => 423,429 => 1,430 => 500,431 => 300,432 => 200,433 => 425,434 => 200,435 => 200,436 => 235,437 => 173,438 => 320,439 => 358,
            440 => 360,441 => 320,442 => 675,443 => 675,444 => 675,445 => 274,446 => 200,447 => 200,448 => 510,449 => 510,450 => 510,451 => 510,452 => 310,453 => 305,454 => 310,455 => 310,
            456 => 305,457 => 310,458 => 305,459 => 310,460 => 315,461 => 305,462 => 305,463 => 155,
        );
        return isset($sensorsPerimeterList[$sensorId])?$sensorsPerimeterList[$sensorId]:1;
    }

    private function sendAddConfirmation($timeAddConfirmation, $adress){
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$adress);
        curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
        curl_setopt($ch,CURLOPT_POSTFIELDS, $timeAddConfirmation);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER, false);
        curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
        curl_setopt($ch,CURLOPT_TIMEOUT, 20);
        $response = curl_exec($ch);
        curl_close ($ch);
    }
	
}