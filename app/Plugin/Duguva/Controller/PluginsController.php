<?php
App::uses('DuguvaAppController', 'Duguva.Controller');
App::uses('FoundProblem', 'Model');

class PluginsController extends DuguvaAppController {
    
    public $uses = array('Shift','Record','Sensor','Duguva.Duguva','FoundProblem');
    
    public function index(){
        die();
        $this->layout = 'ajax';
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', Configure::read('companyTitle').'.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }
    
    /*function save_sensors_data($port = ''){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start_'.$port.'.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 7200){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        // $context = stream_context_create(array (
            // 'http' => array (
                // 'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            // )
        // ));
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
        //$saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://78.60.165.178:5580/data',
            '5680'=>'http://78.60.165.178:5680/data',
            '5780'=>'http://78.60.165.178:5780/data',
            '5880'=>'http://78.60.165.178:5880/data',
            '5980'=>'http://78.60.165.178:5980/data',
            '6080'=>'http://78.60.165.178:6080/data',
            '6180'=>'http://78.60.165.178:6180/data',
            '6280'=>'http://78.60.165.178:6280/data',
            '6380'=>'http://78.60.165.178:6380/data',
            '6480'=>'http://78.60.165.178:6480/data',
            '6580'=>'http://78.60.165.178:6580/data',
            //'6680'=>'http://78.60.165.178:6680/data',
        );
        if(trim($port)){
            $adressList = array_filter($adressList, function($addressPort)use($port){
                return $addressPort == $port;
            },ARRAY_FILTER_USE_KEY);
        }
        $mailOfErrors = array();
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->executeFailedQueries();
        foreach($adressList as $addressPort => $adress){
            //$data = json_decode(file_get_contents($adress, false, $context));
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            $timeAddConfirmation = array();
            foreach($data->periods as $record){
            	$timeAddConfirmation[] = $record->time_to;
                foreach($saveSensors as $sensorId => $title){
                    list($sensorName, $sensorPort) = explode('_',$title);
                    if($addressPort != $sensorPort) continue;
                    if(!isset($record->$sensorName)) continue;
                    if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
                    $quantity = $this->getPerimeter($sensorId) * $record->$sensorName;
                    try{
                        if(Configure::read('ADD_FULL_RECORD')){
                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                        }else{
                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                        }
                        $db->query($query);
                    }catch(Exception $e){
                        $this->Sensor->logFailedQuery($query);
                        $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                    }
                }
            }
			$this->sendAddConfirmation($timeAddConfirmation, $adress);
        }
        unlink($dataPushStartMarkerPath);
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->Sensor->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 3600 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->FoundProblem->deleteAll(array('FoundProblem.start <'=>date('Y-m-d', strtotime('-1 year'))));
            //$this->requestAction('/work-center/update-status?update_all=1');
            $this->Sensor->informAboutProblems();
            $this->requestAction('/work-center/calcOee3');
            //unlink($markerPath);
        }
        $this->Sensor->moveOldRecords(false);
        session_destroy();
        die();
    }

    private function getPerimeter($sensorId){
        $sensorsPerimeterList = array(
            376 => 308,377 => 308,378 => 308,379 => 275,380 => 273,381 => 273,382 => 273,383 => 275,384 => 275,385 => 270,386 => 270,387 => 275,388 => 275,389 => 278,390 => 275,391 => 275,
            392 => 280,393 => 280,394 => 280,395 => 135,396 => 135,397 => 135,398 => 275,399 => 275,400 => 275,401 => 272,402 => 272,403 => 272,404 => 272,405 => 275,406 => 272,407 => 278,
            408 => 275,409 => 275,410 => 280,411 => 275,412 => 275,413 => 275,414 => 275,415 => 275,416 => 275,417 => 275,418 => 272,419 => 272,420 => 272,421 => 275,422 => 275,423 => 275,
            424 => 735,425 => 735,426 => 1000,427 => 306,428 => 423,429 => 1,430 => 500,431 => 300,432 => 200,433 => 425,434 => 200,435 => 200,436 => 235,437 => 173,438 => 320,439 => 358,
            440 => 360,441 => 320,442 => 675,443 => 675,444 => 675,445 => 274,446 => 200,447 => 200,448 => 510,449 => 510,450 => 510,451 => 510,452 => 310,453 => 305,454 => 310,455 => 310,
            456 => 305,457 => 310,458 => 305,459 => 310,460 => 315,461 => 305,462 => 305,463 => 155,
        );
        return isset($sensorsPerimeterList[$sensorId])?$sensorsPerimeterList[$sensorId]:1;
    }

	private function sendAddConfirmation($timeAddConfirmation, $adress){
		$ch = curl_init();
		curl_setopt($ch,CURLOPT_URL,$adress);
		curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
		curl_setopt($ch,CURLOPT_POSTFIELDS, $timeAddConfirmation);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, false);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		$response = curl_exec($ch);
		curl_close ($ch);
	}*/

    public function show_wc_in_problem(){
        $this->layout = 'bare';
        $this->set('bodyClass',array('work-center'));
        if($this->request->is('ajax')){
            $foundProblemModel = ClassRegistry::init('FoundProblem');
            $foundProblemModel->bindModel(array('belongsTo'=>array('Sensor')));
            $sensors = $this->Duguva->getThreadedActiveProblems(array(3,13), FoundProblem::STATE_IN_PROGRESS);
            array_walk($sensors, function(&$sensor){
                $duration = time() - strtotime($sensor['FoundProblem']['start']);
                (int)$h = sprintf('%02d',$duration / 3600);
                (int)$m = sprintf('%02d',$duration % 3600 / 60);
                (int)$s = sprintf('%02d',$duration % 3600 % 60);
                $sensor['FoundProblem']['duration'] = sprintf('%s:%s:%s',$h,$m,$s);
            });
            echo json_encode(compact('sensors'));
            die();
        }
    }
    
    public function show_combined_oee($showGroups){
        $this->layout = 'bare';
        $this->set('bodyClass',array('work-center'));
        if($this->request->is('ajax')){
            $groups = array(
                'Visų įrenginių bendras OEE'=>array(376,377,378,379,380,381,382,383,384,385,386,387,388,398,399,400,401,402,403,404,405,406,407,408,409,410,389,390,391,392,393,394,395,396,397,416,417,418,419,420,421,422,423,431,432,433,434,435,445,446,447,452,453,454,455,456,457,458,459,460,461,462,424,425,426,427,428,429,430,436,437,438,439,440,441,442,443,444),
                '1 grupės darbo centrų OEE'=>array(376,377,378,379,380,381,382,383,384,385,386,387,388,398,399,400,401,402,403,404,405,406,407,408,409,410),
                '2 grupės darbo centrų OEE'=>array(389,390,391,392,393,394,395,396,397,416,417,418,419,420,421,422,423,431,432,433,434,435,445,446,447,452,453,454,455,456,457,458,459,460,461,462),
                '3 grupės darbo centrų OEE'=>array(424,425,426,427,428,429,430,436,437,438,439,440,441,442,443,444),
                'Audimas 1 darbo centrų OEE'=>array(472, 473, 474, 475, 477, 478),
                'Audimas 2 darbo centrų OEE'=>array(466, 467, 468, 469, 470, 471, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 498),
            );
			if(trim($showGroups)){
				$showGroups = explode('_',$showGroups);
				$groups = array_filter($groups, function($val)use($showGroups){ static $i = 0; return in_array($i++,$showGroups);  });
			}
            $problemsExclusionModel = ClassRegistry::init('ShiftTimeProblemExclusion');
            $recordModel = ClassRegistry::init('Record');
            $exclusionList = $problemsExclusionModel->find('list', array('fields'=>array('problem_id')));
            $exclusionList = empty($exclusionList)?array(0):$exclusionList;
            foreach($groups as $groupTitle => $group){
                $recordModel->bindModel(array('belongsTo'=>array('FoundProblem','Plan')));
                $conditions = array('Record.created >=' => date('Y-m-d H:i:s', time()-1800));
                if(array_sum($group)){
                    $conditions['Record.sensor_id'] = $group;
                }
                $times = $recordModel->find('all', array(
                    'fields'=>array(
                        'TIMESTAMPDIFF(SECOND,MIN(Record.created),MAX(Record.created)) + '.Configure::read('recordsCycle').' - SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . implode(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS available_duration',
                        'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS fact_time',
                        'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, Record.unit_quantity,0)) as total_quantity',
                        'TRIM(Record.approved_order_id) AS approved_order_id',
                        'TRIM(Plan.step) AS step',
                        'TRIM(Record.sensor_id) AS sensor_id',
                    ),'conditions'=>$conditions,'group'=>array(
                        'Record.approved_order_id',
                    )
                 ));
                $dataInHours = array();
                foreach($times as $time){
                     if(!isset($dataInHours['shift_length'])){ $dataInHours['shift_length'] = 0; }
                     if(!isset($dataInHours['fact_prod_time'])){ $dataInHours['fact_prod_time'] = 0; }
                     if(!isset($dataInHours['count_delay'])){ $dataInHours['count_delay'] = 0; }
                     if(!isset($dataInHours['transition_time'])){ $dataInHours['transition_time'] = 0; }
                     $dataInHours['shift_length'] += $time[0]['available_duration'];
                     $dataInHours['shift_length_with_exclusions'] = $dataInHours['shift_length'];
                     $dataInHours['fact_prod_time'] += $time[0]['fact_time'];
                     if($time[0]['step'] > 0){
                         $dataInHours['count_delay'] = bcadd($dataInHours['count_delay'], bcdiv($time[0]['total_quantity'], bcdiv($time[0]['step'],3600,6), 6), 6);
                     }
                }
                $oeeParams[] = array(
                    'title'=>$groupTitle,
                    'data'=>$recordModel->calculateOee($dataInHours)
                );
            }
            echo json_encode(array('groups'=>$oeeParams, 'columns'=>2));
            die();
         }
		$this->set(compact('showGroups'));
    }



	// public function tmpSum(){
		// $apModel = ClassRegistry::init('ApprovedOrder');
		// $rec = $this->Record->find('all', array('fields'=>array('SUM(unit_quantity) as quantity', 'approved_order_id'), 'group'=>array('approved_order_id')));
		// foreach($rec as $r){
			// $apModel->id = $r['Record']['approved_order_id'];
			// $apModel->save(array('quantity'=>$r[0]['quantity'], 'box_quantity'=>$r[0]['quantity']));
		// }
		// die('Atlikta');
	// }
    
}
