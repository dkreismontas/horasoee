<?php
echo $this->Html->css(Configure::read('companyTitle').'.wc_in_problems');
echo $this->Html->script(Configure::read('companyTitle').'.vue');
?>
<div id="sensorsInProblem">
    <h2 class="text-center"><?php echo __('Staklės, kurios sustojo dėl problemos "Baigėsi špūlė"'); ?></h2>
    <table class="table table-bordered">
        <thead>
            <tr>
                <th><?php echo __('Staklės'); ?></th>
                <th><?php echo __('Prastovos pradžia'); ?></th>
                <th><?php echo __('Prastovos trukmė'); ?></th>
            </tr>
        </thead>
        <tbody>
            <tr v-for="sensor in sensors">
                <td>{{sensor[0].name}}</td>
                <td>{{sensor.FoundProblem.start}}</td>
                <td>{{sensor.FoundProblem.duration}}</td>
            </tr>
        </tbody>
    </table>
</div>
<script type="text/javascript">
var vm = new Vue({
    el: '#sensorsInProblem',
    data: {
        sensors: {},
    },
    methods: {
        loadData: function(){
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/'.strtolower(Configure::read('companyTitle')).'/plugins/show_wc_in_problem/',true); ?>",
                type: 'post',
                context: this,
                dataType: 'JSON',
                async: true,
                success: function(request){
                    this.sensors = request.sensors;
                }
            })
        }
    },
    ready: function(){
         
    },
    beforeCompile: function(){ 
        this.loadData();
        setInterval(this.loadData,10000);
    },
    components: {
        
    }
});
        
</script>