<?php
echo $this->Html->css(Configure::read('companyTitle').'.combined_oee.css?v=2');
echo $this->Html->script(Configure::read('companyTitle').'.vue');
echo $this->Html->script(Configure::read('companyTitle').'.raphael.2.1.0.min');
echo $this->Html->script(Configure::read('companyTitle').'.justgage.1.0.1.min');

?>
<div id="combinedOee" class="group_<?php echo $showGroups; ?>">
    <h2 class="text-center widgettitle maintitle">
        <img src="/images/logo.png" height="50" class="logo" />
        <?php echo __('Įrenginių bendras OEE rodiklis'); ?></h2>
    <div id="container">
        <div class="row-fluid" v-for="i in rows">
            <column_comp v-for="sensors_group in sensors_groups|filterByColumns i" :sensors_group="sensors_group"></column_comp>
        </div>
    </div>
</div>
<template id="columnTpl">
    <div class="col-sm-{{12/columns}}">
        <div class="sensorsGroup">
            <div id="oee_gouge_{{index}}"></div>
        </div>
    </div>
</template>
<script type="text/javascript">
var vm = new Vue({
    el: '#combinedOee',
    data: {
        sensors_groups: {},
        columns: 0,
        rows: 0,
        index:0
    },
    methods: {
        loadData: function(){
            jQuery.ajax({
                url: "<?php echo $this->Html->url('/'.strtolower(Configure::read('companyTitle')).'/plugins/show_combined_oee/'.$showGroups,true); ?>",
                type: 'post',
                context: this,
                dataType: 'JSON',
                async: true,
                success: function(request){
                    this.sensors_groups = request.groups;
                    this.columns = request.columns;
                    this.rows = Math.ceil(this.sensors_groups.length / this.columns);
                }
            })
        }
    },
    ready: function(){
         
    },
    beforeCompile: function(){ 
        this.loadData();
        setInterval(this.loadData,1800000);
    },filters: {
        filterByColumns: function(groups, row_nr){
            var settings = {columnsCount: this.columns, currentRowNr: row_nr};
            return groups.filter(function(value,index){
                var maxIndex = (settings.currentRowNr+1)*settings.columnsCount;
                var minIndex = maxIndex - settings.columnsCount;
                return index < maxIndex && index >= minIndex;
            });
        }
    },
    components: {
        'column_comp': {
            template: '#columnTpl',
            props: ['sensors_group'],
            data: function(){return { columns:0, index: 0}},
            beforeCompile: function(){
                this.columns = this.$parent.columns;
                this.$parent.index++;
                this.index = this.$parent.index;
            },
            ready: function(){
                this.buildGraph(); 
            },
            methods: {
                buildGraph: function(){
                        new JustGage({
                            id: 'oee_gouge_'+(this.index),
                            value: Math.min(100, parseInt(Math.round(this.sensors_group.data.oee))),
                            //value: this.sensors_group.data.oee,
                            min: 0,
                            max: 100,
                            levelColors: ['#f40c05', '#f46a05', '#D64D49', '#f4d505', '#e0f405', '#aef405', '#7bf405', '#22de03', '#1ebb04', '#189104'],
                            levelColorsGradient: true,
                            title: this.sensors_group.title,
                            titleMinFontSize: "15",
                            titlePosition: "below",
                        });
                }
            }
        }
    }
});
        
</script>