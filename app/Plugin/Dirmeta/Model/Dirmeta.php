<?php
class Dirmeta extends AppModel{

    public function WorkCenter_BuildStep_Hook(&$fPlan, &$fProduct, $fSensor, $dataFromInputForm){
        $fPlan['production_name'] = isset($dataFromInputForm['add_production_code'])?$fPlan['mo_number'].' '.$dataFromInputForm['add_production_code']:$fPlan['production_name'];
        $fPlan['production_code'] = $dataFromInputForm['add_production_code']??$fPlan['production_code'];
        $fPlan['quantity'] = $dataFromInputForm['add_quantity']??0;
    }

    public function Users_ChooseSensorBeforeRedirect_Hook(&$choosedSensorId){
        $sensorModel = ClassRegistry::init('Sensor');
        $fSensor = $sensorModel->findById($choosedSensorId);
        $sensorModel->updateAll(array('Sensor.active_user_id'=>Configure::read('user')->id), array('Sensor.id'=>$choosedSensorId));
    }

    public function Users_beforeChooseSensor_Hook(&$usersController, &$fUser){
        if($fUser && $fUser['User']['active_sensor_id'] > 0){
            $fUser['User']['active_sensor_id']  = 0;
        }
    }

}
    