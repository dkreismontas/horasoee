<?php
//error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$centerAlignBoldStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 20,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$rightAlignBoldStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$borderStyleArrayBold = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ),
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
);
$lineTitleStyle = array(
    'font' => array(
        'size' => 14,
        'name' => 'Times New Roman',
    ),
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    ),
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);
if(!function_exists('nts')){
    function nts($col) {// number to letter cordinates
        return PHPExcel_Cell::stringFromColumnIndex($col);
    }
    function cts($row, $col) {// number cordinates to letter
        return nts($row) . $col;
    }
}
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Dienos rezultatai'), false);
$colNr = 0; $rowNr = 5;
$shiftTypeRowMap = array();
$sensorColumnMap = array();
$totalInBlock = array();
$shiftsTotals = array();
$fontBoldStyle = $lineTitleStyle; $fontBoldStyle['font']['bold'] = true;
$pinkBgStyle = $centerAlignStyleArray;
$pinkBgStyle['fill'] = array(
    'type' => PHPExcel_Style_Fill::FILL_SOLID,
    'color' => array('rgb' => 'ff99a6')
);
$pinkBgStyle['font'] = array('size' => 14, 'name' => 'Times New Roman');
$yellowBgStyle = $pinkBgStyle;
$yellowBgStyle['fill']['color'] = array('rgb' => 'fffaa3');
foreach($calculations as $calculation){
    $line = $calculation['Line'];
    $calculation = $calculation['DashboardsCalculation'];
    $sensorId = $calculation['sensor_id'];
    $shiftNr = substr($dayShifts[$calculation['shift_id']],-1);
    if(!isset($shiftTypeRowMap[$shiftNr])){ //Aprasome naujos pamainos numerius ataskaitos headerius eilutese
        if(!empty($shiftTypeRowMap)){
            $shiftTypeRowMap[$shiftNr] = max($shiftTypeRowMap) + 1;
        }else{
            $shiftTypeRowMap[$shiftNr] = $rowNr + 2;
        }
        $activeSheet->setCellValueByColumnAndRow($colNr, $shiftTypeRowMap[$shiftNr], $shiftNr.' '.__('Pamaina'))->getStyle(cts($colNr, $shiftTypeRowMap[$shiftNr]))->applyFromArray($lineTitleStyle);
    }
    if(!isset($sensorColumnMap[$sensorId])){ //Aprasome naujo jutiklio ataskaitos headerius stulpeliuose
        if(!empty($sensorColumnMap)){
            $sensorColumnMap[$sensorId] = max($sensorColumnMap) + 4;
        }else{
            $sensorColumnMap[$sensorId] = $colNr+1;
        }
        $currentCol = $sensorColumnMap[$sensorId];
        $activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr, $line['name'])->getStyle(cts($currentCol, $rowNr))->applyFromArray($fontBoldStyle);
        $activeSheet->mergeCells(nts($currentCol).$rowNr.':'.nts($currentCol+3).$rowNr);
        $activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Laikas'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($centerAlignStyleArray);
        $activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Išeiga'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($centerAlignStyleArray);
        $activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Kokybė'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($lineTitleStyle);
        $activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Dienos OEE'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($yellowBgStyle);
    }
    if(!isset($totalInBlock[$sensorId])) {
        $totalInBlock[$sensorId] = $calculation;
    }else{
        array_walk($totalInBlock[$sensorId], function(&$value,$key)use($calculation){
            $value = (float)$value + (float)$calculation[$key];
        });
    }
    if(!isset($shiftsTotals[$shiftNr])) {
        $shiftsTotals[$shiftNr] = $calculation;
    }else{
        array_walk($shiftsTotals[$shiftNr], function(&$value,$key)use($calculation){
            $value = (float)$value + (float)$calculation[$key];
        });
    }
    $currentCol = $sensorColumnMap[$sensorId];
    $currentRow = $shiftTypeRowMap[$shiftNr];
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($calculation['exploitation_factor_with_exclusions']*100,2,'.',''))->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($calculation['operational_factor']*100,2,'.',''))->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, $calculation['all_quantity'] > 0?number_format(bcdiv($calculation['good_quantity'],$calculation['all_quantity'],2)*100,2,'.',''):0)->getStyle(cts($currentCol++, $currentRow))->applyFromArray($lineTitleStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($calculation['oee_with_exclusions'],2,'.',''))->getStyle(cts($currentCol++, $currentRow))->applyFromArray($yellowBgStyle);
}
//Bendras blokas skaiciuoja pagal pamainos numeri
$currentCol = max($sensorColumnMap)+4;
$sensorColumnMap[0] = max($sensorColumnMap) + 4;
$activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr, __('Bendras'))->getStyle(cts($currentCol, $rowNr))->applyFromArray($fontBoldStyle);
$activeSheet->mergeCells(nts($currentCol).$rowNr.':'.nts($currentCol+3).$rowNr);
$activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Laikas'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Išeiga'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($centerAlignStyleArray);
$activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Kokybė'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($lineTitleStyle);
$activeSheet->setCellValueByColumnAndRow($currentCol, $rowNr+1, __('Dienos OEE'))->getStyle(cts($currentCol++, $rowNr+1))->applyFromArray($yellowBgStyle);
foreach($shiftsTotals as $shiftNr => $calculation){
    $currentCol = $sensorColumnMap[0];
    $currentRow = $shiftTypeRowMap[$shiftNr];
    $qualityFactor = $calculation['all_quantity'] > 0?number_format(bcdiv($calculation['good_quantity'],$calculation['all_quantity'],2)*100,2,'.',''):0;
    $calculation['all_quantity'] = $calculation['good_quantity'] = 0;
    $oeeCalc = $recordModel->calculateOee($calculation);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['exploitation_factor_exclusions']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['operational_factor']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, ($qualityFactor).'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($lineTitleStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($yellowBgStyle);
    if(!isset($totalInBlock[0])) {
        $totalInBlock[0] = $calculation;
    }else{
        array_walk($totalInBlock[0], function(&$value,$key)use($calculation){
            $value = (float)$value + (float)$calculation[$key];
        });
    }
}

//Linijos EMV*
$currentRow = max($shiftTypeRowMap)+2;
$activeSheet->setCellValueByColumnAndRow($colNr, $currentRow, __('Linijos EMV*'))->getStyle(cts($colNr, $currentRow))->applyFromArray($pinkBgStyle);
$activeSheet->getRowDimension($currentRow)->setRowHeight(50);
$totalEMV = array();
foreach($monthCalculations as $sensorId => $calculation){
    $qualityFactor = $calculation['all_quantity'] > 0?number_format(bcdiv($calculation['good_quantity'],$calculation['all_quantity'],2)*100,2,'.',''):0;
    $calculation['all_quantity'] = $calculation['good_quantity'] = 0;
    $oeeCalc = $recordModel->calculateOee($calculation);
    $currentCol = $sensorColumnMap[$sensorId];
    $currentRow = max($shiftTypeRowMap)+2;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['exploitation_factor_exclusions']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['operational_factor']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, $qualityFactor.'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
    if(empty($totalEMV)) {
        $totalEMV = $calculation;
    }else{
        array_walk($totalEMV, function(&$value,$key)use($calculation){
            $value = (float)$value + (float)$calculation[$key];
        });
    }
}

//Bendras dienos rezultatas eilute
$currentRow = max($shiftTypeRowMap)+1;
$activeSheet->setCellValueByColumnAndRow($colNr, $currentRow, __('Bendras dienos rezultatas'))->getStyle(cts($colNr, $currentRow))->applyFromArray($lineTitleStyle);
$activeSheet->getRowDimension($currentRow)->setRowHeight(50);
foreach($totalInBlock as $sensorId => $calculation){  //$sensorId: 0 Bendras dienos rezultatas
    $qualityFactor = $calculation['all_quantity'] > 0?number_format(bcdiv($calculation['good_quantity'],$calculation['all_quantity'],2)*100,2,'.',''):0;
    $calculation['all_quantity'] = $calculation['good_quantity'] = 0;
    $oeeCalc = $recordModel->calculateOee($calculation);
    $currentCol = $sensorColumnMap[$sensorId];
    $currentRow = max($shiftTypeRowMap)+1;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['exploitation_factor_exclusions']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['operational_factor']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, $qualityFactor.'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($lineTitleStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalc['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($yellowBgStyle);
}

//Galutinis bendras rausvas EMV eilute
$oeeCalcEMV = $recordModel->calculateOee($totalEMV);
$currentCol = $sensorColumnMap[0];
$currentRow = max($shiftTypeRowMap)+2;
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalcEMV['exploitation_factor_exclusions']*100,2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalcEMV['operational_factor']*100,2,'.','').' %')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, ($totalEMV['all_quantity'] > 0?number_format(bcdiv($totalEMV['good_quantity'],$totalEMV['all_quantity'],2)*100,2,'.',''):0).'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, number_format($oeeCalcEMV['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol++, $currentRow))->applyFromArray($pinkBgStyle);

//Pagrindiniai ataskaitos pavadinimai
$currentCol = 0; $currentRow = $rowNr - 3;
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, $this->request->data['date'])->getStyle(cts($currentCol, $currentRow))->applyFromArray($centerAlignBoldStyleArray);
$activeSheet->mergeCells(nts($currentCol).$currentRow.':'.nts($currentCol+max($sensorColumnMap)+3).$currentRow);
$activeSheet->getRowDimension($currentRow)->setRowHeight(30);
$currentCol = 0; $currentRow = $rowNr - 2;
$activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, __('Dienos rezultatai'))->getStyle(cts($currentCol, $currentRow))->applyFromArray($centerAlignBoldStyleArray);
$activeSheet->mergeCells(nts($currentCol).$currentRow.':'.nts($currentCol+max($sensorColumnMap)+3).$currentRow);
$activeSheet->getRowDimension($currentRow)->setRowHeight(30);

if(isset($totalInBlock[0])) {
    $currentRow = max($shiftTypeRowMap) + 4;
    $currentCol = 1;
    $oeeCalc = $recordModel->calculateOee($totalInBlock[0]);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, __('Bendras dienos'));
    $activeSheet->mergeCells(nts($currentCol) . $currentRow . ':' . nts($currentCol + 1) . $currentRow)->getStyle(nts($currentCol) . $currentRow . ':' . nts($currentCol + 1) . $currentRow)->applyFromArray($lineTitleStyle);
    $currentRow++;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, __('Kokybė'))->getStyle(cts($currentCol, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol+1, $currentRow, __('Dienos OEE'))->getStyle(cts($currentCol + 1, $currentRow))->applyFromArray($centerAlignStyleArray);
    $currentRow++;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, ($totalInBlock[0]['all_quantity'] > 0?number_format(bcdiv($totalInBlock[0]['good_quantity'],$totalInBlock[0]['all_quantity'],2)*100,2,'.',''):0).'%')->getStyle(cts($currentCol, $currentRow))->applyFromArray($lineTitleStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol+1, $currentRow, number_format($oeeCalc['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol+1, $currentRow))->applyFromArray($lineTitleStyle);
}
if($totalEMV) {
    $currentRow = max($shiftTypeRowMap) + 4;
    $currentCol = 4;
    $oeeCalc = $recordModel->calculateOee($totalInBlock[0]);
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, __('Bendras EMV*'));
    $activeSheet->mergeCells(nts($currentCol) . $currentRow . ':' . nts($currentCol + 1) . $currentRow)->getStyle(nts($currentCol) . $currentRow . ':' . nts($currentCol + 1) . $currentRow)->applyFromArray($lineTitleStyle);
    $currentRow++;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, __('Kokybė'))->getStyle(cts($currentCol, $currentRow))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($currentCol+1, $currentRow, __('OEE'))->getStyle(cts($currentCol + 1, $currentRow))->applyFromArray($centerAlignStyleArray);
    $currentRow++;
    $activeSheet->setCellValueByColumnAndRow($currentCol, $currentRow, ($totalEMV['all_quantity'] > 0?number_format(bcdiv($totalEMV['good_quantity'],$totalEMV['all_quantity'],2)*100,2,'.',''):0).'%')->getStyle(cts($currentCol, $currentRow))->applyFromArray($pinkBgStyle);
    $activeSheet->setCellValueByColumnAndRow($currentCol+1, $currentRow, number_format($oeeCalcEMV['oee_exclusions'],2,'.','').'%')->getStyle(cts($currentCol+1, $currentRow))->applyFromArray($pinkBgStyle);
}

$activeSheet->setCellValueByColumnAndRow(0, $currentRow+2, __('* - Einamojo mėnesio vidurkis (EMV)'));

for ($col = 0; $col != max($sensorColumnMap)+8; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(15);
}
$activeSheet->getColumnDimension(nts(0))->setWidth(20);
//$activeSheet->getColumnDimension(nts(0))->setWidth(30);
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    ob_end_clean();
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename=paros_ataskaita.xlsx' );
    $objWriter->save('php://output');
    die();
}

