<?php
$current_page = trim(urldecode($_GET['current_page']),'/');
if(!isset($current_page)) die();
if(false){ ?><script type="text/javascript"><?php }
    if(preg_match('/.*dashboards\/inner.*/i',trim($current_page,'/'))){ ?>
    jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#geraldaDaysReportForm" aria-controls="geraldaDaysReportForm" role="tab" data-toggle="tab"><?php echo __('Dienos rezultatų ataskaita'); ?></a></li>');
    let geraldaDaysReportForm = jQuery('#five').clone();
    geraldaDaysReportForm.find('form').attr('action', '/geralda/reports/generate_days_report');
    geraldaDaysReportForm.find('input.start_end_date').parent().find('label').html('<?php echo __('Data'); ?>');
    geraldaDaysReportForm.find('input.start_end_date').parent().append('<input type="text" value="<?php echo date('Y-m-d',strtotime('-1 day')); ?>" class="datepicker form-control" name="date" />');
    geraldaDaysReportForm.find('input.start_end_date').remove();
    geraldaDaysReportForm.find('#form_type').val('Dienos rezultatu ataskaita');
    geraldaDaysReportForm.find('.timePattern').remove();
    geraldaDaysReportForm.find('form').prepend('<input type="hidden" name="data[time_pattern]" class="time_pattern" value="0">');
    jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane" id="geraldaDaysReportForm">'+geraldaDaysReportForm.html()+'</div>');
    jQuery(document).ready(function ($) {
        $(".datepicker").datepicker(<?php echo json_encode(array_merge(array('maxDate'=>-1),$this->App->datePickerConfig())); ?>);
    });


<?php }; ?>