<?php
class Geralda extends AppModel{
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
    
    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Geralda/css/Geralda_bare.css';
    }
    
    public function setTimeGraphHoverProduct(&$obj){
        static $founProblemModel = null, $ApprovedOrder = null, $avgSpeedByPlan = null;
        if($founProblemModel == null){
            $founProblemModel = ClassRegistry::init('FoundProblem');
        }
        if($ApprovedOrder == null){
            $ApprovedOrder = ClassRegistry::init('ApprovedOrder'); 
        }
        if($avgSpeedByPlan == null){
            $avgSpeedByPlan = array(); 
        }
        if(!isset($avgSpeedByPlan[$obj->data[Plan::NAME]['id']])){
            $avgSpeed = $founProblemModel->find('first', array(
                'fields'=>array(
                    'SUM(FoundProblem.quantity) / (SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end))/3600) AS avg_speed'
                ),'conditions'=>array(
                    'FoundProblem.plan_id'=>$obj->data[Plan::NAME]['id'],
                    'FoundProblem.problem_id'=>-1
                )
            ));
            $avgSpeedByPlan[$obj->data[Plan::NAME]['id']] = number_format($avgSpeed[0]['avg_speed'],2,'.','');
        }
        $obj->title .= '<br/><b>'.__('Gaminys').'</b>: '.$obj->data[Plan::NAME]['production_code'].' ('.$obj->data[Plan::NAME]['production_name'].')';
        $obj->title .= '<br/><b>'.__('Užsakymo nr.').'</b>: '.$obj->data[Plan::NAME]['mo_number'];
        $obj->title .= '<br/><b>'.__('Vid. užsakymo greitis').'</b>: '.($avgSpeedByPlan[$obj->data[Plan::NAME]['id']]).' vnt/h';
        if(isset($obj->data[ApprovedOrder::NAME])) {
            $relatedCount = $ApprovedOrder->getRelatedApprovedOrders($obj->data[Plan::NAME]['id'], $obj->data[ApprovedOrder::NAME]['id']);             
            $obj->title .= '<br/><b>' . __('Pagaminta').':</b> '. ($obj->data[ApprovedOrder::NAME]['quantity'] + $relatedCount).($obj->data[Plan::NAME]['quantity']>0?' '.__('iš').' '.$obj->data[Plan::NAME]['quantity']:'') ;
            //$obj->title .= '<br/><b>' . __('Intervalo kiekis').':</b> '. $obj->intervalQuantity;
            
            $obj->title .= '<br/><b>' . __('Pažymėto intervalo kiekis').':</b> '. round($obj->intervalQuantity,2);
            if(isset($obj->data['Work']) && !empty($obj->data['Work'])){
                $obj->title .= '<br/><b>' . __('Viso intervalo kiekis').':</b> '.round($obj->data['Work']['quantity'],2);
            }
            if($obj->data[Plan::NAME]['quantity'] > 0){
                $obj->title .= '<br/><b>' . __('Likęs kiekis').':</b> '.($obj->data[Plan::NAME]['quantity'] - $obj->data[ApprovedOrder::NAME]['quantity'] - $relatedCount);
            }
            if($relatedCount > 0){
                $obj->title .= '<br/><b>' . __('Nuo paskutinio atidėjimo pagaminta').':</b> '.$relatedCount;
            }
        }
    }

//    public function WorkCenter_index_Hook(&$data, &$fSensor){
//        if($fSensor['Sensor']['type'] == Sensor::TYPE_MIXING){
//            $data['structure']['add_collectable_by_hand'] = array('title'=>__('Užsakymas nurenkamas?'), 'additional_info'=>true, 'type'=>'checkbox', 'required'=>false);
//        }
//    }

    public function QualityFactor_AfterAddSuggestionToAvailableMos_Hook(&$notUsedMoList){
        $notUsedMoList = array_filter($notUsedMoList, function($data){
            $additionalData = json_decode($data['ApprovedOrder']['additional_data']);
            return !isset($additionalData->add_collectable_by_hand) || $additionalData->add_collectable_by_hand != 1;
        });
    }
    
    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$periods){
        foreach($periods as &$period){
            $period->title = preg_replace_callback('/greitis<\/b>:\s*([^\s]+)\s*vnt\/min/', function($match){
                return isset($match[1])?'greitis</b>: '.($match[1]*60).' vnt/h':$match[0];
            }, $period->title);
			if(trim($period->shortTitle) && isset($period->data['Work']['id']) && $period->data['Work']['problem_id'] == -1){
				$period->shortTitle = $period->data['Plan']['production_code'];
			}
			
        }
    }
    
    public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
        $settingsModel = ClassRegistry::init('Settings');
        $testDowntimeId = $settingsModel->getOne('test_production_downtime_id');
        if(!$testDowntimeId){ return null; }
        $planModel = ClassRegistry::init('Plan');
        $planModel->bindModel(array('belongsTo'=>array('Product')));
        $plan = $planModel->findById($appOrder['ApprovedOrder']['plan_id']);
        if(!preg_match('/^test.+/i',trim($plan['Product']['name']))){ return null; }
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $logModel = ClassRegistry::init('Log');
        $shiftModel = ClassRegistry::init('Shift');
        $recordModel = ClassRegistry::init('Record');
        $sensorModel = ClassRegistry::init('Sensor');   
        $foundProblemModel->deleteAll(array('FoundProblem.end >'=>$appOrder['ApprovedOrder']['created'], 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
        $fShift = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $start = new DateTime($appOrder['ApprovedOrder']['created']);
        $quantity = $recordModel->find('first', array('fields'=>array('SUM(Record.quantity) AS quantity_sum'), 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$start->format('Y-m-d H:i:s'))));
        $currentProblem = $foundProblemModel->newItem($start, new DateTime(),
                            $testDowntimeId, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $plan[Plan::NAME]['id'], array(), $quantity[0]['quantity_sum']);
        $recordModel->updateAll(
            array('Record.found_problem_id'=>$currentProblem['FoundProblem']['id']), 
            array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$currentProblem['FoundProblem']['start'])
        );
        $sensorModel->updateAll(
            array('Sensor.tmp_prod_starts_on'=>$fSensor['Sensor']['prod_starts_on'], 'Sensor.prod_starts_on'=>9999),
            array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
        );
    }

    public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
        if($fSensor['Sensor']['prod_starts_on'] >= 9999){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(
                array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
                array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
            );
        }
    }

    public function WorkCenter_BeforePostphoneProdSave_Hook(&$ord,&$fSensor){
        if($fSensor['Sensor']['prod_starts_on'] >= 9999){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(
                array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
                array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
            );
        }
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, $plans, &$currentPlan, &$quantityOutput, $fShift){
        if(!empty($currentPlan)){
            $currentPlan['Plan']['step_unit_title'] = 'vnt/h';
            $currentPlan['Plan']['step_unit_divide'] = 1;
        }
    }

    public function Workcenter_changeUpdateStatusVariables_Hook2(&$val, $fSensor){
        $val['lastMinuteSpeed'] *= 60;
    }

}
    