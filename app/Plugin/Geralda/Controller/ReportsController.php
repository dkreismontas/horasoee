<?php
class ReportsController extends GeraldaAppController{

    public $uses = array('DashboardsCalculation','Shift','Sensor');

    public function generate_days_report(){
        if(!isset($this->request->data['sensors'])) {
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('plugin' => false, 'controller' => 'dashboards', 'action' => 'inner'));
        }
        if(!preg_match('/^\d{4}-\d{2}-\d{2}$/i', trim($this->request->data['date']))){
            $this->Session->setFlash(__('Neparinkta data'));
            $this->redirect(array('plugin' => false, 'controller' => 'dashboards', 'action' => 'inner'));
        }
        $sensorsBranches = $this->Sensor->find('list', array('fields'=>array('Sensor.id','Sensor.branch_id'), 'conditions'=>array('Sensor.id'=>$this->request->data['sensors'])));
        $dayShifts = $this->Shift->find('list', array(
            'fields'=>array('Shift.id','Shift.type'),
            'conditions'=>array(
                'Shift.branch_id'=>$sensorsBranches,
                'Shift.disabled'=>0,
                'DATE_FORMAT(Shift.start, \'%Y-%m-%d\')' => $this->request->data['date'],
            )
        ));
        $this->DashboardsCalculation->bindModel(array('belongsTo'=>array(
            'Sensor','Line'=>array('foreignKey'=>false, 'conditions'=>array('Sensor.line_id = Line.id')),'Shift'
        )));
        $calculations = $this->DashboardsCalculation->find('all', array(
            'fields'=>array(
                'DashboardsCalculation.*',
                'Line.name'
            ),
            'conditions'=>array(
                'DashboardsCalculation.shift_id' => array_keys($dayShifts),
                'DashboardsCalculation.sensor_id'=>$this->request->data['sensors']
            ),'order'=>array(
                'Line.name','Shift.type'
            )
        ));
        $this->DashboardsCalculation->bindModel(array('belongsTo'=>array('Shift')));
        $monthCalculations = Hash::combine($this->DashboardsCalculation->find('all', array(
            'fields'=>array(
                'SUM(DashboardsCalculation.shift_length) AS shift_length',
                'SUM(DashboardsCalculation.shift_length_with_exclusions) AS shift_length_with_exclusions',
                'SUM(DashboardsCalculation.fact_prod_time) AS fact_prod_time',
                'SUM(DashboardsCalculation.count_delay) AS count_delay',
                'SUM(DashboardsCalculation.all_quantity) AS all_quantity',
                'SUM(DashboardsCalculation.good_quantity) AS good_quantity',
                'DashboardsCalculation.sensor_id'
            ),
            'conditions'=>array(
                'Shift.disabled'=>0,
                'DATE_FORMAT(Shift.start, \'%Y-%m\')' => date('Y-m', strtotime($this->request->data['date'])),
                'DashboardsCalculation.sensor_id'=>$this->request->data['sensors']
            ),'group'=>array(
                'DashboardsCalculation.sensor_id'
            )
        )),'{n}.DashboardsCalculation.sensor_id', '{n}.0');
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $recordModel = ClassRegistry::init('Record');
        $this->set(compact('calculations','dayShifts','objPHPExcel','recordModel','monthCalculations'));
        //pr($calculations);die();
    }

}