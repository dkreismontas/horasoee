<?php
App::uses('GeraldaAppController', 'Geralda.Controller');

class PluginsController extends GeraldaAppController {
	
    public $uses = array('Shift','Record','Sensor','Branch');
    
    public function index(){
        $this->layout = 'ajax';
    }
    
    public function beforeFilter(){
        if(is_array(Configure::read('ByPassServersIP')) && in_array($_SERVER['REMOTE_ADDR'], Configure::read('ByPassServersIP'))){
            $this->Auth->allow(array(
                'get_current_shift_oee'
            ));
        }
        parent::beforeFilter();
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Geralda.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }
    
    /*function save_sensors_data(){
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start.txt';
        if(file_exists($dataPushStartMarkerPath) && time() - filemtime($dataPushStartMarkerPath) < 7200){
            $this->Sensor->informAboutProblems();
            die('Vis dar vyksta duomenu irasymas is praejusios sesijos'); 
        }
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $context = stream_context_create(array (
            'http' => array (
                'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            )
        ));
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
        $this->Sensor->virtualFields = array();
        //$saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://88.119.201.38:5580/data',
        );
        $mailOfErrors = array();
        $fh = fopen($dataPushStartMarkerPath, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        $this->Sensor->executeFailedQueries();
        foreach($adressList as $addressPort => $adress){
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $title){
                    list($sensorName, $sensorPort) = explode('_',$title);
                    if($addressPort != $sensorPort) continue;
                    if(!isset($record->$sensorName)) continue;
                    $quantity = $record->$sensorName;
                    try{
                        if(Configure::read('ADD_FULL_RECORD')){
                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                        }else{
                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                        }
                        $db->query($query);
                    }catch(Exception $e){
                        $this->Sensor->logFailedQuery($query);
                        $this->Sensor->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                    }
                }
            }
        }
        unlink($dataPushStartMarkerPath);
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->Sensor->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) >  60){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->Sensor->informAboutProblems();
            $this->requestAction('/work-center/update-status?update_all=1');
            //unlink($markerPath);
        }
        $this->Sensor->moveOldRecords();
        session_destroy();
        die();
    }*/
 
    public function get_current_shift_oee(){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $oeeObj = new OeeShell;
        $branches = $this->Branch->find('list', array('fields'=>array('id')));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('ps_id','id'), 'conditions'=>array('ps_id <>'=>'')));
        $json = array();
        foreach($branches as $branchId){
            $fShift = $this->Shift->findCurrent($branchId);
            if(empty($fShift)){ continue; }
            $fShift[Shift::NAME]['end'] = date('Y-m-d H:i');
            $oeeObj->shift = $fShift;
            $oeeObj->workWithMo = false;
            $oeeData = $oeeObj->start_sensors_calculation($sensorsListInDB,true);
            foreach($oeeData as $data){
                $json[array_search($data['sensor_id'], $sensorsListInDB)] = array('oee'=>min(100, round($data['oee_with_exclusions'],0)));
            }
        }
        echo json_encode($json);
        die();
    }

}
