<?php
class HanzasMaiznicaReport extends AppModel{
    public $useTable = false;
    public $OrderCalculation, $FoundProblem, $DashboardsCalculation, $Plan, $Record, $Loss, $LossType, $User, $Problem, $PartialQuantity, $ApprovedOrder, $maxDowntimeLevel, $downtimesExclusionsIds;
    private static $colors = array(-1=>'419641', 1=>'DAA520',2=>'666666', 3=>'C12E2A');
    public $Help;
    public $sheetNr = 1;
    
    public function generateShiftSheet($objPHPExcel, $shift, $sensor_id, $sheet_number) {
        $return_arr = array();
        $true_problem_count = 0;
        $transition_count = 0;
        $no_work_time = 0;
        $this->OrderCalculation->bindModel(array('hasOne' => array('Plan' => array('className' => 'Plan', 'foreignKey' => false, 'conditions' => array('Plan.mo_number = OrderCalculation.mo_number AND Plan.sensor_id = OrderCalculation.sensor_id')))));
        $calculation_mos = $this->OrderCalculation->find('all', array(
            'conditions' => array('OrderCalculation.shift_id' => $shift['Shift']['id'], 'OrderCalculation.sensor_id' => $sensor_id),
            'order'      => array('OrderCalculation.mo_number ASC')
        ));
        $quantities_by_mo = array();
        foreach ($calculation_mos as $cm) {
            $quantities_by_mo[$cm['OrderCalculation']['mo_number']] = $cm['OrderCalculation']['quantity'];
        }
        //$problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        //$found_problems = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,0);
        $found_problems_transitions = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,1);
        $found_problems_no_work = $this->FoundProblem->getByShiftForReport($shift,$sensor_id,2);
        foreach($found_problems_no_work as $fp) {
            $no_work_time += $fp[0]['found_problem_duration'];
            $true_problem_count+=1;
        }
        $no_work_time=round($no_work_time/60);
        $transition_count = count($found_problems_transitions);
        $transition_time = 0;


        $found_problem_durations_sum_by_plan = array();
        foreach ($found_problems as $fp) {
            $plan_id = $fp['FoundProblem']['plan_id'];
            if (!array_key_exists($plan_id, $found_problem_durations_sum_by_plan)) {
                $found_problem_durations_sum_by_plan[$plan_id] = $fp[0]['found_problem_duration'];
            } else {
                $found_problem_durations_sum_by_plan[$plan_id] += $fp[0]['found_problem_duration'];
            }
        }
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'SUM('.$col.') AS '.$col; });
        $calculation = $this->DashboardsCalculation->find('first', array(
            'fields'=>$fields,
            'conditions' => array(
                'DashboardsCalculation.shift_id' => $shift['Shift']['id'], 
                'DashboardsCalculation.sensor_id' => $sensor_id
            ),'group'=>array('DashboardsCalculation.shift_id')
        ));
        if (sizeof($calculation) == 0) {
            return array();
            /*$objPHPExcel->createSheet();
            $workSheet->setCellValue('A1', __('Pamaina'));
            $workSheet->setCellValue('B1', $shift['Shift']['start'] . " - " . $shift['Shift']['end'] . " (" . $shift['Shift']['id'] . ")");
            $workSheet->setCellValue('E2', __('Nėra duomenų'));
            $return_arr = array(
                'theorical_time_in_shift' => 0,
                'shift_start'         => $shift['Shift']['start'],
                'shift_end'         => $shift['Shift']['end'],
                'shift_length'        => round((strtotime($shift['Shift']['end']) - strtotime($shift['Shift']['start']))/60),
                'theory_prod_time'    => 0,
                'fact_prod_time'      => 0,
                'true_problem_time'   => 0,
                'true_problem_count'  => 0,
                'transition_time'     => $transition_time,
                'transition_count'    => $transition_count,
                'exceeded_transition_time'=> 0,
                'no_work_time'        => $no_work_time,
                'exploitation_factor' => 0,
                'quantity_type1'      => 0,
                'quantity_type2'      => 0,
                'total_quantity'      => 0,
                'sensor_quantity'      => 0,
                'planned_quantity'    => 0,
                'quality_factor'      => 0,
                'losses_quantity'     => 0,
                'operational_factor'  => 0,
                'oee'                 => 0,
                'mttf'                => 0,
                'mttr'                => 0,
                'count_delay'         => 0,
                'non_planned_stops_quantity'=>0,
                'non_planned_stops_duration'=>0,
                'planned_stops_quantity'=>0,
                'planned_stops_duration'=>0,
            );
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $return_arr['exploitation_factor_with_exclusions'] = 0;
                $return_arr['oee_with_exclusions'] = 0;
                $return_arr['shift_length_with_exclusions'] = $return_arr['shift_length'] - $no_work_time;
            }
            return $return_arr;*/
        }
        if ($this->sheetNr != 0) {
            $objPHPExcel->createSheet();
        }
        $workSheet = $objPHPExcel->setActiveSheetIndex($this->sheetNr++);
        for ($col = 'A'; $col !== 'P'; $col ++){
            $workSheet
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }
        $sheetTitle = str_replace(':', ' ', $shift['Shift']['start']);
        $workSheet->setTitle($sheetTitle, false);
        
        $calculation = $calculation[0];
        //$calculation['shift_length'] = round((strtotime($shift['Shift']['end']) - strtotime($shift['Shift']['start']))/60);

        $approved_orders = $this->Record->getApprovedOrderTimes($shift, $sensor_id);
        $approved_orders_ids = Set::extract('/ApprovedOrder/id', $approved_orders);

        $plans = $this->Plan->find('all', array(
            'fields'     => array(
                'Plan.*',
                '(TIMESTAMPDIFF(SECOND,Plan.start, Plan.end)) as plan_time_all',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN Plan.start > \'' . $shift['Shift']['start'] . '\' THEN Plan.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN Plan.end < \'' . $shift['Shift']['end'] . '\' THEN Plan.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as plan_time',
            ),
            'conditions' => array(
                'start <'   => $shift['Shift']['end'],
                'end >'     => $shift['Shift']['start'],
                'sensor_id' => $sensor_id
            ),
            'order'      => array('Plan.start ASC')
        ));


        $quantities = $this->Record->getQuantities($shift, $sensor_id);
        $user = $this->Record->getUser($shift, $sensor_id);
        $quantities_fixed = array();
        foreach ($quantities as $q) {
            $quantities_fixed[$q[0]['approved_order_id']] = $q[0]['quantity'];
        }
        $quantities = $quantities_fixed;


        $this->Loss->bindModel(array('belongsTo' => array('LossType')));
        $losses = $this->Loss->find('all', array(
            'fields'     => array('SUM(Loss.value) as losses', 'CONCAT(LossType.name," (",LossType.unit,")") as full_name', 'LossType.id'),
            'conditions' => array('Loss.approved_order_id' => $approved_orders_ids),
            'group'      => array('loss_type_id')
        ));
        $losses = Hash::combine($losses, '{n}.LossType.id', '{n}');

        //$grouped_problems = $this->FoundProblem->getGroupedByProblem($found_problems);

        // Header shift info
        $workSheet->setCellValue('A1', __('Pamaina'));
        $workSheet->setCellValue('B1', $shift['Shift']['start'] . " - " . $shift['Shift']['end'] . " (" . $shift['Shift']['id'] . ")");

        $workSheet->getStyle('A1:B1')->getFont()->setBold(true);
        $currentRow = 4;
        $start_column = 'A';
        // Fact times
        #region region:fact times
        $workSheet->setCellValue('A3', __('Faktiniai laikai'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Darbo centras'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Produkcijos pavadinimas'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Produkcijos kodas'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Užsakymo numeris'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Pradžia'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Pabaiga'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Viso užsakymo kiekis iš jutiklio (vnt.)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Kiekis pamainoje iš jutiklio (vnt)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Patvirtintos produkcijos kiekis (vnt)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Deklaruotas kiekis pamainoje (vnt)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Teorinis gamybos laikas (min)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Trukmė pamainoje (min) su sustojimais'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Gamybos trukmė pamainoje (min)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Teorinis kiekis pamainoje (min)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Vid. gamybos greičio nuokrypis nuo normos (%)'));

        $workSheet->getStyle('A3:M4')->getFont()->setBold(true);

//        $workSheet->setCellValue('J4', 'Trukmė pamainoje (min)');
        $currentRow = 5;
        $start_column = 'A';
        $total_planned_quantity = 0;
        $theoricalTimeInShift = 0;
        $partialQuantityModel = ClassRegistry::init('PartialQuantity');
        $vilniausduonaModel = ClassRegistry::init($this->plugin.'.'.$this->plugin);
        foreach ($approved_orders as $ao) {
            $partialQuantity = $partialQuantityModel->find('first', array('conditions'=>array('PartialQuantity.approved_order_id'=>$ao['ApprovedOrder']['id'], 'PartialQuantity.shift_id'=>$shift['Shift']['id'])));
            $problem_durations = 0;
            $confirmedCountInShift = $vilniausduonaModel->getOrderCountInShift($ao['ApprovedOrder']['id'], $shift['Shift'], $ao['Sensor']['type'], $ao['Plan']['id']);
            $theoricalTime = $vilniausduonaModel->getTheoryTime($confirmedCountInShift, $ao['Plan']['step']);
            $theoricalTimeInShift += $theoricalTime;
            if (array_key_exists($ao['Plan']['id'], $found_problem_durations_sum_by_plan)) {
                $problem_durations = $found_problem_durations_sum_by_plan[$ao['Plan']['id']];
            }
//            echo ($ao[0]['fact_time']/60)." - ";
//            echo $found_problem_durations_sum_by_plan[$ao['Plan']['id']] . "<br>";
            $workSheet->setCellValue($start_column . $currentRow, $ao['Sensor']['name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['production_name'].'('.$ao['ApprovedOrder']['id'].')');
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['created']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['end']);  
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['quantity']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao[0]['sum_quantity']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['confirmation_shift_id'] == $shift['Shift']['id']?$ao['ApprovedOrder']['confirmed_quantity']:0);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['confirmation_shift_id'] == $shift['Shift']['id']?0:(!empty($partialQuantity)?$partialQuantity['PartialQuantity']['quantity']:0));
            $workSheet->setCellValue(++ $start_column . $currentRow, round($theoricalTime,2));
            $workSheet->setCellValue(++ $start_column . $currentRow, round(($ao[0]['fact_time']) / 60));
            $workSheet->setCellValue(++ $start_column . $currentRow, round(($ao[0]['fact_time_work']) / 60));
            $planned_quantity = max(0, round(($ao[0]['fact_time_work'] / 3600) * $ao['Plan']['step'] * 10) / 10);
            $total_planned_quantity += $planned_quantity;
            $workSheet->setCellValue(++ $start_column . $currentRow, $planned_quantity);
            //$speed = $ao[0]['fact_time_work'] > 0?$ao[0]['sum_quantity'] / $ao['Plan']['box_quantity'] / round(($ao[0]['fact_time_work']) / 60):0; //kiekis per minute
            $speed = round(($ao[0]['fact_time_work']) / 60) > 0?$ao[0]['sum_quantity'] / round(($ao[0]['fact_time_work']) / 60):0; //kiekis per minute
            $deviation = $ao['Plan']['step'] > 0?round($speed / ($ao['Plan']['step'] / 60) * 100, 2):0;
            $workSheet->setCellValue(++ $start_column . $currentRow, $deviation);
//            $workSheet->setCellValue(++$start_column.$currentRow,round($ao['operational_factor'] * 100)/100);
            $start_column = 'A';
            $currentRow ++;
        }

        // Found problem times
        #region region:fp times
        $currentRow ++;
        $workSheet->setCellValue('A' . $currentRow, __('Prastovos (žurnalas)'));
        $letterStart = 'A';
        $workSheet->setCellValue($letterStart++ . ++ $currentRow, __('Darbo centras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('MO'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio pavadinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio kodas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Paaiškinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 1'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 2'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 3'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 4'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pradžia'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pabaiga'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė tarp prastovų (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Plano greitis (step) vnt/h'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis pakuotėje/porcijoje'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':Q' . $currentRow)->getFont()->setBold(true);

        $currentRow ++;
        $start_column = 'A';
        $previous_date = null;
        /*foreach ($found_problems as $fp) {
            try{
                $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
            }catch(Exeption $e){
                $approvedAddData = null;
            }
            $workSheet->setCellValue($start_column . $currentRow, $fp['Sensor']['name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['DiffCard']['description']);
            $parsedProblemsTree = $fp['Problem']['id'] > 0 && isset($problemsTreeList[$fp['Problem']['id']])?array_reverse($problemsTreeList[$fp['Problem']['id']]):array();
            for($i=0; $i<4; $i++){
                if(isset($parsedProblemsTree[$i])){
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($parsedProblemsTree[$i]));
                }else{
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($fp['Problem']['name']));
                }
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['start']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['end']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($fp[0]['found_problem_duration'] / 60 * 10) / 10);
            if ($previous_date == null && $fp['FoundProblem']['start'] > $shift['Shift']['start']) {
                $previous_date = $shift['Shift']['start'];
            }
            if ($previous_date != null) {
                $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date);
                if ($date_diff < 0) {
                    $date_diff = 0;
                }
            }

            $workSheet->setCellValue(++ $start_column . $currentRow, ($previous_date != null) ? round($date_diff / 60, 2) : "");
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['step']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $this->getIntervalQuantity($fp));
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['box_quantity']);
            $previous_date = $fp['FoundProblem']['end'];
            //$bgCol = 'A';$start_column++;
            $backgroundColor = isset(self::$colors[$fp['FoundProblem']['problem_id']])?self::$colors[$fp['FoundProblem']['problem_id']]:self::$colors[3];
            $backgroundColor = in_array($fp['FoundProblem']['problem_id'], $this->downtimesExclusionsIds)?self::$colors[2]:$backgroundColor;
            $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->applyFromArray(
                array(
                    'fill' => array(
                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => array('rgb' => $backgroundColor)
                    )
                )
            );
            $start_column = 'A';
            $currentRow ++;
        }
        unset($found_problems);
        #endregion
        $currentRow ++;
        $workSheet->setCellValue('A' . $currentRow, __('Prastovos (ataskaita)'));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Pavadinimas'));
        $workSheet->setCellValue('B' . $currentRow, __('Viso trukmė (min)'));
        $workSheet->setCellValue('C' . $currentRow, __('Kiekis (vnt)'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':F' . $currentRow)->getFont()->setBold(true);
        $currentRow ++;
        $start_column = 'A';
        foreach ($grouped_problems as $gp) {
            $workSheet->setCellValue($start_column . $currentRow, $gp['problem_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($gp['duration'] / 60 * 10) / 10);
            $workSheet->setCellValue(++ $start_column . $currentRow, $gp['count']);

            $start_column = 'A';
            $currentRow ++;
        }
        */
        // Results
        #region region:results
        $currentRow ++;
        $workSheet->setCellValue('A' . $currentRow, __('Rodikliai'));
        $workSheet->getStyle('A' . ($currentRow) . ':A' . $currentRow)->getFont()->setBold(true);
        
        $oeeParams = $this->Record->calculateOee($calculation);
        // $parameters = array(&$shift, &$workSheet, &$currentRow, &$sensor_id, &$return_arr, &$calculation, &$oeeParams);
        // $this->callPluginFunction('Report_UpdateShiftSheetColumns_Hook', $parameters, Configure::read('companyTitle'));
        
        $workSheet->setCellValue('A' . ++ $currentRow, __('Teorinis gamybos laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, $theoricalTimeInShift);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Pamainos trukmė (min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['shift_length']);
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('Pamainos trukmė su išimtimis (min)'));
            $workSheet->setCellValue('B' . $currentRow, $calculation['shift_length_with_exclusions']);
        }
        $workSheet->setCellValue('A' . ++ $currentRow, __('Gamybos laikas faktas  (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['fact_prod_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų laikas  (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų kiekis (vnt)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_count'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Planuoti sustojimai (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['planned_stops_duration']/60,2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Planuoti sustojimai (kiekis)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['planned_stops_quantity']);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Neplanuoti sustojimai (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['non_planned_stops_duration']/60,2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Neplanuoti sustojimai (kiekis)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['non_planned_stops_quantity']);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Derinimų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['transition_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Viršytų derinimų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['exceeded_transition_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Viso supakuotas kiekis (vnt)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['sensor_quantity'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Visas patvirtintas kiekis (vnt)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['total_quantity'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prieinamumo koef.'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['exploitation_factor'],2));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('Prieinamumo koef. su išimtimis'));
            $workSheet->setCellValue('B' . $currentRow, round($oeeParams['exploitation_factor_exclusions'],2));
        }
        $workSheet->setCellValue('A' . ++ $currentRow, __('Efektyvumo koef.'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['operational_factor'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('OEE koef.'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['oee'],2));
        
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('OEE koef. su išimtimis'));
            $workSheet->setCellValue('B' . $currentRow, round($oeeParams['oee_exclusions'],2));
        }
        // $workSheet->setCellValue('A' . ++ $currentRow, __('Formavimas (vnt)'));
        // $workSheet->setCellValue('B' . $currentRow, $various_values['quantities']['type1']);
        // $workSheet->setCellValue('A' . ++ $currentRow, __('Linijos vidurys (vnt)'));
        // $workSheet->setCellValue('B' . $currentRow, $various_values['quantities']['type2']);
        ++ $currentRow;
        $workSheet->setCellValue('A' . ++ $currentRow, __('Deklaruotas brokas'));
        ++ $currentRow;
        foreach ($losses as $l) {
            $workSheet->setCellValue('A' . $currentRow, $l[0]['full_name']);
            $workSheet->setCellValue('B' . $currentRow, $l[0]['losses']);
            ++ $currentRow;
        }
        ++ $currentRow;
        $workSheet->setCellValue('A' . ++ $currentRow, 'MTTF');
        $workSheet->setCellValue('B' . $currentRow, $calculation['mttf']);
        $workSheet->setCellValue('A' . ++ $currentRow, 'MTTR');
        $workSheet->setCellValue('B' . $currentRow, $calculation['mttr']);

        #endregion
        $return_arr = array_merge($return_arr, array(
            'theorical_time_in_shift' => $theoricalTimeInShift,
            'shift_start'         => $shift['Shift']['start'],
            'shift_end'         => $shift['Shift']['end'],
            'shift_length'        => $calculation['shift_length'],
            'theory_prod_time'    => round($calculation['theory_prod_time'], 2),
            'fact_prod_time'      => round($calculation['fact_prod_time'],2),
            'true_problem_time'   => round($calculation['true_problem_time'],2),
            'true_problem_count'  => $calculation['true_problem_count'],
            'transition_time'     => round($calculation['transition_time'],2),
            'exceeded_transition_time'=> round($calculation['exceeded_transition_time'],2),
            'transition_count'    => $transition_count,
            'no_work_time'        => round($no_work_time,2),
            'exploitation_factor' => $oeeParams['exploitation_factor'],
            'total_quantity'      => round($calculation['total_quantity'],2),
            'sensor_quantity'      => round($calculation['sensor_quantity'],2),
            'planned_quantity'    => round($total_planned_quantity,2),
            'quality_factor'      => round($calculation['quality_factor'],2),
            'losses_quantity'     => ($losses) ? $losses : array(),
            'operational_factor'  => $oeeParams['operational_factor'],
            'oee'                 => $oeeParams['oee'],
            'mttf'                => round($calculation['mttf'],2),
            'mttr'                => round($calculation['mttr'],2),
            'count_delay'         => round($calculation['count_delay'],2),
            'user'                => isset($user[0]['user_id']) && $user[0]['user_id']>0?$this->User->findById($user[0]['user_id']):array(),
            'non_planned_stops_quantity'=>$calculation['non_planned_stops_quantity'],
            'non_planned_stops_duration'=>$calculation['non_planned_stops_duration'],
            'planned_stops_quantity'=>$calculation['planned_stops_quantity'],
            'planned_stops_duration'=>$calculation['planned_stops_duration'],
        ));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $return_arr['exploitation_factor_with_exclusions'] = $oeeParams['exploitation_factor_exclusions'];
            $return_arr['oee_with_exclusions'] = $oeeParams['oee_exclusions'];
            $return_arr['shift_length_with_exclusions'] = $calculation['shift_length_with_exclusions'];
        }
        return $return_arr;
    }
    
    public function generateGlobalSheet($objPHPExcel, $generatedGlobalData, $shifts_start, $shifts_end, $sensor_id) {
        if(empty($generatedGlobalData)) return;
        $workSheet = $objPHPExcel->setActiveSheetIndex(0);
        $workSheet->setTitle(__('Bendri'), false);

        $currentColumn = '0';
        $currentRow = 1;
        $startRow = 4;
        $lossTypes = $this->LossType->find('all', array('fields' => array('id', 'CONCAT(name," (",unit,")") as full_name')));
        //$problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        #region headers
        $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ++ $currentRow, __('Data'));
        $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ++ $currentRow, __('Pamaina'));
        $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ++ $currentRow, __('Rodikliai'));
        $workSheet->getStyle(PHPExcel_Cell::stringFromColumnIndex($currentColumn) . $currentRow)->getFont()->setBold(true);
        $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex(++$currentColumn) . 2, __('Bendri'));
        $dayColumns = array();
        foreach ($generatedGlobalData as $key => $single_shift) {
            if(empty($single_shift)) continue;
            //$shiftStart = current(array_reverse(explode(' ',$single_shift['shift_start'])));
            $shiftStart = $single_shift['shift_start'];
            $shiftEnd = $single_shift['shift_end'];
            $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex(++$currentColumn) . ($startRow-1), date('H:i:s', strtotime($shiftStart)).' - '.date('H:i:s', strtotime($shiftEnd)));
            $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ($startRow-2), date('Y-m-d', strtotime($shiftStart)));
            if(!isset($dayColumns[date('Y-m-d', strtotime($shiftStart))])){ $dayColumns[date('Y-m-d', strtotime($shiftStart))] = 0; }
            $dayColumns[date('Y-m-d', strtotime($shiftStart))]++;
            end($dayColumns);
            if(!empty($dayColumns) && key($dayColumns) == date('Y-m-d', strtotime($shiftStart)) && current($dayColumns) > 1){
                $FirstColLetter = PHPExcel_Cell::stringFromColumnIndex($currentColumn-current($dayColumns)+1);
                $LastColLetter = PHPExcel_Cell::stringFromColumnIndex($currentColumn);
                $workSheet->mergeCells($FirstColLetter.($startRow-2).':'.$LastColLetter.($startRow-2));
                $workSheet->setCellValue($FirstColLetter . ($startRow-2), key($dayColumns));
                $workSheet->getStyle($FirstColLetter . ($startRow-2))->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
            }
        }
        $workSheet->setCellValue(PHPExcel_Cell::stringFromColumnIndex(++$currentColumn) . 2, __('Bendri'));
        $currentColumn ='A';
        // $parameters = array(&$generatedGlobalData, &$workSheet, &$currentRow, &$startRow);
        // $this->callPluginFunction('Report_UpdateGlobalSheetColumns_Hook', $parameters, Configure::read('companyTitle'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Teorinis gamybos laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Pamainos trukmė (min)'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Pamainos trukmė su išimtimis (min)'));
        }
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Gamybos laikas planas (min)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Gamybos laikas faktas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Planuoti sustojimai (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Planuoti sustojimai (kiekis)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Neplanuoti sustojimai (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Neplanuoti sustojimai (kiekis)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Derinimo laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Derinimų kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Viršytų derinimų laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra darbo laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prieinamumo koef.'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prieinamumo koef. su išimtimis'));
        }
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Efektyvumo koef.'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('OEE'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('OEE su išimtimis (pamaina)'));
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('OEE su išimtimis (para)'));
        }
        ++ $currentRow;
        // $workSheet->setCellValue($currentColumn . ++ $currentRow, 'Formavimas (vnt)');
        // $workSheet->setCellValue($currentColumn . ++ $currentRow, 'Linijos vidurys (vnt)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Viso supakuotas kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Visas patvirtintas kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis kiekis per minutę'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis kiekis per minutę su išimtimis'));
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Suplanuotas kiekis (vnt)');
        ++ $currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Kokybės koef.');
//        ++$currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Praradimai dėl kepimo (vnt)');
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Praradimai dėl raikymo (vnt)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Deklaruotas brokas'));
        foreach ($lossTypes as $lt) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $lt[0]['full_name']);
        }
        ++ $currentRow;
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTTF (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTTR (min)'));
        ++ $currentRow;

        #endregion

        $currentColumn ++;
        $currentColumn ++;

        $total = array('shift_length'       => 0, 'theory_prod_time' => 0, 'fact_prod_time' => 0, 'true_problem_time' => 0,
            'true_problem_count' => 0, 'transition_time' => 0, 'transition_count' => 0,'no_work_time'=>0, 'exploitation_factor' => 0, 'quantity_type1' => 0,'exceeded_transition_time'=>0,
            'quantity_type2'     => 0, 'total_quantity' => 0, 'planned_quantity' => 0, 'quality_factor' => 0,
            'losses_quantity' => array(), 'operational_factor' => 0, 'oee' => 0, 'mttf' => 0, 'mttr' => 0, 'count_delay'=>0, 'theorical_time_in_shift'=>0,
            'planned_stops_duration'=>0, 'planned_stops_quantity'=>0,'non_planned_stops_duration'=>0, 'non_planned_stops_quantity'=>0
        );

        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $total['shift_length_with_exclusions'] = 0;
            $total['oee_with_exclusions'] = 0;
            $total['exploitation_factor_with_exclusions'] = 0;
        }

        $emptyShiftCount = 0;
        $oeeDayCombiner = array();
        foreach ($generatedGlobalData as $single_shift) {
            if(empty($single_shift)) continue;
            $currentRow = $startRow;
            $shiftStartDay = date('Y-m-d',strtotime($single_shift['shift_start']));
            if (!array_key_exists('shift_length', $single_shift)) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra duomenų'));
                $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra duomenų'));
                $emptyShiftCount += 1;
                $currentColumn ++;
                continue;
            }
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['theorical_time_in_shift'],2));
            $total['theorical_time_in_shift'] += $single_shift['theorical_time_in_shift'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['shift_length'],2));
            $total['shift_length'] += $single_shift['shift_length'];
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['shift_length_with_exclusions'],2));
                $total['shift_length_with_exclusions'] += $single_shift['shift_length_with_exclusions'];
            }
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['theory_prod_time']);
            $total['theory_prod_time'] += $single_shift['theory_prod_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['fact_prod_time'],2));
            $total['fact_prod_time'] += $single_shift['fact_prod_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['true_problem_time'],2));
            $total['true_problem_time'] += $single_shift['true_problem_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['true_problem_count']);
            $total['true_problem_count'] += $single_shift['true_problem_count'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['planned_stops_duration']/60,2));
            $total['planned_stops_duration'] += $single_shift['planned_stops_duration'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['planned_stops_quantity']);
            $total['planned_stops_quantity'] += $single_shift['planned_stops_quantity'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['non_planned_stops_duration']/60,2));
            $total['non_planned_stops_duration'] += $single_shift['non_planned_stops_duration'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['non_planned_stops_quantity']);
            $total['non_planned_stops_quantity'] += $single_shift['non_planned_stops_quantity'];

            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['transition_time'],2));
            $total['transition_time'] += $single_shift['transition_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['transition_count']);
            $total['transition_count'] += $single_shift['transition_count'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exceeded_transition_time'],2));
            $total['exceeded_transition_time'] += $single_shift['exceeded_transition_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['no_work_time'],2));
            $total['no_work_time'] += $single_shift['no_work_time'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exploitation_factor'],2));
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exploitation_factor_with_exclusions'],2));
            }
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['operational_factor'],2));
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['oee'],2));
            $total['count_delay'] += $single_shift['count_delay'];
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['oee_with_exclusions'],2));
            }
            if(!isset($oeeDayCombiner[$shiftStartDay])){
                $oeeDayCombiner[$shiftStartDay] = array('data'=>$single_shift, 'columnLetters'=>array($currentColumn), 'rowNumber'=>++ $currentRow);
            }else{
                array_walk($oeeDayCombiner[$shiftStartDay]['data'], function(&$data, $key)use($single_shift){
                    $data = floatval($single_shift[$key])+floatval($data);
                });
                $oeeDayCombiner[$shiftStartDay]['columnLetters'][] = $currentColumn;
                ++$currentRow;
            }
            ++ $currentRow;
            // $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['quantity_type1']);
            // $total['quantity_type1'] += $single_shift['quantity_type1'];
            // $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['quantity_type2']);
            // $total['quantity_type2'] += $single_shift['quantity_type2'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['sensor_quantity']);
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['total_quantity']);
            $total['total_quantity'] += $single_shift['total_quantity'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['shift_length'] > 0?round($single_shift['total_quantity'] / $single_shift['shift_length'],2):0);
            if(isset($single_shift['shift_length_with_exclusions'])){
                if($single_shift['shift_length_with_exclusions'] > 0){
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['total_quantity'] / $single_shift['shift_length_with_exclusions'],2));
                }else{
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, 0);
                }
            }else{++$currentRow;}
            $total['planned_quantity'] += $single_shift['planned_quantity'];
            ++ $currentRow;
            ++ $currentRow;
            foreach ($lossTypes as $lt) {
                if ($single_shift['losses_quantity'] == null || !array_key_exists($lt['LossType']['id'], $single_shift['losses_quantity'])) {
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, "0");
                } else {
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['losses_quantity'][$lt['LossType']['id']][0]['losses']);
                    if (!array_key_exists($lt['LossType']['id'], $total['losses_quantity'])) {
                        $total['losses_quantity'][$lt['LossType']['id']] = 0;
                    }
                    $total['losses_quantity'][$lt['LossType']['id']] += $single_shift['losses_quantity'][$lt['LossType']['id']][0]['losses'];
                }
            }
            ++ $currentRow;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['mttf']);
            $total['mttf'] += $single_shift['mttf'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['mttr']);
            $total['mttr'] += $single_shift['mttr'];

            $currentColumn ++;
        }
        foreach($oeeDayCombiner as $oeeDayCombinerData){
            $oeeParams = $this->Record->calculateOee($oeeDayCombinerData['data']);
            $workSheet->mergeCells(current($oeeDayCombinerData['columnLetters']).$oeeDayCombinerData['rowNumber'].':'.current(array_reverse($oeeDayCombinerData['columnLetters'])).$oeeDayCombinerData['rowNumber']);
            $workSheet->setCellValue(current($oeeDayCombinerData['columnLetters']) . $oeeDayCombinerData['rowNumber'], round($oeeParams['oee_exclusions'],2));
            $workSheet->getStyle(current($oeeDayCombinerData['columnLetters']) . $oeeDayCombinerData['rowNumber'])->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        }
        unset($oeeDayCombiner);
        $validShiftCount = count($generatedGlobalData);

        #region bendri
        $totalColumnsLetters = array('B', $currentColumn);
        foreach($totalColumnsLetters as $totalColumnsLetter) {
            $currentRow = $startRow;
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['theorical_time_in_shift'],2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['shift_length'],2));
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['shift_length_with_exclusions'],2));
            }
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['fact_prod_time'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['true_problem_time'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, $total['true_problem_count']);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['planned_stops_duration']/60,2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, $total['planned_stops_quantity']);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['non_planned_stops_duration']/60,2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, $total['non_planned_stops_quantity']);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['transition_time'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, $total['transition_count']);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['exceeded_transition_time'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['no_work_time'], 2));
            //$workSheet->setCellValue($totalColumnsLetter . ++ $currentRow, round($total['exploitation_factor'] / $validShiftCount, 2));
            $oeeParams = $this->Record->calculateOee($total);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['exploitation_factor'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['exploitation_factor_exclusions'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['operational_factor'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['oee'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['oee_exclusions'], 2));
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($oeeParams['oee_exclusions'], 2));
            ++$currentRow;
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, $total['total_quantity']);
            $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['total_quantity'] / $total['shift_length'], 2));
            if (isset($total['shift_length_with_exclusions']) && $total['shift_length_with_exclusions'] > 0) {
                $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, round($total['total_quantity'] / $total['shift_length_with_exclusions'], 2));
            } else {
                ++$currentRow;
            }
            ++$currentRow;
            ++$currentRow;
            foreach ($lossTypes as $lt) {
                $workSheet->setCellValue($totalColumnsLetter . ++$currentRow, (array_key_exists($lt['LossType']['id'], $total['losses_quantity'])) ? $total['losses_quantity'][$lt['LossType']['id']] : 0);
            }
            unset($oeeParams);
        }
        /*
                ++ $currentRow;
                ++ $currentRow;
                ++ $currentRow;

                $shift['Shift'] = array('start'=>$shifts_start, 'end'=>$shifts_end);
                $found_problems = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,0);
                ++ $currentRow;
                ++ $currentRow;

                $workSheet->setCellValue('A' . $currentRow, __('Prastovos (žurnalas)'));
                $letterStart = 'A'; $currentRow++;
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Darbo centras'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('MO'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio pavadinimas'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio kodas'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Paaiškinimas'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 1'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 2'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 3'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas 4'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Pradžia'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Pabaiga'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
                $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė tarp prastovų (min)'));
                $workSheet->getStyle('A' . ($currentRow - 1) . ':L' . $currentRow)->getFont()->setBold(true);
                        ++ $currentRow;

                        $start_column = 'A';
                        $previous_date = null;

                        $global_mttr = 0;
                        $global_mttf = 0;
                        foreach ($found_problems as $fp) {
                            try{
                                $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
                            }catch(Exeption $e){
                                $approvedAddData = null;
                            }
                            $workSheet->setCellValue($start_column . $currentRow, $fp['Sensor']['name']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_name']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_code']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['DiffCard']['description']);
                            $parsedProblemsTree = $fp['Problem']['id'] > 0 && isset($problemsTreeList[$fp['Problem']['id']])?array_reverse($problemsTreeList[$fp['Problem']['id']]):array();
                            for($i=0; $i<4; $i++){
                                if(isset($parsedProblemsTree[$i])){
                                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($parsedProblemsTree[$i]));
                                }else{
                                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($fp['Problem']['name']));
                                }
                            }
                            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['start']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['end']);
                            $workSheet->setCellValue(++ $start_column . $currentRow, round($fp[0]['found_problem_duration'] / 60,2));

                            $global_mttr += round($fp[0]['found_problem_duration'] / 60 * 10) / 10;
                            if ($previous_date == null && $fp['FoundProblem']['start'] > $shifts_start) {
                                $previous_date = $shifts_start;
                            }
                            if ($previous_date != null) {
                                $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date);
                                $global_mttf += round($date_diff / 60, 2);
                                if ($date_diff < 0) {
                                    $date_diff = 0;
                                }
                            }
                //            pr($date_diff/60);
                            $workSheet->setCellValue(++ $start_column . $currentRow, ($previous_date != null) ? round($date_diff / 60, 2) : "");
                            $previous_date = $fp['FoundProblem']['end'];
                            //$start_column++;
                            $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->applyFromArray(
                                array(
                                    'fill' => array(
                                        'type' => PHPExcel_Style_Fill::FILL_SOLID,
                                        'color' => array('rgb' => isset(self::$colors[$fp['FoundProblem']['problem_id']])?self::$colors[$fp['FoundProblem']['problem_id']]:self::$colors[3])
                                    )
                                )
                            );
                            $start_column = 'A';
                            $currentRow ++;
                            if (end($found_problems) === $fp && $fp['FoundProblem']['end'] < $shifts_end) { // If last, add difference till next shift
                                $global_mttf += round((strtotime($shifts_end) - strtotime($fp['FoundProblem']['end'])) / 60, 2);
                            }
                        }
                        unset($found_problems);
                        if (!empty($found_problems)) {
                            $global_mttf /= count($found_problems);
                            $global_mttr /= count($found_problems);
                        } else {
                            $global_mttf = 0;
                            $global_mttr = 0;
                        }
                        ++ $currentRow;
                        $workSheet->setCellValue('A' . ++ $currentRow, __('MTTR (min)'));
                        $workSheet->setCellValue('B' . $currentRow, round($global_mttr, 2));
                        $workSheet->setCellValue('A' . ++ $currentRow, __('MTTF (min)'));
                        $workSheet->setCellValue('B' . $currentRow, round($global_mttf, 2));
                */
        for ($col = 'A'; $col !== $currentColumn; $col ++) {
            $workSheet->getColumnDimension($col)->setAutoSize(true);
        }
    }

    public function getWeekleXLSData($requestData, $periodDetails){
        $date_start = $requestData['start_date'];
        $date_end = $requestData['end_date'];
        $dateTypes = array('week'=>'W', 'month'=>'m', 'day'=>'z');
        $recordModel = ClassRegistry::init('Record');
        $start = strtotime($date_start);
        $periodsList = array();
        while($start < strtotime($date_end)){
            $periodsList[] = date('Y', $start).'-'.(int)date($dateTypes[$periodDetails['type']], $start);
            $start = strtotime('+1 '.$periodDetails['type'], $start);
        }      
        $conditions = array(
            "CONCAT(DashboardsCalculation.year,'-',DashboardsCalculation.{$periodDetails['type']})" => $periodsList,
        );
        if(isset($requestData['sensors']) && !empty($requestData['sensors'])){
            $conditions['DashboardsCalculation.sensor_id']=$requestData['sensors'];
            $conditions['DashboardsCalculation.day'] = 0;
            $conditions['DashboardsCalculation.month'] = 0;
            $conditions['DashboardsCalculation.week'] = 0;
            if(isset($conditions['DashboardsCalculation.'.$periodDetails['type']])){ unset($conditions['DashboardsCalculation.'.$periodDetails['type']]); }
        }
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'TRIM('.$col.') AS '.$col; });
        $calculation = $this->DashboardsCalculation->find('all', array(
            'fields'=>array_merge($fields, array(
                'CONCAT(DashboardsCalculation.year,\'-\',DashboardsCalculation.'.$periodDetails['type'].', \''.$periodDetails['time_title_mechanism'].'\') AS nr',
            )),
            'conditions' => $conditions,
            //'group'=>array(
            //    'DashboardsCalculation.sensor_id',
            //),
            'order'=>array('DashboardsCalculation.sensor_id', 'DashboardsCalculation.'.$periodDetails['type'])
        ));
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'SUM('.$col.') AS '.$col; });
        if(in_array(20, $conditions['DashboardsCalculation.sensor_id']) && sizeof($conditions['DashboardsCalculation.sensor_id']) > 1){
            unset($conditions['DashboardsCalculation.sensor_id'][array_search(20, $conditions['DashboardsCalculation.sensor_id'])]);
        }
        $summarizedCalculation = $this->DashboardsCalculation->find('all', array(
            'fields'=>array_merge($fields, array(
                'CONCAT(DashboardsCalculation.year,\'-\',DashboardsCalculation.'.$periodDetails['type'].', \''.$periodDetails['time_title_mechanism'].'\') AS nr',
            )),
            'conditions' => $conditions,
            'group' => key($conditions),
            'order'=>array('DashboardsCalculation.'.$periodDetails['type'])
        ));
        foreach($summarizedCalculation as &$calc){
            $oeeParams = $recordModel->calculateOee($calc[0]);
            $calc[0]['exploitation_factor'] = round($oeeParams['exploitation_factor'],2);
            $calc[0]['exploitation_factor_with_exclusions'] = round($oeeParams['exploitation_factor_exclusions'],2);
            $calc[0]['operational_factor'] = round($oeeParams['operational_factor'],2);
            $calc[0]['oee'] = round($oeeParams['oee'],2);
            $calc[0]['oee_with_exclusions'] = round($oeeParams['oee_exclusions'],2);
            $calc[0]['sensor_id'] = 0;
        }
        $calculation = array_merge($calculation, $summarizedCalculation);
        return $calculation;
    }

    private function getIntervalQuantity($data){
        if($data['Sensor']['plan_relate_quantity'] == 'quantity'){ return $data['FoundProblem']['quantity']; }
        $multiplier = 1;
        if(isset($data['ApprovedOrder']) && !empty($data['ApprovedOrder']) && $data['ApprovedOrder']['box_quantity'] > 0){           
            $multiplier = bcdiv($data['ApprovedOrder']['quantity'],$data['ApprovedOrder']['box_quantity'],6);
        }
        return bcmul($data['FoundProblem']['quantity'],$multiplier,2);
    }
}
