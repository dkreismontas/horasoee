<?php
class HanzasMaiznica extends AppModel{
	
	 public $name = 'HanzasMaiznica';
	
	 private static $virtualSensors = array(
         18 => array('cool'=>3, 'pap'=>4, 'kst'=>4),
         19 => array('kst'=>8, 'pap'=>10),
         20 => array('kst'=>9, 'pap'=>11),
         21 => array('kst'=>13, 'apa'=>15, 'cool'=>14, 'pap'=>16),
     );
     
//     public function Record_calculateOee_Hook(&$oeeParams, $data){
//        $oeeParams['operational_factor'] = 1;
//        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
//        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
//     }
     
	 public function Workcenter_beforeReturnUserSensor_Hook(&$fSensor){
        //Jei siuo metu atidarytas vienas is virtualiu sensoriu ir jam bandoma uzdeti plana irasu irasymo metu, reikia stabdyti bet kokia funkcija, keiciant jutikli i neegzsituoajnti
        if(in_array($fSensor['Sensor']['id'], array_keys(self::$virtualSensors))){
            $markerPath = __dir__.sprintf('/../webroot/files/virtual_sensors_update_%d.txt', $fSensor['Sensor']['id']);
            if(file_exists($markerPath)){
                $fSensor['Sensor']['id'] = 0;
            }
        }
     }
	 
	 public function Workcenter_changeUpdateStatusVariables_Hook2(&$data){
        //Jei siuo metu atidarytas vienas is virtualiu sensoriu ir jam rasomi duomenys, visi mygtukai turi buti nematomi operatoriui, nes ji neturi galeti nei pradeti naujo plano nei uzbaigti kol nesubegs visi irasai
        if(in_array($data['sensor']['id'], array_keys(self::$virtualSensors))){
            $markerPath = __dir__.sprintf('/../webroot/files/virtual_sensors_update_%d.txt', $data['sensor']['id']);
            if(file_exists($markerPath) || !$data['sensor']['id']){
                array_walk($data['sensor'], function(&$value,$key){ if(preg_match('/.+button/i', $key)){ $value = 0; } });
                $data['sensor']['has_access_to_register_events'] = false;
                $data['event'] = new TimelineEvent(TimelineEvent::TYPE_NONE, null, null, null);
            }
        }
        if(!$data['sensor']['time_interval_declare_count_on_finish']){
            $data['lossTypes'] = array();
        }
     }

	public function addQuantitiesToVirtualSensors($date,$activePlans){
        static $recordModel, $foundProblemModel,$logModel,$sensorModel = null, $db = null;
        if($recordModel == null){
        	 $recordModel = ClassRegistry::init('Record'); 
        	 $foundProblemModel = ClassRegistry::init('FoundProblem'); 
			 $logModel = ClassRegistry::init('Log');
			 $sensorModel = ClassRegistry::init('Sensor');
		}
        if($db == null){ $db = ConnectionManager::getDataSource('default'); }
		
		foreach(self::$virtualSensors as $virtualSensorId => $dataFrom){
	        if(isset($activePlans[$virtualSensorId])){
	            $markerPath = __dir__.'/../webroot/files/virtual_sensors_update_'.$virtualSensorId.'.txt';
	            if(!file_exists($markerPath) || time() - filemtime($markerPath) >  Configure::read('recordsCycle') * 3){
	                $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
					$fSensor = $sensorModel->findById($virtualSensorId);
	                $lastRecord = $recordModel->find('first', array('conditions'=>array('Record.sensor_id'=>$virtualSensorId),'order'=>array('Record.id DESC')));
	                $lastRecordDate = !empty($lastRecord)?$lastRecord['Record']['created']:date('Y-m-d H:i:s', strtotime('-1 day'));
	                $planCreateType = strtolower(trim($activePlans[$virtualSensorId]['crate_type']));
	                $sensorIdToGetDataFrom = isset($dataFrom[$planCreateType])?$dataFrom[$planCreateType]:0;
					if(!$sensorIdToGetDataFrom){ continue; }
                    $currentFoundProblem = $foundProblemModel->find('first', array('conditions'=>array(
                        'FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS,
                        'FoundProblem.sensor_id'=>$virtualSensorId,
                        'FoundProblem.start >'=>$lastRecordDate,
                    )));
                    if(!empty($currentFoundProblem)){
                        $logModel->write('Pradedami rasyti atsilike virtualaus jutiklio duomenys nuo '.$lastRecordDate, $fSensor);
                        $logModel->write('Dirbtinai sustabdoma si aktyvi prastova, nes ji sukurta veliau nei pradedami rasyti irasai. Prastova gali buti aktyvuota, kai irasai virsys prastovos pradzia: '.json_encode($currentFoundProblem), $fSensor);
                        $foundProblemModel->findAndComplete($fSensor);
                    }
                    $laterProblems = $foundProblemModel->find('all',array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.start >'=>$lastRecordDate),'order'=>array('FoundProblem.start')));
//                    if(!empty($laterProblems)){
//                        $logModel->write('Is sistemos pasalinamos sios prastovos, nes jos sukurtos veliau, nei bus rasomi irasai: '.json_encode($laterProblems), $fSensor);
//                        $foundProblemModel->deleteAll(array('FoundProblem.id'=>Set::extract('/FoundProblem/id', $laterProblems)));
//                    }
	                $newRecords = $recordModel->find('all', array(
	                    'fields'=>array(
	                        'TRIM(Record.quantity) AS quantity',
	                        'TRIM(Record.created) AS created'
	                    ),'conditions'=>array(
	                        'Record.created >'=>$lastRecordDate,
	                        'Record.sensor_id'=>$sensorIdToGetDataFrom,  
	                    ))
	                );
                    $firstUnknownActiveProblem = array();
	                foreach($newRecords as $newRecord){ $newRecord = $newRecord[0];
	                    $oldFoundProblemToReenable = array_filter($laterProblems, function($foundProblem)use($newRecord){
                            return strtotime($foundProblem['FoundProblem']['start']) <= strtotime($newRecord['created']);
                        });
	                    if(!empty($oldFoundProblemToReenable)){
	                        unset($laterProblems[key($oldFoundProblemToReenable)]);
                            $updateFields = array('state'=>1);
                            $currentActiveFProblem = $foundProblemModel->findCurrent($fSensor['Sensor']['id']);
                            if(!empty($currentActiveFProblem)){
                                $updateFields['start'] = '\''.date('Y-m-d H:i:s',strtotime($currentActiveFProblem['FoundProblem']['end'])+1).'\'';
                                $logModel->write('Duomenu rasymo virtualizacija: uzdaroma aktyvi prastova: '.json_encode($currentActiveFProblem), $fSensor);
                                $foundProblemModel->findAndComplete($fSensor);
                            }
                            $logModel->write('Duomenu rasymo virtualizacija: Atidaroma uzdaryta prastova:'.json_encode($oldFoundProblemToReenable).' '.json_encode($updateFields), $fSensor);
                            $foundProblemModel->updateAll($updateFields, array('FoundProblem.id'=>current($oldFoundProblemToReenable)['FoundProblem']['id']));
                        }
                        $db->query('CALL ADD_FULL_RECORD(\''.$newRecord['created'].'\', '.$virtualSensorId.', '.$newRecord['quantity'].', '.Configure::read('recordsCycle').'); ');
						touch($markerPath);
	                    /*if(!empty($currentFoundProblem) && strtotime($newRecord['created']) >= strtotime($currentFoundProblem['FoundProblem']['start'])){
							$foundProblemModel->findAndComplete($fSensor);
							$foundProblemModel->updateAll(array('state'=>1, 'start'=>'\''.date('Y-m-d H:i:s',strtotime($newRecord['created'])+1).'\''), array('FoundProblem.id'=>$currentFoundProblem['FoundProblem']['id']));
							$problemsWithoutPlan = $foundProblemModel->find('all',array('conditions'=>array('FoundProblem.sensor_id'=>$virtualSensorId, 'FoundProblem.start >='=>$lastRecordDate, 'FoundProblem.end <='=>$newRecord['created'])));
							if(!empty($problemsWithoutPlan)){
								$logModel->write('Nuimamas planas siems found_problems irasams, kurie ejo iki plano pradzios: '.json_encode($problemsWithoutPlan), $fSensor);
								$foundProblemModel->updateAll(array('plan_id'=>null, 'approved_order_id'=>null), array('FoundProblem.id'=>Set::extract('/FoundProblem/id',$problemsWithoutPlan)));
							}
							$logModel->write('Uzdaroma esama ir aktyvuojama buvusi aktyvi prastova pries irasu irasyma: '.json_encode($currentFoundProblem), $fSensor);
							$currentFoundProblem = array();
						}else{
	                        //pradedant rasyti irasus paskutine buvusi prastova turetu dazniausiai buti "Nera darbo", taciau ji uzdaroma su derinimu pradedant gamyba. Nera darbo prastova yra pasalinama sioje funkcijoje (auksciau), nes yra sukurta veliau, nei paskutinis recordas.
                            //Bandysime pirmai sukurtai "Nepazymeta prastova" prastovai uzdeti paskutine prastova, kuri buvo sukurta iki pradedant rasyti irasams
                            if(empty($firstUnknownActiveProblem) && !empty($laterProblems)){
                                $firstUnknownActiveProblem = $foundProblemModel->find('first', array('conditions'=>array(
                                    'FoundProblem.state'=>FoundProblem::STATE_IN_PROGRESS,
                                    'FoundProblem.sensor_id'=>$virtualSensorId,
                                    'FoundProblem.start >'=>$lastRecordDate,
                                    'FoundProblem.problem_id'=>Problem::ID_NOT_DEFINED,
                                )));
                                if(!empty($firstUnknownActiveProblem)){
                                    $logModel->write('Pirmai sukurtai Nepazymetai prastovai, kurios id: '.$firstUnknownActiveProblem['FoundProblem']['id'].', uzdedamas problem_id pagal pirma prastova, kuri buvo sukurta veliau nei paskutinis sustojes recordas. '.json_encode($laterProblems), $fSensor);
                                    $problemId = current($laterProblems)['FoundProblem']['problem_id'];
                                    $foundProblemModel->updateAll(array('problem_id'=>$problemId, 'super_parent_problem_id'=>$problemId), array('FoundProblem.id'=>$firstUnknownActiveProblem['FoundProblem']['id']));
                                }
                            }
                        }*/
	                }
	            }
                if(isset($fh)){ fclose($fh);}
                if(file_exists($markerPath)){ unlink($markerPath); }
	        }
	    }
    }

    public function Cron_BeforeImportPlanSave_Hook(&$plan){
        $resetBoxQuantity = false;
        switch($plan['sensor_id']){
            case 18:
                if(in_array(strtolower($plan['crate_type']),array('cool','pap','kst'))){
                    $resetBoxQuantity = true;
                }
            break;
            case 19:
            case 20:
                if(in_array(strtolower($plan['crate_type']),array('pap'))){
                    $resetBoxQuantity = true;
                }
            break;
            case 21:
                if(in_array(strtolower($plan['crate_type']),array('pap','apa','cool'))){
                    $resetBoxQuantity = true;
                }
            break;
        }
        if($resetBoxQuantity){
            $plan['box_quantity'] = 1;
        }
    }
    
    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
        $this->calculateHoursInfo($dcHoursInfoBlocks, $hours, $fSensor);
        if($fSensor['Sensor']['type'] == 1){
            $fSensor['Sensor']['declare_quantities_label'] = __('Kiekis kubilais');
        }elseif($fSensor['Sensor']['type'] == 3){
            $fSensor['Sensor']['declare_quantities_label'] = __('Kiekis vienetais');
        }

    }
    
    public function calculateHoursInfo(&$dcHoursInfoBlocks, $hours, $fSensor){
        //Planuotas kiekis (vnt.) ir Pagamintas kiekis (vnt.)
        if(empty($hours)){return;}
        $recordModel = ClassRegistry::init('Record');
        $founProblemModel = ClassRegistry::init('FoundProblem');
        $hoursToWorkOn = Set::extract('/value', $hours);
        $recordModel->bindModel(array('belongsTo'=>array('Plan','FoundProblem')));
        $lastFullHour = array_reverse($hours);
        $firstDate = current($hours)['date'].' '.sprintf('%01d',current($hours)['value']).':00';
        $lastDate = current($lastFullHour)['date'].' '.sprintf('%01d',current($lastFullHour)['value']).':00';
        if(strtotime('+1 hour',strtotime($lastDate)) <= time()){
            $lastDate = date('Y-m-d H:i', strtotime('+1 hour',strtotime($lastDate)));
        }
        $quantityField = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        $planCountData = $recordModel->find('all',array(
            'fields'=>array(
                'SUM(IF((Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL) OR (FoundProblem.problem_id >= 3),' . (Configure::read('recordsCycle')) . ',0)) AS work_problem_time',
                'SUM(IF(FoundProblem.problem_id = 1,' . (Configure::read('recordsCycle')) . ',0)) AS fact_transition_time',
                'GROUP_CONCAT(DISTINCT(IF(FoundProblem.problem_id = 1, FoundProblem.id, 0))) AS transition_problem_id',
                'MAX(IF(FoundProblem.problem_id = 1, Record.created, \'0000-00-00\')) AS transition_end',
                'SUM(Record.'.$quantityField.') AS fact_quantity',
                'IF(Record.approved_order_id > 0,Plan.step / 60,0) AS step',
                'Record.approved_order_id',
                'Plan.preparation_time',
                'DATE_FORMAT(Record.created, \'%H\') AS hour'
            ),'conditions'=>array(
                'Record.created >='=>$firstDate,
                'Record.created <'=>$lastDate,
                'Record.sensor_id'=>$fSensor['Sensor']['id']
            ),'group'=>array(
                'DATE_FORMAT(Record.created, \'%Y-%m-%d %H\')',
                'Record.approved_order_id'
            )
        ));
        $plannedHoursQuantity = $factHoursQuantity = array();
        foreach($planCountData as $data){
            if(!$data['Record']['approved_order_id']){ continue; }
            $currentHour = (int)$data[0]['hour'];             
            $transitionOverflow = 0;
            $transitionsProblemId = array_filter(explode(',', $data[0]['transition_problem_id']));          
            if(!empty($transitionsProblemId)){
                sort($transitionsProblemId);
                $transitionProblem = $founProblemModel->findById(current($transitionsProblemId)); //imame pirma nari, nes vienas uzsakymas gali tureti tik viena derinima               
                if(!empty($transitionProblem)){
                    $factTransitionDuration = strtotime($data[0]['transition_end']) - strtotime($transitionProblem['FoundProblem']['start']);
                    $transitionOverflow = max(0, $factTransitionDuration - $data['Plan']['preparation_time']*60);
                    if($transitionOverflow > $data[0]['fact_transition_time']){
                        $transitionOverflow = min($data[0]['fact_transition_time'], $transitionOverflow);
                    }                
                }   
            }          
            if(!isset($plannedHoursQuantity[$currentHour])){ $plannedHoursQuantity[$currentHour] = 0; }
            if(!isset($factHoursQuantity[$currentHour])){ $factHoursQuantity[$currentHour] = 0; }
            $plannedHoursQuantity[$currentHour] += bcmul(bcdiv($data[0]['work_problem_time']+$transitionOverflow,60,6), $data[0]['step'], 6);
            $factHoursQuantity[$currentHour] += round($data[0]['fact_quantity']);           
        }
        array_walk($plannedHoursQuantity, function(&$data){ $data = round($data); });
        $dcHoursInfoBlocks['plan_count']['value'] = $plannedHoursQuantity;
        $dcHoursInfoBlocks['fact_count']['value'] = $factHoursQuantity;
        
        //Pamainos atsilikimas nuo plano (min.)
        $delayTimes = $this->getHourFactFromPlanDelayTimes($fSensor, strtotime($firstDate), strtotime($lastDate), 'shift');
        $dcHoursInfoBlocks['delay_in_shift']['value'] = $delayTimes;
        $delayTimes = $this->getHourFactFromPlanDelayTimes($fSensor, strtotime($firstDate), strtotime($lastDate), 'day');
        $dcHoursInfoBlocks['delay_in_day']['value'] = $delayTimes;
    }

    private function getHourFactFromPlanDelayTimes($fSensor, $currentHour, $lastHour, $startPointType){
        static $shiftModel=null,$planModel=null,$recordModel=null;
        $shiftModel = $shiftModel===null?ClassRegistry::init('Shift'):$shiftModel;
        $planModel = $planModel===null?ClassRegistry::init('Plan'):$planModel;
        $recordModel = $recordModel===null?ClassRegistry::init('Record'):$recordModel;
        $hoursDelayList = array();
        $quantityField = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        while($currentHour < $lastHour){
            $currentHour += 3600;
            $currentShift = $shiftModel->find('first',array('conditions'=>array('Shift.branch_id'=>$fSensor['Sensor']['branch_id'], 'Shift.start <='=>date('Y-m-d H:i:s', $currentHour-60), 'Shift.end >'=>date('Y-m-d H:i:s', $currentHour-60))));
            if(!$currentShift) continue;
            if($startPointType == 'shift'){//skaiciuojama satsilikimas nuo pamainos, kuri yra esamoje valandoje
                $startPoint = $currentShift['Shift']['start'];
            }else{//skaiciuojama satsilikimas nuo paros, kuri yra esamoje valandoje
                //jei esamos pamainos pradzia yra vienoje dienoje, o pabaiga kitoje, vadinasi esame naktineje pamainoje, todel para prasideda praejuios dienos pirmoje pamainoje
                $dayStart = date('d',strtotime($currentShift['Shift']['start'])) != date('d',strtotime($currentShift['Shift']['end']))?date('Y-m-d',strtotime($currentShift['Shift']['start'])) : date('Y-m-d',strtotime($currentShift['Shift']['end']));
                $dayStartShift = $shiftModel->find('first',array('conditions'=>array('Shift.branch_id'=>$fSensor['Sensor']['branch_id'], 'Shift.start >='=>$dayStart)));
                $startPoint = $dayStartShift['Shift']['start'];
            }
            $plans = $planModel->find('all', array(
                'conditions'=>array(
                    'Plan.start <='=>date('Y-m-d H:i:s', $currentHour),
                    'Plan.end >='=>$startPoint,
                    'Plan.sensor_id'=>$fSensor['Sensor']['id']
                )
            ));
            if(empty($plans)){ continue; }
            $recordModel->bindModel(array('belongsTo'=>array('Plan')));
            $factProduction = $recordModel->find('all', array(
                'fields'=>array(
                    'SUM(Record.'.$quantityField.') AS quantity',
                    'TRIM(Record.plan_id) AS plan_id',
                    'TRIM(Plan.step) AS step',
                ),'conditions'=>array(
                    'Record.sensor_id'=>$fSensor['Sensor']['id'],
                    'Record.created >='=>$startPoint,
                    'Record.created <'=>date('Y-m-d H:i:s', $currentHour)
                ),'group'=>array('Record.plan_id')
            ));
            $plansIdsList = Set::extract('/Plan/id', $plans);
            $factsMadeNotByPlan = array_filter($factProduction, function($data)use($plansIdsList){ return !in_array($data[0]['plan_id'], $plansIdsList); });
            $timeDelay = 0;
            foreach($factsMadeNotByPlan as $factMadeNotByPlan){
                if(!floatval($factMadeNotByPlan[0]['step']))continue;
                $timeDelay = bcadd($timeDelay, bcdiv($factMadeNotByPlan[0]['quantity'],bcdiv($factMadeNotByPlan[0]['step'],60,6),6), 6);
            }
            foreach($plans as $plan){
                $step = floatval(bcdiv($plan['Plan']['step'],60,6));//kiek vnt pagamina per minute
                $planStart = max(strtotime($currentShift['Shift']['start']), strtotime($plan['Plan']['start']));
                $planEnd = min($currentHour, strtotime($plan['Plan']['end']));
                $factData = array_filter($factProduction, function($data)use($plan){ return $plan['Plan']['id'] == $data[0]['plan_id']; });
                $factCount = !empty($factData)?current($factData)[0]['quantity']:0;
                $planCount = $step>0?bcmul(bcdiv(($planEnd - $planStart),60,6), $step, 6):0;
                $timeDelay = $step>0?bcadd($timeDelay, bcdiv(bcsub(min($factCount, $planCount), $planCount, 6), $step, 6), 6):0; //sumuojame bendra kiekvieno plano fakta nuo teorijos neatitikima minutemis
/*fb(bcdiv(($planEnd - $planStart),60,6),'trukme');                
 fb($step,'$step');                
 fb($factCount,'faktinis kiekis');                
 fb($planCount,'planuotas kiekis');                
 fb($timeDelay, 'bendras atsilikimas');
*/
            }
            $hoursDelayList[(int)date('H', $currentHour-3600)] = round($timeDelay);
        }
        return $hoursDelayList;
    }

	public function ApprovedOrdersIndexHook(&$controller){
        $controller->PartialQuantity->virtualFields = array('quantity_sum'=>'SUM(PartialQuantity.quantity)');
        $partialQuantities = $controller->PartialQuantity->find('list', array(
            'fields' => array('PartialQuantity.approved_order_id','quantity_sum'),
            'conditions' => array('PartialQuantity.approved_order_id' =>  Set::extract('/ApprovedOrder/id', $controller->viewVars['list'])),
            'group'      => array('PartialQuantity.approved_order_id')
        ));
        $controller->PartialQuantity->virtualFields = array();
        foreach($controller->viewVars['list'] as &$list){
            $appOrderId = $list['ApprovedOrder']['id'];
            $list['ApprovedOrder']['partial_quantities_sum'] = isset($partialQuantities[$appOrderId])?$partialQuantities[$appOrderId]:0;
        }
        //$controller->viewVars['sensorOptions'] = array_diff_key($controller->viewVars['sensorOptions'], array_flip(array(3,4,10,11,8,9,15,16,14,13)));
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/index');
    }

    public function Sensor_afterGetAsSelectOptions_Hook(&$sensorsList){
        $sensorsList = array_diff_key($sensorsList, array_flip(array(3,4,10,11,8,9,15,16,14,13)));
    }

	public function ApprovedOrdersEditHook(&$controller){
        if(!empty($controller->request->data) && !isset($controller->request->data['Sensor'])){//vykdomas uzsakymo patvirtinimas
            $id = $controller->request->data['ApprovedOrder']['id'];
            $item = $controller->ApprovedOrder->withRefs()->findById($id);
            $shiftModel = ClassRegistry::init('Shift');
            $currentTime = date('Y-m-d H:i:s');
            $currentShift = $shiftModel->find('first', array('conditions'=>array('Shift.start <='=>$currentTime, 'Shift.end >='=>$currentTime, 'Shift.branch_id'=>$item['Branch']['id'])));
            if($currentShift){
                $updateData = array('confirmation_shift_id'=>$currentShift['Shift']['id']);
                //papildomai is records irasu ieskome ar uzsakymas buvo gamintas kitoje pamainoje nei vykdomas patvirtinimas. Jei buvo, tuomet turi buti daliniu kiekiu is partial_quantities lenteles tai pamainai. Jei nera, vesime pranesima
                $recordModel = ClassRegistry::init('Record');
                $prevShiftsIdsList = $recordModel->find('list', array(
                    'fields'=>array('Record.shift_id'),
                    'conditions'=>array(
                        'Record.sensor_id'=>$item['ApprovedOrder']['sensor_id'],
                        'Record.shift_id <>'=>$currentShift['Shift']['id'],
                        'Record.approved_order_id'=>$id,
                    ),'group'=>array('Record.shift_id')
                ));
                if(!empty($prevShiftsIdsList)){
                    $partialQuantities = $controller->PartialQuantity->find('all', array(
                        'conditions'=>array(
                            'PartialQuantity.sensor_id'=>$item['ApprovedOrder']['sensor_id'],
                            'PartialQuantity.shift_id'=>$prevShiftsIdsList,
                            'PartialQuantity.approved_order_id'=>$id,
                        )
                    ));
                    if(empty($partialQuantities)){
                        $updateData['prev_shift_no_quantities'] = 1;
                    }
                }
                $controller->ApprovedOrder->updateAll($updateData, array('ApprovedOrder.id'=>$id));
            }
        }
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/edit');
    }

	public function ApprovedOrders_Edit_AfterSave_Hook(&$approvedOrdersController, &$item, &$lossItems){
        if($item['ApprovedOrder']['status_id'] == 4 || $item['ApprovedOrder']['status_id'] == 6) {
            $approvedOrdersController->Log->write(__('1. Patvirtintas atidetas arba dalinai uzbaigtas užsakymas su duomenimis').': '.json_encode($item));
            Exporter::exportPostphoned($item);
        } else {
            if($item['Sensor']['type'] != 1) {
                if($item['Sensor']['type'] == 3) {
                    $related_export_item = $approvedOrdersController->ApprovedOrder->withRefs()->find('first',array(
                        'conditions'=>array(
                            'Plan.mo_number'=>$item['Plan']['mo_number'],
                            'Sensor.type'=>1,
                            'ApprovedOrder.confirmed'=>true
                        )
                    ));
                    if($related_export_item != null && !empty($related_export_item)) {
                        $approvedOrdersController->Log->write(__('2. Patvirtintas užsakymas su duomenimis').': '.json_encode($item));
                        Exporter::exportCompleted($related_export_item, 'xml');
                    }
                }
                Exporter::exportCompleted($item);
            }
        }
        $hasLoss = false;
        $losses = array();
        foreach ($lossItems as $loss) {
            $approvedOrdersController->Log->write('Loss Found : ' .$loss['LossType']['code'] ." amount: " . $loss['Loss']['value']);
            if(!array_key_exists($loss['LossType']['code'],$losses)) {
                $losses[$loss['LossType']['code']] = 0;
            }
            $losses[$loss['LossType']['code']] += $loss['Loss']['value'];
        }
        $approvedOrdersController->Log->write("Total losses: " . json_encode($losses).";");
        $index = 0;
        foreach($losses as $key=>$loss) {
            Exporter::exportLosses($item,$key,$loss,$index);
            $index++;
        }
    }

	public function ApprovedOrders_Index_BeforeSearch_Hook(&$searchParameters, &$request, &$ApprovedOrderModel){
        $userModel = ClassRegistry::init('User');
        $userliveData = $userModel->findById(Configure::read('user')->id);
        if(Configure::read('user')->sensor_id > 0){
            $searchParameters['conditions']['ApprovedOrder.confirmed'] = 0;
            $searchParameters['conditions']['ApprovedOrder.status_id'] = ApprovedOrder::STATE_COMPLETE;
            $searchParameters['conditions']['ApprovedOrder.sensor_id'] = $userliveData['User']['active_sensor_id'] > 0?$userliveData['User']['active_sensor_id']:Configure::read('user')->sensor_id;
        }
    }

    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../HanzasMaiznica/css/HanzasMaiznica_bare.css';
    }
    
    public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
        $settingsModel = ClassRegistry::init('Settings');
        $testDowntimeId = $settingsModel->getOne('test_production_downtime_id');
        if(!$testDowntimeId){ return null; }
        $planModel = ClassRegistry::init('Plan');
        $planModel->bindModel(array('belongsTo'=>array('Product')));
        $plan = $planModel->findById($appOrder['ApprovedOrder']['plan_id']);
        $planName = isset($plan['Product']['name']) && trim($plan['Product']['name'])?$plan['Product']['name']:$plan['Plan']['production_name'];
        if(!preg_match('/^test.+/i',trim($planName))){ return null; }
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $logModel = ClassRegistry::init('Log');
        $shiftModel = ClassRegistry::init('Shift');
        $recordModel = ClassRegistry::init('Record');
        $sensorModel = ClassRegistry::init('Sensor');   
        $foundProblemModel->deleteAll(array('FoundProblem.end >'=>$appOrder['ApprovedOrder']['created'], 'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']));
        $fShift = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $start = new DateTime($appOrder['ApprovedOrder']['created']);
        $quantity = $recordModel->find('first', array('fields'=>array('SUM(Record.quantity) AS quantity_sum'), 'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$start->format('Y-m-d H:i:s'))));
        $quantity = isset($quantity[0]['quantity_sum'])?$quantity[0]['quantity_sum']:0;
        $currentProblem = $foundProblemModel->newItem($start, new DateTime(),
                            $testDowntimeId, $fSensor[Sensor::NAME]['id'], $fShift[Shift::NAME]['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $plan[Plan::NAME]['id'], array(), $quantity);
        $recordModel->updateAll(
            array('Record.found_problem_id'=>$currentProblem['FoundProblem']['id']), 
            array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.created >='=>$currentProblem['FoundProblem']['start'])
        );
        $sensorModel->updateAll(
            array('Sensor.tmp_prod_starts_on'=>$fSensor['Sensor']['prod_starts_on'], 'Sensor.prod_starts_on'=>9999),
            array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
        );
    }

    public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
        if($fSensor['Sensor']['prod_starts_on'] >= 9999){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(
                array('Sensor.prod_starts_on' => $fSensor['Sensor']['tmp_prod_starts_on'], 'Sensor.tmp_prod_starts_on'=>0),
                array('Sensor.id'=>$fSensor[Sensor::NAME]['id'])
            );
        }
    }
    
    public function WorkCenter_beforeDetectFullRecordsEvents_Hook(&$fSensor,&$fFoundProblem,&$currentPlan){
        if(isset($fFoundProblem['FoundProblem']['id']) && $fFoundProblem['FoundProblem']['id'] == 57){
            sleep(10);
        }
    }

	public function WorkCenter_BuildStep_Hook(&$fPlan, &$fProduct, $fSensor){
		if(isset($fProduct['Product']['crate_type'])){
			$fPlan['crate_type'] = $fProduct['Product']['crate_type'];
		}
	}

	public function WorkCenter24_BeforeRecordSearch_Hook(&$searchQuery){
        $searchQuery['order'] = array('FIELD(Record.sensor_id, 2, 18, 5, 6, 12, 21, 7, 19, 20)','Record.created');
    }

    //deklaruotas kiekis yra paciame darbo centre paspaudus mygtuka "Deklaruoti kieki" i forma ivestas kiekis
    //Patvirtintas kiekis yra per "Uzsakymu statusas" Meniu redaguojamas konkretus approvedOrder ir ten ivestas kiekis. Po ivedimo uzsakymas igauna statusa: patvirtinta  (confirmed=1)
    public function Oeeshell_filterGetFactTimeRecords_Hook(&$approvedOrdersData, $currentShift, &$records, &$returnData){
        if($returnData) return; //jei OEE skaiciuojamas realiu laiku, perskaciuoti kiekiu nereikia
        //nuresetinamas teorinio laiko paskaiciavimas, kadangi jis apsakiciuojamas pagal formule veliau
        foreach($records['sensors'] as $sensorId => &$recordData){
            $recordData['theory_prod_time'] = 0;
        }
        //Susirandame sioje pamainoje uzbaigtus uzsakymus, kuriu nera tarp rekordu, pvz jei uzsakymas pradetas ir uzbaigtas per trumpa laika, jo recordai nefiksuoja
        $ordersIdsList = array_filter(Set::extract('/Record/approved_order_id', $approvedOrdersData));
        $orderModel = ClassRegistry::init('ApprovedOrder');
        $orderModel->bindModel(array('belongsTo'=>array('Sensor','Plan')));
        $ordersNotInList = $orderModel->find('all', array('conditions'=>array(
            'ApprovedOrder.end <='=>$currentShift['Shift']['end'],
            'ApprovedOrder.end >='=>$currentShift['Shift']['start'],
            'ApprovedOrder.id NOT'=>$ordersIdsList,
            'Sensor.branch_id'=>$currentShift['Shift']['branch_id'],
        )));
        foreach($ordersNotInList as $orderNotInList){
            if(empty($ordersIdsList)){ break; }
            $orderTmp = current($approvedOrdersData);
            $orderTmp['Record']['approved_order_id'] = $orderNotInList['ApprovedOrder']['id'];
            $orderTmp['Record']['sensor_id'] = $orderNotInList['Sensor']['id'];
            $orderTmp['Plan'] = $orderNotInList['Plan'];
            $orderTmp['Sensor'] = $orderNotInList['Sensor'];
            array_walk($orderTmp[0], function(&$value){ $value = 0; });
            $orderTmp[0]['start'] = $orderNotInList['ApprovedOrder']['created'];
            $orderTmp[0]['end'] = $orderNotInList['ApprovedOrder']['end'];
            $orderTmp[0]['total_time'] = strtotime($orderTmp[0]['end']) - strtotime($orderTmp[0]['start']);
            $approvedOrdersData[] = $orderTmp;
        }
        foreach($approvedOrdersData as &$approvedOrderData){
            $approvedOrderData[0]['sensor_quantity'] = $approvedOrderData[0]['total_quantity'];
            if(!$approvedOrderData['Record']['approved_order_id']) continue;
            $approvedOrderData[0]['total_quantity'] = $this->getOrderCountInShift($approvedOrderData['Record']['approved_order_id'], $currentShift['Shift'], $approvedOrderData['Sensor']['type'], $approvedOrderData['Plan']['id']);
            $approvedOrderData[0]['theory_prod_time'] = $this->getTheoryTime($approvedOrderData[0]['total_quantity'], $approvedOrderData['Plan']['step']);
        }
    }

    public function Oeeshell_beforeSearchGetFactTime_Hook(&$additional_fields, &$recordBindModels){
        $recordBindModels['belongsTo']['SuperParentProblem'] = array('className'=>'Problem', 'foreignKey'=>false, 'conditions'=>array('SuperParentProblem.id = FoundProblem.super_parent_problem_id'));
        $additional_fields[] = 'SUM(IF(POSITION(LOWER("non planned stop") IN SuperParentProblem.name) > 0, ' . Configure::read('recordsCycle') . ', 0)) AS non_planned_stops_duration';
        $additional_fields[] = 'SUM(IF(POSITION(LOWER("]planned stop") IN SuperParentProblem.name) > 0, ' . Configure::read('recordsCycle') . ', 0)) AS planned_stops_duration';
        $additional_fields[] = 'GROUP_CONCAT(DISTINCT CASE WHEN POSITION(LOWER("non planned stop") IN SuperParentProblem.name) > 0 THEN FoundProblem.id END ) AS non_planned_stops_quantity';
        $additional_fields[] = 'GROUP_CONCAT(DISTINCT CASE WHEN POSITION(LOWER("]planned stop") IN SuperParentProblem.name) > 0 THEN FoundProblem.id END ) AS planned_stops_quantity';
    }

    public function Oeeshell_appendGetFactTimeLoop_Hook(&$records, &$val, $sensor_id){
        //return false;
        if(!isset($records['sensors'][$sensor_id]['sensor_quantity'])){
            $records['sensors'][$sensor_id]['sensor_quantity'] = 0;
            $records['sensors'][$sensor_id]['non_planned_stops_duration'] = 0;
            $records['sensors'][$sensor_id]['non_planned_stops_quantity'] = 0;
            $records['sensors'][$sensor_id]['planned_stops_duration'] = 0;
            $records['sensors'][$sensor_id]['planned_stops_quantity'] = 0;
        }
        $records['sensors'][$sensor_id]['non_planned_stops_duration'] += isset($val[0]['non_planned_stops_duration'])?$val[0]['non_planned_stops_duration']:0;
        $records['sensors'][$sensor_id]['planned_stops_duration'] += isset($val[0]['planned_stops_duration'])?$val[0]['planned_stops_duration']:0;
        $records['sensors'][$sensor_id]['non_planned_stops_quantity'] += isset($val[0]['non_planned_stops_quantity'])? sizeof(explode(',',$val[0]['non_planned_stops_quantity'])):0;
        $records['sensors'][$sensor_id]['planned_stops_quantity'] += isset($val[0]['planned_stops_quantity'])? sizeof(explode(',',$val[0]['planned_stops_quantity'])):0;
        $records['sensors'][$sensor_id]['sensor_quantity'] += isset($val[0]['sensor_quantity'])?$val[0]['sensor_quantity']:0;
    }

    //grazinima teorine plano trukme
    public function getTheoryTime($count, $planStep){
        if($planStep > 0){
            return $count * 60 / $planStep;
        }else return 0;
    }

    //grazinamas atitinkamo uzsakymo pagamintas kiekis nurodytoje pamainoje. jei uzsakymo jutiklis yra maisymo tipo, papildomai kiekis perskaiciuojamas per formule
    public function getOrderCountInShift($orderId, $shift, $sensorType, $planId){
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $planModel = ClassRegistry::init('Plan');
        $shiftModel = ClassRegistry::init('Shift');
        $approvedOrderModel->bindModel(array('hasMany'=>array('PartialQuantity')));
        $shiftId = $shift['id'];
        $approvedOrder = $approvedOrderModel->findById($orderId);
        if(empty($approvedOrder)) return 0;
        $count = 0;
        $partialQuantitiesInCurrentShift = array_filter($approvedOrder['PartialQuantity'], function($partial)use($shiftId){
            return $partial['shift_id'] == $shiftId;
        });
        $shiftOrderEnd = null;
        if($approvedOrder['ApprovedOrder']['confirmed']){
            $shiftOrderEnd = $shiftModel->find('first', array('conditions'=>array(
                'Shift.start <='=>$approvedOrder['ApprovedOrder']['end'],
                'Shift.end >'=>$approvedOrder['ApprovedOrder']['end'],
                'Shift.branch_id'=>$shift['branch_id'],
            )));
        }
        if(in_array($approvedOrder['ApprovedOrder']['sensor_id'], array(19,20))){ //3. Teorinio gamybos laiko apskaičiavimas A5 1 Pack (ID19) turi būti atliekamas tik iš patvirtintų kiekių ir atiduodamas tai pamainai, kada uzsakymas uzbaigtas
            if($approvedOrder['ApprovedOrder']['confirmed'] && !empty($shiftOrderEnd) && $shiftOrderEnd['Shift']['id'] == $shiftId){
                return $approvedOrder['ApprovedOrder']['confirmed_quantity'];
            }else{
                return 0;
            }
        }
        if(!$approvedOrder['ApprovedOrder']['confirmed'] || !$shiftOrderEnd || $shiftOrderEnd['Shift']['id'] != $shiftId){
            $count = !empty($partialQuantitiesInCurrentShift)?Set::extract('/quantity', $partialQuantitiesInCurrentShift):0;
            return is_array($count) && !empty($count)?array_sum($count):$count;
        }else{
            $partialQuantitiesInOtherShifts = array_filter($approvedOrder['PartialQuantity'], function($partial)use($shiftId){
                return $partial['shift_id'] != $shiftId;
            });
            $countByPartial = !empty($partialQuantitiesInOtherShifts)?Set::extract('/quantity', $partialQuantitiesInOtherShifts):0;
            $countByPartial = is_array($countByPartial) && !empty($countByPartial)?array_sum($countByPartial):$countByPartial;
            $count = bcsub($approvedOrder['ApprovedOrder']['confirmed_quantity'], $countByPartial, 2);
        }

        //jei sensorius yra maisymo tipo (patvirtinti kiekiai gaunami kubilais), galutini kieki verciame per papildoma formule
        if($sensorType == Sensor::TYPE_MIXING){
            $plan = $planModel->findById($planId);
            if($plan && $plan['Plan']['qty_1'] > 0){
                $count = round($count * $plan['Plan']['yield'] / $plan['Plan']['qty_1']);
            }
        }
        return $count;
    }

    //per sia vieta pakeiciame kiekius kiekvienam uzsakymui atskirai
    public function Oeeshell_filterOrderStatsRecords_Hook(&$approvedOrderData,$currentShift){
        $records['sensors'] = array();
        $approvedOrdersData = array(array(
            'Record'=>array('approved_order_id'=>$approvedOrderData['approved_order_id']),
            'Plan'=>array('id'=>$approvedOrderData['plan_id'], 'step'=>1000),
            'Sensor'=>array('type'=>$approvedOrderData['sensor_type'])
        ));
        $this->Oeeshell_filterGetFactTimeRecords_Hook($approvedOrdersData, $currentShift, $records);
        $approvedOrderData['total_quantity'] = isset($approvedOrdersData[0][0]['total_quantity'])?$approvedOrdersData[0][0]['total_quantity']:$approvedOrderData['total_quantity'];
        if(isset($approvedOrdersData[0][0]['theory_prod_time'])){
            $approvedOrderData['theory_prod_time'] = $approvedOrdersData[0][0]['theory_prod_time'];
        }
    }

    public function ReportsContr_BeforeGenerateXLS_Hook(&$reportModel, &$requestData){
        $reportModel = ClassRegistry::init($this->name.'.'.$this->name. 'Report');
        if(isset($requestData['sensors']) && in_array(20, $requestData['sensors']) && sizeof($requestData['sensors']) > 1 && $requestData['view_angle'] == 0){
            unset($requestData['sensors'][array_search(20, $requestData['sensors'])]);
        }
    }

    public function ReportsContr_BeforeCalculatePeriodXLS_Hook(&$reportModel, &$templateTitle){
        $templateTitle = '/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Reports/calculate_period_xls';
    }

    public function Oeeshell_afterSaveOrderData_Hook(&$saveData, &$order, &$approvedOrder){
        $saveData['confirmed_quantity'] = $order[0]['total_quantity'];
        $saveData['quantity'] = $order[0]['sensor_quantity'];
    }

    public function Messages_beforeCheckProjectAlive_Hook(&$projectList){
        $projectList = array(
            'Leibur'=>'http://10.131.33.143/messages/get_latest_record',
        );
    }

    public function FoundProblem_beforeManipulateCycleStart_Hook(&$fSensor,&$fFoundProblem){
        static $foundProblemModel, $approvedOrderModel, $planModel, $sensorModel, $productionMovementTimeModel, $logModel = null;
        if($foundProblemModel == null){
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $planModel = ClassRegistry::init('Plan');
            $foundProblemModel = ClassRegistry::init('FoundProblem');
            $sensorModel = ClassRegistry::init('Sensor');
            $productionMovementTimeModel = ClassRegistry::init('ProductionMovementTime');
            $logModel = ClassRegistry::init('Log');
        }
        if($fSensor['Sensor']['type'] == Sensor::TYPE_PACKING && !empty($fFoundProblem) && $fFoundProblem['FoundProblem']['problem_id'] == Problem::ID_NOT_DEFINED && isset($fSensor['ApprovedOrder']['id']) && $fSensor['ApprovedOrder']['id'] > 0){ //pakavimas ir vyksta nepazymeta prastova vykdant gamyba
            $problemDuration = strtotime($fFoundProblem['FoundProblem']['end']) - strtotime($fFoundProblem['FoundProblem']['start']);
            if($problemDuration >= $fSensor['Sensor']['records_count_to_start_problem'] * Configure::read('recordsCycle')){
                $sameLineSensorsIds = $sensorModel->getRelatedSensors($fSensor['Sensor']['id'],null,true);
                if(!empty($sameLineSensorsIds)) {
                    $foundProblemModel->bindModel(array('belongsTo' => array('Sensor','Plan')));
                    $productionMovementTime = $productionMovementTimeModel->find('first', array(
                        'conditions'=>array('ProductionMovementTime.production_code'=>$fSensor['Plan']['production_code'])
                    ));
                    if(!empty($productionMovementTime)) {
                        $productionMovementTime = $productionMovementTime['ProductionMovementTime']['time'] * 60;
                        $timeBack = strtotime($fFoundProblem['FoundProblem']['start']) - $productionMovementTime;
                        $queryParams = array(
                            'fields' => array(
                                'ABS(TIMESTAMPDIFF(SECOND, \'' . date('Y-m-d H:i:s', $timeBack) . '\', FoundProblem.start)) AS deviation',
                                'FoundProblem.id',
                                'FoundProblem.problem_id',
                                'FoundProblem.super_parent_problem_id',
                            ), 'conditions' => array(
                                'Sensor.type' => Sensor::TYPE_MIXING,
                                'Sensor.id' => $sameLineSensorsIds,
                                'Plan.paste_code' => $fSensor['Plan']['paste_code'],
                                sprintf('FoundProblem.start BETWEEN \'%s\' AND \'%s\'', date('Y-m-d H:i:s', $timeBack - 120), date('Y-m-d H:i:s', $timeBack + 120)),
                                'FoundProblem.problem_id <>' => Problem::ID_NOT_DEFINED
                            ), 'order' => array(
                                'ABS(TIMESTAMPDIFF(SECOND, \'' . date('Y-m-d H:i:s', $timeBack) . '\', FoundProblem.start))'
                            )
                        );
                        $lineStartProblem = $foundProblemModel->find('first', $queryParams);
                        if(!empty($lineStartProblem)){
                            $logModel->write('Pakavimo darbo centro prastova automatiskai keiciama i tos pacios linijos pradzios jutiklio prastova. Keiciama prastova: '.json_encode($fFoundProblem).' Linijos pradzios prastova: '.json_encode($lineStartProblem).' Vykdyta uzklausa su parametrais: '.json_encode($queryParams), $fSensor);
                            $foundProblemModel->updateAll(
                                array('problem_id'=>$lineStartProblem['FoundProblem']['problem_id'], 'super_parent_problem_id'=>$lineStartProblem['FoundProblem']['super_parent_problem_id']),
                                array('FoundProblem.id'=>$fFoundProblem['FoundProblem']['id'])
                            );
                        }
                    }
                }
            }
        }
        $planDiffAction = explode('_', trim($fSensor['Sensor']['duona_plan_difference_action']??''));
        if(sizeof($planDiffAction) == 3){//prastovos ID, prastovos atšokimo trukmę minutėmis ir minimalų laiko tarpą tarp planų minutėmis, pvz.: 123_60_300
            list($problemId, $timeJumpDuration, $durationBetweenPlans) = $planDiffAction;
            $approvedOrderModel->bindModel(array('belongsTo'=>'Plan'));
            $lastApprovedOrder = $approvedOrderModel->find('first', array(
                'conditions'=>array('ApprovedOrder.sensor_id'=>$fSensor['Sensor']['id']),
                'order'=>array('ApprovedOrder.created DESC')
            ));
            $problemWasAlreadyCreated = $foundProblemModel->find('first', array(
                'conditions'=>array('FoundProblem.plan_id'=>$lastApprovedOrder['Plan']['id'], 'FoundProblem.problem_id'=>$problemId)
            ));
            if($lastApprovedOrder && !empty($lastApprovedOrder['Plan']) && empty($problemWasAlreadyCreated)) {
                $nextOrderByPlan = $planModel->find('first', array('conditions' => array('Plan.start >=' =>$lastApprovedOrder['Plan']['end'])));
                if($nextOrderByPlan){
                    $timeDiffBetweenPlans = strtotime($nextOrderByPlan['Plan']['start']) - strtotime($lastApprovedOrder['Plan']['end']);
                    if($timeDiffBetweenPlans > $durationBetweenPlans*60){//Jei skirtumas tarp planu yra didesnis nei nurodytas minimalus laiko tarpas tarp planų jutiklio nustatymuose
                        //turi būti iš antrojo plano pradžios (plans.start) atimama nustatyme nurodyta prastovos atšokimo reikšmė.
                        //Jei apskaičiuota reikšmė yra mažesnė už dabartinį laiko momentą, sistema turi užbaigti prieš tai vykusią prastovą ir pradėti naują, nurodyto tipo, prastovą.
                        if(strtotime($nextOrderByPlan['Plan']['start']) - $timeJumpDuration*60 < time()){
                            if(!empty($fFoundProblem)){//Jei tuo metu kai turės būti pradėta prastova automatiškai jutiklis fiksuos gamybą, prastova nebus pradėta (gamybos metu $currentProblem = null)
                                $time = new DateTime();
                                $foundProblemModel->findAndComplete($fSensor, $time);
                                $time = $time->add(new DateInterval('PT1S'));
                                $shift = $this->Shift->findCurrent($fSensor['Sensor']['branch_id']);
                                $newFoundProblem = $foundProblemModel->newItem($time, $time, $problemId, $fSensor['Sensor']['id'], $shift['Shift']['id'], FoundProblem::STATE_IN_PROGRESS, null, false, $lastApprovedOrder['Plan']['id']);
                                $logModel->write('Uzdaroma esama problema, kadangi tarp esamo ir busimo plano gaunamas per didelis laiko tarpas (pagal jutiklio nustatyma duona_plan_difference_action). Uzdaroma esama prastova '.json_encode($fFoundProblem).' Sukurta nauja prastova: '.json_encode($newFoundProblem), $fSensor);
                                $logModel->write('Esamas planas '.json_encode($lastApprovedOrder).' Busimas planas: '.json_encode($nextOrderByPlan), $fSensor);
                            }
                        }
                    }
                }
            }
        }
    }

    public function View_EditSensorLeftBlock_Hook(&$form){
        echo $form->input('duona_plan_difference_action', array('label' => __('Pamainos pradėjimas. Nurodykite prastovos ID, prastovos atšokimo trukmę minutėmis ir minimalų laiko tarpą tarp planų minutėmis, pvz.: 123_60_300'), 'type'=>'text','class' => 'form-control', 'div' => array('class' => 'form-group')));
    }

}
    