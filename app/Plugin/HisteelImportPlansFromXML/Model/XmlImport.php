<?php
class XmlImport extends AppModel{
	
	private static $importStatusFile, $stopImportFile;
	public $totalOrdersCount = 0;
	private $plans;
	public $xmlFile;
	private $info = array();
	private $Log = array();
	
	private $planModel, $possibilityModel;
	private $sensors;
	
	public function __construct(){
		$sensorModel = ClassRegistry::init('Sensor');
		$this->sensors = $sensorModel->find('list', array('fields'=>array('ps_id', 'id')));
		$this->planModel = ClassRegistry::init('Plan');
		$this->possibilityModel = ClassRegistry::init('Possibility');
		$this->Log = ClassRegistry::init('Log');
	}
	
	public function updateStatusDuringFilter($serialNumber, $rowNumber){
		$this->checkForStopMarker();
		if(!trim(self::$importStatusFile)) return;
        $handle = fopen(self::$importStatusFile, 'w+');
        fwrite($handle, 
        	'<h3>'.__('Filtruojami užsakymai').'</h3>'."\n".
        	'<p>'.__('Serijos numeris').': '.$serialNumber.'</p>'."\n".
        	'<p>'.__('Išfiltruota').': '.$rowNumber.' '.__('iš').' '.$this->totalOrdersCount.'</p>'."\n"
		);
        fclose($handle);
	}
	
	public function createStopMarker(){
		$handle = fopen(self::$stopImportFile, 'w+');
		fclose($handle);
	}
	
	public function checkForStopMarker(){
		if(file_exists(self::$stopImportFile)){
			echo josn_encode(array('1=1'));
			die();
		}
	}
	
	public function import_orders($file=''){
		try{
			$this->plans = Xml::toArray(Xml::build($this->xmlFile));
			$this->plans = isset($this->plans['PData']['oee_plan'][0])?$this->plans['PData']['oee_plan']:array($this->plans['PData']['oee_plan']);
			$this->totalOrdersCount = sizeof($this->plans);
			$this->create_orders();
            $this->planModel->reorder();
            
			if(empty($this->info)) return true;
			else return $this->info;
		}catch(Exception $e){
		    $this->Log->write(__('Iš ftp serverio importuotas failas %s neatitiko xml formato', $file)); 
			return false;
		}
	}

	//kuria uzsakymus is paduoto uzsakymu masyvo pagal sistemoje esanciu sablonu pavyzdi
	public function create_orders(){
		$importedOrdersIds = array();
		$productModel = ClassRegistry::init('Product');
		foreach($this->plans as $plan){
			$saveData = $plan;
            if($this->confirmDeleteStatus($plan)) continue;
			$this->checkProblems($plan);
            if(!isset($this->sensors[$plan['w_c']])){
                continue;
            }
			$saveData['sensor_id'] = $this->sensors[$plan['w_c']];
			$saveData['box_quantity'] = $plan['nest'] <= 0?1:round(1/$plan['nest']);
			$saveData['production_code'] = $plan['unit_code'];
			$saveData['production_name'] = $plan['unit_title'];
			$saveData['step'] = $plan['speed']*60;
			$saveData['paste_code'] = $plan['operation'];
			$saveData['mo_number'] = $plan['mo'];
			//$saveData['disabled'] = 0;
			$saveData['has_problem'] = isset($plan['has_problem'])?$plan['has_problem']:0;
			$saveData['serialized_data'] = isset($saveData['serialized_data'])?json_encode($saveData['serialized_data']):'';
			$saveData = array_filter($saveData, function($val){ return !is_array($val); });
			$planExist = $this->planModel->find('first', array('conditions'=>array(
				//'production_code'=>$saveData['production_code'],
				//'paste_code'=>$saveData['paste_code'],
				'Plan.mo_number'=>$saveData['mo_number'],
				'Plan.paste_code'=>$saveData['paste_code'],
				//'Plan.sensor_id'=>$saveData['sensor_id'],
			)));
			if(!empty($planExist)){
				$this->planModel->id = $planExist['Plan']['id'];
                if($planExist['Plan']['sensor_id'] != $saveData['sensor_id'] && isset($plan['posibilities']['posibility'])){
                    $possibilities = isset($plan['posibilities']['posibility'][0])?$plan['posibilities']['posibility']:array($plan['posibilities']['posibility']);
                    foreach($possibilities as $possibility){
                          $possSensorId = isset($this->sensors[$possibility['w_c']])?$this->sensors[$possibility['w_c']]:0;
                          if($possSensorId == $planExist['Plan']['sensor_id']){
                              $saveData = array_merge($saveData, $possibility);
                              $saveData['step'] = $possibility['speed'] * 60; 
                              $saveData['sensor_id'] = $possSensorId;
                          }
                    }
                }
				$this->possibilityModel->deleteAll(array('plan_id'=>$this->planModel->id));
			}else{
				$this->planModel->create();
			}
			$this->planModel->save($saveData);
			$this->addPossibilities($plan); 
		}
	}

	private function addPossibilities($plan){ 
		if(!isset($plan['posibilities']['posibility'])) return;
		$saveData[] = array(
			'sensor_id' 	=> $this->sensors[$plan['w_c']],
			'plan_id'  		=> $this->planModel->id,
			'speed'			=> $plan['speed']*60,
			'client_speed'	=> $plan['client_speed'],
			'preparation_time'	=> $plan['preparation_time'],
			'production_time'	=> $plan['production_time'],
		);
		$possibilities = isset($plan['posibilities']['posibility'][0])?$plan['posibilities']['posibility']:array($plan['posibilities']['posibility']);
		foreach($possibilities as $possibility){
		    $this->checkProblems($possibility, true);
            if(isset($possibility['has_problem'])){
                $this->planModel->updateAll(array('has_problem'=>1), array('Plan.id'=>$this->planModel->id));
            }
			$possibility['sensor_id'] = isset($this->sensors[$possibility['w_c']])?$this->sensors[$possibility['w_c']]:0;
			$possibility['plan_id'] = $this->planModel->id;
			$possibility['speed'] *= 60; 
			$saveData[] = $possibility;
		}
		$this->possibilityModel->saveAll($saveData);
	}
	
	private function checkProblems(&$plan, $possibilityCheck=false){
		$problemFound = false;
		if(!isset($this->sensors[$plan['w_c']])){
		    if(!$possibilityCheck){
			     $this->info[] = __('Plano įkėlimo metu nerastas darbo centras %s', $plan['w_c']);
			}else{
			    $this->info[] = __('Plano papildomų galimybių įkėlimo metu nerastas darbo centras %s.', $plan['w_c']);
			}
			$problemFound = true;
            $plan['has_problem'] = 1;
		}
		if($plan['speed'] <= 0 || $plan['preparation_time'] <= 0 || $plan['production_time'] <= 0){
			$plan['has_problem'] = 1;
		}
		return $problemFound;
	}
    
    private function confirmDeleteStatus($plan){
        if(isset($plan['delete']) && isset($plan['mo']) && $plan['delete'] == 1){
            $this->Log->write("Užsakymas buvo išjungtas užsakymų importavimo metu. Užsakymo duomenys: ".json_encode($plan));
            $this->planModel->updateAll(array('disabled'=>1),array('mo_number'=>$plan['mo']));
            return true;
        }
        return false;
    }

}
