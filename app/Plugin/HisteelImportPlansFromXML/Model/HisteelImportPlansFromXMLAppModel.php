<?php
App::uses('AppModel', 'Model');

class HisteelImportPlansFromXMLAppModel extends AppModel {
	public static $navigationAdditions = array( );

    public static function loadNavigationAdditions() {
        self::$navigationAdditions[2] =
            array(
                'name'=>'Plano importavimas',
                'route'=>Router::url(array('plugin' => 'histeel_import_plans_from_x_m_l','controller' => 'Plugins/index', 'action' => 'index')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
}
