<?php
class Kaunobaldai extends AppModel{
     
     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     }

     public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
         $recordModel = ClassRegistry::init('Record');
         $problemsExclusionModel = ClassRegistry::init('ShiftTimeProblemExclusion');
         $startDate = current($hours)['date'].' '.current($hours)['value'].':00';
         $endDate = date('Y-m-d H:i', strtotime($startDate) + 3600 * (sizeof($hours)));
         $exclusionList = $problemsExclusionModel->find('list', array('fields'=>array('problem_id')));
         $recordModel->bindModel(array('belongsTo'=>array('FoundProblem')));
         $times = $recordModel->find('all', array(
            'fields'=>array(
                'TIMESTAMPDIFF(SECOND,MIN(Record.created),MAX(Record.created)) + '.Configure::read('recordsCycle').' - SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . implode(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS available_duration',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS fact_prod_time',
                'DATE_FORMAT(Record.created, \'%H\') AS hour'
            ),'conditions'=>array(
                'Record.created >=' => $startDate,
                'Record.created <=' => $endDate,
                'Record.sensor_id' => $fSensor['Sensor']['id'],
            ),'group'=>array(
                'DATE_FORMAT(Record.created, \'%H\')'
            )
         ));
         $dcHoursInfoBlocks['exploitation_factor']['value'] = array();
         foreach($times as $time){
             $hourNr = $time[0]['hour'];
             $dcHoursInfoBlocks['exploitation_factor']['value'][(int)$hourNr] = $time[0]['available_duration'] > 0?number_format($time[0]['fact_prod_time'] / $time[0]['available_duration'] * 100,2,'.',''):0;
         }
     }

     //automatiskai pridedame 60 minuciu prastovos, mazinancios pamainos laika, kiekvienai pamainai
     public function Oeeshell_beforeLastGetFactTimeLoop_Hook(&$record, $sensor_id){
         $record['shift_length_with_exclusions'] = max(0, ($record['shift_length_with_exclusions'] - 60));
         $record['shift_time_exclusions'] += 60;
         $record['true_problem_time_exclusions'] = max(0, $record['true_problem_time_exclusions'] - 60);
     }
     
}