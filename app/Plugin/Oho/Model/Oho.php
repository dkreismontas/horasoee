<?php
class Oho extends AppModel{
        
    public function Record_calculateOee_Hook(&$oeeParams, $data){
        //if(isset($data['sensor_id']) && in_array($data['sensor_id'], array(1))){
            $oeeParams['operational_factor'] = 1;
            $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
            $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
        //}
    }
    
    public function Workcenter_changeUpdateStatusVariables_Hook2(&$data){
        if(isset($data['workcenterHoursInfoBlocks']['avgspeed_in_hour'])){
            $data['workcenterHoursInfoBlocks']['avgspeed_in_minute'] = $data['workcenterHoursInfoBlocks']['avgspeed_in_hour'];
            $data['workcenterHoursInfoBlocks']['avgspeed_in_minute']['title'] = str_replace('vnt/h','vnt/min',$data['workcenterHoursInfoBlocks']['avgspeed_in_hour']['title']);
            array_walk($data['workcenterHoursInfoBlocks']['avgspeed_in_minute']['value'], function(&$value){
               $value = round(bcdiv($value,60,4)); 
            });
            array_walk($data['workcenterHoursInfoBlocks']['avgspeed_in_hour']['value'], function(&$value){
               $value = round($value); 
            });
        }
    }
      
}
    