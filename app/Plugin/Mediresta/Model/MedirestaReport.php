<?php
class MedirestaReport extends AppModel{
    public $useTable = false;
    public $OrderCalculation, $FoundProblem, $DashboardsCalculation, $Plan, $Record, $Loss, $LossType, $User, $Problem, $PartialQuantity, $ApprovedOrder, $maxDowntimeLevel, $downtimesExclusionsIds;
    private static $colors = array(-1=>'97dc97', 1=>'e8c775',2=>'c4c4c4', 3=>'fe9b98');
    public $sheetNr = 1;
    public $Help;

    public function generateShiftSheet($objPHPExcel, $shift, $sensor_id, $sheet_number) {
        $return_arr = array();
        $true_problem_count = 0;
        $transition_count = 0;
        $no_work_time = 0;

        $this->OrderCalculation->bindModel(array('hasOne' => array('Plan' => array('className' => 'Plan', 'foreignKey' => false, 'conditions' => array('Plan.mo_number = OrderCalculation.mo_number AND Plan.sensor_id = OrderCalculation.sensor_id')))));
        $calculation_mos = $this->OrderCalculation->find('all', array(
            'conditions' => array('OrderCalculation.shift_id' => $shift['Shift']['id'], 'OrderCalculation.sensor_id' => $sensor_id),
            'order'      => array('OrderCalculation.mo_number ASC')
        ));
        $quantities_by_mo = array();
        foreach ($calculation_mos as $cm) {
            $quantities_by_mo[$cm['OrderCalculation']['mo_number']] = $cm['OrderCalculation']['quantity'];
        }
        //$problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        //$found_problems = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,0);
        $found_problems_transitions = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,1);
        $found_problems_no_work = $this->FoundProblem->getByShiftForReport($shift,$sensor_id,2);
        foreach($found_problems_no_work as $fp) {
            $no_work_time += $fp[0]['found_problem_duration'];
            $true_problem_count+=1;
        }
        $no_work_time=round($no_work_time/60);
        $transition_count = count($found_problems_transitions);
        $transition_time = 0;

//        $found_problem_durations_sum_by_plan = array();
//        foreach ($found_problems as $fp) {
//            $plan_id = $fp['FoundProblem']['plan_id'];
//            if (!array_key_exists($plan_id, $found_problem_durations_sum_by_plan)) {
//                $found_problem_durations_sum_by_plan[$plan_id] = $fp[0]['found_problem_duration'];
//            } else {
//                $found_problem_durations_sum_by_plan[$plan_id] += $fp[0]['found_problem_duration'];
//            }
//        }
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'SUM('.$col.') AS '.$col; });
        $calculation = $this->DashboardsCalculation->find('first', array(
            'fields'=>$fields,
            'conditions' => array(
                'DashboardsCalculation.shift_id' => $shift['Shift']['id'],
                'DashboardsCalculation.sensor_id' => $sensor_id
            ),'group'=>array('DashboardsCalculation.shift_id')
        ));
        if (sizeof($calculation) > 0) {
            if ($this->sheetNr != 0) {
                $objPHPExcel->createSheet();
            }
            $workSheet = $objPHPExcel->setActiveSheetIndex($this->sheetNr++);
            for ($col = 'A'; $col !== 'P'; $col ++){
                $workSheet->getColumnDimension($col)->setAutoSize(true);
            }
            $sheetTitle = $shift['Shift']['type'].' '.str_replace(':', ' ', $shift['Shift']['start']);
            $workSheet->setTitle(mb_substr($sheetTitle,0,30), false);
        }else{
            return array();
            $workSheet->setCellValue('A1', __('Pamaina'));
            $workSheet->setCellValue('B1', $shift['Shift']['type'].' '.$shift['Shift']['start'] . " - " . $shift['Shift']['end'] . " (" . $shift['Shift']['id'] . ")");
            $workSheet->setCellValue('E2', __('Nėra duomenų'));

            $return_arr = array(
                'shift_type'          => $shift['Shift']['type'],
                'shift_start'         => $shift['Shift']['start'],
                'shift_end'           => $shift['Shift']['end'],
                'shift_disabled'      => 0,
                'shift_length'        => round((strtotime($shift['Shift']['end']) - strtotime($shift['Shift']['start']))/60),
                'theory_prod_time'    => 0,
                'fact_prod_time'      => 0,
                'true_problem_time'   => 0,
                'true_problem_count'  => 0,
                'transition_time'     => $transition_time,
                'transition_count'    => $transition_count,
                'exceeded_transition_time'=> 0,
                'no_work_time'        => $no_work_time,
                'exploitation_factor' => 0,
                'quantity_type1'      => 0,
                'quantity_type2'      => 0,
                'total_quantity'      => 0,
                'planned_quantity'    => 0,
                'quality_factor'      => 0,
                'losses12'            => 0,
                'losses23'            => 0,
                'losses_quantity'     => 0,
                'operational_factor'  => 0,
                'oee'                 => 0,
                'mttf'                => 0,
                'mttr'                => 0,
                'count_delay'         => 0,
                'true_problem_count_exclusions' => 0,
                'true_problem_time_exclusions' => 0,
                'mtt_problem_count' => 0,
                'mtt_problem_time' => 0,
                'slow_speed_duration' => 0,
                'theorical_transition_time' => 0,
                'avg_worker_speed' => 0,
            );
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $return_arr['exploitation_factor_with_exclusions'] = 0;
                $return_arr['oee_with_exclusions'] = 0;
                $return_arr['shift_length_with_exclusions'] = $return_arr['shift_length'] - $no_work_time;
            }

            return $return_arr;
        }
        $calculation = $calculation[0];
        //$calculation['shift_length'] = round((strtotime($shift['Shift']['end']) - strtotime($shift['Shift']['start']))/60);

        $approved_orders = $this->Record->getApprovedOrderTimes($shift, $sensor_id);
        $approved_orders_ids = Set::extract('/ApprovedOrder/id', $approved_orders);

        $plans = $this->Plan->find('all', array(
            'fields'     => array(
                'Plan.*',
                '(TIMESTAMPDIFF(SECOND,Plan.start, Plan.end)) as plan_time_all',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN Plan.start > \'' . $shift['Shift']['start'] . '\' THEN Plan.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN Plan.end < \'' . $shift['Shift']['end'] . '\' THEN Plan.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as plan_time',
            ),
            'conditions' => array(
                'start <'   => $shift['Shift']['end'],
                'end >'     => $shift['Shift']['start'],
                'sensor_id' => $sensor_id
            ),
            'order'      => array('Plan.start ASC')
        ));


        $quantities = $this->Record->getQuantities($shift, $sensor_id);
        $user = $this->Record->getUser($shift, $sensor_id);
        $quantities_fixed = array();
        foreach ($quantities as $q) {
            $quantities_fixed[$q[0]['approved_order_id']] = $q[0]['quantity'];
        }
        $quantities = $quantities_fixed;


        $this->Loss->bindModel(array('belongsTo' => array('LossType')),false);
        $losses = $this->Loss->find('all', array(
            'fields'     => array('SUM(Loss.value) as losses', 'CONCAT(LossType.name," (",LossType.unit,")") as full_name', 'LossType.id'),
            'conditions' => array('Loss.approved_order_id' => $approved_orders_ids, 'Loss.shift_id'=>$shift['Shift']['id']),
            'group'      => array('Loss.loss_type_id'), 'order'=>array('LossType.id')
        ));
        $losses = Hash::combine($losses, '{n}.LossType.id', '{n}');
        $lossesByOrder = $this->Loss->find('all', array(
            'fields'     => array('SUM(Loss.value) as quantity', 'LossType.id','Loss.approved_order_id'),
            'conditions' => array('Loss.approved_order_id' => $approved_orders_ids, 'Loss.shift_id'=>$shift['Shift']['id']),
            'group'      => array('Loss.approved_order_id','Loss.loss_type_id'), 'order'=>array('LossType.id')
        ));
        $lossesByOrder = Hash::combine($lossesByOrder, '{n}.LossType.id', '{n}.0', '{n}.Loss.approved_order_id');
        //$grouped_problems = $this->FoundProblem->getGroupedByProblem($found_problems);

        // Header shift info
        $workSheet->setCellValue('A1', __('Pamaina'));
        $workSheet->setCellValue('B1', $shift['Shift']['start'] . " - " . $shift['Shift']['end'] . " (" . $shift['Shift']['id'] . ")");

        $workSheet->getStyle('A1:B1')->getFont()->setBold(true);
        $currentRow = 4;
        $start_column = 'A';
        // Fact times
        #region region:fact times
        $workSheet->setCellValue('A3', __('Faktiniai laikai'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Darbo centras'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Produkcijos pavadinimas'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Produkcijos kodas'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Užsakymo numeris'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Pradžia'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Pabaiga'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Kiekis (vnt)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Kiekis pamainoje (vnt)'));
//        $workSheet->setCellValue('H4', 'Dėžių kiekis (vnt)');
        $workSheet->setCellValue($start_column++.$currentRow, __('Patvirtintas kiekis (vnt)'));
//        $workSheet->setCellValue('J4', 'Viso Trukmė (min) su sustojimais');
        $workSheet->setCellValue($start_column++.$currentRow, __('Trukmė pamainoje (min) su sustojimais'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Gamybos trukmė pamainoje (min)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Teorinis kiekis pamainoje'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Vid. gamybos greičio nuokrypis nuo normos (%)'));
        $workSheet->setCellValue($start_column++.$currentRow, __('Įvestas kiekis'));
        foreach($losses as $loss){
            $workSheet->setCellValue($start_column++.$currentRow, $loss[0]['full_name']);
        }
        $workSheet->getStyle('A3:'.$start_column.'4')->getFont()->setBold(true);
//        $workSheet->setCellValue('J4', 'Trukmė pamainoje (min)');
        $currentRow = 5;
        $start_column = 'A';
        $total_planned_quantity = 0;
        $partialQuantities = $this->PartialQuantity->find('all', array(
            'fields'=>array('SUM(PartialQuantity.quantity) AS quantity', 'PartialQuantity.approved_order_id'),
            'conditions'=>array('PartialQuantity.approved_order_id'=>$approved_orders_ids, 'PartialQuantity.shift_id'=>$shift['Shift']['id']),
            'group'=>array('PartialQuantity.approved_order_id')
        ));
        $partialQuantities = Hash::combine($partialQuantities, '{n}.PartialQuantity.approved_order_id', '{n}.0.quantity');
        foreach ($approved_orders as $ao) {
            //pr($ao);die();
//            $problem_durations = 0;
//            if (array_key_exists($ao['Plan']['id'], $found_problem_durations_sum_by_plan)) {
//                $problem_durations = $found_problem_durations_sum_by_plan[$ao['Plan']['id']];
//            }
//            echo ($ao[0]['fact_time']/60)." - ";
//            echo $found_problem_durations_sum_by_plan[$ao['Plan']['id']] . "<br>";
            $workSheet->setCellValue($start_column . $currentRow, $ao['Sensor']['name'].' '.(isset($ao['Factory']['name'])?$ao['Factory']['name']:''));
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['production_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['created']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['end']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['quantity']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao[0]['sum_quantity']);
            //$workSheet->setCellValue(++ $start_column . $currentRow, array_key_exists($ao['Plan']['mo_number'], $quantities_by_mo) ? $quantities_by_mo[$ao['Plan']['mo_number']] : '');
//            $workSheet->setCellValue(++$start_column . $currentRow, $ao['ApprovedOrder']['box_quantity']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $ao['ApprovedOrder']['confirmed_quantity']);
//            $workSheet->setCellValue(++$start_column . $currentRow, "---"); //round(($ao[0]['fact_time_all']) / 60)
            $workSheet->setCellValue(++ $start_column . $currentRow, round(($ao[0]['fact_time']) / 60));
            $workSheet->setCellValue(++ $start_column . $currentRow, round(($ao[0]['fact_time_work']) / 60));
            $planned_quantity = max(0, round(($ao[0]['fact_time_work'] / 3600) * $ao['Plan']['step'] * 10) / 10);
            $total_planned_quantity += $planned_quantity;
            $workSheet->setCellValue(++ $start_column . $currentRow, $planned_quantity);
            //$speed = $ao[0]['fact_time_work'] > 0?$ao[0]['sum_quantity'] / $ao['Plan']['box_quantity'] / round(($ao[0]['fact_time_work']) / 60):0; //kiekis per minute
            $speed = round(($ao[0]['fact_time_work']) / 60) > 0?$ao[0]['sum_quantity'] / round(($ao[0]['fact_time_work']) / 60):0; //kiekis per minute
            $deviation = $ao['Plan']['step'] > 0?round($speed / ($ao['Plan']['step'] / 60) * 100, 2):0;
            $workSheet->setCellValue(++ $start_column . $currentRow, $deviation);
            $workSheet->setCellValue(++ $start_column . $currentRow, isset($partialQuantities[$ao['ApprovedOrder']['id']])?$partialQuantities[$ao['ApprovedOrder']['id']]:0);
            if(isset($lossesByOrder[$ao['ApprovedOrder']['id']])){
                foreach($lossesByOrder[$ao['ApprovedOrder']['id']] as $losType => $lossByOrderData){
                    foreach($losses as $loss){
                        if($loss['LossType']['id'] != $losType){continue;}
                        $workSheet->setCellValue(++$start_column.$currentRow, $lossByOrderData['quantity']);
                    }
                }
            }
//            $workSheet->setCellValue(++$start_column.$currentRow,round($ao['operational_factor'] * 100)/100);
            $start_column = 'A';
            $currentRow ++;
        }
        $oeeParams = $this->Record->calculateOee($calculation);
        $parameters = array(&$shift, &$workSheet, &$currentRow, &$sensor_id, &$return_arr, &$calculation, &$oeeParams, &$found_problems);
        $this->callPluginFunction('Report_UpdateShiftSheetColumns_Hook', $parameters, Configure::read('companyTitle'));
        // Found problem times
        #region region:fp times
        $currentRow ++;
        $letterStart = 'A';
        $workSheet->setCellValue($letterStart . $currentRow, __('Visų pamainų įvykių žurnalas'));
        $workSheet->setCellValue($letterStart++ . ++ $currentRow, __('Darbo centras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('MO'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio pavadinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio kodas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Paaiškinimas'));
        for($i=0; $i<=$this->maxDowntimeLevel; $i++){
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas %d', $i+1));
        }
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pradžia'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pabaiga'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė tarp prastovų (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Komentaras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Plano greitis (step) vnt/h'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis pakuotėje/porcijoje'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':Q' . $currentRow)->getFont()->setBold(true);

        $currentRow ++;
        $start_column = 'A';
        $previous_date = null;
        /*$totalByProblemType = array();
        foreach ($found_problems as $fp) {
            try{
                $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
            }catch(Exeption $e){
                $approvedAddData = null;
            }
            $fp['Sensor']['name'] = $fp['Sensor']['name'].' '.(isset($fp['Factory']['name'])?$fp['Factory']['name']:'');
            $workSheet->setCellValue($start_column . $currentRow, $fp['Sensor']['name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['DiffCard']['description']);
            $problemId = $fp['FoundProblem']['transition_problem_id'] > 0?$fp['FoundProblem']['transition_problem_id']:$fp['Problem']['id'];
            $parsedProblemsTree = $problemId > 0 && isset($problemsTreeList[$problemId])?array_reverse($problemsTreeList[$problemId]):array();
            for($i=0; $i<= $this->maxDowntimeLevel; $i++){
                if(isset($parsedProblemsTree[$i])){
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($parsedProblemsTree[$i]));
                }else{
                    //$problemName = $fp['FoundProblem']['transition_problem_id'] > 0?
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($fp['Problem']['name']));
                }
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['start']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['end']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($fp[0]['found_problem_duration'] / 60 * 10) / 10);
            if ($previous_date == null && strtotime($fp['FoundProblem']['start']) >= strtotime($shift['Shift']['start'])) {
                $previous_date = $shift['Shift']['start'];
            }
			$date_diff = 0;
            if($fp['FoundProblem']['problem_id'] >= Problem::ID_NOT_DEFINED && !in_array($fp['FoundProblem']['problem_id'], $this->downtimesExclusionsIds)) {
                if ($previous_date != null) {
                    $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date);
                    $date_diff = $date_diff > 0?$date_diff:0;
                }
                $previous_date = $fp['FoundProblem']['end'];
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, ($previous_date != null) ? round($date_diff / 60, 2) : "");
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['comments']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['step']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $this->getIntervalQuantity($fp));
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['box_quantity']);
            //$start_column++;
            $backgroundColor = isset(self::$colors[$fp['FoundProblem']['problem_id']])?self::$colors[$fp['FoundProblem']['problem_id']]:self::$colors[3];
            $backgroundColor = in_array($fp['FoundProblem']['problem_id'], $this->downtimesExclusionsIds)?self::$colors[2]:$backgroundColor;
            //do{
                $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => $backgroundColor)
                        )
                    )
                );
            //}while($bgCol != $start_column);
            $start_column = 'A';
            $currentRow ++;
            if(!isset($totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']])){
                $totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']] = array('problem_name'=>$fp['Problem']['name'], 'total'=>0,'sensor'=>$fp['Sensor']['name']);
            }
            $totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']]['total'] += strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start']);
        }
        #endregion
        $currentRow ++;
        $letterStart = 'A';
        $workSheet->setCellValue($letterStart . $currentRow, __('Visų pamainų susumuotas įvykių laikų žurnalas'));
        $workSheet->setCellValue($letterStart++ . ++ $currentRow, __('Darbo centras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':C' . $currentRow)->getFont()->setBold(true);
        foreach($totalByProblemType as $sensorDowntimes){
            foreach($sensorDowntimes as $sensorDowntime){
                $letterStart = 'A';
                $currentRow++;
                $workSheet->setCellValue($letterStart++ . $currentRow, $sensorDowntime['sensor']);
                $workSheet->setCellValue($letterStart++ . $currentRow, Settings::translate($sensorDowntime['problem_name']));
                $workSheet->setCellValue($letterStart++ . $currentRow, bcdiv($sensorDowntime['total'],60,2));
            }
        }

        $currentRow += 2;;
        $workSheet->setCellValue('A' . $currentRow, __('Prastovos (ataskaita)'));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Pavadinimas'));
        $workSheet->setCellValue('B' . $currentRow, __('Viso trukmė (min)'));
        $workSheet->setCellValue('C' . $currentRow, __('Kiekis (vnt)'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':F' . $currentRow)->getFont()->setBold(true);
        $currentRow ++;
        $start_column = 'A';
        foreach ($grouped_problems as $problemId => $gp) {
            if($problemId <= 2) continue;
            $workSheet->setCellValue($start_column . $currentRow, $gp['problem_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($gp['duration'] / 60 * 10) / 10);
            $workSheet->setCellValue(++ $start_column . $currentRow, $gp['count']);

            $start_column = 'A';
            $currentRow ++;
        }*/
        // Results
        #region region:results
        $currentRow ++;
        $workSheet->setCellValue('A' . $currentRow, __('Rodikliai'));
        $workSheet->getStyle('A' . ($currentRow) . ':A' . $currentRow)->getFont()->setBold(true);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Pamainos trukmė (min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['shift_length']);
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('Pamainos trukmė su išimtimis (min)'));
            $workSheet->setCellValue('B' . $currentRow, $calculation['shift_length_with_exclusions']);
        }
        $workSheet->setCellValue('A' . ++ $currentRow, __('Gamybos laikas faktas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['fact_prod_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų kiekis (vnt)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_count'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų laikas su išimtimis (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_time_exclusions'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prastovų kiekis su išimtimis (vnt)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['true_problem_count_exclusions'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Derinimų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['transition_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Viršytų derinimų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['exceeded_transition_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Teorinis derinimų laikas (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['theorical_transition_time'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Viso supakuotas kiekis (m)'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['total_quantity'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Vidutinis 1 žmogaus greitis m/val'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['avg_worker_speed'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Gero greičio minučių kiekis (%)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['fact_prod_time'] > 0?round(bcdiv($calculation['fact_prod_time'] - $calculation['slow_speed_duration'], $calculation['fact_prod_time'],4) * 100,2):0);
        $workSheet->setCellValue('A' . ++ $currentRow, __('Lėtesnių nei numatyta darbo minučių laikas'));
        $workSheet->setCellValue('B' . $currentRow, round($calculation['slow_speed_duration'],2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('Prieinamumo koef. su planinėmis prastovomis'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['exploitation_factor'],2));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('Prieinamumo koef. su išimtimis'));
            $workSheet->setCellValue('B' . $currentRow, round($oeeParams['exploitation_factor_exclusions'],2));
        }
        $workSheet->setCellValue('A' . ++ $currentRow, __('Efektyvumo koef.'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['operational_factor'],2));
        if($oeeParams['quality_factor'] > 0){
            $workSheet->setCellValue('A' . ++ $currentRow, __('Kokybės koef.'));
            $workSheet->setCellValue('B' . $currentRow, round($oeeParams['quality_factor'],2));
        }
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue('A' . ++ $currentRow, __('OEE be planinių prastovų'));
            $workSheet->setCellValue('B' . $currentRow, round($oeeParams['oee_exclusions'],2));
        }
        $workSheet->setCellValue('A' . ++ $currentRow, __('OEE su planinėmis prastovomis'));
        $workSheet->setCellValue('B' . $currentRow, round($oeeParams['oee'],2));
        // $workSheet->setCellValue('A' . ++ $currentRow, __('Formavimas (vnt)'));
        // $workSheet->setCellValue('B' . $currentRow, $various_values['quantities']['type1']);
        // $workSheet->setCellValue('A' . ++ $currentRow, __('Linijos vidurys (vnt)'));
        // $workSheet->setCellValue('B' . $currentRow, $various_values['quantities']['type2']);
        ++ $currentRow;
        $workSheet->setCellValue('A' . ++ $currentRow, __('Deklaruotas brokas'));
        ++ $currentRow;
        foreach ($losses as $l) {
            $workSheet->setCellValue('A' . $currentRow, $l[0]['full_name']);
            $workSheet->setCellValue('B' . $currentRow, $l[0]['losses']);
            ++ $currentRow;
        }
        ++ $currentRow;
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTTR (min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['true_problem_count_exclusions'] > 0?round($calculation['true_problem_time_exclusions'] / $calculation['true_problem_count_exclusions'], 2):0);
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTTR (pažymėtoms prastovoms, min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['mtt_problem_count'] > 0?round($calculation['mtt_problem_time'] / $calculation['mtt_problem_count'], 2):0);
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTBF (min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['true_problem_count_exclusions'] > 0?round(($calculation['fact_prod_time'] + $calculation['transition_time']) / $calculation['true_problem_count_exclusions'], 2):0);
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTBF (pažymėtoms prastovoms, min)'));
        $workSheet->setCellValue('B' . $currentRow, $calculation['mtt_problem_count'] > 0?round(($calculation['fact_prod_time'] + $calculation['transition_time']) / $calculation['mtt_problem_count'], 2):0);
        #endregion
        $return_arr = array_merge($return_arr, array(
            'shift_type'         => $shift['Shift']['type'],
            'shift_start'         => $shift['Shift']['start'],
            'shift_end'         => $shift['Shift']['end'],
            'shift_disabled'      => $shift['Shift']['disabled'],
            'shift_length'        => $calculation['shift_length'],
            'theory_prod_time'    => round($calculation['theory_prod_time'], 2),
            'fact_prod_time'      => round($calculation['fact_prod_time'],2),
            'true_problem_time'   => round($calculation['true_problem_time'],2),
            'true_problem_count'  => $calculation['true_problem_count'],
            'transition_time'     => round($calculation['transition_time'],2),
            'exceeded_transition_time'=> round($calculation['exceeded_transition_time'],2),
            'transition_count'    => $transition_count,
            'no_work_time'        => round($no_work_time,2),
            'exploitation_factor' => $oeeParams['exploitation_factor'],
            //'quantity_type1'      => $various_values['quantities']['type1'],
            //'quantity_type2'      => $various_values['quantities']['type2'],
            'total_quantity'      => round($calculation['total_quantity'],2),
            'planned_quantity'    => round($total_planned_quantity,2),
            'quality_factor'      => round($calculation['quality_factor'],2),
            //'losses12'            => $various_values['quantities']['type1'] - $various_values['quantities']['type2'],
            //'losses23'            => $various_values['quantities']['type2'] - $various_values['quantities']['type3'],
            'losses_quantity'     => ($losses) ? $losses : array(),
            'operational_factor'  => $oeeParams['operational_factor'],
            'quality_factor'      => $oeeParams['quality_factor'],
            'good_quantity'       => $calculation['good_quantity'],
            'all_quantity'        => $calculation['all_quantity'],
            'oee'                 => $oeeParams['oee'],
            'count_delay'         => round($calculation['count_delay'],2),
            'user'                => isset($user[0]['user_id']) && $user[0]['user_id']>0?$this->User->findById($user[0]['user_id']):array(),
            'true_problem_count_exclusions' => $calculation['true_problem_count_exclusions'],
            'true_problem_time_exclusions' => $calculation['true_problem_time_exclusions'],
            'mtt_problem_count' => $calculation['mtt_problem_count'],
            'mtt_problem_time' => $calculation['mtt_problem_time'],
            'slow_speed_duration' => $calculation['slow_speed_duration'],
            'theorical_transition_time' => $calculation['theorical_transition_time'],
            'avg_worker_speed' => $calculation['avg_worker_speed'],
        ));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $return_arr['exploitation_factor_with_exclusions'] = $oeeParams['exploitation_factor_exclusions'];
            $return_arr['oee_with_exclusions'] = $oeeParams['oee_exclusions'];
            $return_arr['shift_length_with_exclusions'] = $calculation['shift_length_with_exclusions'];
        }
        return $return_arr;
    }

    public function generateGlobalSheet($objPHPExcel, $generatedGlobalData, $shifts_start, $shifts_end, $sensor_id) {
        if(empty($generatedGlobalData)) return;
        $workSheet = $objPHPExcel->setActiveSheetIndex(0);
        $workSheet->setTitle(__('Bendras'), false);
        $currentColumn = 'B';
        $currentRow = 1;
        $startRow = 3;
        $lossTypes = $this->LossType->find('all', array('fields' => array('id', 'CONCAT(name," (",unit,")") as full_name')));
        $problemsTreeList = $this->Problem->parseThreadedProblemsTitles($this->Problem->find('threaded'));
        #region headers
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Data'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Rodikliai'));
        $workSheet->getStyle($currentColumn . $currentRow)->getFont()->setBold(true);
        $workSheet->getRowDimension($startRow-1)->setRowHeight(30);
        foreach ($generatedGlobalData as $single_shift) {
            //$workSheet->setCellValue($currentColumn . $startRow, isset($single_shift['user']['User']['shift_title'])?$single_shift['user']['User']['shift_title']:'');
            $workSheet->setCellValue($currentColumn . ($startRow-1), $single_shift['shift_type'].' '.$single_shift['shift_start']." - \n".$single_shift['shift_end']);
            $workSheet->setCellValue($currentColumn . ($startRow-2), $single_shift['shift_disabled']?__('Išjungta pamaina'):__('Aktyvi pamaina'));
            $backgroundColor = $single_shift['shift_disabled']?self::$colors[3]:self::$colors[-1];
            $workSheet->getStyle($currentColumn.($startRow-2))->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => $backgroundColor))));
            $currentColumn++;
        }
        $workSheet->setCellValue($currentColumn . ($startRow-1), __('Aktyvių pamainų bendri duomenys'));
        $workSheet->getStyle($currentColumn.($startRow-2))->applyFromArray(array('fill' => array('type' => PHPExcel_Style_Fill::FILL_SOLID,'color' => array('rgb' => self::$colors[-1]))));
        $currentColumn ='A';

        $parameters = array(&$generatedGlobalData, &$workSheet, &$currentRow, &$startRow);
        $this->callPluginFunction('Report_UpdateGlobalSheetColumns_Hook', $parameters, Configure::read('companyTitle'));

        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Pamainos trukmė (min)'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Pamainos trukmė su išimtimis (min)'));
        }
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Gamybos laikas planas (min)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Gamybos laikas faktas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų laikas su išimtimis (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prastovų kiekis su išimtimis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Derinimo laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Derinimų kiekis (vnt)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Viršytų derinimų laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Teorinis derinimų laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra darbo laikas (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Gero greičio minučių kiekis (%)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Lėtesnių nei numatyta darbo minučių laikas'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prieinamumo koef. su planinėmis prastovomis'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Prieinamumo koef. su išimtimis'));
        }
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Efektyvumo koef.'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Kokybės koef.'));
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, __('OEE be planinių prastovų'));
        }
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('OEE su planinėmis prastovomis'));
        ++ $currentRow;
        // $workSheet->setCellValue($currentColumn . ++ $currentRow, 'Formavimas (vnt)');
        // $workSheet->setCellValue($currentColumn . ++ $currentRow, 'Linijos vidurys (vnt)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Viso supakuotas kiekis (m)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis kiekis per minutę'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis kiekis per minutę su išimtimis'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis kiekis per minutę (gamybos laikas)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Vidutinis 1 žmogaus greitis m/val'));
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Suplanuotas kiekis (vnt)');
        ++ $currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Kokybės koef.');
//        ++$currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Praradimai dėl kepimo (vnt)');
//        $workSheet->setCellValue($currentColumn . ++$currentRow, 'Praradimai dėl raikymo (vnt)');
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Deklaruotas brokas'));
        foreach ($lossTypes as $lt) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $lt[0]['full_name']);
        }
        ++ $currentRow;
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTTR (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTTR (pažymėtoms prastovoms, min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTBF (min)'));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, __('MTBF (pažymėtoms prastovoms, min)'));
        ++ $currentRow;

        #endregion

        $currentColumn ++;

        $total = array('shift_length'       => 0, 'theory_prod_time' => 0, 'fact_prod_time' => 0, 'true_problem_time' => 0,
            'true_problem_count' => 0, 'transition_time' => 0, 'transition_count' => 0,'no_work_time'=>0, 'exploitation_factor' => 0, 'quantity_type1' => 0,'exceeded_transition_time'=>0,
            'quantity_type2'     => 0, 'total_quantity' => 0, 'planned_quantity' => 0, 'good_quantity'=>0, 'all_quantity'=>0, 'losses12' => 0,
            'losses23'           => 0, 'losses_quantity' => array(), 'operational_factor' => 0, 'oee' => 0, 'mttf' => 0, 'mttr' => 0, 'count_delay'=>0,
            'true_problem_count_exclusions'=>0,'true_problem_time_exclusions'=>0,'mtt_problem_count'=>0,'mtt_problem_time'=>0, 'slow_speed_duration'=>0,
            'theorical_transition_time'=>0, 'avg_worker_speed'=>0
        );

        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $total['shift_length_with_exclusions'] = 0;
            $total['oee_with_exclusions'] = 0;
            $total['exploitation_factor_with_exclusions'] = 0;
        }

        $emptyShiftCount = 0;
        foreach ($generatedGlobalData as $single_shift) {
            $currentRow = $startRow;
            if (!array_key_exists('shift_length', $single_shift)) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra duomenų'));
                $workSheet->setCellValue($currentColumn . ++ $currentRow, __('Nėra duomenų'));
                $emptyShiftCount += 1;
                $currentColumn ++;
                continue;
            }
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['shift_length']);
            $total['shift_length'] += !$single_shift['shift_disabled']?$single_shift['shift_length']:0;
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['shift_length_with_exclusions']);
                $total['shift_length_with_exclusions'] += !$single_shift['shift_disabled']?$single_shift['shift_length_with_exclusions']:0;
            }
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['theory_prod_time']);
            $total['theory_prod_time'] += !$single_shift['shift_disabled']?$single_shift['theory_prod_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['fact_prod_time'],2));
            $total['fact_prod_time'] += !$single_shift['shift_disabled']?$single_shift['fact_prod_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['true_problem_time'],2));
            $total['true_problem_time'] += !$single_shift['shift_disabled']?$single_shift['true_problem_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['true_problem_count']);
            $total['true_problem_count'] += !$single_shift['shift_disabled']?$single_shift['true_problem_count']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['true_problem_time_exclusions'],2));
            $total['true_problem_time_exclusions'] += !$single_shift['shift_disabled']?$single_shift['true_problem_time_exclusions']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['true_problem_count_exclusions']);
            $total['true_problem_count_exclusions'] += !$single_shift['shift_disabled']?$single_shift['true_problem_count_exclusions']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['transition_time'],2));
            $total['transition_time'] += !$single_shift['shift_disabled']?$single_shift['transition_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['transition_count']);
            $total['transition_count'] += !$single_shift['shift_disabled']?$single_shift['transition_count']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exceeded_transition_time'],2));
            $total['exceeded_transition_time'] += !$single_shift['shift_disabled']?$single_shift['exceeded_transition_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['theorical_transition_time'],2));
            $total['theorical_transition_time'] += !$single_shift['shift_disabled']?$single_shift['theorical_transition_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['no_work_time'],2));
            $total['no_work_time'] += !$single_shift['shift_disabled']?$single_shift['no_work_time']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['fact_prod_time'] > 0?round(bcdiv($single_shift['fact_prod_time'] - $single_shift['slow_speed_duration'], $single_shift['fact_prod_time'],4) * 100,2):0);
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['slow_speed_duration'],2));
            $total['slow_speed_duration'] += !$single_shift['shift_disabled']?$single_shift['slow_speed_duration']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exploitation_factor'],2));
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['exploitation_factor_with_exclusions'],2));
            }
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['operational_factor'],2));

            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['quality_factor'],2));
            $total['good_quantity'] += !$single_shift['shift_disabled']?$single_shift['good_quantity']:0;
            $total['all_quantity'] += !$single_shift['shift_disabled']?$single_shift['all_quantity']:0;
            $total['count_delay'] += !$single_shift['shift_disabled']?$single_shift['count_delay']:0;
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['oee_with_exclusions'],2));
            }
            $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['oee'],2));
            ++ $currentRow;
            // $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['quantity_type1']);
            // $total['quantity_type1'] += $single_shift['quantity_type1'];
            // $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['quantity_type2']);
            // $total['quantity_type2'] += $single_shift['quantity_type2'];
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['total_quantity']);
            $total['total_quantity'] += !$single_shift['shift_disabled']?$single_shift['total_quantity']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['shift_length'] > 0?round($single_shift['total_quantity'] / $single_shift['shift_length'],2):0);
            if(isset($single_shift['shift_length_with_exclusions'])){
                if($single_shift['shift_length_with_exclusions'] > 0){
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, round($single_shift['total_quantity'] / $single_shift['shift_length_with_exclusions'],2));
                }else{
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, 0);
                }
            }else{++$currentRow;}
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['fact_prod_time'] > 0?round($single_shift['total_quantity'] / $single_shift['fact_prod_time'],2):0);
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['planned_quantity']);
            $total['planned_quantity'] += !$single_shift['shift_disabled']?$single_shift['planned_quantity']:0;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['avg_worker_speed'] > 0?round($single_shift['avg_worker_speed'],2):0);
            $total['avg_worker_speed'] += !$single_shift['shift_disabled']?$single_shift['avg_worker_speed']:0;
            ++$currentRow;
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['quality_factor']);
//            $total['quality_factor'] += $single_shift['quality_factor'];
//            ++$currentRow;
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['losses12']);
            //$total['losses12'] += !$single_shift['shift_disabled']?$single_shift['losses12']:0;
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['losses23']);
            // $total['losses23'] += !$single_shift['shift_disabled']?$single_shift['losses23']:0;
//            $workSheet->setCellValue($currentColumn . ++$currentRow, $single_shift['losses_quantity']);
            ++ $currentRow;
            foreach ($lossTypes as $lt) {
                if ($single_shift['losses_quantity'] == null || !array_key_exists($lt['LossType']['id'], $single_shift['losses_quantity'])) {
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, "0");
                } else {
                    $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['losses_quantity'][$lt['LossType']['id']][0]['losses']);
                    if (!array_key_exists($lt['LossType']['id'], $total['losses_quantity'])) {
                        $total['losses_quantity'][$lt['LossType']['id']] = 0;
                    }
                    if(!$single_shift['shift_disabled']){
                        $total['losses_quantity'][$lt['LossType']['id']] += $single_shift['losses_quantity'][$lt['LossType']['id']][0]['losses'];
                    }
                }
            }
            ++ $currentRow;
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['true_problem_count_exclusions'] > 0?round($single_shift['true_problem_time_exclusions'] / $single_shift['true_problem_count_exclusions'], 2):0);
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['mtt_problem_count'] > 0?round($single_shift['mtt_problem_time'] / $single_shift['mtt_problem_count'], 2):0);
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['true_problem_count_exclusions'] > 0?round(($single_shift['fact_prod_time'] + $single_shift['transition_time']) / $single_shift['true_problem_count_exclusions'], 2):0);
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $single_shift['mtt_problem_count'] > 0?round(($single_shift['fact_prod_time'] + $single_shift['transition_time']) / $single_shift['mtt_problem_count'], 2):0);
            $currentColumn ++;
        }
        $validShiftCount = count($generatedGlobalData);

        $currentRow = $startRow;

        #region bendri
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['shift_length']);
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['shift_length_with_exclusions']);
        }
//        $workSheet->setCellValue($currentColumn . ++$currentRow, $total['theory_prod_time']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['fact_prod_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['true_problem_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['true_problem_count']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['true_problem_time_exclusions'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['true_problem_count_exclusions']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['transition_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['transition_count']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['exceeded_transition_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['theorical_transition_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['no_work_time'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['fact_prod_time'] > 0?round(bcdiv($total['fact_prod_time'] - $total['slow_speed_duration'], $total['fact_prod_time'],4) * 100,2):0);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['slow_speed_duration'],2));
        //$workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['exploitation_factor'] / $validShiftCount, 2));
        $oeeParams = $this->Record->calculateOee($total);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['exploitation_factor'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['exploitation_factor_exclusions'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['operational_factor'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['quality_factor'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['oee_exclusions'],2));
        $workSheet->setCellValue($currentColumn . ++ $currentRow, round($oeeParams['oee'],2));
        ++ $currentRow;
        //$workSheet->setCellValue($currentColumn . ++ $currentRow, $total['quantity_type1']);
        //$workSheet->setCellValue($currentColumn . ++ $currentRow, $total['quantity_type2']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['total_quantity']);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['shift_length'] > 0?round($total['total_quantity'] / $total['shift_length'], 2):0);
        if(isset($total['shift_length_with_exclusions'])){
            $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['shift_length_with_exclusions']>0?round($total['total_quantity'] / $total['shift_length_with_exclusions'], 2):0);
        }else{++$currentRow;}
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['fact_prod_time'] > 0?round($total['total_quantity'] / $total['fact_prod_time'], 2):0);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['avg_worker_speed'] > 0?round($total['avg_worker_speed'], 2):0);
//        $workSheet->setCellValue($currentColumn . ++$currentRow, $total['planned_quantity']);
        ++ $currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, round($total['quality_factor'] / $validShiftCount, 2));
//        ++$currentRow;
//        $workSheet->setCellValue($currentColumn . ++$currentRow, $total['losses12']);
//        $workSheet->setCellValue($currentColumn . ++$currentRow, $total['losses23']);
//        $workSheet->setCellValue($currentColumn . ++$currentRow, $total['losses_quantity']);
        ++ $currentRow;
        foreach ($lossTypes as $lt) {
            $workSheet->setCellValue($currentColumn . ++ $currentRow, (array_key_exists($lt['LossType']['id'], $total['losses_quantity'])) ? $total['losses_quantity'][$lt['LossType']['id']] : 0);
        }
        ++ $currentRow;
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['true_problem_count_exclusions'] > 0?round($total['true_problem_time_exclusions'] / $total['true_problem_count_exclusions'], 2):0);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['mtt_problem_count'] > 0?round($total['mtt_problem_time'] / $total['mtt_problem_count'], 2):0);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['true_problem_count_exclusions'] > 0?round(($total['fact_prod_time'] + $total['transition_time']) / $total['true_problem_count_exclusions'], 2):0);
        $workSheet->setCellValue($currentColumn . ++ $currentRow, $total['mtt_problem_count'] > 0?round(($total['fact_prod_time'] + $total['transition_time']) / $total['mtt_problem_count'], 2):0);
        ++ $currentRow;
        ++ $currentRow;
        //$workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['mttf'] / $validShiftCount, 2));
        //$workSheet->setCellValue($currentColumn . ++ $currentRow, round($total['mttr'] / $validShiftCount, 2));
        #endregion

        /*$found_problems = $this->FoundProblem->withRefs()->find('all', array(
            'fields'     => array(
                'FoundProblem.start', 'FoundProblem.end',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN FoundProblem.start > \'' . $shifts_start . '\' THEN FoundProblem.start ELSE \'' . $shifts_start . '\' END,
                    CASE WHEN FoundProblem.end < \'' . $shifts_end . '\' THEN FoundProblem.end ELSE \'' . $shifts_end . '\' END)) as found_problem_duration',
                'Problem.id', 'Problem.name',
                'FoundProblem.plan_id'
            ),
            'conditions' => array(
                'FoundProblem.start <'      => $shifts_end,
                'FoundProblem.end >'        => $shifts_start,
                'FoundProblem.sensor_id'    => $sensor_id,
                'FoundProblem.problem_id >' => 2,
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN FoundProblem.start > \'' . $shifts_start . '\' THEN FoundProblem.start ELSE \'' . $shifts_start . '\' END,
                    CASE WHEN FoundProblem.end < \'' . $shifts_end . '\' THEN FoundProblem.end ELSE \'' . $shifts_end . '\' END)) > 60'
            )
        ));*/
        $shift['Shift'] = array('start'=>$shifts_start, 'end'=>$shifts_end);
        //$found_problems = $this->FoundProblem->getByShiftForReport($shift, $sensor_id,0);
        ++ $currentRow;
        ++ $currentRow;

        $workSheet->setCellValue('A' . $currentRow, __('Visų pamainų įvykių žurnalas'));
        $letterStart = 'A'; $currentRow++;
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Darbo centras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('MO'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio pavadinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Gaminio kodas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Paaiškinimas'));
        for($i=0; $i <= $this->maxDowntimeLevel; $i++){
            $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas %d', $i+1));
        }
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pradžia'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Pabaiga'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė tarp prastovų (min)'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Komentaras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Plano greitis (step) vnt/h'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Kiekis pakuotėje/porcijoje'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':Q' . $currentRow)->getFont()->setBold(true);

        ++ $currentRow;

        $start_column = 'A';
        $previous_date = null;

        $global_mttr = 0;
        $global_mtbf = 0;
        $totalByProblemType = array();
        $realProblemsCount = 0;
        /*foreach ($found_problems as $fp) {
            try{
                $approvedAddData = json_decode($fp['ApprovedOrder']['additional_data']);
            }catch(Exeption $e){
                $approvedAddData = null;
            }
            $fp['Sensor']['name'] = $fp['Sensor']['name'].' '.(isset($fp['Factory']['name'])?$fp['Factory']['name']:'');
            $workSheet->setCellValue($start_column . $currentRow, $fp['Sensor']['name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, isset($approvedAddData->add_order_number)?$approvedAddData->add_order_number:$fp['Plan']['mo_number']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_name']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['production_code']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['DiffCard']['description']);
            $problemId = $fp['FoundProblem']['transition_problem_id'] > 0?$fp['FoundProblem']['transition_problem_id']:$fp['Problem']['id'];
            $parsedProblemsTree = $problemId > 0 && isset($problemsTreeList[$problemId])?array_reverse($problemsTreeList[$problemId]):array();
            for($i=0; $i <= $this->maxDowntimeLevel; $i++){
                if(isset($parsedProblemsTree[$i])){
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($parsedProblemsTree[$i]));
                }else{
                    $workSheet->setCellValue(++ $start_column . $currentRow, Settings::translate($fp['Problem']['name']));
                }
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['start']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['end']);
            $workSheet->setCellValue(++ $start_column . $currentRow, round($fp[0]['found_problem_duration'] / 60,2));
            if ($previous_date == null && strtotime($fp['FoundProblem']['start']) >= strtotime($shifts_start)) {
                $previous_date = $shifts_start;
            }
            $date_diff = 0;
            if($fp['FoundProblem']['problem_id'] >= Problem::ID_NOT_DEFINED && !in_array($fp['FoundProblem']['problem_id'], $this->downtimesExclusionsIds)) {
                $realProblemsCount++;
                $global_mttr += $fp[0]['found_problem_duration'];
                if ($previous_date != null) {
                    $date_diff = strtotime($fp['FoundProblem']['start']) - strtotime($previous_date);
                    $date_diff = $date_diff > 0?$date_diff:0;
                    $global_mtbf += $date_diff;
                }
                $previous_date = $fp['FoundProblem']['end'];
            }else{ //kadangi mtbf yra tarpu tarp prastovu vidurkis, tai is esmes tai yra darbo, derinimo ir exlusion prastovu sumos dalyba is tikru prastovu kiekio
                $global_mtbf += $fp[0]['found_problem_duration'];
            }
            $workSheet->setCellValue(++ $start_column . $currentRow, ($date_diff > 0) ? round($date_diff / 60, 2) : "");
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['FoundProblem']['comments']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['step']);
            $workSheet->setCellValue(++ $start_column . $currentRow, $this->getIntervalQuantity($fp));
            $workSheet->setCellValue(++ $start_column . $currentRow, $fp['Plan']['box_quantity']);
            //$start_column++;
            $backgroundColor = isset(self::$colors[$fp['FoundProblem']['problem_id']])?self::$colors[$fp['FoundProblem']['problem_id']]:self::$colors[3];
            $backgroundColor = in_array($fp['FoundProblem']['problem_id'], $this->downtimesExclusionsIds)?self::$colors[2]:$backgroundColor;
            //do{
                $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->applyFromArray(
                    array(
                        'fill' => array(
                            'type' => PHPExcel_Style_Fill::FILL_SOLID,
                            'color' => array('rgb' => $backgroundColor)
                        )
                    )
                );
            //}while($bgCol != $start_column);
            $start_column = 'A';
            $currentRow ++;
            if(!isset($totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']])){
                $totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']] = array('problem_name'=>$fp['Problem']['name'], 'total'=>0,'sensor'=>$fp['Sensor']['name']);
            }
            $totalByProblemType[$fp['Sensor']['id']][$fp['FoundProblem']['problem_id']]['total'] += strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start']);
        }
        //if ($previous_date != null) { // If last, add difference till next shift
            //$global_mtbf += strtotime($shifts_end) - strtotime($previous_date);
        //}
        $currentRow ++;
        $letterStart = 'A';
        $workSheet->setCellValue($letterStart . $currentRow, __('Visų pamainų susumuotas įvykių laikų žurnalas'));
        $workSheet->setCellValue($letterStart++ . ++ $currentRow, __('Darbo centras'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Prastovos pavadinimas'));
        $workSheet->setCellValue($letterStart++ . $currentRow, __('Trukmė (min)'));
        $workSheet->getStyle('A' . ($currentRow - 1) . ':C' . $currentRow)->getFont()->setBold(true);
        foreach($totalByProblemType as $sensorDowntimes){
            foreach($sensorDowntimes as $sensorDowntime){
                $letterStart = 'A';
                $currentRow++;
                $workSheet->setCellValue($letterStart++ . $currentRow, $sensorDowntime['sensor']);
                $workSheet->setCellValue($letterStart++ . $currentRow, Settings::translate($sensorDowntime['problem_name']));
                $workSheet->setCellValue($letterStart++ . $currentRow, bcdiv($sensorDowntime['total'],60,2));
            }
        }
        if ($realProblemsCount > 0) {
            $global_mtbf /= $realProblemsCount;
            $global_mttr /= $realProblemsCount;
        } else {
            $global_mtbf = 0;
            $global_mttr = 0;
        }

        ++ $currentRow;
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTTR (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($global_mttr/60, 2));
        $workSheet->setCellValue('A' . ++ $currentRow, __('MTBF (min)'));
        $workSheet->setCellValue('B' . $currentRow, round($global_mtbf/60, 2));
        */
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $approvedOrderFromProductData = $this->ApprovedOrder->find('all', array(
            'fields'=>array(
                'Sensor.name',
                'Plan.id',
                'Plan.production_name',
                'Plan.production_code',
                'AVG(Plan.step) AS step',
                'SUM(Plan.preparation_time) AS preparation_time',
                'GROUP_CONCAT(ApprovedOrder.id)',
                'COUNT(DISTINCT(ApprovedOrder.id)) AS production_count',
                'SUM(IF(Sensor.plan_relate_quantity = "quantity", ApprovedOrder.box_quantity, ApprovedOrder.quantity)) AS production_quantity',
            ),'conditions'=>array(
                'ApprovedOrder.created <' => $shifts_end,
                'ApprovedOrder.end >' => $shifts_start,
                'ApprovedOrder.sensor_id' => $sensor_id,
                'Plan.product_id >' => 0,
            ),'group'=>array('Plan.production_code'),
            'order'=>array('ApprovedOrder.created')
        ));
        if(!empty($approvedOrderFromProductData)){
            $this->FoundProblem->bindModel(array('belongsTo'=>array('Plan')));
            $this->FoundProblem->virtualFields = array('production_duration'=>'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end))/60');
            $downtimesIdList = array(Problem::ID_WORK, Problem::ID_NEUTRAL);
            $transitionExceededId = trim(ClassRegistry::init('Settings')->getOne('exceeded_transition_id'));
            if($transitionExceededId > 0){ $downtimesIdList[] = trim($transitionExceededId); }
            $approvedOrderFromProductDurationsTmp = $this->FoundProblem->find('all', array(
                'recursive'=>1,
                'fields'=>array(
                    'FoundProblem.problem_id',
                    'Plan.production_code',
                    'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end))/60 AS production_duration'
                ),'conditions'=>array(
                    'FoundProblem.start <' => $shifts_end,
                    'FoundProblem.end >' => $shifts_start,
                    'FoundProblem.sensor_id' => $sensor_id,
                    'FoundProblem.problem_id' => $downtimesIdList,
                    'Plan.product_id >' => 0,
                ),'group'=>array('Plan.production_code','FoundProblem.problem_id')
            ));
            $approvedOrderFromProductDurations = array();
            foreach($approvedOrderFromProductDurationsTmp as $approvedOrderFromProductDuration){
                $productionCode = $approvedOrderFromProductDuration['Plan']['production_code'];
                $problemId = $approvedOrderFromProductDuration['FoundProblem']['problem_id'];
                $approvedOrderFromProductDurations[$productionCode][$problemId] = $approvedOrderFromProductDuration[0]['production_duration'];
            }
            unset($approvedOrderFromProductDurationsTmp);
            $start_column = 'A';
            $workSheet->setCellValue($start_column++ . $currentRow, __('Darbo centras'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Gaminio pavadinimas'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Gaminio kodas'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Planinis greitis (vnt/val)'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Planinis perėjimo laikas (min)'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Kiek kartų gaminta'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Kiekis'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Gamybos laikas (min.)'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Vidutinis greitis (vnt/min)'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Perėjimo laikas (min)'));
            $workSheet->setCellValue($start_column++ . $currentRow, __('Derinimo viršijimas (min)'));
            $workSheet->getStyle('A'.$currentRow.':'.$start_column.$currentRow)->getFont()->setBold(true);
            foreach($approvedOrderFromProductData as $rowData){
                $productionCode = $rowData['Plan']['production_code'];
                $productionTime = isset($approvedOrderFromProductDurations[$productionCode][Problem::ID_WORK])?number_format($approvedOrderFromProductDurations[$productionCode][Problem::ID_WORK],2,'.',''):0;
                $transitionTime = isset($approvedOrderFromProductDurations[$productionCode][Problem::ID_NEUTRAL])?number_format($approvedOrderFromProductDurations[$productionCode][Problem::ID_NEUTRAL],2,'.',''):0;
                $transitionOverflowTime = isset($approvedOrderFromProductDurations[$productionCode][$transitionExceededId])?number_format($approvedOrderFromProductDurations[$productionCode][$transitionExceededId],2,'.',''):0;
                $currentRow++; $start_column = 'A';
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData['Sensor']['name']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData['Plan']['production_name']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData['Plan']['production_code']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData[0]['step']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData[0]['preparation_time']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData[0]['production_count']);
                $workSheet->setCellValue($start_column++ . $currentRow, $rowData[0]['production_quantity']);
                $workSheet->setCellValue($start_column++ . $currentRow, $productionTime);
                $workSheet->setCellValue($start_column++ . $currentRow, $productionTime > 0?bcdiv($rowData[0]['production_quantity'],$productionTime,2):0);
                $workSheet->setCellValue($start_column++ . $currentRow, $transitionTime);
                $workSheet->setCellValue($start_column++ . $currentRow, $transitionOverflowTime);
            }
            $this->FoundProblem->virtualFields = array();
        }

        for ($col = 'A'; $col !== 'Z'; $col ++) {
            $workSheet
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }
    }

    public function getWeekleXLSData($requestData, $periodDetails){
        $date_start = $requestData['start_date'];
        $date_end = $requestData['end_date'];
        $dateTypes = array('week'=>'W', 'month'=>'m', 'day'=>'z');
        $recordModel = ClassRegistry::init('Record');
        $start = strtotime($date_start);
        $periodsList = array();
        while($start < strtotime($date_end)){
            $periodsList[] = date('Y', $start).'-'.(int)date($dateTypes[$periodDetails['type']], $start);
            $start = strtotime('+1 '.$periodDetails['type'], $start);
        }
        $conditions = array(
            "CONCAT(DashboardsCalculation.year,'-',DashboardsCalculation.{$periodDetails['type']})" => $periodsList,
        );
        if(isset($requestData['sensors']) && !empty($requestData['sensors'])){
            $conditions['DashboardsCalculation.sensor_id']=$requestData['sensors'];
            $conditions['DashboardsCalculation.day'] = 0;
            $conditions['DashboardsCalculation.month'] = 0;
            $conditions['DashboardsCalculation.week'] = 0;
            if(isset($conditions['DashboardsCalculation.'.$periodDetails['type']])){ unset($conditions['DashboardsCalculation.'.$periodDetails['type']]); }
        }
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'TRIM(DashboardsCalculation.'.$col.') AS '.$col; });
        $fields[] = 'TRIM(Sensor.branch_id) AS branch_id';
        $fields[] = 'TRIM(DashboardsCalculation.good_quantity / DashboardsCalculation.all_quantity) AS quality_factor';
        $this->DashboardsCalculation->bindModel(array('belongsTo'=>array('Sensor')));
        $calculation = $this->DashboardsCalculation->find('all', array(
            'fields'=>array_merge($fields, array(
                'CONCAT(DashboardsCalculation.year,\'-\',DashboardsCalculation.'.$periodDetails['type'].', \''.$periodDetails['time_title_mechanism'].'\') AS nr',
            )),
            'conditions' => $conditions,
            //'group'=>array(
            //    'DashboardsCalculation.sensor_id',
            //),
            'order'=>array('DashboardsCalculation.sensor_id', 'DashboardsCalculation.'.$periodDetails['type'])
        ));
        $fields = array_keys($this->DashboardsCalculation->schema());
        array_walk($fields, function(&$col){ $col = 'SUM(DashboardsCalculation.'.$col.') AS '.$col; });
        $summarizedCalculation = $this->DashboardsCalculation->find('all', array(
            'fields'=>array_merge($fields, array(
                'CONCAT(DashboardsCalculation.year,\'-\',DashboardsCalculation.'.$periodDetails['type'].', \''.$periodDetails['time_title_mechanism'].'\') AS nr',
            )),
            'conditions' => $conditions,
            'group' => key($conditions),
            'order'=>array('DashboardsCalculation.'.$periodDetails['type'])
        ));
        foreach($summarizedCalculation as &$calc){
            $oeeParams = $recordModel->calculateOee($calc[0]);
            $calc[0]['exploitation_factor'] = $oeeParams['exploitation_factor'];
            $calc[0]['exploitation_factor_with_exclusions'] = $oeeParams['exploitation_factor_exclusions'];
            $calc[0]['operational_factor'] = $oeeParams['operational_factor'];
            $calc[0]['oee'] = $oeeParams['oee'];
            $calc[0]['oee_with_exclusions'] = $oeeParams['oee_exclusions'];
            $calc[0]['sensor_id'] = 0;
            $calc[0]['quality_factor'] = $oeeParams['quality_factor'];
        }
        $calculation = array_merge($calculation, $summarizedCalculation);
        return $calculation;
    }

    private function getIntervalQuantity($data){
        if($data['Sensor']['plan_relate_quantity'] == 'quantity'){ return $data['FoundProblem']['quantity']; }
        $multiplier = 1;
        if(isset($data['ApprovedOrder']) && !empty($data['ApprovedOrder']) && $data['ApprovedOrder']['box_quantity'] > 0){
            $multiplier = bcdiv($data['ApprovedOrder']['quantity'],$data['ApprovedOrder']['box_quantity'],6);
        }
        return bcmul($data['FoundProblem']['quantity'],$multiplier,2);
    }
}
