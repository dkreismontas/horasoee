<?php
App::uses('AppModel', 'Model');
App::uses('Mediresta', 'Mediresta.Model');

class MedirestaAppModel extends AppModel {
    public static $navigationAdditions = array( );

    public function getWCSidebarLinks(){
        if(strtolower(Configure::read('companyTitle')) != 'mediresta'){ return false; }
        $url = Router::url(array('plugin'=>'mediresta','controller'=>'losses', 'action'=>'registrate'),true);
        $javascript = '<script type="text/javascript">
        jQuery(document).ready(function($){
            $(".quick-btn-toolbar").on("click", ".declare-losses,.register-event", function(){
                $("#declareLossesBlock").html("<img src=\"/images/loaders/loader12.gif\" />");
                $("#declareLossesBlockOnFinish").html("<img src=\"/images/loaders/loader12.gif\" />");
                //$("#totalLossesBlock").html("");
                $.ajax({
                   url: "'.Router::url('/mediresta/losses/registrate/',true).'"+window.sensorId,
                   type: "POST",
                   data: {layout: "ajax"},
                   context: $(this),
                   success: function(data){
                       //$("#declareLossesBlock").html($(data).find("table").parent().html());
                       //let table = $(data).find(".row-fluid").parent().html();
                       $("#declareLossesBlock").html(data);
                       $("#declareLossesBlockOnFinish").html(data);
                   }
               })
            });
             $(".quick-btn-toolbar").on("click", "#workersCountButton", function(){
                $("#workersCountForm").html(""); 
                $("#workersCountForm").html($("select[name=\"add_workers_count\"]").closest("form").html()+"<button class=\"btn btn-primary confirm\">Patvirtinti</button> ");
                $("#workersCountForm label").remove();
                let workersCount = $(this).attr("data-workers_count");
                $("#workersCountForm select option:first-child").remove();
                $("#workersCountForm select").val(workersCount);
                $("#workersCountHtml .workers_count span").html(workersCount);
                popupWindow("#workersCountHtml", "auto", "500","auto", "'.__('Darbuotojų skaičius').'", "'.__('Uždaryti langą').'");
             });
             $("body").on("click", "#workersCountHtml form button.confirm", function(){
                 let workersCount = parseInt($(this).closest("form").find("select").val());
                 if(Number.isNaN(workersCount)){
                     alert("Įveskite darbuotojų skaičių");
                     return false;
                 }
                 $("#workersCountButton").data("workers_count", workersCount);
                 $.ajax({
                   url: "'.Router::url('/mediresta/plugins/change_workers_count/',true).'",
                   type: "POST",
                   data: {workers_count: workersCount, sensor_id: window.sensorId},
                   success: function(data){
                       $("#workersCountButton").attr("data-workers_count", workersCount);
                       $(".ui-dialog-titlebar-close").trigger("click");
                   }
               })
             });
             MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
              var observer = new MutationObserver(function(mutations, observer) {
                if(mutations[0].attributeName == "value" && parseInt(mutations[0].oldValue) > 0 && !window.baseUriDc) {
                    $("#workersCountButton").trigger("click");
                }
              });
              observer.observe($("#currentShiftId")[0], {
                attributes: true,
                attributeOldValue: true
              });
        });
        </script>';
        $hasAccessToCreatePallet = self::hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'edit_pallet'));
        $hasAccessToTransferPallet = self::hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'transfer_pallets'));
        $hasAccessToRegisterPacket = self::hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'register_packet'));
        $registruotiPaketusButton = $hasAccessToCreatePallet?'<a href="'.Router::url(array('plugin'=>'mediresta','controller'=>'registrations','action'=>'edit_pallet')).'" target="_blank" ng-if="sensor.mediresta_sensor_type == '.(Mediresta::SKENAVIMAS_TYPE).' && currentPlan" title="'.__('Registruoti paketus').'" class="btn btn-warning">'.__('Registruoti paketus').'</a>':'';
        $paleciuPerkelimasButton = $hasAccessToTransferPallet?'<a href="'.Router::url(array('plugin'=>'mediresta','controller'=>'registrations','action'=>'transfer_pallets')).'/{{sensor.id}}" target="_blank" ng-if="sensor.mediresta_sensor_type == '.(Mediresta::DYGIAVIMAS_TYPE).' && currentPlan" title="'.__('Palečių perkėlimas').'" class="btn btn-warning">'.__('Palečių perkėlimas').'</a>':'';
        $produkcijosRegistravimasButton = $hasAccessToRegisterPacket?'<a href="'.Router::url(array('plugin'=>'mediresta','controller'=>'registrations','action'=>'register_packet')).'" target="_blank" ng-if="sensor.mediresta_sensor_type == '.(Mediresta::PRESAVIMAS_TYPE).' && currentPlan" title="'.__('Produkcijos registravimas').'" class="btn btn-warning">'.__('Produkcijos registras').'</a>':'';
        $workersCountHtml = '
            <input type="hidden" id="currentShiftId" value="{{currentShift.Shift.id}}" />
            <div style="display: none"><div id="workersCountHtml"><h4 class="workers_count">Šiuo metu dirba: <span>{{sensor.workers_count}}</span> darbuotojai</h4><form id="workersCountForm"></form></div></div>
            <a id="workersCountButton" data-workers_count="{{sensor.workers_count}}" ng-if="sensor.mediresta_sensor_type == '.(Mediresta::SKENAVIMAS_TYPE).' && currentPlan" title="'.__('Darbuotojų skaičius').'" class="btn btn-warning">'.__('Darbuotojų skaičius').'</a>
            '.$registruotiPaketusButton.$paleciuPerkelimasButton.$produkcijosRegistravimasButton.'
        ';
        return
            $javascript.'<a ng-if="sensor.mediresta_sensor_type == '.(Mediresta::OBLIAVIMAS_TYPE).'" target="_blank" href="'.$url.'/{{sensor.id}}" title="'.__('Defektų registras').'" class="btn btn-warning">'.__('Defektų registras').'</a>'.
            $workersCountHtml;
    }
    
}
