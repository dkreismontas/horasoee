<?php
class Mediresta extends AppModel{

     public $useTable = false;
     CONST SKENAVIMAS_TYPE = 1;
     CONST DYGIAVIMAS_TYPE = 2;
     CONST OBLIAVIMAS_TYPE = 3;
     CONST PRESAVIMAS_TYPE = 4;
     private static $avgSpeedPerPerson = array();

    public function getWCSidebarLinks(){
        $url = Router::url(array('plugin'=>'mediresta','controller'=>'losses', 'action'=>'registration'),true);
        return '<div ng-if="sensor.mediresta_sensor_type == '.self::OBLIAVIMAS_TYPE.'"><a target="_blank" href="'.$url.'/index/sensorId:{{sensor.id}}" title="'.__('Defektų deklaravimas').'" class="btn btn-warning ng-scope">'.__('Defektų deklaravimas').'</a></div>';
    }

    public function changeUpdateStatusVariablesHook($fSensor, &$plans, &$currentPlan, &$quantityOutput, $fShift){
        $recordModel = ClassRegistry::init('Record');
        $lossModel = ClassRegistry::init('Loss');
        $qantityField = $fSensor['Sensor']['plan_relate_quantity'];
        $quantity = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.'.$qantityField.') AS total'),
            'conditions'=>array('Record.sensor_id'=>$fSensor['Sensor']['id'], 'Record.shift_id'=>$fShift['Shift']['id'],'Record.plan_id >'=>0)
        ));
        $quantityValue = $quantity[0]['total'] > 0?$quantity[0]['total']:0;
        $quantityOutput['quantity'] = number_format($quantityValue,2,'.','');
        $losses = $lossModel->find('first', array(
            'fields'=>array('SUM(Loss.value) AS losses'),
            'conditions'=>array('Loss.created_at >='=>$fShift['Shift']['start'])
        ));
        if($losses[0]['losses'] > 0){
            $quantityOutput['quantity'] .= ' / '.$losses[0]['losses'];
        }
        if(isset($currentPlan['ApprovedOrder'])){
            $currentPlan['Plan']['original_step'] = $currentPlan['Plan']['step'];
        }
        foreach($plans as &$plan){
            $plan['Plan']['original_step'] = $plan['Plan']['step'];
        }
    }

    public function WorkCenter_index_Hook(&$data, &$fSensor){
        if($fSensor['Sensor']['mediresta_sensor_type'] == self::SKENAVIMAS_TYPE) {
            $data['structure']['add_workers_count'] = array('title'=>__('Darbuotojų skaičius'), 'additional_info'=>true, 'type'=>'select', 'required'=>__('Pasirinkite darbuotojų skaičių'), 'outputOnTable'=>false, 'options'=>array(4=>4,5=>5,6=>6,7=>7,8=>8));
        }
    }

    public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder,&$fPlan,&$fSensor){
        $appOrder['ApprovedOrder']['creation_moment'] = date('Y-m-d H:i:s');
        $dataFromInputForm = json_decode($appOrder['ApprovedOrder']['additional_data'], true);
        if(isset($dataFromInputForm['add_workers_count'])){
            $sensorModel = ClassRegistry::init('Sensor');
            $recordModel = ClassRegistry::init('Record');
            $sensorModel->updateAll(array('Sensor.workers_count'=>$dataFromInputForm['add_workers_count']), array('Sensor.id'=>$fSensor['Sensor']['id']));
            $recordModel->updateAll(
                array(
                    'workers_count'=>$dataFromInputForm['add_workers_count'],
                ),array(
                    'Record.created >='=>$appOrder['ApprovedOrder']['created'],
                    'Record.sensor_id' => $fSensor['Sensor']['id'],
                    'Record.plan_id IS NULL'
                )
            );
        }
    }

    public function verificationHook($recordModel, $currentPlan, &$fFoundProblem, &$fSensor){
        if(!$currentPlan || $fSensor['Sensor']['mediresta_sensor_type'] != self::DYGIAVIMAS_TYPE){
            return array('promptVerification'=>false);
        }
        $revisesModel = ClassRegistry::init('RevisesData');
        $lastRevise = $revisesModel->find('first', array(
            'conditions'=>array('RevisesData.approved_order_id'=>$currentPlan['ApprovedOrder']['id']),
            'order'=>array('RevisesData.id DESC')
        ));
        if(!empty($lastRevise) && !$lastRevise['RevisesData']['confirmed']){
            return array('promptVerification'=>true, 'text'=>__('<h3>Ar atlikta lentos tiesumo patikra?</h3><h3>Ar atlikta klijų kiekio dygyje patikra?</h3><h3>Ar patikrintas lentų preso slėgis?</h3>'));
        }
    }

    public function Workcenter_beforeRegisterVerification_Hook(&$currentPlan,&$fSensor){
        $revisesModel = ClassRegistry::init('RevisesData');
        $lastRevise = $revisesModel->find('first', array(
            'conditions'=>array('RevisesData.approved_order_id'=>$currentPlan['ApprovedOrder']['id']),
            'order'=>array('RevisesData.id DESC')
        ));
        if(!empty($lastRevise)){
            $revisesModel->id = $lastRevise['RevisesData']['id'];
            $revisesModel->save(array('user_id'=>Configure::read('user')->id, 'confirmed'=>date('Y-m-d H:i:s')));
        }
        die();
    }

    public function createRevises($sensorsIdToNameList){
        $recordModel = ClassRegistry::init('Record');
        $revisesModel = ClassRegistry::init('RevisesData');
        $foundProblemModel = ClassRegistry::init('FoundProblem');
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');

        $approvedOrderModel->bindModel(array('belongsTo'=>array('Sensor')));
        $activePlans = $approvedOrderModel->find('all',array(
            'fields'=>array(
                'ApprovedOrder.id',
                'ApprovedOrder.creation_moment',
                'ApprovedOrder.sensor_id',
            ),'conditions'=>array(
                'ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS,
                'Sensor.mediresta_sensor_type' => self::DYGIAVIMAS_TYPE,
                'Sensor.id' => array_keys($sensorsIdToNameList),
            ),'group'=>array('ApprovedOrder.sensor_id')
        ));
        if(empty($activePlans)){ return false; }
        $promptAfterSpecificCountReached = 25;
        $problemDurationToCreateRevise = 1200;
        $promptEveryCountOriginal = 150;
        $revisesList = Hash::combine($revisesModel->find('all', array(
            'fields' => array(
                'GROUP_CONCAT(RevisesData.found_problem_id) as found_problem_ids_used',
                'SUM(RevisesData.first_revise_by_count) AS first_revise_by_count',
                'MAX(RevisesData.created) AS created',
                'GROUP_CONCAT(RevisesData.overflow_quantity) AS overflow_quantity',
                'TRIM(RevisesData.sensor_id) AS sensor_id'
            ), 'conditions' => array('RevisesData.approved_order_id' => Set::extract('/ApprovedOrder/id', $activePlans)),
            'group'=>array('RevisesData.sensor_id')
        )),'{n}.0.sensor_id', '{n}.0');
        foreach($activePlans as $currentPlan) {
            $infinityLoopCounter = 0;
            while(true) {
                if($infinityLoopCounter++ >= 500){ break; }
                $promptEveryCount = $promptEveryCountOriginal;
                $sensorId = $currentPlan['ApprovedOrder']['sensor_id'];
                $revises = $revisesList[$sensorId] ?? array();
                $madedCountSearchStart = $currentPlan['ApprovedOrder']['creation_moment']; //ieskome nuo uzsakymo sukurimo
                if (!empty($revises) && $revises['first_revise_by_count']) { //jei atlikta pirma patikra pagal kieki, skaiciuojame nuo paskutines patikros
                    $madedCountSearchStart = $revises['created'];
                } else {
                    $promptEveryCount = $promptAfterSpecificCountReached;
                }
                $madedCount = $recordModel->find('first', array(
                    'fields' => array(
                        'SUM(Record.unit_quantity) AS sum',
                    ), 'conditions' => array(
                        'Record.approved_order_id' => $currentPlan['ApprovedOrder']['id'],
                        'Record.created >' => $madedCountSearchStart,
                        'Record.found_problem_id IS NULL'
                    )
                ));
                //Kadangi irasai atsiuncia kiekius > 0, kai paskutinis atejes kiekis suformuoja patikra, gali susidaryti porcijos kiekio pervirsis. Si kieki perkeliamas i kita porcija sumazinant kiekio poreikio per kiekio pervirsi is praejusios patikros
                $lastQuantityOverflow = !empty($revises) ? (int)current(array_reverse(explode(',', $revises['overflow_quantity']))) : 0;
                $madedCount[0]['sum'] += $lastQuantityOverflow;
                $newReviseData = array(
                    'sensor_id' => $sensorId,
                    'approved_order_id' => $currentPlan['ApprovedOrder']['id'],
                    'created' => date('Y-m-d H:i:s')
                );
                //patikrame ar dabar vyksta gamyba ir pries ja ejusi prastova netruko ilgiau kaip 20min ir uz ta prastova dar nera sukurta patikra
                $createRevise = false;
                $lastRealProblem = $foundProblemModel->findLastRealProblem($sensorId);
                if (!empty($lastRealProblem) && $lastRealProblem['FoundProblem']['state'] == FoundProblem::STATE_COMPLETE && $lastRealProblem['FoundProblem']['approved_order_id'] == $currentPlan['ApprovedOrder']['id'] && !in_array($lastRealProblem['FoundProblem']['id'], explode(',', $revises['found_problem_ids_used']))) {
                    $lastRealProblemDuration = strtotime($lastRealProblem['FoundProblem']['end']) - strtotime($lastRealProblem['FoundProblem']['start']);
                    if ($lastRealProblemDuration >= $problemDurationToCreateRevise) {
                        $newReviseData['found_problem_id'] = $lastRealProblem['FoundProblem']['id'];
                        $createRevise = true;
                    }
                }
                if ($madedCount[0]['sum'] >= $promptEveryCount) {//1. ismesti lentele patikrai jei pagamino reikiama kieki nuo gamybos pradzios
                    if ($promptEveryCount == $promptAfterSpecificCountReached) {
                        $newReviseData['first_revise_by_count'] = 1;
                    }
                    $newReviseData['overflow_quantity'] = max(0, $madedCount[0]['sum'] - $promptEveryCount);
                    $createRevise = true;
                }
                if ($createRevise) {
                    $revisesModel->create();
                    $revisesModel->save($newReviseData);
                    if(isset($newReviseData['overflow_quantity']) && $newReviseData['overflow_quantity'] >= $promptEveryCountOriginal){
                        $revisesList[$sensorId] = array_merge($revises, $newReviseData);
                    }else{ break; }
                }else{
                    break;
                }
            }
        }
    }

    public function WorkCenter_BeforeAddPeriodsToHours_Hook(&$hPeriods, &$periodData, $fSensor, $graphHoursLinesCount){
        static $revisesDataModel, $createdRevises, $confirmedRevises = null;
        if($revisesDataModel == null){
            $revisesDataModel = ClassRegistry::init('RevisesData');
            $intervalStart = $periodData['date'].' '.$periodData['value'].':00:00';
            $revises = $revisesDataModel->find('all', array(
                'conditions'=>array(
                    'OR'=>array(
                        'RevisesData.confirmed >=' => $intervalStart,
                        'RevisesData.created >=' => $intervalStart,
                    ),
                    'RevisesData.created <=' => date('Y-m-d H:i:s',strtotime($intervalStart) + $graphHoursLinesCount*3600),
                    'RevisesData.sensor_id' => $fSensor['Sensor']['id']
                ),'order'=>array('RevisesData.created')
            ));
            foreach($revises as $revise){
                $hourCreated = date('H',strtotime($revise['RevisesData']['created']));
                $createdRevises[(int)$hourCreated][] = $revise['RevisesData'];
                if($revise['RevisesData']['confirmed']) {
                    $hourConfirmed = date('H', strtotime($revise['RevisesData']['confirmed']));
                    $confirmedRevises[(int)$hourConfirmed][] = $revise['RevisesData'];
                }
            }
        }
        if(isset($createdRevises[$periodData['value']])){
            foreach($createdRevises[$periodData['value']] as $revise){
                $minutes = (int)date('i',strtotime($revise['created']));
                $periodData['markers'][$minutes] = array('minutes'=>$minutes, 'class'=>'reservation');
            }
        }
        if(isset($confirmedRevises[$periodData['value']])){
            foreach($confirmedRevises[$periodData['value']] as $revise){
                if((int)$revise['confirmed']>0){
                    $minutes = (int)date('i',strtotime($revise['confirmed']));
                    $periodData['markers'][$minutes] = array('minutes'=>$minutes, 'class'=>'confirmation');
                }
            }
        }

    }

    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Mediresta/css/Mediresta_bare.css';
    }

    public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift, &$fSensor){
        if($fSensor['Sensor']['mediresta_sensor_type'] == self::SKENAVIMAS_TYPE){
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->updateAll(array('workers_count'=>0), array('Sensor.id'=>$fSensor['Sensor']['id']));
        }
    }

    //10.2.3. Turint kiekvienos darbuotojų skaičiaus grupės pagamintą kiekį ir jos darbo trukmę minutėmis, turi būti apskaičiuojamas kiekvienos darbuotojų skaičiaus grupės, vieno darbuotojo, toje grupėje, greitis metrais per valandą. Atitinkamos grupės pagamintas kiekis turi būti dalinamas iš toje grupėje dirbusių žmonių skaičiaus, tada dalinamas iš tos grupės darbo laiko trukmės minutėmis ir padauginamas iš 60;
    //10.2.4. Apskaičiavus kiekvienos darbuotojų skaičiaus grupės vieno darbuotojo greitį metrais per valandą toje grupėje, turi būti susumuojami visų grupių gauti rezultatai ir padalinami iš grupių skaičiaus. Tokiu būdu gaunamas vidutinis vieno žmogaus greitis pamainoje metrais per valandą;
    public function Oeeshell_filterGetFactTimeRecords_Hook(&$time, &$shift, &$records, &$returnData, &$sensors, &$shift_length){
        if(!$returnData) {
            $recordModel = ClassRegistry::init('Record');
            $recordModel->bindModel(array('belongsTo' => array('Sensor')));
            $quantitiesPerGroups = $recordModel->find('all', array(
                'fields' => array(
//                    'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS fact_prod_time',
//                    'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity),0)) as quantity',
                    'SUM('.Configure::read('recordsCycle').') AS fact_prod_time',
                    'SUM(IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity)) as quantity',
                    'Record.sensor_id',
                    'Record.workers_count'
                ), 'conditions' => array(
                    'Record.shift_id' => $shift['Shift']['id'],
                    'Record.sensor_id' => $sensors,
                    'Record.workers_count >' => 0,
                    'Sensor.mediresta_sensor_type' => self::SKENAVIMAS_TYPE
                ), 'group' => array('Record.sensor_id', 'Record.workers_count'),
                'order' => array('Record.sensor_id')
            ));
            $singleWorkerAvgSpeedPerGroup = array();
            foreach ($quantitiesPerGroups as $quantityPerGroup) {
                if ($quantityPerGroup[0]['fact_prod_time'] == 0) {
                    continue;
                }
                $sensorId = $quantityPerGroup['Record']['sensor_id'];
                $workersCount = $quantityPerGroup['Record']['workers_count'];
                //$singleWorkerAvgSpeedPerGroup[$sensorId][$workersCount] = bcmul(bcdiv(bcdiv($quantityPerGroup[0]['quantity'], $workersCount, 6), bcdiv($quantityPerGroup[0]['fact_prod_time'], 60, 6), 6), 60, 6);
                $singleWorkerAvgSpeedPerGroup[$sensorId][$workersCount] = array(
                    'quantity_per_person'=>bcdiv($quantityPerGroup[0]['quantity'], $workersCount, 6),
                    'duration_hour'=>bcdiv($quantityPerGroup[0]['fact_prod_time'], 3600, 6)
                );
            }
            unset($quantitiesPerGroups);
            foreach ($singleWorkerAvgSpeedPerGroup as $sensorId => $infoPerPerson) {
                self::$avgSpeedPerPerson[$sensorId] = bcdiv(array_sum(Set::extract('/quantity_per_person', $infoPerPerson)), array_sum(Set::extract('/duration_hour', $infoPerPerson)), 6);
            }
            unset($singleWorkerAvgSpeedPerGroup);
        }
    }

    public function Oeeshell_beforeLastGetFactTimeLoop_Hook(&$record, $sensor_id){
        if(isset(self::$avgSpeedPerPerson[$sensor_id])){
            $record['avg_worker_speed'] = self::$avgSpeedPerPerson[$sensor_id];
        }
    }

    public function ReportsContr_BeforeGenerateXLS_Hook(&$reportModel){
        $reportModel = ClassRegistry::init('Mediresta.MedirestaReport');
    }

    public function ReportsContr_BeforeCalculatePeriodXLS_Hook(&$reportModel, &$templateTitle){
        $templateTitle = '/../Plugin/Mediresta/View/ProjectViewsHooks/Reports/calculate_period_xls';
    }

    public function Workcenter_changeUpdateStatusVariables_Hook2(&$val, $fSensor){
        // Skenavimo tipo DC (sensors.mediresta_sensor_type = 1) teorinis kiekis, naudojamas spidometro duomenų apskaičiavime, yra apskaičiuojamas grupuojant įrašus pagal dirbusių žmonių skaičių (records.worker_count) tame laikotarpyje, kuris yra pasirinktas jutiklio nustatyme Nr. 45. Turint, kiekvienos dirbusių žmonių skaičiaus grupės, įrašų kiekį, jis yra dauginamas iš 20 ir dalinamas iš 3600, kad būtų gaunamas kiekvienos grupės darbo trukmė valandomis. Gauta trukmė dauginama iš žmonių skaičiaus toje grupėje ir normos – 185 m/val. vienam žmogui, kuri bus naudojama statinė ir užsakovas negalės jos keisti. Susumavus gautus sandaugų rezultatus gaunamas teorinis kiekis. Jei pasirinktas spidometro pateikimas iš paskutinės minutės kiekių, norma turi būti konvertuojama į 3,08 m/min.
        if($fSensor['Sensor']['mediresta_sensor_type'] == 1){
            $time = time();
            $quantity = isset($fSensor['Sensor']['plan_relate_quantity']) ? $fSensor['Sensor']['plan_relate_quantity'] : 'quantity';
            $recordModel = ClassRegistry::init('Record');
            switch ($fSensor['Sensor']['speedometer_type']) {
                case 2: //paskutine valanda
                    $duration = 3600;
                    $step = 185;//norma vnt/h
                    break;
                case 0: //paskutine minute
                    $duration = 60;
                    $step = 3.08;//norma vnt/min
                    break;
                case 1: //paskutine pamaina
                    $duration = $time - strtotime($val['currentShift']['Shift']['start']);
                    $step = $duration/3600*185; //norma vnt/pam

            }
            $params = array(
                'fields' => array(
                    'SUM(' . Configure::read('recordsCycle') . ') / '.$duration.' * Record.workers_count * '.$step.' AS theorical_step',
                    'Record.workers_count'
                ), 'recursive' => -1,
                'conditions' => array(
                    'Record.created >=' => date('Y-m-d H:i:s', $time - $duration - Configure::read('recordsCycle')),
                    'Record.created <' => date('Y-m-d H:i:s', $time - Configure::read('recordsCycle')),
                    'Record.sensor_id' => $fSensor['Sensor']['id'],
                    'Record.workers_count >' => 0
                ), 'group' => array('Record.workers_count')
            );
            $stepInGroups = Hash::combine($recordModel->find('all', $params),'{n}.Record.workers_count', '{n}.0.theorical_step');
            $val['currentPlan']['Plan']['step'] = round(array_sum($stepInGroups),2);
            //return $step;
        }
        $dashboardModel = ClassRegistry::init('DashboardsCalculation');
        $dashboardModel->virtualFields = array('prev_shift_quantity'=>'CONCAT(\''.__('kiekis').': \',ROUND(DashboardsCalculation.total_quantity))');
        $dashboardModel->bindModel(array('belongsTo'=>array('Shift')));
        $val['prevShiftsOEE'] = array_values($dashboardModel->find('list', array(
            'fields'=>array('DashboardsCalculation.shift_id','prev_shift_quantity'),
            'conditions'=>array(
                'DashboardsCalculation.sensor_id'=>$fSensor['Sensor']['id'],
                'DashboardsCalculation.shift_id <'=>$val['selectedTimeShift']['Shift']['id'],
                'Shift.disabled'=>0,
            ), 'order'=>array('DashboardsCalculation.shift_id DESC'),
            'limit'=>2, 'recursive'=>1
        )));
    }

    public function App_createMenu_Hook(&$navigation, $params){
        $registrationJournal = new MainMenuItem(__('Registracijos žurnalas'), '#', false, 'iconfa-edit', null, array(
            new MainMenuItem(__('Paketų registravimas'), Router::url(array('plugin'=>'mediresta','controller' => 'registrations', 'action' => 'display_pallets')), (strtolower($params['controller']) == 'registrations' &&  $params['action'] == 'display_pallets'), 'iconfa-book'),
            new MainMenuItem(__('Palečių sukūrimas'), Router::url(array('plugin'=>'mediresta','controller' => 'registrations', 'action' => 'display_unregistered_pallets')), (strtolower($params['controller']) == 'registrations' &&  $params['action'] == 'display_unregistered_pallets'), 'iconfa-edit'),
            new MainMenuItem(__('Palečių perkėlimas'), Router::url(array('plugin'=>'mediresta','controller' => 'registrations', 'action' => 'transfer_pallets')), (strtolower($params['controller']) == 'registrations' &&  $params['action'] == 'transfer_pallets'), 'iconfa-arrow-right'),
            new MainMenuItem(__('Produkcijos registravimas'), Router::url(array('plugin'=>'mediresta','controller' => 'registrations', 'action' => 'display_unmoved_packets')), (strtolower($params['controller']) == 'registrations' &&  $params['action'] == 'display_unmoved_packets'), 'iconfa-book'),
            new MainMenuItem(__('Produkcijos išvežimas'), Router::url(array('plugin'=>'mediresta','controller' => 'registrations', 'action' => 'transfer_packets')), (strtolower($params['controller']) == 'registrations' &&  $params['action'] == 'transfer_packets'), 'iconfa-arrow-right'),
        ));
        //pr($navigation);
        $navigation = array_merge(array_slice($navigation, 0, 6), array($registrationJournal), array_slice($navigation, 6));
    }

    public function Users_ChooseSensorBeforeRedirect_Hook(&$choosedSensorId){
        $sensorModel = ClassRegistry::init('Sensor');
        $fSensor = $sensorModel->findById($choosedSensorId);
        if($fSensor['Sensor']['mediresta_sensor_type'] == self::PRESAVIMAS_TYPE){
            $sensorModel->updateAll(array('Sensor.active_user_id'=>Configure::read('user')->id), array('Sensor.mediresta_sensor_type'=>self::PRESAVIMAS_TYPE));
        }
    }

    public function Oeeshell_beforeStartDurationCalculation2_Hook(&$fields){
        $fields[] = 'SUM(DashboardsCalculation.avg_worker_speed) / COUNT(DashboardsCalculation.shift_id) AS avg_worker_speed';
    }
}
    