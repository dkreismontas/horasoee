<?php
class Packet extends MedirestaAppModel{
    public $packetTransferValidation = array();
    public $transferConfirmation = false;

    function __construct(){
        $this->validate = array(
            'height' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite produkto aukštį milimetrais',true)
            ),
            'width' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite produkto plotį milimetrais',true)
            ),
            'length' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite produkto ilgį milimetrais',true)
            ),
            'quantity' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite produkto kiekį pakete',true)
            ),
            'production_date' => array(
                'rule' => array('date', 'ymd'),
                'message' =>__('Įveskite gamybos datą',true)
            ),
            'transfer_packet_number' => array(
                'number_exist' => array(
                    'rule' =>array('validation_number_exist'),
                    'message' =>__('Produkcijos su įvestu numeriu rasti nepavyko',true)
                ),
                'packet_moved' => array(
                    'rule' =>array('validation_packet_moved'),
                    'message' =>__('Produkcija su tokiu numeriu jau yra išvežta',true)
                ),
                'packet_deleted' => array(
                    'rule' =>array('validation_packet_deleted'),
                    'message' =>__('Produkcija su tokiu numeriu yra ištrinta',true)
                ),
                'packet_save_confirmed' => array(
                    'rule' =>array('validation_packet_save_confirmed'),
                    'message' =>''
                ),
            )
        );
        parent::__construct();
    }

    public function validation_number_exist($data,$field){
        return !empty($this->packetTransferValidation);
    }

    public function validation_packet_moved($data,$field){
        return is_null($this->packetTransferValidation['Packet']['moved']);
    }

    public function validation_packet_deleted($data,$field){
        return is_null($this->packetTransferValidation['Packet']['deleted']);
    }

    public function validation_packet_save_confirmed($data,$field){
        if(!$this->data['Packet']['save_confirmed']){$this->transferConfirmation = true;}
        return $this->data['Packet']['save_confirmed'] == 1;
    }

    public function calculatePacketNumber(){
        $lastRegisteredPacket = $this->find('first', array('order' => array('Packet.id DESC')));
        $lastNumber = empty($lastRegisteredPacket)?'00AA':++$lastRegisteredPacket['Packet']['packet_number'];
        if(strlen($lastNumber) > 4){ $lastNumber = '00AA'; }
        return $lastNumber;
    }

}