<?php
class Pallet extends MedirestaAppModel{
    public $palletTransferValidation = array();
    public $transferConfirmation = false;

    function __construct(){
        $this->validate = array(
            'height' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite lentos storį milimetrais',true)
            ),
            'width' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Įveskite lentos plotį milimetrais',true)
            ),
            'camera' => array(
                'rule' => 'naturalNumber',
                'message' =>__('Kameros numeris turi būti skaičius > 0',true)
            ),
            'humidity_1' => array(
                'rule' => array('decimal'),
                'message' =>__('Įveskite pirmą drėgmės matavimą',true)
            ),
            'humidity_2' => array(
                'rule' => array('decimal'),
                'message' =>__('Įveskite antrą drėgmės matavimą',true)
            ),
            'humidity_3' => array(
                'rule' => array('decimal'),
                'message' =>__('Įveskite trečią drėgmės matavimą',true)
            ),
            'package_type' => array(
                'rule' => array('alphaNumeric'),
                'message' =>__('Įveskite paketo tipą',true)
            ),
            'pallet_height' => array(
                'rule' => array('naturalNumber'),
                'message' =>__('Įveskite paletės aukštį',true)
            ),
            'fill_factor' => array(
                'rule' => array('decimal'),
                'message' =>__('Pasirinkite užpildymo koeficientą',true)
            ),
            'load_date' => array(
                'rule' => array('date', 'ymd'),
                'message' =>__('Įveskite pakrovimo datą',true)
            ),
            'transfer_pallet_number' => array(
                'number_exist' => array(
                    'rule' =>array('validation_number_exist'),
                    'message' =>__('Paletės su įvestu numeriu rasti nepavyko',true)
                ),
                'pallet_moved' => array(
                    'rule' =>array('validation_pallet_moved'),
                    'message' =>__('Paletė su tokiu numeriu jau yra perkelta',true)
                ),
                'pallet_deleted' => array(
                    'rule' =>array('validation_pallet_deleted'),
                    'message' =>__('Paletė su tokiu numeriu yra ištrinta',true)
                ),
                'pallet_save_confirmed' => array(
                    'rule' =>array('validation_pallet_save_confirmed'),
                    'message' =>''
                ),
            )
        );
        parent::__construct();
    }

    public function validation_number_exist($data,$field){
        return !empty($this->palletTransferValidation);
    }

    public function validation_pallet_moved($data,$field){
        return is_null($this->palletTransferValidation['Pallet']['moved']);
    }

    public function validation_pallet_deleted($data,$field){
        return is_null($this->palletTransferValidation['Pallet']['deleted']);
    }

    public function validation_pallet_save_confirmed($data,$field){
        if(!$this->data['Pallet']['save_confirmed']){$this->transferConfirmation = true;}
        return $this->data['Pallet']['save_confirmed'] == 1;
    }

    public function calculatePalletNumber(){
        $lastRegisteredPallet = $this->find('first', array('conditions' => array('Pallet.registered IS NOT NULL','Pallet.parent_id >'=>0), 'order' => array('Pallet.registered DESC')));
        $lastNumber = empty($lastRegisteredPallet)?'AA00':++$lastRegisteredPallet['Pallet']['pallet_number'];
        if(strlen($lastNumber) > 4){ $lastNumber = 'AA00'; }
        return $lastNumber;
    }

}