<?php
App::uses('MedirestaAppController', 'Mediresta.Controller');
class PluginsController extends MedirestaAppController {
	
    public $uses = array('Shift','Record','Sensor');

    public function index(){
        $this->layout = 'ajax';
        $sensorsList = $this->Sensor->find('list', array('fields'=>array('id', 'mediresta_sensor_type'),
            //'conditions'=>array('Sensor.mediresta_sensor_type'=>array(Mediresta::OBLIAVIMAS_TYPE, Mediresta::DYGIAVIMAS_TYPE))
        ));
        $this->set(compact('sensorsList'));
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Mediresta.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }

    public function change_workers_count(){
        if(!$this->request->is('ajax')){die();}
        $this->Sensor->id = $this->request->data['sensor_id'];
        $this->Sensor->save(array('workers_count'=>$this->request->data['workers_count']));
        fb($this->request->data);
        die();
    }

    /*public function encrypted(){
        App::import('Vendor', 'Fernet');
        $ch = curl_init('http://86.100.76.166:5580/data/encrypted');
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
        ));
        $data = curl_exec($ch);
        $fernetDecoder = new Fernet\Fernet('FdZhRq5lFLmmHmHAEwKPzl7VuO_3Wnsvfjc3ztNUnOg');
        pr($fernetDecoder->decode($data));
        die();
    }*/

}
