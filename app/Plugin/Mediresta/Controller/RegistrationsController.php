<?php
class RegistrationsController extends AppController{

    public $uses = array('Pallet','Packet');

    /** @requireAuth Žaliavos registravimas: Atvaizduoti sukurtus paketus */
    public function display_pallets(){
        $this->set(array(
            'pallets'=>$this->Pallet->find('all', array('conditions'=>array('Pallet.deleted IS NULL','Pallet.parent_id'=>0), 'order'=>array('Pallet.id DESC'), 'limit'=>25)),
            'hasAccessToEdit' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'edit_pallet')),
            'hasAccessToDelete' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'delete_pallet')),
            'hasAccessToFinish' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'finish_pallet')),
        ));
    }

    /** @requireAuth Žaliavos registravimas: Sukurti/redaguoti paketą */
    public function edit_pallet($id=0){
        if(!empty($this->request->data)){
            if(!$this->request->data['Pallet']['id']) {
                $this->request->data['Pallet']['created_user_id'] = Configure::read('user')->id;
            }else{
                $this->request->data['Pallet']['created_edit_user_id'] = Configure::read('user')->id;
                $this->request->data['Pallet']['created_edit'] = date('Y-m-d H:i:s');
            }
            $this->request->data['Pallet']['humidity_1'] = str_replace(',','.',$this->request->data['Pallet']['humidity_1']);
            $this->request->data['Pallet']['humidity_2'] = str_replace(',','.',$this->request->data['Pallet']['humidity_2']);
            $this->request->data['Pallet']['humidity_3'] = str_replace(',','.',$this->request->data['Pallet']['humidity_3']);
            if($this->Pallet->save($this->request->data)) {
                if(!$id){
                    $this->Session->setFlash(__('Paketas sėkmingai užregistruotas'));
                    $this->request->data = array();
                }else{
                    $this->Session->setFlash(__('Paketas sėkmingai atnaujintas'));
                }
                if($this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'display_pallets'))){
                    $this->redirect(array('action' => 'display_pallets'));
                }
            }
        }
        if(!$id){
            $h1_for_layout = __('Naujas paketas');
        }else{
            $this->request->data = $this->Pallet->findById($id);
            $h1_for_layout = __('Paketo redagavimas');
        }
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $this->set(array(
            'icon_for_layout'=>'iconfa-book',
            'h1_for_layout'=>!$id?__('Naujas paketas'):__('Paketo redagavimas'),
            'usersList'=>$this->User->find('list', array('fields'=>array('id','full_name')))
        ));
    }

    /** @requireAuth Žaliavos registravimas: ištrinti paketą */
    public function delete_pallet($id=0){
        $this->Pallet->updateAll(
            array('Pallet.deleted'=>'\''.date('Y-m-d H:i:s').'\'', 'Pallet.deleted_user_id'=>Configure::read('user')->id),
            array('Pallet.id'=>$id)
        );
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /** @requireAuth Žaliavos registravimas: pabaigti paketą */
    public function finish_pallet($id=0){
        $this->Pallet->updateAll(
            array('Pallet.registered'=>'\''.date('Y-m-d H:i:s').'\'', 'registered_user_id'=>Configure::read('user')->id),
            array('Pallet.id'=>$id)
        );
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    /** @requireAuth Žaliavos registravimas: palečių kūrimo puslapis */
    public function display_unregistered_pallets(){
        $this->set(array(
            'hasAccessToAdditionalPallet' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'register_additional_pallet')),
            'display_registered_pallets' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'display_registered_pallets')),
            'pallets' => $this->Pallet->find('all', array('conditions' => array('Pallet.registered IS NULL', 'Pallet.deleted IS NULL','Pallet.parent_id'=>0), 'order' => array('Pallet.id DESC'), 'limit' => 5)),
        ));
    }

    public function display_registered_pallets(){
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Sukurtos paletės'),
            'pallets' => $this->Pallet->find('all', array('conditions' => array('Pallet.registered IS NOT NULL', 'Pallet.deleted IS NULL', 'Pallet.parent_id >'=>0), 'order' => array('Pallet.registered DESC'), 'limit' => 25)),
            'hasAccessToEditPallet' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'edit_registered_pallet')),
            'hasAccessToDeletePallet' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'delete_pallet')),
        ));
    }

    public function register_pallet($id=0){
        if(!empty($this->request->data)){
            $parentInfo = $this->Pallet->findById($id)['Pallet'];
            unset($parentInfo['id']);
            $this->request->data['Pallet']['registered'] = date('Y-m-d H:i:s');
            $this->request->data['Pallet']['registered_user_id'] = Configure::read('user')->id;
            $this->request->data['Pallet']['pallet_number'] = $this->Pallet->calculatePalletNumber();
            $this->request->data['Pallet'] = array_merge($parentInfo, $this->request->data['Pallet']);
            if($this->Pallet->save($this->request->data)) {
                $this->Session->setFlash('<h1 style="text-align: center; font-size: 100px;">'.$this->request->data['Pallet']['pallet_number'].'</h1>','default',array(),'pallet_creation_success');
                $this->redirect(array('action' => 'display_unregistered_pallets'));
            }
        }

        $this->request->data = $this->Pallet->findById($id);
        $this->request->data['Pallet']['parent_id'] = $id;
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Sukurti paletę'),
            'pallets'=>$this->Pallet->find('all', array('conditions'=>array('Pallet.registered IS NULL', 'Pallet.deleted IS NULL'), 'order'=>array('Pallet.id DESC'), 'limit'=>5)),
        ));
    }

    /** @requireAuth Žaliavos registravimas: papildomos paletės kūrimas */
    public function register_additional_pallet(){
        if(!empty($this->request->data)){
            $this->request->data['Pallet']['registered'] = date('Y-m-d H:i:s');
            $this->request->data['Pallet']['registered_user_id'] = Configure::read('user')->id;
            $this->request->data['Pallet']['pallet_number'] = $this->Pallet->calculatePalletNumber();
            if($this->Pallet->save($this->request->data)) {
                $this->Pallet->save(array('parent_id'=>$this->Pallet->id));
                $this->Session->setFlash('<h1 style="text-align: center; font-size: 100px;">'.$this->request->data['Pallet']['pallet_number'].'</h1>','default',array(),'pallet_creation_success');
                $this->redirect(array('action' => 'display_unregistered_pallets'));
            }
        }
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Sukurti papildomą paletę'),
        ));
    }

    /** @requireAuth Žaliavos registravimas: redaguoti sukurtą paletę */
    public function edit_registered_pallet($id=0){
        if(!$id){
            $this->redirect(array('action' => 'display_registered_pallets'));
        }
        if(!empty($this->request->data)){
            $this->request->data['Pallet']['registered_edit_user_id'] = Configure::read('user')->id;
            $this->request->data['Pallet']['registered_edit'] = date('Y-m-d H:i:s');
            if($this->Pallet->save($this->request->data)) {
                $this->Session->setFlash(__('Paletė sėkmingai atnaujinta'));
                $pallet = $this->Pallet->read();
                if($pallet['Pallet']['moved']){
                    $this->redirect(array('action' => 'transfer_pallets'));
                }else{
                    $this->redirect(array('action' => 'display_unregistered_pallets'));
                }

            }
        }
        $this->request->data = $this->Pallet->findById($id);
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Paletės redagavimas'),
            'usersList'=>$this->User->find('list', array('fields'=>array('id','full_name')))
        ));
    }

    /** @requireAuth Žaliavos registravimas: palečių perkėlimas */
    public function transfer_pallets($sensorId = 0){
        if(!empty($this->request->data)){
            $this->request->data['Pallet']['moved_user_id'] = Configure::read('user')->id;
            $this->request->data['Pallet']['moved'] = date('Y-m-d H:i:s');
            $this->Pallet->palletTransferValidation = $this->Pallet->find('first', array('conditions'=>array('Pallet.pallet_number'=>$this->request->data['Pallet']['transfer_pallet_number'])));
            if(!empty($this->Pallet->palletTransferValidation)){ $this->Pallet->id = $this->Pallet->palletTransferValidation['Pallet']['id']; }
            if($this->Pallet->save($this->request->data)) {
                $this->Session->setFlash(__('Paletė sėkmingai perkelta'));
            }
        }
        $conditions = array('Pallet.moved IS NOT NULL', 'Pallet.deleted IS NULL','Pallet.parent_id >'=>0);
        if($sensorId > 0) {
            $usersIds = $this->User->find('list', array('conditions'=>array('FIND_IN_SET('.$sensorId.',User.sensor_id)')));
            if(!empty($usersIds)){
                $conditions['Pallet.moved_user_id'] = $usersIds;
            }
        }
        $this->set(array(
            'transferConfirmation' => $this->Pallet->transferConfirmation,
            'pallets'=>$this->Pallet->find('all', array('conditions'=>$conditions, 'order'=>array('Pallet.moved DESC'), 'limit'=>30)),
            'hasAccessToReturnPallet' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'return_transfer_pallets')),
            'hasAccessToDeletePallet' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'delete_pallet')),
        ));
    }

    /** @requireAuth Žaliavos registravimas: grąžinti pakėlimui paruoštas paletes */
    public function return_transfer_pallets($palletId = 0){
        if(!$palletId){ die(); }
        $this->Pallet->id = $palletId;
        $this->Pallet->save(array(
            'moved'=>null, 'moved_user_id'=>0
        ));
        die();
    }

    public function display_unmoved_packets(){
        $this->set(array(
            'packets'=>$this->Packet->find('all', array('conditions'=>array('Packet.moved IS NULL', 'Packet.deleted IS NULL'), 'order'=>array('Packet.registered DESC'), 'limit'=>5)),
            'hasAccessToEditPacket' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'edit_packet')),
            'hasAccessToDeletePacket' => $this->hasAccessToAction(array('plugin'=>'mediresta','controller'=>'registrations', 'action'=>'delete_packet')),
        ));
    }

    /** @requireAuth Žaliavos registravimas: naujos produkcijos kūrimas */
    public function register_packet(){
        if(!empty($this->request->data)){
            $this->request->data['Packet']['registered'] = date('Y-m-d H:i:s');
            $this->request->data['Packet']['registered_user_id'] = Configure::read('user')->id;
            $this->request->data['Packet']['packet_number'] = $this->Packet->calculatePacketNumber();
            $numberIsUniqueInShortPeriod = $this->Packet->find('first', array('conditions'=>array(
                'Packet.packet_number'=>$this->request->data['Packet']['packet_number'],
                'Packet.registered >' => date('Y-m-d H:i:s', strtotime('-1 week'))
            )));
            //neaisku kaip sistemoje atsirado vienas salia kito du vienodi numeriai. Sis skriptas turi papildomai uztikrinti, kad numeris butu unikalus
            if(!empty($numberIsUniqueInShortPeriod)){
                $this->Session->setFlash(__('Naudojamas jau egzistuojantis paketo numeris %s. Pabandykite registruoti dar kartą, kitu atveju kreipkitės į sistemos administratorių.', $this->request->data['Packet']['packet_number']));
            }elseif($this->Packet->save($this->request->data)) {
                $this->Session->setFlash(__('Produkcija sėkmingai sukurta'));
                $this->Session->setFlash('<h1 style="text-align: center; font-size: 100px;">'.$this->request->data['Packet']['packet_number'].'</h1>','default',array(),'packet_creation_success');
            }
            $this->redirect(array('action' => 'display_unmoved_packets'));
        }
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Nauja produkcija'),
        ));
    }

    /** @requireAuth Žaliavos registravimas: redaguoti produktą */
    public function edit_packet($id=0){
        if(!empty($this->request->data)){
            $this->request->data['Packet']['registered_edit'] = date('Y-m-d H:i:s');
            $this->request->data['Packet']['registered_edit_user_id'] = Configure::read('user')->id;
            if($this->Packet->save($this->request->data)) {
                $this->Session->setFlash(__('Paketas sėkmingai atnaujintas'));
                $this->redirect(array('action' => 'display_unmoved_packets'));
            }
        }
        $this->request->data = $this->Packet->findById($id);
        if(empty($this->request->data)){
            $this->Session->setFlash(__('Produkcija ID: %s nerasta', $id));
            $this->redirect(array('action' => 'display_unmoved_packets'));
        }
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $this->set(array(
            'icon_for_layout'=>'iconfa-edit',
            'h1_for_layout'=>__('Produkcija ID: %s', $id),
            'usersList'=>$this->User->find('list', array('fields'=>array('id','full_name')))
        ));
    }

    /** @requireAuth Žaliavos registravimas: ištrinti produktą */
    public function delete_packet($id=0){
        $this->Packet->updateAll(
            array('Packet.deleted'=>'\''.date('Y-m-d H:i:s').'\'', 'Packet.deleted_user_id'=>Configure::read('user')->id),
            array('Packet.id'=>$id)
        );
        $this->redirect($_SERVER['HTTP_REFERER']);
    }

    public function transfer_packets(){
        if(!empty($this->request->data)){
            $this->request->data['Packet']['moved_user_id'] = Configure::read('user')->id;
            $this->request->data['Packet']['moved'] = date('Y-m-d H:i:s');
            $this->Packet->packetTransferValidation = $this->Packet->find('first', array('conditions'=>array('Packet.packet_number'=>$this->request->data['Packet']['transfer_packet_number'])));
            if(!empty($this->Packet->packetTransferValidation)){ $this->Packet->id = $this->Packet->packetTransferValidation['Packet']['id']; }
            if($this->Packet->save($this->request->data)) {
                $this->Session->setFlash(__('Produkcijos išvežimas patvirtintas'));
            }
        }
        $this->set(array(
            'transferConfirmation' => $this->Packet->transferConfirmation,
            'packets'=>$this->Packet->find('all', array('conditions'=>array('Packet.moved IS NOT NULL', 'Packet.deleted IS NULL'), 'order'=>array('Packet.moved DESC'), 'limit'=>25)),
        ));
    }

}