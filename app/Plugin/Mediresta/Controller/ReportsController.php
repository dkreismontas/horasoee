<?php
class ReportsController extends AppController{

    public $uses = array('Loss','LossType','RevisesData','Pallet','Packet','User');

    public function generate_losses_report(){
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        if(empty($this->request->data['sensors'])) {
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('plugin'=>false,'controller'=>'dashboards','action'=>'inner'));
        }
        $this->Loss->bindModel(array('belongsTo'=>array(
            'ApprovedOrder','LossType','Shift',
            'Sensor'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.sensor_id = Sensor.id'))
        )));
        $lossesTmp = $this->Loss->find('all', array(
            'fields'=>array(
                'SUM(Loss.value) AS quantity',
                'TRIM(LossType.parent_id) AS loss_parent_id',
                'TRIM(LossType.name) AS loss_name',
                'TRIM(Sensor.name) AS sensor_name',
                'LossType.id',
                'CONCAT(Shift.type,\' \',Shift.start, "\n", Shift.end) AS shift_name',
            ),
            'conditions'=>array(
                'Loss.created_at >=' => $date_start,
                'Loss.created_at <=' => $date_end,
                'Sensor.id'=>$this->request->data['sensors']
            ),'group'=>array(
                'Shift.id','Sensor.id','LossType.id'
            ),'order'=>array(
                'Shift.id','Sensor.name'
            )
        ));
        $lossesByShifts = array();
        foreach($lossesTmp as $lossTmp){
            $shiftName = $lossTmp[0]['shift_name'];
            $sensorName = $lossTmp[0]['sensor_name'];
            $lossTypeId = $lossTmp['LossType']['id'];
            $lossesByShifts[$shiftName][$sensorName][$lossTypeId] = $lossTmp[0];
        }
        $this->Loss->bindModel(array('belongsTo'=>array(
            'ApprovedOrder','LossType',
            'Sensor'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.sensor_id = Sensor.id')),
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')),
        )));
        $lossesTmp = $this->Loss->find('all', array(
            'fields'=>array(
                'SUM(Loss.value) AS quantity',
                'TRIM(LossType.parent_id) AS loss_parent_id',
                'TRIM(LossType.name) AS loss_name',
                'TRIM(Sensor.name) AS sensor_name',
                'LossType.id',
                'ApprovedOrder.id',
                'TRIM(ApprovedOrder.created) AS order_start',
                'TRIM(ApprovedOrder.end) AS order_end',
                'TRIM(Plan.production_name) AS production_name',
            ),
            'conditions'=>array(
                'Loss.created_at >=' => $date_start,
                'Loss.created_at <=' => $date_end,
                'Sensor.id'=>$this->request->data['sensors']
            ),'group'=>array(
                'ApprovedOrder.id','LossType.id'
            ),'order'=>array(
               'ApprovedOrder.id','Sensor.name'
            )
        ));
        $lossesByOrder = array();
        foreach($lossesTmp as $lossTmp){
            $orderId = $lossTmp['ApprovedOrder']['id'];
            $sensorName = $lossTmp[0]['sensor_name'];
            $lossTypeId = $lossTmp['LossType']['id'];
            $lossesByOrder[$orderId][$lossTypeId] = $lossTmp[0];
        }
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(array(
            'lossesByShifts' => $lossesByShifts,
            'lossesByOrder' => $lossesByOrder,
            'obliavimasLossesTypes' => $this->LossType->find('threaded', array('conditions'=>array('LossType.sensor_types'=>Mediresta::OBLIAVIMAS_TYPE))),
            'objPHPExcel'=>$objPHPExcel
        ));
    }

    public function generate_revises_report(){
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        if(empty($this->request->data['sensors'])) {
            $this->Session->setFlash(__('Neparinktas jutiklis'));
            $this->redirect(array('plugin'=>false,'controller'=>'dashboards','action'=>'inner'));
        }
        $this->RevisesData->bindModel(array('belongsTo'=>array(
            'Sensor','ApprovedOrder','User',
            'Plan'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id'))
        )));
        $revises = $this->RevisesData->find('all', array(
            'fields'=>array('RevisesData.created','RevisesData.confirmed','Sensor.name','Plan.production_name','User.first_name','User.last_name'),
            'conditions'=>array(
                'RevisesData.created >=' => $date_start,
                'RevisesData.created <=' => $date_end,
                'RevisesData.sensor_id'=>$this->request->data['sensors']
            ),'order'=>array(
                'Sensor.name','RevisesData.created'
            )
        ));
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(array(
            'revises' => $revises,
            'objPHPExcel'=>$objPHPExcel
        ));
    }

    public function generate_remainings_report(){
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $this->set(array(
            'pallets' => $this->Pallet->find('all', array('conditions'=>array('Pallet.moved IS NULL', 'Pallet.registered IS NOT NULL', 'Pallet.deleted IS NULL', 'Pallet.parent_id >'=>0), 'order'=>array('Pallet.registered'))),
            'packets' => $this->Packet->find('all', array('conditions'=>array('Packet.moved IS NULL', 'Packet.deleted IS NULL'), 'order'=>array('Packet.registered'))),
            'objPHPExcel'=>$objPHPExcel,
            'usersList'=>$this->User->find('list', array('fields'=>array('id','full_name')))
        ));
    }

    public function generate_registrations_report(){
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $palletsConditions = array('Pallet.deleted IS NULL');
        $packetsConditions = array('Packet.deleted IS NULL');
        switch($this->request->data['selection_type']){
            case 0:
                $palletsConditions[] = sprintf('Pallet.created BETWEEN \'%s\' AND \'%s\'',$date_start, $date_end);
                $packetsConditions[] = sprintf('Packet.registered BETWEEN \'%s\' AND \'%s\'',$date_start, $date_end);
                $palletsOrdering = array('Pallet.created');
                $packetsOrdering = array('Packet.registered');
            break;
            case 1:
                $palletsConditions[] = sprintf('Pallet.moved BETWEEN \'%s\' AND \'%s\'',$date_start, $date_end);
                $packetsConditions[] = sprintf('Packet.moved BETWEEN \'%s\' AND \'%s\'',$date_start, $date_end);
                $palletsOrdering = array('Pallet.moved');
                $packetsOrdering = array('Packet.moved');
            break;
        }
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $this->set(array(
            'pallets' => $this->Pallet->find('all', array('conditions'=>$palletsConditions, 'order'=>$palletsOrdering)),
            'packets' => $this->Packet->find('all', array('conditions'=>$packetsConditions, 'order'=>$packetsOrdering)),
            'objPHPExcel'=>$objPHPExcel,
            'usersList'=>$this->User->find('list', array('fields'=>array('id','full_name')))
        ));
    }

}