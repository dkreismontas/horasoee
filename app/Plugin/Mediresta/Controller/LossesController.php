<?php
class LossesController extends AppController{
    public $uses = array('LossType','Loss','LossType','ApprovedOrder','Shift','Sensor');

    public function registrate($sensorId=0){
        if($this->request->is('ajax') && isset($this->request->data['layout'])){ //kai kreipiasi is darbo centro nereikia mygtuku, todel neisvesime javascripto
            $this->layout = $this->request->data['layout'];
            $this->set('addActionButtons', true);
        }else{
            $this->set('addActionButtons', true);
        }
        $fSensor = $this->Sensor->findById($sensorId);
        $currentOrder = $this->ApprovedOrder->findCurrent($sensorId);
        $currentShift = $this->Shift->findCurrent($fSensor[Sensor::NAME]['branch_id']);
        $conditions = array(
            'approved_order_id' => $currentOrder['ApprovedOrder']['id']??0,
            'shift_id' => $currentShift['Shift']['id'],
            'user_id' => $this->Auth->user()['id'],
        );
        if($this->request->is('ajax') && !empty($this->request->data) && isset($this->request->data['loss_id'])){
            $lossType = $this->LossType->findById($this->request->data['loss_id']);
            $appendedLossTypesList = array(-1, 0, $lossType['LossType']['id'], $lossType['LossType']['parent_id']); //-1 reiskia kieki visame uzsakyme, 0 esamoje pamainoje, >=1 kieki esamoje pamainoje konkreciam nuostoliui
            if(!$currentOrder){
                echo json_encode(array($lossType['LossType']['id'] => __('Defektas neužfiksuotas, kadangi darbo centre nėra parinkto užsakymo. Norėdami fiksuoti defektus pirmiausiai pasirinkite vykdomą užsakymą')));
                die();
            }
            $lossExist = $this->Loss->find('first', array('conditions'=>array_merge(array('loss_type_id'=>$lossType['LossType']['id']),$conditions)));
            if(!$lossExist){
                $this->Loss->save(array_merge($conditions, array(
                    'value'=>1, 'created_at'=>date('Y-m-d H:i:s'), 'loss_type_id'=>$lossType['LossType']['id']
                )));
            }else{
                $this->Loss->id = $lossExist['Loss']['id'];
                $this->Loss->save(array('value'=>$lossExist['Loss']['value'] + 1));
            }
        }
        $this->Loss->bindModel(array('belongsTo'=>array('LossType')));
        $totalByLossesParents = Hash::combine($this->Loss->find('all',array(
            'fields'=>array('SUM(ROUND(Loss.value)) AS total', 'LossType.parent_id'),
            'conditions'=>array_merge(array('parent_id >'=>0),$conditions),
            'group'=>array('LossType.parent_id')
        )),'{n}.LossType.parent_id', '{n}.0.total');
        $totalByLosses = $this->Loss->find('all',array(
            'fields'=>array(
                'SUM(ROUND(Loss.value)) AS total_in_order',
                'SUM(IF(Loss.shift_id = '.$currentShift['Shift']['id'].', ROUND(Loss.value), 0)) AS total_in_current_shift',
                'TRIM(Loss.loss_type_id) AS loss_type_id'
            ),'conditions'=>array(
                'approved_order_id' => $currentOrder['ApprovedOrder']['id']??0,
                //'shift_id' => $currentShift['Shift']['id'],
                //'user_id' => $this->Auth->user()['id'],
            ),
            'group'=>array('Loss.loss_type_id')
        ));//'{n}.Loss.loss_type_id', '{n}.0');
        $currentShiftLosses = Hash::combine($totalByLosses, '{n}.0.loss_type_id', '{n}.0.total_in_current_shift');
        $allOrderLosses = Hash::combine($totalByLosses, '{n}.0.loss_type_id', '{n}.0.total_in_order');
        $totals = $currentShiftLosses + $totalByLossesParents + array(0 => !empty($currentShiftLosses)?array_sum($currentShiftLosses):0) + array(-1 => !empty($allOrderLosses)?array_sum($allOrderLosses):0);
        if(isset($appendedLossTypesList)){
            echo json_encode(array_intersect_key($totals, array_flip(array_unique($appendedLossTypesList))));
            die();
        }
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(array(
            'h1_for_layout' => __('Defektų registravimas'),
            'icon_for_layout' => 'iconfa-check',
            'obliavimasLossesTypes' => $this->LossType->find('threaded', array('conditions'=>array('LossType.sensor_types'=>Mediresta::OBLIAVIMAS_TYPE))),
            'totals' => $totals,
        ));
    }

}