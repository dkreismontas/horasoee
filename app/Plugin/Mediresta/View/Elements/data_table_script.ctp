<script type="text/javascript">

    jQuery('#palletsTable').dataTable({
        "sPaginationType": "full_numbers",
        //"bScrollInfinite" : true,
        "aLengthMenu": [[100, 200, 500, -1], [100, 200, 500, "<?php echo __('Visi'); ?>"]],
        "aaSorting": [],
        "bScrollCollapse" : true,
        "sScrollY" : "800px",
        "iDisplayLength": -1,
        "paging": true,
        "scrollY": 500,
        "oLanguage":{
            'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
            'sSearch':'<?php echo __('Paieška');?>',
            'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
            'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
            'sInfoEmpty':"",
            'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
            'oPaginate':{
                'sFirst': '<?php echo __('Pirmas');?>',
                'sPrevious': '<?php echo __('Ankstesnis');?>',
                'sNext': '<?php echo __('Kitas');?>',
                'sLast': '<?php echo __('Paskutinis');?>'
            }
        },
        "oSearch": {"sSearch": "<?php echo $this->Session->read('FilterText.'.$this->params['controller']); ?>"}
    });
</script>