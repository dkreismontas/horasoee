<?php
ob_end_clean();
error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 11,
        'name' => 'Times New Roman',
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

//Pamaininė defektų ataskaita ------------------------------------
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Patikrų ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbo centras'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Patikros laikas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Patikra atlikta'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbuotojas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminys'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$rowNr = 2;
foreach($revises as $revise){
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $revise['Sensor']['name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $revise['RevisesData']['created']);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, (int)$revise['RevisesData']['confirmed']>0?$revise['RevisesData']['confirmed']:__('Neatlikta'));
    if((int)$revise['RevisesData']['confirmed']>0 && strtotime($revise['RevisesData']['confirmed']) - strtotime($revise['RevisesData']['created']) < 180){
        $activeSheet->getStyle(cts($colNr, $rowNr))->applyFromArray(array(
            'fill' => array(
                'type' => PHPExcel_Style_Fill::FILL_SOLID,
                'color' => array('rgb' => 'ff7e8e')
            )
        ));
    }
    $colNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $revise['User']['first_name'].' '.$revise['User']['last_name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $revise['Plan']['production_name']);
    $rowNr++;
}
for ($col = 0; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Patikrų ataskaita') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
