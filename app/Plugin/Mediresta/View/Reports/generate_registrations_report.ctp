<?php
ob_end_clean();
error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 11,
        'name' => 'Times New Roman',
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

//Paketu ataskaita ------------------------------------
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Paketų ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketo ID'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Lentos storis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Lentos plotis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Kamera'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 1'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 2'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 3'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pastabos'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pakrovimo data'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketą sukūrė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketas sukurtas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketą pabaigė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketas pabaigtas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->getRowDimension(1)->setRowHeight(30);
$rowNr = 2;
foreach($pallets as $pallet){
    if($pallet['Pallet']['parent_id'] > 0){ continue; }
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['id']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['height']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['width']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['camera']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_1']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_2']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_3']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['comments']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['load_date']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$pallet['Pallet']['created_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['created']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$pallet['Pallet']['registered_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['registered']);
    $rowNr++;
}
for ($col = 0; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}

//Neperkeltos paletes ------------------------------------
$activeSheet = $objPHPExcel->createSheet(1);
$activeSheet->setTitle(__('Palečių ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletės ID'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletės numeris'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Lentos storis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Lentos plotis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletės aukštis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletės tūris, m3'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Kamera'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketo rūšis'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 1'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 2'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Drėgmė 3'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pastabos'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pakrovimo data'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletę sukūrė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletė sukurta'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletę užregistravo'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletė užregistruota'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletę perkėlė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paletė perkelta'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->getRowDimension(1)->setRowHeight(30);
$rowNr = 2;
foreach($pallets as $pallet){
    if(!$pallet['Pallet']['parent_id']){ continue; }
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['id']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['pallet_number'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['height']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['width']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['pallet_height']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['pallet_volume']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['camera']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['package_type']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_1']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_2']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['humidity_3']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['comments']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['load_date']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$pallet['Pallet']['created_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['created']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$pallet['Pallet']['registered_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['registered']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$pallet['Pallet']['moved_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $pallet['Pallet']['moved']);
    $rowNr++;
}
for ($col = 0; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}

//Neisvezta produkcija ------------------------------------
$activeSheet = $objPHPExcel->createSheet(2);
$activeSheet->setTitle(__('Produkcijos ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketo ID'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketo numeris'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkto aukštis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkto plotis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkto ilgis, mm'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkto kiekis pakete, vnt.'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamybos data'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pastabos'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketą užregistravo'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Paketas užregistruotas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkciją perkėlė'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Produkcija perkelta'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->getRowDimension(1)->setRowHeight(30);
$rowNr = 2;
foreach($packets as $packet){
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['id']);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $packet['Packet']['packet_number'])->getStyle(cts($colNr++, $rowNr))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['height']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['width']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['length']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['production_date']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['comments']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$packet['Packet']['registered_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['registered']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $usersList[$packet['Packet']['moved_user_id']]??'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $packet['Packet']['moved']);
    $rowNr++;
}
for ($col = 0; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Registracijos ataskaita') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
