<?php
ob_end_clean();
error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 11,
        'name' => 'Times New Roman',
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

//Pamaininė defektų ataskaita ------------------------------------
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Pamaininė defektų ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Laikotarpis'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbo centras'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$lossTypeMap = array();
foreach($obliavimasLossesTypes as $lossType){
    if(isset($lossType['children']) && !empty($lossType['children'])){
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $lossType['LossType']['name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
        $lossTypeMap[$lossType['LossType']['id']] = $rowNr++;
        foreach($lossType['children'] as $childLossType){
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $childLossType['LossType']['name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray);
            $lossTypeMap[$childLossType['LossType']['id']] = $rowNr++;
        }
    }else{
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $lossType['LossType']['name']);
        $lossTypeMap[$lossType['LossType']['id']] = $rowNr++;
    }
}
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Iš viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
$lossTypeMap[0] = $rowNr++;
$colNr++;
$totalsByLossType = array();
$totalOfTotals = 0;
foreach($lossesByShifts as $shiftName => $lossesInShifts){
    $rowNr = 1;
    $activeSheet->mergeCells(nts($colNr).$rowNr.':'.nts($colNr+sizeof($lossesInShifts)-1).$rowNr)->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $shiftName);
    $activeSheet->getRowDimension($rowNr)->setRowHeight(40);
    foreach($lossesInShifts as $sensorName => $lossesInSensors){
        $rowNr = 2;
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $sensorName)->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray); $rowNr++;
        $childrenQuantitySum = array();
        $totalInShift = 0;
        foreach($lossesInSensors as $lossTypeId => $loss){
            if($loss['loss_parent_id'] > 0){
                if(!isset($childrenQuantitySum[$loss['loss_parent_id']])){ $childrenQuantitySum[$loss['loss_parent_id']] = 0; }
                $childrenQuantitySum[$loss['loss_parent_id']] += $loss['quantity'];
            }
            $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossTypeId], (float)$loss['quantity'])->getStyle(cts($colNr, $lossTypeMap[$lossTypeId]))->applyFromArray($centerAlignStyleArray);
            $totalInShift += $loss['quantity'];
            $totalOfTotals += $loss['quantity'];
            if(!isset($totalsByLossType[$lossTypeId])){ $totalsByLossType[$lossTypeId] = 0; }
            $totalsByLossType[$lossTypeId] += $loss['quantity'];
        }
        foreach($childrenQuantitySum as $lossParentId => $quantity){
            if(!isset($totalsByLossType[$lossParentId])){ $totalsByLossType[$lossParentId] = 0; }
            $totalsByLossType[$lossParentId] += $quantity;
            $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossParentId], (float)$quantity)->getStyle(cts($colNr, $lossTypeMap[$lossParentId]))->applyFromArray($centerAlignStyleArray);
        }
        $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[0], (float)$totalInShift)->getStyle(cts($colNr, $lossTypeMap[0]))->applyFromArray($centerAlignStyleArray);
        $colNr++;
    }
}
$activeSheet->getStyle(nts(0).'1:'.nts($colNr).(max($lossTypeMap)))->applyFromArray($borderStyleArray);
$rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Iš viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
foreach($totalsByLossType as $lossTypeId => $totalByLossType){
    $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossTypeId], (float)$totalByLossType)->getStyle(cts($colNr, $lossTypeMap[$lossTypeId]))->applyFromArray($centerAlignStyleArray);
}
$activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[0], (float)$totalOfTotals)->getStyle(cts($colNr, $lossTypeMap[0]))->applyFromArray($centerAlignStyleArray);
$activeSheet->getColumnDimension(nts(0))->setWidth(40);
for ($col = 1; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(23);
}


//užsakymų defektų ataskaita-----------------------------------------
$activeSheet = $objPHPExcel->createSheet(1);
$activeSheet->setTitle(__('Užsakymų defektų ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo duomenys'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pradžia')); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pabaiga')); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Darbo centras')); $rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminio pavadinimas')); $rowNr++;$rowNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Defektai'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2); $rowNr++;
$lossTypeMap = array();
foreach($obliavimasLossesTypes as $lossType){
    if(isset($lossType['children']) && !empty($lossType['children'])){
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $lossType['LossType']['name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
        $lossTypeMap[$lossType['LossType']['id']] = $rowNr++;
        foreach($lossType['children'] as $childLossType){
            $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $childLossType['LossType']['name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray);
            $lossTypeMap[$childLossType['LossType']['id']] = $rowNr++;
        }
    }else{
        $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $lossType['LossType']['name']);
        $lossTypeMap[$lossType['LossType']['id']] = $rowNr++;
    }
}
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Iš viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
$lossTypeMap[0] = $rowNr++;
$colNr++;
$totalOfTotals = 0;
$totalsByLossType = array();
foreach($lossesByOrder as $orderId => $lossByOrder){
    $rowNr = 2;
    $orderData = current($lossByOrder);
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $orderData['order_start'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray); $rowNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $orderData['order_end'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray); $rowNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $orderData['sensor_name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray); $rowNr++;
    $activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, $orderData['production_name'])->getStyle(cts($colNr, $rowNr))->applyFromArray($centerAlignStyleArray); $rowNr += 3;
    $childrenQuantitySum = array();
    $totalInOrder = 0;
    foreach($lossByOrder as $lossTypeId => $loss){
        if($loss['loss_parent_id'] > 0){
            if(!isset($childrenQuantitySum[$loss['loss_parent_id']])){ $childrenQuantitySum[$loss['loss_parent_id']] = 0; }
            $childrenQuantitySum[$loss['loss_parent_id']] += $loss['quantity'];
        }
        $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossTypeId], (float)$loss['quantity'])->getStyle(cts($colNr, $lossTypeMap[$lossTypeId]))->applyFromArray($centerAlignStyleArray);
        $totalInOrder += $loss['quantity'];
        $totalOfTotals += $loss['quantity'];
        if(!isset($totalsByLossType[$lossTypeId])){ $totalsByLossType[$lossTypeId] = 0; }
        $totalsByLossType[$lossTypeId] += $loss['quantity'];
    }
    foreach($childrenQuantitySum as $lossParentId => $quantity){
        if(!isset($totalsByLossType[$lossParentId])){ $totalsByLossType[$lossParentId] = 0; }
        $totalsByLossType[$lossParentId] += $quantity;
        $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossParentId], (float)$quantity)->getStyle(cts($colNr, $lossTypeMap[$lossParentId]))->applyFromArray($centerAlignStyleArray);
    }
    $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[0], (float)$totalInOrder)->getStyle(cts($colNr, $lossTypeMap[0]))->applyFromArray($centerAlignStyleArray);
    $colNr++;
}
$activeSheet->getStyle(nts(0).'1:'.nts($colNr).(max($lossTypeMap)))->applyFromArray($borderStyleArray);
$rowNr = 7;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Iš viso'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray2);
foreach($totalsByLossType as $lossTypeId => $totalByLossType){
    $activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[$lossTypeId], (float)$totalByLossType)->getStyle(cts($colNr, $lossTypeMap[$lossTypeId]))->applyFromArray($centerAlignStyleArray);
}
$activeSheet->setCellValueByColumnAndRow($colNr, $lossTypeMap[0], (float)$totalOfTotals)->getStyle(cts($colNr, $lossTypeMap[0]))->applyFromArray($centerAlignStyleArray);
$activeSheet->getColumnDimension(nts(0))->setWidth(40);
for ($col = 1; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Pamaininė defektų ataskaita') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
