<style>
    table td{
        vertical-align: middle !important;
    }
</style>
<div>
<div class="row-fluid">
    <div class="<?php echo !$this->request->is('ajax')?'col-md-6':''; ?>">
        <table class="table table-bordered table-striped" id="lossesTable">
            <thead>
            <tr>
                <th style="width: 50%;"><?php echo __('Defektas'); ?></th>
                <?php if($addActionButtons){ ?><th style="width: 20%;"><?php echo __('Registravimas'); ?></th><?php } ?>
                <th style="width: 30%;"><?php echo __('Iš viso'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php
            echo printTableLine($obliavimasLossesTypes, 0, $totals, $addActionButtons);
            ?>
            <tr class="loss_id_0">
                <td colspan="<?php echo $addActionButtons?2:0; ?>" class="text-right"><h4><?php echo __('Iš viso užfiksuota defektų esamoje pamainoje'); ?>:</h4></td>
               <td class="total text-center"><h3><?php echo $totals[0]; ?></h3></td>
            </tr>
            <tr class="loss_id_-1">
               <td colspan="<?php echo $addActionButtons?2:0; ?>" class="text-right"><h4><?php echo __('Iš viso užfiksuota defektų visame užsakyme'); ?>:</h4></td>
               <td class="total text-center"><h3><?php echo $totals[-1]; ?></h3></td>
            </tr>
            </tbody>
        </table>
    </div>
</div>
</div>
<?php

function printTableLine($obliavimasLossesTypes, $level, $totals, $addActionButtons){
    $rows = '';
    foreach($obliavimasLossesTypes as $loss){
        $boldParent = '';
        $addButton = '<button class="btn btn-warning registrate" data-loss_id="'.$loss['LossType']['id'].'">'.__('Fiksuoti').'</button>';
        $childRows = '';
        if(!empty($loss['children'])){
            $childRows = printTableLine($loss['children'], $level+1, $totals, $addActionButtons);
            $boldParent = 'font-weight: bold;';
            $addButton = '';
        }
        $currentRow = '
            <tr class="loss_id_'.$loss['LossType']['id'].'">
                <td><h4 style="padding-left: '.($level*15).'%; '.$boldParent.'">'.$loss['LossType']['name'].'</h4></td>
                '.($addActionButtons?'<td><div class="buttonWrapper" style="position: relative; text-align: center;">'.$addButton.'</div></td>':'').'
                <td class="total text-center"><h3>'.($totals[$loss['LossType']['id']]??'').'</h3></td>
            </tr>';
        $rows .= $currentRow.$childRows;
    }
    return $rows;
}
if($addActionButtons){ ?>
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('button.registrate').bind('click', function(){
            $(this).closest('tr').find('td.total').html('<img class="loader" src="/images/loaders/loader12.gif" />');
            $.ajax({
                url: '<?php echo $this->here; ?>',
                type: 'POST',
                data: $(this).data(),
                dataType: 'JSON',
                context: $(this),
                success: function(losses) {
                    $(this).closest('tr').find('td.total img.loader').remove();
                    for(var i in losses){
                        let elem = $('#lossesTable tr.loss_id_'+i+' td.total');
                        if($.isNumeric(losses[i])){
                            var color = '7bfc8a';
                            var wrapIn = 'h3';
                        }else{
                            var color = 'ff8979';
                            var wrapIn = 'p';
                        }
                        elem.html('<'+wrapIn+'>'+losses[i]+'</'+wrapIn+'>').animate({backgroundColor:'#'+color}, 500, function(){
                           elem.animate({backgroundColor:'#fff'}, 500);
                        })
                    }
                }
            })
        });
    });
</script>
<?php } ?>
