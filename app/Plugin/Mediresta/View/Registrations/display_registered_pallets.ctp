<div style="display: none"><div id="popupContainer"></div></div>
    <br class="clearfix" />
    <table class="table table-bordered table-striped" id="palletsTable">
        <thead>
        <tr>
            <th><?php echo __('ID'); ?></th>
            <th><?php echo __('Užregistruota'); ?></th>
            <th><?php echo __('Paletės numeris'); ?></th>
            <th><?php echo __('Paketo rūšis'); ?></th>
            <th><?php echo __('Paletės aukštis, mm'); ?></th>
            <th><?php echo __('Paletės tūris, m3'); ?></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php if (empty($pallets)): ?>
            <tr>
                <td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td>
            </tr>
        <?php endif; ?>
        <?php foreach ($pallets as $pallet): ?>
            <tr>
                <td><?php echo $pallet['Pallet']['id']; ?></td>
                <td><?php echo $pallet['Pallet']['registered']; ?></td>
                <td><?php echo $pallet['Pallet']['pallet_number']; ?></td>
                <td><?php echo $pallet['Pallet']['package_type']; ?></td>
                <td><?php echo $pallet['Pallet']['pallet_height']; ?></td>
                <td><?php echo $pallet['Pallet']['pallet_volume']; ?></td>
                <td>
                    <?php echo $hasAccessToEditPallet?$this->Html->link('<span class="glyphicon glyphicon-pencil"></span> '.__('Redaguoti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'edit_registered_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false)):''; ?>
                    <?php echo $hasAccessToDeletePallet?$this->Html->link('<span class="glyphicon glyphicon-trash"></span> '.__('Pašalinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'delete_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false),__('Ar tikrai norite pašalinti šią paletę?')):''; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            let newPalletNumber = '<?php echo $this->Session->flash('pallet_creation_success'); ?>';
            if (newPalletNumber.length) {
                console.log(newPalletNumber)
                jQuery('#popupContainer').html(newPalletNumber);
                popupWindow('#popupContainer', 'auto', '500', 'auto', "<?php echo __('Suteiktas paletės numeris'); ?> ", "<?php echo __('Uždaryti langą') ?>");
            }
        });
    </script>
<?php
echo !empty($pallets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
