<?php
echo $this->Form->create('Pallet');
?>
<div class="row-fluid">
    <div class="col-md-3">
        <?php
        echo $this->Form->input('id', array('type'=>'hidden'));
        echo $this->Form->input('height', array('type'=>'text', 'label'=>__('Lentos storis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('width', array('type'=>'text', 'label'=>__('Lentos plotis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('load_date', array('type'=>'text', 'label'=>__('Pakrovimo data'), 'class'=>'form-control datepicker', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('camera', array('type'=>'text', 'label'=>__('Kameros numeris'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        ?>
    </div>
    <div class="col-md-3">
        <?php
        echo $this->Form->input('humidity_1', array('type'=>'text', 'label'=>__('Drėgmė 1'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('humidity_2', array('type'=>'text', 'label'=>__('Drėgmė 2'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('humidity_3', array('type'=>'text', 'label'=>__('Drėgmė 3'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        ?>
    </div>
    <div class="col-md-3">
        <?php
        echo $this->Form->input('comments', array('type'=>'textarea', 'label'=>__('Pastabos'), 'class'=>'form-control'));
        if(!empty($this->request->data) && $this->request->data['Pallet']['id'] > 0){
            echo '<h4><strong>'.__('Paketas užregistruotas').': </strong> '.$this->request->data['Pallet']['created'].'</h4>';
            echo '<h4><strong>'.__('Paketą sukūrė').': </strong> '.($usersList[$this->request->data['Pallet']['created_user_id']]??'').'</h4>';
        }
        ?>
    </div>
    <br class="clearfix" />
</div>
<div class="row-fluid"><div class="col-md-3">
<?php
echo $this->Form->submit(__('Išsaugoti'), array('class'=>'btn btn-primary', 'div'=>false)).'&nbsp;';
echo $this->Html->link(__('Atšaukti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'display_pallets'), array('class'=>'btn btn-primary',));
echo $this->Form->end();
?>
</div></div>
<script type="text/javascript" charset="utf-8">
    jQuery("document").ready(function () {
        $('.datepicker').datepicker(
            <?php echo json_encode($this->App->datePickerConfig()) ?>
        );
    });
</script>