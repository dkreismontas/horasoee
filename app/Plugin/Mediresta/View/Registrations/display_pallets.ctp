<?php echo $this->Html->link(__('Registruoti naują paketą'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'edit_pallet'), array('class'=>'btn btn-default pull-right')); ?>
<br class="clearfix" />
<table class="table table-bordered table-striped" id="palletsTable">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Užregistruota'); ?></th>
        <th><?php echo __('Storis,mm'); ?></th>
        <th><?php echo __('Plotis,mm'); ?></th>
        <th><?php echo __('Pakrovimo data'); ?></th>
        <th><?php echo __('Kamera'); ?></th>
        <th><?php echo __('Drėgmė 1'); ?></th>
        <th><?php echo __('Drėgmė 2'); ?></th>
        <th><?php echo __('Drėgmė 3'); ?></th>
        <th><?php echo __('Pastabos'); ?></th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($pallets)): ?>
        <tr>
            <td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($pallets as $pallet): ?>
        <tr class="<?php echo $pallet['Pallet']['registered']?'success':'' ?>">
            <td><?php echo $pallet['Pallet']['id']; ?></td>
            <td><?php echo $pallet['Pallet']['created']; ?></td>
            <td><?php echo $pallet['Pallet']['height']; ?></td>
            <td><?php echo $pallet['Pallet']['width']; ?></td>
            <td><?php echo $pallet['Pallet']['load_date']; ?></td>
            <td><?php echo $pallet['Pallet']['camera']; ?></td>
            <td><?php echo $pallet['Pallet']['humidity_1']; ?></td>
            <td><?php echo $pallet['Pallet']['humidity_2']; ?></td>
            <td><?php echo $pallet['Pallet']['humidity_3']; ?></td>
            <td><?php echo $pallet['Pallet']['comments']; ?></td>
            <td>
                <?php echo $hasAccessToEdit?$this->Html->link('<span class="glyphicon glyphicon-pencil"></span> '.__('Redaguoti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'edit_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false)):''; ?>
                <?php echo $hasAccessToFinish && !$pallet['Pallet']['registered']?$this->Html->link('<span class="glyphicon glyphicon-pencil"></span> '.__('Pabaigti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'finish_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false),__('Ar tikrai norite pabaigti šį paketą?')):''; ?>
                <?php echo $hasAccessToDelete?$this->Html->link('<span class="glyphicon glyphicon-trash"></span> '.__('Pašalinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'delete_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false),__('Ar tikrai norite pašalinti šį paketą?')):''; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
echo !empty($pallets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>