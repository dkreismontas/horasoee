<?php
if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android')){
    $this->Html->css('Mediresta.android_browser_mediresta', array('inline'=>false));
}
?>
<div style="display: none"><div id="transfrerConfirmation" class="text-center">
    <h3><?php echo __('Ar tikrai norite išvežti produkciją nr.: %s?', $this->request->data['Packet']['transfer_packet_number']??''); ?></h3><br />
    <button class="btn btn-primary" id="confirmYes"><?php echo __('Taip'); ?></button>
    <button class="btn btn-default" id="confirmNo"><?php echo __('Ne'); ?></button>
</div></div>
<div class="row-fluid"><div class="col-md-5 col-lg-4"></div></div>
<div class="row-fluid">
    <div class="col-md-2 col-lg-4 text-center">
        <?php
        echo $this->Form->create('Packet',array('class'=>'transferFrom'));
        ?><h3><?php echo __('Produkcijos numeris'); ?></h3><br /><?php
        echo $this->Form->input('save_confirmed', array('type'=>'hidden', 'value'=>0));
        echo $this->Form->input('transfer_packet_number', array('type'=>'text', 'label'=>false, 'class'=>'form-control input-small','div'=>false, 'oninvalid'=>'this.setCustomValidity("'.__('Įveskite produkcijos numerį prieš atlikdami išvežimą').'")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->submit(__('Išvežti'), array('class'=>'btn btn-primary', 'div'=>false));
        echo $this->Form->end();
        ?>
    </div>
</div>
<table class="table table-bordered table-striped" id="PacketsTable">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Perkelta'); ?></th>
        <th><?php echo __('Aukštis,mm'); ?></th>
        <th><?php echo __('Plotis,mm'); ?></th>
        <th><?php echo __('Ilgis, mm'); ?></th>
        <th><?php echo __('Produktų kiekis pakete, vnt'); ?></th>
        <th><?php echo __('Gamybos data'); ?></th>
        <th><?php echo __('Produkcijos numeris'); ?></th>
        <th><?php echo __('Pastabos'); ?></th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($packets)): ?>
        <tr>
            <td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($packets as $packet): ?>
        <tr>
            <td><?php echo $packet['Packet']['id']; ?></td>
            <td><?php echo $packet['Packet']['moved']; ?></td>
            <td><?php echo $packet['Packet']['height']; ?></td>
            <td><?php echo $packet['Packet']['width']; ?></td>
            <td><?php echo $packet['Packet']['length']; ?></td>
            <td><?php echo $packet['Packet']['quantity']; ?></td>
            <td><?php echo $packet['Packet']['production_date']; ?></td>
            <td><?php echo $packet['Packet']['packet_number']; ?></td>
            <td><?php echo $packet['Packet']['comments']; ?></td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
    <?php if($transferConfirmation): ?>
    popupWindow('#transfrerConfirmation', 'auto', '500', 'auto', "<?php echo __('Išvežimo patvirtinimas'); ?> ", "<?php echo __('Uždaryti langą') ?>");
    $('#confirmNo').bind('click', function(){
        $('.ui-dialog-titlebar-close').trigger('click');
    });
    $('#confirmYes').bind('click', function(){
        $('#PacketSaveConfirmed').val('1');
        $('#PacketTransferPacketsForm').trigger('submit');
    });
    <?php endif; ?>
</script>
<?php
echo !empty($pallets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
<div style="height:100px; width: 100%; display: block;"></div>