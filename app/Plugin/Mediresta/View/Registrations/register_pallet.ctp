<?php
echo $this->Form->create('Pallet');
?>
<div class="row-fluid">
    <div class="col-md-3">
        <?php
        echo $this->Form->input('parent_id', array('type'=>'hidden'));
        echo $this->Form->input('pallet_volume', array('type'=>'hidden'));
        echo $this->Form->input('package_type', array('type'=>'text', 'label'=>__('Paketo rūšis'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('pallet_height', array('type'=>'text', 'label'=>__('Paletės aukštis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        $options = array('---',0.5,0.6,0.7,0.8,0.85,0.9);
        echo $this->Form->input('fill_factor', array('type'=>'select', 'label'=>__('Užpildymo koeficientas'), 'class'=>'form-control', 'default'=>'', 'options'=>array_combine(array_merge(array(''), array_slice($options,1)),$options), 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        ?>
    </div>
    <div class="col-md-3">
        <?php
        echo '<h4><strong>'.__('Tūris m3').': </strong><span id="volume"></span></h4>';
        echo '<h4><strong>'.__('Lentos storis, mm').': </strong><span>'.$this->request->data['Pallet']['height'].'</span></h4>';
        echo '<h4><strong>'.__('Lentos plotis, mm').': </strong><span>'.$this->request->data['Pallet']['width'].'</span></h4>';
        echo '<h4><strong>'.__('Pakrovimo data').': </strong><span>'.$this->request->data['Pallet']['load_date'].'</span></h4>';
        echo '<h4><strong>'.__('Kameros numeris').': </strong><span>'.$this->request->data['Pallet']['camera'].'</span></h4>';
        echo '<h4><strong>'.__('Drėgmė 1').': </strong><span>'.$this->request->data['Pallet']['humidity_1'].'</span></h4>';
        echo '<h4><strong>'.__('Drėgmė 2').': </strong><span>'.$this->request->data['Pallet']['humidity_2'].'</span></h4>';
        echo '<h4><strong>'.__('Drėgmė 3').': </strong><span>'.$this->request->data['Pallet']['humidity_3'].'</span></h4>';
        echo '<h4><strong>'.__('Pastabos').': </strong><span>'.$this->request->data['Pallet']['comments'].'</span></h4>';
        ?>
    </div>
    <br class="clearfix" />
</div>
<div class="row-fluid"><div class="col-md-3">
        <?php
        echo $this->Form->submit(__('Išsaugoti'), array('class'=>'btn btn-primary', 'div'=>false)).'&nbsp;';
        echo $this->Html->link(__('Atšaukti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'display_unregistered_pallets'), array('class'=>'btn btn-primary',));
        echo $this->Form->end();
        ?>
    </div></div>
<?php $this->Html->script('Mediresta.volume_calculation', array('inline'=>false)); ?>