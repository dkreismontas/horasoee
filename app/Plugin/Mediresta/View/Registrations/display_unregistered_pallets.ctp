<div style="display: none"><div id="popupContainer"></div></div>
<?php
    echo $display_registered_pallets?$this->Html->link(__('Sukurtos paletės'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'display_registered_pallets'), array('class'=>'btn btn-default pull-right')):'';
    echo $hasAccessToAdditionalPallet?$this->Html->link(__('Papildoma paletė'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'register_additional_pallet'), array('class'=>'btn btn-default pull-right', 'style'=>'margin-right: 5px;')):'';
?>
    <br class="clearfix" />
    <table class="table table-bordered table-striped" id="palletsTable">
        <thead>
        <tr>
            <th><?php echo __('ID'); ?></th>
            <th><?php echo __('Užregistruota'); ?></th>
            <th><?php echo __('Storis,mm'); ?></th>
            <th><?php echo __('Plotis,mm'); ?></th>
            <th><?php echo __('Pakrovimo data'); ?></th>
            <th><?php echo __('Kamera'); ?></th>
            <th><?php echo __('Drėgmė 1'); ?></th>
            <th><?php echo __('Drėgmė 2'); ?></th>
            <th><?php echo __('Drėgmė 3'); ?></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php if (empty($pallets)): ?>
            <tr>
                <td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td>
            </tr>
        <?php endif; ?>
        <?php foreach ($pallets as $pallet): ?>
            <tr>
                <td><?php echo $pallet['Pallet']['id']; ?></td>
                <td><?php echo $pallet['Pallet']['created']; ?></td>
                <td><?php echo $pallet['Pallet']['height']; ?></td>
                <td><?php echo $pallet['Pallet']['width']; ?></td>
                <td><?php echo $pallet['Pallet']['load_date']; ?></td>
                <td><?php echo $pallet['Pallet']['camera']; ?></td>
                <td><?php echo $pallet['Pallet']['humidity_1']; ?></td>
                <td><?php echo $pallet['Pallet']['humidity_2']; ?></td>
                <td><?php echo $pallet['Pallet']['humidity_3']; ?></td>
                <td>
                    <?php echo $this->Html->link(__('Sukurti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'register_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false)); ?><br />
                    <?php //echo $hasAccessToDelete?$this->Html->link('<span class="glyphicon glyphicon-trash"></span> '.__('Pašalinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'delete_pallet', $pallet['Pallet']['id']), array('class'=>'', 'escape'=>false),__('Ar tikrai norite pašalinti šį paketą?')):''; ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <script type="text/javascript">
        jQuery("document").ready(function ($) {
            let newPalletNumber = '<?php echo $this->Session->flash('pallet_creation_success'); ?>';
            if (newPalletNumber.length) {
                console.log(newPalletNumber)
                jQuery('#popupContainer').html(newPalletNumber);
                popupWindow('#popupContainer', 'auto', '500', 'auto', "<?php echo __('Suteiktas paletės numeris'); ?> ", "<?php echo __('Uždaryti langą') ?>");
            }
        });
    </script>
<?php
echo !empty($pallets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
