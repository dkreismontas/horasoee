<?php
echo $this->Form->create('Packet');
?>
    <div class="row-fluid">
        <div class="col-md-3">
            <?php
            echo $this->Form->input('id', array('type'=>'hidden'));
            echo $this->Form->input('height', array('type'=>'text', 'label'=>__('Produkto aukštis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
            echo $this->Form->input('width', array('type'=>'text', 'label'=>__('Produkto plotis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
            echo $this->Form->input('length', array('type'=>'text', 'label'=>__('Produkto ilgis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
            echo $this->Form->input('quantity', array('type'=>'text', 'label'=>__('Produkto kiekis pakete, vnt.'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
            echo $this->Form->input('production_date', array('type'=>'text', 'label'=>__('Gamybos data'), 'class'=>'form-control datepicker', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
            ?>
        </div>
        <div class="col-md-3">
            <?php
            echo $this->Form->input('comments', array('type'=>'textarea', 'label'=>__('Pastabos'), 'class'=>'form-control'));
            ?>
        </div>
        <br class="clearfix" />
    </div>
    <div class="row-fluid"><div class="col-md-3">
            <?php
            echo $this->Form->submit(__('Išsaugoti'), array('class'=>'btn btn-primary', 'div'=>false)).'&nbsp;';
            echo $this->Html->link(__('Atšaukti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'display_unmoved_packets'), array('class'=>'btn btn-primary',));
            echo $this->Form->end();
            ?>
        </div></div>
<div style="display: none"><div id="popupContainer"></div></div>
<?php $this->Html->script('Mediresta.volume_calculation', array('inline'=>false)); ?>
<script type="text/javascript" charset="utf-8">
    jQuery("document").ready(function () {
        $('.datepicker').datepicker(
            <?php echo json_encode($this->App->datePickerConfig()) ?>
        );
    });
    let newPacketNumber = '<?php echo $this->Session->flash('packet_creation_success'); ?>';
    if (newPacketNumber.length) {
        jQuery('#popupContainer').html(newPacketNumber);
        popupWindow('#popupContainer', 'auto', '500', 'auto', "<?php echo __('Suteiktas produkcijos numeris'); ?> ", "<?php echo __('Uždaryti langą') ?>");
    }
</script>
