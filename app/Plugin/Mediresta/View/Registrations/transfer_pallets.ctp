<?php
if(strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'android')){
    $this->Html->css('Mediresta.android_browser_mediresta', array('inline'=>false));
}
?>
<div style="display: none"><div id="transfrerConfirmation" class="text-center">
    <h3><?php echo __('Ar tikrai norite perkelti paletę?'); ?></h3><br />
    <button class="btn btn-primary" id="confirmYes"><?php echo __('Taip'); ?></button>
    <button class="btn btn-default" id="confirmNo"><?php echo __('Ne'); ?></button>
</div></div>
<div class="row-fluid"><div class="col-md-5 col-lg-4"></div></div>
<div class="row-fluid">
    <div class="col-md-2 col-lg-4 text-center">
        <?php
        echo $this->Form->create('Pallet', array('class'=>'transferFrom'));
        ?><h3><?php echo __('Paletės numeris'); ?></h3><br /><?php
        echo $this->Form->input('save_confirmed', array('type'=>'hidden', 'value'=>0));
        echo $this->Form->input('transfer_pallet_number', array('type'=>'text', 'label'=>false, 'class'=>'form-control input-small','div'=>false, 'oninvalid'=>'this.setCustomValidity("'.__('Įveskite paletės numerį prieš atlikdami perkėlimą').'")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->submit(__('Perkelti'), array('class'=>'btn btn-primary', 'div'=>false));
        echo $this->Form->end();
        ?>
    </div>
</div>
<div class="row-fluid"><div class="col-md-5  col-lg-4"></div></div>
<br class="clearfix" /><br />
<table class="table table-bordered table-striped" id="palletsTable">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Perkelta'); ?></th>
        <th><?php echo __('Storis,mm'); ?></th>
        <th><?php echo __('Plotis,mm'); ?></th>
        <th><?php echo __('Paletės numeris'); ?></th>
        <th><?php echo __('Paketo rūšis'); ?></th>
        <th><?php echo __('Paletės aukštis, mm'); ?></th>
        <th><?php echo __('Paletės tūris, m3'); ?></th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($pallets)): ?>
        <tr>
            <td colspan="6"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($pallets as $pallet): ?>
        <tr>
            <td><?php echo $pallet['Pallet']['id']; ?></td>
            <td><?php echo $pallet['Pallet']['moved']; ?></td>
            <td><?php echo $pallet['Pallet']['height']; ?></td>
            <td><?php echo $pallet['Pallet']['width']; ?></td>
            <td><?php echo $pallet['Pallet']['pallet_number']; ?></td>
            <td><?php echo $pallet['Pallet']['package_type']; ?></td>
            <td><?php echo $pallet['Pallet']['pallet_height']; ?></td>
            <td><?php echo $pallet['Pallet']['pallet_volume']; ?></td>
            <td>
                <?php echo $this->Html->link('<span class="glyphicon glyphicon-pencil"></span> '.__('Redaguoti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'edit_registered_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false)); ?>
                <?php echo $hasAccessToReturnPallet?$this->Html->link('<span class="glyphicon glyphicon-repeat"></span> '.__('Grąžinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'return_transfer_pallets', $pallet['Pallet']['id']), array('class'=>'btn btn-default returnPallet', 'escape'=>false)):''; ?>
                <?php echo $hasAccessToDeletePallet?$this->Html->link('<span class="glyphicon glyphicon-trash"></span> '.__('Pašalinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'delete_pallet', $pallet['Pallet']['id']), array('class'=>'btn btn-default', 'escape'=>false),__('Ar tikrai norite pašalinti šią paletę?')):''; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<script type="text/javascript">
    <?php if($transferConfirmation): ?>
    popupWindow('#transfrerConfirmation', 'auto', '500', 'auto', "<?php echo __('Perkėlimo patvirtinimas'); ?> ", "<?php echo __('Uždaryti langą') ?>");
    $('#confirmNo').bind('click', function(){
        $('.ui-dialog-titlebar-close').trigger('click');
    });
    $('#confirmYes').bind('click', function(){
        $('#PalletSaveConfirmed').val('1');
        $('#PalletTransferPalletsForm').trigger('submit');
    });
    <?php endif; ?>
    $('.returnPallet').bind('click', function(e){
        e.preventDefault();
        $.ajax({
            url: $(this).attr('href'),
            context: $(this),
            success: function(){
                $(this).closest('tr').remove();
            }
        });
    });
</script>
<?php
echo !empty($pallets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
