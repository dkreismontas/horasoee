<?php
echo $this->Form->create('Pallet');
?>
<div class="row-fluid">
    <div class="col-md-3">
        <?php
        echo $this->Form->input('pallet_volume', array('type'=>'hidden'));
        echo $this->Form->input('package_type', array('type'=>'text', 'label'=>__('Paketo rūšis'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('pallet_height', array('type'=>'text', 'label'=>__('Paletės aukštis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        $options = array('---',0.5,0.6,0.7,0.8,0.85,0.9);
        echo $this->Form->input('fill_factor', array('type'=>'select', 'label'=>__('Užpildymo koeficientas'), 'class'=>'form-control', 'default'=>'', 'options'=>array_combine(array_merge(array(''), array_slice($options,1)),$options), 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo '<h4><strong>'.__('Tūris m3').': </strong><span id="volume"></span></h4>';
        ?>
    </div>
    <div class="col-md-3">
        <?php
        echo $this->Form->input('height', array('type'=>'text', 'label'=>__('Lentos storis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('width', array('type'=>'text', 'label'=>__('Lentos plotis, mm'), 'class'=>'form-control', 'oninvalid'=>'this.setCustomValidity("Laukelis privalomas")', 'onchange'=>'this.setCustomValidity("")'));
        echo $this->Form->input('comments', array('type'=>'textarea', 'label'=>__('Pastabos'), 'class'=>'form-control'));
        ?>
    </div>
    <br class="clearfix" />
</div>
<div class="row-fluid"><div class="col-md-3">
        <?php
        echo $this->Form->submit(__('Išsaugoti'), array('class'=>'btn btn-primary', 'div'=>false)).'&nbsp;';
        echo $this->Html->link(__('Atšaukti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'display_unregistered_pallets'), array('class'=>'btn btn-primary',));
        echo $this->Form->end();
        ?>
    </div></div>
<?php $this->Html->script('Mediresta.volume_calculation', array('inline'=>false)); ?>