<?php echo $this->Html->link(__('Užregistruoti produkciją'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'register_packet'), array('class'=>'btn btn-default pull-right')); ?>
<br class="clearfix" />
<table class="table table-bordered table-striped" id="PacketsTable">
    <thead>
    <tr>
        <th><?php echo __('ID'); ?></th>
        <th><?php echo __('Užregistruota'); ?></th>
        <th><?php echo __('Aukštis,mm'); ?></th>
        <th><?php echo __('Plotis,mm'); ?></th>
        <th><?php echo __('Ilgis, mm'); ?></th>
        <th><?php echo __('Produktų kiekis pakete, vnt'); ?></th>
        <th><?php echo __('Gamybos data'); ?></th>
        <th><?php echo __('Produkcijos numeris'); ?></th>
        <th><?php echo __('Pastabos'); ?></th>
        <th>&nbsp;</th>
    </tr>
    </thead>
    <tbody>
    <?php if (empty($packets)): ?>
        <tr>
            <td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td>
        </tr>
    <?php endif; ?>
    <?php foreach ($packets as $packet): ?>
        <tr>
            <td><?php echo $packet['Packet']['id']; ?></td>
            <td><?php echo $packet['Packet']['registered']; ?></td>
            <td><?php echo $packet['Packet']['height']; ?></td>
            <td><?php echo $packet['Packet']['width']; ?></td>
            <td><?php echo $packet['Packet']['length']; ?></td>
            <td><?php echo $packet['Packet']['quantity']; ?></td>
            <td><?php echo $packet['Packet']['production_date']; ?></td>
            <td><?php echo $packet['Packet']['packet_number']; ?></td>
            <td><?php echo $packet['Packet']['comments']; ?></td>
            <td>
                <?php echo $hasAccessToEditPacket?$this->Html->link('<span class="glyphicon glyphicon-pencil"></span> '.__('Redaguoti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'edit_packet', $packet['Packet']['id']), array('class'=>'btn btn-default', 'escape'=>false)):''; ?>
                <?php echo $hasAccessToDeletePacket?$this->Html->link('<span class="glyphicon glyphicon-trash"></span> '.__('Pašalinti'), array('plugin'=>'mediresta', 'controller'=>'registrations', 'action'=>'delete_packet', $packet['Packet']['id']), array('class'=>'btn btn-default', 'escape'=>false),__('Ar tikrai norite ištrinti šią produkciją?')):''; ?>
            </td>
        </tr>
    <?php endforeach; ?>
    </tbody>
</table>
<?php
echo !empty($packets)?$this->element('Mediresta.data_table_script'):'';
$this->Html->script('jquery.dataTables.min',array('inline'=>false));
?>
<div style="display: none"><div id="popupContainer"></div></div>
<script type="text/javascript" charset="utf-8">
    let newPacketNumber = '<?php echo $this->Session->flash('packet_creation_success'); ?>';
    if (newPacketNumber.length) {
        jQuery('#popupContainer').html(newPacketNumber);
        popupWindow('#popupContainer', 'auto', '500', 'auto', "<?php echo __('Suteiktas produkcijos numeris'); ?> ", "<?php echo __('Uždaryti langą') ?>");
    }
</script>
