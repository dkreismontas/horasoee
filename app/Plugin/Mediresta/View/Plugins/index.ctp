<?php
$current_page = trim(urldecode($_GET['current_page']),'/');
if(!isset($current_page)) die();
if(false){ ?><script type="text/javascript"><?php }
    if(preg_match('/.*dashboards\/inner.*/i',trim($current_page,'/'))){ ?>
        jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#lossReportForm" aria-controls="lossReportForm" role="tab" data-toggle="tab"><?php echo __('Defektų ataskaita'); ?></a></li>');
        let lossReportForm = jQuery('#four').clone();
        lossReportForm.find('form').attr('action', '/mediresta/reports/generate_losses_report');
        lossReportForm.find('.view_angle').remove();
        jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane" id="lossReportForm">'+lossReportForm.html()+'</div>');
        jQuery('#lossReportForm select option').each(function(){
            if(jQuery.inArray(parseInt(jQuery(this).val()), [<?php echo implode(',', array_keys(array_filter($sensorsList, function($sensorType){ return $sensorType == Mediresta::OBLIAVIMAS_TYPE; }))); ?>]) < 0){
                jQuery(this).remove();
            }
        });

        jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#revisesReportForm" aria-controls="revisesReportForm" role="tab" data-toggle="tab"><?php echo __('Patikrų ataskaita'); ?></a></li>');
        let revisesReportForm = jQuery('#four').clone();
        revisesReportForm.find('form').attr('action', '/mediresta/reports/generate_revises_report');
        revisesReportForm.find('.view_angle').remove();
        jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane" id="revisesReportForm">'+revisesReportForm.html()+'</div>');
        jQuery('#revisesReportForm select option').each(function(){
            if(jQuery.inArray(parseInt(jQuery(this).val()), [<?php echo implode(',', array_keys(array_filter($sensorsList, function($sensorType){ return $sensorType == Mediresta::DYGIAVIMAS_TYPE; }))); ?>]) < 0){
                jQuery(this).remove();
            }
        });

        jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#remainingsReportForm" role="tab" data-toggle="tab"><?php echo __('Likučių ataskaita'); ?></a></li>');
        jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane text-center" id="remainingsReportForm"><br /><?php echo $this->Html->link(__('Siųstis'), array('plugin'=>'mediresta','controller'=>'reports', 'action'=>'generate_remainings_report'), array('class'=>'btn btn-primary')); ?></div>');

        jQuery('.nav.nav-tabs').append('<li role="presentation"><a href="#medirestaRegistrationsReportForm" aria-controls="revisesReportForm" role="tab" data-toggle="tab"><?php echo __('Registracijos ataskaita'); ?></a></li>');
        let medirestaRegistrationsReportForm = jQuery('#four').clone();
        medirestaRegistrationsReportForm.find('form').attr('action', '/mediresta/reports/generate_registrations_report');
        medirestaRegistrationsReportForm.find('.view_angle,.add-subscription').remove();
        jQuery('.tab-content').append('<div role="tabpanel" class="tab-pane" id="medirestaRegistrationsReportForm">'+medirestaRegistrationsReportForm.html()+'</div>');
        jQuery('#medirestaRegistrationsReportForm select').parent().parent().html('<?php echo str_replace(array("\n","\r"),"",$this->Form->input('selection_type', array('label'=> __('Atrinkimas'),'class'=>'form-control', 'type'=>'select','options'=>array(0=>__('Pagal sukūrimo/registravimo laiką'), 1=>__('Pagal perkėlimo/išvežimo laiką'))))); ?>');
<?php }; ?>