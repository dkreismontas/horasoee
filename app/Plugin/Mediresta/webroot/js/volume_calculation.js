jQuery("document").ready(function ($) {
    var calculateVolume = function(){
        let height = parseFloat($('#PalletPalletHeight').val());
        height = Number.isNaN(height)?0:height/1000;
        let factor = parseFloat($('#PalletFillFactor').val());
        factor = Number.isNaN(factor)?0:factor;
        let volume = height * factor * 1.2 * 0.8;
        $('#volume').html(volume.toFixed(3));
        $('#PalletPalletVolume').val(volume);
    };
    $('#PalletFillFactor').bind('change', calculateVolume);
    $('#PalletPalletHeight').bind('keyup', calculateVolume);
    calculateVolume();
});