DROP PROCEDURE IF EXISTS HOOK_AFTER_RECORD_INSERT;
DELIMITER $$

CREATE PROCEDURE `HOOK_AFTER_RECORD_INSERT`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `branch_id` INT(11), IN `quantity` DECIMAL(12,4), IN `shift_id` INT(11), IN `active_fproblem_id` INT(11), IN `active_order_id` INT(11), IN `active_plan_id` INT(11), IN `record_cycle` SMALLINT(6)) NO SQL
BEGIN
	DECLARE `workers_count` TINYINT(3) DEFAULT 0;
	SELECT `s`.`workers_count` INTO `workers_count` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	IF(`workers_count` > 0) THEN
		UPDATE `records` AS `r` SET `r`.`workers_count` = `workers_count` WHERE `r`.`sensor_id` = `sensor_id` AND `r`.`created` = `date_time`;
	END IF;
END$$

DELIMITER ;
