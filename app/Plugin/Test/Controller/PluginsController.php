<?php
App::uses('TestAppController', 'Test.Controller');

class PluginsController extends TestAppController {
    public $uses = array('Shift', 'FoundProblem', 'Problem');
    CONST PROPABILITY = 10; //santykis, su kuriuo 1/propability gamyba virs i problema ir atvirksciai
    
    public function index(){
        $this->layout = 'ajax';
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'Test.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        //$shell->args = array('0.0.0.0:0');
        $shell->save_sensors_data();
        die();
    }

}
