<?php
App::uses('FpiAppController', 'Fpi.Controller');

class PluginsController extends FpiAppController {
    
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        die();
    }

    function save_sensors_data($ipAndPort = ''){
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', Configure::read('companyTitle').'.SensorsRecordsShell');
        $shell = new SensorsRecordsShell();
        if (trim($ipAndPort)) {
            $shell->args = array(str_replace('_', ':', $ipAndPort));
        }
        $shell->save_sensors_data();
        die();
    }
    
    /*function save_sensors_data($ipAndPort = ''){
        $adressList = array(
            'http://88.119.141.28:5580/data',
        );
        $ip = $port = $uniqueString = '';
        if(trim($ipAndPort)){
            $ipAndPort = explode('_', $ipAndPort);
            if(sizeof($ipAndPort) == 2){
                list($ip, $port) = $ipAndPort;
                $uniqueString = str_replace('.','_',$ip).'|'.$port;
            }
        }
        $mailOfErrors = array();
        $dataPushStartMarkerPath = __dir__.'/../webroot/files/data_push_start_'.$uniqueString.'.txt';
        if(file_exists($dataPushStartMarkerPath)){
            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
                $this->Sensor->informAboutProblems();
                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
            }
        }
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
		$this->Sensor->virtualFields = array();
        $db = ConnectionManager::getDataSource('default');
        if(trim($uniqueString)){
            $adressList = array_filter($adressList, function($address)use($ip,$port){
                return preg_match('/'.$ip.':'.$port.'/', $address);
            });
        }
        $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
        foreach($adressList as $address){
            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
            $addessIpAndPort = current($ipAndPort);
            $ch = curl_init($address);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
                continue;
            }
			if(!isset($data->periods)){ continue; }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $title){
                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
                    if(!isset($record->$sensorPinName)) continue;
					if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
					if($sensorPinName == 'i5' && $record->$sensorPinName == 0){//jei i5 pino duomenys lygus 0, tuomet imame duomenis is i6
                        $quantity = $record->i6;
                    }else{
                        $quantity = $record->$sensorPinName;
                    }
                    try{
                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
                        fwrite($fh, $queryLog);
                    }catch(Exception $e){
                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
                    }
                }
            }
        }
        fclose($fh);
        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->Sensor->moveOldRecords();
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->Sensor->informAboutProblems();
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }*/

}