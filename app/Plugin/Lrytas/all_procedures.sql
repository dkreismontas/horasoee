DELIMITER $$

CREATE PROCEDURE `ADD_RECORD_RINSE`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` INT(11), IN `rinse_quantity` INT(11))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
    SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	
	SELECT `s`.`branch_id` INTO `branch_id` FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id` INTO `shift_id` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`, `rinse_quantity`, `start_rinsing`) VALUES (`date_time`, `quantity`, `quantity`, `sensor_id`, NULL, `shift_id`, NULL, NULL, `rinse_quantity`,1);
END$$

DELIMITER ;