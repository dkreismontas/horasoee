<?php
    ob_end_clean();
    error_reporting(0);
    $styleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman'
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
    );
    $headerStyleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman',
            'bold'=>true
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'wrap'=>true,
            'rotation'   => 0,
        ),
    );

//HEADERS
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Darbuotojų ataskaita'));
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Vardas'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pavardė'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagaminta produkcija'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Vidutinis pritaisymo kiekis (vnt.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Vidutinė pasiruošimo trukmė (min.)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Vidutinis tiražo greitis (vnt/h)'))->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $colNr++;
//CONTENT
$colNr = 0; $rowNr = 2;
foreach($data as $rec){$rec = current($rec);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['first_name']);
     $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['last_name']);
     $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['total_quantity']);
     $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['avg_attach_count']);
     $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['avg_transition_time']);
     $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $rec['avg_printing_speed']);
    for($i = 0; $i < $colNr; $i++){
        $activeSheet->getStyleByColumnAndRow($i, $rowNr)->applyFromArray($styleArray);
    }
    $rowNr++; $colNr = 0;
}
// for ($col = 'A'; $col != 'G'; $col++){
    // $objPHPExcel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
// }
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . date('Y-m-d') . '_' . time() . '.xlsx"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
die();

function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}