<?php
    ob_end_clean();
    error_reporting(0);
    $styleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman'
        ),
        'alignment' => array('horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER)
    );
    $headerStyleArray = array(
        'borders' => array('outline' => array(
                'style' => PHPExcel_Style_Border::BORDER_THIN,
                'color' => array('argb' => '000000'),
            ), ),
        'font' => array(
            'size' => 9,
            'name' => 'Times New Roman',
            'bold'=>true
        ),
        'alignment' => array(
            'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
            'wrap'=>true,
            'rotation'   => 0,
        ),
    );

//HEADERS
$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle('Įrenginių darbo ataskaita');
$rowNr = 1;$colNr = 0;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Spaudos pradžia')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Spaudos pabaiga')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Įrenginys')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Leidinys')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(25); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'SM')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Dalis')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Lankas')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Pasiruošimo laikas')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Pritaisymo laikas')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Popieriaus pavadinimas')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Popieriaus GMS')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Popieriaus matmenys/rulono plotis')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Lanko pusė')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Atspaudų kiekis užsakyme')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Pasiruošimo atspaudų kiekis')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Pritaisymo atspaudų kiekis')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Tiražo atspaudai (NETO)')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Tiražo atspaudai (BRUTO)')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Tiražo laikas (NETO)')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, 'Vidutinis kiekis per valandą (Tiražo)')->getStyle(cts($colNr, $rowNr))->applyFromArray($headerStyleArray); $activeSheet->getColumnDimension(nts($colNr))->setWidth(15); $colNr++;
$activeSheet->getRowDimension($rowNr)->setRowHeight(40);
$colNr = 0; $rowNr++;
foreach($approvedOrders as $key => $order){
    $jsonData = json_decode($order['Plan']['serialized_data'],true);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['created']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['end']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Sensor']['name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['production_name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['sm_number']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['order_part']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['signature_number']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($order['Problems'][1])?timeFormating($order['Problems'][1]['duration']):0);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, timeFormating($order['ApprovedOrder']['attachment_time']));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($jsonData['paper'])?$jsonData['paper']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, isset($jsonData['grammage'])?$jsonData['grammage']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, (isset($jsonData['paper_width'])?$jsonData['paper_width']:'').' '.isset($jsonData['paper_height'])?$jsonData['paper_height']:'');
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['signature_side']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['transition_quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['attachment_quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['printing_quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, timeFormating($order['ApprovedOrder']['printing_time']));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['printing_time']>0?bcdiv($order['ApprovedOrder']['printing_quantity'],bcdiv($order['ApprovedOrder']['printing_time'],3600,6),2):0);
    
    for($i = 0; $i < $colNr; $i++){
        $activeSheet->getStyleByColumnAndRow($i, $rowNr)->applyFromArray($styleArray);
    }
    $rowNr++; $colNr = 0;
}
$maxColNr = $colNr;
$rowNr = 3;


// for ($col = 0; $col != $maxColNr; $col++){
    // $activeSheet->getColumnDimension(nts($col))->setAutoSize(true);
// }
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="' . date('Y-m-d') . '_' . time() . '.xlsx"');
$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');

function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
function timeFormating($seconds){
    return sprintf('%02d',(int)($seconds/3600)).':'.sprintf('%02d',(int)($seconds%3600/60)).':'.sprintf('%02d',$seconds%3600%60%60);
}
