<div class="col-xs-6">
    <table class="table table-bordered table-condensed table-striped table-proptable">
    <tbody>
        <tr><td><label><?php echo __('Darbo centras'); ?></label></td><td><?php echo Sensor::buildName($this->request->data, false); ?></td></tr>
        <tr><td><label><?php echo __('Operacijos kodas'); ?></label></td><td><?php echo $this->request->data['Plan']['paste_code']; ?></td></tr>
        <tr><td><label><?php echo __('Produkcijos pavadinimas'); ?></label></td><td><?php echo $this->request->data['Plan']['production_name']; ?></td></tr>
        <tr><td><label><?php echo __('Produkcijos kodas'); ?></label></td><td><?php echo $this->request->data['Plan']['production_code']; ?></td></tr>
        <tr><td><label><?php echo __('Mo numeris'); ?></label></td><td><?php echo $this->request->data['Plan']['mo_number']; ?></td></tr>
        <tr><td><label><?php echo __('Pradžia'); ?></label></td><td><?php echo $this->request->data['Plan']['start']; ?></td></tr>
        <tr><td><label><?php echo __('Pabaiga'); ?></label></td><td><?php echo $this->request->data['Plan']['end']; ?></td></tr>
        <tr><td><label><?php echo __('Greitis vnt/min'); ?></label></td><td><?php echo round($this->request->data['Plan']['step']/60, 2); ?></td></tr>
        <tr><td><label><?php echo __('Kiekis dėžėje'); ?></label></td><td><?php echo $this->request->data['Plan']['box_quantity']; ?></td></tr>
        <tr><td><label><?php echo __('Kiekis'); ?></label></td><td><?php echo $this->request->data['Plan']['quantity']; ?></td></tr>
        <tr><td><label><?php echo __('Pasiruošimo laikas (min.)'); ?></label></td><td><?php echo $this->request->data['Plan']['preparation_time']; ?></td></tr>
        <tr><td><label><?php echo __('Gamybos laikas (min.)'); ?></label></td><td><?php echo $this->request->data['Plan']['production_time']; ?></td></tr>
        <tr><td><label><?php echo __('Numatytas greitis iš kliento (vnt/h)'); ?></label></td><td><?php echo $this->request->data['Plan']['client_speed']; ?></td></tr>
        <tr><td><label><?php echo __('Papildoma informacija'); ?></label></td><td><?php
            $serializedData = json_decode($this->request->data['Plan']['serialized_data'],true);
            if(is_array($serializedData)){
                foreach($serializedData as $key => $sData){
                    echo '<b>'.$key.'</b>: '.$sData.'<br />';
                }
            }
            ?></td></tr>
    </tbody>
</table>
</div>
<?php echo $this->Form->create('Plan'); ?>
<div class="col-xs-6">
    <div class="row">
        <div class="col-xs-12"><label><?php echo __('Gamybos laikas'); ?></label></div>
    </div>
    <div class="row">
        <div class="col-xs-3">
            <?php echo $this->Form->input('start_date', array('label' => __('Nuo'),'class'=>'start_date form-control datepicker')); ?>
        </div>
        <div class="col-xs-3">
            <?php echo $this->Form->input('end_date', array('label' => __('Iki'),'class'=>'end_date form-control datepicker')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php echo $this->Form->input('user_id', array('label' => __('Darbuotojas'),'class'=>'form-control', 'options'=>$users)); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php echo $this->Form->input('all_production', array('label' => __('Visa produkcija su broku (atspaudų vnt.)'),'class'=>'form-control', 'type'=>'text')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php echo $this->Form->input('good_production', array('label' => __('Gera produkcija (atspaudų vnt.)'),'class'=>'form-control', 'type'=>'text')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php echo $this->Form->input('used_paper', array('label' => __('Pritaisymo metu sunaudotas popierius (vnt.)'),'class'=>'form-control', 'type'=>'text')); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-6">
            <?php echo $this->Form->input(__('Išsaugoti'), array('label'=>false,'class'=>'btn btn-primary', 'type'=>'submit')); ?>
        </div>
    </div>
</div>
<?php echo $this->Form->end(); ?>
<script type="text/javascript" charset="utf-8">
    jQuery("document").ready(function ($) {
        jQuery(".datepicker").datetimepicker({
            dateFormat : 'yy-mm-dd',
            timeFormat : 'HH:mm',
            firstDay : 1,
            closeText: 'Uždaryti',
            currentText: 'Dabar',
            timeText: 'Laikas',
            hourText: 'Valandos',
            minuteText: 'Minutės',
            alwaysSetTime: true,
            autoClose: false,
            onSelect: function(dateText,inst){
                //$('#innerForm ').trigger('click');
                $('#innerForm input.time_pattern').val('');
            }
        });
    });
</script>