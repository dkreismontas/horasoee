<?php
	if (!isset($boldLabels)) $boldLabels = false;
	if (!function_exists('echoLabel')) {
		function echoLabel($label, $boldLabels = false) {
			if ($boldLabels) {
				echo '<strong>'.$label.'</strong>';
			} else {
				echo $label;
			}
		}
	}
	if (!function_exists('echoSrz')) {
		function echoSrz($key) {
		    static $serializedData = null;
            if($serializedData == null){ $serializedData = $key; return; }
			return isset($serializedData[$key]) && trim($serializedData[$key])?$serializedData[$key]:'---';
		}
        echoSrz(json_decode($item['Plan']['serialized_data'],true));
	}
?>
<table class="table table-bordered table-condensed">
	<tbody>
		<tr><td><?php echo __('Darbo centras'); ?>: <?php echo $item['Sensor']['name']; ?></td></tr>
		<tr>
			<td>
				<?php echoLabel(__('Planas'), $boldLabels); ?>: <?php echo $item['Plan']['production_name'].' ('.__('<b>SM nr</b>: %s; <b>Leidinio dalis</b>: %s; <b>Lanko numeris</b>: %s; <b>Lanko pusė</b>: %s', $item['Plan']['sm_number'],$item['Plan']['order_part'],$item['Plan']['signature_number'], $item['Plan']['signature_side']).')'; ?><br />
				<?php echoLabel(__('Popierius'), $boldLabels); ?>: <?php echo __('%s (<b>popieriaus matmenys:</b> %s x %s mm, <b>gramatūra:</b> %s; <b>norma,kg:</b> %s; <b>norma,vnt:</b> %s; <b>spalvingumas:</b> %s; <b>popieriaus kodas:</b> %s)', echoSrz('paper'),echoSrz('paper_width'),echoSrz('paper_height'), echoSrz('grammage'), echoSrz('paper_norm_kg'), echoSrz('paper_norm_sheets'), echoSrz('colourfulness'), echoSrz('paper_code')); ?> <br />
			</td>
		</tr>
		<tr>
			<td>
				<?php echoLabel(__('Faktinė gamyba'), $boldLabels); ?>:<br />
				<?php echoLabel(__('Data ir laikas'), $boldLabels); ?>: <?php
					$ds = new DateTime($item[ApprovedOrder::NAME]['created']);
					$de = new DateTime($item[ApprovedOrder::NAME]['end']);
					echo $ds->format('Y-m-d H:i').' &mdash; '.$de->format('Y-m-d H:i');
				?><br />
				<?php echoLabel(__('Trukmė'), $boldLabels); ?>: <?php echo number_format(($de->getTimestamp() - $ds->getTimestamp()) / 60, 1, ',', ''); ?> <?php echo __('min.'); ?><br />
				<?php echoLabel(__('Pasiruošimo laikas'), $boldLabels); ?>:  <?php echo __('%s min.', $transitionDuration); ?><br />
				<?php echoLabel(__('Pritaisymo laikas'), $boldLabels); ?>:  <?php echo __('%s min.', number_format($item[ApprovedOrder::NAME]['attachment_time'] / 60,2,'.','')); ?><br />
				<?php echoLabel(__('Tiražo laikas'), $boldLabels); ?>:  <?php echo __('%s min.', number_format($item[ApprovedOrder::NAME]['printing_time'] / 60,2,'.','')); ?><br />
			</td>
		</tr>
		<tr>
		    <td> 
		        <?php echoLabel(__('Vidutinis pritaisymo greitis'), $boldLabels); ?>:  <?php echo __('%s vnt./val.', $item[ApprovedOrder::NAME]['attachment_time'] > 0?number_format(round($item[ApprovedOrder::NAME]['attachment_quantity']/($item[ApprovedOrder::NAME]['attachment_time']/3600),2), 2,'.',''):0); ?><br />
		        <?php echoLabel(__('Vidutinis tiražo greitis'), $boldLabels); ?>:  <?php echo __('%s vnt./val.', $item[ApprovedOrder::NAME]['printing_time'] > 0?number_format(round($item[ApprovedOrder::NAME]['printing_quantity']/($item[ApprovedOrder::NAME]['printing_time']/3600),2), 2,'.',''):0); ?><br />
		        <?php echoLabel(__('Vidutiniai tiražo greičiai pagal operatorius'), $boldLabels); ?>:
		        <ul style="list-style-type: none;">
		        <?php 
		        foreach($usersQuantities as $usersQuantity){
		            $userId = $usersQuantity[0]['user_id'];
		            echo '<li style="padding-left: 30px;">'.(isset($users[$userId])?$users[$userId]:__('Pašalintas operatorius')).': '.__('%s vnt./val.',($usersQuantity[0]['printing_time'] > 0?number_format(round($usersQuantity[0]['printing_quantity']/($usersQuantity[0]['printing_time']/3600),2), 2,'.',''):0)).'</li>';
		        }
		        ?>
		        </ul>
		    </td>
		</tr>
		<tr>
		    <td>
		        <?php echoLabel(__('Užsakymo kiekiai (atspaudais)'), $boldLabels); ?><br />
		        <?php echoLabel(__('Pasiruošimo kiekis'), $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['transition_quantity']; ?><br />
		        <?php echoLabel(__('Pritaisymo kiekis'), $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['attachment_quantity']; ?><br />
		        <?php echoLabel(__('Tiražo kiekis'), $boldLabels); ?>: <?php echo $item[ApprovedOrder::NAME]['printing_quantity']; ?><br />
		        <?php 
		          $plovimasLosses = array_filter($lossItems, function($data){ return $data['Loss']['loss_type_id']==-1; }); 
		          $plovimasLossesCount = Set::extract('/Loss/value',$plovimasLosses); 
		          echoLabel(__('Bendras kiekis'), $boldLabels); echo ': '.($item[ApprovedOrder::NAME]['box_quantity']+array_sum($plovimasLosses));
		        ?>
		    </td>
		</tr>
		<tr>
		    <td>
		        <?php echoLabel(__('Užsakymo kiekiai (Deklaruoti kiekiai ir nuostoliai)'), $boldLabels); ?><br />
		        <?php echoLabel(__('Bendras deklaruotas kiekis'), $boldLabels); ?>: <?php echo array_sum(Set::extract('/PartialQuantity/quantity',$partialQuantities)); ?><br />
		        <?php echoLabel(__('Nuostoliai'), $boldLabels); ?><br />
		        <?php 
		          $lossCountsByType = array();
                  foreach($lossItems as $lossItem){
                      $type = $lossItem['LossType']['name'];
                      if(!isset($lossCountsByType[$type])){ $lossCountsByType[$type] = array('value'=>$lossItem['Loss']['value'], 'unit'=>$lossItem['LossType']['unit']); }
                      else{ $lossCountsByType[$type]['value'] += $lossItem['Loss']['value']; }
                  }
                  foreach($lossCountsByType as $lossTitle => $loss){
                      echo '<div style="padding-left: 30px">'; echoLabel($lossTitle, $boldLabels); echo ': '.$loss['value'].' '.$loss['unit'].'</div>';
                  }
		        ?>
		    </td>
		</tr>
		<tr>
		    <td>
		        <?php if (isset($lossItems) && !empty($lossItems)): ?>
                <?php echoLabel(__('Nuostolių deklaravimo istorija'), $boldLabels); ?>:<br />
                <?php 
                $prevType = null;
                foreach ($lossItems as $li){
                    if(!trim($li[Loss::NAME]['value']) || $li[Loss::NAME]['value'] == 0) continue;
                    if($prevType != null && $prevType != $li[LossType::NAME]['id']){ echo '<hr style="margin: 10px 0; border: 0;" />'; }
                    ?><b><?php echo trim($li['User']['first_name'].' '.$li['User']['last_name']).' '.$li['Loss']['created_at']; ?></b> | <?php echo $li[LossType::NAME]['name']; ?>: <?php echo $li[Loss::NAME]['value'].' '.$li[LossType::NAME]['unit']; ?><br /><?php
                    $prevType = $li[LossType::NAME]['id'];
                } ?>
                <?php endif; ?>
		    </td>
		</tr>
		<tr>
		    <td>
		        <?php if (isset($partialQuantities) && !empty($partialQuantities)): ?>
                <?php echoLabel(__('Kiekių deklaravimo istorija'), $boldLabels); ?>:<br />
                <?php 
                foreach ($partialQuantities as $li){
                    if(!trim($li[PartialQuantity::NAME]['quantity']) || $li[PartialQuantity::NAME]['quantity'] == 0) continue;
                    ?><b><?php echo trim($li['User']['first_name'].' '.$li['User']['last_name']).' '.$li[PartialQuantity::NAME]['created']; ?></b> : <?php echo $li[PartialQuantity::NAME]['quantity']; ?><br /><?php
                } ?>
                <?php endif; ?>
		    </td>
		</tr>
	</tbody>
</table>
