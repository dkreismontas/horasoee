<?php
echo $this->Form->create('MoStat');
?>
    <ul>
        <?php foreach ($mo_list as $date => $day)
        { ?>
            <li>

                <span class="mo-day"><label><?php echo $this->Form->checkbox('date', array('class' => 'date-toggle')) ?> <?php echo __('Data').': '.$date; ?></label></span>
                <ul>
                    <?php foreach($day as $line_id=>$line) { ?>
                        <span class="mo-line"><label><?php echo $this->Form->checkbox('line', array('class' => 'line-toggle')) ?> <?php echo $line_id; ?></label></span>


                        <ul style="-webkit-column-count: 4; -moz-column-count: 4; column-count:4;">
                            <?php foreach ($line as $mo){
                                $additionalData = json_decode($mo['additional_data'],true); 
                                $nr = isset($additionalData['add_order_number'])?'(nr: '.$additionalData['add_order_number'].')':'';
                            ?>
                                <li><label class="default"><?php echo $this->Form->checkbox('mo.', array('value' => $mo['mo_number'])) ?> <?php echo __('Užsakymas').': '.$mo['sm_number']; ?> - <?php echo $mo['production_name']. ' ('.$mo['order_part'].', '.$mo['signature_number'].', '.$mo['signature_side'].')'; ?></label> <?php echo $nr; ?></li>
                            <?php } ?>
                        </ul>

                    <?php }?>
                </ul>

            </li>
        <?php } ?>
    </ul>
<?php
echo $this->Form->submit(__('Ieškoti'), array('class' => 'btn btn-primary'));
echo $this->Form->end();
?>