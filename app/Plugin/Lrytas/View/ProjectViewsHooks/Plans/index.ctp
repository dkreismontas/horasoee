<style>
    .marked td{
        background-color: #0088CC !important;
    }
    
</style>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas planas'); ?></button>
<br /><br /> */ ?>
<?php if($forced_sensorId == null) {?>
<div class="clearfix">
    <a style="display:none;" href="/plans/create" class="btn btn-primary">Sukurti planą</a>
	<div class="btn-group pull-left">
        <h3><i class="iconsweets-trashcan2"></i> <?php echo __('Paieška'); ?></h3>
        <?php
        echo $this->Form->create('ApprovedOrder', array('class'=>'row-fluid', 'type'=>'get'));
        echo $this->Form->input('order_number', array('type'=>'text', 'label'=>__('Įveskite užsakymo nr.'),  'class'=>'form-control', 'div'=>'pull-left col-xs-5 nopadding', 'value'=>isset($this->request->query['order_number'])?$this->request->query['order_number']:''));
        echo $this->Form->input('product_code', array('type'=>'text', 'label'=>__('Gaminio kodas'),  'class'=>'form-control', 'div'=>'pull-left col-xs-4', 'value'=>isset($this->request->query['product_code'])?$this->request->query['product_code']:''));
        echo $this->Form->input(__('Ieškoti'), array('type'=>'submit', 'label'=>'&nbsp;',  'class'=>'form-control btn btn-primary', 'div'=>'pull-left col-xs-3 nopadding'));
        echo $this->Form->end();
        ?>
    </div>
	<div class="btn-group pull-right">
		<button class="btn btn-grey" id="reloadPage"><?php echo __('Perkrauti puslapį') ?></button>
		<button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Darbo centras') ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
		<ul class="dropdown-menu">
			<?php foreach ($sensorOptions as $lid => $li): ?>
			<li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
			<?php endforeach; ?>			
		</ul>
	</div>
</div>
<br/>
<?php } ?>
<table class="table table-bordered table-striped" id="plansTable">
	<thead id="order_0">
		<tr>
			<th><?php echo __('Veiksmai'); ?></th>
			<th><?php echo __('ID'); ?></th>
			<th><?php echo __('Darbo centras'); ?></th>
			<th><?php echo __('SM nr.'); ?></th>
			<th><?php echo __('Operacija'); ?></th>
			<th><?php echo __('Gaminys'); ?></th>
			<!--th><?php echo __('Gaminio kodas'); ?></th-->
			<th><?php echo __('Leidinio dalis'); ?></th>
			<th><?php echo __('Lankas'); ?></th>
			<th><?php echo __('Lanko pusė'); ?></th>
			<th><?php echo __('Popieriaus matmenys, mm'); ?></th>
			<th><?php echo __('Popieriaus kodas'); ?></th>
			<th><?php echo __('Atspaudų kiekis, vnt'); ?></th>
			<th><?php echo __('Tiražas, vnt'); ?></th>
			<th><?php echo __('Kiekis atspaude'); ?></th>
			<th><?php echo __('Pradžia'); ?></th>
			<th><?php echo __('Pabaiga'); ?></th>
			<th><?php echo __('Būsena'); ?></th>
			<?php if(isset($arr['plan_exist'])){ ?><th><?php echo __('Planas'); ?></th><?php } ?>
			<th>&nbsp;</th>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="16"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model];
            $serializedData = json_decode($li->serialized_data,true);
			$liSensor = (isset($li_[Sensor::NAME]) ? ((object) $li_[Sensor::NAME]) : null);
			$liLine = (isset($li_[Line::NAME]) ? ((object) $li_[Line::NAME]) : null); ?>
		<tr class="<?php 
			$state = isset($li_[ApprovedOrder::NAME]['status_id']) ? intval($li_[ApprovedOrder::NAME]['status_id']) : 0;
			if ($state == ApprovedOrder::STATE_COMPLETE) echo 'success';
			else if ($state == ApprovedOrder::STATE_CANCELED) echo 'danger';
			else if ($state == ApprovedOrder::STATE_POSTPHONED) echo 'warning';
			else if ($state == ApprovedOrder::STATE_IN_PROGRESS) echo 'info';
			else if ($li_[Plan::NAME]['has_problem']) echo 'problem';
			?>" id="order_<?php echo $li->id; ?>">
			<td><?php echo '
			    <i order_id="' . $li->id . '" class="' . ($li->position_frozen ? 'iconfa-star' : 'iconfa-star-empty') . ' freezePosition" title="' . __('Užšaldyti / atšildyti užsakymo pozicijos keitimą') . '"></i>&nbsp;&nbsp;
			';?>
			<input type="checkbox" class="mover" title="<?php echo __('Apjungti užsakymus rikiavimui'); ?>" />
			</td>
			<td><?php echo $li->id; ?></td>
			<td><?php echo Sensor::buildName($li_, false); ?></td>
			<!--td><?php //echo Line::buildName($li_, false); ?></td-->
			<td><?php echo $li->sm_number; ?></td>
			<td><?php echo isset($serializedData['operation'])?$serializedData['operation']:''; ?></td>
			<td><?php echo $li->production_name; ?></td>
			<!--td><?php //echo $li->production_code; ?></td-->
			<td><?php echo $li->order_part; ?></td>
			<td><?php echo $li->signature_number; ?></td>
			<td><?php echo $li->signature_side; ?></td>
			<td><?php echo isset($serializedData['paper_width'])?$serializedData['paper_width']:''; ?> x <?php echo isset($serializedData['paper_height'])?$serializedData['paper_height']:''; ?></td>
			<td><?php echo isset($serializedData['paper_code'])?$serializedData['paper_code']:''; ?></td>
			<td><?php echo $li->quantity; ?></td>
			<td><?php echo isset($serializedData['quantity'])?$serializedData['quantity']:__('Kiekis neatsiųstas'); ?></td>
			<td><?php echo $li->box_quantity; ?></td>
			<td><?php $sdt = new DateTime($li->start); echo $sdt->format('Y-m-d H:i'); ?></td>
			<td><?php $edt = new DateTime($li->end); echo $edt->format('Y-m-d H:i'); ?></td>
            <td><?php echo (isset($li_[ApprovedOrder::NAME]['id'])) ? ((array_key_exists($li_[ApprovedOrder::NAME]['status_id'],$statuses)) ? __($statuses[$li_[ApprovedOrder::NAME]['status_id']]) : __('Nežinoma')) : __('Nepradėta') ?></td>
            <?php if(isset($arr['plan_exist'])){ ?>
                <td><?php echo $li->plan_start.' - '.$li->plan_end; ?></td>
            <?php } ?>
            <td>
				<?php if (isset($li_[ApprovedOrder::NAME]['status_id']) && $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_IN_PROGRESS): ?>
				    <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a><br />
                <?php elseif($li_[Plan::NAME]['control'] == 'T'):?>
                    <?php if( $li_[ApprovedOrder::NAME]['status_id'] != 2 ) : ?>
                    <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Patvirtinti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Patvirtinti') ?></a>
                    <a href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
				    <?php endif; ?>
                <?php elseif (!isset($li_[ApprovedOrder::NAME]['id']) || $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_CANCELED || $li_[ApprovedOrder::NAME]['status_id'] == ApprovedOrder::STATE_POSTPHONED): ?>
				    <a href="<?php printf($removeUrl, $li->id); ?>" onclick="return confirm('<?php echo htmlspecialchars($removeMessage); ?>');" title="<?php echo htmlspecialchars(__('Pašalinti')); ?>"><span class="glyphicon glyphicon-trash"></span>&nbsp;<?php echo __('Pašalinti') ?></a>
				<?php else: ?>
				&nbsp;
				<?php endif; ?>
				<?php
					echo $this->Html->link('&nbsp;<span class="glyphicon glyphicon-edit"></span>&nbsp;'.__('Keisti duomenis').'<br />', array('action'=>'change_data', $li->id),array('escape'=>false));	
					echo $this->Html->link('&nbsp;<span class="glyphicon glyphicon-edit"></span>&nbsp;'.__('Užbaigti'), array('plugin'=>'lrytas','controller'=>'plugins','action'=>'manual_finish_order', $li->id)+$this->request->params['named'],array('escape'=>false));	
				?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
<script type="text/javascript">
    jQuery(document).ready(function($){
        var fixHelper = function(e, ui) { 
            ui.children().each(function() {
                $(this).width($(this).width());
            });
            return ui;
        };
        var groupRows = function(){
            var tmp = [];
            var reorganisations = [];
            var currentStatus,prevStatus = null;
            checkStatus = 0;
            $('#plansTable tr').not('thead tr').each(function(){
                $(this).removeAttr('combined');
                currentStatus = $(this).find('td:first-child input.mover').is(':checked')?true:false;
                if(currentStatus != prevStatus){
                    checkStatus++;
                }
                $(this).attr('combined',checkStatus);
                prevStatus = currentStatus || prevStatus;
            });
            $('#plansTable tbody tr').unwrap();
            for(var i = 1; i <= checkStatus; i++){
                var idsList = '';
                $('#plansTable tr[combined="'+i+'"]').each(function(){
                    idsList += $(this).attr('id').replace('order_','')+',';
                });
                $('#plansTable tr[combined="'+i+'"]').wrapAll('<tbody id="order_'+idsList+'" />');
            }
            $('#plansTable').sortable({
                helper: fixHelper,
                //containment: "parent",
                items: "tbody",
                update: function(event, ui) {
                    $.post("<?php echo $this->Html->url('/lrytas/plugins/change_order_position/',true); ?>", { order: $('#plansTable').sortable('serialize') } );
                },
            });
        }
        
        var fillEmptyChecks = function(thisOne){
            var startAutoMarking = false;
            if(thisOne.is(':checked')){
                var firstChecked = $('#plansTable tr td:first-child input.mover:checked').first().closest('tr').attr('id');
                var setStatus = true;
            }else{
                var firstChecked = thisOne.closest('tr').attr('id');
                var setStatus = false;
            }
            var lastChecked = $('#plansTable tr td:first-child input.mover:checked').last().closest('tr').attr('id');
            $('#plansTable tr').removeClass('marked');
            if(firstChecked != lastChecked){
                $('#plansTable tr').each(function(){
                    if($(this).attr('id') == firstChecked){ startAutoMarking = true; }
                    if(startAutoMarking){
                        $(this).find('input.mover').prop('checked', setStatus);
                    }
                    currentCheckStatus = $(this).find('td:first-child input.mover').is(':checked')?true:false;
                    if(currentCheckStatus){
                        $(this).addClass('marked');
                    }
                    if($(this).attr('id') == lastChecked){ return false;}
                });
            }
        }
        
        
        <?php if($sensorId > 0){ ?>
        $('#plansTable').on('click', '.mover', function(){
            fillEmptyChecks($(this));
            groupRows();
        });
        $('#plansTable input.mover').prop('checked',false);
        groupRows();
        //uzsaldyti / atsildyti uzsakymo pozicijos keitima
        $('.freezePosition').bind('click',function(){
            var frozenStatus = $(this).hasClass('iconfa-star')?'0':'1';
            var thisOne = $(this);
            $.ajax({
                url: "<?php echo $this->Html->url('/'.$this->params['controller'].'/freeze_order/',true); ?>",
                type: 'post',
                data: {order_id: $(this).attr('order_id'), frozenStatus: frozenStatus},
                complete: function(){
                    var currentClickedId = thisOne.parents('tr').attr('id');
                    <?php if(isset($frozing_orders_type) && $frozing_orders_type==1){ ?>
                        thisOne.toggleClass('iconfa-star-empty iconfa-star');
                        return false;
                    <?php } ?>
                    if(frozenStatus == '1'){
                        thisOne.parents('tbody').children('tr').each(function(){
                            if($(this).attr('id') == currentClickedId){
                                $(this).find('.freezePosition').addClass('iconfa-star').removeClass('iconfa-star-empty');
                                return false;
                            }
                            $(this).find('.freezePosition').addClass('iconfa-star').removeClass('iconfa-star-empty');
                        });
                    }else{
                        var letUncheck = false;
                        thisOne.parents('tbody').children('tr').each(function(){
                            if($(this).attr('id') == currentClickedId){
                                letUncheck = true;
                            }
                            if(letUncheck){
                                $(this).find('.freezePosition').removeClass('iconfa-star').addClass('iconfa-star-empty');
                            }
                        });
                    }

                }
            });
        });
        $('#reloadPage').bind('click', function(){
            location.reload();
        });
        <?php } ?>
    })
</script>
