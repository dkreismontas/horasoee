<style type="text/css">
    table th.asc{
        background: #2AABD2 !important;
    }
    table th.desc{
        background: #6C6CFF !important;
    }
</style>
<?php if (($msg = $this->Session->flash('saveMessage'))): ?>
<div class="alert alert-error"><?php echo $msg; ?></div>
<?php endif; ?>
<?php /* <button class="btn btn-primary" type="button" onclick="location.href='<?php echo $newUrl; ?>';"><?php echo __('Naujas patvirtintas užsąkymas'); ?></button>
<br /><br /> */ ?>

<?php if($forced_sensorId == null) {?>
<div class="clearfix">
    <div class="btn-group pull-left">
        <h3><i class="iconsweets-trashcan2"></i> <?php echo __('Paieška'); ?></h3>
        <?php
        echo $this->Form->create('ApprovedOrder', array('class'=>'row-fluid', 'type'=>'get'));
        echo $this->Form->input('order_number', array('type'=>'text', 'label'=>__('Įveskite užsakymo nr.'),  'class'=>'form-control', 'div'=>'pull-left col-xs-5 nopadding', 'value'=>isset($this->request->query['order_number'])?$this->request->query['order_number']:''));
        echo $this->Form->input('product_code', array('type'=>'text', 'label'=>__('Gaminio kodas'),  'class'=>'form-control', 'div'=>'pull-left col-xs-4', 'value'=>isset($this->request->query['product_code'])?$this->request->query['product_code']:''));
        echo $this->Form->input(__('Ieškoti'), array('type'=>'submit', 'label'=>'&nbsp;',  'class'=>'form-control btn btn-primary', 'div'=>'pull-left col-xs-3 nopadding'));
        echo $this->Form->end();
        ?>
    </div>
	<div class="btn-group pull-right">
		<button data-toggle="dropdown" class="btn btn-default dropdown-toggle"><strong><?php echo __('Darbo centras') ?>:</strong>&nbsp;<?php echo $sensorId ? $sensorOptions[$sensorId] : __('Nepriskirta') ; ?>&nbsp;<span class="caret"></span></button>
		<ul class="dropdown-menu">
			<?php foreach ($sensorOptions as $lid => $li): ?>
			<li><a href="<?php echo htmlspecialchars(str_replace('__DATA__', $lid, $filterUrl)); ?>"><?php echo $li; ?></a></li>
			<?php endforeach; ?>			
		</ul>
	</div>
</div>
<br/>
<?php }
$tableHeaders = array(
    'ApprovedOrder.id'=>array('title'=>__('ID'), 'sort'=>true),
    'ApprovedOrder.created'=>array('title'=>__('Data ir laikas'),'sort'=>true),
    'Sensor.name'=>array('title'=>__('Darbo centrai'),'sort'=>true),
    'Plan.sm_number'=>array('title'=>__('SM'),'sort'=>true),
    array('title'=>__('Operacija'),'sort'=>false),
    'Plan.production_name'=>array('title'=>__('Leidinio pavadinimas'),'sort'=>true),
    //'Plan.mo_number'=>array('title'=>__('Planas'),'sort'=>true),
    'Plan.order_part'=>array('title'=>__('Leidinio dalis'),'sort'=>true),
    'Plan.signature_number'=>array('title'=>__('Lankas'),'sort'=>true),
    'Plan.signature_side'=>array('title'=>__('Lanko pusė'),'sort'=>true),
    'Plan.quantity'=>array('title'=>__('Atspaudų kiekis pagal tiražą'),'sort'=>true),
    array('title'=>__('Tiražo kiekis pagal planą'),'sort'=>false),
    'ApprovedOrder.attachment_quantity'=>array('title'=>__('Pritaisymo kiekis (atspaudai, faktas)'),'sort'=>true),
    'ApprovedOrder.printing_quantity'=>array('title'=>__('Tiražas (atspaudai, faktas)'),'sort'=>true),
    'ApprovedOrder.printing_unit_quantity'=>array('title'=>__('Tiražas (faktas)'),'sort'=>true),
    array('title'=>__('Bendras kiekis'),'sort'=>false),
    'ApprovedOrder.confirmed_quantity'=>array('title'=>__('Patvirtintas kiekis'),'sort'=>true),
    //array('title'=>__('Patvirtintas'),'sort'=>false),
    array('title'=>__('Būsena'),'sort'=>false),
    array('title'=>'','sort'=>false),
);
?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
		    <?php
            $i = 1;
		    foreach($tableHeaders as $key => $tableHeader){
		        //echo '<th>'.$this->Html->link($tableHeader['title'], array('sort'=>Inflector::variable($tableHeader['title']))).'</th>';
		        $sortType = 'asc';
                $urlParams = $this->params['url'];
                $class = '';
		        if(isset($this->params['url']['sort'.$i])){
		            $sortType = current(array_reverse(explode('|',$this->params['url']['sort'.$i])));
                    $sortType = current(array_diff(array('asc','desc'),array($sortType)));
                    //unset($urlParams['sort'.$i]);
                    $class = current(array_diff(array('asc','desc'),array($sortType)));;
		        }
                if($tableHeader['sort']){
                    $namedParams = array();
                    //isset($this->request->named['sensorId']){ $namedParams['sensorId']= }
                    echo '<th class="'.$class.'">'.$this->Html->link($tableHeader['title'], array_merge($this->request->named,array('?'=>array_merge($urlParams,array('sort'.$i => $key.'|'.$sortType))))).'</th>';
                }else{
                    echo '<th>'.$tableHeader['title'].'</th>';
                }
                $i++;
		    }
		    ?>
		</tr>
	</thead>
	<tbody>
		<?php if (empty($list)): ?>
		<tr><td colspan="9"><?php echo __('Sąrašas yra tuščias.'); ?></td></tr>
		<?php endif; ?>
		<?php foreach ($list as $li_): $li = (object) $li_[$model]; $serializedData = json_decode($li_['Plan']['serialized_data'],true);  ?>
		<tr>
			<td><?php echo $li->id; ?></td>
			<td><?php $ds = new DateTime($li->created); echo $ds->format('Y-m-d H:i'); ?></td>
			<td><?php echo Sensor::buildName($li_); ?></td>
			<td><?php echo $li_['Plan']['sm_number']; ?></td>
			<td><?php echo isset($serializedData['operation'])?$serializedData['operation']:''; ?></td>
			<td><?php echo $li_['Plan']['production_name']; ?></td>
			<!--td><?php //echo Plan::buildName($li_); ?></td-->
			<td><?php echo $li_['Plan']['order_part']; ?></td>
			<td><?php echo $li_['Plan']['signature_number']; ?></td>
			<td><?php echo $li_['Plan']['signature_side']; ?></td>
			<td><?php echo $li_['Plan']['quantity']; ?></td>
			<td><?php echo isset($serializedData['quantity'])?$serializedData['quantity']:''; ?></td>
			<td><?php echo $li->attachment_quantity; ?></td>
			<td><?php echo $li->printing_quantity; ?></td>
			<td><?php echo $li->printing_unit_quantity; ?></td>
			<td><?php echo $li->box_quantity+$li_[0]['loss_sum']; ?></td>
			<td><?php echo $li->confirmed_quantity; ?></td>
			
			<!--td><?php //echo ($li->status_id == ApprovedOrder::STATE_COMPLETE || $li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE) ? ($li->confirmed ? __('Taip') : __('Ne')) : '&mdash;'; ?></td-->
			<td><?php echo __(Status::buildName($li_)); ?></td>
			<td>
				<?php if ($li->status_id == ApprovedOrder::STATE_COMPLETE): ?>
    				<a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                    <?php echo $allowToRestore?$this->Html->link('<span class="glyphicon glyphicon-arrow-up">'.__('Atkurti').'</span>', array('action'=>'return_order', $li->id), array('escape'=>false)):''; ?>
    				<?php endif; ?>
    				<?php if ($li->status_id == ApprovedOrder::STATE_POSTPHONED || $li->status_id == ApprovedOrder::STATE_PART_COMPLETE): ?>
                        <?php if(!$li->confirmed): ?>
                            <a href="<?php printf($editUrl, $li->id); ?>" title="<?php echo htmlspecialchars(__('Redaguoti')); ?>"><span class="glyphicon glyphicon-pencil"></span>&nbsp;<?php echo __('Redaguoti') ?></a>
                        <?php endif; ?>
                        &nbsp;
                        <?php if($li->status_id != ApprovedOrder::STATE_PART_COMPLETE) : ?>
                            <a href="<?php printf($finishUrl, $li->id); ?>"><span class="glyphicon glyphicon-asterisk"></span>&nbsp;<?php echo __('Pabaigti'); ?></a>
                    <?php endif; ?>
    		    <?php elseif($li->status_id == ApprovedOrder::STATE_IN_PROGRESS): ?>
    				    <?php echo $li->status_id==1?$this->Html->link('<span class="glyphicon glyphicon-arrow-up">'.__('Peržiūrėti').'</span>', array('action'=>'edit', $li->id), array('escape'=>false)):''; ?>
				<?php endif; ?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>
<div><?php echo $this->App->properPaging(true, array('sensorId' => $sensorId)); ?></div>
