<?php echo $this->Form->create($model, array('url' => $formUrl, 'autocomplete' => 'off')); ?>
<?php echo $this->Form->input('id', array('div' => null, 'label' => null, 'type' => 'hidden')); ?>
<?php echo $this->element('Lrytas.ApprovedOrderTable', array('relatedPostponedOrders'=>$rpo,'item' => $item, 'lossItems' => $lossItems, 'boldLabels' => true)); ?>
<?php 
$orderConfirmed = false;
if ($item[ApprovedOrder::NAME]['confirmed'] || ($item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_COMPLETE && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_POSTPHONED && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_PART_COMPLETE)): ?>
    <?php 
    $orderConfirmed = true;
    if(isset($item[ApprovedOrder::NAME]['modified']) && $item[ApprovedOrder::NAME]['modified'] != $item[ApprovedOrder::NAME]['created'] && (int)$item[ApprovedOrder::NAME]['modified'] != 0){
    	echo __('Patvirtinimas atliktas').':'.$item[ApprovedOrder::NAME]['modified'];
    } ?>
    <div class="form-group"> 
        <?php
        if($item[ApprovedOrder::NAME]['confirmed']) {
            echo __("<b>Kiekis (patvirtintas):</b>") . $item[ApprovedOrder::NAME]['confirmed_quantity'];
        }
        ?>
    </div>
<?php endif; ?>

<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
	<div class="col-xs-12">
		<?php 
		if(!$orderConfirmed){
		    echo $this->Form->input('confirmed_item_quantity', array('label' => __('Tiražo kiekis tvirtinimui'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'value'=>($item[ApprovedOrder::NAME]['tirazas_count'])));
	    }elseif($item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_IN_PROGRESS){
	        echo $this->Form->input('confirmed_item_quantity', array('label' => __('Redaguoti patvirtintą kiekį'), 'type' => 'text', 'class' => 'form-control', 'div' => array('class' => 'form-group'), 'value'=>($item[ApprovedOrder::NAME]['confirmed_quantity'])));
	    }
        ?>
	</div>
</div>

<?php if(!$orderConfirmed): ?>
<div class="row-fluid" style="margin: 0px -15px 0px -15px; <?php if(!$edit_approved_order_quantities) echo "display:none;"; ?>">
    <div class="col-md-4 col-xs-12">
        <?php foreach($lossTypes as $lt) {
            ?>
            <div class="row">
            <label class="control-label col-xs-6"><?php echo $lt['LossType']['name'] ;?> (<?php echo $lt['LossType']['code'] ;?>)</label>
            <div class="col-xs-6">
                <div class="input-group">
                    <input class="form-control input-default" type="text" name="lt_<?php echo $lt['LossType']['id'] ;?>" value=""/>
                    <span class="input-group-addon"><?php echo $lt['LossType']['unit'] ;?></span>
                </div>
            </div>
            </div>
        <?php } ?>
    </div>
</div>
<div style="clear:both;"></div>
<?php endif; ?>
<div>
	<br />
	<?php if ($canSave && $item[ApprovedOrder::NAME]['status_id'] != ApprovedOrder::STATE_IN_PROGRESS): ?>
	<button type="submit" class="btn btn-primary"><?php echo __('Išsaugoti'); ?></button>
	<?php endif; ?>
	<!--<a href="<?php echo $listUrl; ?>" class="btn btn-link"><?php echo __('Atšaukti'); ?></a> -->
</div>
<?php echo $this->Form->end();