<?php
App::uses('LrytasAppController', 'Lrytas.Controller');
App::uses('ApprovedOrder', 'Model');

class PluginsController extends LrytasAppController {
	
    public $uses = array('Shift','Record','Sensor','ApprovedOrder','LrytasImportPlansFromXML.XmlImport','Plan','User','Loss','FoundProblem');
    
    public function index(){
        $this->layout = 'ajax';
    }
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        //vykdome iraso iterpima i DB
        //$saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        // $context = stream_context_create(array (
            // 'http' => array (
                // 'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            // )
        // ));
        $this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
        $saveSensors = $this->Sensor->find('list', array('fields'=>array('id', 'name'), 'conditions'=>array('pin_name <>'=>'')));
        //$saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://193.41.197.129:5580/data',
            '5680'=>'http://193.41.197.129:5680/data',
            '5780'=>'http://193.41.197.129:5780/data',
        );
        $this->ApprovedOrder->virtualFields = array('last_id'=>'MAX(ApprovedOrder.id)');
        $lastApproovedOrdersIds = $this->ApprovedOrder->find('list', array('fields'=>array('last_id'),'conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS),'group'=>array('ApprovedOrder.sensor_id')));
        $this->ApprovedOrder->virtualFields = array();
        $lastApproovedOrdersData = $this->ApprovedOrder->find('list', array(
            'fields'=>array('ApprovedOrder.sensor_id', 'ApprovedOrder.additional_data'),
            'conditions'=>array('ApprovedOrder.id'=>$lastApproovedOrdersIds)
        ));
        array_walk($lastApproovedOrdersData, function(&$data){ $data = json_decode($data,true); });
        $mailOfErrors = array();
        foreach($adressList as $addressPort => $adress){
            //$data = json_decode(file_get_contents($adress, false, $context));
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $title){
                    list($sensorName, $sensorPort) = explode('_',$title);
                    if($addressPort != $sensorPort) continue;
                    if(!isset($record->$sensorName)) continue;
                    $quantity = $record->$sensorName;
                    if($sensorPort == '5680' && $sensorName == 'i1'){
                        $quantity += $record->i5;
                    }
                    if(isset($lastApproovedOrdersData[$sensorId]['start_rinsing'])){
                        try{//kai esamas vykdomas uzsakymas turi statusa plovimas, kiekiai perkeliami i rinse_quantity laukeli
                            $query = 'CALL ADD_RECORD_RINSE(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', 0, '.$quantity.'); ';  
                            $db->query($query);
                        }catch(Exception $e){
                            $this->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                        }    
                    }else{
                        try{
                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                            $db->query($query);
                        }catch(Exception $e){
                            $this->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                        }
                    }
                }
            }
        }
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
             $this->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        $this->start_cron_upload();//keliam eplanus
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
        //if($_SERVER['REMOTE_ADDR'] == '78.57.255.40'){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        $this->moveOldRecords();
        session_destroy();
        die();
    }

    private function moveOldRecords(){
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = '2 WEEK';
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
    }
    
    private function checkRecordsIsPushing($message,$type=1){
        pr($message);
        $markerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!file_exists($markerPath)){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, time()+300); //leisime siusti pirma maila uz 5min jei vis dar neis irasai
            fclose($fh);
            return false;
        }
        $settings = $this->Settings->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList))return;
        $minutes = explode(':', $settings);
        $minutes = isset($minutes[1]) && is_numeric(trim($minutes[1]))?floatval(trim($minutes[1])):0;
        if(!$minutes) $minutes = 60;
        switch($type){
            case 1: $subject = __("Įrašų siuntimo problema ").Configure::read('companyTitle'); break;
            case 2: $subject = (Configure::read('ADD_FULL_RECORD')?'ADD_FULL_RECORD':'ADD_RECORD').' '.__('vykdymo problema').' '.Configure::read('companyTitle'); break;
        }
        $fh = fopen($markerPath, "c+") or die("Unable to open file!");
        $time = fread($fh,filesize($markerPath));
        if(time() - $time >= 0){
            $headers = "Content-Type: text/plain; charset=UTF-8";
            mail(implode(',',$emailsList),$subject,implode(";\n",$message),$headers);
            fwrite($fh, time()+$minutes*60); //leisime siusti sekanti maila uz nustatyme nurodyto laiko kas kiek siusti mailus
        }
        fclose($fh);
    }
    
    public function start_action_process(){
        if(!$this->request->is('ajax')) die();
        $sensorId = $this->request->data['sensor_id'];
        $action = $this->request->data['action'];
        $currentActiveOrder = $this->ApprovedOrder->find('first', array('conditions'=>array('ApprovedOrder.sensor_id'=>$sensorId, 'ApprovedOrder.status_id'=>ApprovedOrder::STATE_IN_PROGRESS)));
        if(!$currentActiveOrder){
            echo json_encode(array('error'=>__('Šiuo metu nevykdomas nei vienas užsakymas. Pradėkite gamybą.')));
        }else{
            $additionalData = trim($currentActiveOrder['ApprovedOrder']['additional_data'])?json_decode($currentActiveOrder['ApprovedOrder']['additional_data'],true):array();
            if(isset($additionalData[$action])){
                unset($additionalData[$action]);
            }else{
                $additionalData[$action] = date('Y-m-d H:i:s');
            }
            $this->ApprovedOrder->id = $currentActiveOrder['ApprovedOrder']['id'];
            $this->ApprovedOrder->save(array('additional_data'=>json_encode($additionalData)));
            $this->Log->write('Operatorius darbo centro lange paspaude mygtuką ir atsiuntė šiuos duomenis: '. json_encode($additionalData), $this->Sensor->findById($sensorId));
            echo json_encode(array('success'=>__('OK')));
        }
        die();
    }
    
    private function start_cron_upload(){
        $path = ROOT.'/app/Plugin/LrytasImportPlansFromXML/files/';
        $dir = scandir($path);
        foreach($dir as $file){
            $imageFileType = pathinfo($path.$file,PATHINFO_EXTENSION);
            if(strtolower($imageFileType) != 'xml') continue;
            $this->XmlImport->xmlFile = $path.$file;
            $info = $this->XmlImport->import_orders();
            @unlink($this->XmlImport->xmlFile); 
        }
    }
    
    public function ReportsContr_generateWorkersReport_Hook($start, $end, $requestData){
        $shifts = $this->Shift->find('list', array('fields'=>array('id','id'), 'conditions'=>array('Shift.start <'=>$end, 'Shift.end >'=>$start)));
        $operatorsCalcModel = ClassRegistry::init('OperatorsCalculation');
        $operatorsCalcModel->bindModel(array('belongsTo'=>array('User')));
        $data = $operatorsCalcModel->find('all', array(
            'fields'=>array(
                'TRIM(User.first_name) AS first_name',
                'TRIM(User.last_name) AS last_name',
                'SUM(OperatorsCalculation.attach_quantity + OperatorsCalculation.printing_quantity) AS total_quantity',
                'ROUND(SUM(OperatorsCalculation.transition_time) / 60 / SUM(OperatorsCalculation.transition_count), 2) AS avg_transition_time',
                'ROUND(SUM(OperatorsCalculation.attach_quantity) / SUM(OperatorsCalculation.attach_count), 2) AS avg_attach_count',
                'ROUND(SUM(OperatorsCalculation.printing_unit_quantity) / (SUM(OperatorsCalculation.printing_time) / 3600), 2) AS avg_printing_speed',
            ),'conditions'=>array(
                'OperatorsCalculation.shift_id'=>$shifts,
                'OperatorsCalculation.user_id'=>$requestData['operators']
            ),'group'=>array('OperatorsCalculation.user_id')
        ));
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);
        $view = new View($this, false);
        $view->set(compact('data','objPHPExcel'));
        $view->render('/../Plugin/Lrytas/View/Plugins/generateWorkersReport');
    }

    public function change_order_position(){
        parse_str($this->request->data['order'],$ordersSort);
        $pos = 0;
        foreach($ordersSort['order'] as $orders){
            $orders = explode(',',trim($orders,','));
            foreach($orders as $orderId){
                $this->Plan->updateAll(array('position'=>$pos++),array('Plan.id'=>$orderId, 'Plan.has_problem'=>0));
            }
        }
        die();
    }
    
    public function manual_finish_order($planId){
        $plan = $this->Plan->withRefs()->find('first', array('recursive'=>2,'conditions'=>array('Plan.id'=>$planId)));
        if(empty($plan)){
            $this->redirect(array('controller'=>'plans','action'=>'index'));
        }
        if($this->request->is(array('post', 'put')) && !empty($this->request->data)){
            $currentShift = $this->Shift->find('first', array('conditions'=>array(
                'Shift.start <='=>$this->request->data['Plan']['end_date'],
                'Shift.end >='=>$this->request->data['Plan']['end_date'],
                'Shift.branch_id'=>$plan['Sensor']['branch_id'],
            )));
            $dataToSave = array(
                'created'=>$this->request->data['Plan']['start_date'],
                'end'=>$this->request->data['Plan']['end_date'],
                'sensor_id'=>$plan['Sensor']['id'],
                'plan_id'=>$plan['Plan']['id'],
                'status_id'=>ApprovedOrder::STATE_COMPLETE,
                'quantity'=>$this->request->data['Plan']['all_production'] * $plan['Plan']['box_quantity'],
                'box_quantity'=>$this->request->data['Plan']['all_production'],
                'confirmed_quantity'=>$this->request->data['Plan']['good_production'],
                'printing_quantity'=>$this->request->data['Plan']['good_production'],
                'printing_unit_quantity'=>$this->request->data['Plan']['good_production'] * $plan['Plan']['box_quantity'],
                'printing_time'=>strtotime($this->request->data['Plan']['end_date']) - strtotime($this->request->data['Plan']['start_date']),
                'confirmed'=>1,
                'manual_order'=>1,
                'user_id'=>$this->request->data['Plan']['user_id'],
                'shift_id'=>!empty($currentShift)?$currentShift['Shift']['id']:0,
            );
            $this->ApprovedOrder->save($dataToSave);
            $dataToSave = array(
                'approved_order_id'=>$this->ApprovedOrder->id,
                'loss_type_id'=>-2,
                'value'=>$this->request->data['Plan']['used_paper'],
                'while_confirming'=>0,
                'shift_id'=>!empty($currentShift)?$currentShift['Shift']['id']:0
            );
            $this->Loss->save($dataToSave);
            $redirect = array('plugin'=>false,'controller'=>'plans','action'=>'index')+$this->request->params['named'];
            $this->redirect($redirect);
        }
        $this->request->data = $plan;
        $this->User->virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \', User.last_name)');
        $users = $this->User->find('list', array('fields'=>array('id','full_name'), 'conditions'=>array('User.group_id'=>User::ID_OPERATOR), 'order'=>array('full_name')));
        $this->set(compact('users'));
        $parameters = array(&$this);
        $pluginData = $this->Help->callPluginFunction('Plans_ChangeData_After_Hook', $parameters, Configure::read('companyTitle'));
    }

    public function generate_attr_report(){
        $this->layout = 'ajax';
        if(empty($this->request->data)){ $this->redirect('/'); }
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $approvedOrders = $this->ApprovedOrder->find('all',array(
            'conditions'=>array(
                'ApprovedOrder.created >='=>$date_start,
                'ApprovedOrder.end <='=>$date_end,
                'ApprovedOrder.sensor_id'=>$this->request->data['sensors'],
            ),'order'=>array('ApprovedOrder.created')
        ));
        foreach($approvedOrders as &$order){
            $order['Problems'] = Hash::combine($this->FoundProblem->find('all', array(
                'fields'=>array(
                    'SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) AS duration',
                    'FoundProblem.problem_id',
                ),'conditions'=>array(
                    'FoundProblem.plan_id'=>$order['Plan']['id'],
                    'FoundProblem.problem_id'=>1,
                    'FoundProblem.start >='=>$order['ApprovedOrder']['created'],
                    'FoundProblem.end <'=>$order['ApprovedOrder']['end'],
                ),'group'=>array(
                    'FoundProblem.problem_id'
                ),'order'=>array('FoundProblem.id')
            )), '{n}.FoundProblem.problem_id', '{n}.0');
        }
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $this->set(compact('approvedOrders','objPHPExcel'));
    }
	
}
