<?php
App::uses('AppModel', 'Model');

class LrytasAppModel extends AppModel {
    
    CONST RITININIAI_IRENGINIAI_LINE_ID = 11;
    
    public function getWCSidebarLinks(){
        $javascript = '<script type="text/javascript">
        jQuery(document).ready(function($){
           $(".actionInitiator").live("click", function(e){
               e.preventDefault();    
               if($(this).hasClass("btn-success") && $(this).data("action") == "start_printing") return;
               $.ajax({
                   url: "'.Router::url('/lrytas/plugins/start_action_process',true).'",
                   type: "POST",
                   data: {sensor_id: $(this).attr("sensor_id"), action: $(this).data("action")},
                   context: $(this),
                   dataType: "JSON",
                   success: function(data){
                       if(data.hasOwnProperty("error")){
                           alert(data.error);
                       }else if(data.hasOwnProperty("success")){
                           $(this).toggleClass("btn-danger btn-success");
                       }
                   }
               });
           }) 
        });
        </script>';
        $attachingAction = 'sensor.line_id=='.self::RITININIAI_IRENGINIAI_LINE_ID.' && !currentPlan.ApprovedOrder.additional_data.start_attaching';
        $attachingButton = '<button ng-if="'.$attachingAction.'" data-action="start_attaching" class="actionInitiator btn btn-danger" sensor_id="{{sensor.id}}">'.__('Pritaisymas').'</button>';
        $printingAction = 'sensor.line_id !='.self::RITININIAI_IRENGINIAI_LINE_ID.' || currentPlan.ApprovedOrder.additional_data.start_attaching';
        $printingButton = '<button ng-if="'.$printingAction.'" data-action="start_printing" class="btn actionInitiator {{currentPlan.ApprovedOrder.additional_data.start_printing?\'btn-success\':\'btn-danger\'}}" sensor_id="{{sensor.id}}">'.__('Tiražas').'</button>';
        $rinsingAction = 'currentPlan.ApprovedOrder.additional_data.start_printing';
        $rinsingButton = '
            <button ng-if="'.$rinsingAction.' && sensor.id==1" data-action="start_rinsing" class="btn actionInitiator {{currentPlan.ApprovedOrder.additional_data.start_rinsing?\'btn-success\':\'btn-danger\'}}" sensor_id="{{sensor.id}}">{{\''.__('Pjovimas').'\'}}</button>
            <button ng-if="'.$rinsingAction.' && [6,5,7,8].indexOf(sensor.id) >= 0" data-action="start_rinsing" class="btn actionInitiator {{currentPlan.ApprovedOrder.additional_data.start_rinsing?\'btn-success\':\'btn-danger\'}}" sensor_id="{{sensor.id}}">{{\''.__('Plovimas').'\'}}</button>
        ';
        return '<div ng-if="!needTimePager">{{rytasAction}}'.$attachingButton.$printingButton.$rinsingButton.'</div>'.$javascript;
    }
    
}
