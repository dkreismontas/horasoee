<?php
App::uses('TimelinePeriod', 'Controller/Component');
App::uses('Problem', 'Model');
class Lrytas extends LrytasAppModel{

    CONST ID_OPERATOR = 4;

     public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
    }
     
    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Lrytas/css/Lrytas_bare.css';
    }
    
    public function Record_FindAndSetOrder_Hook($sensor, $approvedOrderId, $plan, $multiplier, &$fields, &$conditions){
        static $approvedOrderModel = null;
        if(!isset($approvedOrderModel)) $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $approvedOrder = $approvedOrderModel->findById($approvedOrderId);
        if($approvedOrder){
            $additionalData = trim($approvedOrder['ApprovedOrder']['additional_data'])?json_decode($approvedOrder['ApprovedOrder']['additional_data'],true):array();
            if(isset($additionalData['start_printing'])){
                //$fields['start_printing'] = 1;
                $fields['start_printing'] = 'IF(Record.created >= \''.$additionalData['start_printing'].'\',1,0)';
            }
            // if(isset($additionalData['start_rinsing'])){
                // $fields['start_rinsing'] = 1;
            // }
        }
    }
    
    public function WorkCenter_AddOrderBuildTimeGraph_Hook($fApprovedOrders, &$periods, $fRecords){
        foreach ($fApprovedOrders as $approvedOrder) {
            $additionalData = trim($approvedOrder['ApprovedOrder']['additional_data'])?json_decode($approvedOrder['ApprovedOrder']['additional_data'],true):array();
            $originalProductionName = $approvedOrder['Plan']['production_name'].' '.$approvedOrder['Plan']['production_code'];
            if(isset($additionalData['start_printing'])){
                $approvedOrder['Plan']['production_name'] = __('Pritaisymas').' '.$originalProductionName;
                TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(
                    TimelinePeriod::TYPE_PRODUCT,
                    new DateTime($approvedOrder[ApprovedOrder::NAME]['created']),
                    new DateTime($additionalData['start_printing']),
                    $approvedOrder
                ));
                $approvedOrder['Plan']['production_name'] = __('Tiražas').' '.$originalProductionName;
                TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(
                    TimelinePeriod::TYPE_PRINTING,
                    new DateTime(date('Y-m-d H:i:s',strtotime($additionalData['start_printing'])+1)),
                    new DateTime($approvedOrder[ApprovedOrder::NAME]['end']),
                    $approvedOrder
                ));
            }else{
                $approvedOrder['Plan']['production_name'] = __('Pritaisymas').' '.$originalProductionName;
                TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(
                    TimelinePeriod::TYPE_PRODUCT,
                    new DateTime($approvedOrder[ApprovedOrder::NAME]['created']),
                    new DateTime($approvedOrder[ApprovedOrder::NAME]['end']),
                    $approvedOrder
                ));
            }
            
        }
        return true;
    }

     public function FoundProblem_fillEmptyGapsToBuildGraph_Hook($fSensor, $fRecords, $start, $end){
        $foundproblemModel = Classregistry::init('FoundProblem');
        $fProblem = $foundproblemModel->schema();
        array_walk($fProblem, function(&$val){ $val = 0; });
        $foundProblems = array();
        if(current(array_reverse($fRecords))['Record']['created'] < $end){
            $fRecords[] = array('Record'=>array('created'=>$end));
        }
        $rinsingStart = '';
        foreach($fRecords as $key => $fRecord){
            if(strtotime($fRecord['Record']['created']) - strtotime($start) > Configure::read('recordsCycle')){
                $fProblem['start'] = $start;
                $fProblem['end'] = $fRecord['Record']['created'];
                $fProblem['problem_id'] = 0;
                $foundProblems[] = array(
                    'FoundProblem'=>$fProblem, 
                    'Problem'=>array('id'=>0,'name'=>__('Išjungtas jutiklis'))
                );
            }
            if(isset($fRecord['Record']['start_rinsing']) && $fRecord['Record']['start_rinsing'] && !trim($rinsingStart)){
                $rinsingStart = $fRecord['Record']['created'];
                
            }elseif(trim($rinsingStart) && (!isset($fRecord['Record']['start_rinsing']) || !$fRecord['Record']['start_rinsing'])){
                $fProblem['start'] = $rinsingStart;
                $fProblem['end'] = $fRecord['Record']['created'];
                $fProblem['problem_id'] = -2;
                $foundProblems[] = array(
                    'FoundProblem'=>$fProblem, 
                    'Problem'=>array('id'=>-2,'name'=>__('Plovimas'))
                );
                $rinsingStart = '';
            }
            $start = $fRecord['Record']['created'];
        }
        return !empty($foundProblems)?$foundProblems:true;
    }

    public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, $fShift,&$fTransitionProblemTypes, &$allPlans){
        if(isset($currentPlan['ApprovedOrder'])){
            $recordModel = ClassRegistry::init('Record');
            $dataSum = $recordModel->find('first', array(
                'fields'=>array(
                    'SUM(Record.quantity) AS total_quantity',
                    'SUM(Record.unit_quantity) AS total_unit_quantity',
                    'ROUND(SUM(IF(Record.found_problem_id IS NULL AND Record.start_printing = 0, Record.unit_quantity, 0)) / SUM(IF(Record.found_problem_id IS NULL AND Record.start_printing = 0, '.Configure::read('recordsCycle').', 0)),2) AS avg_atach_speed',
                    'ROUND(SUM(IF(Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.unit_quantity, 0)) / SUM(IF(Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, '.Configure::read('recordsCycle').', 0)),2) AS avg_printing_speed',
                ),'conditions'=>array(
                    'Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id']
                )
            ));
            $quantityOutput['quantity'] = $currentPlan['ApprovedOrder']['box_quantity'].' '.__('vnt.');
            $currentPlan['Plan'] = array_merge($currentPlan['Plan'], $dataSum[0]);
            $currentPlan['Plan'] = array_merge($currentPlan['Plan'], $this->calculateInformationTable($currentPlan['ApprovedOrder']['plan_id']));
            $currentPlan['Plan']['preparation_time'] = self::convertMinutesToTime($currentPlan['Plan']['preparation_time']);
            $currentPlan['Plan']['attach_time'] = self::convertMinutesToTime($currentPlan['Plan']['attach_time']);
            $currentPlan['Plan']['production_time'] = self::convertMinutesToTime($currentPlan['Plan']['production_time']);
            
            $currentPlan['Plan']['step_unit_divide'] = 1;  //DEL GREICIU GRAFIKO HALF DONUT, KAD STEPO NEDALINTU IS 60 IR REZIAI BUTU PAGAL GREITI VALANDOMIS
        }
        foreach($allPlans as &$allPlan){
            if(!isset($allPlan['Plan']['signature_side'])) continue;
            $serializedData = json_decode($allPlan['Plan']['serialized_data'],true);
            $allPlan['Plan']['name'] = $allPlan['Plan']['production_name']. '('.$allPlan['Plan']['sm_number'].'; '.$allPlan['Plan']['order_part'].'; '.$allPlan['Plan']['signature_number'].'; '.$allPlan['Plan']['signature_side'].'; '.(isset($serializedData['paper_code'])?$serializedData['paper_code']:'').')';
        }
        
    }

    private static function convertMinutesToTime($minutes){
        $h = (int)($minutes / 60)>0?(int)($minutes / 60).'h ':'';
        return $h.($minutes % 60).'min';
    }

    public function ApprovedOrdersEditHook(&$controller){
        if(!$controller->viewVars['item']['ApprovedOrder']['confirmed']){
            $recordModel = ClassRegistry::init('Record');
            $tirazasQuantity = $recordModel->find('first', array(
                'fields'=>array(
                    'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0, Record.quantity, 0)) AS tirazas_count',
                    //'SUM(IF(Record.start_printing = 0, Record.quantity, 0)) AS pritaisymas_count'
                ),
                'conditions'=>array(
                    'Record.approved_order_id'=>$controller->viewVars['item']['ApprovedOrder']['id'],
                )
            ));
            $controller->viewVars['item']['ApprovedOrder']['tirazas_count'] = $tirazasQuantity[0]['tirazas_count'];
            //$controller->viewVars['item']['ApprovedOrder']['quantity'] = $tirazasQuantity[0]['tirazas_count'] + $tirazasQuantity[0]['pritaisymas_count'];
        }else{
            //$controller->viewVars['item']['ApprovedOrder']['quantity'] = $controller->viewVars['item']['ApprovedOrder']['box_quantity'];
        }
        if(isset($controller->request->data['ApprovedOrder']['confirmed_item_quantity'])){
            $controller->ApprovedOrder->updateAll(
                array('confirmed_quantity'=>$controller->request->data['ApprovedOrder']['confirmed_item_quantity']),
                array('ApprovedOrder.id'=>$controller->request->data['ApprovedOrder']['id'])
            );
        }
        $transitionTime = $controller->FoundProblem->find('first', array(
            'fields'=>array('SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) AS duration'),
            'conditions'=>array(
                'FoundProblem.sensor_id'=>$controller->viewVars['item']['ApprovedOrder']['sensor_id'],
                'FoundProblem.start >='=>$controller->viewVars['item']['ApprovedOrder']['created'],
                'FoundProblem.end <='=>$controller->viewVars['item']['ApprovedOrder']['end'],
                'FoundProblem.problem_id'=>Problem::ID_NEUTRAL
            )
        ));
        $userModel = ClassRegistry::init('User');
        $appOrderModel = ClassRegistry::init('ApprovedOrder');
        $userModel->virtualFields = array('user_name'=>'CONCAT(User.first_name,\' \', User.last_name)');
        $recordModel = ClassRegistry::init('Record');
        $usersQuantities = $recordModel->find('all', array(
            'fields'=>array(
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.quantity,0)) as printing_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0,' . (Configure::read('recordsCycle')) . ',0)) AS printing_time',
                'TRIM(Record.user_id) AS user_id'
            ),
            'conditions'=>array(
                'Record.approved_order_id'=>$controller->viewVars['item']['ApprovedOrder']['id'],
            ),'group'=>array('Record.user_id')
        ));
        if(empty($usersQuantities)){
            //$appOrderModel->bindModel(array('belongsTo'=>array('User')));
            $appOrder = $appOrderModel->find('first',array(
                'conditions'=>array(
                    'ApprovedOrder.id'=>$controller->viewVars['item']['ApprovedOrder']['id'],
                    'ApprovedOrder.user_id >'=>0,
                )
            ));
            if($appOrderModel && !empty($appOrder)){
                $usersQuantities[0][0] = array(
                    'printing_quantity'=>$appOrder['ApprovedOrder']['printing_quantity'], 
                    'printing_time'=>$appOrder['ApprovedOrder']['printing_time'], 
                    'user_id'=>$appOrder['ApprovedOrder']['user_id']);
            }
        }
        $controller->set(array(
            'transitionDuration'=> number_format(round(floatval($transitionTime[0]['duration']/60),2),2,'.',''),
            'users'=>$userModel->find('list', array('fields'=>array('User.id', 'user_name'))),
            'usersQuantities'=>$usersQuantities
        ));
        
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/edit');
    }

    private function calculateInformationTable(int $planId){
        static $recordModel = null;
        if($recordModel == null) $recordModel = ClassRegistry::init('Record');
        $data = $recordModel->find('first', array(
            'fields'=>array(
                'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0,Record.quantity,0)) AS atspaudu_kiekis',
                'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0,Record.unit_quantity,0)) AS tirazo_kiekis',
                'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0,Record.quantity,0)) AS tirazo_kiekis_atspaudai',
                'SUM(IF(Record.start_printing = 0,Record.quantity,0)) AS pritaisymo_kiekis',
                'SUM(IF(Record.start_printing = 0,'.Configure::read('recordsCycle').',0)) AS pritaisymo_trukme',
                'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0,'.Configure::read('recordsCycle').',0)) AS tirazo_trukme',
            ),'conditions'=>array(
                'Record.found_problem_id IS NULL',
                'Record.plan_id'=>$planId,
            )
        ));
        $data[0]['vid_pritaisymo_greitis'] = $data[0]['pritaisymo_trukme'] > 0?round($data[0]['pritaisymo_kiekis'] / ($data[0]['pritaisymo_trukme'] / 3600), 2):0;
        $data[0]['vid_tirazo_greitis'] = $data[0]['tirazo_trukme'] > 0?round($data[0]['tirazo_kiekis_atspaudai'] / ($data[0]['tirazo_trukme'] / 3600), 2):0;
        return current($data);
    }

    public function Oeeshell_beforeSearchGetFactTime_Hook(&$additional_fields){
        $additional_fields[] = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id = 1, Record.quantity, 0)) AS pasiruosimas_unit_count';
        $additional_fields[] = 'SUM(IF(Record.start_printing = 0, Record.quantity, 0)) AS pritaisymas_unit_count';
        $additional_fields[] = 'SUM(IF(Record.start_printing = 1 AND Record.start_rinsing = 0, Record.quantity, 0)) AS tirazas_unit_count';
        $additional_fields[] = 'SUM(IF(Record.plan_id IS NOT NULL, Record.quantity,0)) as total_quantity';
    }

    public function ReportsContr_BeforeGenerateXLS_Hook(&$reportModel){
        $reportModel = ClassRegistry::init('Lrytas.LrytasReport');
    }

    public function ReportsContr_BeforeCalculatePeriodXLS_Hook(&$reportModel, &$templateTitle){
        $templateTitle = '/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Reports/calculate_period_xls';
    }

    public function Oeeshell_appendGetFactTimeLoop_Hook(&$records, &$val, $sensor_id){
        if(!isset($records['sensors'][$sensor_id]['pasiruosimas_unit_count'])) $records['sensors'][$sensor_id]['pasiruosimas_unit_count'] = 0;
        if(!isset($records['sensors'][$sensor_id]['pritaisymas_unit_count'])) $records['sensors'][$sensor_id]['pritaisymas_unit_count'] = 0;
        if(!isset($records['sensors'][$sensor_id]['tirazas_unit_count'])) $records['sensors'][$sensor_id]['tirazas_unit_count'] = 0;
        $records['sensors'][$sensor_id]['pasiruosimas_unit_count'] += $val[0]['pasiruosimas_unit_count'];
        $records['sensors'][$sensor_id]['pritaisymas_unit_count'] += $val[0]['pritaisymas_unit_count'];
        $records['sensors'][$sensor_id]['tirazas_unit_count'] += $val[0]['tirazas_unit_count'];
    }

    public function Oeeshell_filterGetFactTimeRecords_Hook(&$time, &$shift, $records, $returnData, $sensors){
        if($returnData) return;
        //apskaiciuosime duomenis pagal darbuotoja
        $recordModel = ClassRegistry::init('Record');
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $operatorsCalculationModel = ClassRegistry::init('OperatorsCalculation');
        $recordModel->bindModel(array('belongsTo'=>array('User','FoundProblem')));
        $data = $recordModel->find('all', array(
            'fields'=>array(
                'TRIM(Record.sensor_id) AS sensor_id',
                'TRIM(Record.user_id) AS user_id',
                'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id=1,' . Configure::read('recordsCycle') . ',0)) as transition_time',
                'COUNT(DISTINCT CASE WHEN FoundProblem.problem_id = 1 THEN FoundProblem.id END ) as transition_count',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 0, Record.quantity,0)) as attach_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 0, Record.unit_quantity,0)) as attach_unit_quantity',
                'COUNT(DISTINCT(Record.approved_order_id)) as attach_count',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.unit_quantity,0)) as printing_unit_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.quantity,0)) as printing_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0,' . (Configure::read('recordsCycle')) . ',0)) AS printing_time',
            ),'conditions'=>array(
                'Record.sensor_id'=>$sensors,
                'Record.shift_id'=>$shift['Shift']['id'],
                'User.group_id'=>self::ID_OPERATOR
            ),'group'=>array('Record.user_id')
        ));    
        $operatorsCalculationModel->deleteAll(array('sensor_id'=>$sensors, 'shift_id'=>$shift['Shift']['id']));
        $manualFinishOrders = $approvedOrderModel->withRefs()->find('all', array('conditions'=>array(
            'ApprovedOrder.manual_order'=>1,
            'ApprovedOrder.shift_id'=>$shift['Shift']['id'],
        )));
        $manualFinishOrdersKeysUsed = array();
        foreach($data as $calc){
            //jei sioje pamainoje esamas useris su tuo paciu sensorium jau gamino kazka, tai prie esamu duomenu reikia prideti rankiniu budu uzbaigto uzsakymo duomenis ir saugoti kaip viena irasa
            foreach($manualFinishOrders as $key => $manualFinishOrder){
                if($calc[0]['user_id'] == $manualFinishOrder['ApprovedOrder']['user_id'] && $calc[0]['sensor_id'] == $manualFinishOrder['ApprovedOrder']['sensor_id']){
                    $manualFinishOrdersKeysUsed[] = $key;
                    $calc[0]['printing_unit_quantity'] += $manualFinishOrder['ApprovedOrder']['printing_unit_quantity'];
                    $calc[0]['printing_quantity'] += $manualFinishOrder['ApprovedOrder']['printing_quantity'];
                    $calc[0]['printing_time'] += $manualFinishOrder['ApprovedOrder']['printing_time'];
                }
            }
            $operatorsCalculationModel->create();
            $operatorsCalculationModel->save(current($calc)+array('shift_id'=>$shift['Shift']['id']));
        }
        //patikriname gal liko rankiniu budu uzbaigtu uzsakymu, kurie nesutapo su sioje pamainoje gamintais records lentelej fiksuotais operatoriaus irasais
        $notUsedManualFinishOrders = array_diff_key($manualFinishOrders, array_flip($manualFinishOrdersKeysUsed));
        $operatorsDataToSave = array();
        foreach($notUsedManualFinishOrders as $notUsedManualFinishOrder){
            $userId = $notUsedManualFinishOrder['ApprovedOrder']['user_id'];
            $sensorId = $notUsedManualFinishOrder['ApprovedOrder']['sensor_id'];
            if(!isset($operatorsDataToSave[$userId])){
                $operatorsDataToSave[$userId][$sensorId] = array(
                    'shift_id'=>$notUsedManualFinishOrder['ApprovedOrder']['shift_id'],
                    'sensor_id'=>$notUsedManualFinishOrder['ApprovedOrder']['sensor_id'],
                    'user_id'=>$userId,
                    'printing_unit_quantity'=>0,
                    'printing_quantity'=>0,
                    'printing_time'=>0,
                );
            }
            $operatorsDataToSave[$userId][$sensorId]['printing_unit_quantity'] += $notUsedManualFinishOrder['ApprovedOrder']['printing_unit_quantity'];
            $operatorsDataToSave[$userId][$sensorId]['printing_quantity'] += $notUsedManualFinishOrder['ApprovedOrder']['printing_quantity'];
            $operatorsDataToSave[$userId][$sensorId]['printing_time'] += $notUsedManualFinishOrder['ApprovedOrder']['printing_time'];
        }
        //dar karta saugome dabar jau tik rankiniu budu uzbaigtus uzsakymus
        foreach($operatorsDataToSave as $userId => $operatorsData){
            foreach($operatorsData as $sensorId => $saveRecord){
                $operatorsCalculationModel->create();
				pr($saveRecord);
                $operatorsCalculationModel->save($saveRecord);
            }
        }
        //Prasideda dashboards_calculation lenteliu irasams rankinku budu baigtiem papildyti
        //Prie is record irasu apskaiciuotu uzsakymu pridesime rankiniu budu uzbaigtus uzsakymus tiesiai is approved_orders lenteles
        if(!empty($manualFinishOrders)){
            if(!empty($time)){
                $pattern = current($time);
                array_walk($pattern, function(&$data){
                    foreach($data as &$record){
                         $record = 0;
                    }
                });
            }else $pattern = array();
            foreach($manualFinishOrders as $manualFinishOrder){
                $record = $pattern;
                $record[0]['total_time']=$manualFinishOrder['ApprovedOrder']['printing_time'];
                $record[0]['fact_prod_time']=$manualFinishOrder['ApprovedOrder']['printing_time'];
                $record[0]['total_quantity']=$manualFinishOrder['ApprovedOrder']['confirmed_quantity'];
                $record[0]['start']=$manualFinishOrder['ApprovedOrder']['created'];
                $record[0]['end']=$manualFinishOrder['ApprovedOrder']['end'];
                $record['Sensor']['type']=$manualFinishOrder['Sensor']['type'];
                $record['Plan']['mo_number']=$manualFinishOrder['Plan']['mo_number'];
                $record['Plan']['box_quantity']=$manualFinishOrder['Plan']['box_quantity'];
                $record['Plan']['step']=$manualFinishOrder['Plan']['step'];
                $record['Plan']['id']=$manualFinishOrder['Plan']['id'];
                $record['Record']['approved_order_id']=$manualFinishOrder['ApprovedOrder']['id'];
                $record['Record']['shift_id']=$manualFinishOrder['ApprovedOrder']['shift_id'];
                $record['Record']['sensor_id']=$manualFinishOrder['ApprovedOrder']['sensor_id'];
                $time[] = $record;
            }
        }
    }

    public function verificationHook($recordModel, $currentPlan){
        return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>false);//TODO nuimti kai reikes ijungti vel patikras
        if(!$currentPlan || !$currentPlan['Plan']['quantity']){
            return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>false);
        }
        $sensorId = $currentPlan['ApprovedOrder']['sensor_id'];
        $types = array(
            'lapiniai'=>array(1, 6, 10),
            'ritininiai'=>array(5, 7, 8)
        );
        if(!in_array($sensorId, $types['lapiniai']) && !in_array($sensorId, $types['ritininiai']) ){
            return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>false);
        }
        $madedCount = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.unit_quantity) AS sum'),
            'conditions'=>array('Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id'], 'Record.start_printing'=>1, 'Record.start_rinsing'=>0)
        ));
        $verificationModel = ClassRegistry::init('Verification');
        $verificationsCount = $verificationModel->find('first', array(
            'fields'=>array('COUNT(Verification.id) AS count'), 
            'conditions'=>array('Verification.approved_order_id'=>$currentPlan['ApprovedOrder']['id'])
        ));
        $verificationsCount = (int)$verificationsCount[0]['count'];
        $mustBeMadeCount = $madedCount[0]['sum'] > 0?1:0;
        if(in_array($sensorId, $types['lapiniai'])){
            if($currentPlan['Plan']['quantity'] < 5000){
                $mustBeMadeCount += floor(min(1000, $madedCount[0]['sum']) / 250) + max(0, floor(($madedCount[0]['sum']-1000) / 1000));
            }elseif($currentPlan['Plan']['quantity'] < 10000){
                $mustBeMadeCount += floor(min(1500, $madedCount[0]['sum']) / 500) + max(0, floor(($madedCount[0]['sum']-1500) / 1500));
            }elseif($currentPlan['Plan']['quantity'] < 15000){
                $mustBeMadeCount += floor(min(2000, $madedCount[0]['sum']) / 500) + max(0, floor(($madedCount[0]['sum']-2000) / 2000));
            }else{
                $mustBeMadeCount += floor(min(3000, $madedCount[0]['sum']) / 1000) + max(0, floor(($madedCount[0]['sum']-3000) / 3000));
            }
        }else{//ritininiai
            if($currentPlan['Plan']['quantity'] < 5000){
                $mustBeMadeCount += floor($madedCount[0]['sum'] / 1000);
            }elseif($currentPlan['Plan']['quantity'] < 10000){
                $mustBeMadeCount += floor($madedCount[0]['sum'] / 1500);
            }elseif($currentPlan['Plan']['quantity'] < 15000){
                $mustBeMadeCount += floor($madedCount[0]['sum'] / 2000);
            }elseif($currentPlan['Plan']['quantity'] < 50000){
                $mustBeMadeCount += floor($madedCount[0]['sum'] / 3000);
            }else{
                $mustBeMadeCount += floor($madedCount[0]['sum'] / 10000);
            }
        }
        if($mustBeMadeCount > $verificationsCount){
            return array('promptVerification'=>true, 'countdown'=>0, 'verificationOnProductionEnd'=>true,'text'=>__('Atlikite kokybės kontrolę!'));
        }else{
            return array('promptVerification'=>false, 'countdown'=>0, 'verificationOnProductionEnd'=>false);
        }
    }

    public function Workcenter_beforeRegisterVerification_Hook($currentPlan){
        $verificationModel = ClassRegistry::init('Verification');
        $recordModel = ClassRegistry::init('Record');
        $madedCount = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.unit_quantity) AS sum'),
            'conditions'=>array('Record.approved_order_id'=>$currentPlan['ApprovedOrder']['id'], 'Record.start_printing'=>1, 'Record.start_rinsing'=>0)
        ));
        $verificationModel->save(array(
            'approved_order_id'=>$currentPlan['ApprovedOrder']['id'],
            'plan_id'=>$currentPlan['ApprovedOrder']['plan_id'],
            'current_count'=>$madedCount[0]['sum']
        ));
        die();
    }

    public function Plans_AfterIndex_Hook(&$controller){
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Plans/index');
    }
    public function ApprovedOrdersIndexHook(&$controller){
        $controller->set('allowToRestore',$controller->hasAccessToAction(array('controller'=>'ApprovedOrders', 'action'=>'edit')));
        $controller->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/ApprovedOrders/index');
    }
    public function Plan_Constructor_Hook(&$planModel){
        if(!in_array('get_mo_stats', explode('/',Router::url()))){
            $planModel->virtualFields = array('mo_number'=>'sm_number');
        }
    }

    public function ApprovedOrders_Index_BeforeSearch_Hook(&$arr, &$requestParams, &$approvedOrderModel){
        $approvedOrderModel->bindModel(array('belongsTo'=>array('Loss'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.id = Loss.approved_order_id AND Loss.loss_type_id = -1')))));
        if(isset($arr['conditions'])){
            array_walk($arr['conditions'], function(&$data){
                 $data = str_replace('mo_number','sm_number',$data); 
                 $data = str_replace('production_code','production_name',$data); 
            });
        }
        $arr['fields'] = array('ApprovedOrder.*','Plan.*','Sensor.*','Status.*','SUM(Loss.value) AS loss_sum');
        $arr['group'] = array('ApprovedOrder.id');
        $sortParams = array_filter($requestParams->query, function($key){ return preg_match('/^sort\d+/',$key); },ARRAY_FILTER_USE_KEY);
        if(!empty($sortParams)){
            $arr['order'] = array();
            foreach($sortParams as $sortParam){
                $parts = explode('|', $sortParam);
                if(sizeof($parts)!=2)continue;
                $arr['order'][$parts[0]] = $parts[1];
            }
        }
    }

    public function beforePlanPaginateHook(&$arr){
        array_walk($arr['conditions'], function(&$data){
            $data = str_replace('mo_number','sm_number',$data);
            $data = str_replace('production_code','production_name',$data);  
        });
    }
    
    public function detectEventsHook(&$record, &$siblingRowsHavingQuantity, $currentPlan, &$mustReachZerosCountToStartProblem, &$event, $previousRecord, &$fFoundProblem, &$prodStartsOn, &$problemStartsOn, $fSensor){
        if(!$currentPlan) return;
        $additionalData = json_decode($currentPlan['ApprovedOrder']['additional_data'],true);
        if(self::RITININIAI_IRENGINIAI_LINE_ID == $fSensor['Line']['id'] && !isset($additionalData['start_attaching'])){//jei jutiklis priklauso ruloniniams ir dar nepaspaustas mygtukas "Pritaisymas", turi vykti derinimas
            $problemStartsOn = PHP_INT_MAX;
            $prodStartsOn = PHP_INT_MAX;
        }elseif(!isset($additionalData['start_printing'])){//jei dar nepaspaustas mygtukas "Tirazas", problemos kurtis neturi
            //Kai paspaustas "pritaisymas" dar gali buti likusiu neapdorotu irasu, kuriu laikas mazesnis nei pritaisymo paspaudimas. Jie vis dar turi buti laikomi derinimo dalimi
            if(isset($additionalData['start_attaching']) && strtotime($record['Record']['created']) < strtotime($additionalData['start_attaching'])){
                $problemStartsOn = PHP_INT_MAX;
                $prodStartsOn = PHP_INT_MAX;
            }elseif(!$fFoundProblem){
                $problemStartsOn = -1;
                $prodStartsOn = 0;
            }
        }//kai paspaudziamas tirazas dar gali buti like neapdorotu irasu su 0 kiekiais. Kad jie nevirstu i problema, reikia dirbtinai sakyti, kad tai geri irasai
        elseif(isset($additionalData['start_printing']) && strtotime($record['Record']['created']) < strtotime($additionalData['start_printing'])){
             $problemStartsOn = -1;
             $prodStartsOn = 0;
        }
    }

    public function Plans_ChangeData_After_Hook(&$plansController){
        $joindesLinesId = array(10,11);
        $sensorId = $plansController->request->data['Plan']['sensor_id'];
        $fSensor = $plansController->Sensor->withRefs()->findById($sensorId);
        $linesIds = array();
        foreach($fSensor['Line'] as $line){
            $linesIds[] = $line['id'];
        }
        if(array_intersect($linesIds, $joindesLinesId)){
            $linesIds = array_unique(array_merge($linesIds, $joindesLinesId));
        }
        $lineSensorModel = ClassRegistry::init('LineSensor');
        $lineSensorModel->bindModel(array('belongsTo'=>array('Sensor')));
        $sameLineSensors = $lineSensorModel->find('all', array('recursive'=>1,'fields'=>array('Sensor.id','Sensor.name'),'conditions'=>array('LineSensor.line_id'=>$linesIds)));
        foreach($sameLineSensors as $sameLineSensor){
            $sensorId = $sameLineSensor['Sensor']['id'];
            $plansController->request->data['Possibility'][$sensorId] = $sameLineSensor;
        }
        $plansController->set(array(
            'columns'   => array(
                'id'=>array('title'=>'', 'type'=>'hidden'),
            ),
        ));
        $additionalData = json_decode($plansController->request->data['Plan']['serialized_data'],true);
        $plansController->request->data['Plan']['paste_code'] = isset($additionalData['operation'])?$additionalData['operation']:'';
        $plansController->request->data['Plan']['serialized_data'] = str_replace(
            array('"paper"','"paper_width"','"paper_height"', '"grammage"', '"paper_norm_kg"', '"paper_norm_sheets"', '"colorfulness"','"quantity"','"operation"'),
            array(__('"Popierius"'), __('"Popieriaus plotis"'), __('"Popieriaus ilgis"'), __('"Gramatūra"'), __('"Norma, kg"'), __('"Norma, vnt"'), __('"Spalvingumas;"'), __('"Tiražo kiekis"'), __('"Operacija"')),
            $plansController->request->data['Plan']['serialized_data']
        );
    }

    public function setTimeGraphHoverProduct($timeLineObj){
        $title = '<br/><b>'.__('Gaminys').'</b>: '.$timeLineObj->data['Plan']['production_code'].' ('.$timeLineObj->data['Plan']['production_name'].'), '.$timeLineObj->data['Plan']['order_part'].', '.$timeLineObj->data['Plan']['signature_number'].', '.$timeLineObj->data['Plan']['signature_side'];
        $title .= '<br/><b>'.__('Užsakymo nr.').'</b>: '.$timeLineObj->data['Plan']['mo_number'];
        if(isset($timeLineObj->data['ApprovedOrder'])) {
            //$ApprovedOrder = ClassRegistry::init('ApprovedOrder');
            //$relatedCount = $ApprovedOrder->getRelatedApprovedOrders($timeLineObj->data['Plan']['id']);             
            $title .= '<br/><b>' . __('Pritaisymo kiekis').':</b> '. $timeLineObj->data['ApprovedOrder']['attachment_quantity'];
            $title .= '<br/><b>' . __('Tiražo kiekis').':</b> '. ($timeLineObj->data['ApprovedOrder']['printing_quantity']).($timeLineObj->data['Plan']['quantity']>0?' '.__('iš').' '.$timeLineObj->data['Plan']['quantity']:'') ;
            if($timeLineObj->data['Plan']['quantity'] > 0){
                $title .= '<br/><b>' . __('Likęs kiekis').':</b> '.($timeLineObj->data['Plan']['quantity'] - $timeLineObj->data['ApprovedOrder']['printing_quantity']);
            }
            $title .= '<br/><b>' . __('Bendras kiekis').':</b> '. $timeLineObj->data['ApprovedOrder']['box_quantity'];
        }
        return $title;
    }
    
    public function Plan_getMOListByRangeBeforeSearch_Hook(&$binder, &$searchParams){
        $searchParams['fields'][] = 'Plan.sm_number';
        $searchParams['fields'][] = 'Plan.order_part';
        $searchParams['fields'][] = 'Plan.signature_number';
        $searchParams['fields'][] = 'Plan.signature_side';
        $searchParams['conditions']['Plan.sm_number <>'] = '';
    }
    
    public function DashboardsGetMoStatsHook(&$dashboardsController){
        $dashboardsController->render('/../Plugin/'.$this->name.'/View/ProjectViewsHooks/Dashboards/get_mo_stats'); 
    }

    public function Dashboards_ReplaceMoNumber_Hook(&$moNumbers){
        if(!is_array($moNumbers) && trim($moNumbers)){
            $planModel = ClassRegistry::init('Plan');
            $moNumbers = $planModel->find('list', array('fields'=>array('Plan.id','Plan.mo_number'), 'conditions'=>array('Plan.sm_number'=>$moNumbers)));
        }
    }
    
    public function WorkCenter_ExtendProdUpdateOrderQuantities_Hook(&$approvedOrder, &$recordModel){
        $recordModel->bindModel(array('belongsTo'=>array('FoundProblem')));
        $approvedOrder['ApprovedOrder'] = array_merge($approvedOrder['ApprovedOrder'], current($recordModel->find('first', array(
            'fields'     => array(
                'SUM(Record.unit_quantity) AS quantity',
                'SUM(IF(FoundProblem.problem_id = '.Problem::ID_NEUTRAL.', Record.quantity, 0)) AS transition_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 0, Record.quantity, 0)) AS attachment_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.quantity, 0)) AS printing_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0, Record.unit_quantity, 0)) AS printing_unit_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 0,' . (Configure::read('recordsCycle')) . ',0)) AS attachment_time',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL AND Record.start_printing = 1 AND Record.start_rinsing = 0,' . (Configure::read('recordsCycle')) . ',0)) AS printing_time',
            ),'conditions' => array(
                'Record.approved_order_id' => $approvedOrder['ApprovedOrder']['id'],
                'Record.unit_quantity >' => 0,
            )
        ))));
        return true; //privalo grazinti true, kitaip bus atlikta papildomai standartine HorasOee salyga po sio metodo iskvietiumo
    }

    public function WorkCenter_BeforeCompleteProdSave_Hook(&$ord, $shift){
        $logModel = ClassRegistry::init('Log');
        $lossModel = ClassRegistry::init('Loss');
        $recordModel = ClassRegistry::init('Record');
        $rinsingQuantity = $recordModel->find('first', array(
            'fields'=>array('SUM(Record.rinse_quantity) AS rinse_quantity'),
            'conditions'=>array('Record.approved_order_id'=>$ord['ApprovedOrder']['id'])
        ));
        if($rinsingQuantity[0]['rinse_quantity'] > 0){
            $lossModel->newItem($ord[ApprovedOrder::NAME]['id'], -1, $rinsingQuantity[0]['rinse_quantity'], ($shift ? $shift[Shift::NAME]['id'] : null),0,(array)$logModel->user);
        }
    }

    public function WorkCenter_BeforePostphoneProdSave_Hook(&$ord){
        $this->WorkCenter_BeforeCompleteProdSave_Hook($ord, false);
        if(isset($ord[ApprovedOrder::NAME]['plan_id'])){
            $planModel = ClassRegistry::init('Plan');
            $planModel->save(array('position'=>999999,'id'=>$ord[ApprovedOrder::NAME]['plan_id']));
        }
    }
    
    public function Sensor_getAsSelectOptions_Hook(){
        $sensorModel = ClassRegistry::init('Sensor');
        $sensorModel->bindModel(array('belongsTo'=>array(
            'LineSensor'=>array('foreignKey'=>false, 'conditions'=>array('LineSensor.sensor_id = Sensor.id')),
            'Line'=>array('foreignKey'=>false, 'conditions'=>array('LineSensor.line_id = Line.id')),
        )));
        $items = $sensorModel->find('all',array(
            'conditions'=>array('Sensor.is_partial'=>0, 'Line.id IS NOT NULL'),
            'order'=>array('Line.name','Sensor.name')
        ));
        $options = array();
        $options[0] = __('-- Nepasirinktas --');
        foreach ($items as $li) {
            $options[$li['Sensor']['id']] = $li['Sensor']['name'].(!empty($li['Line'])?' ('.$li['Line']['name'].')':'');
        }
        return $options;
    }
    
    public function WorkCenter_BeforeAppOrderSave_Hook(&$appOrder, &$fPlan){
        //Jei neatitinka buvusio ir dabar renkamo plano keli prarametrai, prie esamo plano pridedame pasiruosimo laiko konstantas
        $appOrderModel = ClassRegistry::init('ApprovedOrder');
        $previousApprovedOrder = $appOrderModel->withRefs()->find('first',array(
            'conditions'=>array(
                'ApprovedOrder.sensor_id'=>$appOrder['ApprovedOrder']['sensor_id'],
                'ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE,
            ),
            'order'=>array('ApprovedOrder.end'=>'DESC')
        ));
        if(!empty($previousApprovedOrder)){
            $prevPlanSerializedData = json_decode($previousApprovedOrder['Plan']['serialized_data'],true);
            $currentPlanSerializedData = json_decode($fPlan['Plan']['serialized_data'],true);
            $preparationAdded = 0;
            if(isset($prevPlanSerializedData['paper_width']) && isset($currentPlanSerializedData['paper_width']) && isset($currentPlanSerializedData['paper_width_const']) && $prevPlanSerializedData['paper_width'] > $currentPlanSerializedData['paper_width']){
                $preparationAdded += $currentPlanSerializedData['paper_width_const'];
            }
            if(isset($prevPlanSerializedData['publication_format']) && isset($currentPlanSerializedData['publication_format']) && isset($currentPlanSerializedData['publication_format_const']) && trim($prevPlanSerializedData['publication_format']) != trim($currentPlanSerializedData['publication_format'])){
                $preparationAdded += $currentPlanSerializedData['publication_format_const'];
            }
            if($preparationAdded > 0){
                $planModel = ClassRegistry::init('Plan');
                $planModel->updateAll(
                    array('Plan.preparation_time'=>$fPlan['Plan']['preparation_time']+$preparationAdded),
                    array('Plan.id'=>$fPlan['Plan']['id'])
                );
            }
        }
    }

    public function Record_getLastMinuteSpeed_Hook(&$fSensor,&$currentPlan){   
        /*$time = time();
        $quantity = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        $recordModel = ClassRegistry::init('Record');
        $timeFrom = $time-120-3600+1;
        $speed = $recordModel->find('first', array(
            'fields'=>array(
                'SUM('.$quantity.') AS last_houre_quantities',
                //'SUMx(IF(Record.created >= \''.date('Y-m-d H:i:s', $timeFrom).'\','.$quantity.',0)) AS last_houre_quantities',
            ),'recursive'=>-1,
            'conditions'=>array(
                'Record.created <='=>date('Y-m-d H:i:s', $time-120), 
                'Record.created >'=>date('Y-m-d H:i:s', $timeFrom), 
                'Record.sensor_id'=>$fSensor['Sensor']['id']
            )
        ));
        $speed = isset($speed[0]['last_houre_quantities'])?$speed[0]['last_houre_quantities']:0;*/
        $time = time();
        $recordModel = ClassRegistry::init('Record');
        $quantity = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        $speed = $recordModel->find('first', array(
            'fields'=>array(
                'SUM(IF(Record.created >= \''.date('Y-m-d H:i:s', $time-$recordModel::RECORD_AGE-59).'\','.$quantity.',0)) AS last_minute_quantities',
            ),'recursive'=>-1,
            'conditions'=>array(
                'Record.created <='=>date('Y-m-d H:i:s', $time-$recordModel::RECORD_AGE), 
                'Record.created >'=>date('Y-m-d H:i:s', $time-300), 
                'Record.sensor_id'=>$fSensor['Sensor']['id'],
                'Record.approved_order_id'=>isset($currentPlan['ApprovedOrder']['id']) && $currentPlan['ApprovedOrder']['id'] > 0? $currentPlan['ApprovedOrder']['id']:null
            )
        ));
        $speed = isset($speed[0]['last_minute_quantities'])?$speed[0]['last_minute_quantities']:0;
        //kai sensorius yra ritininio linijos pritaisymo procesa pradeti kai greitis pasiekia 83.33vnt/min
        if($speed >= 83.33 && self::RITININIAI_IRENGINIAI_LINE_ID == $fSensor['Line']['id'] && $currentPlan && !isset($currentPlan['ApprovedOrder']['additional_data']->start_attaching)){//jei jutiklis priklauso ruloniniams ir dar nepaspaustas mygtukas "Pritaisymas"
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $logModel = ClassRegistry::init('Log');
            if(!is_object($currentPlan['ApprovedOrder']['additional_data'])){
                $currentPlan['ApprovedOrder']['additional_data'] = (object)$currentPlan['ApprovedOrder']['additional_data'];
            }
            $currentPlan['ApprovedOrder']['additional_data']->start_attaching = date('Y-m-d H:i:s');
            $approvedOrderModel->id = $currentPlan['ApprovedOrder']['id'];
            $logModel->write('Sensorius pasieke 83.33vnt/min greiti, todel pradedamas automatinis pritaisymas: '.json_encode($currentPlan['ApprovedOrder']['additional_data']), $fSensor);
            $approvedOrderModel->save(array('additional_data'=>json_encode($currentPlan['ApprovedOrder']['additional_data'])));
        }
        return $speed*60;
    }
}
    