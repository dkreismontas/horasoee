<h1><?php echo __('Planų importavimas iš xls/xlsx dokumento'); ?></h1>
<!--<ul style="padding-left: 20px;">-->
<!--	<li>--><?php //echo __('Atributo kodas stulpelyje C'); ?><!--</li>-->
<!--	<li>--><?php //echo __('Proceso kodas stulpelyje D'); ?><!--</li>-->
<!--	<li>--><?php //echo __('Paleidimo laikas stulpelyje F'); ?><!--</li>-->
<!--	<li>--><?php //echo __('Kiekvienas gaminys vedamas naujame stulpelyje pradedant nuo stulpelio G'); ?><!--</li>-->
<!--	<li>--><?php //echo __('Susikertančiose lastelėse vedamas gamybos laikas minutėmis vienam vienetui pagaminti'); ?><!--</li>-->
<!--</ul>-->
<form style="display:inline-block;" id="uploadCsv" enctype="multipart/form-data">
<input type="hidden" value="0" id="selector" name="selector" />
<input style="display:none;" name="file" type="file" />
<a id="importer" class="btn btn-primary " href="" style="margin-right: 4px;"> <i class="iconsweets-link"></i> &nbsp; <?php echo __('Pasirinkti xls failą'); ?></a>
<a style="display: none;" id="orderCreationButton" class="btn btn-rounded btn-danger" href="" style="margin-right: 4px;"> <i class="iconsweets-link"></i> &nbsp; <?php echo __('Pradėti importavimą'); ?></a>
<a href="<?php echo $this->Html->url(array('controller'=>'','action'=>'files','example-fixed.xlsx')) ?>" class="btn btn-warning"> <i class="iconsweets-megaphone iconsweets-white"></i>&nbsp;<?php echo __('Importo failo pavyzdys'); ?></a>
<?php echo $this->Html->image('./../images/loaders/loader12.gif', array('id' => 'saveLoader', 'style' => 'display: none;')); ?>
</form>

<div id="dataContent" style="position: relative;">
	
</div>
<?php
    $this->Html->script('ImportPlansXls.jquery.dataTables.min',array('inline'=>false));
?>
<script type="text/javascript">
	//Uzsakymu importas
	jQuery('#importer').bind('click',function(){
		jQuery('form#uploadCsv input[type="file"]').trigger('click');
		return false;
	});
	jQuery('form#uploadCsv input[type="file"]').live('change',function(){
		jQuery('#selector').val(0);
		jQuery('form#uploadCsv').trigger('submit');
	});
	
	//uzkrauna pradinius failo duomenis i ekrana perziurai
	jQuery('form#uploadCsv').live('submit',function(){
		jQuery('#saveLoader').show();
		var formData = new FormData(jQuery(this)[0]);
		jQuery.ajax({
			type: 'POST',
			url: '<?php echo Router::url(array('plugin' => 'import_plans_xls','controller' => 'plugins/start_upload'),true); ?>',
			data: formData,
			success: function(data){
				jQuery('#saveLoader').hide();
				if(jQuery('#selector').val()==0){
					jQuery('#dataContent').html( data );
					jQuery('#orderCreationButton').show('fast');
					jQuery('#dyntable2').dataTable({
						"sPaginationType": "full_numbers",
						//"bScrollInfinite" : true,
						"aaSorting": [],
						"bScrollCollapse" : true,
						"sScrollY" : "800px",
						"iDisplayLength": 100,
						"paging": true,    
						"scrollY": 400,
						"oLanguage":{
							'sLengthMenu':'<?php echo __('Rodyti');?> _MENU_ <?php echo __('įrašų'); ?>',
							'sSearch':'<?php echo __('Paieška');?>',
			                'sInfo':"<?php echo __('Rodoma nuo _START_ iki _END_ iš _TOTAL_ įrašų');?>",
			                'sInfoFiltered': '<?php echo __('(atfiltruota iš _MAX_ įrašų)');?>',
			                'sInfoEmpty':"",
			                'sEmptyTable': '<?php echo __('Lentelė tuščia');?>',
			                'oPaginate':{
			                	'sFirst': '<?php echo __('Pirmas');?>',
			                	'sPrevious': '<?php echo __('Ankstesnis');?>',
			                	'sNext': '<?php echo __('Kitas');?>',
			                	'sLast': '<?php echo __('Paskutinis');?>'
			                }
			                
						}
					});
				}else{ //informacija apie importavimo statusa
					jQuery('#orderCreationButton').hide();
					alert('<?php echo __('Gaminių importavimas atliktas'); ?>');
				}
			},
			cache: false,
            contentType: false,
            processData: false
		});
		return false;
	});
	
	//pradedamas tikrasis uzsakymu importavimas
	jQuery('#orderCreationButton').live('click',function(e){
		e.preventDefault();
		jQuery('#selector').val(1);
		jQuery('form#uploadCsv').trigger('submit');
	});
</script>
