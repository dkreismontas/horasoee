<?php //fb($data); ?>
<table id="dyntable2" class="table table-bordered" >
	<thead><tr>
		<?php
		$headers = $data[0]; 
		foreach(array_values($headers) as $key => $header){
			?><th><?php //echo $header; 
			if(isset($dataColumnsPos[$key])){
				$realName = current($dataColumnsPos[$key]);
			}elseif($key >= sizeof($dataColumnsPos)){
				$realName = $header.'<br/>{'.__('parametras').'}';
			}else $realName = '';
			echo trim($realName)?'<span style="color:#ff0000">'.$realName.'</span>':$header ?>
			</th><?php
		}
		?>
	</tr></thead>
	<tbody>
		<?php 
		foreach($data as $key => $tbodyColumns){
			if($key==0) continue;
			?><tr>
				<?php
				foreach($headers as $columnKey => $t){
					if(!isset($tbodyColumns[$columnKey])){
						?><td></td><?php
					}else{
						?><td><?php echo $tbodyColumns[$columnKey]; ?></td><?php
					}
				}
				?>
			</tr><?php
		}
		?>
	</tbody>
</table>
