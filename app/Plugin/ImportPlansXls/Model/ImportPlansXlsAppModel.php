<?php
App::uses('AppModel', 'Model');

class ImportPlansXlsAppModel extends AppModel {
    
    public static $navigationAdditions = array( );
    
    public static function loadNavigationAdditions() {
        self::$navigationAdditions[2] =
            array(
                'name'=>__('Planų importavimas iš xls failo'),
                'route'=>Router::url(array('plugin' => 'import_plans_xls','controller' => 'plugins', 'action' => 'select_settings')),
                'selectedName'=>'workslist',
                'icon'=>'iconfa-list-alt',
            );
    }
}
