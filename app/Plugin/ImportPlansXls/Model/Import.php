<?php

class Import extends AppModel{
    var $name = __CLASS__;
    public $fileName;
    public $rawData;
    public $fileBasePath;
    public $dataColumnsPos = array();
    public $sensorsListPsIdToId = array();
    public $dataPreview = false;
    
    public function __construct(){
        $this->dataColumnsPos = array(
            0 => array('sensor_id'=>__('Darbo centro ID')),
        );
    }

    public function readXlsFile()
    {
        $this->fileName = basename($_FILES["file"]["name"]);
        $this->fileBasePath = $_SERVER['DOCUMENT_ROOT'] . '/app/Plugin/ImportPlansXls/files' . '/' . $this->fileName;
        $fileType = pathinfo($this->fileName, PATHINFO_EXTENSION);
        if (!in_array($fileType, array('xls', 'xlsx'))) {
            die(__('Netinkamas failo formatas. '.$fileType));
        }
        if ($_FILES['file']['error'] == UPLOAD_ERR_OK             //checks for errors
            && is_uploaded_file($_FILES['file']['tmp_name'])
        ) { //checks that file is uploaded
            if (move_uploaded_file($_FILES["file"]["tmp_name"], $this->fileBasePath)) {
                try {
                    set_time_limit(200);
                    App::import('Vendor', 'mexel/PHPExcel');
                    App::import('Vendor', 'mexel/PHPExcel/IOFactory');
                    $objPHPExcel = PHPExcel_IOFactory::createReader('Excel2007');
                    try {
                        $objPHPExcel = $objPHPExcel->load($this->fileBasePath);
                    } catch (Exception $e) {
                        die('Error loading file "' . pathinfo($this->fileName, PATHINFO_BASENAME) . '": ' . $e->getMessage());
                    }
                    $sheetData = $objPHPExcel->getSheet(0);
                    $this->rawData = $this->getDataSheet($sheetData);
                } catch (Exception $e) {
                    return false;
                }
            }
        }
        @unlink($this->fileBasePath);
    }

    

    public function getDataSheet($sheet){
        $fullData = array();
        $highestRow = $this->highestRow($sheet, 1);
        $highestColumn = $this->highestColumn($sheet, 1, 0);
        $i = 1;
        $currentRow = 1;
        $colsPosTitles = array();
        foreach($this->dataColumnsPos as $key => $col){
            $colsPosTitles[$key] = key($col);
        }
        while ($i < $highestRow) { //  skaitome tol, kol rasime tuscia eilute
            $i += 1;
            $data = $sheet->rangeToArray('A' . $currentRow . ':' . $highestColumn . $currentRow, '0', TRUE, FALSE);
            //$data = array_filter($data[0], 'strlen');
            $data = $data[0];
            if(sizeof($data)>sizeof($colsPosTitles)){
                $colsCountDiff = sizeof($data) - sizeof($colsPosTitles);
                for($c = 1; $c <= $colsCountDiff; $c++){
                    $colsPosTitles[] = $data[sizeof($colsPosTitles)];//array($data[sizeof($colsPosTitles)]=>$data[sizeof($colsPosTitles)].'{'.__('Papildomas parametras %d',$c).'}');
                }
            }
            $data = array_combine($colsPosTitles, $data); 
            $data[key($data)] = $data['sensor_id'] = isset($this->sensorsListPsIdToId[$data[key($data)]])?$this->sensorsListPsIdToId[$data[key($data)]]:0;//exselyje esanti jutiklio ps_id konvertuojame i jutiklio id
            $currentRow++;
            if (empty($data)) break;
            $fullData[] = $data;
        }
        //pagal imone pasikeisti stulpeliu reiksmes, prideti id jei reikia atnaujinti plano duomenis. Kadangi kiekviena imone gali reiklaauti savaip atnaujinima daryti, nieko nekeiciant visi irasai po kiekvieno ikelimo bus kuriami is naujo
        $parameters = array(&$fullData, $this->dataPreview);
        $this->callPluginFunction('ImportPlansXlsPlugin_prepareXlsData_Hook', $parameters, Configure::read('companyTitle'));
        return $fullData;
    }

    

    public function highestRow($sheet, $row = 1){
        $currentRow = $row;
        do {
            $currentRow++;
            $index = "A" . $currentRow;
            $content = trim($sheet->getCell($index)->getValue()); 
        } while ($content != "");
        return $currentRow;
    }

    public function highestColumn($sheet, $row = 1, $offset = 0){
        $currentColumn = 0;
        do {
            $index = PHPExcel_Cell::stringFromColumnIndex($currentColumn) . ($row); 
            $content = trim($sheet->getCell($index)->getValue());
            $currentColumn++;
        } while ($content != "");
        $currentColumn -= 2;
        return PHPExcel_Cell::stringFromColumnIndex($currentColumn - $offset);
    }

    public function moveDataToDb(){
        array_shift($this->rawData);
        $planModel = ClassRegistry::init('Plan');
        $planModel->virtualFields = array('unique_plan'=>'CONCAT(Plan.sensor_id,\'_\',Plan.mo_number)');
        foreach($this->rawData as $plan){
            $planExists = $planModel->find('first', array(
                'fields'=>array('Plan.id'),
                'conditions'=>array('Plan.sensor_id'=>$plan['sensor_id'], 'Plan.mo_number'=>$plan['mo_number'])
            ));
            if(!empty($planExists)){
                $plan['id'] = $planExists['Plan']['id'];
            }else{
                $planModel->create();
            }
            array_walk($plan, function(&$data){
                if(preg_match('/^[\d\.\,]+$/',$data)){
                    $data = str_replace(',','.',$data);
                }
            });
            $planModel->save($plan);
            unset($planExists);
        }
        die();
    }

}
