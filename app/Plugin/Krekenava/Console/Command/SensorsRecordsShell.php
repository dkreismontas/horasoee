<?php
// /var/www/dev1.horasmpm.eu/app/Console/cake Grafija.test display 192.168.1:1 55
class SensorsRecordsShell extends Shell {
	
	public $uses = array('Shift','FoundProblem','Sensor','Log','Record','Problem','ShiftTimeProblemExclusion','SMS');
    
    function save_sensors_data(){
    	ob_start();
		try{
	        $adressList = array(
                'http://88.119.157.142:5580/data',
                'http://88.119.157.142:5680/data',
                'http://88.119.157.142:5780/data',
	        );
            $db = ConnectionManager::getDataSource('default');
            if(isset($db->config['plugin']) && !Configure::read('companyTitle')){
                Configure::write('companyTitle', $db->config['plugin']);
            }
	        $ip = $port = $uniqueString = '';
			$ipAndPort = $this->args[0]??'';
	        if(trim($ipAndPort)){
	            $ipAndPort = explode(':', $ipAndPort);
	            if(sizeof($ipAndPort) == 2){
	                list($ip, $port) = $ipAndPort;
	                $uniqueString = str_replace('.','_',$ip).'|'.$port;
	            }
	        }else{ $ipAndPort = array(); }
            $this->Sensor->informAboutProblems(implode(':',$ipAndPort));
	        $mailOfErrors = array();
	        $dataPushStartMarkerPath = __dir__.'/../../webroot/files/data_push_start_'.$uniqueString.'.txt';
	        if(file_exists($dataPushStartMarkerPath)){
	            if(time() - filemtime($dataPushStartMarkerPath) < Configure::read('recordsCycle') * 3) {
	                //$this->Sensor->informAboutProblems();
	                die('Vis dar vyksta duomenu irasymas is praejusios sesijos');
	            }elseif($this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors) === false){
	                $this->Sensor->checkRecordsIsPushing($mailOfErrors); die();
	            }
	        }
            $fh = fopen($dataPushStartMarkerPath, 'a') or die("negalima irasyti failo");
			$this->Sensor->virtualFields = array('name'=>'CONCAT(pin_name,\'_\',port)');
			$sensorsConditions = array('Sensor.pin_name <>'=>'');
			if(!empty($ipAndPort)){
				$sensorsConditions['Sensor.port'] = implode(':',$ipAndPort); 
			}
			$sensorsListInDB = $this->Sensor->find('all', array('fields'=>array('id', 'name', 'branch_id', 'st_tower'), 'conditions'=>$sensorsConditions));
	        //$sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
	        $branches = array_unique(Set::extract('/Sensor/branch_id', $sensorsListInDB));
	        $currentShifts = array();
	        foreach($branches as $branchId){
	            //jei reikia importuojame pamainas
	            $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            if(!$currentShifts[$branchId]){
                    App::uses('CakeRequest', 'Network');
                    App::uses('CakeResponse', 'Network');
                    App::uses('Controller', 'Controller');
                    App::uses('AppController', 'Controller');
                    App::import('Controller', 'Cron');
	                $CronController = new CronController;
	                $CronController->generateShifts();
	                $currentShifts[$branchId] = $this->Shift->findCurrent($branchId, new DateTime());
	            }
	        }
	        if(!empty($emptyShifts = array_filter($currentShifts, function($shift){ return empty($shift); }))){
                $mailOfErrors[] = 'Padaliniai, kuriems nepavyko sukurti pamainų: '.implode(',', array_keys($emptyShifts));
            }

			$saveSensors = Hash::combine($sensorsListInDB,'{n}.Sensor.id', '{n}.Sensor.name');
			$this->Sensor->virtualFields = array();
	        if(trim($uniqueString)){
	            $adressList = array_filter($adressList, function($address)use($ip,$port){
	                return preg_match('/'.$ip.':'.$port.'\//', $address);
	            });
	        }
            $periodsFound = false;
	        foreach($adressList as $address){
	            if(!preg_match('/\d+[^\/]+/', $address, $ipAndPort)){continue; }
	            $addessIpAndPort = current($ipAndPort);
	            $ch = curl_init($address);
	            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
	            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
	            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
	                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW'))
	            ));
	            $data = curl_exec($ch);
	            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
	            if(curl_errno($ch) == 0 AND $http == 200) {
	                $data = json_decode($data);
	            }else{
	                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $address);
	                continue;
	            }
				if(!isset($data->periods)){ continue; }
	            foreach($data->periods as $record){
                    $periodsFound = true;
	                foreach($saveSensors as $sensorId => $title){
	                    list($sensorPinName, $sensorPortAndIp) = explode('_',$title);
	                    if(trim($sensorPortAndIp) != trim($addessIpAndPort)) continue;
	                    if(!isset($record->$sensorPinName)) continue;
						if($record->time_to < strtotime('-1 year')){ $mailOfErrors[] = sprintf('Jutikliui ID %s paduodama neteisinga data %s. Irasas praleidziamas.',$sensorId, date('Y-m-d H:i:s', $record->time_to)); continue; }
	                    $quantity = $record->$sensorPinName;
	                    try{
	                        $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
	                        $queryLog = json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n";
	                        fwrite($fh, $queryLog);
	                    }catch(Exception $e){
	                        $mailOfErrors[] = __('Nepavyko įrašyti gautų duomenų į laikinąjį failą: '.$queryLog);
	                    }
	                }
	            }
	        }
	        fclose($fh);
	        $this->Sensor->executeRecordsQueries($dataPushStartMarkerPath, $mailOfErrors);
	        $dataNotSendingMarkerPath = __dir__.'/../../webroot/files/last_push_check.txt';
	        if(!empty($mailOfErrors)){
	             $this->Sensor->checkRecordsIsPushing($mailOfErrors);
	        }elseif(file_exists($dataNotSendingMarkerPath) && $periodsFound){
	            unlink($dataNotSendingMarkerPath); 
	        }
	        //sutvarkome atsiustu irasu duomenis per darbo centra
	        $this->Sensor->moveOldRecords();
	        $markerPath = __dir__.'/../../webroot/files/last_sensors_update_'.$uniqueString.'.txt';
	        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
	            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
	            fclose($fh);
	            $sensorsWithTowers = array_filter($sensorsListInDB, function($sensor){
	                return trim($sensor['Sensor']['st_tower']);
                });
                $this->passDataToBlinkers(Set::extract('/Sensor/id', $sensorsWithTowers));
	            $this->FoundProblem->manipulate(array_keys($saveSensors));
	            $this->Record->calculateDashboardsOEE($currentShifts, $sensorsListInDB);
	            unlink($markerPath);
	        }
		}catch(Exception $e){
			$systemWarnings = $e->getMessage();
		}
		$systemWarnings = isset($systemWarnings)?$systemWarnings."\n".ob_get_clean():ob_get_clean();
        if(trim($systemWarnings)){
        	pr($systemWarnings);
            $this->Log->sendSysMail('Duomenu irasymo metu sistemoje gauti pranesimai',$systemWarnings);
        }
        die();
    }

    public function passDataToBlinkers(Array $sensorsIds, $returnJSON = false){
        if(empty($sensorsIds)){ return null; }
        $maxCreatedRecord = $this->Record->find('first', array(
            'fields'=>array('Record.created'),
            'conditions'=>array('Record.sensor_id'=>$sensorsIds),
            'order'=>array('Record.created DESC')
        ));
        $this->Record->bindModel(array('belongsTo'=>array('Sensor','Plan',
            'FoundProblem'=>array('foreignKey'=>false, 'conditions'=>array('Record.sensor_id = FoundProblem.sensor_id AND FoundProblem.state = '.FoundProblem::STATE_IN_PROGRESS)),
        )));
        $sensorsData = $this->Record->find('all', array(
            'fields' => array(
                'SUM(IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity)) AS quantity',
                'TRIM(Sensor.id) AS sensor_id',
                'TRIM(Sensor.pin_name) AS pin_name',
                'TRIM(Sensor.st_tower) AS st_tower',
                'TRIM(Sensor.records_count_to_start_problem) AS records_count_to_start_problem',
                'TRIM(Sensor.port) AS address',
                'TRIM(FoundProblem.problem_id) AS problem_id',
                'TRIM(Plan.step / 60) AS step_in_minute',
                'TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end) AS problem_duration'
            ), 'recursive' => 1,
            'conditions' => array(
                'Sensor.id'=> $sensorsIds,
                'Sensor.st_tower <>'=> '',
                'Record.created >' => date('Y-m-d H:i:s', strtotime($maxCreatedRecord['Record']['created']) - 60),
            ),'group'=>array('Sensor.id')
        ));
        $dataToBlinkers = array();
        $exclusionList = $this->ShiftTimeProblemExclusion->find('list',array('fields'=>array('problem_id')));
        if(empty($sensorsData)){ return null; }
        foreach($sensorsData as $data){ $data = $data[0];
            list($ip,$port) = explode(':',$data['address']);
            switch($data['problem_id']){
                case Problem::ID_WORK:
                    $indicators = array('green'=>1, 'yellow'=>$data['step_in_minute']>$data['quantity']?1:0);
                break;
                case Problem::ID_NEUTRAL:
                    $indicators = array('yellow'=>1);
                    break;
                case Problem::ID_NOT_DEFINED:
                    if($data['problem_duration'] < $data['records_count_to_start_problem']*Configure::read('recordsCycle')){
                        $indicators = array('green'=>1, 'yellow'=>$data['step_in_minute']>$data['quantity']?1:0);
                    }elseif($data['problem_duration'] < 300){
                        $indicators = array('red'=>1);
                    }else{
                        $indicators = array('red'=>1, 'alarm'=>1);
                    }
                break;
                default:
                    if(in_array($data['problem_id'], $exclusionList)){
                        $indicators = array('yellow'=>1);
                    }else{ //found_problems.problem_id > 3
                        $indicators = array('red'=>1);
                    }

            }
            $indicators = array_merge(array('green'=>0,'red'=>0,'yellow'=>0,'alarm'=>0), $indicators);
            $dataToBlinkers[$ip.':'.$port][$data['st_tower']] = $indicators;
        }
        foreach($dataToBlinkers as $ip_port => $dataSendTo){
            $ch = curl_init('http://'.$ip_port.'/outputs');
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("horas:".Configure::read('PHP_AUTH_PW')),
                'Content-Type: application/json'
            ));
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($dataSendTo));
            $result     = curl_exec ($ch);
            $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) > 0) {
                //pr('Nesekmingas duomenu siuntimas i svyturelius Krekenava. Klaidos nr.:'.curl_errno($ch));
                //$this->SMS->send('Nesekmingas duomenu siuntimas i svyturelius Krekenava','862412214','OEE','eventas',160,true);
            }
        }
        if($returnJSON){
            pr($dataToBlinkers);
        }
    }

//	private function calcOee(Array $currentShifts, Array $sensorsListInDB){
//        $prevShifts = array();
//        foreach($currentShifts as $branchId => $shift){
//        	if(time() - strtotime($shift['Shift']['start']) > Configure::read('recordsCycle')*2){ continue; }
//        	$prevShift = $this->Shift->getPrev($shift['Shift']['start'], $branchId);
//			if(empty($prevShift)){ continue; }
//			$prevShifts[$prevShift['Shift']['id']] = $prevShift;
//        }
//		if(empty($prevShifts)){ return null; }
//		$prevShiftsIds = array_keys($prevShifts);
//		$oeeCalculatedShiftsIds = $this->DashboardsCalculation->find('list', array(
//			'fields'=>array('DashboardsCalculation.shift_id','DashboardsCalculation.shift_id'),
//			'conditions'=>array('DashboardsCalculation.shift_id'=>$prevShiftsIds),
//			'group'=>array('DashboardsCalculation.shift_id')
//		));
//		$unCalculatedShiftsIds = array_diff($prevShiftsIds, $oeeCalculatedShiftsIds);
//		if(empty($unCalculatedShiftsIds)){ return null; }
//
//        set_time_limit(2000);
//        App::import('Console/Command', 'AppShell');
//        App::import('Console/Command', 'OeeShell');
//        $shell = new OeeShell();
//
//		foreach($unCalculatedShiftsIds as $shiftId){
//			$this->Log->write('Apskaiciuojamas OEE praejusiai pamainai. Pamainos ID: '.$shiftId);
//            $shift = $prevShifts[$shiftId];
//			$shell->shift = $shift;
//			$sensorsIdsInCurrentShift = Set::extract('/Sensor/id', array_filter($sensorsListInDB, function($sensor)use($shift){
//				return $sensor['Sensor']['branch_id'] == $shift['Shift']['branch_id'];
//			}));
//            $shell->start_sensors_calculation($sensorsIdsInCurrentShift,false);
//		}
//    }
	
	
}