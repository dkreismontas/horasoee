<?php
class Krekenava extends AppModel{
     
     // public function Record_calculateOee_Hook(&$oeeParams, $data){
        // $oeeParams['operational_factor'] = 1;
        // $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        // $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
     // }
     
     // public function WorkCenter_BuildStep_Hook(&$fPlan, $product, $fSensor){
        // $fPlan['step'] = $product['Product']['formu_greitis'];
     // }
    public function App_beforeBeforeFilter_Hook(&$appController){
        $allowAddress = preg_match('/.*slides\/show\/3$/i',$_SERVER['REQUEST_URI']);
        if(in_array($_SERVER['REMOTE_ADDR'], Configure::read('ServerIP')) && ($allowAddress || $appController->Session->read('allowAccessNoLogin'))){
            $userModel = ClassRegistry::init('User');
            $user = $userModel->find('first', array('conditions'=>array('User.id'=>User::ID_SUPER_ADMIN)));
            $user['User']['id'] = 0;
            $user['User']['group_id'] = 0;
            $userData = $user['User']; unset($user['User']); $user = $userData + $user;
            $appController::$user = $user;
            $appController->Auth->allow(array(
                'index','updateStatus','show'
            ));
            $appController->Session->write('allowAccessNoLogin', 1);
        }
    }
     
     
     
}
    