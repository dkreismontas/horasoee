<?php
ob_end_clean();
error_reporting(0);
$centerAlignStyleArray = array(
    'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
    ),
    'font' => array(
        'size' => 11,
        'name' => 'Times New Roman',
    ),
);
$borderStyleArray = array(
    'borders' => array(
        'allborders' => array(
            'style' => PHPExcel_Style_Border::BORDER_THIN
        )
    )
);
$headerStyleArray2 = array(
    'font' => array(
        'size' => 12,
        'name' => 'Times New Roman',
        'bold'=>true
    ),
    'alignment' => array(
        //'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
        'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
        'wrap'=>true,
        'rotation'   => 0,
    ),
);

$activeSheet = $objPHPExcel->createSheet(0);
$activeSheet->setTitle(__('Užsakymų ataskaita'), false);
$colNr = 0; $rowNr = 1;
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminio pavadinimas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gaminio kodas'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pradžia'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo pabaiga'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Užsakymo vykdymo trukmė (h)'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Gamybos trukmė (h)'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Pagamintas kiekis (vnt)'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Faktinis greitis (vnt/h)'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->setCellValueByColumnAndRow($colNr, $rowNr, __('Teorinis greitis (vnt/h)'))->getStyle(cts($colNr++, $rowNr))->applyFromArray($headerStyleArray2);
$activeSheet->getRowDimension(1)->setRowHeight(30);
$rowNr = 2;
foreach($orders as $order){
    $colNr = 0;
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['production_name']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['production_code']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['created']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['end']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order[0]['order_duration']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order[0]['work_duration']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['ApprovedOrder']['quantity']);
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, bcdiv($order['ApprovedOrder']['quantity'],$order[0]['work_duration'],2));
    $activeSheet->setCellValueByColumnAndRow($colNr++, $rowNr, $order['Plan']['step']);

    $rowNr++;
}
for ($col = 0; $col != $colNr; $col++){
    $activeSheet->getColumnDimension(nts($col))->setWidth(20);
}


$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
if(isset($this->request->params['named']['exportToFile'])){
    $objWriter->save(WWW_ROOT.'files/'.$this->request->params['named']['exportToFile']);
}else {
    header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
    header("Cache-Control: no-store, no-cache, must-revalidate");
    header("Cache-Control: post-check=0, pre-check=0", false);
    header("Pragma: no-cache");
    header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
    header('Content-Disposition: attachment;filename="' . __('Užsakymų ataskaita') .' '.$date. '.xlsx"');
    $objWriter->save('php://output');
    die();
}
function nts($col) {// number to letter cordinates
    return PHPExcel_Cell::stringFromColumnIndex($col);
}
function cts($row, $col) {// number cordinates to letter
    return nts($row) . $col;
}
