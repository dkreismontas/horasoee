<?php
App::uses('Problem', 'Model');
class ReportsController extends AdaxAppController{

    public $uses = array('FoundProblem');

    public function generate_orders_report(){
        if(empty($this->request->data['sensors'])) {
            $this->Session->setFlash(__('Parinkite bent vieną darbo centrą'));
            $this->redirect($_SERVER['HTTP_REFERER']);
        }
        if(isset($this->request->data['time_pattern']) && $this->request->data['time_pattern'] >= 0 && is_numeric($this->request->data['time_pattern'])){
            list($date_start,$date_end) = $this->Help->setStartEndByPattern($this->request->data);
        }else{
            if(isset($this->request->data['start_end_date'])){
                list($this->request->data['start_date'], $this->request->data['end_date']) = explode(' ~ ',$this->request->data['start_end_date']);
            }
            $date_start = $this->request->data['start_date'];
            $date_end = $this->request->data['end_date'];
        }
        App::import("Vendor", "mexel/PHPExcel");
        $objPHPExcel = new PHPExcel();
        $queryParams = array(
            'fields'=>array(
               'ROUND(SUM(IF(FoundProblem.problem_id = '.Problem::ID_WORK.', TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end), 0))/3600,2) AS work_duration',
               'ROUND(TIMESTAMPDIFF(SECOND, ApprovedOrder.created, ApprovedOrder.end)/3600,2) AS order_duration',
               'ApprovedOrder.created',
               'ApprovedOrder.end',
               'ApprovedOrder.quantity',
               'Plan.production_name',
               'Plan.production_code',
               'Plan.step',
            ),'conditions'=> array(
                'ApprovedOrder.created <=' => $date_end,
                'ApprovedOrder.end >=' => $date_start,
                'ApprovedOrder.sensor_id' => $this->request->data['sensors'],
            ),'group'=>array(
                'ApprovedOrder.id'
            ),'order'=>array('ApprovedOrder.created')
        );
        $this->FoundProblem->bindModel(array('belongsTo'=>array('Plan','ApprovedOrder')));
        $this->set(array(
            'orders'=>$this->FoundProblem->find('all', $queryParams),
            'objPHPExcel'=>$objPHPExcel,
        ));
    }

}