<?php

App::uses('AppModel', 'Model');
App::uses('User', 'Model');
App::uses('Perm', 'Model');

/**
 */
class UserPerm extends AppModel {
	const NAME = __CLASS__;
	
	/**
	 * Create and save new item to database
	 * @param int $userId
	 * @param int $permId
	 * @return array new record
	 */
	public function newItem($userId, $permId) {
		$this->create();
		if (!$userId || !intval($userId)) throw new ErrorException("Bad user ID value '".$userId."'.");
		if (!$permId || !intval($permId)) throw new ErrorException("Bad permission ID value '".$permId."'.");
		if ($this->save($arr = array(
			'user_id' => intval($userId),
			'perm_id' => intval($permId)
		))) {
			$arr['id'] = intval($this->id);
			return array(self::NAME => $arr);
		}
		return null;
	}
	
	/** Get indexed (by perm key) user permissions */
	public function getPerms($userId) {
		if (!$userId || !intval($userId)) throw new ErrorException("Bad user ID value '".$userId."'.");
		$list = $this->withRefs()->find('all', array(
			'conditions' => array(
				self::NAME.'.user_id' => intval($userId)
			)
		));
		$ilist = array();
		foreach ($list as $perm) {
			$ilist[$perm[Perm::NAME]['key']] = $perm;
		}
		return $ilist;
	}
	
	/** Remove all user permissions */
	public function removePerms($userId) {
		if (!$userId || !intval($userId)) throw new ErrorException("Bad user ID value '".$userId."'.");
		$this->query("DELETE FROM `$this->table` WHERE `user_id`=:user_id", array(':user_id' => intval($userId)), false);
	}
	
	/**
	 * Test if user has permission (slow - requests database)
	 * @param string $key
	 * @param int $userId
	 * @return boolean
	 */
	public function hasPermission($key, $userId) {
		if (!$key || !$userId) return false;
		$list = $this->withRefs()->find('first', array(
			'conditions' => array(
				Perm::NAME.'.key' => $key,
				self::NAME.'.user_id' => intval($userId)
			),
			'limit' => 1
		));
		
		return !empty($list);
	}
	
	/** @return Loss */
	public function withRefs() {
		$this->bindModel(array(
			'belongsTo' => array(
				Perm::NAME => array(
					'className' => Perm::NAME,
					'foreignKey' => 'perm_id'
				)
			)
		));
		return $this;
	}
	
}
