<?php

App::uses('AppModel', 'Model');
App::uses('Problem', 'Model');
App::uses('DiffCard', 'Model');
App::uses('Sensor', 'Model');
App::uses('Plan', 'Model');
App::uses('ApprovedOrder', 'Model');

/**
 */
class FoundProblem extends AppModel {

    const NAME = __CLASS__;

    const STATE_IN_PROGRESS = 1;
    const STATE_COMPLETE = 2;
    const STATE_UNKNOWN = 3;

    /**
     * Create and save new item to database
     * @param DateTime $start
     * @param DateTime $end
     * @param int $problemId
     * @param int $sensorId
     * @param int $shiftId
     * @param int $state
     * @param int $diffCardId
     * @param boolean $whileWorking
     * @param int $planId
     * @return array new record
     */
    public function newItem(DateTime $start, DateTime $end, $problemId, $sensorId, $shiftId, $state = self::STATE_IN_PROGRESS, $diffCardId = null, $whileWorking = false, $planId = null, $dataForJson = array(), $quantity=0, $userId=0) {
        if ($problemId == 1 && $whileWorking == true) {
            $problemId = 3; //nepazymeta problema
        }
		$logModel = ClassRegistry::init('Log');
        $problemModel = ClassRegistry::init('Problem');
        if(!$userId){
            $userModel = ClassRegistry::init('User');
            $activeOperator = $userModel->getActiveUser($sensorId);
            if(!empty($activeOperator)){
                $userId = $activeOperator['User']['id'];
            }
        }
		$userId = $userId > 0?$userId:$logModel->user->id;
        $this->create();

        if (!$problemId || !intval($problemId)) throw new ErrorException("Bad problem ID value '" . $problemId . "'.");
        if (!$sensorId || !intval($sensorId)) throw new ErrorException("Bad sensor ID value '" . $sensorId . "'.");
        if (!$shiftId || !intval($shiftId)) throw new ErrorException("Bad shift ID value '" . $shiftId . "'.");
        $arr = array(
            'start'         => $start->format('Y-m-d H:i:s'),
            'end'           => $end->format('Y-m-d H:i:s'),
            'problem_id'    => intval($problemId),
            'sensor_id'     => intval($sensorId),
            'shift_id'      => intval($shiftId),
            'plan_id'       => ($planId ? intval($planId) : null),
            'state'         => intval($state),
            'diff_card_id'  => (($diffCardId && intval($diffCardId)) ? intval($diffCardId) : null),
            'while_working' => ($whileWorking ? 1 : 0),
            'json'          => is_array($dataForJson)?json_encode($dataForJson):$dataForJson,
            'quantity'      => $quantity,
            'user_id'       => $userId,
            'comments'      => ''
        );
        $fProblem = $problemModel->find('first',array('conditions'=>array('Problem.id'=>$problemId, 'Problem.id >'=>0)));
        if($fProblem['Problem']['transition_problem']){
            $arr['problem_id'] = $arr['super_parent_problem_id'] = Problem::ID_NEUTRAL;
            $arr['transition_problem_id'] = $fProblem['Problem']['id'];
        }else{
            $problemsTreeList = $problemModel->parseThreadedProblemsTitles($problemModel->find('threaded'));
            if(isset($problemsTreeList[$problemId])){
                $arr['super_parent_problem_id'] = current(array_reverse(array_keys($problemsTreeList[$problemId])));
            }
        }
        if ($this->save($arr)) {
            $arr['id'] = intval($this->id);
            return array(self::NAME => $arr);
        }

        return null;
    }

    /**
     *
     * @param int $sensorId
     * @return array
     */
    public function findCurrent($sensorId) {
        $f = $this->find('first', array(
            'conditions' => array(
                self::NAME . '.sensor_id' => $sensorId,
                self::NAME . '.state'     => self::STATE_IN_PROGRESS,
                self::NAME . '.problem_id >='=> 0
            ),
            'order'      => array(self::NAME . '.start DESC')
        ));
        return ($f && !empty($f)) ? $f : null;
    }

    public function findLast($sensorId) {
        $this->bindModel(array('belongsTo'=>array('Problem')));
        $f = $this->find('first', array(
            'conditions' => array(
                self::NAME . '.sensor_id' => $sensorId,
                'Problem.for_speed' => 0,
            ),
            'order'      => array(self::NAME . '.end DESC')
        ));
        return ($f && !empty($f)) ? $f : null;
    }
    public function findLastRealProblem($sensorId) {
        $this->bindModel(array('belongsTo'=>array('Problem')));
        $f = $this->find('first', array(
            'conditions' => array(
                self::NAME . '.sensor_id' => $sensorId,
                self::NAME . '.problem_id >=' => Problem::ID_NOT_DEFINED,
                'Problem.for_speed' => 0,
            ),
            'order'      => array(self::NAME . '.end DESC')
        ));
        return ($f && !empty($f)) ? $f : null;
    }

    /**
     * Find and complete problems in progress
     * @param int $sensorId
     * @param DateTime $completeTime
     */
    public function findAndComplete($fSensor, DateTime $completeTime = null) {
        STATIC $logModel = null;
        if($logModel == null){
            $logModel = ClassRegistry::init('Log');
        }
        if (!isset($fSensor['Sensor']['id'])) throw new ErrorException('Sensor ID is required.');
        $fItems = $this->find('all', array(
            'conditions' => array(
                self::NAME . '.sensor_id' => $fSensor['Sensor']['id'],
                self::NAME . '.state'     => self::STATE_IN_PROGRESS
            ),
            'order'      => array(self::NAME . '.start DESC')
        ));
        $cc = 0;
        foreach ($fItems as $li) {
            if (!$cc && $completeTime) {
                $li[self::NAME]['end'] = $completeTime->format('Y-m-d H:i:s');
            }
            $li[self::NAME]['state'] = self::STATE_COMPLETE;
            $shortProblemId = $duration = 0;
            if(isset($fSensor['Sensor']['short_problem']) && trim($fSensor['Sensor']['short_problem']) && $li[self::NAME]['problem_id'] == self::STATE_UNKNOWN){
                list($shortProblemId, $duration) = explode('_', $fSensor['Sensor']['short_problem']);
                if((int)$shortProblemId > 0 && floatval($duration) > 0){
                    if(strtotime($li[self::NAME]['end']) - strtotime($li[self::NAME]['start']) <= ($duration * 60)){
                        $li[self::NAME]['problem_id'] = $shortProblemId;
                    }
                }
            }
            $this->save($li);
            $logModel->write('Uzdarytos prastovos duomenys: '.json_encode($this->read()), $fSensor);
            $cc ++;
        }
    }

    /** @return FoundProblem */
    public function withRefs() {
        $this->bindModel(array(
            'belongsTo' => array(
                Sensor::NAME => array(
                    'className'  => Sensor::NAME,
                    'foreignKey' => 'sensor_id'
                ),
                Problem::NAME => array(
                    'className'  => Problem::NAME,
                    'foreignKey' => 'problem_id'
                ),
                Plan::NAME => array(
                    'className'  => Plan::NAME,
                    'foreignKey' => 'plan_id'
                ),
                DiffCard::NAME => array(
                    'className'  => DiffCard::NAME,
                    'foreignKey' => false,
                    'conditions' => array(DiffCard::NAME.'.found_problem_id = '.FoundProblem::NAME.'.id AND '.DiffCard::NAME.'.sensor_id = '.FoundProblem::NAME.'.sensor_id')
                ),
                ApprovedOrder::NAME => array(
                    'className'  => ApprovedOrder::NAME,
                    'foreignKey' => false,
                    'conditions' => array(ApprovedOrder::NAME.'.plan_id = '.FoundProblem::NAME.'.plan_id AND '.ApprovedOrder::NAME.'.sensor_id = '.FoundProblem::NAME.'.sensor_id AND ('.FoundProblem::NAME.'.end BETWEEN '.ApprovedOrder::NAME.'.created and '.ApprovedOrder::NAME.'.end OR '.FoundProblem::NAME.'.start BETWEEN '.ApprovedOrder::NAME.'.created and '.ApprovedOrder::NAME.'.end)')
                ),
                'Branch' => array(
                    'foreignKey' => false,
                    'conditions'=>array('Branch.id = Sensor.branch_id')
                ),
                'Factory' => array(
                    'foreignKey' => false,
                    'conditions'=>array('Branch.factory_id = Factory.id')
                ),
                'User' => array(
                    'foreignKey' => 'user_id',
                ),
                'Shift' => array(
                    'foreignKey' => 'shift_id',
                )
            )
        ));
        return $this;
    }

    public function getByMO($mo_number) {
        $this->bindModel(array(
            'belongsTo' => array(
                'Plan' => array(
                    'className'  => 'Plan',
                    'foreignKey' => false,
                    'conditions' => array('FoundProblem.plan_id = Plan.id')
                ),
                'Problem'
            )
        ));
        $found_problems = $this->find('all', array(
            'fields'     => array(
                'Plan.mo_number',
                'FoundProblem.start',
                'FoundProblem.end',
                'SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) as length',
                'FoundProblem.problem_id',
                'FoundProblem.sensor_id',
                'Problem.name'
            ),
            'conditions' => array(
                'Plan.mo_number' => $mo_number,
                //'FoundProblem.problem_id != 1'
            ),
            'group'      => array(
                'FoundProblem.problem_id',
                'FoundProblem.sensor_id',
                'Plan.mo_number'
            ),
            'order'      => array('FoundProblem.problem_id')
        ));


        return $found_problems;
    }

    public function getAllByPlanId($plan_id) {
        $this->bindModel(array('belongsTo' => array('Problem')));

        return $this->find('all', array(
            'conditions' => array(
                'plan_id' => $plan_id
            )
        ));
    }

    public static function getTimeSum($fpArray) {
        $timeSum = 0;
        foreach ($fpArray as $fp) {
            $diff = strtotime($fp['FoundProblem']['end']) - strtotime($fp['FoundProblem']['start']);
            $timeSum += ($diff > 0) ? $diff : 0;
        }

        return $timeSum;
    }

    public function getByShiftForReport($shift, $sensor_id, $exactProblem=0) {
        $problemFilter = '';//'(FoundProblem.problem_id > 2 OR FoundProblem.problem_id < 0)';
        if($exactProblem > 0) {
            $problemFilter = 'FoundProblem.problem_id = ' . $exactProblem;
        }
        if($exactProblem == -1) {
            $problemFilter = '';
        }
        if(is_array($exactProblem)) $problemFilter = $exactProblem;

        $found_problems = $this->withRefs()->find('all', array(
            'fields'     => array(
                'Sensor.name','Sensor.plan_relate_quantity','Sensor.id','Factory.name',
                'FoundProblem.id','FoundProblem.problem_id','FoundProblem.start', 'FoundProblem.end','FoundProblem.comments','FoundProblem.quantity','FoundProblem.transition_problem_id',
                'ApprovedOrder.additional_data','ApprovedOrder.quantity','ApprovedOrder.box_quantity','ApprovedOrder.id',
                'Plan.mo_number','Plan.step','Plan.production_name','Plan.production_code','Plan.box_quantity',
                'User.first_name','User.last_name', 'User.unique_number',
                'Shift.start','Shift.end',
                'DiffCard.description',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN FoundProblem.start > \'' . $shift['Shift']['start'] . '\' THEN FoundProblem.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN FoundProblem.end < \'' . $shift['Shift']['end'] . '\' THEN FoundProblem.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as found_problem_duration',
                'Problem.id', 'Problem.name',
                'FoundProblem.plan_id'
            ),
            'conditions' => array(
                'FoundProblem.shift_id'      => $shift['Shift']['id'], //pakeista is laikotarpio i id, kad parinkus du darbo centrus is skirtingu padaliniu negrazintu tu paciu duomenu (geralda 2020-08-10)
//                'FoundProblem.start <'      => $shift['Shift']['end'],
//                'FoundProblem.end >'        => $shift['Shift']['start'],
                'FoundProblem.sensor_id'    => $sensor_id,
                'FoundProblem.start < FoundProblem.end',
                $problemFilter,
                //'(TIMESTAMPDIFF(SECOND,
                //    CASE WHEN FoundProblem.start > \'' . $shift['Shift']['start'] . '\' THEN FoundProblem.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                //    CASE WHEN FoundProblem.end < \'' . $shift['Shift']['end'] . '\' THEN FoundProblem.end ELSE \'' . $shift['Shift']['end'] . '\' END)) > 60'
            ),
            'order'=>array('FoundProblem.sensor_id','FoundProblem.start'),
            'group'=>array('FoundProblem.sensor_id','FoundProblem.start')
        ));
        return $found_problems;
    }

    public function getGroupedByProblem($found_problems){
        $grouped = array();
        foreach ($found_problems as $fp) {
            $problem_id = $fp['Problem']['id'];
            if (!array_key_exists($problem_id, $grouped)) {
                $grouped[$problem_id]['problem_name'] = Settings::translate($fp['Problem']['name']);
                $grouped[$problem_id]['duration'] = $fp[0]['found_problem_duration'];
                $grouped[$problem_id]['count'] = 1;
                continue;
            }
            $grouped[$problem_id]['duration'] += $fp[0]['found_problem_duration'];
            $grouped[$problem_id]['count'] += 1;
        }
        return $grouped;
    }
    
    public function addSupperParentToProblems($problemsTreeList){
        $this->bindModel(array('belongsTo'=>array('Problem')));
        $foundProblems = $this->find('list', array(
            'recursive'=>1,
            'fields'=>array('id','problem_id'), 
            'conditions'=>array(
                'FoundProblem.super_parent_problem_id IN'=>array(0, self::STATE_UNKNOWN), 
                'FoundProblem.problem_id <> FoundProblem.super_parent_problem_id', 
                'Problem.id IS NOT NULL'
                //'FoundProblem.start >'=>date('Y-m-d', strtotime('-1 WEEKS'))
            ),'limit'=>10000
        ));
        foreach($foundProblems as $foundProblemId => $problemId){
            if(isset($problemsTreeList[$problemId])){
                $superParentProblemId = current(array_reverse(array_keys($problemsTreeList[$problemId])));
            }else{ continue; }
            $this->updateAll(array('super_parent_problem_id'=>$superParentProblemId),array('FoundProblem.id'=>$foundProblemId));
        }
    }
    
    public function createNewFProblemWhenCurrentExceeds(Array &$fFoundProblem, Array $fSensor, int $diffMins){
        STATIC $problemModel = null, $problemsList = null, $logModel = null;
        if($problemModel == null){
            $problemModel = ClassRegistry::init('Problem');
            $logModel = ClassRegistry::init('Log');
            $problemsList = $problemModel->find('list', array('fields'=>array('Problem.id','Problem.id'),'conditions'=>array('Problem.disabled'=>0)));
        }
        $settingsPairs = explode(',',$fSensor['Sensor']['new_fproblem_when_current_exceeds2']);
        array_walk($settingsPairs, function(&$data){ $data = trim($data); });
        $settings = array();
        foreach($settingsPairs as $settingsPair){
            $s = explode('_', $settingsPair);
            if(sizeof($s) != 3) continue;
            list($fromId,$duration,$toId) = $s;
            if(!isset($problemsList[$fromId]) || !isset($problemsList[$toId])) continue;
            if(!in_array($fromId, array($fFoundProblem[self::NAME]['problem_id'], $fFoundProblem[self::NAME]['transition_problem_id']))) continue;
            $settings = compact('fromId','duration','toId');
        }
        //if(!empty($settings) && $settings['duration'] <= $diffMins){
        if(!empty($settings) && $settings['duration'] <= $diffMins){
            $start = new DateTime($fFoundProblem[self::NAME]['end']);
            $start = $start->add(new DateInterval('PT1S'));
            $this->findAndComplete($fSensor);
            $userModel = ClassRegistry::init('User');
			$activeUser = $userModel->getActiveUserInSensor($fSensor);
            $oldFoundProblem = $fFoundProblem;
			$fFoundProblem = $this->newItem($start, $start, $settings['toId'], $fSensor['Sensor']['id'], $fFoundProblem[self::NAME]['shift_id'], self::STATE_IN_PROGRESS, null, false, $fFoundProblem[self::NAME]['plan_id'], json_decode($fFoundProblem[self::NAME]['json']), 0, $activeUser['User']['id']??0);
            $logModel->write('Virsytas tiesiogiai numatytas prastovos laikas [new_fproblem_when_current_exceeds2], todel kuriama nauja prastova'.json_encode($fFoundProblem).'. Buvusi uzdaryta prastova: '.json_encode($oldFoundProblem),$fSensor);
        }
    }
    
    //Kai esame problema esamos pamainos mastu pasiekia nurodyta riba, sukuriama nauja
    public function createNewFProblemWhenCurrentExceedsInShift(Array &$fFoundProblem, Array $fSensor, int $diffMins){
        STATIC $problemModel = null, $problemsList = null, $logModel = null;
        if($problemModel == null){
            $problemModel = ClassRegistry::init('Problem');
            $logModel = ClassRegistry::init('Log');
            $problemsList = $problemModel->find('list', array('fields'=>array('Problem.id','Problem.id'),'conditions'=>array('Problem.disabled'=>0)));
        }
        $settingsPairs = explode(',',$fSensor['Sensor']['new_fproblem_when_current_exceeds']);
        array_walk($settingsPairs, function(&$data){ $data = trim($data); });
        $settings = array();
        foreach($settingsPairs as $settingsPair){
            $s = explode('_', $settingsPair);
            if(sizeof($s) != 3) continue;
            list($fromId,$duration,$toId) = $s;
            if(!isset($problemsList[$fromId]) || !isset($problemsList[$toId])) continue;
            if($fFoundProblem[self::NAME]['problem_id'] != $fromId) continue;
            $settings = compact('fromId','duration','toId');
        }
        //if(!empty($settings) && $settings['duration'] <= $diffMins){
        if(!empty($settings)){
            $fromProblemDurationInShift = $this->find('first', array(
                'fields'=>array('SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)) AS duration'),
                'conditions'=>array(
                    'FoundProblem.shift_id'=>$fFoundProblem[self::NAME]['shift_id'], 
                    'FoundProblem.problem_id'=>$settings['fromId'],
                    'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']
                )
            ));
            if($settings['duration']*60 <= $fromProblemDurationInShift[0]['duration']){
                $toProblemExistInShift = $this->find('first', array('conditions'=>array(
                    'FoundProblem.shift_id'=>$fFoundProblem[self::NAME]['shift_id'], 
                    'FoundProblem.problem_id'=>$settings['toId'],
                    'FoundProblem.sensor_id'=>$fSensor['Sensor']['id']
                )));
                if(empty($toProblemExistInShift)){//jei virsyta problema dar neegzsituoja pamainoje, esama stabdome ir kuriame nauja
                    $start = new DateTime($fFoundProblem[self::NAME]['end']);
                    $start = $start->add(new DateInterval('PT1S'));
                    $this->findAndComplete($fSensor);
					$userModel = ClassRegistry::init('User');
					$activeUser = $userModel->getActiveUserInSensor($fSensor);
                    $fFoundProblem = $this->newItem($start, $start, $settings['toId'], $fSensor['Sensor']['id'], $fFoundProblem[self::NAME]['shift_id'], self::STATE_IN_PROGRESS, null, false, $fFoundProblem[self::NAME]['plan_id'], json_decode($fFoundProblem[self::NAME]['json']), 0, $activeUser['User']['id']??0);
                    $logModel->write('Pamainos mastu virsytas numatytas prastovos laikas [new_fproblem_when_current_exceeds], todel kuriama nauja prastova'.json_encode($fFoundProblem),$fSensor);
                }else{
                    $this->updateAll(array('problem_id'=>$settings['toId']), array('FoundProblem.id'=>$fFoundProblem[self::NAME]['id']));
                    $fFoundProblem['FoundProblem']['problem_id'] = $settings['toId'];
                }
            }
        }
    }
    
    public function sendMailsOnProblemOutgo(Array &$fFoundProblem, Array $fSensor, int $diffMins){
        $markerPath = __dir__.'/../tmp/logs/lock_mails_send_'.$fSensor['Sensor']['id'].'.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, 1);
            fclose($fh);
        }else{
            return;
        }
        STATIC $exclusionProblemsModel, $problemModel, $userModel, $exclusionsList, $problem, $smsModel, $logModel, $mailModel, $shiftModel = null;
        $exclusionProblemsModel = $exclusionProblemsModel == null?ClassRegistry::init('ShiftTimeProblemExclusion'):$exclusionProblemsModel;
        $problemModel = $problemModel == null?ClassRegistry::init('Problem'):$problemModel;
        $userModel = $userModel == null?ClassRegistry::init('User'):$userModel;
		$smsModel = $smsModel == null? ClassRegistry::init('SMS'):$smsModel;
		$logModel = $logModel == null? ClassRegistry::init('Log'):$logModel;
        $mailModel = $mailModel == null?ClassRegistry::init('Mail'):$mailModel;
        $shiftModel = $shiftModel == null?ClassRegistry::init('Shift'):$shiftModel;
        $currentShift = $shiftModel->findById($fFoundProblem[self::NAME]['shift_id']);
        if(empty($currentShift) || $currentShift['Shift']['disabled']){ return null; }
        //$exclusionsList = array();//2019-03-26 siunciame visais atvejais
        $exclusionsList = $exclusionsList == null?$exclusionProblemsModel->find('list', array('fields'=>array('problem_id'))):$exclusionsList;
        if(!in_array($fFoundProblem[self::NAME]['problem_id'], $exclusionsList)){
            //$fFoundProblem = $foundProblemModel->findById($fFoundProblem[self::NAME]['id']);//jei i sia funkcija kreipiamasi kelis kartus is ciklo, kiekviena karta duomenis reikia uzsikrauti is naujo, nes galejo buti jau issiusti laiskai
            $problemId = $fFoundProblem[self::NAME]['id'];
            $mailsAlreadySended[$problemId] = array();
            if($fFoundProblem[self::NAME]['prev_shift_problem_id']){
                $this->bindModel(array('belongsTo'=>array('Shift')));
                $prevShiftFoundProblem = $this->find('first', array(
                    'fields'=>array(
                        'TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end) AS diff',
                        'FoundProblem.mail_inform_sended'
                    ),'conditions'=>array(
                        'FoundProblem.id'=>$fFoundProblem[self::NAME]['prev_shift_problem_id'],
                        'FoundProblem.problem_id NOT'=>$exclusionsList,
                        'Shift.disabled'=>0,
                    )
                ));
                if(!empty($prevShiftFoundProblem)){
                    if($prevShiftFoundProblem[0]['diff']){
                        $diffMins += ($prevShiftFoundProblem[0]['diff']/60);
                    }
                    if(trim($prevShiftFoundProblem[self::NAME]['mail_inform_sended'])){
                        $mailsAlreadySended[$problemId] = explode(',', $prevShiftFoundProblem[self::NAME]['mail_inform_sended']);
                    }
                }
            }
            $mailProblemsGroups = explode(';', $fSensor['Sensor']['inform_about_problems']);
            $mailsSendingNow = $smsSendingNow = array();
            //if(!isset($mailsAlreadySended[$problemId])){
                if(trim($fFoundProblem[self::NAME]['mail_inform_sended'])){
                    $mailsAlreadySended[$problemId] = array_merge($mailsAlreadySended[$problemId], explode(',', $fFoundProblem[self::NAME]['mail_inform_sended']));
                }
            //}
            $sendWhenReachesDuration = 0;
            $patternEmail = '/[a-z0-9_\-\+\.]+@[a-z0-9\-]+\.([a-z]{2,3})(?:\.[a-z]{2})?/i';
            $patternPhone = '/\+\d{11,}/';
            foreach($mailProblemsGroups as $mailProblemsGroup){
                $rightPart = current(array_reverse(explode(':', $mailProblemsGroup)));
                $sendWhenReachesDuration = intval($rightPart);
                if(preg_match('/\d+\s*\[([^\]]+)\]/i', $rightPart, $match)){ //ieskome ar grupeje yra nurodyta siusti pranesimus tik atitinkamas problemas
                    $problemsListToSend = isset($match[1])?explode(',', $match[1]):array();
                    array_walk($problemsListToSend, function(&$id){ $id = trim($id); });
                    if(!in_array($fFoundProblem[self::NAME]['problem_id'], $problemsListToSend)) continue; //jei esama problema nera tarp aprasytu grupei, pranesimo nesiunciame
                }
                if($sendWhenReachesDuration > 0 && $diffMins >= $sendWhenReachesDuration){//jei pasiekiamos laikas virsyja mailu grupes laika
                    preg_match_all($patternEmail, $mailProblemsGroup, $mails);
                    $mails = isset($mails[0])?$mails[0]:array();
                    foreach($mails as $mail){
                        $mail .= ':'.$sendWhenReachesDuration;
                        if(trim($mail) && !in_array(trim($mail), $mailsAlreadySended[$problemId])){//patikriname ar si problema jau nebuvo issiusta konkreciam mailui
                            $mailsSendingNow[] = trim($mail);
                        }
                    }
                    preg_match_all($patternPhone, $mailProblemsGroup, $phones);
                    $phones = isset($phones[0])?$phones[0]:array();
                    foreach($phones as $phone){
                        $phone .= ':'.$sendWhenReachesDuration;
                        if(trim($phone) && !in_array(trim($phone), $mailsAlreadySended[$problemId])){//patikriname ar si problema jau nebuvo issiusta konkreciam mailui
                            $smsSendingNow[] = trim($phone);
                        }
                    }
                }
            }
            $mailsSendingNow = array_unique($mailsSendingNow);
            if(!empty($mailsSendingNow)){
                $mailsSendingNow = array_diff($mailsSendingNow, $mailsAlreadySended[$problemId]);
                $user = $userModel->find('first', array('conditions'=>array('User.sensor_id'=>$fSensor['Sensor']['id'])));
                $lineId = !empty($user)?$user['User']['line_id']:(isset($fSensor['Line']['id'])?$fSensor['Line']['id']:0);
                $problem = $problem == null || $problem['Problem']['id'] != $fFoundProblem[self::NAME]['problem_id']?$problemModel->findById($fFoundProblem[self::NAME]['problem_id']):$problem;
                $filteredMails = $mailsSendingNow; //atsifiltruojame mailu gaveju sarsa, ismesdami is adresu laiko dali, kuri pasako kada reikia siusti maila. Mailus saugome su laiko dalimi, kad galima butu issiusti ta pacia problema tam paciam gavejui skirtingais laiko momentais
                array_walk($filteredMails, function(&$mail)use($patternEmail){
                    preg_match($patternEmail, $mail, $match); 
                    if(isset($match[0])){ $mail = $match[0]; }
                });
                $url = 'https://'.current(array_reverse(explode('/', ROOT))).'/work-center/'.$lineId.'_'.$fSensor['Sensor']['id'];
                $message = '
                    <table>
                        <tr><th colspan="2">'.__('HorasOEE pranešimas apie vykstančias prastovas').'</th></tr>
                        <tr><td colspan="2"></td></tr>
                        <tr><td><b>'.__('Data ir laikas').'</b></td><td>'.date('Y-m-d H:i').'</td></tr>
                        <tr><td><b>'.__('Darbo centras').'</b></td><td>'.$fSensor['Sensor']['name'].'</td></tr>
                        <tr><td><b>'.__('Padalinys').'</b></td><td>'.$fSensor['Branch']['name'].'</td></tr>
                        <tr><td><b>'.__('Linija').'</b></td><td>'.(isset($fSensor['Line'])?$fSensor['Line']['name']:'').'</td></tr>
                        <tr><td colspan="2"></td></tr>
                        <tr><td><b>'.__('Prastovos pav.').'</b></td><td>'.Settings::translate($problem['Problem']['name']).'</td></tr>
                        <tr><td><b>'.__('Komentaras').'</b></td><td>'.$fFoundProblem[self::NAME]['comments'].'</td></tr>
                        <tr><td><b>'.__('Trukmė (min.)').'</b></td><td>'.round($diffMins,2).'</td></tr>
                        <tr><td colspan="2"></td></tr>
                        <tr><td colspan="2"><a href="'.$url.'" target="_blank">HorasOEE</a></td></tr>
                    </table>
                ';
                $mailModel->send(array_unique($filteredMails), $message, __('HorasOEE pranešimas apie einančias prastovas'));
                $mailsAlreadySended[$problemId] = array_merge($mailsSendingNow, $mailsAlreadySended[$problemId]);
                $this->id = $fFoundProblem[self::NAME]['id'];
                $fFoundProblem[self::NAME]['mail_inform_sended'] = implode(',', $mailsAlreadySended[$problemId]);
                $this->save(array('mail_inform_sended'=>$fFoundProblem[self::NAME]['mail_inform_sended']));
            }
			if(!empty($smsSendingNow)){
				$smsSendingNow = array_diff($smsSendingNow, $mailsAlreadySended[$problemId]);
				$problem = $problem == null || $problem['Problem']['id'] != $fFoundProblem[self::NAME]['problem_id']?$problemModel->findById($fFoundProblem[self::NAME]['problem_id']):$problem;
				$filteredPhones = $smsSendingNow; //atsifiltruojame mailu gaveju sarsa, ismesdami is adresu laiko dali, kuri pasako kada reikia siusti maila. Mailus saugome su laiko dalimi, kad galima butu issiusti ta pacia problema tam paciam gavejui skirtingais laiko momentais
                array_walk($filteredPhones, function(&$phone)use($patternPhone){
                    preg_match($patternPhone, $phone, $match); 
                    if(isset($match[0])){ $phone = $match[0]; }
                });
                $problemName = ucfirst(strtolower(Inflector::humanize(Inflector::slug(Settings::translate($problem['Problem']['name'])))));
				$text = __('HorasOEE vykstanti prastova')."\n\n";
				$text .= __('DC').': "'.$fSensor['Sensor']['name']."\"\n";
				$text .= __('Padalinys').': "'.$fSensor['Branch']['name']."\"\n";
				//$text .= __('Linija').': "'.$fSensor['Line']['name']."\"\n";
				$text .= __('Prastovos pav.').': "'.$problemName.' - '.round($diffMins,2).' '.__('min.');
				foreach($filteredPhones as $phoneNr){
					$backData = $smsModel->send($text, $phoneNr, 'HorasOEE', __('Užfiksuota prastova'));
				}
				$mailsAlreadySended[$problemId] = array_merge($smsSendingNow, $mailsAlreadySended[$problemId]);
                $this->id = $fFoundProblem[self::NAME]['id'];
                $fFoundProblem[self::NAME]['mail_inform_sended'] = implode(',', $mailsAlreadySended[$problemId]);
                $this->save(array('mail_inform_sended'=>$fFoundProblem[self::NAME]['mail_inform_sended']));
			}
        }
        unlink($markerPath);
    }

    public function fillEmptyGaps($fSensor, $fRecords, $start, $end){
        $fProblem = $this->schema();
        array_walk($fProblem, function(&$val){ $val = 0; });
        $foundProblems = array();
        if(current(array_reverse($fRecords))['Record']['created'] < $end){
            $fRecords[] = array('Record'=>array('created'=>$end));
        }
        foreach($fRecords as $key => $fRecord){
            if(strtotime($fRecord['Record']['created']) - strtotime($start) > Configure::read('recordsCycle')){
                $fProblem['start'] = $start;
                $fProblem['end'] = $fRecord['Record']['created'];
                $foundProblems[] = array(
                    'FoundProblem'=>$fProblem, 
                    'Problem'=>array('id'=>0,'name'=>__('Nėra ryšio'))
                );
            }
            $start = $fRecord['Record']['created'];
        }
        return $foundProblems;
    }
    
    
    public function createExceedingTransitionByPlan($fSensor, $currentPlan, &$fFoundProblem, $fShift){
        //Jei bent karta esamam uzsakymui derinimas buvo virsytas ir sukurta virsijimo problema, tai antra karta derinant ta pati uzsakyma (po atidejimo pvz) nebereikia
        static $recordModel = null;
        static $settingModel = null;
        static $logModel = null;
        if($recordModel == null) $recordModel = ClassRegistry::init('Record');
        if($settingModel == null) $settingModel = ClassRegistry::init('Settings');
        if($logModel == null) $logModel = ClassRegistry::init('Log');
        $exceededTrans = false;
        $totalPreparationTime = 0;
        //if(intval($fSensor[Sensor::NAME]['working_on_plans_or_products'] == 0)){//jei OEE vykdo planus
        if(intval($currentPlan['Plan']['preparation_time'] > 0)){
            $totalPreparationTime = (int)current($this->find('first', array(
                'fields' => array(
                    'SUM(TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end)) AS total_preparation_time',
                ),'conditions' => array(
                    'FoundProblem.plan_id' => $currentPlan[Plan::NAME]['id'], 
                    'FoundProblem.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    'FoundProblem.problem_id'=>Problem::ID_NEUTRAL
                ),
            )))['total_preparation_time']; 
            $exceededTrans = $this->find('first', array('conditions'=>array(
                'FoundProblem.plan_id'=>$currentPlan[Plan::NAME]['id'], 
                'FoundProblem.problem_id' => $settingModel->getOne('exceeded_transition_id')
            )));
        }
        if(is_array($exceededTrans) && !empty($exceededTrans)){ //jei virsijimas jau buvo o dabar vel yra derinimas, derinima iskart verciame  virsijimu (tik kai vykdomi planai)
            $fFoundProblem[FoundProblem::NAME]['problem_id'] = $settingModel->getOne('exceeded_transition_id');
            $logModel->write('Derinimas buvo paverstas derinimo virsijimu, kadangi uzsakymui jau isnaudotas leistinas derinimo laikas',$fSensor);
        }elseif($totalPreparationTime > $currentPlan['Plan']['preparation_time']*60){
            $oldFoundProblem = $fFoundProblem;
            $allowTransitionEnd = strtotime($fFoundProblem['FoundProblem']['start']) + ($currentPlan['Plan']['preparation_time']*60);
            $allowTransitionEnd = time() > $allowTransitionEnd?new DateTime(date('Y-m-d H:i:s',$allowTransitionEnd)):new DateTime();
            $this->findAndComplete($fSensor, $allowTransitionEnd);
            $logModel->write('problema turi buti uzdaryta cia: '.json_encode($this->findById($fFoundProblem[FoundProblem::NAME]['id'])),$fSensor);
            $allowTransitionEnd->add(new DateInterval('PT1S'));
            $fFoundProblem = $this->newItem(
                $allowTransitionEnd, 
                $allowTransitionEnd,
                $settingModel->getOne('exceeded_transition_id'), 
                $fSensor[Sensor::NAME]['id'], 
                $fShift[Shift::NAME]['id'], 
                FoundProblem::STATE_IN_PROGRESS, 
                null, 
                false, 
                $currentPlan[Plan::NAME]['id']
            );
            $recordModel->updateAll(
                array('found_problem_id'=>$fFoundProblem['FoundProblem']['id']),
                array(
                    'Record.created >='=>$allowTransitionEnd->format('Y-m-d H:i:s'),
                    'Record.found_problem_id'=>$oldFoundProblem['FoundProblem']['id']
                )
            );
            $logModel->write('virsytas derinimo laikas, todel kuriama derinimo virsijimo prastova: '.json_encode($fFoundProblem),$fSensor);
        }
    }

    public function createExceedingTransitionByTransition($fSensor, $currentPlan, &$fFoundProblem, $fShift, $problem){
        if(empty($problem) || $problem['Problem']['transition_time'] <= 0) return;
        static $recordModel = null;
        static $settingModel = null;
        static $logModel = null;
        if($recordModel == null) $recordModel = ClassRegistry::init('Record');
        if($settingModel == null) $settingModel = ClassRegistry::init('Settings');
        if($logModel == null) $logModel = ClassRegistry::init('Log');
        $exceededTrans = false;
        $totalPreparationTime = 0;
        $totalPreparationTime = (int)current($recordModel->find('first', array(
                'fields' => array(
                    'SUM('.Configure::read('recordsCycle').') AS total_preparation_time',
                ),'conditions' => array(
                    'Record.found_problem_id' => array($fFoundProblem['FoundProblem']['id'], $fFoundProblem['FoundProblem']['prev_shift_problem_id']), 
                    'Record.sensor_id' => $fSensor[Sensor::NAME]['id'],
                ),
            )))['total_preparation_time'];
        if($totalPreparationTime >= $problem['Problem']['transition_time']*60){
            $this->findAndComplete($fSensor);
            $fFoundProblem = $this->newItem(
                new DateTime(date('Y-m-d H:i:s',strtotime($fFoundProblem['FoundProblem']['end'])+1)), 
                new DateTime(date('Y-m-d H:i:s',strtotime($fFoundProblem['FoundProblem']['end'])+1)),
                Problem::ID_NOT_DEFINED, 
                $fSensor[Sensor::NAME]['id'], 
                $fShift[Shift::NAME]['id'], 
                FoundProblem::STATE_IN_PROGRESS, 
                null, 
                false, 
                $currentPlan[Plan::NAME]['id'],
                array('transition_exceeded'=>1)
            );
            $logModel->write('2 virsytas derinimo laikas, todel kuriama derinimo virsijimo problema: '.json_encode($fFoundProblem),$fSensor);
        }
    }
    
    //prastovos skaldymas i pervirsi kai prastovos tipas nurodomas jau pasibaigusiai prastovai
    public function splitProblemIfExceeds($oldData, $newData, $fSensor, $problemsTreeList){
        STATIC $problemModel = null, $problemsList = null, $logModel = null;
        if($problemModel == null){
            $problemModel = ClassRegistry::init('Problem');
            $logModel = ClassRegistry::init('Log');
            $problemsList = $problemModel->find('list', array('fields'=>array('Problem.id','Problem.id'),'conditions'=>array('Problem.disabled'=>0)));
        } 
        if(!trim($fSensor[Sensor::NAME]['new_fproblem_when_current_exceeds2']) || $oldData[self::NAME]['problem_id'] != self::STATE_UNKNOWN){ return false; }//skaldome tik nepazymeta prastova
        $settingsPairs = explode(',',$fSensor['Sensor']['new_fproblem_when_current_exceeds2']);
        array_walk($settingsPairs, function(&$data){ $data = trim($data); });
        $settings = array();
        foreach($settingsPairs as $settingsPair){
            $s = explode('_', $settingsPair);
            if(sizeof($s) != 3) continue;
            list($fromId,$duration,$toId) = $s;
            if(!isset($problemsList[$fromId]) || !isset($problemsList[$toId])) continue;
            if($newData['problem_id'] != $fromId) continue; //jei nepazymeta prastova verciama ne i ta, kuri turi skilti ant pervirsio, ieskome prastovos toliau
            $settings = compact('fromId','duration','toId');
            break;
        }
        $diffMins = floor((strtotime($oldData[self::NAME]['end']) - strtotime($oldData[self::NAME]['start'])) / 60);
        if(!empty($settings) && $settings['duration'] <= $diffMins){
            $recordModel = ClassRegistry::init('Record');
            $maxLengthEnd = strtotime($oldData[self::NAME]['start']) + $settings['duration']*60; 
            $recordsData = $recordModel->find('first',array(
                'fields'=>array('SUM(Record.quantity) AS quantity', 'MIN(Record.created) AS created'),
                'conditions'=>array('Record.created >='=>date('Y-m-d H:i:s',$maxLengthEnd), 'Record.found_problem_id'=>$oldData[self::NAME]['id']), 
                'order'=>array('Record.created ')
            ));
            $this->save(array('state'=>self::STATE_COMPLETE,'end'=>$recordsData[0]['created'], 'id'=>$oldData[self::NAME]['id'], 'quantity'=>bcsub($oldData[self::NAME]['quantity'], $recordsData[0]['quantity'],4)));
            $this->create();
            
            $newData['start'] = date('Y-m-d H:i:s', strtotime($recordsData[0]['created'])+1);
            $newData['problem_id'] = $toId;
            $newData['quantity'] = $recordsData[0]['quantity'];
            if(isset($problemsTreeList[$toId])){
                $newData['super_parent_problem_id'] = current(array_reverse(array_keys($problemsTreeList[$toId])));
            }
            unset($newData['id']);
            $this->save($newData);
            $updateWhat = array('Record.found_problem_id'=>$this->id);
            $updateWho = array('Record.created >'=>$newData['start'], 'Record.found_problem_id'=>$oldData[self::NAME]['id']);
            $recordModel->updateAll($updateWhat,$updateWho);
            $logModel->write('Pazymeta prastova luzta i kita, nes virsyja leidziama laika. Senoji prastova'.json_encode($oldData).'. Naujoji prastova: '.json_encode($this->read()).'. Virsijanciu prastovu irasai verciami i naujaja prastova pagal salyga: '.json_encode($updateWho), $fSensor);
        }
    }

    public function splitProblemByUserRequest($fSensor, $foundProblemId, $problemTypeId, $downtimeCutType, $downtimeCutDuration){
        if(!$downtimeCutDuration || !$foundProblemId || $downtimeCutDuration < 0) return false;
        $fFoundProblem = $this->findById($foundProblemId);
        $recordModel = ClassRegistry::init('Record');
        $logModel = ClassRegistry::init('Log');
        if($downtimeCutDuration * 60 > strtotime($fFoundProblem['FoundProblem']['end'])-strtotime($fFoundProblem['FoundProblem']['start'])){ return true; }
        if($downtimeCutType == 0){//kerpama nuo pradzios
            $dt = strtotime($fFoundProblem['FoundProblem']['start']) + $downtimeCutDuration * 60 + 1;
            $recordsData = $recordModel->find('first',array(
                'fields'=>array('SUM(Record.quantity) AS quantity'),
                'conditions'=>array('Record.created >'=>date('Y-m-d H:i:s', $dt), 'Record.found_problem_id'=>$foundProblemId), 
            ));
            //nutoliname pradzia esamai prastovai
            $this->save(array(
                'id'=>$foundProblemId,
                'start'=>date('Y-m-d H:i:s', $dt),
                'quantity'=>$recordsData[0]['quantity'],
                'exported_to_mpm'=>0,
            ));
            $quantity = bcsub($fFoundProblem['FoundProblem']['quantity'],$recordsData[0]['quantity'],4);
            $this->newItem(new DateTime($fFoundProblem['FoundProblem']['start']), new DateTime(date('Y-m-d H:i:s',$dt-1)), $problemTypeId, $fSensor['Sensor']['id'], $fFoundProblem['FoundProblem']['shift_id'], self::STATE_COMPLETE, null, false, $fFoundProblem['FoundProblem']['plan_id'], $fFoundProblem['FoundProblem']['json'], $quantity);
            $updateWhat = array('Record.found_problem_id'=>$this->id);
            $updateWho = array('Record.created <'=>date('Y-m-d H:i:s', $dt), 'Record.found_problem_id'=>$foundProblemId);
            $recordModel->updateAll($updateWhat,$updateWho);
            $logModel->write('Prastova atkirpta nuo pradzios '.$downtimeCutDuration.' min. Naujos prastovos duomenys: '.json_encode($this->read()).'; Originalios prastovos duomenys: '.json_encode($fFoundProblem), $fSensor);
        }else{//kerpame nuo pabaigos
            $dt = strtotime($fFoundProblem['FoundProblem']['end']) - $downtimeCutDuration * 60 - 1;
            $recordsData = $recordModel->find('first',array(
                'fields'=>array('SUM(Record.quantity) AS quantity'),
                'conditions'=>array('Record.created <'=>date('Y-m-d H:i:s', $dt), 'Record.found_problem_id'=>$foundProblemId), 
            ));
            //sutruminame pabaiga easmai prastovai
            $this->save(array(
                'id'=>$foundProblemId,
                'end'=>date('Y-m-d H:i:s', $dt),
                'quantity'=>$recordsData[0]['quantity'],
                'exported_to_mpm'=>0,
            ));
            $quantity = bcsub($fFoundProblem['FoundProblem']['quantity'],$recordsData[0]['quantity'],4);
            $this->newItem(new DateTime(date('Y-m-d H:i:s',$dt+1)), new DateTime($fFoundProblem['FoundProblem']['end']), $problemTypeId, $fSensor['Sensor']['id'], $fFoundProblem['FoundProblem']['shift_id'], self::STATE_COMPLETE, null, false, $fFoundProblem['FoundProblem']['plan_id'], $fFoundProblem['FoundProblem']['json'], $quantity);
            $updateWhat = array('Record.found_problem_id'=>$this->id);
            $updateWho = array('Record.created >'=>date('Y-m-d H:i:s', $dt), 'Record.found_problem_id'=>$foundProblemId);
            $recordModel->updateAll($updateWhat,$updateWho);
            $logModel->write('Prastova atkirpta nuo pabaigos '.$downtimeCutDuration.' min. Naujos prastovos duomenys: '.json_encode($this->read()).'; Originalios prastovos duomenys: '.json_encode($fFoundProblem), $fSensor);
        }
        return true;
    }

	public function addDowntimeTypeToOtherSameGroupSensors($fSensor,$sensorsDowntimesGroupsString,$fProblemType,$fFoundProblem,$problemsTreeList){
		$parameters = array(&$fSensor,&$sensorsDowntimesGroupsString,&$fProblemType,&$fFoundProblem);
		$this->callPluginFunction('FoundProblem_beforeAddDowntimeTypeToOtherSameGroupSensors_Hook', $parameters, Configure::read('companyTitle'));
		if(!preg_match_all('/\[[(\d|:|,\s)]+\]/i',$sensorsDowntimesGroupsString, $sensorsDowntimesGroups)){ return false; }
		if(empty($sensorsDowntimesGroups)){ return false; }
		$sensorsDowntimesGroups = array_map(function($data){ return trim($data); }, $sensorsDowntimesGroups[0]);
		$foundProblemsIdsToUpdate = array();
		foreach($sensorsDowntimesGroups as $sensorsDowntimesGroups){
			$sensorsAndProblemsIds = explode(':', rtrim(ltrim($sensorsDowntimesGroups,'['),']'));
			$sensorsIds = explode(',', $sensorsAndProblemsIds[0]);
			$sensorsIds = array_map(function($data){ return trim($data); }, $sensorsIds);
			$problemsIds = isset($sensorsAndProblemsIds[1])?explode(',', $sensorsAndProblemsIds[1]):array();
			$problemsIds = array_map(function($data){ return trim($data); }, $problemsIds);
			if(!empty($problemsIds) && !in_array($fProblemType['Problem']['id'], $problemsIds)){ continue; }
			$otherSensorsIds = array_diff($sensorsIds, array($fSensor['Sensor']['id']));
			if(empty($otherSensorsIds) || !in_array($fSensor['Sensor']['id'], $sensorsIds)){ continue; }
        	$otherDowntimes = $this->find('all', array(
        	    'fields'=>array(
        	       'FoundProblem.id',
                   'ABS(TIMESTAMPDIFF(SECOND, FoundProblem.start, \''.$fFoundProblem['FoundProblem']['start'].'\')) + ABS(TIMESTAMPDIFF(SECOND, FoundProblem.end, \''.$fFoundProblem['FoundProblem']['end'].'\')) AS distance'
                ),'conditions'=>array(
					'FoundProblem.sensor_id'=>$otherSensorsIds,
					'FoundProblem.problem_id <>'=>Problem::ID_WORK,
					sprintf('FoundProblem.start BETWEEN \'%s\' AND \'%s\'', date('Y-m-d H:i:s', strtotime($fFoundProblem['FoundProblem']['start'])-60), date('Y-m-d H:i:s', strtotime($fFoundProblem['FoundProblem']['start'])+60)),
					sprintf('FoundProblem.end BETWEEN \'%s\' AND \'%s\'', date('Y-m-d H:i:s', strtotime($fFoundProblem['FoundProblem']['end'])-60), date('Y-m-d H:i:s', strtotime($fFoundProblem['FoundProblem']['end'])+60)),
				)
			));  
            $otherDowntimes = Hash::combine($otherDowntimes, '{n}.FoundProblem.id', '{n}.0.distance');
            asort($otherDowntimes);         
            if(!empty($otherDowntimes)){  
			     $foundProblemsIdsToUpdate[] = key($otherDowntimes);
            }
		}
		if(!empty($foundProblemsIdsToUpdate)){
			$logModel = ClassRegistry::init('Log');
			$logModel->write('Pakeistos sustojimo priezastys i prastova ID '.$fProblemType['Problem']['id'].' tos pacios grupes darbo centrams, kadangi juose buvo aptiktos +/- 60s intervale esancios prastovos. Prastovos pries atnaujinima'.json_encode($this->find('all',array('conditions'=>array('FoundProblem.id'=>$foundProblemsIdsToUpdate)))), $fSensor);
			if($fProblemType['Problem']['transition_problem']){
			    $updateArray['FoundProblem.transition_problem_id'] = $fProblemType['Problem']['id'];
			    $updateArray['FoundProblem.problem_id'] = Problem::ID_NEUTRAL;
			}else{
			    $updateArray['FoundProblem.problem_id'] = $fProblemType['Problem']['id'];
			}
            if(isset($problemsTreeList[$updateArray['FoundProblem.problem_id']])){
                $updateArray['FoundProblem.super_parent_problem_id'] = current(array_reverse(array_keys($problemsTreeList[$updateArray['FoundProblem.problem_id']])));
            }
			$this->updateAll($updateArray, array('FoundProblem.id'=>$foundProblemsIdsToUpdate));
		}
	}

	public function manipulate($sensorsIds){
        $sensorModel = ClassRegistry::init('Sensor');
		$sensorModel->bindModel(array('belongsTo'=>array(
			'Branch',
			'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.sensor_id = Sensor.id AND ApprovedOrder.status_id='.ApprovedOrder::STATE_IN_PROGRESS)),
			'Plan'=>array('foreignKey'=>false, 'conditions'=>array('Plan.id = ApprovedOrder.plan_id')),
		)));
		$sensors = $sensorModel->find('all', array('conditions'=>array('Sensor.id'=>$sensorsIds)));
        foreach($sensors as $fSensor){
        	$fFoundProblem = $this->findCurrent($fSensor[Sensor::NAME]['id']);
            $parameters = array(&$fSensor,&$fFoundProblem);
            $this->callPluginFunction('FoundProblem_beforeManipulateCycleStart_Hook', $parameters, Configure::read('companyTitle'));
        	if(trim($fSensor[Sensor::NAME]['inform_about_problems'])){
	            if($fFoundProblem){
	                $diffMins = floor((strtotime($fFoundProblem['FoundProblem']['end']) - strtotime($fFoundProblem['FoundProblem']['start'])) / 60);
	                $this->sendMailsOnProblemOutgo($fFoundProblem, $fSensor, $diffMins);
	            }
	        }
	        if(trim($fSensor[Sensor::NAME]['new_fproblem_when_current_exceeds'])){//Virtimas i kita prastova pamainos mastu
	            if($fFoundProblem){
	                $diffMins = floor((strtotime($fFoundProblem['FoundProblem']['end']) - strtotime($fFoundProblem['FoundProblem']['start'])) / 60);
	                $this->createNewFProblemWhenCurrentExceedsInShift($fFoundProblem, $fSensor, $diffMins);
	            }
	        }
	        if(trim($fSensor[Sensor::NAME]['new_fproblem_when_current_exceeds2'])){//Virtimas i kita prastovas esamos prastovos mastu kai prastova, kuri turi luzti ir prasideti nauja, vyksta siuo metu
	            if($fFoundProblem){
	                $diffMins = floor((strtotime($fFoundProblem['FoundProblem']['end']) - strtotime($fFoundProblem['FoundProblem']['start'])) / 60);
	                $this->createNewFProblemWhenCurrentExceeds($fFoundProblem, $fSensor, $diffMins);
	            }
	        }
	        if(isset($fSensor['ApprovedOrder']['id']) && $fSensor['ApprovedOrder']['id'] > 0 && !empty($fFoundProblem)){
	            if($fFoundProblem['FoundProblem']['problem_id'] == Problem::ID_NEUTRAL && $fFoundProblem['FoundProblem']['transition_problem_id']){
	                $fProblemJsonData = json_decode($fFoundProblem['FoundProblem']['json']);
	                if(!isset($fProblemJsonData->transition_exceeded)){//jei perejimas dar nera virsijes ir nuo jo nesusikure derinimo virsyjimo problema
	                    if(!isset($problemModel)){
	                    	$problemModel = ClassRegistry::init('Problem');
						}
	                    if(!isset($shiftModel)){
	                    	$shiftModel = ClassRegistry::init('Shift');
						}
	                    $problem = $problemModel->findById($fFoundProblem['FoundProblem']['transition_problem_id']);
						if(!isset($fShift[$fSensor[Sensor::NAME]['branch_id']])){
							$fShift[$fSensor[Sensor::NAME]['branch_id']] = $shiftModel->findCurrent($fSensor[Sensor::NAME]['branch_id']);
						}
	                    $this->createExceedingTransitionByTransition($fSensor, $fSensor, $fFoundProblem, $fShift[$fSensor[Sensor::NAME]['branch_id']], $problem);
	                }
	            }
	            
	        }
	    }
    }
    
}
