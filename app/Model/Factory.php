<?php
App::uses('AppModel', 'Model');
class Factory extends AppModel{
    const NAME = __CLASS__;
    
    public static function buildName(array $li, $external = true) {
        return $li[self::NAME]['name'];
    }
    
}
