<?php
class Mail extends AppModel{

    public $useTable = false;

    public function send($emailsList, $message = '', $subject=''){
        App::uses('CakeEmail', 'Network/Email');
        $mail = new CakeEmail('default');
        $parameters = array(&$mail, &$emailsList, &$message, &$subject);
        if($this->callPluginFunction('Mail_BeforeSendMail_Hook', $parameters, Configure::read('companyTitle')) === true){
            return null;
        }
        $mail->from('noreply@horasoee.eu');
        $mail->to($emailsList);
        $mail->subject($subject);
        $mail->emailFormat('both');
        $mail->send($message);
    }

    public function send_proxy_sms($text, $phoneNr=null, $subject = ''){
        $proxyData = Configure::read('SMS_PROXY_DATA');
        $url = "https://test.horasoee.eu/messages/execute_proxy_sms";
        if(!$phoneNr){
            if(!isset($proxyData['phone_nr']) || empty($proxyData['phone_nr'])){ return null; }
            $phoneNr = $proxyData['phone_nr'];
        }elseif(!is_array($phoneNr)){
            $phoneNr = explode(',', $phoneNr);
        }
		$message = array(
			'text' => $text,
			'to' => $phoneNr,
            'subject'=>$subject
		);
		$_body = json_encode($message, JSON_UNESCAPED_UNICODE );
		$ch = curl_init();
		$options = array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_USERPWD        => $proxyData['user'].":".$proxyData['password'],
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_SSL_VERIFYHOST => 2,
			CURLOPT_SSL_VERIFYPEER => true,
			CURLOPT_USERAGENT      => "Curl script",
			CURLOPT_VERBOSE        => true,
			CURLOPT_URL            => $url,
			CURLOPT_POST           => true,
			CURLOPT_POSTFIELDS     => $_body,
            CURLOPT_TIMEOUT        => 10,
            CURLOPT_CONNECTTIMEOUT => 10,
			CURLOPT_HTTPHEADER     => array(
				'Content-Type:application/json',
				'Content-Length: '.strlen($_body)
			)
		);
		curl_setopt_array($ch , $options);
		$output = curl_exec($ch);
//		if(curl_errno($ch)){
//			$errorNumber = curl_error($ch);
//			curl_close($ch);
//			return array('status'=>0, 'error'=>$errorNumber);
//		} else {
//			// {"status":1,"error":""} - sms išsiųstas
//			// {"status":0,"error":"Priežastis kodėl neišsiuntė"} - sms neišsiųstas
//			// error priežastis kol kas nerodo
//			return json_decode($output, true);
//		}
    }

}