<?php

App::uses('AppModel', 'Model');

/**
 */
class Status extends AppModel {
	const NAME = __CLASS__;
	
	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */
	public static function buildName(array $li, $external = true) {
		return $li[self::NAME]['name'];
	}
	
}
