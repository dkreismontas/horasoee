<?php

App::uses('AppModel', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');
App::uses('Line', 'Model');
App::uses('Plan', 'Model');
App::uses('Problem', 'Model');
App::uses('FoundProblem', 'Model');
App::uses('User', 'Model');
App::uses('DiffType', 'Model');

/**
 */
class DiffCard extends AppModel {
	const NAME = __CLASS__;
	
	const STATE_NEW = 0;
	const STATE_ACCEPTED = 1;
	const STATE_EXPLAINED = 2;
	const STATE_EXPLANATION_DENIED = 3;
	const STATE_COMPLETE = 4;
	
	const PFX_SUB_TYPE = '_sub_type';
	const PFX_SHIFT_MANAGER = '_shift_manager';
	const PFX_RESPONSIVE_SECTION = '_responsive_section';
	const PFX_QUALITY_SECTION = '_quality_section';
	const PFX_PRODUCTION_MANAGER = '_production_manager';
	
	/**
	 * Get state key value pair array,
	 * where key is state ID and value is state name.
	 * @return array
	 */
	public static function getStates() {
		return array(
			self::STATE_NEW => __('Naujas'),
			self::STATE_ACCEPTED => __('Priimtas'),
			self::STATE_EXPLAINED => __('Paaiškintas'),
			self::STATE_EXPLANATION_DENIED => __('Paaiškinimas atmestas'),
			self::STATE_COMPLETE => __('Baigtas')
		);
	}
	
	/** @return DiffCard */
	public function withRefs() {
		$this->bindModel(array(
            'hasOne'=>array(
                Problem::NAME => array(
                    'className' => Problem::NAME,
                    'foreignKey' => false,
                    'conditions'=>array('FoundProblem.problem_id = Problem.id')
                ),
            ),
			'belongsTo' => array(
				Sensor::NAME => array(
					'className' => Sensor::NAME,
					'foreignKey' => 'sensor_id'
				),
				Branch::NAME => array(
					'className' => Branch::NAME,
					'foreignKey' => 'branch_id'
				),
				Line::NAME => array(
					'className' => Line::NAME,
					'foreignKey' => 'line_id'
				),
				Plan::NAME => array(
					'className' => Plan::NAME,
					'foreignKey' => 'plan_id'
				),
				ApprovedOrder::NAME => array(
                    'className' => ApprovedOrder::NAME,
                    'foreignKey' => false,
                    'conditions'=>array('DiffCard.sensor_id = ApprovedOrder.sensor_id AND DiffCard.date > ApprovedOrder.created AND DiffCard.date < ApprovedOrder.end')
                ),
                'Product' => array(
                    'className' => 'Product',
                    'foreignKey' => false,
                    'conditions'=>array('Plan.product_id = Product.id')
                ),
				FoundProblem::NAME => array(
					'className' => FoundProblem::NAME,
					'foreignKey' => 'found_problem_id'
				),
				DiffType::NAME => array(
					'className' => DiffType::NAME,
					'foreignKey' => 'diff_type_id'
				),
				DiffType::NAME.self::PFX_SUB_TYPE => array(
					'className' => DiffType::NAME,
					'foreignKey' => 'diff_sub_type_id'
				),
				User::NAME.self::PFX_SHIFT_MANAGER => array(
					'className' => User::NAME,
					'foreignKey' => 'shift_manager_user_id'
				),
				User::NAME.self::PFX_RESPONSIVE_SECTION => array(
					'className' => User::NAME,
					'foreignKey' => 'responsive_section_user_id'
				),
				User::NAME.self::PFX_QUALITY_SECTION => array(
					'className' => User::NAME,
					'foreignKey' => 'quality_section_user_id'
				),
				User::NAME.self::PFX_PRODUCTION_MANAGER => array(
					'className' => User::NAME,
					'foreignKey' => 'production_manager_user_id'
				)
			)
		));
		return $this;
	}

    public function getByMO($mo_number) {
        $this->bindModel(
            array(
                'belongsTo' => array('Plan')
            ));
        $diff_cards = $this->find('all', array(
            'fields' => array(
                'Plan.mo_number',
                'DiffCard.sensor_id',
                'SUM(DiffCard.consequences) as sum_error'
            ),
            'conditions' => array(
                'Plan.mo_number' => $mo_number
            ),
            'group' => array(
                'DiffCard.sensor_id',
                'Plan.mo_number'
            )
        ));

        return $diff_cards;
    }
	
}
