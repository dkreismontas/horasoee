<?php

App::uses('AppModel', 'Model');
App::uses('Line', 'Model');
App::uses('Branch', 'Model');

/**
 */
class Sensor extends AppModel {
	const NAME = __CLASS__;
	
	const TYPE_NONE = 0;
	const TYPE_MIXING = 1;
	const TYPE_SLICING = 2;
	const TYPE_PACKING = 3;
    //public $virtualFields = array('name'=>'CONCAT(Sensor.name,\' \',Factory.name)');
	
	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */
	public static function buildName(array $li, $external = true) {
		$types = self::getTypes();
		$name = 'ID:'.$li[self::NAME]['id'].' | '.$li[self::NAME]['name']
				.(isset($li['Factory']) && trim($li['Factory']['name']) ? ' '.$li['Factory']['name'] : '')
				//.(isset($types[$li[self::NAME]['type']]) ? (' '.$types[$li[self::NAME]['type']]) : '')
				.(isset($li[self::NAME]['ps_id']) && trim($li[self::NAME]['ps_id']) ? (' ('.$li[self::NAME]['ps_id'].')') : '')
				.(($external && isset($li[Branch::NAME]['name']) && trim($li[Branch::NAME]['name'])) ? (' ('.$li[Branch::NAME]['name'].')') : '');
        return $name;
	}
	
	/**
	 * Get type key value pair array,
	 * where key is type ID and value is type name.
	 * @return array
	 */
	public static function getTypes() {
		return array(
			self::TYPE_NONE => '',//__('Nepriskirta'),
			self::TYPE_MIXING => __('Linijos pradžia'),
			self::TYPE_SLICING => __('Linijos vidurys'),
			self::TYPE_PACKING => __('Linijos pabaiga')
		);
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false, $conditions=array(), $printAll = false) {
	    if(!$printAll && is_object(Configure::read('user'))){
	       $conditions['Sensor.id'] = Configure::read('user')->selected_sensors;
        }
		$items = $this->withRefs()->find('all',array('conditions'=>array_merge($conditions,array('is_partial'=>0)), 'order'=>array('Sensor.name')));
		$options = array();
		if ($addEmpty) {
			$options[''] = __('-- Nepasirinktas --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li);
		}
        $parameters = array(&$options);
        $this->callPluginFunction('Sensor_afterGetAsSelectOptions_Hook', $parameters, Configure::read('companyTitle'));
		return $options;
	}
	
	/** @return Sensor */
	public function withRefs() {
		$this->bindModel(array(
//			'hasAndBelongsToMany' => array(
//				Line::NAME => array(
//					'className' => Line::NAME,
//					'joinTable' => 'line_sensors',
//					'foreignKey' => 'sensor_id',
//					'associationForeignKey' => 'line_id',
//				)
//			),
			'belongsTo' => array(
				Branch::NAME => array(
					'className' => Branch::NAME,
					'foreignKey' => 'branch_id'
				),
                Line::NAME => array(
					'className' => Line::NAME,
				),
				'Factory' => array(
					'foreignKey' => false,
					'conditions'=>array('Branch.factory_id = Factory.id')
				)
			)
		));
		return $this;
	}

    public function getRelatedSensors($sensorId,$lineId,$plainList = false, $includeSelfSensor = false) {
	    if($lineId == null) {
            $lineId = $this->findById($sensorId)['Sensor']['line_id'];
        }
	    if(!$lineId){ return array(); }
        $conditions = array(
            'Sensor.line_id'=>$lineId,
        );
	    if(!$includeSelfSensor){
            $conditions['Sensor.id !='] = $sensorId;
        }
	    $otherSensors = $this->find('all',array(
            'conditions'=>$conditions
        ));
        if(!$plainList) {
            return $otherSensors;
        }
        return Set::extract('/Sensor/id',$otherSensors);
    }

	public function uploadImage(&$data){
	      $file_name = $data['image']['name'];
		  if(!trim($file_name)){unset($data['image']); return false; }
	      $file_size = $data['image']['size'];
	      $file_tmp = $data['image']['tmp_name'];
	      $file_type = $data['image']['type'];
	      $file_ext=current(array_reverse(explode('.',$file_name)));
	      $extensions= array("jpeg","jpg","png");
	      if(in_array($file_ext,$extensions)=== false){
	         $errors[]="extension not allowed, please choose a JPEG or PNG file.";
	      }
	      if($file_size > 2097152) {
	         $errors[]='File size must be excately 2 MB';
	      }
	      if(empty($errors)==true) {
	      	 $data['image'] = "sensor-id-{$data['id']}.$file_ext";
	         move_uploaded_file($file_tmp,ROOT.'/app/webroot/img/'.$data['image']);
	      }
	}

	public function checkDataCorrectivity($requestData){
	    $errors = array();
	    if(!trim($requestData['Sensor']['name'])){
            $errors[] = __('Įrašykite sensoriaus pavadinimą');
        }
        $parameters = array(&$errors, &$requestData);
        $this->callPluginFunction('Sensor_afterCheckDataCorrectivity_Hook', $parameters, Configure::read('companyTitle'));
	    return $errors;
    }
    
    //visiems senoriams prideda skydelio uzprenumeruotu grafiku, kad jie siustusi id:-1 vartotojo mailais
    public function addDashboardsToFakeUser(){
    	return false;
        $dashboardsSettingModel = ClassRegistry::init('DashboardsSetting');
        $sensors = $this->find('list',array('fields'=>array('Sensor.id','Sensor.name'),'conditions'=>array('Sensor.pin_name <>'=>'')));
        $fakeDashboards = $dashboardsSettingModel->find('list', array('fields'=>array('DashboardsSetting.data'),'conditions'=>array('DashboardsSetting.user_id'=>-1)));
        foreach($sensors as $sensorId => $sensorName){
            $sensorHasFakeDashboards = array_filter($fakeDashboards, function($data)use($sensorId){
                $formSettings = unserialize($data);
                return isset($formSettings['Node']['sensors']) && in_array($sensorId,$formSettings['Node']['sensors']); 
            });
            if(!empty($sensorHasFakeDashboards)){ continue; }
            $dashboardssToCreate = array(
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Prienamumas su išimtimis" praėjęs mėnuo iki dabar'), 'indicator1'=>24, 'graph_type'=>1, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjęs mėnuo iki dabar'), 'time_pattern'=>9, 'time_group'=>1, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Gamybos laikas" praėjęs mėnuo iki dabar'), 'indicator1'=>4, 'graph_type'=>1, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjęs mėnuo iki dabar'), 'time_pattern'=>9, 'time_group'=>1, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Darbo laikai" praėjęs mėnuo'), 'indicator1'=>18, 'graph_type'=>2, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjęs mėnuo'), 'time_pattern'=>3, 'time_group'=>0, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Darbo laikai" praėjusi savaitė'), 'indicator1'=>18, 'graph_type'=>2, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjusi savaitė'), 'time_pattern'=>2, 'time_group'=>0, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Darbo laikai" praėjusi para'), 'indicator1'=>18, 'graph_type'=>2, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjusi para'), 'time_pattern'=>1, 'time_group'=>0, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
                array('Node'=>array('uid'=>'uid'.substr(md5(rand(10000,99999)),0,15), 'id'=>0, 'type'=>1, 'graph_name'=>__($sensorName.' "Darbo laikai" praėjusi pamaina'), 'indicator1'=>18, 'graph_type'=>2, 'sensors'=>array($sensorId), 'shifts'=>array('-1'), 'date_start_end'=>__('Praėjusi pamaina'), 'time_pattern'=>0, 'time_group'=>0, 'action'=>1, 'decimal_count'=>'', 'order_columns_desc'=>'', 'trendline'=>'', 'combine_all_sensors'=>'', 'export_type'=>0, 'base64_img_src'=>0, 'ident'=>'', 'file_name'=>'')),
            );
            foreach($dashboardssToCreate as $dashboardsToCreate){
                $dashboardsSettingModel->create();
                $dashboardsSettingModel->save(array('user_id'=>-1,'type'=>1,'data'=>serialize($dashboardsToCreate),'subscribed_users'=>-1));
            }
        }
    }

    //papildoma funkcija kuri tikrina ar irasai eina i duomenu baze. Nes gali buti, jog irasus is dezute gaunam, taciau procedura ju neirase, nors klaidos ir neismete
    public function informAboutProblems($ipAndPort=''){
        $settingsModel = ClassRegistry::init('Settings');
        $mailModel = ClassRegistry::init('Mail');
        $settings = $settingsModel->getOne('warn_about_no_records');
        if(!trim($settings)){ return null; }
        $recordsDowntimeDuration = $periodicity = 60;
        $durations = explode(':', $settings);
        if(isset($durations[1])){
            $durationsSetting = explode('_', $durations[1]);
            $recordsDowntimeDuration = is_numeric(trim($durationsSetting[0]))?floatval(trim($durationsSetting[0])):$recordsDowntimeDuration;
            $periodicity = isset($durationsSetting[1]) && is_numeric(trim($durationsSetting[1]))?floatval(trim($durationsSetting[1])):$periodicity;
        }
	    $sensorModel = ClassRegistry::init('Sensor');
        $queryParameters = array(
            'fields'=>array('Sensor.id','Sensor.ps_id','Sensor.pin_name','Sensor.last_norecordsmailsend_time','Sensor.name'),
            'conditions'=>array(
                'SUBSTR(TRIM(Sensor.pin_name),1,1)'=>'i',
                'Sensor.last_norecordsmailsend_time <'=>date('Y-m-d H:i:s',time()-$periodicity*60)
            )
        );
        if(trim($ipAndPort)){
            $queryParameters['conditions']['Sensor.port'] = $ipAndPort;
        }
        //TODO Leisti tik jei kreipinys ne is shello, reikia Hampidjanui sio hookso
        if(method_exists($this, 'callPluginFunction')) {
            $parameters = array(&$queryParameters);
            $this->callPluginFunction('Sensor_beforeInformAboutProblems_Hook', $parameters, Configure::read('companyTitle'));
        }
        $realSensors = Hash::combine($sensorModel->find('all', $queryParameters),'{n}.Sensor.id', '{n}.Sensor');
        $recordModel = ClassRegistry::init('Record');
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList)){ echo 'Neteisingas warn_about_no_records nustatymo formatas. Nustatymas turi prasideti el pastu sarasu, atskirtu simboliu "|"'; return null; }
        $lastRecordsTmp = $recordModel->find('all', array(
            'fields'=>array('Record.sensor_id','MAX(Record.created) AS last_created'), 
            'conditions'=>array(
                'Record.created >='=>date('Y-m-d H:i:s', time()-$recordsDowntimeDuration*60),
                'Record.sensor_id'=>array_keys($realSensors)
            ),
            'recursive'=>-1,
            'group'=>array('Record.sensor_id')
        ));
        $lastRecords = array();
        foreach($lastRecordsTmp as $lastRecord){
            $sensorId = $lastRecord['Record']['sensor_id'];
            $lastRecords[$sensorId] = $lastRecord[0]['last_created'];
        }
        $message = '';
        $sensorsHavingmailsList = array();
        $caseString = 'CASE Sensor.id WHEN 0 THEN 0';
        $currentTime = date('Y-m-d H:i:s');
        foreach($realSensors as $sensorId => $realSensor){
            $recordTime = $lastRecords[$sensorId]??null;
            if($recordTime == null || (time() - strtotime($recordTime) >= $recordsDowntimeDuration*60)){
                $message .= sprintf('Negaunami duomenys iš sensoriaus, kurio identifikatorius: %s, pavadinimas: %s, kojelės nr.: %s. Dabarties momentas: %s.'."\r\n", $realSensor['ps_id'],$realSensor['name'],$realSensor['pin_name'],date('Y-m-d H:i:s'));
                $sensorsHavingmailsList[] = $realSensor['id'];
                $caseString .= ' WHEN '.$sensorId.' THEN \''.$currentTime.'\'';
            }else{
                $caseString .= ' WHEN '.$sensorId.' THEN \''.$recordTime.'\'';
            }
        }
        $caseString .= ' ELSE `last_norecordsmailsend_time` END';
        $this->updateAll(array('last_norecordsmailsend_time'=>$caseString),array('Sensor.id'=>array_keys($realSensors)));
        if(trim($message)){ 
//            $headers = "Content-Type: text/plain; charset=UTF-8";
//            mail(implode(',',$emailsList),"Įrašų siuntimo problema ".Configure::read('companyTitle'),$message,$headers);
            $mailModel->send($emailsList, $message, "Įrašų siuntimo problema ".Configure::read('companyTitle'));
        }
    }

    public function moveOldRecords($backupsOldRecords = true) {
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $db = ConnectionManager::getDataSource('default');
        $interval = trim(Configure::read('RECORDS_LIVE_INTERVAL'))?Configure::read('RECORDS_LIVE_INTERVAL'):'1 WEEK';
        $queries = array();
        if($backupsOldRecords){
            $queries[] = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 10000 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;';
        }
        $queries[] = 'delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 10000;';
        $db->query(implode('',$queries));
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
    }
    
    public function checkRecordsIsPushing($message,$type=1){
        //pr($message);
        $markerPath = __dir__.'/../Plugin/'.Configure::read('companyTitle').'/webroot/files/last_push_check.txt';
        if(!file_exists($markerPath)){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, json_encode(array('message_send_time'=>time()+300, 'send_after_5min'=>1))); //leisime siusti pirma maila uz 5min jei vis dar neis irasai
            fclose($fh);
            return false;
        }
        $settingsModel = ClassRegistry::init('Settings');
        $mailModel = ClassRegistry::init('Mail');
        $settings = $settingsModel->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList))return;
        $recordsDowntimeDuration = $periodicity = 60;
        $durations = explode(':', $settings);
        if(isset($durations[1])){
            $durationsSetting = explode('_', $durations[1]);
            $recordsDowntimeDuration = is_numeric(trim($durationsSetting[0]))?floatval(trim($durationsSetting[0])):$recordsDowntimeDuration;
            $periodicity = isset($durationsSetting[1]) && is_numeric(trim($durationsSetting[1]))?floatval(trim($durationsSetting[1])):$periodicity;
        }
        switch($type){
            case 1: $subject = __("Įrašų siuntimo problema ").Configure::read('companyTitle'); break;
            case 2: $subject = (Configure::read('ADD_FULL_RECORD')?'ADD_FULL_RECORD':'ADD_RECORD').' '.__('vykdymo problema').' '.Configure::read('companyTitle'); break;
        }
        $fh = fopen($markerPath, "r+") or die("Unable to open file!");
        $messageSendInfo = json_decode(fread($fh,filesize($markerPath)),true);
        if(isset($messageSendInfo['message_send_time']) && time() - $messageSendInfo['message_send_time'] >= 0){
            $mailModel->send($emailsList, implode(";\n",$message), $subject);
//            if(isset($messageSendInfo['send_after_5min'])){
//                $mailModel->send_proxy_sms(implode(";\n",$message),null,Configure::read('companyTitle'));
//            }
            ftruncate($fh, 0);
            fseek($fh, 0);
            fwrite($fh, json_encode(array('message_send_time'=>time()+$periodicity*60))); //leisime siusti sekanti maila uz nustatyme nurodyto laiko kas kiek siusti mailus
        }
        fclose($fh);
    }

    public function logFailedQuery($query){
        $path = __dir__.'/../Plugin/'.Configure::read('companyTitle').'/webroot/files/failed_records_import_queries.txt';
        $fh = fopen($path, 'a');
        fwrite($fh, json_encode(array('query'=>$query, 'date'=>date('Y-m-d H:i:s')))."\n");
        fclose($fh);
    }

    public function executeFailedQueries(){
    	$markerPath = __dir__.'/../Plugin/'.Configure::read('companyTitle').'/webroot/files/failed_queries_execution_in_progress.txt';
        if(file_exists($markerPath) && time() - filemtime($markerPath) < 3600){return false;}
        $path = __dir__.'/../Plugin/'.Configure::read('companyTitle').'/webroot/files/failed_records_import_queries.txt';
        if(file_exists($path)){
          $handle = fopen($path, 'r+');
          $line = json_decode(fgets($handle, 2000),true);
		  $remainingFileData = stream_get_contents($handle);
		  if(!is_array($line) && !trim($remainingFileData)){ return true; }
		  $markerOpen = fopen($markerPath, 'w');
          fwrite($markerOpen, 1); 
          fclose($markerOpen);
          try{
	          	if(isset($line['query']) && isset($line['date']) && trim($line['query']) && time() - strtotime($line['date']) < 3600 * 24){
	                $this->query($line['query']);
				}
                ftruncate($handle, 0);
                fseek($handle, 0);
                fwrite($handle, $remainingFileData);
                fclose($handle);
                $this->executeFailedQueries();
           }catch(Exception $e){
               fclose($handle);
			   unlink($markerPath);
               return false;
           }
        }
		if(file_exists($markerPath)){unlink($markerPath);}
		return true;
    }

    public function executeRecordsQueries(String $path, Array &$mailOfErrors){
        if(file_exists($path)){
          $handle = fopen($path, 'r+');
          $line = json_decode(fgets($handle, 2000),true);
		  $remainingFileData = stream_get_contents($handle);
		  if(!is_array($line) && !trim($remainingFileData)){
              unlink($path);
		      return true;
		  }
          try{
	          	//if(isset($line['query']) && isset($line['date']) && trim($line['query']) && time() - strtotime($line['date']) < 3600 * 24){
	          	if(isset($line['query'])){
	                $this->query($line['query']);
				}
	          	if(isset($line['raw_query'])){
	                $this->query($line['raw_query']);
				}
                ftruncate($handle, 0);
                fseek($handle, 0);
                fwrite($handle, $remainingFileData);
				unset($remainingFileData);
                fclose($handle);
                $this->executeRecordsQueries($path, $mailOfErrors);
				return true;
           }catch(Exception $e){
		        $shiftModel = ClassRegistry::init('Shift');
                $shiftModel->extendAllBranchesShifts();
                fclose($handle);
                $mailOfErrors[] = __('Užklausos vykdymo klaida').': '.json_encode($e);
                return false;
           }
        }
    }

}
