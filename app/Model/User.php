<?php

App::uses('AppModel', 'Model');
App::uses('Branch', 'Model');
App::uses('Sensor', 'Model');
App::uses('Line', 'Model');

/**
 * Description of User
 */
class User extends AppModel {
	
	const NAME = __CLASS__;
	const ID_SUPER_ADMIN = 1;
	CONST ID_MODERATOR = 2;
	CONST ID_GUEST = 3;
	CONST ID_OPERATOR = 4;
    var $belongsTo = array('Group');
    var $validate = array();
    //public $virtualFields = array('full_name'=>'CONCAT(User.first_name,\' \',User.last_name)');
	
	
	function __construct(){
        if(isset($_POST['data']['Private'])){
            $this->data['User'] = $_POST['data']['Private'];
        }
        $this->validate = array(
          'username' => array(
            'unique_name' => array(
                'rule' =>array('uniqueUsername'),
                'message' =>__('Toks vartotojo vardas jau egzistuoja',true)
            ),
            'empty_name' => array(
                'rule' =>'notBlank',
                'message' =>__('Įveskite vartotojo vardą',true)
            )
          ),
        );
        parent::__construct();
    }
    
    function uniqueUsername($data,$field){
	    if(!isset($this->data[$this->name]['id'])){ return true; }
        $exist = $this->find('first',array('conditions'=>array('User.username'=>current($data), 'User.id <>'=>$this->data[$this->name]['id'])));
        if (empty($exist)){
            return true;
        }else return false;
    }
    
    //tikrina prisijungimoi faila, jei tuscias, iraso IP bei vartotojo id ir atlogina. Kita karta jei nuskacius faila ras siuos duomenis, nebeatlogins
    /*public function checkAllUsersLogoutStatus($user, &$cookieComponent){
        $usersLougoutFile = ROOT."/app/tmp/logs/usersLougout.txt";
        if(empty($user) || !file_exists($usersLougoutFile)) return true;
        $handle = fopen($usersLougoutFile, "r");
        while (!feof($handle)) {
            $lineData = json_decode(fgets($handle, 4000));
            if(is_object($lineData) &&  //patikra ar apskirai eilute korektiska
                isset($lineData->userId) && $lineData->userId == $user['id'] && //patikra ar sutampa esamo prisiloginusio ir iregistruoto faile user id
                isset($lineData->ip) && $lineData->ip == $_SERVER['REMOTE_ADDR'] 
            ) return true;
        }
        fclose($handle);
        $cookieComponent->write('login','',true,-1,'/');
        $handle = fopen($usersLougoutFile, "a+");
        fwrite($handle, json_encode(array('userId'=>$user['id'], 'ip'=>$_SERVER['REMOTE_ADDR']))."\r\n");
        fclose($handle);
        $cookieComponent->delete('login');
        return false;
        //jei vartotojas nerastas faile, irasome ji i faile ir loginame lauk (pasaliname cookius)
    }*/
    
    public function constructRestrictionData($restrictions){
        foreach($restrictions as &$restriction){
            $restriction = array_filter($restriction, function($data){
                return $data >= 0;
            });
        }
        return json_encode($restrictions);
    }
	
	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @param string $pfx data key postfix
	 * @return string
	 */
	public static function buildName(array $li, $external = true, $pfx = null) {
		$key = self::NAME.($pfx ? $pfx : '');
		return (($li[$key]['first_name'] && $li[$key]['last_name']) ? ($li[$key]['first_name'].' '.$li[$key]['last_name']) : $li[$key]['username'])
			.($external ? ($li[$key]['email'] ? (' ('.$li[$key]['email'].')') : '') : '');
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @return array
	 */
	public function getResponsiveAsSelectOptions($addEmpty = false) {
		$items = $this->find('all', array('conditions' => array(self::NAME.'.responsive' => 1)));
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- Nepasirinktas --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li);
		}
		return $options;
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false) {
		$items = $this->find('all');
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- Nepasirinktas --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li);
		}
		return $options;
	}
	
	/** @return User */
	public function withRefs() {  
		$this->bindModel(array(
			'belongsTo' => array(
				Sensor::NAME => array(
					'className' => Sensor::NAME,
					'foreignKey' => 'sensor_id'
				),
                Branch::NAME => array(
                    'className' => Branch::NAME,
                    'foreignKey' => false,
                    'conditions'=>array('Branch.id = Sensor.branch_id')
                ),
				'Factory'=>array(
                    'foreignKey'=>false,
                    'conditions'=>array('Branch.factory_id = Factory.id')
                ),
				 Line::NAME => array(
					 'foreignKey' => false,
                     'conditions'=>array('Sensor.line_id = Line.id')
				 )
			),
		));
		return $this;
	}
    
    public function getActiveUserInSensor(Array $fSensor){
		$activeUser = $this->find('first', array('conditions'=>array('User.active_sensor_id'=>$fSensor['Sensor']['id'])));
        if(empty($activeUser)){
        	$sensorModel = ClassRegistry::init('Sensor');
			$sensorModel->bindModel(array('belongsTo'=>array('User'=>array('foreignKey'=>false, 'conditions'=>array('Sensor.active_user_id = User.id')))));
			$activeUser = $sensorModel->find('first', array(
				'fields'=>array('User.*'),
				'conditions'=>array('Sensor.id'=>$fSensor['Sensor']['id'])
			));
        }
		return $activeUser;
	}

	public function getActiveUser($sensorId){
	    $user = $this->find('first', array('conditions'=>array('User.active_sensor_id'=>$sensorId)));
	    if(empty($user)){
	        $sensorModel = ClassRegistry::init('Sensor');
            $sensorModel->bindModel(array('belongsTo'=>array('User'=>array('foreignKey'=>false, 'conditions'=>array('Sensor.active_user_id = User.id')))));
	        $user = $sensorModel->find('first', array('conditions'=>array('Sensor.id'=>$sensorId)));
        }
	    return $user;
    }
	
}
