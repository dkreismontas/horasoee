<?php

/**
 */
class Product extends AppModel {
	const NAME = __CLASS__;

	public function checkDataCorrectivity($requestData){
        $errors = array();
        $product = $this->find('first', array('conditions'=>array('Product.production_code'=>$requestData['Product']['production_code'], 'Product.id <>'=>$requestData['Product']['id'])));
        if(!empty($product)){
            $errors[] = __('Gaminys su tokiu kodu jau yra sukurtas');
        }
        if(isset($requestData['Product']['production_code']) && !trim($requestData['Product']['production_code'])){
            $errors[] = __('Įrašykite gaminio kodą');
        }
        if(isset($requestData['Product']['name']) && !trim($requestData['Product']['name'])){
            $errors[] = __('Įrašykite gaminio pavadinimą');
        }
        $parameters = array(&$errors, &$requestData);
        $this->callPluginFunction('Product_afterCheckDataCorrectivity_Hook', $parameters, Configure::read('companyTitle'));
        return $errors;
    }
	
}
