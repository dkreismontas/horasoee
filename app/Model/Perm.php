<?php

App::uses('AppModel', 'Model');

/**
 */
class Perm extends AppModel {
	const NAME = __CLASS__;
	
	/**
	 * Get indexed (by key) permission list
	 * @return array
	 */
	public function getPerms() {
		$list = $this->find('all', array('order' => self::NAME.'.key ASC'));
		$ilist = array();
		foreach ($list as $perm) {
			$ilist[$perm[self::NAME]['key']] = $perm;
		}
		return $ilist;
	}
	
	/** Sync convenience method */
	public function resetUpdated() {
		$this->query("UPDATE `$this->table` SET `updated`=0", false);
	}
	
	/** Sync convenience method */
	public function removeNotUpdated() {
		$this->query("DELETE FROM `$this->table` WHERE `updated`=0", false);
	}
	
	/**
	 * Create and save new item to database
	 * @param string $key
	 * @param string $description
	 * @return array new record
	 */
	public function newItem($key, $description = null) {
		$this->create();
		if (!$key) throw new ErrorException("Bad permision key '".$key."'.");
		if ($this->save($arr = array(
			'key' => $key,
			'description' => $description,
			'updated' => 1
		))) {
			$arr['id'] = intval($this->id);
			return array(self::NAME => $arr);
		}
		return null;
	}
	
}
