<?php
App::uses('Problem', 'Model');
class OeeMpmSoapServer extends AppModel{
    
    CONST LOGIN = 'mpm_request';
    CONST PASS = 'Apy^ksX!34';
    private static $clientlogin;
    public $useTable = false;
    public $ApprovedOrder, $FoundProblem, $Plan, $Log = null;
    
    public function __construct(){
        $this->ApprovedOrder = ClassRegistry::init('ApprovedOrder'); 
        $this->Plan = ClassRegistry::init('Plan'); 
        $this->Sensor = ClassRegistry::init('Sensor'); 
        $this->FoundProblem = ClassRegistry::init('FoundProblem'); 
        $this->Log = ClassRegistry::init('Log');
    }
    
    public function set_login($login){
        self::$clientlogin = $login;
    }
    
    private static function check_login(){
        if(self::$clientlogin->username != self::LOGIN || self::$clientlogin->password != self::PASS){
            die('Neteisingi prisijungimo duomenys');
        }
    }
    
    public function get_data_from_oee($dataFromMpm=''){
        self::check_login();
        $this->insertPlans($dataFromMpm);
        $data = array(
            'plans_quantities'=>$this->getPlansQuantities(),
            'work_intervals'=>$this->getWorkIntervals()
        );
        $parameters = array(&$data);
        $this->callPluginFunction('OeeMpm_afterGetDataFromOee_Hook', $parameters, Configure::read('companyTitle'));
        return $data; 
    }
    
    private function getPlansQuantities(){ 
        $this->ApprovedOrder->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $plansQuantitiesTmp = $this->ApprovedOrder->find('all', array(
            'fields'=>array(
                'SUM(IF(Sensor.plan_relate_quantity = \'quantity\', ApprovedOrder.box_quantity, ApprovedOrder.quantity)) AS quantity',
                'TRIM(Plan.mo_number) AS mo_number',
                'TRIM(Plan.id) AS plan_id',
                'GROUP_CONCAT(ApprovedOrder.status_id) AS state',
                'Sensor.ps_id'
            ),'conditions'=>array(
                'Plan.exported_to_mpm'=>0, 'Sensor.has_mpm_attribute'=>1
            ),'group'=>array('ApprovedOrder.plan_id'),
            'limit'=>100 //TODO pasalinti
        ));
        $parameters = array(&$plansQuantitiesTmp);
        $this->callPluginFunction('OeeMpm_afterGetPlansQuantitiesSearch_Hook', $parameters, Configure::read('companyTitle'));
        $plansQuantities = array();
        $finishedPlansIds = array();
        foreach($plansQuantitiesTmp as $activePlan){
            $activePlan[0]['finished'] = in_array(ApprovedOrder::STATE_COMPLETE, explode(',', $activePlan[0]['state']));
            $plansQuantities[$activePlan['Sensor']['ps_id']][] = $activePlan[0];
            if($activePlan[0]['finished']){
                $finishedPlansIds[] = $activePlan[0]['plan_id'];
            }
        }
        if(!empty($finishedPlansIds)){
            $this->Plan->updateAll(array('Plan.exported_to_mpm'=>1), array('Plan.id'=>$finishedPlansIds));
        }
        unset($plansQuantitiesTmp);
        return $plansQuantities;
    }

    private function insertPlans($dataFromMpm){
        $plans = json_decode($dataFromMpm,true)['plans'];
        if(empty($plans)){ return array(); }
        $sensorsList = $this->Sensor->find('list', array('fields'=>array('Sensor.ps_id','Sensor.id')));
        $this->Plan->bindModel(array('belongsTo'=>array('ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.plan_id = Plan.id')))));
        $plansToDelete = $this->Plan->find('all', array('fields'=>array('Plan.id'),'conditions'=>array('ApprovedOrder.id IS NULL')));
        if(!empty($plansToDelete)){
            $plansIdsToDelete = Set::extract('/Plan/id', $plansToDelete);
            $this->Log->write('Vykdomas uzsakymu importavimas is MPM. Salinami sie planai pagal id, kuriems nesukurtas approved_orders irasas. '.json_encode($plansIdsToDelete));
            $this->Plan->deleteAll(array('Plan.id'=>$plansIdsToDelete));
        }
        $moNumbersList = $this->Plan->find('list', array(
            'fields'=>array('Plan.mo_number', 'Plan.id'),
            'conditions'=>array('Plan.mo_number'=>Set::extract('/mo_number',$plans)),
            'order'=>array('Plan.id DESC'),
            'limit'=>5000
        ));
        foreach($plans as $key => $plan){
            if(!isset($sensorsList[$plan['ps_id']])){ unset($plans[$key]); continue; }
            $planId = $moNumbersList[$plan['mo_number']]??0;
            if($plan['finished'] == 1 && $planId > 0){
                $this->ApprovedOrder->updateAll(array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE),
                    array('ApprovedOrder.status_id <>'=>ApprovedOrder::STATE_IN_PROGRESS, 'ApprovedOrder.plan_id'=>$planId)
                );
            }else{
                $plans[$key]['sensor_id'] = $sensorsList[$plan['ps_id']];
                if($planId > 0) {
                    $plans[$key]['id'] = $planId;
                    $plans[$key]['exported_to_mpm'] = 0; //2020-08-19 grazinimo atveju visad reikia nuresetinti si parametra planui
                    $this->ApprovedOrder->updateAll(    //2020-08-19 jei uzsakymas jau buvo uzdarytas, bet jis pareina is MPM (grazinamas), OEE jis turi vel atsidaryti. Duodamas 5min uzdelsimas, kad OEE uzdarytas uzsakymas neatsidarytu del tuo paciu metu MPM ivykusio perplanavimo
                        array('status_id'=>ApprovedOrder::STATE_POSTPHONED),
                        array(
                            'ApprovedOrder.plan_id'=>$planId,
                            'ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE,
                            'ApprovedOrder.end <'=>'\''.date('Y-m-d H:i:s', time()-300).'\''
                        )
                    );
                }
                $this->Plan->create();
                $this->Plan->save($plans[$key]);
            }
        }
        return $plans;
    }

    private function getWorkIntervals(){
        $this->FoundProblem->bindModel(array('belongsTo'=>array('Sensor','Problem','Plan','User')));
        $workIntervals = $this->FoundProblem->find('all', array(
            'fields'=>array(
                'TRIM(FoundProblem.start) AS start',
                'TRIM(FoundProblem.end) AS end',
                'TRIM(Problem.name) AS problem_name',
                'TRIM(Sensor.ps_id) AS ps_id',
                'TRIM(Plan.mo_number) AS mo_number',
                'TRIM(FoundProblem.quantity) AS quantity',
                'TRIM(FoundProblem.id) AS oee_time_interval_id',
                'TRIM(User.unique_number) AS worker_tab_nr',
                'TRIM(FoundProblem.comments) AS comments',
                'TRIM(FoundProblem.plan_id) AS plan_id',
                'TRIM(FoundProblem.state) AS state',
                'TRIM(FoundProblem.problem_id) AS problem_id',
                'FoundProblem.plan_id',
                'FoundProblem.sensor_id',
            ),'conditions'=>array(
                'FoundProblem.exported_to_mpm'=>0,
                'Sensor.has_mpm_attribute'=>1,
                'FoundProblem.plan_id >'=>0,
                'OR'=>array(
                    array('FoundProblem.problem_id'=>array(Problem::ID_WORK, Problem::ID_NEUTRAL), 'TIMESTAMPDIFF(SECOND, FoundProblem.start, FoundProblem.end) >='=>60),
                    array('FoundProblem.problem_id >'=>Problem::ID_NOT_DEFINED)
                )
            )
        ));
        $parameters = array(&$workIntervals);
        $this->callPluginFunction('OeeMpm_afterGetWorkIntervalsSearch_Hook', $parameters, Configure::read('companyTitle'));
        $needToAddMarkerIntervals = array_filter($workIntervals, function($interval){
            return $interval[0]['state'] == FoundProblem::STATE_COMPLETE && ($interval[0]['problem_id'] > Problem::ID_NOT_DEFINED || in_array($interval[0]['problem_id'], array(Problem::ID_WORK, Problem::ID_NEUTRAL)));
        });
        $needToAddMarkerIntervals = Set::extract('/0/oee_time_interval_id', $needToAddMarkerIntervals);
        if(!empty($needToAddMarkerIntervals)){
            $this->FoundProblem->updateAll(array('FoundProblem.exported_to_mpm'=>1), array('FoundProblem.id'=>$needToAddMarkerIntervals));
        }
        $workIntervals = array_map(function($interval){ return $interval[0]; }, $workIntervals);
        return $workIntervals;
    } 
    
}
