<?php

App::uses('Model', 'Model');

/**
 * Description of AppModel
 */
class AppModel extends Model {
    
    public function callPluginFunction($methodTitle, &$parameters, $companyName){
        if(!trim($companyName)){
            $dataSource = ConnectionManager::getDataSource('default');
            if(isset($dataSource->config['plugin'])){
                $companyName = $dataSource->config['plugin'];
            }
        }
        //kviesime plugino modelyje esancia funkcija
        if(file_exists(ROOT.DS.'app'.DS.'Plugin'.DS.$companyName)){
            $companyMainPluginModel = ClassRegistry::init($companyName.'.'.$companyName);
            if(method_exists($companyMainPluginModel,$methodTitle)) {
                return call_user_func_array(array($companyMainPluginModel, $methodTitle), $parameters);
            }
        }
        //kviesime plugino controleryje esancia funkcija
        /*foreach (CakePlugin::loaded() as $plugin) {
            if(isset($this->enabledPlugins->$plugin) || $plugin != $companyName) continue;
            App::uses('PluginsController', $plugin.'.Controller');
            $contr = 'PluginsController';
            $company = new $contr();
            if(method_exists($company,$methodTitle)) {
                //$appendedRoles = $company->afterManualOrderPositioning($ordersSort['order']);
                return call_user_func_array(array($company, $methodTitle), $parameters);
            }
        }*/
    }

	function unbindModelAll($secondQueryUnbind = true) {
            foreach(array(
                    'hasOne' => array_keys($this->hasOne),
                    'hasMany' => array_keys($this->hasMany),
                    'belongsTo' => array_keys($this->belongsTo),
                    'hasAndBelongsToMany' => array_keys($this->hasAndBelongsToMany)
            ) as $relation => $model) {
                    $this->unbindModel(array($relation => $model),$secondQueryUnbind);
            }
    }

    public function hasAccessToAction($address, $permissionSet = false) {
        $user = Configure::read('user');
        if($user && $user->id == User::ID_SUPER_ADMIN) {
            //return true;
        }
        $controller = isset($address['controller'])?$address['controller']:'';
        $action = isset($address['action'])?$address['action']:'';
        $plugin = isset($address['plugin'])?$address['plugin']:'';
        $PermissionModel = ClassRegistry::init('Permission');
        $permission = $PermissionModel->find('first', array('conditions'=>array('controllers'=>$controller, 'actions'=>$action, 'plugin'=>$plugin)));
        if(empty($permission) || (!empty($permission) && empty(unserialize($permission['Permission']['users'])))){
            return !$permissionSet?true:false;
        }else{
            $users = unserialize($permission['Permission']['users']);
            if($users){
                $users = array_combine($users, $users);
                return array_search($user->group_id, $users);
            }
        }
    }

    // function find($conditions = null, $fields = array(), $order = null, $recursive = null) {
        // $data = parent::find($conditions, $fields, $order, $recursive);
        // array_walk_recursive($data, function(&$item, $key){
                    // $item = utf8_encode($item);
        // });
        // return $data;
    // }
    
    
}


