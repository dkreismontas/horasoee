<?php

App::uses('AppModel', 'Model');

/**
 */
class LossType extends AppModel {
	const NAME = __CLASS__;

    function __construct(){
        $this->validate = array(
            'line_ids' => array(
                'rule' => 'notEmpty',
                'message' =>__('Pasirinkite bent vieną liniją',true)
            )
        );
        parent::__construct();
    }
}
