<?php

App::uses('AppModel', 'Model');

/**
 */
class DiffType extends AppModel {
	const NAME = __CLASS__;
	
	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @param string $pfx data key postfix
	 * @return string
	 */
	public static function buildName(array $li, $external = true, $pfx = null) {
		$key = self::NAME.($pfx ? $pfx : '');
		return ($li[$key]['code'] ? ($li[$key]['code'].'. ') : '').$li[$key]['name'];
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @param int $parentId
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false, $parentId = null) {
		$items = $this->find('all', array('conditions' => array(self::NAME.'.parent_id' => $parentId), 'order' => self::NAME.'.id ASC'));
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- None --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li);
		}
		return $options;
	}
	
}
