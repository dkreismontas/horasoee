<?php
App::uses('TimelinePeriod', 'Controller/Component');
class WorkCenter extends AppModel{
    
    public $useTable = false;
    
    public function combineSensorsIntoColumns($sensors, $columnsCount){ 
        $rowsCount = ceil(sizeof($sensors) / $columnsCount);
        $data = array();
        for($i = 0; $i < $rowsCount; $i++){
            $data[$i] = array_values(array_slice($sensors, $i*$columnsCount, $columnsCount, true));
        }
        return $data;
    }

    public function attacheTechCardPath(&$plan, $fSensor){
        if(!isset($fSensor['Sensor']['tech_cards_path'])){ return null; }
        if(preg_match_all('/\[([^\]]+)\]/',$fSensor['Sensor']['tech_cards_path'],$match)){
            foreach($match[1] as $dynamicPart){
                $modelAndField = explode('.', $dynamicPart);
                if(sizeof($modelAndField)!=2){ continue; }
                list($model, $field) = $modelAndField;
                if(!isset($plan[$model][$field])){ continue; }
                if(!isset($plan['Plan']['tech_cards_path']) || !trim($plan['Plan']['tech_cards_path'])){
                    $plan['Plan']['tech_cards_path'] = $fSensor['Sensor']['tech_cards_path'];
                }
                $plan['Plan']['tech_cards_path'] = str_replace('['.$dynamicPart.']', $plan[$model][$field], $plan['Plan']['tech_cards_path']);
            }
            if(isset($plan['Plan']['tech_cards_path'])) {
                $plan['Plan']['tech_cards_path'] = base64_encode($plan['Plan']['tech_cards_path']);
            }
        }else{
            return null;
        }
    }

    public function showShiftChangeButton($fSensor, $currentShift){
        if(!isset($fSensor['Sensor']['manual_shift_change']) || !trim($fSensor['Sensor']['manual_shift_change'])){ return false; }
        if(isset($fSensor['Branch']['shift_change_init']) && $fSensor['Branch']['shift_change_init'] == 1){ return false; }
        if(strtotime($currentShift['Shift']['start']) + 3600 > time()){ return false; }
        $manualShiftChangeSettings = array_map(function($val){return (float)trim($val);}, explode(',', $fSensor['Sensor']['manual_shift_change']));
        if(sizeof($manualShiftChangeSettings) != 2){ return false; }
        $savedOriginalinfo = json_decode($currentShift['Branch']['last_shift_end'], true);
        $originalShiftEnd = !empty($savedOriginalinfo) && $savedOriginalinfo['shift_id'] == $currentShift['Shift']['id']?$savedOriginalinfo['end']:$currentShift['Shift']['end'];
        if(strtotime($originalShiftEnd) - $manualShiftChangeSettings[0] * 60 <= time()){ return true; }
        return false;
    }

    public function buildTimeGraph($fSensor, $graphHoursLinesCount = 0) {
        static $planModel, $problemModel, $recordModel, $foundProblemModel, $approvedOrderModel, $lossModel, $userModel = null;
        if($planModel == null){
            $planModel = ClassRegistry::init('Plan');
            $problemModel = ClassRegistry::init('Problem');
            $recordModel = ClassRegistry::init('Record');
            $foundProblemModel = ClassRegistry::init('FoundProblem');
            $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
            $lossModel = ClassRegistry::init('Loss');
            $userModel = ClassRegistry::init('User');
        }
        $hours = array();
        if(!isset($fSensor['Sensor']['timezone'])){ $fSensor['Sensor']['timezone'] = 'Europe/Vilnius'; }
        $tz = new DateTimeZone($fSensor['Sensor']['timezone']);
        $startPointDate = new DateTime();
        $startPointDate->setTimezone($tz);
        $dateNow = clone($startPointDate);
        if (isset($fSensor['dcTime']) && ($fSensor['dcTime'] instanceof DateTime)) {
            $dateNow = ($fSensor['dcTime'] < $dateNow) ? clone $fSensor['dcTime'] : $dateNow;
        }
        if(!$graphHoursLinesCount){
            $graphHoursLinesCount = Configure::read('graphHoursDefaultLinesCount') > 0?Configure::read('graphHoursDefaultLinesCount'):6;
        }
        $diffH = floor(($startPointDate->getTimestamp() - $dateNow->getTimestamp()) / 3600);
        if($diffH - $graphHoursLinesCount >= 0){
            $dateNow->add(new DateInterval('PT'.($graphHoursLinesCount).'H'));
        }else{
            $dateNow = clone $startPointDate;
        }
        $problemExclusionModel = ClassRegistry::init('ShiftTimeProblemExclusion');
        $exlusionProblemsList = $problemExclusionModel->find('list', array('fields'=>array('id','problem_id')));
        //$dateNow = new DateTime('2014-11-20 18:55:00');
        $toDateTime = clone $dateNow;
        $toDateTime2 = clone $dateNow;
        $toDateTime2->setTime(intval($toDateTime2->format('H')), 59, 59);
        $date = clone $dateNow;
        $date->setTime(intval($date->format('H')), 59, 59);
        $date->sub(new DateInterval('PT'.($graphHoursLinesCount-1).'H'));
        $date->setTime(intval($date->format('H')), 0, 0);
        $fromDateTime = clone $date;
        $dis = new DateInterval('PT1S');
        $backDis = new DateInterval('PT'.Configure::read('recordsCycle').'S');
        $periods = array();
        $periods[] = new TimelinePeriod(TimelinePeriod::TYPE_NONE, $fromDateTime, $toDateTime);
        $planPeriods = array();
        if($fSensor['Sensor']['show_plan_workcenter_intervals']){
            $planPeriods[] = new TimelinePeriod(TimelinePeriod::TYPE_TRANSITION, $fromDateTime, $toDateTime2);
            $fPlans = $planModel->find('all', array(
                'conditions' => array(
                    Plan::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Plan::NAME.'.end >=' => $fromDateTime->format('Y-m-d H:i:s'),
                    Plan::NAME.'.start <=' => $toDateTime2->format('Y-m-d H:i:s'),
                    Plan::NAME.'.disabled'=>0,
                ),
                'limit'=>30,
                'order' => array(Plan::NAME.'.start ASC')
            ));
            //$fPlans = $planModel->find('all', array('conditions'=>array('Plan.end >'=>$fromDateTime->format('Y-m-d H:i:s')), 'limit'=>30));
            foreach ($fPlans as $fPlan) {
                TimelinePeriod::binaryIntersect($planPeriods, new TimelinePeriod(
                    TimelinePeriod::TYPE_PLAN,
                    new DateTime($fPlan[Plan::NAME]['start']),
                    new DateTime($fPlan[Plan::NAME]['end']),
                    $fPlan
                ));
            }
            unset($fPlans);
        }

        $transitionProblems = Hash::combine($problemModel->find('all', array('conditions'=>array('transition_problem'=>1))),'{n}.Problem.id','{n}.Problem');
        $recordModel->bindModel(array('belongsTo'=>array('FoundProblem')));
        if(!isset($fSensor['Sensor']['dark_and_light_green_intervals']) || $fSensor['Sensor']['dark_and_light_green_intervals'] == 1) {
            $fRecords = $recordModel->find('all', array(
                'conditions' => array(
                    Record::NAME . '.sensor_id' => $fSensor[Sensor::NAME]['id'],
                    Record::NAME . '.created >=' => $fromDateTime->format('Y-m-d H:i:s'),
                    Record::NAME . '.created <=' => $toDateTime->format('Y-m-d H:i:s')
                ),
                'order' => array(Record::NAME . '.created ASC')
            ));
        }else{ $fRecords = array();} //formuojant tik vienos spalvos darbo intervalus recordu nereikia }

        $fFoundProblems = $foundProblemModel->withRefs()->find('all', array(
            'conditions' => array(
                FoundProblem::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                FoundProblem::NAME.'.end >=' => $fromDateTime->format('Y-m-d H:i:s'),
                FoundProblem::NAME.'.start <=' => $toDateTime->format('Y-m-d H:i:s'),
                Problem::NAME.'.for_speed' => 0,
                'OR'=>array(
                    FoundProblem::NAME.'.start <> '.FoundProblem::NAME.'.end',
                    FoundProblem::NAME.'.state' => FoundProblem::STATE_IN_PROGRESS,
                )
                //Problem::NAME.'.id <>' => -1,
            ),
            'order' => array(FoundProblem::NAME.'.start ASC')
        ));
        $parameters = array($fSensor, $fRecords, $fromDateTime->format('Y-m-d H:i:s'),$toDateTime->format('Y-m-d H:i:s'));
        if(!$gapsFilledAsProblem = $this->callPluginFunction('FoundProblem_fillEmptyGapsToBuildGraph_Hook', $parameters, Configure::read('companyTitle'))){
            //$gapsFilledAsProblem = $foundProblemModel->fillEmptyGaps($fSensor, $fRecords, $fromDateTime->format('Y-m-d H:i:s'),$toDateTime->format('Y-m-d H:i:s'));
        }
        $gapsFilledAsProblem = is_array($gapsFilledAsProblem)?$gapsFilledAsProblem:array();
        $fFoundProblems = array_merge($fFoundProblems, $gapsFilledAsProblem);
        $problemPeriods = array(new TimelinePeriod(TimelinePeriod::TYPE_NONE, $fromDateTime, $toDateTime));
        $shortProblemId = $duration = '';
        if(isset($fSensor['Sensor']['short_problem']) && trim($fSensor['Sensor']['short_problem'])){
            list($shortProblemId, $durationToShortProblem) = explode('_', $fSensor['Sensor']['short_problem']);
        }
        $fApprovedOrders = $approvedOrderModel->withRefs()->find('all', array(
            'conditions' => array(
                ApprovedOrder::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                ApprovedOrder::NAME.'.end >=' => $fromDateTime->format('Y-m-d H:i:s'),
                ApprovedOrder::NAME.'.created <=' => $toDateTime->format('Y-m-d H:i:s')
            ),
            'order' => array(ApprovedOrder::NAME.'.created ASC')
        ));
        $workDurations = Hash::combine($foundProblemModel->find('all', array(
            'fields'=>array('SUM(TIMESTAMPDIFF(SECOND,FoundProblem.start,FoundProblem.end)) AS duration', 'FoundProblem.plan_id'),
            'conditions'=>array(
                'FoundProblem.sensor_id'=>$fSensor['Sensor']['id'],
                'FoundProblem.plan_id'=>Set::extract('/ApprovedOrder/plan_id', $fApprovedOrders),
                'FoundProblem.problem_id'=>Problem::ID_WORK,
            ),'group'=>array( 'FoundProblem.plan_id')
        )),'{n}.FoundProblem.plan_id', '{n}.0.duration');
        $lossModel->bindModel(array('belongsTo'=>array('LossType')));
        $losses = $lossModel->find('all', array(
            'fields'=>array('SUM(Loss.value) AS losses_quantity', 'Loss.approved_order_id', 'Loss.loss_type_id', 'LossType.name','LossType.unit'),
            'conditions'=>array('Loss.approved_order_id'=>Set::extract('/ApprovedOrder/id', $fApprovedOrders)),
            'group'=>array('Loss.approved_order_id', 'Loss.loss_type_id')
        ));
        //kai yra recordu, approved_orders duomenys pareina formuojant sviesiai ir tamsiai zalius intervalus, todel reikia pilnos info apie orderius.
        $fApprovedOrders = array_map(function ($approvedOrder) use ($workDurations, $losses) {
            $appOrderPlanId = $approvedOrder['ApprovedOrder']['plan_id'];
            $appOrderId = $approvedOrder['ApprovedOrder']['id'];
            if (!empty($losses)) {
                $orderLosses = array_filter($losses, function ($loss) use ($appOrderId) {
                    return $loss['Loss']['approved_order_id'] == $appOrderId;
                });
                $approvedOrder['ApprovedOrder']['losses'] = array();
                foreach ($orderLosses as $orderLoss) {
                    $lossTypeId = $orderLoss['Loss']['loss_type_id'];
                    $approvedOrder['ApprovedOrder']['losses'][$lossTypeId] = array_merge(array('quantity' => $orderLoss[0]['losses_quantity']), $orderLoss['LossType']);
                }
            }
            $approvedOrder['ApprovedOrder']['work_duration_from_start'] = isset($workDurations[$appOrderPlanId]) ? $workDurations[$appOrderPlanId] : 0;
            return $approvedOrder;
        }, $fApprovedOrders);
        foreach ($fFoundProblems as $fFoundProblem){
            if($fFoundProblem['ApprovedOrder']['id'] > 0){
                $fFoundProblem[FoundProblem::NAME]['end'] = date('Y-m-d H:i:s',min(strtotime($fFoundProblem[FoundProblem::NAME]['end']), strtotime($fFoundProblem['ApprovedOrder']['end'])));
            }
            if($fFoundProblem[FoundProblem::NAME]['problem_id'] == Problem::ID_WORK){
                $appOrderPlanId = $fFoundProblem['ApprovedOrder']['plan_id'];
                $appOrderId = $fFoundProblem['ApprovedOrder']['id'];
                if(!empty($losses)){
                    $orderLosses = array_filter($losses, function($loss)use($appOrderId){ return $loss['Loss']['approved_order_id'] == $appOrderId; });
                    $approvedOrder['ApprovedOrder']['losses'] = array();
                    foreach($orderLosses as $orderLoss){
                        $lossTypeId = $orderLoss['Loss']['loss_type_id'];
                        $fFoundProblem['ApprovedOrder']['losses'][$lossTypeId] = array_merge(array('quantity'=>$orderLoss[0]['losses_quantity']), $orderLoss['LossType']);
                    }
                }
                $fFoundProblem['ApprovedOrder']['work_duration_from_start'] = isset($workDurations[$appOrderPlanId])?$workDurations[$appOrderPlanId]:0;
                $start = new DateTime($fFoundProblem[FoundProblem::NAME]['start']);
                $end = new DateTime($fFoundProblem[FoundProblem::NAME]['end']);
                $fFoundProblem['Work'] = $fFoundProblem['FoundProblem'];
                unset($fFoundProblem['FoundProblem']);
                TimelinePeriod::binaryIntersect($periods, new TimelinePeriod(TimelinePeriod::TYPE_PRODUCT, $start, $end, $fFoundProblem),true);
                continue;
            }
            if(isset($fFoundProblem[FoundProblem::NAME]['transition_problem_id']) && $fFoundProblem[FoundProblem::NAME]['transition_problem_id'] > 0 && isset($transitionProblems[$fFoundProblem[FoundProblem::NAME]['transition_problem_id']]) && $transitionProblems[$fFoundProblem[FoundProblem::NAME]['transition_problem_id']]){
                $fFoundProblem['Transition'] = $transitionProblems[$fFoundProblem[FoundProblem::NAME]['transition_problem_id']];
            }
//            CakeLog::write('debug', 'Periods:' . json_encode($fFoundProblem));
            if ($fFoundProblem[FoundProblem::NAME]['state'] == FoundProblem::STATE_IN_PROGRESS) {
                $fFoundProblem[FoundProblem::NAME]['end'] = $dateNow->format('Y-m-d H:i:s');
            }
            $start = new DateTime($fFoundProblem[FoundProblem::NAME]['start']);
            $end = new DateTime($fFoundProblem[FoundProblem::NAME]['end']);
            $diff = $end->diff($start, true);
            if($diff->s < Configure::read('recordsCycle')){
                $end->sub(new DateInterval('PT1S'));
            }
            if(in_array($fFoundProblem['FoundProblem']['problem_id'], $exlusionProblemsList)){
                $type = TimelinePeriod::TYPE_PROBLEM_EXCLUSIONS;
            }else{
                switch($fFoundProblem['FoundProblem']['problem_id']){
                    case 0: $type = TimelinePeriod::TYPE_SHUTDOWN; break;
                    case 3: $type = TimelinePeriod::TYPE_UNDEFINED; break;
                    case $shortProblemId: $type = TimelinePeriod::TYPE_SHORT_PROBLEM; break;
                    default: $type = TimelinePeriod::TYPE_PROBLEM;
                }
            }
            $tlperiod = new TimelinePeriod(
                $type,
                $start,
                $end,
                $fFoundProblem
            );
            TimelinePeriod::binaryIntersect($problemPeriods, $tlperiod);
        }
        $lastDateTime = null; $openDateTime = null;
        $previousRecordUser = 0;
        $parameters = array(&$fApprovedOrders, &$periods, $fRecords, $fFoundProblems, $fSensor);
        if(!$this->callPluginFunction('WorkCenter_AddOrderBuildTimeGraph_Hook', $parameters, Configure::read('companyTitle'))){ //vykdome uzsakymo vykdymo piesima per plugina (unikalus veikimas)
            TimelinePeriod::attachApprovedOrderToPeriod($fApprovedOrders, $periods, $fRecords, $fSensor, $fFoundProblems);
        }
        unset($fApprovedOrders);
        $i = 0;
        foreach ($problemPeriods as $pp) {
            if ($pp->type != TimelinePeriod::TYPE_NONE) TimelinePeriod::binaryIntersect($periods, $pp, true);
        }
        unset($problemPeriods);
        $dtHoursTo = clone $fromDateTime; $dtHoursTo->add(new DateInterval('PT59M59S'));
        $di = new DateInterval('PT1H');
        $userModel->virtualFields = array('bothNames'=>'CONCAT(User.first_name,\' \',User.last_name)');
        $operatos = $userModel->find('list', array('fields'=>array('id','bothNames')));
        $userModel->virtualFields = array();

        for ($i = 0; $i < $graphHoursLinesCount; $i++) {
            $hPeriods = TimelinePeriod::binarySlice($periods, $dtHoursTo, $toDateTime);
            //TimelinePeriod::conectSpaces($hPeriods, (($i == $graphHoursLinesCount) ? $toDateTime : null));
            TimelinePeriod::attachIntervalQuantitiesToPeriod($fRecords, $hPeriods, $fSensor, $fFoundProblems);
            foreach ($hPeriods as $p) { $p->buildData(); }
            foreach ($hPeriods as $p) { $p->updateTimes($operatos); }
            $hPlanPeriods = TimelinePeriod::binarySlice($planPeriods, $dtHoursTo, $toDateTime);
            foreach ($hPlanPeriods as $p) { $p->buildData(); $p->updateTimes($operatos); }
            $markers = array(); //valandoje esancios pozicijos
            $periodData = array(
                'value' => intval($dtHoursTo->format('H')),
                'date' => $dtHoursTo->format('Y-m-d'),
                'planValue' => __('Planas'),
                'planPeriods' => $hPlanPeriods,
            );
            $parameters = array(&$hPeriods, &$periodData, $fSensor, $graphHoursLinesCount);
            $this->callPluginFunction('WorkCenter_BeforeAddPeriodsToHours_Hook', $parameters, Configure::read('companyTitle'));
            $periodData['periods'] = $hPeriods;
            $hours[] = $periodData;
            $dtHoursTo->add($di);
        }
        unset($fRecords);
        return $hours;
    }
    
    
}
