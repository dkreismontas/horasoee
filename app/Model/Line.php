<?php

App::uses('AppModel', 'Model');
App::uses('Sensor', 'Model');
App::uses('Branch', 'Model');

/**
 * Description of Line
 */
class Line extends AppModel {
	
	const NAME = __CLASS__;
	
	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */
	public static function buildName(array $li, $external = true) {
		return $li[self::NAME]['name']
				//.((isset($li[self::NAME]['ps_id']) && $li[self::NAME]['name'] !== $li[self::NAME]['ps_id']) ? (' ('.$li[self::NAME]['ps_id'].')') : '')
				.(($external && isset($li[Branch::NAME]['name'])) ? (' ('.$li[Branch::NAME]['name'].')') : '');
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false, $returnLinesList = false) {
        $this->bindModel(array('belongsTo'=>array(
            'Sensor'=>array('foreignKey'=>false,'conditions'=>array('Line.id = Sensor.line_id'))
        )));
        $items = $this->find('all',array(
            'conditions'=>array(
                'OR'=>array('Sensor.id'=>Configure::read('user')->selected_sensors, 'Sensor.id IS NULL'),
            ),'order'=>array('Line.id'),'group'=>array('Line.id')
        ));
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- None --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = $li[self::NAME]['name']
					.(isset($li[self::NAME]['id']) ? (' | ID:'.$li[self::NAME]['id'].'') : '');
					//.(isset($li[self::NAME]['ps_id']) ? (' ('.$li[self::NAME]['ps_id'].')') : '');
					//.(isset($li[Branch::NAME]['name']) ? (' ('.$li[Branch::NAME]['name'].')') : '');
		}
		if($returnLinesList){ return array_keys($options); }
		return $options;
	}
	
	/** @return Line */
	public function withRefs() {
//		$this->bindModel(array(
//			'belongsTo' => array(
//				Branch::NAME => array(
//					'className' => Branch::NAME,
//					'foreignKey' => 'branch_id'
//				)
//			)
//		));
		return $this;
	}
	
}
