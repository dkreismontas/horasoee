<?php
    class TmpTable extends AppModel{  
      
        var $name = 'TmpTable';
		
        function setSource($tableName) {  
            $this->setDataSource($this->useDbConfig);  
            $db = ConnectionManager::getDataSource($this->useDbConfig);  
            $db->cacheSources = ($this->cacheSources && $db->cacheSources);   
            $this->table = $this->useTable = $tableName;  
            $this->tableToModel[$this->table] = $this->alias;  
            //$this->schema();  
        }  
    }  