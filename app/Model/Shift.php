<?php
App::uses('ShiftPattern', 'Controller/Component');
App::uses('AppModel', 'Model');

/**
 */
class Shift extends AppModel {
	const NAME = __CLASS__;
	//public $virtualFields = array('real_name'=>'SUBSTR(`name`,12)');

	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */
	public static function buildName(array $li, $external = true) {
		return $li[self::NAME]['name'];
	}

	/**
	 *
	 * @param int $branchId
	 * @param DateTime $now
	 * @return array
	 */
	public function findCurrent($branchId=0, DateTime $now = null) {
		if (!$now) $now = new DateTime();
		$conditions = array(
			self::NAME.'.start <=' => $now->format('Y-m-d H:i:s'),
			self::NAME.'.end >=' => $now->format('Y-m-d H:i:s')
		);
		if($branchId > 0){
			$conditions[self::NAME.'.branch_id'] = $branchId;
		}
        $this->bindModel(array('belongsTo'=>array('Branch')));
		$f = $this->find('first', array(
			'conditions' => $conditions,
			'order' => array(self::NAME.'.start ASC')
		));
		return ($f && !empty($f)) ? $f : null;
	}
    
	public function findAllCurrentShifts(DateTime $now = null) {
		if (!$now) $now = new DateTime();
		$f = $this->find('list', array(
		    'fields'=>array('Shift.id','Shift.id'),
			'conditions' => array(
				self::NAME.'.start <=' => $now->format('Y-m-d H:i:s'),
				self::NAME.'.end >=' => $now->format('Y-m-d H:i:s')
			),
		));
		return ($f && !empty($f)) ? $f : null;
	}

    public function getPrev($start, $branchId=1){
        return $this->find('first', array(
          'conditions'=>array(
            'Shift.start <' => $start,
            'Shift.branch_id' => $branchId,
          ),'order'=>array('Shift.start DESC')
        ));
    }

    public function extendAllBranchesShifts(){
	    $branchesModel = ClassRegistry::init('Branch');
        $branches = $branchesModel->find('list', array('fields'=>array('Branch.id')));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::uses('CakeRequest', 'Network');
                App::uses('CakeResponse', 'Network');
                App::uses('Controller', 'Controller');
                App::uses('AppController', 'Controller');
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts(false, $branchId);
            }
        }
    }

    public function generateShifts($generateBranchId = null){
        static $branchModel, $sensorModel = null;
        if($branchModel == null){
            $branchModel = ClassRegistry::init('Branch');
            $sensorModel = ClassRegistry::init('Sensor');
        }
	    $patternsTpls = Configure::read('shift_patterns');
        $calendar = new ShiftPattern();
        $daysToConstruct = 14;
        foreach($patternsTpls as $branchId => $branchPattern){
            if($generateBranchId > 0 && $branchId != $generateBranchId){ continue; }
            set_time_limit(10);
            $branch = $branchModel->findById($branchId);
            if(empty($branch)){ continue; }
            $currTime = new DateTime();
            if($branch['Branch']['sensor_id']>0) {
                //if($this->findCurrent($branchId, $currTime)){continue;}
                $lastShift = $this->find('first', array('conditions'=>array('Shift.branch_id'=>$branchId), 'order'=>array('Shift.start DESC')));
                if(!empty($lastShift) && $currTime < new DateTime($lastShift['Shift']['end'])){ continue; }//pamaina vis dar tesiasi, todel nieko daryti nereikia
                $fSensor = $sensorModel->findById($branch['Branch']['sensor_id']);
                $branchModel->id = $branchId;
                $branchModel->save(array('shift_change_init'=>0));
                if(!empty($fSensor) && !empty($lastShift)) {
                    $manualShiftChangeSettings = array_map(function($val){return (float)trim($val);}, explode(',', $fSensor['Sensor']['manual_shift_change']));
                    if(sizeof($manualShiftChangeSettings) == 2){
                        $originalShiftEnd = trim($branch['Branch']['last_shift_end'])?json_decode($branch['Branch']['last_shift_end'],true):array();
                        if(!isset($originalShiftEnd['shift_id']) || $originalShiftEnd['shift_id'] != $lastShift['Shift']['id']){//reiskia uzsibaige nauja pamaina, reikia nurodyti nauja padalinio pamainos pabaiga
                            $originalShiftEnd = array('shift_id'=>$lastShift['Shift']['id'], 'end'=>$lastShift['Shift']['end']);
                            $branchModel->save(array('last_shift_end'=>json_encode($originalShiftEnd)));
                        }
                        $maxExtendedShiftEnd = new DateTime($originalShiftEnd['end']);
                        $maxExtendedShiftEnd->add(new DateInterval(sprintf('PT%sM', $manualShiftChangeSettings[1])));
                        if($currTime < $maxExtendedShiftEnd){//jei esamas laikas mazesnis uz maksimaliai galima prailginti pamainos trukme, pamaina tesime
                            $this->id = $lastShift['Shift']['id'];
                            $this->save(array('end'=>$maxExtendedShiftEnd->format('Y-m-d H:i:s')));//pratesiame lygiai 1 minute
                            continue;
                        }
                    }
                }
                $daysToConstruct = 1;
            }
            $this->deleteAll(array('Shift.start >' => $currTime->format('Y-m-d H:i:s'), 'Shift.branch_id'=>$branchId));
            $lastDate = $this->find('first', array('conditions'=>array('Shift.branch_id'=>$branchId), 'order'=>array('Shift.start DESC')));
            $start = isset($lastDate['Shift']['end'])?new DateTime($lastDate['Shift']['end']):new DateTime();
            $startPoint = clone($start);
            $intervals = $calendar->constructCalendar($start, $daysToConstruct, $branchPattern, (int)$branchId, $lastDate);
            if(isset($intervals[0]) && !empty($lastDate)){
                $intervals[0]['start'] = $startPoint->add(new DateInterval('PT1S'))->format('Y-m-d H:i:s');
            }
            if($branch['Branch']['sensor_id']>0){//pamainos uzdaromos/atidaromos rankiniu budu, todel reikia tik vienos
                foreach($intervals as $interval){
                    $this->create();
                    $this->save($interval);
                    if(strtotime($interval['end']) > time()){
                        break;
                    }
                }
            }else{
                $this->saveAll($intervals);
            }
        }
    }

}
