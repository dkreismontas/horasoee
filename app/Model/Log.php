<?php

App::uses('AppModel', 'Model');
App::uses('Session', 'Model');
App::uses('CakeEmail', 'Network/Email');
/**
 */
class Log extends AppModel {

    public $user;
    private $group_stamp;

	const NAME = __CLASS__;

    public function write($content, $sensor=array()) {
        if($this->group_stamp == null) {
            $this->group_stamp = microtime();
        }
        try {
            unset($this->id);
            // Get caller function
            $trace = debug_backtrace();
            $caller = $trace[1];
            $caller_res = "";
            if (isset($caller['class'])) {
                $caller_res .= $caller['class'];
            }
            if (isset($caller['function'])) {
                $caller_res .= "::" . $caller['function'];
            }
//            if (isset($caller['line'])) {
//                $caller_res .= " :".$caller['line'];
//            }
            // End get caller function
			$userData = isset($this->user->id) ? $this->user->id . " (" . $this->user->username . ")" : "";
            $data = array(
                'Log' => array(
                    'source'     => $caller_res,
                    'user_id'    => $userData,
                    'content'    => $content,
                    'group_stamp' => $this->group_stamp,
                    'created_at' => date("Y-m-d H:i:s"),
                    'sensor' => isset($sensor['Sensor'])?$sensor['Sensor']['name']:$userData
                )
            );

            $this->save($data);
        }
        catch(Exception $e) {
            $data = array(
                'Log' => array(
                    'source'     => "",
                    'user_id'    => "",
                    'content'    => $e,
                    'created_at' => date("Y-m-d H:i:s")
                )
            );

            $this->save($data);
        }
    }

	public function sendSysMail($subject, $text, $emailAddress = 'd.kreismontas@horasmpm.eu'){
		$lastLog = $this->find('first', array('conditions'=>array('Log.source'=>'Log::sendSysMail'), 'order'=>array('Log.id DESC')));
		if(!empty($lastLog)){
			if(time() - strtotime($lastLog['Log']['created_at']) < 3600){ return null; }
		}
		$text .= "\n<br /><br />Logai formuojami ne dažniau kaip kartą per valandą. Jei norite gauti paskutinius pranešimus, pašalinkite paskutinės valandos logus, kurių source = Log::sendSysMail";
		$this->write($subject."\n".$text);
		$mail = new CakeEmail('default');
        $mail->from('noreply@horasoee.eu');
        $mail->to($emailAddress);
        $mail->subject(Configure::read('companyTitle').' '.$subject);
        $mail->emailFormat('both');
        $mail->send($text);
	}

}
