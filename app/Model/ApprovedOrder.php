<?php

App::uses('AppModel', 'Model');
App::uses('Sensor', 'Model');
App::uses('Plan', 'Model');
App::uses('Status', 'Model');

/**
 */
class ApprovedOrder extends AppModel {
	const NAME = __CLASS__;
	
	const STATE_NONE = 0;
	const STATE_IN_PROGRESS = 1;
	const STATE_COMPLETE = 2;
	const STATE_CANCELED = 3;
	const STATE_POSTPHONED = 4;
	const STATE_OTHER_PROGRESS = 5;
	const STATE_PART_COMPLETE = 6;
	const STATE_WAITING_FOR_DATA = 7;
	
	/**
	 * 
	 * @param int $sensorId
	 * @return array
	 */
	public function findCurrent($sensorId) {
		$f = $this->find('first', array(
			'conditions' => array(
				self::NAME.'.sensor_id' => $sensorId,
				self::NAME.'.status_id' => self::STATE_IN_PROGRESS
			),
			'order' => array(self::NAME.'.created DESC')
		));
		return ($f && !empty($f)) && $f[self::NAME]['id'] ? $f : null;
	}
	
	/**
	 * 
	 * @param int $sensorId
	 * @return array
	 */
	public function findWaitingForData($sensorId) {
		$f = $this->find('first', array(
			'conditions' => array(
				self::NAME.'.sensor_id' => $sensorId,
				self::NAME.'.status_id' => self::STATE_WAITING_FOR_DATA
			),
			'order' => array(self::NAME.'.created DESC'),
			'limit' => 1
		));
		return ($f && !empty($f)) && $f[self::NAME]['id'] ? $f : null;
	}
	
	/**
	 * Make posphoned orders partially complete
	 * @param int $sensorId
	 * @param int $planId
	 */
	public function partialComplete($sensorId, $planId) {
		$this->query(
			"UPDATE `$this->table` `ao`".
			" SET".
				" `ao`.`status_id`=:state_part_complete".
			" WHERE `ao`.`sensor_id`=:sensor_id".
				" AND `ao`.`plan_id`=:plan_id".
				" AND `ao`.`status_id`=:state_postphoned",
			array(
				':sensor_id' => $sensorId,
				':plan_id' => $planId,
				':state_part_complete' => self::STATE_PART_COMPLETE,
				':state_postphoned' => self::STATE_POSTPHONED
			),
			false
		);
	}
	
	/**
	 * Find and complete items in progress
	 * @param int $sensorId
	 * @param DateTime $completeTime
	 */
	public function findAndComplete($fSensor, DateTime $completeTime = null) {
		if(!isset($fSensor['Sensor']['id'])) throw new ErrorException('Sensor ID is required.');
		$fItems = $this->find('all', array(
			'conditions' => array(
				self::NAME.'.sensor_id' => $fSensor['Sensor']['id'],
				self::NAME.'.status_id' => self::STATE_IN_PROGRESS
			),
			'order' => array(self::NAME.'.created DESC')
		));
		$cc = 0;
		foreach ($fItems as $li) {
			if (!$cc && $completeTime) {
				$li[self::NAME]['end'] = $completeTime->format('Y-m-d H:i:s');
			}
			$li[self::NAME]['status_id'] = self::STATE_COMPLETE;
			$this->create();
			$this->save($li);
			$cc++;
		}
	}
	
	/** @return ApprovedOrder */
	public function withRefs($destroyBindAfterFirstSearch = true) { 
		$this->bindModel(array(
			'belongsTo' => array(
				Sensor::NAME => array(
					'className' => Sensor::NAME,
					'foreignKey' => 'sensor_id'
				),
				Plan::NAME => array(
					'className' => Plan::NAME,
					'foreignKey' => 'plan_id'
				),
				Status::NAME => array(
					'className' => Status::NAME,
					'foreignKey' => 'status_id'
				),
				Branch::NAME => array(
					'className' => Branch::NAME,
					'foreignKey' => '',
					'conditions' => array(
						Sensor::NAME.'.branch_id = '.Branch::NAME.'.id'
					),
					'dependent' => false
				),
				'Factory'=>array(
                    'foreignKey'=>false,
                    'conditions'=>array('Branch.factory_id = Factory.id')
                )
			)
		),$destroyBindAfterFirstSearch);
		return $this;
	}

    public function getRelatedApprovedOrders($planId, $dismissApprovedOrders=0) {
        $res = $this->find('all',array(
            'fields'=>array('SUM(ApprovedOrder.quantity) AS postponed_quantity'),
            'conditions'=>array(
                'ApprovedOrder.plan_id'=>$planId,
                'ApprovedOrder.id <>'=>$dismissApprovedOrders,
                'OR'=>array(
                    array('ApprovedOrder.status_id'=>self::STATE_POSTPHONED),
                    array('ApprovedOrder.status_id'=>self::STATE_PART_COMPLETE)
                )
            )
        ));
//        CakeLog::write('debug','plan: ' . $planId .' - postponed quantity: ' . json_encode($res[0][0]['postponed_quantity']));

        return ((!empty($res) && $res[0][0]['postponed_quantity'] != null) ? $res[0][0]['postponed_quantity'] : 0);
    }

    public function findPrevious($ao) {
        return $this->find('first',array(
            'conditions'=>array(
                'created <' => $ao['ApprovedOrder']['created'],
                'sensor_id' => $ao['ApprovedOrder']['sensor_id']
            ),
            'order' => array('created'=>'DESC')
        ));
    }


    public function getByShiftForReport($shift,$sensor_id) {
        $this->bindModel(array(
            'belongsTo' => array('Plan')
        ));
        $approved_orders = $this->find('all', array(
            'fields'     => array(
                'ApprovedOrder.id','ApprovedOrder.created', 'ApprovedOrder.end', 'ApprovedOrder.quantity', 'ApprovedOrder.box_quantity', 'ApprovedOrder.confirmed_quantity',
                //'ApprovedOrder.status_id',
                '(TIMESTAMPDIFF(SECOND,ApprovedOrder.created, ApprovedOrder.end)) as fact_time_all',
                '(TIMESTAMPDIFF(SECOND,Plan.start, Plan.end)) as plan_time_all',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN ApprovedOrder.created > \'' . $shift['Shift']['start'] . '\' THEN ApprovedOrder.created ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN ApprovedOrder.end < \'' . $shift['Shift']['end'] . '\' THEN ApprovedOrder.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as fact_time',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN Plan.start > \'' . $shift['Shift']['start'] . '\' THEN Plan.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN Plan.end < \'' . $shift['Shift']['end'] . '\' THEN Plan.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as plan_time',

                'Plan.id', 'Plan.paste_code', 'Plan.production_name', 'Plan.production_code','Plan.mo_number', 'Plan.start', 'Plan.end', 'Plan.line_quantity', 'Plan.step', 'Plan.box_quantity', 'Plan.quantity',
            ),
            'conditions' => array(
                'ApprovedOrder.created <'    => $shift['Shift']['end'],
                'ApprovedOrder.end >'        => $shift['Shift']['start'],
                'ApprovedOrder.sensor_id'    => $sensor_id,
                'ApprovedOrder.status_id > ' => 1
            )
        ));

        return $approved_orders;
    }


}
