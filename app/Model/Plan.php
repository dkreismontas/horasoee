<?php

App::uses('AppModel', 'Model');
App::uses('Line', 'Model');
App::uses('Sensor', 'Model');
App::uses('ApprovedOrder', 'Model');

/**
 */
class Plan extends AppModel {

    const NAME = __CLASS__;
    public $virtualFields = array();

    /**
     * Build name from data
     * @param array $li
     * @param boolean $external if true reference item names will be added
     * @return string
     */
    // public function __construct($id = false, $table = null, $ds = null) {
        // $params = array(&$this);
        // $this->callPluginFunction('Plan_Constructor_Hook', $params, Configure::read('companyTitle'));
        // parent::__construct($id, $table, $ds);
    // }
     
    public static function buildName(array $li, $external = true) {
        $additionalData = isset($li['ApprovedOrder'])?json_decode($li['ApprovedOrder']['additional_data'],true):array();
        $nr = isset($additionalData['add_order_number'])?' (nr.: '.$additionalData['add_order_number'].')':'';
        return __('Gaminys').': '.$li[self::NAME]['production_name'] .
        ($li[self::NAME]['production_code'] ? (' ('.__('gaminio kodas').': ' . $li[self::NAME]['production_code'] . ')') : '') .
        (($external && $li[self::NAME]['mo_number']) ? (' ('.__('Užsakymo nr.').': '.$li[self::NAME]['mo_number'] . ')') : '').$nr;
    }

    /**
     * Remove with dependencies
     * @param int $planId
     */
    public function removeWithDeps($planId) {
        if (!$planId) return;
        $this->query(
            "DELETE `dc` FROM `diff_cards` `dc`" .
            " WHERE `dc`.`plan_id`=:plan_id",
            array(':plan_id' => $planId),
            false
        );
        $this->query(
            "DELETE `ao` FROM `approved_orders` `ao`" .
            " WHERE `ao`.`plan_id`=:plan_id",
            array(':plan_id' => $planId),
            false
        );
        $this->query(
            "DELETE `p` FROM `$this->table` `p`" .
            " WHERE `p`.`id`=:plan_id",
            array(':plan_id' => $planId),
            false
        );
    }

    /**
     * Remove plans that where not used
     */
    public function removeUnused() {
        $this->query(
            "DELETE `p` FROM `$this->table` `p`" .
            " LEFT JOIN `approved_orders` `ao` ON `ao`.`plan_id`=`p`.`id`" .
            " LEFT JOIN `diff_cards` `dc` ON `dc`.`plan_id`=`p`.`id`" .
            " WHERE `ao`.`id` IS NULL AND `dc`.`id` IS NULL",
            array(),
            false
        );
    }

    /**
     * Get item list as associative array
     * where key is item ID and value is item name.
     * @param boolean $addEmpty
     * @param boolean $onlyCurrAndNext will output 4 items one of which is current
     * @return array
     */
    public function getAsSelectOptions($addEmpty = false, $onlyCurrAndNext = true) {
        $params = array();
        if ($onlyCurrAndNext) {
            $params = array(
                'conditions' => array(
                    'OR' => array(
                        array(ApprovedOrder::NAME . '.status_id' => ApprovedOrder::STATE_IN_PROGRESS),
                        array(ApprovedOrder::NAME . '.status_id' => null)
                    )
                ),
                'order'      => array(self::NAME . '.start ASC'),
                'limit'      => 4
            );
        }
        $items = $this->withRefs()->find('all', $params);
        $options = array();
        if ($addEmpty) {
            $options[0] = __('-- Nepasirinktas --');
        }
        foreach ($items as $li) {
            $options[$li[self::NAME]['id']] = self::buildName($li, true);
        }

        return $options;
    }

    /**
     * @param boolean $reset refs will be reset after find
     * @return Plan
     */
    public function withRefs($reset = true, $approvedOrderCond = array()) {
        $this->bindModel(array(
            'belongsTo' => array(
                Line::NAME   => array(
                    'className'  => Line::NAME,
                    'foreignKey' => 'line_id'
                ),
                Sensor::NAME => array(
                    'className'  => Sensor::NAME,
                    'foreignKey' => 'sensor_id'
                ),
                Branch::NAME => array(
					'className' => Branch::NAME,
					'foreignKey' => '',
					'conditions' => array(
						Sensor::NAME.'.branch_id = '.Branch::NAME.'.id'
					),
					'dependent' => false
				),
				'Factory'=>array(
                    'foreignKey'=>false,
                    'conditions'=>array('Branch.factory_id = Factory.id')
                ),
                'Product' => array(
                    'className'  => 'Product',
                    'foreignKey' => 'product_id'
                )
            ),
            'hasOne'    => array(
                ApprovedOrder::NAME => array_merge($approvedOrderCond, array(
                    'className'  => ApprovedOrder::NAME,
                    'foreignKey' => 'plan_id',
                )),
                Status::NAME        => array(
                    'className'  => Status::NAME,
                    'foreignKey' => '',
                    'conditions' => array(
                        ApprovedOrder::NAME . '.status_id = ' . Status::NAME . '.id'
                    ),
                    'dependent'  => false
                )
            )
        ), $reset);

        return $this;
    }

    public function getMOListByRange($request) {
        $start = $request['MoStat']['start_date'];
        $end = $request['MoStat']['end_date'];
        $productCodeCond = isset($request['MoStat']['product_code']) && trim($request['MoStat']['product_code'])?array('LOWER(Plan.production_code) LIKE \''.mb_strtolower($request['MoStat']['product_code']).'\''):array();
        $binder = array(
            'hasOne' => array(
                ApprovedOrder::NAME => array(
                    'className'  => ApprovedOrder::NAME,
                    'foreignKey' => 'plan_id'
                ),
                'OrderCalculation'  => array(
                    'foreignKey' => false,
                    'conditions' => 'Plan.mo_number = OrderCalculation.mo_number and Plan.sensor_id = OrderCalculation.sensor_id'
                ),
                'Branch'            => array(
                    'foreignKey' => false,
                    'conditions' => 'Sensor.branch_id = Branch.id'
                )
            ),'belongsTo' => array('Line','Sensor')
        );
        $searchParams['fields'] = array(
            'ApprovedOrder.additional_data',
            'Plan.mo_number',
            'Plan.production_name',
            'Plan.production_code',
            'CAST(OrderCalculation.order_start AS DATE) as date',
            'Plan.line_id',
            'CONCAT(Branch.name," ",Line.ps_id) as branch'
        );
        $searchParams['conditions'] = array_merge($productCodeCond, array(
            'OrderCalculation.order_end >=' => $start,
            'OrderCalculation.order_start <=' => $end,
            'ApprovedOrder.sensor_id' => Configure::read('user')->selected_sensors,
            'ApprovedOrder.id IS NOT NULL',
            'OrderCalculation.id IS NOT NULL'
        ));
        
        $searchParams['order_by'] = array(
            'CAST(Plan.start as DATE)',
            'CONCAT(Branch.name," ",Line.ps_id)',
        );
        $params = array(&$binder, &$searchParams);
        $this->callPluginFunction('Plan_getMOListByRangeBeforeSearch_Hook', $params, Configure::read('companyTitle'));
        
        $this->bindModel($binder);
        $list = $this->find('all', array(
            'fields'     => $searchParams['fields'],
            'conditions' => $searchParams['conditions'],
            'order'      => $searchParams['order_by']
        ));
        $listFormatted = array();
        array_walk($list, function ($val) use (&$listFormatted) {
            $date = $val[0]['date'];
            $val['Plan']['additional_data'] = $val['ApprovedOrder']['additional_data'];
            $branch = trim($val[0]['branch'])?$val[0]['branch']:__('Pažymėti visus');
            $listFormatted[$date] [$branch] [$val['Plan']['mo_number']] = $val['Plan'];
        });
        $params = array(&$listFormatted, &$list);
        $this->callPluginFunction('Plan_getMOListByRangeAfterSearch_Hook', $params, Configure::read('companyTitle'));
        return $listFormatted;
    }

    public function findPrevious($plan) {
        return $this->find('first', array(
            'conditions' => array(
                'start <'   => $plan['Plan']['start'],
                'sensor_id' => $plan['Plan']['sensor_id']
            ),
            'order'      => array('start' => 'DESC')
        ));
    }

    public function reorder() {
            $position_string = 'Plan.end';
            $this->withRefs(false, array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE)));
            $sensorModel = ClassRegistry::init('Sensor');
            $sensorsList = $sensorModel->find('list', array('fields'=>array('id'), 'conditions'=>array('Sensor.is_partial'=>0)));
            foreach($sensorsList as $sensorId){
                $maxFrozenPos = $this->find('first', array('fields'=>array('MAX(Plan.position) as max_pos'), 'conditions'=>array('Plan.position_frozen'=>1, 'ApprovedOrder.id IS NULL', 'Plan.sensor_id'=>$sensorId)));
                $maxFrozenPos = $maxFrozenPos[0]['max_pos'] > 0?$maxFrozenPos[0]['max_pos']:0;
                $this->withRefs(false, array('conditions'=>array('ApprovedOrder.status_id'=>ApprovedOrder::STATE_COMPLETE)));
                $records = Set::extract('/Plan/id', $this->find('all',array(
                    'fields'=>array('Plan.id'),
                    'conditions'=>array(
                        'Plan.sensor_id'=>$sensorId,
                        'Plan.position_frozen'=>0,
                        'ApprovedOrder.id IS NULL',
                    ),'order'=>array($position_string))));
                if(!empty($records)){
                    //$records = array_values($records);
                    $case_string = "CASE Plan.id";
                    foreach($records as $index=>$order_id) {
                        $case_string .= " WHEN ".$order_id." THEN ".++$maxFrozenPos;
                    }
                    $case_string .= " END";
                    $this->updateAll(array('Plan.position'=>$case_string),array('Plan.id'=>$records));
                }
            }
        }

    public function checkInputFormFields($dataFromInputForm, $fSensor, $fProduct = array()){
        $requiredFieldsEmpty = array_filter(Configure::read('workcenter_orders_table_structure'), function($params, $key)use($dataFromInputForm){
            return isset($params['required']) && (!isset($dataFromInputForm[$key]) || !trim($dataFromInputForm[$key]));
        },ARRAY_FILTER_USE_BOTH);
        $errorsList = array();
        if(!empty($requiredFieldsEmpty)){
            foreach($requiredFieldsEmpty as $requiredFieldEmpty){
                $errorsList[] = __('Laukelis "%s" negali būti tuščias', $requiredFieldEmpty['title']);
            }
        }
        if(!empty($errorsList)){ return $errorsList; }
        if(!$fSensor['Sensor']['calculate_quality']){ return array(); }
        if(isset($dataFromInputForm['add_mo_number'])){
            $this->bindModel(array('belongsTo'=>array('Sensor')));
            //tikriname ar mo numeris nebuvo ivestas anksciau to paties tipo darbo centruose, nes jis turi buti unikalus
            $moExist = $this->find('first', array('conditions'=>array(
                'Plan.mo_number'=>trim($dataFromInputForm['add_mo_number']),
                'Sensor.type'=>$fSensor['Sensor']['type']
            )));
            if(!empty($moExist)){
                $errorsList[] = __('Toks užsakymo numeris jau yra užregistruotas. Pasirinkite kitą numerį.');
            }
            if($fSensor['Sensor']['type'] == 3){ //linijos pabaigos darbo centram papildomai tikriname ar mo numeris turi atitikmanei pradiniame darbo centre
                $this->bindModel(array('belongsTo'=>array('Sensor','Product')));
                $moExistInLineStart = $this->find('first', array('conditions'=>array(
                    'Plan.mo_number'=>trim($dataFromInputForm['add_mo_number']),
                    'Sensor.type'=>1
                )));
                if(empty($moExistInLineStart)){
                    $errorsList[] = __('Toks užsakymo numeris neturi atitikmens linijos pradžioje. Užsakymo su šiuo numeriu pradėti negalima.');
                }elseif(!empty($fProduct) && $fProduct['Product']['production_code'] != $moExistInLineStart['Product']['production_code']){
                    $errorsList[] = __('Užsakymo numeris neatitinka pasirinkto gaminio');
                }
            }
        }

        return $errorsList;
    }

    public function getCurrentPlan($fSensor) {
        $planSearchParams = array(
            'conditions' => array(
                ApprovedOrder::NAME.'.sensor_id' => $fSensor[Sensor::NAME]['id'],
                ApprovedOrder::NAME.'.status_id' => ApprovedOrder::STATE_IN_PROGRESS
            ),
            'order' => array(ApprovedOrder::NAME.'.created DESC'),
        );
        $parameters = array(&$planSearchParams, &$fSensor);
        $pluginData = $this->callPluginFunction('changePlanSearchInWorkCenterHook', $parameters, Configure::read('companyTitle'));
        $fPlan = $this->withRefs()->find('first', $planSearchParams);
        return empty($fPlan) ? null : $fPlan;
    }

}
