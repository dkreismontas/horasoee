<?php
class SlidesCollection extends AppModel{
    
    public function attachLinkToSlide(&$slidesCollection){
        $slides = json_decode($slidesCollection['SlidesCollection']['slides_json']);
        $sensorModel = ClassRegistry::init('Sensor');
        $links = array();
        foreach($slides as &$slide){
            switch($slide->type){
                case 0:
                    $links[] = Router::url(array('plugin'=>'tv', 'controller'=>'plugins', 'action'=>'index', implode('_',$slide->sensors)),true);
                break;
                case 1:
                    $sensorId = current($slide->sensors);
                    $sensor = $sensorModel->findById($sensorId);
                    $lineId = !empty($sensor)?$sensor['Sensor']['line_id']:0;
                    $links[] = Router::url(array('controller' => 'workCenter', 'action' => 'index', 'dc'=>$lineId.'_'.$sensorId),true);
                break;
                case 2:
                    $links[] = Router::url(array('plugin'=>false, 'controller'=>'work_center_24h', 'action'=>'index', implode('_',$slide->sensors)),true);
                break;
            }
        }
        $slidesCollection['SlidesCollection']['links'] = '\''.implode('\',\'', $links).'\'';
        $slidesCollection['SlidesCollection']['slides_json'] = $slides;
    }
    
}
