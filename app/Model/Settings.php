<?php

App::uses('AppModel', 'Model');

/**
 */
class Settings extends AppModel {
	const NAME = __CLASS__;
	
	/** Sytem email for use in from field for emails */
	const S_SYSTEM_EMAIL = 'system_email';
	/** System name for sending emails */
	const S_SYSTEM_EMAIL_NAME = 'system_email_name';
	/** Time offset backwards in seconds */
	const S_RECORD_TIME_OFFSET = 'record_time_offset';
	/** Speed detection interval in minutes */
	const S_SPEED_DETECT_INTERVAL = 'speed_detect_interval';
	/** Good speed threshold in %, if speed is lower than it's slow speed */
	const S_OK_SPEED_THRESHOLD = 'ok_speed_threshold';


    const S_REMOVE_EMPTY_SHIFTS_FROM_OEE = 'remove_empty_shifts_from_oee';
	
	/** @var array */
	private static $itemCache = null;
	
	/** @return array */
	public function getAll() {
		if (self::$itemCache == null) {
			self::$itemCache = array();
			$list = $this->find('all');
			foreach ($list as $li) {
				self::$itemCache[$li[self::NAME]['key']] = $li[self::NAME]['value'];
			}
		}
		return self::$itemCache;
	}
	
	/**
	 * Get settings value
	 * @param string $key
	 * @param string $default
	 * @return string
	 */
	public function getOne($key, $default = null) {
		$settings = $this->getAll();
		return isset($settings[$key]) ? $settings[$key] : (is_null($default) ? null : $default);
	}
	
	public static function translate($str){
		static $currentLang = null;
		if($currentLang == null){$currentLang = current(explode('_',Configure::read('currentLang')));}
		$langParts = array_filter(explode('[:', $str));
		foreach($langParts as $langPart){
		    if(substr($langPart,0,2) == $currentLang){
		        return substr($langPart,3);
		    }
		}
		// if(preg_match('/\[:'.$currentLang.'\]([^\[]*|.*$)/i', $str, $match)){
			// if(isset($match[1])){ return $match[1]; }
		// }
		if(preg_match('/^\[:([^\]]+)\]$/i',trim($str))){ return ''; }
		return $str;
	}	
	public static function translateThreaded($array, $fieldsToTranslate){
		foreach($array as &$arrayBranch){
			foreach($fieldsToTranslate as $fieldToTranslate){
				$keys = explode('.',$fieldToTranslate);
				$field = &$arrayBranch;
				foreach($keys as $key){
					if(is_array($field) && isset($field[$key])){
						$field = &$field[$key];
					}
				}
				if(!is_array($field) && trim($field)){
					$field = self::translate($field);
				}
			}
			if(isset($arrayBranch['children']) && !empty($arrayBranch['children'])){
				$arrayBranch['children'] = self::translateThreaded($arrayBranch['children'], $fieldsToTranslate);
			}
		}
		return $array;
	}

    public function getLatestFilePathFromFtp($pathToFTP, $deleteFile = false, $filterExtensions = array()){
        if(preg_match('/^ftp:\/\/([^:]+):([^@]+)@([^:]+):([^\/]+)(\/*.*)/i', $pathToFTP, $match)){
            if(sizeof($match) == 6){
                array_shift($match);
                list($ftpLogin, $ftpPass, $ftpServer, $ftpPort, $path) = $match;
            }
        }else{
            return array('errors'=>'Wrong FTP path specified');
        }
        $path = rtrim($path,'/').'/';
        $ftpConn = ftp_connect($ftpServer,$ftpPort,10);
        if(!$ftpConn){
            return array('errors'=>"Could not connect to $ftpServer");
        }
        $login = ftp_login($ftpConn, $ftpLogin, $ftpPass);
        if(!$login){
            return array('errors'=>"Wrong FTP parameters specified. Login: $ftpLogin, Password: $ftpPass");
        }
        ftp_pasv($ftpConn, true);
        //$xmlFiles = ftp_nlist($ftp_conn, '/HorasMPM/Uzsakymai');
        $tmpLocalFileLocation = tempnam(sys_get_temp_dir(), "ftp");
        $info = false;
        $files = ftp_nlist($ftpConn, $path);
        if(!is_array($files)){ ftp_close($ftpConn); return array('errors'=>'FTP catalog is empty'); }
        $files = array_map(function($file){ return current(array_reverse(explode('/',$file))); }, $files);
        $mostRecentFileDate = array();
        $files = array_filter($files, function($file)use($ftpConn, $path, $filterExtensions){
            $fileHasSize = ftp_size($ftpConn, $path.$file) > 0;
            $extensionPass = empty($filterExtensions) || in_array(strtolower(current(array_reverse(explode('.', $file)))),$filterExtensions);
            return $fileHasSize && $extensionPass;
        });
        usort($files, function($a, $b)use($ftpConn, $path){
            return ftp_mdtm($ftpConn, $path.$a) > ftp_mdtm($ftpConn, $path.$b) ? -1 : 1;
        });
        if(empty($files)){ ftp_close($ftpConn); return array('errors'=>'FTP catalog does not have a file with size'); }
        if(ftp_get($ftpConn, $tmpLocalFileLocation, $path.$files[0], FTP_BINARY)){
            if($deleteFile){
                ftp_delete($ftpConn, $path.$files[0]);
            }
            ftp_close($ftpConn);
            return array('success'=>$tmpLocalFileLocation);
        }
    }
    
}
