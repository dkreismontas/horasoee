<?php
class QualityFactor extends AppModel{
    public $useTable = false;
    
    public function calculateOrderQualityOnProductionEnd($fSensor, &$currentPlan){
        if($fSensor['Sensor']['type'] != 3 || !$fSensor['Sensor']['calculate_quality']){ return false; }//skaiciuojame tik kai uzdaromas uzsakymas linijos pabaigoje
        $lossModel = ClassRegistry::init('Loss');
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        //tikirname ar yra uzsakymas su tokiu paciu mo numeriu linijos pradzioje
        $quantityType = $fSensor['Sensor']['plan_relate_quantity']=='quantity'?'box_quantity':'quantity';
        $approvedOrderModel->bindModel(array('belongsTo'=>array('Plan','Sensor')));
        $lineQuantities = Hash::combine($approvedOrderModel->find('all', array(
            'fields'=>array('SUM(ApprovedOrder.'.$quantityType.') AS quantity', 'Sensor.type'),
            'conditions'=>array('Plan.mo_number'=>$currentPlan['Plan']['mo_number']),
            'group'=>array('Sensor.type')
        )),'{n}.Sensor.type','{n}.0.quantity');
        if(!isset($lineQuantities[Sensor::TYPE_MIXING]) || !isset($lineQuantities[Sensor::TYPE_PACKING])){ return false; }
        
        $lossModel->bindModel(array('belongsTo'=>array('ApprovedOrder',
            'Sensor'=>array('foreignKey'=>false,'conditions'=>array('Sensor.id = ApprovedOrder.sensor_id')),
            'Plan'=>array('foreignKey'=>false,'conditions'=>array('Plan.id = ApprovedOrder.plan_id'))
        )));
        $lineLossesQuantities = Hash::combine($lossModel->find('all', array(
            'fields'=>array('SUM(Loss.value) AS quantity','Sensor.type'),
            'conditions'=>array('Plan.mo_number'=>$currentPlan['Plan']['mo_number']),
            'group'=>array('Sensor.type')
        )),'{n}.Sensor.type','{n}.0.quantity');
        $lineLossesQuantities[Sensor::TYPE_PACKING] = isset($lineLossesQuantities[Sensor::TYPE_PACKING])?$lineLossesQuantities[Sensor::TYPE_PACKING]:0;
        $lineLossesQuantities[Sensor::TYPE_MIXING] = isset($lineLossesQuantities[Sensor::TYPE_MIXING])?$lineLossesQuantities[Sensor::TYPE_MIXING]:0;
        $allQuantities = $fSensor['Sensor']['calculate_quality'] == 1?bcadd($lineQuantities[Sensor::TYPE_MIXING],$lineLossesQuantities[Sensor::TYPE_MIXING],6):$lineQuantities[Sensor::TYPE_MIXING];
        $goodQuantities = $fSensor['Sensor']['calculate_quality'] == 1?$lineQuantities[Sensor::TYPE_PACKING]:bcsub($lineQuantities[Sensor::TYPE_PACKING],$lineLossesQuantities[Sensor::TYPE_PACKING],6);
        $qualityFacor = bcdiv($goodQuantities, $allQuantities, 6);
        $currentPlan['ApprovedOrder']['quality_factor'] = $qualityFacor;
        // $approvedOrderModel->updateAll(
            // array('quality_factor'=>$qualityFacor), 
            // array('id'=>array($currentPlan['ApprovedOrder']['id'], $lineStartHasSameMo['ApprovedOrder']['id']))
        // );
        //TODO ar atnaujinti visus uzsakymus su mo numeriu ar tik paskutini?
        $approvedOrderModel->bindModel(array('belongsTo'=>array('Plan')));
        $approvedOrderModel->updateAll(
            array('ApprovedOrder.quality_factor'=>$qualityFacor), 
            array('Plan.mo_number'=>$currentPlan['Plan']['mo_number'])
        );
    }

    public function calculateQualityInShift($orders, $branchId){
        $lineStartOrders = array_filter($orders, function($order){
            return $order['Sensor']['calculate_quality'] > 0 && $order['Sensor']['type'] == Sensor::TYPE_MIXING && $order['Record']['approved_order_id'] > 0;
        });
        if(empty($lineStartOrders)){ return array(); }
        $moNumbersList = Set::extract('/Plan/mo_number',$lineStartOrders);
        $approvedOrderModel = ClassRegistry::init('ApprovedOrder');
        $sensorModel = ClassRegistry::init('Sensor');
        $approvedOrderModel->bindModel(array('belongsTo'=>array(
            'Plan','Sensor',
            'Shift'=>array('foreignKey'=>false, 'conditions'=>array('ApprovedOrder.status_id = '.ApprovedOrder::STATE_COMPLETE.' AND ApprovedOrder.end BETWEEN Shift.start AND Shift.end AND Shift.branch_id = '.$branchId))
        )));
        $finishedMosTmp = $approvedOrderModel->find('all', array(
            'fields'=>array(
                'Sensor.type', 
                'Plan.mo_number', 
                'TRIM(Sensor.calculate_quality) AS loss_fixation_pos',
                'SUM(IF(Sensor.plan_relate_quantity = \'quantity\', ApprovedOrder.box_quantity, ApprovedOrder.quantity)) AS quantity',
                'GROUP_CONCAT(Shift.id) AS shift_id'
            ),'conditions'=>array(
                'Plan.mo_number'=>$moNumbersList, 
            ),'group'=>array('Plan.mo_number','Sensor.type')
        ));
        $moTypesFinishShift = array();
        foreach($finishedMosTmp as $finishedMoTmp){
            $moNumber = $finishedMoTmp['Plan']['mo_number'];
            $sensorType= $finishedMoTmp['Sensor']['type'];
            $moTypesFinishShift[$moNumber][$sensorType] = $finishedMoTmp[0];
        }
        unset($finishedMosTmp);
        $lossModel = ClassRegistry::init('Loss');
        $lossModel->bindModel(array('belongsTo'=>array('ApprovedOrder',
            'Sensor'=>array('foreignKey'=>false,'conditions'=>array('Sensor.id = ApprovedOrder.sensor_id')),
            'Plan'=>array('foreignKey'=>false,'conditions'=>array('Plan.id = ApprovedOrder.plan_id'))
        )));
        $lossesTmp = $lossModel->find('all', array(
            'fields'=>array(
                'SUM(Loss.value) AS quantity',
                'Sensor.type',
                'Plan.mo_number',
                'Loss.shift_id'
            ),'conditions'=>array(
                'Plan.mo_number'=>$moNumbersList,
                'Loss.shift_id >'=>0
            ),'group'=>array('Loss.shift_id','Plan.mo_number','Sensor.type')
        ));
        $losses = array();
        foreach($lossesTmp as $lossTmp){
            $moNumber = $lossTmp['Plan']['mo_number'];
            $sensorType= $lossTmp['Sensor']['type'];
            $shiftId= $lossTmp['Loss']['shift_id'];
            $losses[$moNumber][$shiftId][$sensorType] = $lossTmp[0]['quantity'];
        }
        $quantitiesInShift = array();
        foreach($lineStartOrders as $lineStartOrder){
            $moNumber = $lineStartOrder['Plan']['mo_number'];
            $shiftId = $lineStartOrder['Record']['shift_id'];
            $lossFixationPos = $lineStartOrder['Sensor']['calculate_quality'];
            $quantityInShift = $lineStartOrder[0]['total_quantity'];
            $sensorId = $lineStartOrder['Record']['sensor_id'];
            $lineStartLossInCurrentShift = isset($losses[$moNumber][$shiftId][Sensor::TYPE_MIXING])?$losses[$moNumber][$shiftId][Sensor::TYPE_MIXING]:0;
            //Jei užsakymas buvo užbaigtas esamoje pamainoje, o to paties mo numerio užsakymas taip pat užbaigtas ir linijos pabaigoje
            if(!isset($quantitiesInShift[$sensorId])){ $quantitiesInShift[$sensorId] = array('good_quantities'=>0, 'all_quantities'=>0); }
            if(isset($moTypesFinishShift[$moNumber][Sensor::TYPE_MIXING]) && in_array($shiftId, explode(',',$moTypesFinishShift[$moNumber][Sensor::TYPE_MIXING]['shift_id'])) && isset($moTypesFinishShift[$moNumber][Sensor::TYPE_PACKING])){
                if(isset($losses[$moNumber])){
                    $previousShiftsLossesInLineStart = array_filter($losses[$moNumber], function($data,$shiftIdForFilter)use($shiftId){
                        return $shiftIdForFilter < $shiftId && isset($data[Sensor::TYPE_MIXING]);
                    }, ARRAY_FILTER_USE_BOTH);
                    $previousShiftsLossesInLineStart = (float)array_sum(Set::extract('/'.Sensor::TYPE_MIXING, $previousShiftsLossesInLineStart));
                }else{ $previousShiftsLossesInLineStart = 0; }
                $quantityLineStart = $moTypesFinishShift[$moNumber][Sensor::TYPE_MIXING]['loss_fixation_pos'] == 1?bcadd($moTypesFinishShift[$moNumber][Sensor::TYPE_MIXING]['quantity'],$previousShiftsLossesInLineStart,6): bcsub($moTypesFinishShift[$moNumber][Sensor::TYPE_MIXING]['quantity'],$previousShiftsLossesInLineStart,6);
                $quantityLineEnd = $moTypesFinishShift[$moNumber][Sensor::TYPE_PACKING]['loss_fixation_pos'] == 1?$moTypesFinishShift[$moNumber][Sensor::TYPE_PACKING]['quantity']: bcsub($moTypesFinishShift[$moNumber][Sensor::TYPE_PACKING]['quantity'],$previousShiftsLossesInLineStart,6);
                $totalOrderLoss = max(0,bcsub($quantityLineStart, $quantityLineEnd, 6));
                $goodQuantity = bcsub($quantityInShift, $totalOrderLoss, 6);
                $quantitiesInShift[$sensorId]['good_quantities'] = bcadd($quantitiesInShift[$sensorId]['good_quantities'], $goodQuantity, 6);
            }else{
                $goodQuantity = $lossFixationPos == 1?$quantityInShift:bcsub($quantityInShift,$lineStartLossInCurrentShift,6);
                $quantitiesInShift[$sensorId]['good_quantities'] = bcadd($quantitiesInShift[$sensorId]['good_quantities'], $goodQuantity, 6);
            }
            $allQuantity = $lossFixationPos == 1?bcadd($quantityInShift, $lineStartLossInCurrentShift, 6) : $quantityInShift;
            $quantitiesInShift[$sensorId]['all_quantities'] = bcadd($quantitiesInShift[$sensorId]['all_quantities'], $allQuantity,6);
        }
        $sensorsList = Hash::combine($sensorModel->find('all', array('fields'=>array('Sensor.id', 'Sensor.line_id', 'Sensor.type'))),'{n}.Sensor.id', '{n}.Sensor');
        $lineEnds = array();
        foreach($quantitiesInShift as $sensorId => $quantities){
            $lineId = isset($sensorsList[$sensorId])?$sensorsList[$sensorId]['line_id']:0;
            if(!$lineId){ continue; }
            $sensorsInLineEnd = array_filter($sensorsList, function($sensor)use($lineId){
                return $sensor['line_id'] == $lineId && $sensor['type'] == Sensor::TYPE_PACKING;
            });
            foreach($sensorsInLineEnd as $lineEndSensorId => $lineEndSensorData){
                $lineEnds[$lineEndSensorId] = $quantities;
            }
        }
        $quantitiesInShift = $quantitiesInShift + $lineEnds;
        return $quantitiesInShift;
    }

    public function addSuggestionToAvailableMos(&$fSensor){
        if($fSensor['Sensor']['type'] == Sensor::TYPE_PACKING && $fSensor['Sensor']['calculate_quality'] > 0 && array_key_exists('add_mo_number', Configure::read('workcenter_orders_table_structure'))){
            $sensorModel = ClassRegistry::init('Sensor');
            $planModel = ClassRegistry::init('Plan');
            $lineStartSensor = $sensorModel->find('list', array(
                'fields'=>array('Sensor.id'),
                'conditions'=>array('Sensor.type'=>Sensor::TYPE_MIXING, 'Sensor.line_id'=>$fSensor['Line']['id'])
            ));
            $planModel->bindModel(array('belongsTo'=>array(
                'PlanLineEnd'=>array(
                    'foreignKey'=>false, 'className'=>'Plan', 'conditions'=>array('PlanLineEnd.mo_number = Plan.mo_number AND PlanLineEnd.sensor_id = '.$fSensor['Sensor']['id'])
                ),'ApprovedOrder'=>array('foreignKey'=>false, 'conditions'=>array('Plan.id = ApprovedOrder.plan_id'))
            )));
            $notUsedMoList = $planModel->find('all', array('recursive'=>1,'fields'=>array('Plan.mo_number','ApprovedOrder.additional_data'),'conditions'=>array('Plan.sensor_id'=>$lineStartSensor, 'Plan.created >'=>date('Y-m-d H:i:s', strtotime('-1 week')), 'PlanLineEnd.id IS NULL')));
            $params = array(&$notUsedMoList);
            $this->callPluginFunction('QualityFactor_AfterAddSuggestionToAvailableMos_Hook', $params, Configure::read('companyTitle'));
            $notUsedMoList = Set::extract('/Plan/mo_number', $notUsedMoList);
            $fSensor['Sensor']['available_order_numbers'] = $notUsedMoList;
        }
    }
}
