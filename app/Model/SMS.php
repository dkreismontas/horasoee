<?php
class SMS extends AppModel{
    public $useTable = 'sms';
	
	public function send(String $text='', $phoneNr, String $from, String $event='', Int $smsLenght = 160, $forceSend = false){
        $settingsModel = ClassRegistry::init('Settings');
        $mailModel = ClassRegistry::init('Mail');
        if(!$settingsModel->getOne('enable_sms_sending') && !$forceSend){ return false; }
		$text = mb_substr($text,0, $smsLenght);
		$url = "https://cloud.bite.lt/restsmpp/send";
        $phoneNrs = is_array($phoneNr)?$phoneNr:array($phoneNr);
        foreach($phoneNrs as $phoneNr){
            $phoneNr = trim(trim($phoneNr),'+');
            if(substr($phoneNr,0,1) == 8){
                $phoneNr = '370'.substr($phoneNr,1);
            }
            $message = array(
                'src' => 'HorasOEE', //HorasOEE | HorasMPM | 37061111890
                'dst' => $phoneNr,
                'messageText' => $text,
                'from' => $from,
                'dataCoding'=>'latin1', //ucs2
                'registered'=>0, //nurodzius 1 po sekmingo isisuntimo prie output pridedamas messageId, pvz: {"SubmitSmRespCommandStatus":"0", "messageId":"1885235165"}
            );
            $body = json_encode($message, JSON_UNESCAPED_UNICODE );
            $ch = curl_init();
            $options = array(
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_HEADER         => false,
                //CURLOPT_USERPWD        => "s22445:our9or3n",
                CURLOPT_FOLLOWLOCATION => true,
                //CURLOPT_SSL_VERIFYHOST => 2,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_VERBOSE        => false,
                CURLOPT_URL            => $url,
                CURLOPT_POST           => true,
                CURLOPT_POSTFIELDS     => $body,
                CURLINFO_HEADER_OUT    => false,
                CURLOPT_HTTPHEADER     => array(
                    'cache-control:no-cache',
                    'content-type:application/json',
                    'password:our9or3n',
                    'username:s22445',
                    'Content-Length: '.strlen($body)
                )
            );
            curl_setopt_array($ch , $options);
            $output = curl_exec($ch);
            //pr($output);
            $outputJSON = json_decode($output,true);
            if(curl_errno($ch) || (!isset($outputJSON['SubmitSmRespCommandStatus']) || $outputJSON['SubmitSmRespCommandStatus'] != 0)){
                $logModel = ClassRegistry::init('Log');
                $logText = "Nepavyko išsiųsti sms numeriu $phoneNr. \n<br />Klaidos nr.: ".curl_error($ch). " \n<br />SMS tekstas: $text \n<br />CURL grazintas rezultatas: ".$output;
                $logModel->write($logText);
                $mailModel->send('support@horasmpm.eu', $logText, 'Nesėkmingas SMS išsiuntimas projekte '.Configure::read('companyTitle'));
                curl_close($ch);
                return false;
                //return array('status'=>0, 'error'=>$errorNumber);
            } else {
                curl_close($ch);
                $this->create();
                $this->save(array(
                    'phone_nr' => $phoneNr,
                    'text' => $text,
                    'event' => $event,
                ));
            }
        }
        return true;
	}

//Siuntima per Inotecha
//	public function send(String $text='', String $phoneNr, String $from){
//		$url = "https://dev.inotecha.lt/horas/send_sms";
//		// http basic
//		$user = "horas";
//		$pass = "gunfireventedslothpoodle7|";
//		// certificate
//		$crt_file = ROOT.'/app/webroot/sms_certificates/horas.crt';
//		$key_file = ROOT.'/app/webroot/sms_certificates/horas.key';
//		$cert_pass = "clairWilburEchoingAmplify8`";
//		$message = array(
//			'text' => $text,
//			'from' => $from,
//			'to' => $phoneNr,
//		);
//		$_body = json_encode($message, JSON_UNESCAPED_UNICODE );
//		$ch = curl_init();
//		$options = array(
//			CURLOPT_RETURNTRANSFER => true,
//			// CURLOPT_HEADER         => true,
//			CURLOPT_USERPWD        => $user.":".$pass,
//			CURLOPT_FOLLOWLOCATION => true,
//			CURLOPT_SSL_VERIFYHOST => 2,
//			CURLOPT_SSL_VERIFYPEER => true,
//			CURLOPT_USERAGENT      => "Curl script",
//			CURLOPT_VERBOSE        => true,
//			CURLOPT_URL            => $url,
//			CURLOPT_SSLCERT        => $crt_file,
//			CURLOPT_SSLCERTPASSWD  => $cert_pass,
//			CURLOPT_SSLKEY         => $key_file,
//			CURLOPT_POST           => true,
//			CURLOPT_POSTFIELDS     => $_body,
//			CURLOPT_HTTPHEADER     => array(
//				'Content-Type:application/json',
//				'Content-Length: '.strlen($_body)
//			)
//		);
//		curl_setopt_array($ch , $options);
//		$output = curl_exec($ch);
//		if(curl_errno($ch)){
//			$errorNumber = curl_error($ch);
//			curl_close($ch);
//			return array('status'=>0, 'error'=>$errorNumber);
//		} else {
//			// {"status":1,"error":""} - sms išsiųstas
//			// {"status":0,"error":"Priežastis kodėl neišsiuntė"} - sms neišsiųstas
//			// error priežastis kol kas nerodo
//			return json_decode($output, true);
//		}
//	}

}
