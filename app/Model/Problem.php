<?php

App::uses('AppModel', 'Model');
App::uses('Sensor', 'Model');

/**
 */
class Problem extends AppModel {
	const NAME = __CLASS__;

	const ID_WORK = -1;
	const ID_NEUTRAL = 1;
	const ID_NO_WORK = 2;
    const ID_NOT_DEFINED = 3;

	/**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */

    public static function buildName(array $li, $external = true) {
        $types = Sensor::getTypes();
        $lineTitle = isset($li['Line']['name'])?trim($li['Line']['name']).' ':'';
		$rightPart = $lineTitle.(($external && isset($types[$li[self::NAME]['sensor_type']])) ? (($li[self::NAME]['sensor_type'] > 0?(trim($lineTitle)?' - ':'').$types[$li[self::NAME]['sensor_type']].'':'')) : '');
        return Settings::translate($li[self::NAME]['name']).(trim($rightPart)?' ('.$rightPart.')':'');
    }
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @param boolean $onlyTyped include only if has valid sensor type
	 * @param boolean $includeForSpeed if true include speed types
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false, $onlyTyped = true, $includeForSpeed = false, $exclude = array()) {
		$conds = array();
		if ($onlyTyped) $conds[self::NAME.'.sensor_type >'] = 0;
		if (!$includeForSpeed) $conds[self::NAME.'.for_speed'] = 0;
        $conds['Problem.id !='] = $exclude;
		$lineModel = ClassRegistry::init('Line');
        $conds['Problem.line_id'] = $lineModel->getAsSelectOptions(true,true);
        $this->bindModel(array('belongsTo'=>array('Line')));
		$items = $this->find('all', array('conditions'=>$conds, 'order'=>array(self::NAME.'.name')));
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- Nepasirinktas --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li, true);
		}
		return $options;
	}
    
    public function parseThreadedProblemsTitles($tree){
        $data = array();
        foreach($tree as $list){
            if(!empty($list['children'])){
                $childData = $this->parseThreadedProblemsTitles($list['children']);
                foreach($childData as $chk => $ch){
                    $data[$chk] = $ch + array(current($list)['id'] => current($list)['name']);
                }
            }
            if(!empty($list)){
                $data[current($list)['id']][current($list)['id']] = current($list)['name'];
            }
        }
        return $data;
    }
    
    public function addLevelNr($list){
        $levelNotAdded = array_filter($list, function($problem){
            return $problem['Problem']['parent_id'] > 0 && $problem['Problem']['level'] == 0 && $problem['Problem']['disabled'] == 0;
        });
        if(empty($levelNotAdded)) return false;
        $parentNotAddedExist = $this->find('list', array('conditions'=>array('Problem.id'=>array_unique(Set::extract('/Problem/parent_id', $levelNotAdded)))));
        if(empty($parentNotAddedExist)) return false;
        $problemsTreeList = $this->parseThreadedProblemsTitles($this->find('threaded'));
        $caseString = '';
        foreach($levelNotAdded as $problem) {
            if(isset($problemsTreeList[$problem['Problem']['id']])){
                $levelNr = sizeof($problemsTreeList[$problem['Problem']['id']]) - 1;
                $caseString .= " WHEN ".$problem['Problem']['id']." THEN ".$levelNr;
            }
        }
        if(!trim($caseString)){ return false; }
        $this->updateAll(array('Problem.level'=>'CASE Problem.id '.$caseString.' ELSE Problem.level END'));
    }
    
}
