<?php

App::uses('AppModel', 'Model');

/**
 */
class PartialQuantity extends AppModel {
	const NAME = __CLASS__;


    public function sumByApprovedOrderId($approvedOrdersIdsSamePlan) {
        $res = $this->find('first', array(
            'fields'=>array('SUM(PartialQuantity.quantity) AS sum'),
            'conditions'=>array('PartialQuantity.approved_order_id'=>$approvedOrdersIdsSamePlan)
        ));
        return $res[0]['sum']; 
        //$res = $this->query("SELECT SUM(quantity) as sum FROM partial_quantities WHERE approved_order_id=".$approved_order_id);
        //return $res[0][0]['sum'];
    }
}
