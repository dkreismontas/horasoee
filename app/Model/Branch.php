<?php

App::uses('AppModel', 'Model');

/**
 */
class Branch extends AppModel {
	const NAME = __CLASS__;
	public $requestData = array();

	public function __construct(){
//	    $this->validate = array(
//            'ps_id' => array(
//                'is_unique' => array(
//                    'rule' =>array('validation_is_unique'),
//                    'message' =>__('Padalinys su tokiu identifikatoriumi jau egzistuoja',true)
//                ),
//            )
//        );
        parent::__construct();
    }

    public function validation_is_unique($data,$field){
	    if(!$this->requestData['Branch']['id']) {
            return empty($this->findByPsId($data['ps_id']));
        }else{
            return empty($this->find('first', array('conditions'=>array('Branch.ps_id'=>$data['ps_id'], 'Branch.id <>'=>$this->requestData['Branch']['id']))));
        }
    }

    /**
	 * Build name from data
	 * @param array $li
	 * @param boolean $external if true reference item names will be added
	 * @return string
	 */
	public static function buildName(array $li, $external = true) {
		return $li[self::NAME]['name']
            //.($li[self::NAME]['ps_id'] ? (' ('.$li[self::NAME]['ps_id'].')') : '')
            ;
	}
	
	/**
	 * Get item list as associative array
	 * where key is item ID and value is item name.
	 * @param boolean $addEmpty
	 * @return array
	 */
	public function getAsSelectOptions($addEmpty = false) {
        $sensorModel = ClassRegistry::init('Sensor');
        $sensorBranches = $sensorModel->find('list', array('fields'=>array('Sensor.id','Sensor.branch_id'), 'conditions'=>array('Sensor.id'=>Configure::read('user')->selected_sensors)));
		$items = $this->find('all', array('conditions'=>array('Branch.id'=>array_unique($sensorBranches))));
		$options = array();
		if ($addEmpty) {
			$options[0] = __('-- None --');
		}
		foreach ($items as $li) {
			$options[$li[self::NAME]['id']] = self::buildName($li);
		}
		return $options;
	}

    public function parseBranchManagerEmails($emailsString) {
        if(strlen($emailsString) == 0) {
            return '';
        }
        $output = array();
        $output = explode(',',$emailsString);
        $output = array_map('trim',$output);
        return $output;
    }
    
    public function withRefs(){
        $this->bindModel(array('belongsTo'=>array('Factory')));
        return $this;
    }
	
}
