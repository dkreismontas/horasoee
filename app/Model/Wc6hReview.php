<?php
class Wc6hReview extends AppModel{

    public $useTable = 'wc6h_reviews';

    public function __construct(){
        $this->validate = array(
            'name' => array(
                'rule' => 'notBlank',
                'message' => __('Įveskite rinkinio pavadinimą', true)
            ),
            'sensors_ids'=>array(
                'rule' => 'check_sensors_count',
                'message' => __('Leidžiamas pasirinkti darbo centrų kiekis nuo 1 iki 6', true)
            )
        );
        parent::__construct();
    }

    public function check_sensors_count($data,$field){
        $sensorsIdsCount = sizeof(explode(',',$data['sensors_ids']));
        return 1 <= $sensorsIdsCount && $sensorsIdsCount <= 6;
    }

    public function combineSensorsIntoColumns($sensors, $columnsCount){
        $rowsCount = ceil(sizeof($sensors) / $columnsCount);
        $data = array();
        for($i = 0; $i < $rowsCount; $i++){
            $data[$i] = array_values(array_slice($sensors, $i*$columnsCount, $columnsCount, true));
        }
        return $data;
    }

}