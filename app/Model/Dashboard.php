<?php
App::uses('ApprovedOrder', 'Model','Record');
class Dashboard extends AppModel {
	const RecordDuration = 20; // Kas kiek laiko sukuriamas įrašas, sekundėmis

    var $name = 'Dashboard';
    var $useTable = false;
    var $components = array();
	var $indicator = 'indicator1';

	private $primaryData;
	private $modelSearch,$model;
	private $start,$end;
	private $conditions;
	private $fieldToSearch,$groupByDate,$xLegend,$angle;
	private $xLabel='xLabel';	//pagal si laukeli isvedame greafiko x asi
	private $operationField,$averageField,$operationFieldAs;
    private $sumMultiplier = 1;
	private $showType = 'single'; //grafiko rodymo tipas (single-paprastas, multiple-sugrupuotas pagal datas ir reiksme)
	private $dataStructure;
	private $axis;
	private $attr;
	private $shiftsToSearch; //pamainos, kuriose bus ieskomi duomenys; Jei bus pazymeta visos pamainos, ieskosim visose
	private $mergeData=array(); //jei vienos uzklausos metu nepavyksta visko suskaiciuot, vykdomas papildomas skaiciavimas ir ant galo duomenys sumerginami i kruva
	private $globalGroupBy = array(); //sarasas, pagal kuri visados grupuosime irasus
	private $bind; //aprasytos per masyva su kokiom lentelem vyksta sajungos
	private $allowPSIdGrouping=true;	//nurodo ar galima grupuoti pagal sensoriu. Kai skaiciuojame bendra laika tokio grupavimo nereikia (18 grafikas pvz)
	private $orderBy = array();
	private $colors = array();
    private $dateTypes = array(2=>array('z', 'day'), 3=>array('W','week'), 4=>array('m','month'));
    public $timeGroupsForSubcribsions = array('','0', '1', '2', '3', '4','9','10','11');
    private $problemsTreeList = null;
    public $time_patterns = array();
    private $sensorsList = array();

   /* <li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="0"><?php echo __('Praėjusi pamaina'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="1"><?php echo __('Praėjusi para'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="2"><?php echo __('Praėjusi savaitė'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="10"><?php echo __('Praėjusi savaitė iki dabar'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="3"><?php echo __('Praėjęs mėnuo'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="9"><?php echo __('Praėjęs mėnuo iki dabar'); ?></a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" data-timeFormat="4"><?php echo __('Praėję metai'); ?></a></li>*/

    public function getTimePatterns(){
        return array(
            0 => __('Praėjusi pamaina'),
            1 => __('Praėjusi para'),
            2 => __('Praėjusi savaitė'),
            10 => __('Praėjusi savaitė iki dabar'),
            3 => __('Praėjęs mėnuo'),
            9 => __('Praėjęs mėnuo iki dabar'),
            4 => __('Praėję metai'),
            6 => __('Ši savaitė'),
            7 => __('Šis mėnuo'),
            8 => __('Šie metai'),
        );
    }

    public function getStatisticalSubscriptionsPeriodicityTypes(){
        return array(
            0=>__('Pamaininis'),
            1=>__('Paromis'),
            2=>__('Savaitinis'),
            3=>__('Mėnesinis')
        );
    }

    private function GetLineParameters($data){
        $graphData = array();
        if(isset($data[$this->indicator]) && trim($data[$this->indicator])!==''){
            $graphData = $this->start_calculation($data,true);
//            fb($graphData,'graphgraphdata');
        }
        return $graphData;
    }

	private function setModel(){
		$bind = array('Sensor','Shift'); //duomenis visados jungiame su Sensor lentele
		$this->orderBy = array();
		switch($this->primaryData[$this->indicator]){ //atranka pagal rodikli
			case 4: case 9:
				$this->model = 'DashboardsCalculation';
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
			case 14:
				$this->model = 'DiffCard';
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
			case 15: //brokas
				$this->model = 'Loss';
				unset($bind[array_search('Sensor',$bind)]);
				$bind['ApprovedOrder'] = array();
				$bind['Sensor'] = array('className'=>'Sensor','foreignKey'=>false,'conditions'=>array('Sensor.id = ApprovedOrder.sensor_id'));
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
			case 16: //NK brokas
				$this->model = 'DiffCard';
				$bind['DiffType'] = array('className'=>'DiffType','foreignKey'=>false,'conditions'=>array($this->model.'.problem_id = DiffType.id'));
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
			case 17: //deklaruotas brokas
				$this->model = 'Loss';
				unset($bind[array_search('Sensor',$bind)]);
				$bind['ApprovedOrder'] = array();
				$bind['Sensor'] = array('className'=>'Sensor','foreignKey'=>false,'conditions'=>array('Sensor.id = ApprovedOrder.sensor_id'));
				$bind['LossType'] = array();
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
            case 18: //darbo laikai
				/*$this->model = 'DashboardsCalculation';
				$this->modelSearch = ClassRegistry::init($this->model);*/
				$this->model = 'FoundProblem';
                $bind['Problem'] = array('className'=>'Problem','foreignKey'=>false,'conditions'=>array('FoundProblem.super_parent_problem_id = Problem.id'));
                $this->modelSearch = ClassRegistry::init($this->model);
                $this->orderBy = array('Problem.id');
                $problemModel = ClassRegistry::init('Problem');
                $mainProblemsTitles = $problemModel->find('list', array('fields'=>array('id','name'), 'conditions'=>array('parent_id'=>0), 'recursive'=>-1));
                $colors = array(-1=>'#5AB65A', 1=>'#FDEE00', 2=>'#666666', 3=>'#D64D49',''=>'#fa768b');
                foreach($mainProblemsTitles as $problemId => $problemTitle){
                     $this->colors[Settings::translate($problemTitle)] = isset($colors[$problemId])?$colors[$problemId]:$colors[3];
                }
			break;
            case 19: // prastovu priezastys tevines
            case 22: // Prastovų kiekis tevines
                $this->model = 'FoundProblem';
                $bind['Problem'] = array('className'=>'Problem','foreignKey'=>false,'conditions'=>array('FoundProblem.super_parent_problem_id = Problem.id'));
                $this->modelSearch = ClassRegistry::init($this->model);
            break;
            case 26: // prastovu priezastys
            case 27: // Prastovų kiekis
            case 6: // Prastovų laikas
                $this->model = 'FoundProblem';
                $bind['Problem'] = array('className'=>'Problem','foreignKey'=>false,'conditions'=>array('FoundProblem.problem_id = Problem.id'));
                $this->modelSearch = ClassRegistry::init($this->model);
            break;
			case 20: //MTTR
				$this->model = 'DashboardsCalculation';
				$this->modelSearch = ClassRegistry::init($this->model);
			break;
			case 21: //MTTF
                $this->model = 'DashboardsCalculation';
                $this->modelSearch = ClassRegistry::init($this->model);
			break;
            default:
                $this->model = 'DashboardsCalculation';
                $this->modelSearch = ClassRegistry::init($this->model);
            break;
		}
		if(in_array('Sensor',$bind) || isset($bind['Sensor'])){ //visur jei tik yra sensor lentele prijungiame branch lentele
			$bind['Branch'] = array('className'=>'Branch', 'foreignKey'=>false, 'conditions'=>array('Sensor.branch_id = Branch.id'));
		}

		$this->bind = $bind;
		$this->modelSearch->bindModel(array('belongsTo'=>$bind),false);
        
        if($this->problemsTreeList === null){
            $problemModel = ClassRegistry::init('Problem');
            $this->problemsTreeList = $problemModel->parseThreadedProblemsTitles($problemModel->find('threaded'));
        }
        $sensorModel = ClassRegistry::init('Sensor');
        $this->sensorsList = $sensorModel->find('list', array('fields'=>array('id','name'), 'conditions'=>array('Sensor.id'=>$this->primaryData['sensors'])));
	}

	private function setConditions(){
		if(isset($this->primaryData['time_pattern']) && $this->primaryData['time_pattern'] >= 0 && is_numeric($this->primaryData['time_pattern'])){
			list($this->start,$this->end) = $this->components['help']->setStartEndByPattern($this->primaryData);
			if($this->model == 'FoundProblem' && $this->primaryData['time_pattern'] == 5){ //siandien intervalo atveju traukiant is found_problems lenteles irasus laika galima pateikti iki esamo momento
                $this->end = date('Y-m-d H:i:s');
            }
		}else{
		    if(isset($this->primaryData['date_start_end'])){
                @list($this->primaryData['date_start'], $this->primaryData['date_end']) = explode(' ~ ', $this->primaryData['date_start_end']);
            }
			if(preg_match('/\d{4}-/', trim($this->primaryData['date_start'])) || !trim($this->primaryData['date_start'])){
	        	$this->start = trim($this->primaryData['date_start'])?trim($this->primaryData['date_start']):current(current($this->modelSearch->find('first',array('fields'=>array('MIN(Shift.start) as min_start'),'conditions'=>array('Shift.start <>'=>0)))));
	            $this->end = trim($this->primaryData['date_end'])?trim($this->primaryData['date_end']):date('Y-m-d H:i');
	        }elseif(is_numeric(trim($this->primaryData['date_start']))){
	            $this->start = date('Y-m-d H:i',strtotime('-'.trim($this->primaryData['date_start'].' days')));
				$this->end = date('Y-m-d H:i');
			}else{
				//die('Neteisingai paduotas pradžios laikas');
			}
		}
        $this->start = date('Y-m-d H:i:s',strtotime($this->start));
        $this->end = date('Y-m-d H:i:s',strtotime($this->end));
		$from = array('Shift.start < \''.$this->end.'\'');
        $till = array('Shift.end >= \''.$this->start.'\'');
		if(in_array($this->primaryData[$this->indicator],array(14,16,18))){ 
			$from = array($this->model.'.start < \''.$this->end.'\'');
        	$till = array($this->model.'.start >= \''.$this->start.'\'');
		}
		if(in_array($this->primaryData[$this->indicator],array(15,17))){ //Loss
			$from = array('ApprovedOrder.end <= \''.$this->end.'\'');
        	$till = array('ApprovedOrder.end > \''.$this->start.'\'');
		}
        if($this->model == 'FoundProblem'){//2019-07-10 salyga reikalinga, kai intervalai nepatenka pilnai i pamaina, kad nesigautu minusiniu laiku, nes tada pakanka, kad prastova patenka i pamaina, bet gali nepatekti i intervala
		//if(in_array($this->primaryData[$this->indicator],array(18,19))){ //FoundProblem
			$from = array($this->model.'.start < \''.$this->end.'\'');
       	    $till = array($this->model.'.end > \''.$this->start.'\'');
		}
        $sensorCondition = $shiftCondition = array();
		if(!empty($this->primaryData['sensors'])){
			$sensorCondition = array('Sensor.id'=>$this->primaryData['sensors']);
		}
        if($this->model == 'FoundProblem' && (!isset($this->primaryData['add_downtime_exclusions']) || $this->primaryData['add_downtime_exclusions'] == 0)){
            $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
            $exclusionList = $plugin_model::getExclusionIdList();
            if(!empty($exclusionList)) {
                $sensorCondition[$this->model . '.problem_id NOT'] = $exclusionList;
            }
        }
		if(!isset($this->primaryData['add_disabled_shifts']) || $this->primaryData['add_disabled_shifts'] == 0){
        	$shiftCondition[] = array('Shift.disabled'=>0);
		}	
		if(!empty($this->shiftsToSearch) && !in_array('-1',$this->primaryData['shifts'])){
			$shiftCondition[] = array('Shift.type'=>$this->shiftsToSearch);
		}
		$this->conditions = array('conditions'=>array_merge($from,$till,$sensorCondition, $shiftCondition));
        /*if(in_array($this->primaryData['time_group'], array(2,3,4)) && in_array($this->primaryData[$this->indicator], array(0,1,2,23,24))){ //savaitinis bei menesinis grupavimas ir grafiko tipas yra OEE, prieinamumo bei efektyvumo koef
            $type = $this->dateTypes[$this->primaryData['time_group']][1];
            $this->conditions = array('conditions'=>array_merge($sensorCondition, array("CONCAT($this->model.year,'-',$this->model.$type)" => $this->getPeriodsList())));
        }*/
	}
    
    //gaunamas nuo start iki end metai-menuo arba metai-savaites nr sarasas, pvz 2017-22, 2017-23, ...
    private function getPeriodsList(){
        $start = strtotime($this->start);
        $periodsList = array();
        while($start < strtotime($this->end)){
            $periodsList[] = date('Y', $start).'-'.(int)date($this->dateTypes[$this->primaryData['time_group']][0], $start);
            $start = strtotime('+1 '.$this->dateTypes[$this->primaryData['time_group']][1], $start);
        }
        return $periodsList;
    }

	//nustatoma kokiu laukeliu ieskosime ir kieno atzvilgiu pamainos ar sensoriaus isvesime grafika
	private function setSearchParameters(){
        $this->groupByDate='';
        $this->angle = array('field'=>'Sensor.name','column'=>'Sensor','as'=>'');
		$this->xLegend = __('Darbo centrai');
		// if(isset($this->primaryData['shifts']) && sizeof($this->primaryData['sensors']) < sizeof($this->primaryData['shifts'])){
			// //$this->angle = array('field'=>'SUBSTR(Shift.name,12)','as'=>'AS real_name','column'=>array(0,'real_name'));
			// $this->angle = array('field'=>'Shift.ps_id','as'=>'','column'=>'Shift');
			// $this->xLegend = __('Pamainos');
		// }
		if(in_array($this->primaryData[$this->indicator],array(16))){ //NK pagal priezastis
			$this->angle = array('field'=>'DiffType.name','column'=>'DiffType','as'=>'');
			$this->xLegend = __('Priežastys');
		}
		if(in_array($this->primaryData[$this->indicator],array(17))){ //deklaruoras brokas pagal priezastis
			$this->angle = array('field'=>'LossType.name','column'=>'LossType','as'=>'');
            $this->globalGroupBy[] = 'LossType.name';
			$this->xLegend = __('Priežastys');
		}
		if(in_array($this->primaryData[$this->indicator],array(18))){ //darbo laikai pagal priezastis
			/*$this->fieldToSearch[] = 'SUM(DashboardsCalculation.transition_time) as `Perėjimas`';
			$this->fieldToSearch[] = 'SUM(DashboardsCalculation.true_problem_time) as `Prastovos`';
			$this->fieldToSearch[] = 'SUM(DashboardsCalculation.no_work_time) as `Nėra darbo`';
			$this->fieldToSearch[] = 'SUM(DashboardsCalculation.other_product_time) as `Kitas gaminys`';*/
			$this->angle = array('field'=>'Problem.name','column'=>'Problem','as'=>'');
            $this->globalGroupBy[] = 'Problem.name';
            $this->xLegend = __('Prastovos');
		}
		if(in_array($this->primaryData[$this->indicator],array(19,26))){ //deklaruoras brokas pagal priezastis
			$this->angle = array('field'=>'Problem.name','column'=>'Problem','as'=>'');
			$this->globalGroupBy[] = 'Problem.name';
			$this->xLegend = __('Prastovos');
		}
        if(in_array($this->primaryData[$this->indicator],array(22, 27))){ //prastovų kiekis
            $this->angle = array('field'=>'Problem.name','column'=>'Problem','as'=>'');
            $this->globalGroupBy[] = 'Problem.name';
            $this->xLegend = __('Prastovos');
        }
		$this->showType = 'single';
        switch($this->primaryData['time_group']){
        	case 0://suminis, grupujuojamas pagal jutiklius, concat naudojamas tik tam, kad cakePHP imestu irasa i ta pacia vieta kaip ir sum
				if($this->angle['column']=='Sensor'){
				    if(isset($this->primaryData['combine_all_sensors']) && $this->primaryData['combine_all_sensors']){
					    $this->fieldToSearch = array('\''.__('Bendra').'\' AS xLabel');
                    }else{
                        $this->fieldToSearch[] = 'CONCAT("",'.$this->angle['field'].') AS xLabel';
                    }
					$this->globalGroupBy[] = 'xLabel';
				}else $this->fieldToSearch[] = 'CONCAT('.$this->angle['field'].') AS xLabel';
			break;
			case 1: //pamaina
				$this->groupByDate = 'Shift.start';
//				$this->fieldToSearch[] = 'CONCAT(Shift.id," ", DATE_FORMAT(Shift.start,\'%d %H:%i\')) AS xLabel';
                $this->fieldToSearch[] = 'CONCAT(DATE_FORMAT(Shift.start,\'%Y-%m-%d %H:%i\'),\' \',Shift.type) AS xLabel';
				$this->showType = 'multiple';
			break;
            case 2: //vidurkis dienoje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m-%d\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%m%d\')';
				$this->showType = 'multiple';
            break;
            case 3: //vidurkis savaiteje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%v\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%v\')';
				$this->showType = 'multiple';
            break;
            case 4: //vidurkis menesyje
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y%m\')';
				$this->showType = 'multiple';
            break;
            case 5: //vidurkis metuose
            	$this->fieldToSearch[] = 'DATE_FORMAT(Shift.start, \'%Y-%m-%d\') AS xLabel';
                $this->groupByDate = 'DATE_FORMAT(Shift.start, \'%Y\')';
				$this->showType = 'multiple';
            break;
        }
        /*if(in_array($this->primaryData['time_group'], array(2,3,4)) && in_array($this->primaryData[$this->indicator], array(0,1,2,23,24))){ //savaitinis bei menesinis grupavimas ir grafiko tipas yra OEE, prieinamumo bei efektyvumo koef
            $this->fieldToSearch = array();
            $type = $this->dateTypes[$this->primaryData['time_group']][1];
            if($this->primaryData['time_group'] == 2){
                $this->fieldToSearch[] = "MAKEDATE($this->model.year, $this->model.day+1) AS xLabel";
            }else{
                $this->fieldToSearch[] = "CONCAT($this->model.year,'-',$this->model.$type) AS xLabel";
            }
            $this->groupByDate = $type;
        }*/
		//pagal grafiko laiko grupavima nurodom koks bus grupavimas bei pagal ka isvesime grafika (pamainos ar sensoriai)
		//visi nesuminiai grafikai issiveda sensoriu padalinio ir sensoriaus id atzvilgiu, todel 18 grafika ignoruojame
		if(($this->showType == 'multiple' || $this->angle['column'] == 'Shift') && $this->allowPSIdGrouping){
			if($this->angle['column'] == 'Sensor' ){
				$this->fieldToSearch[] = 'CONCAT(Sensor.ps_id,\':\',SUBSTR(Sensor.name,1,12)) AS ps_id';
				$this->angle['column'] = array(0,'ps_id');
				$this->globalGroupBy[] = 'ps_id';
			}elseif($this->angle['column'] == 'Shift'){
				$this->fieldToSearch[] = 'Shift.ps_id';
				//$this->globalGroupBy[] = 'Shift.ps_id';
			}elseif($this->angle['column'] == 'Problem'){
				$this->fieldToSearch[] = 'CONCAT(Problem.name) AS problem_name';
				$this->angle['column'] = array(0,'problem_name');
				$this->globalGroupBy[] = 'problem_name';
			}
		}
        if(isset($this->primaryData['combine_all_sensors']) && $this->primaryData['combine_all_sensors']){
            $this->globalGroupBy = array();
        }
	}

	private function setGraphType(){
		$this->averageField = '/ COUNT(distinct('.$this->model.'.shift_id))';
        $this->sumMultiplier = 1;
        $this->operationFieldAs = 'sum';
		switch($this->primaryData[$this->indicator]){  //rakursas pagal ka isvedame grafika
		 	case 0:	//OEE be isimciu skaiciavimas
		 	    //if($this->primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $qualitypart = '(IF(Sensor.calculate_quality > 0, SUM('.$this->model.'.good_quantity), 1) / IF(Sensor.calculate_quality > 0, SUM('.$this->model.'.all_quantity), 1))';
                    $this->fieldToSearch[] = $qualitypart.' * (SUM('.$this->model.'.count_delay) / SUM('.$this->model.'.fact_prod_time)) * (SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length)) * 100 AS sum';
                //}
				$this->operationField = 'LEAST(100,'.$this->model.'.oee)';
				$this->yLegend = 'OEE be išimčių';
                $this->primaryData['action'] = 2;
			break;
            case 23:	//OEE su išimtimis skaiciavimas
                //if($this->primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $qualitypart = '(IF(Sensor.calculate_quality > 0, SUM('.$this->model.'.good_quantity), 1) / IF(Sensor.calculate_quality > 0, SUM('.$this->model.'.all_quantity), 1))';
                    $this->fieldToSearch[] = $qualitypart.' * (SUM('.$this->model.'.count_delay) / SUM('.$this->model.'.fact_prod_time)) * (SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length_with_exclusions)) * 100 AS sum';
                //}
                $this->operationField = $this->model.'.oee_with_exclusions';
                $this->yLegend = __('OEE');
                $this->primaryData['action'] = 2;
            break;
		 	case 1:	//Prieinamumo koeficientas be isimciu
		 	    $this->fieldToSearch[] = 'SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length) * 100 AS sum';
				$this->yLegend = __('Prieinamumo koef. be išimčių').'';
                $this->primaryData['action'] = 2;
			break;
            case 24:	//Prieinamumo su išimtimis koeficientas
                $this->fieldToSearch[] = 'SUM('.$this->model.'.fact_prod_time) / SUM('.$this->model.'.shift_length_with_exclusions) * 100 AS sum';
                $this->yLegend = __('Prieinamumo koef.').')';
                $this->primaryData['action'] = 2;
                break;
		 	case 2:	//Veiklos efektyvumo koeficientas
		 	    //if($this->primaryData['time_group'] == 0){//suminio grupavimo atveju skaiciuojame dinamiskai is naujo
                    $this->fieldToSearch[] = 'SUM('.$this->model.'.count_delay) / SUM('.$this->model.'.fact_prod_time) * 100 AS sum';
                //}
				$this->operationField = $this->model.'.operational_factor';
                $this->sumMultiplier = 100;
				$this->yLegend = __('Veiklos efektyvumo koef.').')';
                $this->primaryData['action'] = 2;
			break;
		 	case 3:	//Kokybės koeficientas
				$this->operationField = $this->model.'.quality_factor';
                $this->sumMultiplier = 100;
				$this->yLegend = __('Kokybės koef.').'';
			break;
		 	case 4:	//Gamybos laikas
//				$this->operationField = self::RecordDuration/60;
				$this->operationField = $this->model.'.fact_prod_time';
				$this->yLegend = __('Gamybos laikas').' ('.__('min.').')';
//				$this->conditions['conditions'][]=$this->model.'.found_problem_id IS NULL';
				//$this->conditions['conditions']['Sensor.branch_id'] = 2;
			break;
			case 5: //perėjimų laikas (problemos id = 1)
                $this->operationField = $this->model.'.transition_time';
//				$this->operationField = 'TIMESTAMPDIFF(SECOND,
//                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
//                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END)/60';
//				$this->conditions['conditions'][$this->model.'.problem_id'] = 1;
				$this->yLegend = __('Derinimų laikas').' ('.__('min.').')';
			break;
			case 6: //prastovu laikas (problemos id >= 3)
				$this->operationField = 'TIMESTAMPDIFF(SECOND,
                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END)/60';
				$this->conditions['conditions'][$this->model.'.problem_id >='] = 3;
				$this->yLegend = __('Prastovų laikas').' ('.__('min.').')';
			break;
			case 7: //nera darbo laikas (problemos id = 2)
                $this->operationField = $this->model.'.no_work_time';
//				$this->operationField = 'TIMESTAMPDIFF(SECOND,
//                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
//                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END)/60';
//				$this->conditions['conditions'][$this->model.'.problem_id'] = 2;
				$this->yLegend = __('Nėra darbo laikas').' ('.__('min.').')';
			break;
			case 8: //gaminamas kitas produktas laikas (problemos id = 14)
                $this->operationField = $this->model.'.other_product_time';
//				$this->operationField = 'TIMESTAMPDIFF(SECOND,
//                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
//                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END)/60';
//				$this->conditions['conditions'][$this->model.'.problem_id'] = 14;
				$this->yLegend = __('Gaminamas kitas produktas').' ('.__('min.').')';
			break;
			case 9: //Pagamintas kiekis
				$this->operationField = $this->model.'.total_quantity';
				//$this->conditions['conditions']['Sensor.type'] = 3;
				$this->yLegend = __('Kiekis');
			break;
			case 14: //NK brokas
				$this->operationField = $this->model.'.consequences';
				$this->yLegend = __('NK brokas').' ('.__('kg').')';
			break;
			case 15: //brokas
				$this->operationField = $this->model.'.value';
				$this->yLegend = __('Brokas').' ('.__('kg').')';
			break;
			case 16: //NK pagal priezastis
				$this->operationField = $this->model.'.consequences';
				$this->yLegend = __('NK brokas pagal priežastis').' ('.__('kg').')';
			break;
			case 17: //deklaruotas brokas pagal priezastis
				$this->operationField = $this->model.'.value';
				$this->yLegend = __('Deklaruotas brokas').' ('.__('kg').')';
			break;
			case 18: //darbo laikai
                /*$this->operationField = -1000000;
				$data = $this->primaryData;
				$data['indicator1'] = 4; //rankiniu budu sugeneruojame nuostatas, su kuriomis pasiimame tik gamybos laikus. Gautus duomenis sujungsime su problemu laikais
				$data['summarize'] = '0';
				$dashboardModel = new self();
				$dashboardModel->allowPSIdGrouping = false;
				$dashboardModel->components = &$this->components;
				$dashboardModel->indicator = 'indicator1';
				$data = $dashboardModel->start_calculation($data,true);
				if($this->showType=='single' && isset($data['data'][0])){
					$this->mergeData[__('Darbo laikas')] = array_sum($data['data'][0]);
				}else{
                    $sum = 0;
                    $entries = 0;
                    if($data['data']){
                    foreach($data['data'] as $index=>$insideArray) {
                        foreach($insideArray as $date => $val) {
                            $this->mergeData[__('Darbo laikas')][$date] = $val;
                            $sum += $val;
                            $entries++;
                        }
                    }
                    if(isset($this->primaryData['summarize']) && $this->primaryData['summarize']==1) {
                        switch($this->primaryData['action']){
                            case 1:
                                $this->mergeData[__('Darbo laikas')]['suma'] = $sum;
                                break;
                            case 2:
                                $this->mergeData[__('Darbo laikas')]['vidurkis'] = $sum / $entries;
                                break;
                        }

                    }
                    }
//					$this->mergeData[__('Darbo laikas')] = current($data['data']);
				}
				$this->yLegend = __('Darbo laikai').' ('.__('min').')';*/
				$this->sumMultiplier = 0.01666667;
                $this->averageField = '/ COUNT(*)';
                $addOneSecond = $this->model == 'FoundProblem'?1:0; //1s reikia prideti, nes found_problems lenteles intyervalai nesikerta, t.y. sekancio intervalo pradzia yra 1s didesni nei pries tai buvio intervalo end laikas
                $this->operationField = '('.$addOneSecond.'+TIMESTAMPDIFF(SECOND,
                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END))';
                $this->conditions['conditions']['Problem.id <>'] = 0;
                // $this->conditions['conditions']['TIMESTAMPDIFF(SECOND,
                    // CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
                    // CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END) > ?'] = array('60');
                $this->yLegend = __('Darbo laikas').' ('.__('min.').')';
			break;
			case 19: //prastovų priežastys tevines
			case 26: //prastovų priežastys
                $this->sumMultiplier = 0.01666667;
                $this->averageField = '/ COUNT(*)';
                $addOneSecond = $this->model == 'FoundProblem'?1:0; //1s reikia prideti, nes found_problems lenteles intyervalai nesikerta, t.y. sekancio intervalo pradzia yra 1s didesni nei pries tai buvio intervalo end laikas
				$this->operationField = '('.$addOneSecond.'+TIMESTAMPDIFF(SECOND,
                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END))';
				$this->conditions['conditions']['Problem.id >='] = 1;
                if(isset($this->primaryData['downtimes']) && !empty($this->primaryData['downtimes']) && $this->primaryData[$this->indicator] != 19){
                    $innerDowntimesIds = array();
                    foreach($this->primaryData['downtimes'] as $selectedDowntimeId){
                        foreach($this->problemsTreeList as $downtimeId => $downtimesTree){
                            if(!isset($downtimesTree[$selectedDowntimeId])){ continue; }
                            $innerDowntimesIds = array_unique(array_merge($innerDowntimesIds, array_keys(array_slice($downtimesTree, 0, array_search($selectedDowntimeId,array_keys($downtimesTree))+1,true))));
                        }
                    }
                    $this->conditions['conditions']['Problem.id'] = $innerDowntimesIds;
                }elseif(isset($this->primaryData['downtime_level']) && strlen($this->primaryData['downtime_level']) > 0){
                    $this->conditions['conditions']['Problem.level >='] = $this->primaryData['downtime_level'];
                }
				// $this->conditions['conditions'][$addOneSecond.'+TIMESTAMPDIFF(SECOND,
                    // CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
                    // CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END) > ?'] = array('60');
                $this->yLegend = __('Prastovos pagal priežastis').' ('.__('min.').')';
                $this->fieldToSearch[] = 'Problem.id';
                $this->fieldToSearch[] = 'Problem.level';
			break;
            case 20: //MTTR
//                $this->operationField = 'TIMESTAMPDIFF(SECOND,
//                    CASE WHEN '.$this->model.'.start > \''.$this->start.'\' THEN '.$this->model.'.start ELSE \''.$this->start.'\' END,
//                    CASE WHEN '.$this->model.'.end < \''.$this->end.'\' THEN '.$this->model.'.end ELSE \''.$this->end.'\' END)/60';
//                $this->averageField = '/ COUNT(*)';
//                $this->conditions['conditions']['Problem.id >='] = 3;
                $this->operationField = $this->model.'.mttr';
                $this->yLegend = __('MTTR').' ('.__('min.').')';
                break;
			case 21: //MTTF skirtumai tarp problemu paspaudimu
                $this->operationField = $this->model.'.mttf';
//				$this->averageField = '/ COUNT(*)';
//				$this->operationField = 'diff';
//				$this->conditions['conditions']['Problem.id >='] = 3;
				$this->yLegend = __('MTTF').' ('.__('min.').')';
			break;
            case 22: //prastovų kiekis tevines
            case 27: //prastovų kiekis
//                $this->averageField = '/ COUNT(*)';
                $this->operationField = '1';
                $this->yLegend = __('Prastovų kiekis');
                $this->conditions['conditions']['Problem.id >='] = 1;
                $this->conditions['conditions'][] = 'FoundProblem.start < FoundProblem.end';
                if(isset($this->primaryData['downtimes']) && !empty($this->primaryData['downtimes']) && $this->primaryData[$this->indicator] != 22){
                    $innerDowntimesIds = array();
                    foreach($this->primaryData['downtimes'] as $selectedDowntimeId){
                        foreach($this->problemsTreeList as $downtimeId => $downtimesTree){
                            if(!isset($downtimesTree[$selectedDowntimeId])){ continue; }
                            $innerDowntimesIds = array_unique(array_merge($innerDowntimesIds, array_keys(array_slice($downtimesTree, 0, array_search($selectedDowntimeId,array_keys($downtimesTree))+1,true))));
                        }
                    }
                    $this->conditions['conditions']['Problem.id'] = $innerDowntimesIds;
                }elseif(isset($this->primaryData['downtime_level']) && strlen($this->primaryData['downtime_level']) > 0){
                    $this->conditions['conditions']['Problem.level >='] = $this->primaryData['downtime_level'];
                }
                $this->fieldToSearch[] = 'Problem.id';
                $this->fieldToSearch[] = 'Problem.level';
            break;
            case 25: //virsytu perejimu laikas (problemos id = pagal nustatyma exceeded_transition_id)
                $this->operationField = $this->model.'.exceeded_transition_time';
                $this->yLegend = __('Viršytų derinimų laikas').' ('.__('min.').')';
            break;
            case 28: //virsytu perejimu laikas (problemos id = pagal nustatyma exceeded_transition_id)
                $this->operationField = $this->model.'.transition_time + '.$this->model.'.exceeded_transition_time';
                $this->yLegend = __('Derinimų palyginimas (min)');
                $this->fieldToSearch[] = 'SUM('.$this->model.'.theorical_transition_time) AS theorical_transition';
                $this->globalGroupBy = array();
            break;
            case 29: //virsytu perejimu laiku nuokrypis (problemos id = pagal nustatyma exceeded_transition_id)
                $this->operationField = $this->model.'.transition_time + '.$this->model.'.exceeded_transition_time';
                $this->yLegend = __('Derinimų palyginimas (min)');
                $this->fieldToSearch[] = 'SUM('.$this->model.'.theorical_transition_time) AS theorical_transition';
                $this->globalGroupBy = array();
            break;
		}
	}

	private function executeSearch(){
	    $params = array(&$this->primaryData, &$this->indicator, &$this->fieldToSearch, &$this->groupByDate, &$this->model, &$this->colors);
        $this->callPluginFunction('Dashboard_beforeExecuteSearch_Hook', $params, Configure::read('companyTitle'));
		if(!array_filter($this->fieldToSearch, function($field){ return strpos(strtolower($field), 'as sum');})){
		    $averageAction = isset($this->primaryData['action']) && $this->primaryData['action']==2 && trim($this->averageField)?$this->averageField:'';
		    $this->fieldToSearch[] = '(SUM('.$this->operationField.')*'.$this->sumMultiplier.' '.$averageAction.') AS '.$this->operationFieldAs;
		}
        //$this->fieldToSearch['XX'] = 'COU()';
		//$averageAction = isset($this->primaryData['action']) && $this->primaryData['action']==2 && trim($this->averageField)?"/ COUNT(distinct({$this->averageField}))":'';
		$this->attr = array(
            'fields'=>$this->fieldToSearch,
            'group' => $this->components['help']->strip_empty_elements(array_merge(array($this->groupByDate),$this->globalGroupBy)),
            'order' => $this->orderBy,
            'y_legend' => $this->yLegend,
            'x_legend' => $this->xLegend,
        );
        //fb($this->components['help']->strip_empty_elements(array_merge(array($this->groupByDate),$this->globalGroupBy)));
       // $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
        //fb($this->dataStructure);die();
//fb(array_merge($this->conditions,$this->attr));
//die();
// $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
// fb($this->fieldToSearch);
// die();
//fb($this->attr);
//fb(array_merge($this->conditions,$this->attr));
// $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));die();
// die();
        try{
		    $this->dataStructure = $this->modelSearch->find('all',array_merge($this->conditions,$this->attr));
        }catch(Exception $e){
            pr($e);
            $this->dataStructure = array();
        }
		//fb($this->dataStructure,'duomenu stuktura neapdorota');
	}

	private function setAxis(){
		$this->axis = array(
            'xAxis' => array(0=>array($this->xLabel)),
            'columns' => $this->angle['column'],
            'valueColumn'=>0,
            'showType'=>$this->showType,
        );
	}

	//Nurodo is kokiu pamainu imti duomenis, bet nenusako valdikliu ar pamainu atzvilgiu bus isvedami duomenys
	private function setShifts(){
		//if(in_array('-1',$this->primaryData['shifts']) && $this->primaryData['action'] > 0){
		if(in_array('-1',$this->primaryData['shifts'])){
			//$shiftsModel = ClassRegistry::init('Shift');
			//$this->shiftsToSearch = $shiftsModel->find('list',array('fields'=>array('Shift.ps_id','Shift.ps_id'),'order'=>array('Shift.id DESC'), 'limit'=>100, 'group'=>'Shift.ps_id'));
		}else{
			$this->shiftsToSearch = $this->primaryData['shifts'];
		}
	}

    public function start_calculation($data,$onlyData=false){
            $lineParameters = array();
			// if(!$onlyData){
                // //$this->indicator = 'indicator2';
// //                fb($data,'datada');
                // $lineParameters = $this->GetLineParameters($data);
// //                fb($lineParameters,'lineparams');
                // $this->indicator = 'indicator1';
            // }
			$this->modelSearch = $this->fieldToSearch=$this->groupByDate=$this->xLegend=$this->angle = $this->attr= null;
			$this->conditions = $this->bind = null;
			$this->mergeData = $this->globalGroupBy = array();
			$this->allowPSIdGrouping = true;
			$this->averageField = '';
            $this->colors = array();
			$this->primaryData = $data;
            $this->setModel();
			$this->setShifts();
			$this->setConditions();
			$this->setSearchParameters();
			$this->setGraphType();
			$this->executeSearch();
			$this->setAxis();
            $options = array(
                'start' => $this->start,
                'end' => $this->end,
            );
            $pareto = 0;
            $columnsType = 0;
            $step = 0;
            $step2 = 0;
            $graphWidth = 1000;
            $legends = array();
            $legends = array(
                //'widget1Tooltip' => isset($legends['widget1Tooltip'])?$legends['widget1Tooltip']:__('Valandos')
            );
            $this->reconstructData();
            $this->set(array('data'=>$this->primaryData, 'start'=>$this->start, 'end'=>$this->end));

            $title= $this->primaryData['graph_name']."\n".$this->start.' - '.$this->end;
            $sumValues = $this->primaryData['type'] == 1?false:true;
			$logModel = ClassRegistry::init('Log');
			if(empty($this->dataStructure)){
                $logModel->write(__('no data on $this->dataStructure on table: '.$this->modelSearch->useTable).' CONDITIONS: '.json_encode(array_merge($this->conditions,$this->attr)));
                fb($data);
                $noDataMessage = '
                <h5>'.__('Nėra duomenų').'</h5><br />
                <table class="">
                    <tr><td style="padding-right: 20px;"><b>'.__('Grafiko pavadinimas').':</b></td><td>'.$data['graph_name'].'</td></tr>
                    <tr><td style="padding-right: 20px;"><b>'.__('Grafiko tipas').':</b></td><td>'.$this->setIndicators()[$data['indicator1']].'</td></tr>
                    <tr><td style="padding-right: 20px;"><b>'.__('Nurodytas laikotarpis').':</b></td><td>'.$data['date_start_end'].'</td></tr>
                    <tr><td style="padding-right: 20px;"><b>'.__('Darbo centrai').':</b></td><td>'.implode('<br />', $this->sensorsList).'</td></tr>
                </table>
                ';
                echo $noDataMessage;return;//__('Nėra duomenų. '); return;
            }
//
//            $records = array();
//            $paretoData = NULL;
//            fb($this->mergeData,"records outer");
            if($this->primaryData['export_type'] == 0){
                $timeGroup = isset($this->primaryData['time_group'])?$this->primaryData['time_group']:0;
                $records = $paretoData = array();
                if(!empty($this->dataStructure[0]) && !empty($this->dataStructure[0][0]) && is_numeric($this->dataStructure[0][0]['sum']) && $this->dataStructure[0][0]['sum'] <= -1000000) {
                    $dataStructure0 = $this->components['help']->splitMulticolumnData($this->dataStructure,$timeGroup);
                    foreach($dataStructure0 as $key=>$struct) {
                        $this->axis['columns'][1] = $key;
                        list($records,$paretoData) = $this->components['help']->collectData($struct,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues,$timeGroup);
                        $this->adjustAction($records);
                        if(!empty($this->mergeData)){
                            if($this->showType == 'single') {
                                $records[0] = array_merge($records[0],$this->mergeData);
                                $this->mergeData = $records[0];
                            }
                            else {
                                $records = array_merge($records,$this->mergeData);
                                $this->mergeData = $records;
                            }
                        }
                    }
                } else {
                    list($records,$paretoData) = $this->components['help']->collectData($this->dataStructure,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues,$timeGroup);
                    $this->adjustAction($records);
                    if(!empty($this->mergeData)){
                        if($this->showType == 'single') $records[0] = array_merge($records[0],$this->mergeData);
                        else $records = array_merge($records,$this->mergeData);
                    }
                }
                $options = array_merge($options,array(
                    'data' => $records,
                    'selectedInfo' => $this->primaryData,
                    'paretoData' => $paretoData,
                    'columnsType' => $columnsType,
                    'step' => $step,
                    'step2' => $step2,
                    'graph_title' => $title.' ['.implode(', ',$this->sensorsList).']',
                    'y_legend' => __($this->attr['y_legend']),
                    'y2_legend' => '%',
                    'x_legend' => $this->attr['x_legend'],
                    'root_path'=> ROOT.'/app/webroot/img/graphs/',
                    'graphWidth' => $graphWidth,
                    //'limit' => sizeof($this->primaryData[key($data)]),
                    'steps' => $step,
                    'legends'=>$legends,
                    'showType'=>$this->showType,
                    'colors'=>$this->colors
                ));
                if($onlyData) return $options;
				//isvalome tuscius duomenis
                if(!isset($options['data'][0])){ //jei ne grupiniai duomenys
                	$tempData = $options['data'];
					$options['data'] = $tempData; unset($tempData);
				}else{
                    //uzkomentuota 2020-05-18 Aurimo prasymu isvesti 0 suminiu grafiku atveju
					//$tempData = $options['data'][0];
                	//array_walk($options['data'][0],function($val,$key)use(&$tempData){if($val==0){unset($tempData[$key]);}});
					//$options['data'][0] = $tempData; unset($tempData);
				}
//                fb($options['data'],'optsdata');
                if(empty($options['data'])){
                	$logModel->write(__('no data on $options[data]').json_encode($options));
                    echo __('Nėra duomenų '); return true;
                }
                switch($this->primaryData['graph_type']){
                    case 0: //linijinis
                        return $this->components['googlechart']->constructGraph($options,$lineParameters);
                        //return $this->components['opengraph']->constructGraphLine($options,$lineParameters);
                    break;
                    case 1://stulpelinis
                        //return  $this->components['opengraph']->constructGraph($options,$lineParameters); //flashinis grafikas
                        return  $this->components['googlechart']->constructGraph($options,$lineParameters); //svg grafikas
                        //return  $this->components['opengraph']->constructGraph($options);
                    break;
                    case 2://skritulinis
                    	if(isset($options['data'][1])) return __('Skritulinis grafikas gali būti atvaizduojamas tik pagal suminį laiko grupavimą');
                        return  $this->components['googlechart']->constructGraph($options,$lineParameters);
                        //return  $this->components['opengraph']->constructGraphPie($options);
                    break;
                }
                die();
            }else{
                 switch($this->primaryData['export_type']){
                    case 2: //grafiko ir duomenu eksportavimas
                        list($records,$paretoData) = $this->components['help']->collectData($this->dataStructure,$this->axis,$pareto,$this->axis['valueColumn'],$sumValues);
                        return array('data'=>$records, 'title'=>$title, 'file_name'=>$this->primaryData['file_name']);
                    break;
                    /*case 3: //duomenu strukturos eksportas
                        $title = __('Duomenų struktūros').' '.$start.' - '.$end;
                        $this->_exportDataStructures($dataStrucure,$title,$file_name,$dataTableHeaders);
                    break;*/
                }
            }
        }

	private function adjustAction(&$records){
		if(isset($this->primaryData['summarize']) && $this->primaryData['summarize']==1){
			switch($this->primaryData['action']){
				case 1:
					foreach($records as $key => $record){
						$records[$key][__('suma')] = array_sum($record);
					}
				break;
				case 2:
					foreach($records as $key => $record){
						$records[$key][''.__('vidurkis')] = round(array_sum($record) / sizeof($record),3);
					}
				break;
			}
			//pasaliname nereikalingus irasus jei parinktas "Visos pamainos" bet ne pati pamaina konkreciai ir tik tada, kai rakursas yra pamaina
			if(in_array('-1',$this->primaryData['shifts']) && $this->primaryData['action'] > 0 && sizeof($this->primaryData['shifts']) >= 2){
				foreach($records[0] as $shift_id => $record){
					if(!in_array($shift_id,$this->primaryData['shifts']) && is_numeric($shift_id)) unset($records[0][$shift_id]);
				}
			}
		}

	}

    public static function getRecordDuration()
    {
        return self::RecordDuration;
    }

	public function validateData($data){
		$errors = array();
//        fb($data,'datatata');
		if($data['indicator1'] == null){
			$errors['NodeIndicator1'] = __('Pasirinkite rodiklį y1');
		}
		if($data['graph_type'] == null){
			$errors['NodeGraphType'] = __('Būtina pasirinkti grafiko tipą');
		}
		if(empty($data['sensors'])){
			$errors['NodeSensors'] = __('Parinkite bent vieną darbo centrą');
		}
		if(empty($data['shifts'])){
			$errors['NodeShifts'] = __('Parinkite bent vieną pamainą');
		}
		if($data['date_start'] == null){
			$errors['field_start'] = __('Nurodykite laiko intervalo pradžios tašką "Nuo"');
		}
		if($data['date_end'] == null){
			$errors['field_end'] = __('Nurodykite laiko intervalo pabaigos tašką "Iki"');
		}
		if($data['indicator1'] > 0 &&  isset($data['indicator2']) && $data['indicator2'] > 0 && $data['graph_type'] == 0){
			$errors['graph_type'] = __('Jei parinkti abu rodikliai, grafiko tipas privalo būti stulpelinis');
		}
        //if($data['indicator1'] == 18 && sizeof($data['sensors'])>1){
            //$errors['only_one_shift'] = __('Darbo laikų grafike galima pasirinkti tik vieną darbo centrą');
        //}
        if($data['indicator1'] == 18 && is_array($data['shifts']) && sizeof($data['shifts'])>1){
            $errors['only_one_shift'] = __('Darbo laikų grafike galima pasirinkti tik vieną pamainą');
        }
        if($data['indicator1'] == 18 && $data['graph_type'] == 2 && $data['time_group'] != 0){
            $errors['only_one_shift'] = __('Darbo laikų grafike skritulinis tipas galimas tik su laiko grupavimu "Suminis"');
        }
        if($data['time_pattern'] == 4 && $data['time_group'] == 1){
            $errors['to_many_data'] = __('Praėjusių metų laikotarpio negalima derinti su pamaininiu grupavimu dėl per didelio duomenų kiekio');
        }
        
		return $errors;
	}

    private function reconstructData(){
        if(isset($this->primaryData['downtime_level']) && strlen($this->primaryData['downtime_level']) > 0){
            $problemModel = ClassRegistry::init('Problem');
            $keyLegend = is_array($this->axis['columns'])?$this->axis['columns'][1]:'xLabel';
            foreach($this->dataStructure as $key => $dataStructure){
                $problemId = $dataStructure['Problem']['id'];
                if(!isset($this->problemsTreeList[$problemId])){ continue; }
                if(isset($this->primaryData['downtimes']) && !empty($this->primaryData['downtimes'])){
                	$downtimeParentInNeededLevel = array_intersect_key($this->problemsTreeList[$problemId], array_flip($this->primaryData['downtimes']));
                	while (key($this->problemsTreeList[$problemId]) !== key($downtimeParentInNeededLevel)){
					    next($this->problemsTreeList[$problemId]);
                    }
                    reset($this->problemsTreeList[$problemId]);
					if(!prev($this->problemsTreeList[$problemId])){ reset($this->problemsTreeList[$problemId]); }
                    $downtimeParentInNeededLevel = array_intersect_key($this->problemsTreeList[$problemId], array_flip(array(key($this->problemsTreeList[$problemId]))));
					$parentHasDataInSameDate = array();
				}else{
					if($dataStructure['Problem']['level'] == $this->primaryData['downtime_level']){ continue; }
					$downtimeParentInNeededLevel = array_slice($this->problemsTreeList[$problemId], '-'.($this->primaryData['downtime_level']+1),1,true);
					if($this->primaryData['time_group'] == 0){
	                    $parentHasDataInSameDate = array_filter($this->dataStructure, function($data)use($downtimeParentInNeededLevel, $dataStructure){
	                        return $data['Problem']['id'] == key($downtimeParentInNeededLevel);
	                    });
	                }else{
	                    $parentHasDataInSameDate = array_filter($this->dataStructure, function($data)use($downtimeParentInNeededLevel, $dataStructure){
	                        return $dataStructure[0]['xLabel'] == $data[0]['xLabel'] && $data['Problem']['id'] == key($downtimeParentInNeededLevel);
	                    });
	                }
				}
    //fb($downtimeParentInNeededLevel,$problemId.': '.$dataStructure[0]['sum']);           
                if(!empty($parentHasDataInSameDate)){
                    $this->dataStructure[key($parentHasDataInSameDate)][0]['sum'] = bcadd($this->dataStructure[key($parentHasDataInSameDate)][0]['sum'], $dataStructure[0]['sum'],6);
                    unset($this->dataStructure[$key]);
                }else{
                    //fb($downtimeParentInNeededLevel);
                    $this->dataStructure[$key][0][$keyLegend] = current($downtimeParentInNeededLevel);
                    $this->dataStructure[$key]['Problem']['id'] = key($downtimeParentInNeededLevel);
                    $this->dataStructure[$key]['Problem']['level'] = $this->primaryData['downtime_level'];
                }
            }
        }
        switch($this->primaryData[$this->indicator]){
            case 28:
                $this->dataStructure = array_map(function($data){ $data[0]['ps_id'] = __('Faktinis derinimo laikas'); return $data; }, $this->dataStructure);
                foreach($this->dataStructure as $data){
                    $data[0]['sum'] = $data[0]['theorical_transition'];
                    $data[0]['ps_id'] = __('Teorinis derinimo laikas');
                    $this->dataStructure[] = $data;
                }
            break; 
            case 29:
                $this->dataStructure = array_map(function($data){ $data[0]['ps_id'] = __('Faktinis derinimo laikas'); return $data; }, $this->dataStructure);
                foreach($this->dataStructure as &$data){
                    $data[0]['sum'] = abs($data[0]['theorical_transition'] - $data[0]['sum']);
                    $data[0]['ps_id'] = __('Derinimo laikų nuokrypis');
                }
            break; 
        }
    }

    public function setIndicators() {
        $indicators = array();
        if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
            $indicators[23] = __('OEE');
            $indicators[24] = __('Prieinamumo koef. su išimtimis');
        }
        $indicators[1] = __('Prieinamumo koef. be išimčių');
        $indicators[2] = __('Veiklos efektyvumo koeficientas');
        $indicators[0] = __('OEE be išimčių');
        $indicators[4] = __('Gamybos laikas');
        $indicators[5] = __('Derinimų laikas');
        $indicators[6] = __('Prastovų laikas');
        $indicators[7] = __('Nėra darbo laikas');
        $indicators[9] = __('Pagamintas kiekis');
        //$indicators[10] = __('Kiekis linijos viduryje (vnt)');
        //$indicators[11] = __('Kiekis linijos pradžioje (vnt)');
        $indicators[18] = __('Darbo laikai');
        $indicators[19] = __('Prastovų priežastys (tik tėvinės)');
        $indicators[26] = __('Prastovų priežastys');
        $indicators[22] = __('Prastovų kiekis (tik tėvinės)');
        $indicators[27] = __('Prastovų kiekis');
        $indicators[25] = __('Viršytų derinimų laikas');
        $indicators[28] = __('Derinimų laikų palyginimas');
        $indicators[29] = __('Derinimų laikų nuokrypis');
        return $indicators;
//        $indicators = array(
//            3 => __('Kokybės koefici__('Gamybos laikas'),
        //8  => __('Gaminamas kitas produktas'),
//            12 => __('Praradimai dėl kepimo'),
//            13 => __('Praradimai dėl raikymo'),
        //14 => __('NK deklaruotas brokas'),
        //15 => __('Deklaruotas brokas'),
        //16 => __('NK Deklaruotas brokas pagal priežastis'),
        //17 => __('Deklaruotas brokas pagal priežastis'),
        //20 => __('MTTR'),
        // 21 => __('MTTF'),
//        );
    }

}