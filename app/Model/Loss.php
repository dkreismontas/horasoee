<?php

App::uses('AppModel', 'Model');

/**
 */
class Loss extends AppModel {
	const NAME = __CLASS__;
	
	/**
	 * Create and save new item to database
	 * @param int $approvedOrderId
	 * @param int $lossTypeId
	 * @param int $value
	 * @param int $shiftId
	 * @return array new record
	 */
	public function newItem($approvedOrderId, $lossTypeId, $value, $shiftId = null,$while_confirming = 0, $user = array()) {
		if(!$value){ return null; }
		$this->create();
		if (!$approvedOrderId || !intval($approvedOrderId)) throw new ErrorException("Bad approved order ID value '".$approvedOrderId."'.");
		if (!$lossTypeId || !intval($lossTypeId)) throw new ErrorException("Bad loss type ID value '".$lossTypeId."'.");
		if ($shiftId && !intval($shiftId)) throw new ErrorException("Bad shift ID value '".$shiftId."'.");
		if ($this->save($arr = array(
			'approved_order_id' => intval($approvedOrderId),
			'loss_type_id' => intval($lossTypeId),
			'value' => $value,
			'shift_id' => ($shiftId ? intval($shiftId) : null),
            'while_confirming'=>$while_confirming,
            'user_id'=>$user['id']
		))) {
			$arr['id'] = intval($this->id);
			return array(self::NAME => $arr);
		}
		return null;
	}
	
	/** @return Loss */
	public function withRefs() {
		$this->bindModel(array(
			'belongsTo' => array(
				LossType::NAME => array(
					'className' => LossType::NAME,
					'foreignKey' => 'loss_type_id'
				),'User'
			)
		));
		return $this;
	}

    public function getByMO($mo_number) {
        $this->bindModel(array(
            'belongsTo'=>array(
                'ApprovedOrder' => array(),
                'Plan' => array(
                    'className' => 'Plan',
                    'foreignKey' => false,
                    'conditions' => array('ApprovedOrder.plan_id = Plan.id')
                ),
                'LossType'
            )
        ));
        $losses = $this->find('all',array(
            'fields' => array(
                'Loss.loss_type_id',
                'Loss.value',
                'Loss.approved_order_id',
                'ApprovedOrder.sensor_id',
                'Plan.mo_number',
                'LossType.name'

            ),
            'conditions' => array(
                'Plan.mo_number' => $mo_number
            )
        ));

        return $losses;
    }

    public function getLossesQuantitiesByOrder($approvedOrdersIdsSamePlan){
        $this->virtualFields = array('sum'=>'SUM(Loss.value)');
        return $this->find('list',array(
            'fields' => array('Loss.loss_type_id', 'sum'),
            'conditions' => array(
                'Loss.loss_type_id >'=>0,
                'Loss.approved_order_id' => $approvedOrdersIdsSamePlan
            ),'group'=>array('Loss.loss_type_id')
        ));
    }

    public function getTypesFormattedEmpty() {
        $types = $this->LossType->find('all',array(
            'fields' => array(
                'id',
                'name'
            ),
            'order' => array('id')
        ));

        $typesFormatted = array();
        array_walk($types,function(&$val,$key) use (&$typesFormatted) {
            $loss_type_id = $val['LossType']['id'];
            $typesFormatted[$loss_type_id] = array(
                'loss_value'=>0,
                'loss_name'=>$val['LossType']['name']
            );
        });

        return $typesFormatted;
    }
	
}
