<?php

App::uses('AppModel', 'Model');
App::uses('Plan', 'Model');
App::uses('Sensor', 'Model');
App::uses('Shift', 'Model');
App::uses('ApprovedOrder', 'Model');
App::uses('FoundProblem', 'Model');

/**
 */
class Record extends AppModel {

    const NAME = __CLASS__;
    CONST RECORD_AGE = 120; //irasu senumas sekundemis greiciu grafikui
    public $user=array();
    public $useTable = 'records';

    public $archiveDuration = '2 WEEK';

    public $belongsTo = array('Sensor');

    /**
     *
     * @param array $sensor
     * @param DateTime $from
     * @param DateTime $to
     * @param int $approvedOrderId
     * @param array $plan
     */
    public function findAndSetOrder($sensor, DateTime $from, DateTime $to, $approvedOrderId, $plan) {
// fb($from->format('Y-m-d H:i:s'));
// fb($plan[Plan::NAME]['id']);
// fb($approvedOrderId);
        if (!$sensor) throw new ErrorException('Sensor is required.');
        if (!$plan) throw new ErrorException('Plan is required.');
        if (!$from || !$to) throw new ErrorException('DateTime interval required.');

        $multiplier = floatval($plan[Plan::NAME]['box_quantity']);
        $fields = array(
            'Record.unit_quantity'=>'`quantity` * '.$multiplier,
            'Record.plan_id'=>$plan[Plan::NAME]['id'],
            'Record.approved_order_id'=>$approvedOrderId,
            'Record.user_id'=>$this->user->id,
        );
        $conditions =  array(
            'sensor_id'=>$sensor[Sensor::NAME]['id'],
            'OR'=>array('approved_order_id <>'=>$approvedOrderId, 'approved_order_id IS NULL'),
            'created >='=>$from->format('Y-m-d H:i:s'),
            'created <='=>$to->format('Y-m-d H:i:s')
        );
        $parameters = array($sensor, $approvedOrderId, $plan, &$multiplier, &$fields, &$conditions);
        if($this->callPluginFunction('Record_FindAndSetOrder_Hook', $parameters, Configure::read('companyTitle')) === false) return;
        $this->updateAll($fields, $conditions, false);
    }

    /**
     * @param int $approvedOrderId
     * @return int
     */
    public function findAndSumOrder($approvedOrderId) {
        if (!$approvedOrderId) throw new ErrorException('Approved Order ID is required.');
        $fItems = $this->find('all', array(
            'fields'     => array('SUM(' . self::NAME . '.unit_quantity) AS total'),
            'conditions' => array(
                self::NAME . '.approved_order_id' => $approvedOrderId,
                self::NAME . '.unit_quantity >' => 0,
            )
        ));

        return (isset($fItems[0][0]['total'])) ? intval($fItems[0][0]['total']) : 0;
    }

    public function findAndSumQuantity($approvedOrderId) {
        if (!$approvedOrderId) throw new ErrorException('Approved Order ID is required.');
        $fItems = $this->find('all', array(
            'fields'     => array('SUM(' . self::NAME . '.quantity) AS total'),
            'conditions' => array(
                self::NAME . '.approved_order_id' => $approvedOrderId,
                self::NAME . '.quantity >' => 0,
            )
        ));
        return (isset($fItems[0][0]['total'])) ? intval($fItems[0][0]['total']) : 0;
    }


    /**
     * Get record quantity sum from interval
     * @param int $sensorId
     * @param DateTime $from
     * @param DateTime $to
     * @return int
     */
    public function findAndSumInterval($sensorId, DateTime $from, DateTime $to) {
        if (!$sensorId) throw new ErrorException('Sensor ID is required.');
        if (!$from || !$to) throw new ErrorException('DateTime interval required.');
        $fItems = $this->find('all', array(
            'fields'     => array('SUM(' . self::NAME . '.unit_quantity) AS total'),
            'conditions' => array(
                self::NAME . '.sensor_id'  => $sensorId,
                self::NAME . '.created >=' => $from->format('Y-m-d H:i:s'),
                self::NAME . '.created <=' => $to->format('Y-m-d H:i:s'),
                self::NAME . '.unit_quantity >' => 0,
            )
        ));

        return (isset($fItems[0][0]['total'])) ? intval($fItems[0][0]['total']) : 0;
    }

    /**
     *
     * @param int $sensorId
     * @param DateTime $from
     * @param DateTime $to
     * @param int $foundProblemId
     */
    public function findAndSetProblem($sensorId, DateTime $from, DateTime $to, $foundProblemId) {
        if (!$sensorId) throw new ErrorException('Sensor ID is required.');
        if (!$from || !$to) throw new ErrorException('DateTime interval required.');
        $this->query(
            "UPDATE `$this->table` `r`" .
            " SET `r`.`found_problem_id`=:found_problem_id, `user_id` = :user_id" .
            " WHERE `r`.`sensor_id`=:sensor_id" .
            " AND (`r`.`found_problem_id`!=:found_problem_id OR IFNULL(`r`.`found_problem_id`, 0) = 0)" .
            //" AND (`r`.`quantity` = 0)" .
            " AND `r`.`created`>:from" .
            " AND `r`.`created`<=:to", 
            array(
                ':sensor_id'        => $sensorId,
                ':found_problem_id' => $foundProblemId,
                ':from'             => $from->format('Y-m-d H:i:s'),
                ':to'               => $to->format('Y-m-d H:i:s'),
                ':user_id'          => $this->user->id
            ),
            false
        );
       
        /*$other_sensors = $this->Sensor->getRelatedSensors($sensorId, null, true);
        $ids_str = implode(",", $other_sensors);
        if (!empty($ids_str)) {
            $this->query(
                "UPDATE `$this->table` `r`" .
                " SET " .
                "`r`.`found_problem_id`=:default_problem_id, `user_id` = :user_id" .
                " WHERE `r`.`sensor_id` IN (" . $ids_str . ")  AND `r`.`quantity` = 0 AND `r`.`plan_id` IS NULL AND `r`.created >= '" . $from->format('Y-m-d H:i:s') . "' AND `r`.created  <= '" . $to->format('Y-m-d H:i:s') . "'",
                array(
                    ':default_problem_id' => 0,
                    ':user_id' => $this->user->id
//                            ':records' => $ids_str
                ),
                false
            );
        }*/
    }

    public function addProblemOnQuantityRecords($record, $fShift){
        $this->virtualFields = array('max'=>'MAX(created)');
        $maxTimes = $this->find('list', array(
            'fields'=>array('found_problem_id', 'max'), 
            'conditions'=>array(
                'Record.created >=' => $fShift['Shift']['start'],
                'Record.created <' => $record['Record']['created'],
                'Record.sensor_id' => $record['Record']['sensor_id'],
                'Record.plan_id' => NULL,
            ),'group'=>array('Record.found_problem_id'), 'order'=>array('max')
        ));
        $this->virtualFields = array();
        $maxTimes = array_diff_key($maxTimes, array(''=>''));
        $caseThen = '';
        foreach($maxTimes as $problemId => $time){
            $caseThen .= ' WHEN Record.created < \''.$time.'\' THEN '.$problemId;
        }
        if(!trim($caseThen)){
            return false;
        }
        $this->updateAll(array('Record.found_problem_id'=>'CASE '.$caseThen.' END'), array(
            'Record.sensor_id' => $record['Record']['sensor_id'],
            'Record.created >=' => $fShift['Shift']['start'],
            'Record.created <' => $record['Record']['created'],
            'Record.quantity >' => 0,
            'Record.found_problem_id' => NULL,
            'Record.plan_id' => NULL,
        ));
    }

    /**
     * @param boolean $reset
     * @param boolean $more
     * @return Record
     */
    public function withRefs($reset = true, $more = false) {
        $refs = array(
            'belongsTo' => array(
                Plan::NAME          => array(
                    'className'  => Plan::NAME,
                    'foreignKey' => 'plan_id'
                ),
                Sensor::NAME        => array(
                    'className'  => Sensor::NAME,
                    'foreignKey' => 'sensor_id'
                ),
                Shift::NAME         => array(
                    'className'  => Shift::NAME,
                    'foreignKey' => 'shift_id'
                ),
                ApprovedOrder::NAME => array(
                    'className'  => ApprovedOrder::NAME,
                    'foreignKey' => 'approved_order_id'
                ),
                FoundProblem::NAME  => array(
                    'className'  => FoundProblem::NAME,
                    'foreignKey' => 'found_problem_id'
                ),
                Problem::NAME       => array(
                    'className'  => Problem::NAME,
                    'foreignKey' => '',
                    'conditions' => array(
                        FoundProblem::NAME . '.problem_id = ' . Problem::NAME . '.id'
                    ),
                    'dependent'  => false
                ),
                'Branch' => array(
					'className' => 'Branch',
					'foreignKey' => false,
					'conditions' => array(
						Sensor::NAME.'.branch_id = Branch.id'
					),
					'dependent' => false
				),
				'Factory'=>array(
                    'foreignKey'=>false,
                    'conditions'=>array('Branch.factory_id = Factory.id')
                )
            )
        );
        if ($more) {
            $refs['belongsTo'][Branch::NAME] = array(
                'className'  => Branch::NAME,
                'foreignKey' => '',
                'conditions' => array(
                    Sensor::NAME . '.branch_id = ' . Branch::NAME . '.id'
                ),
                'dependent'  => false
            );
        }
        $this->bindModel($refs, $reset);

        return $this;
    }

    /** @return Record */
    public function withApprovedOrders() {
        $this->bindModel(array(
            'hasOne' => array(
                ApprovedOrder::NAME => array(
                    'className'  => ApprovedOrder::NAME,
                    'foreignKey' => '',
                    'conditions' => array(
                        self::NAME . '.sensor_id = ' . ApprovedOrder::NAME . '.sensor_id',
                        self::NAME . '.created >= ' . ApprovedOrder::NAME . '.created',
                        self::NAME . '.created <= ' . ApprovedOrder::NAME . '.end'
                    ),
                    'dependent'  => false
                ),
                FoundProblem::NAME  => array(
                    'className'  => FoundProblem::NAME,
                    'foreignKey' => '',
                    'conditions' => array(
                        self::NAME . '.sensor_id = ' . FoundProblem::NAME . '.sensor_id',
                        self::NAME . '.created >= ' . FoundProblem::NAME . '.start',
                        self::NAME . '.created <= ' . FoundProblem::NAME . '.end'
                    ),
                    'dependent'  => false
                )
            )
        ));

        return $this;
    }

    public function getByMO($mo_number, $recordDuration = 1) {
        $this->bindModel(
            array(
                'belongsTo' => array('Plan', 'Sensor', 'ApprovedOrder')
            )
        );
        $records = $this->find('all', array(
            'fields'     => array(
                'SUM(Record.unit_quantity) as sum_quantity',
                'SUM(IF(Sensor.type=1,Record.unit_quantity,0)) as sum_pries_kepima',
                'SUM(IF(Sensor.type=2,Record.unit_quantity,0)) as sum_pries_raikyma',
                'SUM(IF(Sensor.type=3,Record.unit_quantity,0)) as sum_supakuotas',
                'SUM(IF(Record.found_problem_id IS NULL,' . $recordDuration . ',0)) as sum_gamybos_laikas',
                'MIN(Record.created) as jutiklio_gamybos_pradzia',
                'MAX(Record.created) as jutiklio_gamybos_pabaiga',
                'Plan.*',
                'Sensor.id',
                'Sensor.type',
                'Sensor.ps_id',
                'Record.approved_order_id',
                'ApprovedOrder.additional_data'
            ),
            'conditions' => array(
                'Plan.mo_number' => $mo_number
            ),
            'group'      => array(
                'Sensor.id',
                'Plan.mo_number',
                'Record.approved_order_id'
            ),
            'order'      => array(
                'ApprovedOrder.created',
                'Plan.mo_number',
                'Sensor.id'
            )
        ));
        $this->bindModel(
            array(
                'belongsTo' => array('Plan', 'Sensor')
            )
        );
        $max_created = $this->find('all', array(
            'fields'     => array(
                'MAX(Record.created) as jutiklio_gamybos_pabaiga',

                'Plan.mo_number',
                'Sensor.id',
            ),
            'conditions' => array(
                'Plan.mo_number' => $mo_number,
                'Record.quantity > 0'
            ),
            'group'      => array(
                'Sensor.id',
                'Plan.mo_number',
                'Record.approved_order_id'
            ),
            'order'      => array(
                'Plan.mo_number',
                'Sensor.id'
            )
        ));

//        fb($max_created);

        foreach ($records as &$rec) {
            $plan_id = $rec['Plan']['mo_number'];
            $sensor_id = $rec['Sensor']['id'];
            foreach ($max_created as $max_rec) {
                if ($max_rec['Plan']['mo_number'] == $plan_id && $max_rec['Sensor']['id'] == $sensor_id) {
                    $rec[0]['jutiklio_gamybos_pabaiga'] = $max_rec[0]['jutiklio_gamybos_pabaiga'];
                }
            }
        }


        return $records;
    }

    public function getApprovedOrderTimes($shift, $sensor_id, $archive_table = false) {
        $orderCalcModel = ClassRegistry::init('OrderCalculation');
		$orderCalcModel->bindModel(array(
            'belongsTo' => array(
            	'ApprovedOrder', 'Sensor',
            	'Plan'=>array('foreignKey'=>false, 'conditions'=>array('Plan.id = ApprovedOrder.plan_id')), 
                'Branch'=>array('foreignKey'=>false, 'conditions'=>array('Branch.id = Sensor.branch_id')),
                'Factory'=>array('foreignKey'=>false, 'conditions'=>array('Factory.id = Branch.factory_id')),
            )
        ));
		$records_grouped = $orderCalcModel->find('all', array(
			'fields'     => array(
				'Sensor.id','Sensor.name','Factory.name','Sensor.type','ApprovedOrder.*','Plan.*',
				'SUM(OrderCalculation.quantity) AS sum_quantity',
				'TIMESTAMPDIFF(SECOND,Plan.start, Plan.end) as plan_time_all',
				'(TIMESTAMPDIFF(SECOND,
                    CASE WHEN Plan.start > \'' . $shift['Shift']['start'] . '\' THEN Plan.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN Plan.end < \'' . $shift['Shift']['end'] . '\' THEN Plan.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as plan_time',
                'TRIM(OrderCalculation.time_full) AS fact_time',
                'TRIM(OrderCalculation.time_green) AS fact_time_work',
			),'conditions' => array('OrderCalculation.shift_id' => $shift['Shift']['id'], 'OrderCalculation.sensor_id' => $sensor_id),
			'order'=>array('OrderCalculation.sensor_id', 'ApprovedOrder.created'),
            'group'      => array('OrderCalculation.approved_order_id')
		));
		return $records_grouped;
        /*$this->bindModel(array(
            'belongsTo' => array('ApprovedOrder', 'Plan', 
                'Branch'=>array('foreignKey'=>false, 'conditions'=>array('Branch.id = Sensor.branch_id')),
                'Factory'=>array('foreignKey'=>false, 'conditions'=>array('Factory.id = Branch.factory_id')),
            )
        ));
        if ($archive_table == true) {
            $this->setSource('records_archive');
        }
        $records_grouped = $this->find('all', array(
            'fields'     => array(
                'Sensor.id','Sensor.name','Factory.name','Sensor.type','ApprovedOrder.*',
                //'SUM(Record.unit_quantity) AS sum_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, IF(Sensor.plan_relate_quantity=\'quantity\',Record.quantity, Record.unit_quantity),0)) AS sum_quantity', //pakeista del to, kad pamainineje atasakitoje sutaptu duomenys tarp benro sheet ir pamainu
                '(TIMESTAMPDIFF(SECOND,Plan.start, Plan.end)) as plan_time_all',
                '(TIMESTAMPDIFF(SECOND,
                    CASE WHEN Plan.start > \'' . $shift['Shift']['start'] . '\' THEN Plan.start ELSE \'' . $shift['Shift']['start'] . '\' END,
                    CASE WHEN Plan.end < \'' . $shift['Shift']['end'] . '\' THEN Plan.end ELSE \'' . $shift['Shift']['end'] . '\' END)) as plan_time',
                '(COUNT(*) * ' . (Configure::read('recordsCycle')) . ') as fact_time',
                'SUM(IF(Record.found_problem_id IS NULL,20,0)) as fact_time_work',
                'Plan.id', 'Plan.paste_code', 'Plan.production_name', 'Plan.production_code', 'Plan.mo_number', 'Plan.start', 'Plan.end', 'Plan.step', 'Plan.box_quantity', 'Plan.quantity', 'Plan.client_speed','Plan.step'
            ),
            'conditions' => array(
                'Record.approved_order_id IS NOT NULL',
                'Record.shift_id'  => $shift['Shift']['id'],
                'Record.sensor_id' => $sensor_id
            ),
            'group'      => array('Record.sensor_id', 'Record.approved_order_id'),
            'order'      => array('Record.approved_order_id')
        ));

        if ($archive_table == true) {
            $this->setSource('records');
        } else {
            $records_grouped = array_merge($records_grouped, $this->getApprovedOrderTimes($shift, $sensor_id, true));
        }
        return $records_grouped;*/
    }

    public function getQuantities($shift, $sensor_id, $archive_table = false) {
        $orderCalcModel = ClassRegistry::init('OrderCalculation');
		$records_grouped = $orderCalcModel->find('all', array(
			'fields'     => array('TRIM(OrderCalculation.approved_order_id) AS approved_order_id, SUM(OrderCalculation.quantity) as quantity'),
            'conditions' => array('OrderCalculation.shift_id' => $shift['Shift']['id'], 'OrderCalculation.sensor_id' => $sensor_id),
            'group'      => array('OrderCalculation.approved_order_id')
		));
        /*if ($archive_table == true) {
            $this->setSource('records_archive');
        }
        $records_grouped = $this->find('all', array(
            'fields'     => array('Record.approved_order_id,SUM(Record.unit_quantity) as quantity'),
            'conditions' => array('Record.shift_id' => $shift['Shift']['id'], 'Record.sensor_id' => $sensor_id, 'Record.approved_order_id IS NOT NULL'),
            'group'      => array('Record.approved_order_id')
        ));
        if ($archive_table == true) {
            $this->setSource('records');
        } else {
            $records_grouped = array_merge($records_grouped, $this->getQuantities($shift, $sensor_id, true));
        }*/
        return $records_grouped;
    }
    
    public function getUser($shift,$sensorId){
        return $this->find('first', array(
        'fields'=>array(
            'COUNT(Record.user_id) AS record_count',
            'TRIM(Record.user_id) AS user_id'
        ),'conditions'=>array(
            'Record.sensor_id' => $sensorId,
            'Record.shift_id' => $shift['Shift']['id'],
            'Record.user_id > '=>1
        ),'group'=>array('Record.user_id'),
        'order'=>array('record_count DESC')
        ));
    }
    
    /*public function getLastMinuteSpeed($fSensor){
        $time = time();
        $quantity = isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity';
        if($fSensor['Sensor']['speedometer_type'] == 0) {//paskutine minute
            $params = array(
                'fields' => array(
                    'SUM(IF(Record.created >= \'' . date('Y-m-d H:i:s', $time - self::RECORD_AGE - 59) . '\',' . $quantity . ',0)) AS quantities',
                ), 'recursive' => -1,
                'conditions' => array(
                    'Record.created <=' => date('Y-m-d H:i:s', $time - self::RECORD_AGE),
                    'Record.created >' => date('Y-m-d H:i:s', $time - 300),
                    'Record.sensor_id' => $fSensor['Sensor']['id'],
                    'Record.found_problem_id IS NULL'
                )
            );
            $speed = $this->find('first', $params);
        }elseif($fSensor['Sensor']['speedometer_type'] == 2){//paskutine valanda
            $params = array(
                'fields'=>array(
                    'SUM('.$quantity.') AS quantities',
                ),'recursive'=>-1,
                'conditions'=>array(
                    'Record.created >='=>date('Y-m-d H:i:s', $time-3600-Configure::read('recordsCycle')),
                    'Record.created <'=>date('Y-m-d H:i:s', $time-Configure::read('recordsCycle')),
                    'Record.sensor_id'=>$fSensor['Sensor']['id'],
                    'Record.found_problem_id IS NULL'
                )
            );
            $speed = $this->find('first', $params);
        }else{//visa paskutine pamaina
            $fShift = ClassRegistry::init('Shift')->findCurrent($fSensor[Sensor::NAME]['branch_id']);
            if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
                $exclusionList = $plugin_model::getExclusionIdList();
                $shiftDuration = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . join(',', $exclusionList) . '),0,' . Configure::read('recordsCycle') . ')) AS shift_duration';
            }else{
                $shiftDuration = 'SUM('.Configure::read('recordsCycle').') AS shift_duration';
            }
            $this->bindModel(array('belongsTo'=>array('FoundProblem')));
            $params = array(
                'fields'=>array(
                    'SUM(Record.'.$quantity.') AS quantities',
                    $shiftDuration
                ),'recursive'=>1,
                'conditions'=>array(
                    'Record.shift_id'=>$fShift['Shift']['id'],
                    'Record.sensor_id'=>$fSensor['Sensor']['id'],
                )
            );
            $speed = $this->find('first', $params);
            $speed[0]['quantities'] = $speed[0]['shift_duration'] > 0?bcdiv($speed[0]['quantities'], bcdiv($speed[0]['shift_duration'],3600,6),2):0;
        }
        return isset($speed[0]['quantities'])?$speed[0]['quantities']:0;
    }*/

    public function getLastMinuteSpeed($fSensor, $currentPlan, $fShift){
        $time = time();
        $quantity = 'Record.'.(isset($fSensor['Sensor']['plan_relate_quantity'])?$fSensor['Sensor']['plan_relate_quantity']:'quantity');
        $conditions = array('Record.sensor_id' => $fSensor['Sensor']['id']);
        $durationField = 'SUM('.Configure::read('recordsCycle').') AS duration';
        switch ($fSensor['Sensor']['speedometer_type']) {
            case 2: //paskutine valanda
                $duration = 3600;
                break;
            case 0: //paskutine minute
                $time -= Configure::read('recordsCycle');
                $duration = 60;
                break;
            case 1: //paskutine pamaina
                if (in_array('ShiftTimeProblemExclusions', CakePlugin::loaded())) {
                    $plugin_model = ClassRegistry::init('ShiftTimeProblemExclusions.ShiftTimeProblemExclusionsAppModel');
                    $exclusionList = $plugin_model::getExclusionIdList();
                    $durationField = 'SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . join(',', $exclusionList) . '),0,' . Configure::read('recordsCycle') . ')) AS duration';
                }
                $duration = $time - strtotime($fShift['Shift']['start'])+3600;
                $conditions['Record.shift_id'] = $fShift['Shift']['id'];
        }
        $params = array(
            'fields' => array(
                'SUM('.$quantity.') AS quantities',
                $durationField
            ), 'recursive' => 1,
            'conditions' => array_merge($conditions, array(
                'Record.created >=' => date('Y-m-d H:i:s', $time - $duration - Configure::read('recordsCycle')),
                'Record.created <' => date('Y-m-d H:i:s', $time - Configure::read('recordsCycle')),
            ))
        );
        $this->bindModel(array('belongsTo'=>array('FoundProblem')));
        $speed = $this->find('first', $params);
        if($fSensor['Sensor']['speedometer_type'] == 1){
            $speed[0]['quantities'] = $speed[0]['duration'] > 0?bcdiv($speed[0]['quantities'], bcdiv($speed[0]['duration'],3600,6),2):0;
        }
        return isset($speed[0]['quantities'])?$speed[0]['quantities']:0;
    }

    public function calculateMainFactorsInHour(&$dcHoursInfoBlocks, $hours, $fSensor){
         $settingsModel = ClassRegistry::init('Settings');
         if($fSensor['Sensor']['show_oee_or_other_in_wc'] == 0){
            $dcHoursInfoBlocks['oee_factor'] = array('title'=>__('OEE'), 'addSpace'=>40,'value'=>array());
         }else{
            $dcHoursInfoBlocks['exploitation_factor'] = array('title'=>__('Avail'), 'addSpace'=>50,'value'=>array());
         } 
        //Darbo centre apskaiciuojamas valandinis OEE
         //$recordModel = ClassRegistry::init('Record');
         $problemsExclusionModel = ClassRegistry::init('ShiftTimeProblemExclusion');
         $lossModel = ClassRegistry::init('Loss');
         $startDate = current($hours)['date'].' '.current($hours)['value'].':00';
         $endDate = date('Y-m-d H:i', strtotime($startDate) + 3600 * (sizeof($hours)));
         $exclusionList = $problemsExclusionModel->find('list', array('fields'=>array('problem_id')));
         $exclusionList = empty($exclusionList)?array(0):array_filter($exclusionList);
         $this->bindModel(array('belongsTo'=>array('FoundProblem','Plan')));
         $exceededTransitionId = $settingsModel->getOne('exceeded_transition_id');
         $exceededTransitionId = $exceededTransitionId > 0?$exceededTransitionId:-1000;
         $lossModel->bindModel(array('belongsTo'=>array('ApprovedOrder')));
         $lossInHours = Hash::combine($lossModel->find('all', array(
            'fields'=>array(
                'SUM(Loss.value) AS losses_count',
                'DATE_FORMAT(Loss.created_at, \'%H\') AS hour'
            ),'conditions'=>array(
                'ApprovedOrder.sensor_id'=>$fSensor['Sensor']['id'],
                'Loss.created_at >=' => $startDate,
                'Loss.created_at <=' => $endDate
            ),'group'=>array(
                'DATE_FORMAT(Loss.created_at, \'%H\')',
            )
         )),'{n}.0.hour','{n}.0.losses_count');
         $times = $this->find('all', array(
            'fields'=>array(
                //'TIMESTAMPDIFF(SECOND,MIN(Record.created),MAX(Record.created)) + '.Configure::read('recordsCycle').' - SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . implode(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS available_duration',
                'SUM('.Configure::read('recordsCycle').') - SUM(IF(Record.found_problem_id IS NOT NULL AND FoundProblem.problem_id IN (' . implode(',', $exclusionList) . '),' . (Configure::read('recordsCycle')) . ',0)) AS available_duration',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL,' . (Configure::read('recordsCycle')) . ',0)) AS fact_time',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, Record.quantity, 0)) as total_quantity',
                'SUM(IF(Record.plan_id IS NOT NULL AND Record.found_problem_id IS NULL, Record.unit_quantity, 0)) as total_unit_quantity',
                //'SUM(IF(Record.found_problem_id IS NOT NULL AND (FoundProblem.problem_id=1 OR FoundProblem.problem_id='.$exceededTransitionId.'),' . (Configure::read('recordsCycle')) . ',0)) as transition_time',  Isimtas del histeel 2019-08-30 ale turi mazinti OEE virsijimai
                'SUM(IF(Record.found_problem_id IS NOT NULL AND (FoundProblem.problem_id=1),' . (Configure::read('recordsCycle')) . ',0)) as transition_time',
                'DATE_FORMAT(Record.created, \'%H\') AS hour',
                'TRIM(Record.approved_order_id) AS approved_order_id',
                'TRIM(Plan.step) AS step'
            ),'conditions'=>array(
                'Record.created >=' => $startDate,
                'Record.created <=' => $endDate,
                'Record.sensor_id' => $fSensor['Sensor']['id'],
            ),'group'=>array(
                'DATE_FORMAT(Record.created, \'%H\')',
                'Record.approved_order_id',
            )
         ));

         $dataInHours = array();
         foreach($times as $time){
             $hourNr = $time[0]['hour']; 
             if(!isset($dataInHours[$hourNr]['shift_length'])){ $dataInHours[$hourNr]['shift_length'] = 0; }
             if(!isset($dataInHours[$hourNr]['fact_prod_time'])){ $dataInHours[$hourNr]['fact_prod_time'] = 0; }
             if(!isset($dataInHours[$hourNr]['count_delay'])){ $dataInHours[$hourNr]['count_delay'] = 0; }
             if(!isset($dataInHours[$hourNr]['transition_time'])){ $dataInHours[$hourNr]['transition_time'] = 0; }
             if(!isset($dataInHours[$hourNr]['count_in_hour'])){ $dataInHours[$hourNr]['count_in_hour'] = 0; }
             if(!isset($dataInHours[$hourNr]['count_in_hour_quantity'])){ $dataInHours[$hourNr]['count_in_hour_quantity'] = 0; }
             if(!isset($dataInHours[$hourNr]['count_in_hour_unit_quantity'])){ $dataInHours[$hourNr]['count_in_hour_unit_quantity'] = 0; }
             $dataInHours[$hourNr]['shift_length'] += $time[0]['available_duration'];
             $dataInHours[$hourNr]['shift_length_with_exclusions'] = $dataInHours[$hourNr]['shift_length'];
             $dataInHours[$hourNr]['fact_prod_time'] += $time[0]['fact_time'];
             $dataInHours[$hourNr]['transition_time'] += $time[0]['transition_time'];
             $dataInHours[$hourNr]['count_in_hour'] += $time[0]['total_'.$fSensor['Sensor']['plan_relate_quantity']];
             $dataInHours[$hourNr]['count_in_hour_quantity'] += $time[0]['total_quantity'];
             $dataInHours[$hourNr]['count_in_hour_unit_quantity'] += $time[0]['total_unit_quantity'];
             if($time[0]['step'] > 0){
                 $dataInHours[$hourNr]['count_delay'] = bcadd($dataInHours[$hourNr]['count_delay'], bcdiv($time[0]['total_'.$fSensor['Sensor']['plan_relate_quantity']], bcdiv($time[0]['step'],3600,6), 6), 6);
             }
         }
         foreach($dataInHours as $hourNr => $dataInHour){
         	 $dataInHour['sensor_id'] = $fSensor['Sensor']['id'];
             $oeeParams = $this->calculateOee($dataInHour);
             if(array_key_exists('exploitation_factor', $dcHoursInfoBlocks)){
                $dcHoursInfoBlocks['exploitation_factor']['value'][(int)$hourNr] = floatval(number_format($oeeParams['exploitation_factor']*100,0,'.',''));
             }
             if(array_key_exists('operational_factor', $dcHoursInfoBlocks)){
                $dcHoursInfoBlocks['operational_factor']['value'][(int)$hourNr] = floatval(number_format($oeeParams['operational_factor']*100,0,'.',''));
             }
             if(array_key_exists('operational_factor', $dcHoursInfoBlocks)){
                $dcHoursInfoBlocks['operational_factor']['value'][(int)$hourNr] = floatval(number_format($oeeParams['operational_factor']*100,0,'.',''));
             }
             if(array_key_exists('oee_factor', $dcHoursInfoBlocks)){
                $dcHoursInfoBlocks['oee_factor']['value'][(int)$hourNr] = floatval(number_format(min(100,$oeeParams['oee']),0,'.',''));
             }
             if(array_key_exists('count_in_hour', $dcHoursInfoBlocks)){
             	$round = isset($dcHoursInfoBlocks['count_in_hour']['round'])?$dcHoursInfoBlocks['count_in_hour']['round']:2;
                $dcHoursInfoBlocks['count_in_hour']['value'][(int)$hourNr] = floatval(number_format($dataInHour['count_in_hour'],$round,'.',''));
             }
             if(array_key_exists('count_in_hour_quantity', $dcHoursInfoBlocks)){
             	$round = isset($dcHoursInfoBlocks['count_in_hour_quantity']['round'])?$dcHoursInfoBlocks['count_in_hour_quantity']['round']:2;
                $dcHoursInfoBlocks['count_in_hour_quantity']['value'][(int)$hourNr] = floatval(number_format($dataInHour['count_in_hour_quantity'],$round,'.',''));
             }
             if(array_key_exists('count_in_hour_unit_quantity', $dcHoursInfoBlocks)){
             	$round = isset($dcHoursInfoBlocks['count_in_hour_unit_quantity']['round'])?$dcHoursInfoBlocks['count_in_hour_unit_quantity']['round']:2;
                $dcHoursInfoBlocks['count_in_hour_unit_quantity']['value'][(int)$hourNr] = floatval(number_format($dataInHour['count_in_hour_unit_quantity'],$round,'.',''));
             }
             if(array_key_exists('avgspeed_in_hour', $dcHoursInfoBlocks)){
                $round = isset($dcHoursInfoBlocks['avgspeed_in_hour']['round'])?$dcHoursInfoBlocks['avgspeed_in_hour']['round']:2;
                $dcHoursInfoBlocks['avgspeed_in_hour']['value'][(int)$hourNr] = $dataInHour['fact_prod_time'] > 0?floatval(number_format(bcdiv($dataInHour['count_in_hour'],bcdiv($dataInHour['fact_prod_time'],3600,4),2),$round,'.','')):0;
             }
             if(array_key_exists('quality_factor', $dcHoursInfoBlocks)){
                $lossCount = isset($lossInHours[$hourNr])?$lossInHours[$hourNr]:0;
                $round = isset($dcHoursInfoBlocks['quality_factor']['round'])?$dcHoursInfoBlocks['quality_factor']['round']:2;
                $dcHoursInfoBlocks['quality_factor']['value'][(int)$hourNr] = $dataInHour['count_in_hour'] > 0?floatval(number_format(bcdiv(($dataInHour['count_in_hour'] - $lossCount), $dataInHour['count_in_hour'], 4) * 100, $round,'.','')):0;
             }
             $parameters = array(&$dataInHour, &$fSensor, &$oeeParams, &$dcHoursInfoBlocks, $hourNr);
             $this->callPluginFunction('Record_calculateMainFactorsInHourAfterLoop_Hook', $parameters, Configure::read('companyTitle'));
         }
    }

    //randamas esamos pamainos OEE rodiklis
    public function calcOee2($fShift,$fSensor,$currentPlan, $currentTimeShift){
        set_time_limit(2000);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $oeeObj = new OeeShell;
        $fSensor['dcTime']->add(new DateInterval('PT1S'));
        if(!isset($fSensor['dcTime']) || $fSensor['dcTime'] < new DateTime($fShift['Shift']['start'])){
            $fShift[Shift::NAME]['end'] = date('Y-m-d H:i');
        }
        $oeeObj->workWithMo = false;
        $oeeGaugeTitle = array_search($fSensor[Sensor::NAME]['show_oee_or_other_in_wc'], array(__('Pamainos OEE rodiklis')=>0, __('Pamainos prieinamumo koef.')=>1,__('Užsakymo efektyvumas')=>2));
        switch ($fSensor[Sensor::NAME]['show_oee_or_other_in_wc']) {
            case 1:
                $paramterTitle = 'exploitation_factor';
                break;
            case 2: //vykdomo uzsakymo efektyvumas skaiciuojamas tik jei neparinkat atvaizdavimo valanda arba pirmoji valanda vis dar esamoje pamainoje
                if(isset($currentPlan['ApprovedOrder']['id']) && $currentPlan['ApprovedOrder']['id'] > 0 && $currentTimeShift['Shift']['id'] == $fShift['Shift']['id']){
                    $fShift[Shift::NAME]['start'] = $currentPlan['ApprovedOrder']['created'];
                    $oeeObj->searchFromShift = false;
                }else{
                    $oeeGaugeTitle = __('Pamainos efektyvumas');
                }
                $paramterTitle = 'operational_factor';
                break;
            default:
                $paramterTitle = 'oee';
                break;
        }
        $oeeObj->shift = $fShift;
        $data = $oeeObj->start_sensors_calculation(array($fSensor[Sensor::NAME]['id']),true);
        if(isset($data[$fSensor[Sensor::NAME]['id']][$paramterTitle.'_with_exclusions'])){
            $paramterTitle .= '_with_exclusions';
        }
        if(isset($data[$fSensor[Sensor::NAME]['id']][$paramterTitle])){
            if(in_array($paramterTitle, array('oee','exploitation_factor','exploitation_factor_with_exclusions','operational_factor'))){
                $data[$fSensor[Sensor::NAME]['id']][$paramterTitle] *= 100;
            }
            return array(min(100, round($data[$fSensor[Sensor::NAME]['id']][$paramterTitle],0)), $oeeGaugeTitle);
        }else return array(0, $oeeGaugeTitle);
    }

    public function calculateDashboardsOEE(Array $currentShifts, Array $sensorsListInDB){
        $shiftModel = ClassRegistry::init('Shift');
        $prevShifts = array();
        foreach($currentShifts as $branchId => $shift){
            if(time() - strtotime($shift['Shift']['start']) > Configure::read('recordsCycle')*2){ continue; }
            $prevShift = $shiftModel->getPrev($shift['Shift']['start'], $branchId);
            if(empty($prevShift)){ continue; }
            $prevShifts[$prevShift['Shift']['id']] = $prevShift;
        }
        if(empty($prevShifts)){ return null; }
        $prevShiftsIds = array_keys($prevShifts);
        $dashboardsCalculationModel = ClassRegistry::init('DashboardsCalculation');
        $oeeCalculatedShiftsIds = $dashboardsCalculationModel->find('list', array(
            'fields'=>array('DashboardsCalculation.shift_id','DashboardsCalculation.shift_id'),
            'conditions'=>array(
                'DashboardsCalculation.shift_id'=>$prevShiftsIds,
                'DashboardsCalculation.sensor_id'=>Set::extract('/Sensor/id',$sensorsListInDB),
            ),'group'=>array('DashboardsCalculation.shift_id')
        ));
        $unCalculatedShiftsIds = array_diff($prevShiftsIds, $oeeCalculatedShiftsIds);
        if(empty($unCalculatedShiftsIds)){ return null; }
        set_time_limit(2000);
        App::import('Console/Command', 'AppShell');
        App::import('Console/Command', 'OeeShell');
        $shell = new OeeShell();
        $logModel = ClassRegistry::init('Log');
        foreach($unCalculatedShiftsIds as $shiftId){
            $logModel->write('Apskaiciuojamas OEE praejusiai pamainai. Pamainos ID: '.$shiftId);
            $shift = $prevShifts[$shiftId];
            $shell->shift = $shift;
            $sensorsIdsInCurrentShift = Set::extract('/Sensor/id', array_filter($sensorsListInDB, function($sensor)use($shift){
                return $sensor['Sensor']['branch_id'] == $shift['Shift']['branch_id'];
            }));
            $shell->start_sensors_calculation($sensorsIdsInCurrentShift,false);
        }
    }

    public function calculateOee($data){
        $oeeParams = array();
        $oeeParams['exploitation_factor'] = $data['shift_length'] > 0?$data['fact_prod_time'] / $data['shift_length']:0;
        $oeeParams['exploitation_factor_exclusions'] = isset($data['shift_length_with_exclusions']) && $data['shift_length_with_exclusions'] > 0?$data['fact_prod_time'] / $data['shift_length_with_exclusions']:0;
        $oeeParams['operational_factor'] = $data['fact_prod_time'] > 0?$data['count_delay'] / $data['fact_prod_time']:0;
        $oeeParams['quality_factor'] = isset($data['all_quantity']) && $data['all_quantity'] > 0?$data['good_quantity'] / $data['all_quantity']:0;
        $oeeParams['quality_factor'] = $oeeParams['quality_factor'] > 0?$oeeParams['quality_factor']:1;
        $oeeParams['oee'] = 100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'] * $oeeParams['quality_factor'];
        $oeeParams['oee_exclusions'] = 100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'] * $oeeParams['quality_factor'];
        $parameters = array(&$oeeParams, $data);
        $this->callPluginFunction('Record_calculateOee_Hook', $parameters, Configure::read('companyTitle'));
        return $oeeParams;
    }

    function getNewestSensorRecords($sensor, $minutes = 30){
        // RAW SQL: SELECT SUM(quantity), DATE_FORMAT(created, '%i') FROM `records` WHERE sensor_id=3 GROUP BY DATE_FORMAT(created,'%Y-%m-%d %H:%i') ORDER BY created DESC
        $query = $this->find('all', [
            'conditions' => ['sensor_id' => $sensor["id"] ?? 0 ],
            'fields'=> "SUM(quantity) AS qty, DATE_FORMAT(created,'%h:%i') AS min, plan_id",
            'group' => "DATE_FORMAT(created,'%Y-%m-%d %H:%i')",
            'order' => ['created' => 'DESC'],
            'limit' => $minutes + 1,
        ]);

        foreach($query as &$rec){
            $plan_id = $rec[$this::NAME]["plan_id"];
            if($plan_id){
                $plan = $this->Plan->find('first', [ 'id' => $plan_id ]);
                // $rec[0]["step"] = +$plan[$this->Plan::NAME]["step"];
                // $rec[0]["deviation"] = $sensor["tv_speed_deviation"];

                $matches = [];
                $step = +$plan[$this->Plan::NAME]["step"] ?? 0;
                preg_match_all('/\d+/', $sensor["tv_speed_deviation"], $matches );
                $t0  = $step/60 * (1 - ($matches[0][2]?? 0) / 100);
                $t1  = $step/60 * (1 + ($matches[0][3]?? 0) / 100);
                $rec[0]["t0"] = $t0;
                $rec[0]["t1"] = $t1;
            }
        }
        // !'query' rezultatas yra 'apvyniotas' papildome masyve
        // $query[0][0]["qty"]
        $result = array_column($query, 0);
        return array_splice($result, 1); // pasalinamas papildomas masyvas ir pasalinami pirmos minutes duomenys
    }
    
}
