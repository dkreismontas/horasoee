```css

/* montserrat-300 - latin-ext_latin */
@font-face {
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 300;
  src: url('montserrat-v15-latin-ext_latin-300.eot'); /* IE9 Compat Modes */
  src: local(''),
       url('montserrat-v15-latin-ext_latin-300.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('montserrat-v15-latin-ext_latin-300.woff2') format('woff2'), /* Super Modern Browsers */
       url('montserrat-v15-latin-ext_latin-300.woff') format('woff'), /* Modern Browsers */
       url('montserrat-v15-latin-ext_latin-300.ttf') format('truetype'), /* Safari, Android, iOS */
       url('montserrat-v15-latin-ext_latin-300.svg#Montserrat') format('svg'); /* Legacy iOS */
}
/* montserrat-regular - latin-ext_latin */
@font-face {
  font-family: 'Montserrat';
  font-style: normal;
  font-weight: 400;
  src: url('montserrat-v15-latin-ext_latin-regular.eot'); /* IE9 Compat Modes */
  src: local(''),
       url('montserrat-v15-latin-ext_latin-regular.eot?#iefix') format('embedded-opentype'), /* IE6-IE8 */
       url('montserrat-v15-latin-ext_latin-regular.woff2') format('woff2'), /* Super Modern Browsers */
       url('montserrat-v15-latin-ext_latin-regular.woff') format('woff'), /* Modern Browsers */
       url('montserrat-v15-latin-ext_latin-regular.ttf') format('truetype'), /* Safari, Android, iOS */
       url('montserrat-v15-latin-ext_latin-regular.svg#Montserrat') format('svg'); /* Legacy iOS */
}