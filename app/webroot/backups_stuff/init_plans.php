<?php
class PlansInit{
	
	public function __construct(){
		$this->lookIntoProjects();
	}
	
	private function lookIntoProjects(){
		$pathToDomain = __DIR__.'/../domains';
		$di = new DirectoryIterator($pathToDomain);
		foreach ($di as $domain) {
			if(is_dir($domain->getPathname()) && trim($domain->getBasename(),'.') ){
				$logsPath = $pathToDomain.'/'.$domain->getBasename().'/public_html/app/tmp/logs';
				if(!file_exists($logsPath)) continue;
				$projectPlansInitiated = new DirectoryIterator($logsPath);
				foreach($projectPlansInitiated as $planFile){
					if(trim($planFile->getBasename(),'.') && preg_match('/^planMarker_(\d+)_.+/', $planFile->getBasename())){
						
						$lines = file($logsPath.'/'.$planFile->getBasename() , FILE_IGNORE_NEW_LINES );
						//if(isset($lines[0]) && $lines[0] != 'init') continue;
						$lines[0] = 'start';
						file_put_contents( $logsPath.'/'.$planFile->getBasename() , implode( "\n", $lines ) );
						$content = json_decode($lines[3],true);
						if(is_array($content)){
							$content = array_merge($content, array('serverLogin'=>$lines[2]));
							$this->execute_plan_start($domain->getBasename(), $content);
						}
						
						
						/*$handle = fopen($logsPath.'/'.$planFile->getBasename(), 'r');
						$planFileData = '';
						$lineCounter = 1;
						while (!feof($handle)) {
							$planFileData = trim(fgets($handle, 4000));
							if($lineCounter == 1 && $planFileData != 'init') break;
							if($lineCounter == 3) $serverLogin = trim($planFileData);
							if($lineCounter == 4){
								$content = json_decode($planFileData,true);
								if(is_array($content)){
									$content = array_merge($content, compact('serverLogin'));
									$this->execute_plan_start($domain->getBasename(), $content);
								}
							}
							$lineCounter++;
						}*/
					}
				}
			}
		}
	}
	
	private function execute_plan_start($domain, $content){
		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL,'http://www.dev1.horasmpm.eu/plans/start_planing');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, 
		          http_build_query($content));
		curl_setopt($ch, CURLOPT_FAILONERROR, true);

		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$server_output = curl_exec ($ch);
		echo $server_output;
		print_r(curl_getinfo($ch));
		echo curl_errno($ch);
		curl_close ($ch);
		die();
		
		
		
		$postdata = http_build_query($content);
		$opts = array('http' =>
			array(
				'method'  => 'POST',
				'header'  => 'Content-type: application/x-www-form-urlencoded',
				'content' => $postdata
			)
		);
		$context  = stream_context_create($opts);
		$result = file_get_contents('http://'.$domain.'/plans/start_planing?lang=lit', false, $context);
		echo $result;
	}
	
}

new PlansInit;