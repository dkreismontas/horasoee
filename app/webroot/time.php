<?php
date_default_timezone_set("Europe/Vilnius");
echo 
'{
    "time": "'.date("Y-m-d H:i:s").'",
    "unix": '.time().'
}';
?>