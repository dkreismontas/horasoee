(function(angular) {
	'use strict';
	var app = angular.module('WorkCenter', ['ngSanitize']);
	var inDialog = false, soundsLoaded = false;

	if (('createjs' in window) && 'Sound' in createjs) {
		createjs.Sound.alternateExtensions = ['ogg'];
		createjs.Sound.addEventListener('fileload', function() {
			soundsLoaded = true;
		});
		//createjs.Sound.registerSound(window.baseUri + 'sounds/notify.mp3', 'notifySound');
	}

	//-----------------------------------
	// copies of HTML elements
	const template_metric = document.querySelector("#tml-metrics > div");
	const template_graph = document.querySelector("#tml-graphs > div");
	const template_title = document.querySelector("#tml-titles > div");

	//-----------------------------------

	app.controller('WorkCenterCtrl', ['$scope', '$http', '$timeout','$filter','$q', function($scope, $http, $timeout,$filter,$q) {
		$scope.is_side_open = false;
		$scope.calendar_input_str = "";
		$scope.lost_connection=false;
        //$scope.Math=Math; // ???
		$scope.currTime = new Date();
		$scope.selTime = null;
		$scope.needTimePager = window.is_operator?false:true;
		$scope.updateInProgress = false;
		$scope.helpUrl = 'javascript:void(0)';
		$scope.hours = [];
		$scope.workCenter = null;
		$scope.currentPlan = null;
		$scope.oeeGaugeTitle = '';
		$scope.foundProblem = null;
		$scope.texts = null;
		$scope.currentShift = null;
		$scope.historyShift = null;
		$scope.line = null;
		$scope.event = null;
		$scope.plans = [];
		$scope.allPlans = [];
		$scope.products = [];
		$scope.additionalFieldsErrors = [];
        $scope.productsFilterString = "";
        $scope.plansFilterString = "";
        $scope.graphHoursCount = 0; // edit: new default 6 from 0
        $scope.createPlan = {
            paste_code: 'T001',
            production_name: 'Gaminys',
            production_code: 'Gam',
            mo_number: '000009',
            start: '2015-07-20 12:00',
            end: '2015-07-21 12:00',
            quantity: 10,
            line_quantity:1,
            step:2000,
            box_quantity:1,
            package_quantity:1,
            control:'X',
            comment:'Comment',
            weight:0,
            yield:0,
            qty_1:0
        };
        $scope.callProdComplete = false; //kai gamyba baigta ir spaudziamas perejimas jei yra perejimo tipu, realiai kvieciame sendRegisterIssue f-ja, taciau reikia kviesti callProdComplete
        $scope.promptProductionWindow = false;
        $scope.mainPluginTitle = '';
		$scope.problemTypes = [];
		$scope.transitionProblemTypes = [];
		$scope.zeroTimeTransitionProblemTypes = [];
        $scope.workingProblemTypes = [];
        $scope.parentProblemTypes = [];
		$scope.speedProblemTypes = [];
		$scope.sensor = [];
		$scope.lossTypes = [];
		$scope.OEE = 0;
		$scope.lastMinuteSpeed = 0;
		$scope.showSpeedTitle = false;
        $scope.view_r1 = false;
        $scope.sensorType = 0;
        $scope.waitingDiffCardId = 0;
        $scope.verificationOnProductionEnd = false;
        $scope.verification = null;
        $scope.informationText = 'tekstas';

        $scope.serverTime = null;
        $scope.serverUnix = null;

        $scope.toggle_r1 = false;
        $scope.toggleR1Text_off = "Sekti R1 jutiklį";
        $scope.toggleR1Text_on = "Nesekti R1 jutiklio";
        $scope.toggle_r1Text = $scope.toggleR1Text_off;

        $scope.showDeclareQuantities = false;
        $scope.partialQuantity = '';
        $scope.partialQuantityPieces = '';
        $scope.orderPartialQuantity = 0;
        $scope.orderPartialQuantityPieces = 0;
        $scope.orderLossesQuantities = 0;
        $scope.totalOrderLossesQuantities = 0;
        $scope.partialQuantityFinish = '';
        $scope.partialQuantityPiecesFinish = '';
        $scope.laukas = 0;

        $scope.orderPostphoneCountdown = 0;
		$scope.newPlan = null;
		$scope.newProduct = null;
        $scope.newType = 0;
		$scope.stateLoading = false;
		$scope.swapCurrentPlanYesPlans = false;
		$scope.stopProdState = 0;
		$scope.endDlg = {};
		$scope.waitForFakePostphone = false;
		$scope.isScreenLocked = false;
		$scope.problemComment = '';
		$scope.downtimeCutType = 0;
		$scope.downtimeCutDuration = 0;
		$scope.accessesToMethods = [];
		$scope.prevShiftsOEE = [];
		$scope.additionalPlanData = {};
		$scope.availableSensors = {};
		$scope.workcenterHoursInfoBlocks = {};
		$scope.lockScreen = function(lock) {
			if (lock && !$scope.isScreenLocked) {
				modals.lockScreen.show();
			} else if (!lock && $scope.isScreenLocked) {
				modals.lockScreen.hide();
			}
			$scope.isScreenLocked = lock ? true : false;
		};
		$scope.createPlanErrors = [];
		
		$scope.drawTimeline = function(){
			let missing_value_string = "-";

			let emptyTimeline = function(){
				document.querySelectorAll(".popover").forEach(function(e){ e.remove(); });
				document.querySelector("#tml-metrics").innerHTML = null;
				document.querySelector("#tml-graphs").innerHTML = null;
				document.querySelector("#tml-titles").innerHTML = null;
				document.querySelector("#tml-drawer-metrics").innerHTML = null;
			}

			let getBackground = function(period){
				let color = "black";

				if(period.type === 1) {
					color = "green";
				}else if(period.type === 2) {
					color = "red";
				}else if(period.type === 3) {
					if(period.whileWorking) {
						color = "sys_error";
					}else if(period.json && period.json.transition_exceeded > 0){
						color = "exceeded_transition"; // Help: exceeded_transition class?
					}else{
						color = "yellow";
					}
				}else if(period.type === 4) {
					color = "red_grid";
				}else if(period.type === 5) {//spausdinimo proceso indikatorius
					color = "print";
				}else if(period.type === 6) {//tuscio laiko tarpo indikatorius
					color = "white";
				}else if(period.type === 7) { //leto greicio indikatorius darbo metu
					color = "green_slow_speed";
				}else if(period.type === 8) { //trumpos problemos indikatorius
					color = "short_problem";
				}else if(period.type === 9) { //prastova isimtis
					color = "problem_exclusion";
				}else if(period.type === 10) { //planas
					color = "planed_time";
				}else{
					color = (period.type ?? "null_type").toString();
				}

				return color;
			}

			let generatePeriod = function(period, range = 3600){
				// let direction = period.start > 3600/2 ? "left" : "right";
				let e = document.createElement("div"); // period div
				e.setAttribute("data-toggle", "popover");
				e.setAttribute("data-placement", "auto"); 
				e.setAttribute("data-content", period.title);
				// if(period.shortTitle) e.setAttribute("data-title", period.shortTitle);
				if(period.title){
					new bootstrap.Popover(e,{
						trigger:"hover",
						html: true,
						template: /*html*/`
							<div class="popover rounded-0 text-dark" role="tooltip">
								<div class="popover-arrow"></div>
								<h3 class="popover-header"></h3>
								<div class="popover-body p-2 bg-white lh-base"></div>
							</div>`
						}
					)
				}

				//for(const c of getBackground(period).split(" ")) e.classList.add('tml-gitem-' + c);
				e.classList.add("oee-center", 'tml-gitem', 'tml-gitem-' + getBackground(period), 'problemid_' + (period.problemId?? "null"));
				e.style.overflow = "hidden";
				e.style.width = ((period.end - period.start) / range * 100) + "%"; // valandos periodai pasibaigia ties 3598 sekunde, o ne 3600
				e.style.height = "100%";
				e.style.minWidth = "0px";
				e.style.minHeight = "0px";
				//e.style.background = "#"+((1<<24)*Math.random()|0).toString(16);

				if (period.shortTitle){
					let span = document.createElement("span");
					span.innerText = period.shortTitle;
					span.style.fontSize = "0.8em";
					span.style.opacity = "0.7";
					span.style.whiteSpace = "nowrap";

					e.appendChild(span);
				}

				if ((window.allowChangeProcess || (window.baseUriDc || period.type == 4)) && (period.foundProblemId != null)){
					e.onclick = function(){
						period.data.FoundProblem.oldStart = period.data.FoundProblem.start;
						period.data.FoundProblem.oldEnd = period.data.FoundProblem.end;
						period.data.FoundProblem.newStart = '';
						period.data.FoundProblem.newEnd = '';

						let currentDowntimeDuration = (new Date(period.data.FoundProblem.end).getTime() - new Date(period.data.FoundProblem.start).getTime())/1000;
						period.data.FoundProblem.duration = ('0'+String(parseInt(currentDowntimeDuration / 3600))).slice(-2) + ':' + 
															('0'+String(parseInt(currentDowntimeDuration % 3600 / 60))).slice(-2) + ':' + 
															('0'+String(parseInt(currentDowntimeDuration % 3600 % 60))).slice(-2);

						$scope.endProductionProc({currentPlan: null, event: {dateTime: period.dateTime, foundProblemId: period.foundProblemId, type: period.type, problemId: period.problemId, foundProblem: period.data.FoundProblem}});
						
						// Skriptas del bioko
						// jei buvo paspaustas ant intyervalo, kuris susikure kaip derinimo virsijimas (yra su nepazymeta prastova ir turi perejimo problema)
						if(period.json && period.json.transition_exceeded > 0 && period.type == 4){ 
							$scope.prodRegisterIssue($scope.zeroTimeTransitionProblemTypes);
						}
	
						$scope.$apply();
					}
				}else if(period.title.length > 0){
					e.onclick = function() { 
						console.log("show info modal")
						$scope.showInformationModal(period.title, period.data); 
					} 
				}
				
				return e;
			}

			let generateMetrics = function(hour){
				let blocks = $scope.workcenterHoursInfoBlocks;

				for(const key of Object.keys(blocks)){
					const metric = blocks[key];

					let mt = template_metric.cloneNode(true);
					let mb = mt.querySelector("#tml-metric-body");
					mt.querySelector("#tml-metric-head").textContent = metric.title;
					mb.innerHTML = null;

					for (const hour of $scope.hours) {
						let mc = template_metric.querySelector("#tml-metric-body > div").cloneNode();
						mc.innerHTML = metric?.value[ hour.value ] ?? missing_value_string;
						mb.appendChild(mc);
					}
					
					document.querySelector("#tml-metrics").appendChild(mt);
					document.querySelector("#tml-drawer-metrics").appendChild(mt.cloneNode(true));
				};
			}

			let renderTimeline = function(){ 
				for (const hour of $scope.hours) {
					// debug:
					// hour.markers = {
					// 	a:{class: "reservation", text: "test", minutes: Math.floor(Math.random()*60) },
					// 	b:{class: "confirmation", text: "!", minutes: Math.floor(Math.random()*60) },
					// 	c:{class: "reservation", text: "ilgesnis teskstas", minutes: Math.floor(Math.random()*60) }
					// }

					// create
					let graph = template_graph.cloneNode(true);
					let title = template_title.cloneNode(true);

					// fill with markers
					if(hour.markers){
						for(let marker of Object.values(hour.markers)) {
							let div = document.createElement("span");
							div.style.left = `${ (marker.minutes ?? 0) / 60 * 100 }%`;
							div.classList.add("marker", marker.class ?? "null");
							graph.querySelector("#tml-1").appendChild(div);
						}
					}

					// fill with intervals
					title.querySelector("#tml-hour").textContent = hour.value.toString().padStart(2, '0');
					title.querySelector("#tml-date").textContent = hour.date.toString();
					
					for(let i in hour.periods){
						let curr_period = hour.periods[parseInt(i)]; // current period
						let next_period = hour.periods[parseInt(i)+1]; // next period

						// generate gap if necessary
						let error_margin = 6;
						if(next_period){
							if(next_period.start - curr_period.end > error_margin){
								let e = document.createElement("div");
								e.style.width = `${ (next_period.start - curr_period.end) / (3600) * 100 }%`; 
								graph.querySelector("#tml-1").append( e );
							}else{
								// paklaidos taisymas, jei tarp intervalo pabaigos ir sekancio pradzios skirtumas yra mazesnis (arba =) negu 2
								curr_period.end += next_period.start - curr_period.end;
							}
						}

						graph.querySelector("#tml-1").append( generatePeriod(curr_period, 3600 ) ); 
					}
					
					if(hour.planPeriods.length === 0){
						graph.querySelector("#tml-2").remove();
					}else{
						for(let i in hour.planPeriods){
							let curr_period = hour.planPeriods[i];
							graph.querySelector("#tml-1").append( generatePeriod(curr_period, 3600 ) ); 
						}
					}

					document.querySelector("#tml-graphs").appendChild(graph);
					document.querySelector("#tml-titles").appendChild(title);
				}
			}

			let fillTmlQuantity = function(){
				// let last = document.querySelector("#tml-graphs > div:last-child");
				// last?.appendChild( createQuantityBox(scope?.graphTimelinePlan?.ApprovedOrder?.quantity ?? missing_value_string) );
				let quantity = $scope.currentPlan?.ApprovedOrder?.quantity;
				if(quantity != null){
					document.querySelector("#tml-quantity").textContent = quantity.split(" ")[0];
					document.querySelector("#tml-quantity-units").textContent = " " + quantity.split(" ")[1];
				}else{
					document.querySelector("#tml-quantity").textContent = "0";
					document.querySelector("#tml-quantity-units").textContent = "";
				}
			}

			emptyTimeline();
			renderTimeline();
			generateMetrics();
			fillTmlQuantity();
		}

		// note: help: nenaudojama?
        $scope.doCreatePlan = function(plan) {
            plan.line_id = $scope.line.id;
            plan.sensor_id = $scope.workCenter.id;
            //$scope.createPlan = {};
            $http.post(window.baseUri + 'work-center/createAndStartPlan/', {plan:plan}).success(function (data) {
	
                if('errors' in data) {
                    $scope.createPlanErrors = data.errors;
                } else {
                    if('newPlan' in data) {
                        $scope.newPlan = data.newPlan;
                        $scope.prodConfirmStart();
                    }
                }
                //$scope.stateLoading = false;
                //modals.startPlan.hide();
                //inDialog = false;
                //updateStats();
            });
        };
		$scope.selectNewPlan = function(plan) {
			inDialog = true;
			$scope.stateLoading = false;
            $scope.newPlan = plan;
        };
		$scope.selectAdditionalField = function(item,itemTitle) {
            $scope.additionalPlanData[itemTitle] = item;
        };
        $scope.selectNewProduct = function(product) {
        	inDialog = true;
			$scope.stateLoading = false;
            $scope.newProduct = product;
        };
		$scope.$watch(function() { return $scope.event; }, function() {
			setTimeout(function() { // wait for all modals to close
				if ($scope.event) {
					if ($scope.event.type === 'started') { $scope.startProductionProc(); $scope.notifySound(); }
					if ($scope.event.type === 'stopped') { $scope.endProductionProc({autoEvent:true}); $scope.$apply(); $scope.notifySound(); }
					if ($scope.event.type === 'speed') { $scope.startRegisterSpeedIssue(); $scope.notifySound(); }
				}
			}, 1000);
		});
		// $scope.emptyIfZero = function(inputName){
		// 	if(!$scope[inputName]){
		// 		$scope[inputName] = '';
		// 	}
		// };
		$scope.initiateShiftChange = function(){
			let confirmShiftChage = confirm(text_confirm_shift_change);
			if(!confirmShiftChage){ return false; }
			$http.post(window.baseUri + 'workCenter/setShiftEnd/', {'sensor_id':$scope.sensor.id}).success(function (data) {
				updateStats();
			});
		};

		$scope.openConfirmationPage = function(){
			window.open(window.baseUri + 'approved_orders', "new");
            return false;
		};
		$scope.setGraphHoursCount = function(self) {
			$scope.graphHoursCount = parseInt(self.graphHoursCount);

			if($scope.selTime == null ) $scope.changeTime(0);
			else $scope.changeTime();

			// updateStats();
		};
		$scope.notifySound = function() {
			if (soundsLoaded) {
				var snd = createjs.Sound.play('notifySound');
				snd.setVolume(0.2);
			}
		};
		$scope.calcGraphVal = function(val, left) {
			var w = 986;
			var h = 3600;
			var nval = Math.ceil(val/h*w);
			if (nval < 0) nval = 0;
			if (left) nval -= 0; else nval += 1;
			return nval + 'px';
		};

        $scope.confirmOrders = function() {
            window.open(window.baseUri+'approved_orders/index/sensorId:'+$scope.sensor.id, "xx");
            return false; 
        };
        $scope.swapCurrentPlan = function() {
            modals.swapCurrentPlan.show();
        };
        $scope.printPage = function() {
            window.print();
        };
        $scope.swapCurrentPlanYes = function() {
            $scope.swapCurrentPlanYesPlans = true;
        };
        $scope.swapCurrentPlanNo = function() {
            $scope.swapCurrentPlanYesPlans = false;
            $scope.stateLoading = false;
            $scope.swapCurrentPlanYesPlansYes = false;
            modals.swapCurrentPlan.hide();
        };
        $scope.swapCurrentPlanConfirm = function() {
			let new_plan = $scope.sensor.working_on_plans_or_products == 1? $scope.newProduct.Product.name : $scope.newPlan.production_name;
			if($scope.currentPlan.Plan.production_name === new_plan){
				alert("Planas turi būti kitoks");
			}else{
				$scope.swapCurrentPlanYesPlansYes = true;
			}
        };

        $scope.swapCurrentPlanConfirmYes = function() {
            $scope.stateLoading = true;
            var currentPlan = $scope.currentPlan.Plan;
            var options = {
                'currentPlanId':currentPlan.id,
                'sensorId':$scope.workCenter.id,
                'additionalData': $scope.additionalPlanData
            };
            if($scope.sensor.working_on_plans_or_products == 1){
            	var swapProduct = $scope.newProduct.Product;
            	if(currentPlan.product_id == swapProduct.id) {
	                $scope.swapCurrentPlanNo();
	                return;
	            }
	            options.newProductId = swapProduct.id;
			}else if($scope.sensor.working_on_plans_or_products == 0){
				if(currentPlan.id == $scope.newPlan.id) {
	                $scope.swapCurrentPlanNo();
	                return;
	            }
	            options.newPlanId = $scope.newPlan.id;
			}
            $http.get(window.baseUri + 'work-center/swap-current-plan',{params:options}).success(function() {
                //$scope.prodPostphone();
                //$scope.prodConfirmStart();
                $scope.swapCurrentPlanNo();
                inDialog = false;
                updateStats();
                $scope.stateLoading = false;
            }).error(function() {
                console.log("ERROR");
                $scope.stateLoading = false;
            });
        };
        
        

		$scope.showInformationModal = function(information, periodData) {
			let foundProblem = periodData.hasOwnProperty('FoundProblem')?periodData.FoundProblem:null;
			foundProblem = foundProblem == null && periodData.hasOwnProperty('Work')?periodData.Work:foundProblem;
			if($scope.sensor.time_interval_declare_count_button == 2) {
				$scope.totalOrderLossesQuantities = 0;
				$scope.orderLossesQuantities = new Array();
				if (foundProblem.problem_id == -1) {
					for (var i in periodData.ApprovedOrder.losses) {
						let loss = periodData.ApprovedOrder.losses[i];
						$scope.orderLossesQuantities[i] = loss.quantity;
						$scope.totalOrderLossesQuantities += parseFloat(loss.quantity);
					}
				}
				inDialog = true;
			}
			if(foundProblem != null){
				$scope.informationComment = foundProblem.comments;
				$scope.informationFoundProblemId = foundProblem.id;
			}else{
				$scope.informationComment = '';
				$scope.informationFoundProblemId = 0;
			}
			$scope.informationText = information;
			//$scope.informationText = '<div class="col-sm-6">'+information +'</div><div class="col-sm-6">'+ $('#declareQuantitiesModal').find('.modal-body').html()+'</div>';
			$scope.$apply();
			$('#informationText').html($scope.informationText);
			modals.information.show();
		};

		$scope.closeInformationModal = function() {
			if($scope.sensor.time_interval_declare_count_button == 2) {
				inDialog = false;
				updateStats();
			}
			modals.information.hide();
		};
		$scope.saveAndCloseInformationModal = function(foundProblemId) {
			if(foundProblemId > 0){
				let params = {foundProblemId: foundProblemId, problemComment: $scope.informationComment}
				$http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-issue/send_only_comment', {params: params}).success(function (data) {
					updateStats();
					modals.information.hide();
				});
			}
		};

		$scope.debug = function(self){
			console.log("debug function is working!");
			console.log(self);
		}

		$scope.changeTime = function(step_size){
			if(step_size === 0 && !$scope.updateInProgress){ // update time to present
				$scope.selTime = null;
				updateStats();
				return;
			}

			if( $scope.isTimeCtrlDisabled(step_size) || $scope.updateInProgress ){
				console.log("disabled or update")
				return;
			}

			let callendar_string = document.querySelector("#date-picker-input").value; // picked time from callendar
			let server_time = new Date( $scope.serverTime ).setMinutes(59, 59) - $scope.graphHoursCount * 3600_000; // server timestamp

			if(step_size == null && callendar_string != ""){
				console.log("calendar changing value");
				let new_time = new Date( callendar_string ).setMinutes(-1, 59); // current timestamp

				if(new_time > server_time){
					$scope.selTime = null; // display current timeline flow
				}else{
					$scope.selTime = new Date( new_time );
				}
			
			}else{
				let new_time = new Date( $scope.selTime?.getTime() ?? server_time ).setMinutes(59, 59); // current timestamp
				$scope.selTime = new Date( new_time + (step_size??0) * 3600_000 );

				if($scope.selTime.getTime() >= server_time){
					console.log("attaching to move");
					$scope.selTime = null; // display current timeline flow
				}
				
			}

			updateStats();
		}

		$scope.formatDateStr = (d) => (d.getFullYear()) + "-" + ("0"+(d.getMonth()+1)).slice(-2) + "-" + ("0" +(d.getDate())).slice(-2) + " " + ("0" + d.getHours()).slice(-2) + ":" + ("0" + d.getMinutes()).slice(-2);
		$scope.getTimeCtrlClass = (step_size) => $scope.isTimeCtrlDisabled(step_size)? 'oee-bg-light' : 'oee-bg-dark';
		$scope.isTimeCtrlDisabled = function(step_size){
			if(step_size == null) return $scope.updateInProgress;
			let server_time = new Date( $scope.serverTime ).setMinutes(60, 0) - $scope.graphHoursCount * 3600_000; // server timestamp
			let new_time = new Date( ($scope.selTime?.getTime() ?? server_time) + step_size * 3600_000 ).setMinutes(59, 59); // new time
			return new_time >= server_time || $scope.updateInProgress;
		}


		$scope.hideForeground = async function(){
			document.querySelector("#foreground").style.opacity = "0";
			await new Promise(r => setTimeout(r, 200));
			document.querySelector("#foreground").style.display = "none";
		}

		$scope.showToolbarModal = function(){ 

			let areBtnsHidden = function(){
				let nodes = document.querySelectorAll("#toolbar-btns > a");
				let i = 0;
				for(let node of nodes){ if(node.style.display === "none") i++; }
				return nodes.length == i;
			}

			let createModalBtn = function(btn){
				let off_color = "#FF5757";
				let new_btn = btn.cloneNode(true);
				new_btn.setAttribute("data-dismiss","modal");
				new_btn.style.display = "block";
				new_btn.style.width = "100%";
				new_btn.style.backgroundColor = window.getComputedStyle(btn).backgroundColor;
				new_btn.classList.add("p-3"); // increase padding

				let checkbox = document.createElement('div');
				checkbox.classList.add("form-check", "form-switch", "h-100", "d-flex", "align-items-center", "p-0", "ml-2", "mr-4");

				let input = document.createElement('input');
				input.classList.add("form-check-input", "m-0");
				input.setAttribute("type","checkbox");
				input.style.height = "1.5em";
				input.style.width = "3em";
				
				input.checked = window.getComputedStyle(btn).display === "flex";
				input.style.backgroundColor = input.checked? "#009966" : off_color;
				
				if(btn.getAttribute("data-id") != null){
					input.onclick = function(){
						btn.style.display = !input.checked? "none" : "flex";
						input.style.backgroundColor = input.checked? "#009966" : off_color;
						document.querySelector(".grid_tb-menu").style.display = areBtnsHidden()? "none" : "block";
						document.querySelector(".grid_extra").style.display = areBtnsHidden()? "none" : "block";
						$scope.hideToolbarBtnRequest(btn.getAttribute("data-id"), !input.checked);
					}
				}else{
					input.disabled = true;
					input.checked = true;
					input.style.backgroundColor = input.checked? "#009966" : off_color;
				}

				checkbox.appendChild(input);

				//----------------

				let wrapper = document.createElement('div');
				wrapper.classList.add("d-flex","align-items-center");
				wrapper.appendChild(checkbox);
				wrapper.appendChild(new_btn);
				return wrapper;
			}

			modals.toolbar.show(); 
			
			let container = document.querySelector("#modal-toolbar-btns");
			container.innerHTML = new String();

			for(const btn of document.querySelectorAll("#toolbar-btns > a")){
				// create new button
				let new_btn = createModalBtn(btn);
				container.appendChild( new_btn );

				// recompile/register new buttons
				let e = angular.element(new_btn);
				e.injector().invoke(function($compile){
					let scope = e.scope();
					$compile(e)(scope);
				})
			}
		}

		$scope.showToolbarDrawer = function(){ 
			let createDrawerBtn = function(btn){
				let new_btn = btn.cloneNode(true);
				new_btn.setAttribute("data-dismiss","modal");
				new_btn.classList.add("p-3", "fs-3");
				return new_btn;
			}

			let container = document.querySelector("#drawer-toolbar-btns");
			container.innerHTML = new String();

			for(const btn of document.querySelectorAll("#toolbar-btns > a")){
				// create new button
				let new_btn = createDrawerBtn(btn);
				container.appendChild( new_btn );

				// recompile/register new buttons
				let e = angular.element(new_btn);
				e.injector().invoke(function($compile){
					let scope = e.scope();
					$compile(e)(scope);
				})
			}
		}

		$scope.hideToolbarBtnRequest = async function(id, hide){
			if(id.length > 32) { console.warn("button ID length > 32"); return; }
			let response = await $http.post(window.baseUri + 'workCenter/hideButton/', { id, hide });
			if(response.status !== 200) { console.warn("workCenter/hideButton", response.status); return; }
			console.log("btn response:", response.data);
		}

		$scope.verifyToolbarBtnsVisibility = async function(){
			let areBtnsHidden = function(){
				let nodes = document.querySelectorAll("#toolbar-btns > a");
				let i = 0;
				for(let node of nodes){ if(node.style.display === "none") i++; }
				return nodes.length == i;
			}

			await new Promise(r => setTimeout(r, 0));

			for(let id of $scope.hidden_buttons){
				let btn = document.querySelector(`#toolbar-btns > [data-id='${id}']`);
				if(btn) btn.style.display = "none";
			}

			document.querySelector(".grid_tb-menu").style.display = areBtnsHidden()? "none" : "block";
			document.querySelector(".grid_extra").style.display = areBtnsHidden()? "none" : "block";
		}

		$scope.configureSpeedometer = function(){
			let graph = document.querySelector("bar-graph");
			graph.setDataFromRecords($scope.new_records);

			if(($scope.currentPlan?.Plan?.step_unit_divide?? false) === false) return;
			
			// modals.test.show();
			let speedometer = document.querySelector("speedometer-gauge");

			let d = $scope.currentPlan?.Plan?.step_unit_divide ?? 1;
			let step = parseFloat($scope.currentPlan.Plan.step / d);

			let deviations = $scope.sensor.tv_speed_deviation.split(",");

			let min_max = deviations[0]?.match(/\d+/g);
			let thresholds = deviations[1]?.match(/\d+/g);

			let min = step * (1 - (min_max[0]?? 0) / 100);
			let max = step * (1 + (min_max[1]?? 0) / 100);
			let t0  = step * (1 - (thresholds[0]?? 0) / 100);
			let t1  = step * (1 + (thresholds[1]?? 0) / 100);
			
			// console.table({step, min, max, "val": $scope.lastMinuteSpeed})
			speedometer.text = $scope.currentPlan?.Plan?.step_unit_title ?? "-";
			speedometer.value = $scope.lastMinuteSpeed;
			speedometer.min = min;
			speedometer.max = max;
			speedometer.t0 = t0;
			speedometer.t1 = t1;
		};

		$scope.openTmlDrawer = function(){
			$scope.is_side_open = !$scope.is_side_open;
			let s = document.querySelector("#tml-drawer-metrics");
			s.style.left = $scope.is_side_open ? "-220px" : "0px";
		}
		
		$scope.endProductionProc = function(opts) {
			if (typeof opts !== 'object') opts = {};
			if(opts.hasOwnProperty('event')){
				$scope.problemComment = opts.event.foundProblem.comments;
			}
			$scope.endDlg = {
				currentPlan: (('currentPlan' in opts) ? opts.currentPlan : $scope.currentPlan),
				event: (('event' in opts) ? opts.event : $scope.event),
				autoEvent: (opts.autoEvent ? true : false),
				quickSwap: (opts.quickSwap ? true : false)
			};
			modals.endPlan.show();
			inDialog = true;
			if (opts.quickSwap) {
				$scope.stateLoading = true;
				$http.get(window.baseUri + 'work-center/store-timestamp').success(function() {
					$scope.stateLoading = false;
				}).error(function() {
					$scope.stateLoading = false;
				});
				$scope.stopProdState = 2;
			} else {
				$scope.stateLoading = false;
				$scope.stopProdState = 0;
			}
		};
		$scope.startProductionProc = function() {
			$scope.newPlan = null; // edit: important:
			$scope.newProduct = null; // edit: important:
			modals.startPlan.show();
			//inDialog = true;
			//$scope.stateLoading = false;

			// TODO: datepicker, start production, test
            // jQuery(".datepicker").datetimepicker({
            //     dateFormat: "yy-mm-dd"
			// });
			
			// $('#date-picker-input').bootstrapMaterialDatePicker({ 
			// 	format : 'YYYY-MM-DD HH:00', 
			// 	lang: 'lt',
			// 	weekStart : 1, 
			// 	maxDate: new Date(), 
			// 	year: false,
			// 	cancelText: "Atšaukti",
			// 	okText: "Toliau",
			// 	switchOnClick : true,
			// })
		};

        $scope.prodConfirmStartConfirm = function(type) {
			let printRequireText = new Array();
			for (var inputName in requiredInputTexts){
				if(!$scope.additionalPlanData.hasOwnProperty(inputName)){
					printRequireText.push(requiredInputTexts[inputName]);
				}
			}

			if($scope.newPlan == null && $scope.newProduct == null){
				printRequireText.push(text_what_you_produce);
        	}
			if(printRequireText.length){
				alert(printRequireText.join("\n"));
				return;
			}
            if(type == 0) {
                $scope.newType = 0;
                $scope.newProduct = null;
            } else {
                $scope.newType = 1;
                $scope.newPlan = null;
            }
            $scope.prodConfirmStartWaitingConfirmation = true;
        };

        $scope.prodConfirmStart = function () {
            $scope.prodConfirmStartWaitingConfirmation = false;
            if (!$scope.newProduct && !$scope.newPlan) {
                //$scope.newPlan = $scope.plans[0];
                return;
            }
            $scope.stateLoading = true;

            var idToSend = 0;
            if($scope.newType == 0) {
                idToSend = $scope.newPlan.id;
            }
            if($scope.newType == 1) {
                idToSend = $scope.newProduct.Product.id;
            }
            $http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'start-prod/'+ + idToSend, {params: {dateTime: $scope.event.dateTime, type:$scope.newType, additionalData: $scope.additionalPlanData}}).success(function (data) {
				$scope.additionalPlanData = {};
            	modals.startPlan.hide();
                inDialog = false;
                if(data !== null && data.hasOwnProperty('errors')){
            		$scope.additionalFieldsErrors = data.errors;
                }else{
                	$scope.additionalFieldsErrors = [];
                }
                $scope.stateLoading = false;
                updateStats();
            }).error(function () {
                $scope.stateLoading = false;
                modals.startPlan.hide();
                inDialog = false;
                updateStats();
            });
        };
		$scope.prodContinue = function() {
			$scope.stateLoading = false;
			modals.endPlan.hide();
			inDialog = false;
			updateStats();
		};

        $scope.prodPostphoneCountdownConfirm = function() {
            $scope.orderPostphoneCountdown = 1;

        };

        $scope.prodPostphoneCountdown = function (fake, waitFake) {
            $scope.orderEndTime = $scope.serverTime;
            $scope.orderPostphoneCountdown = 3;

            $scope.orderPostphoneCountdown = 2;
            var timePromise = getTime();
            timePromise.then(function () {
                $scope.startEndProductionDate = $scope.serverTime;
            });
            $scope.counter = 3;
            $scope.timeout = function () {
                $scope.counter--;
                if ($scope.counter > 0) {
                    myTimeout = $timeout($scope.timeout, 1000);
                } else {
                    var timePromise = getTime();
                    timePromise.then(function () {
                        $scope.orderEndTime = $scope.serverTime;
                        $scope.orderPostphoneCountdown = 3;
                    });

                    //$scope.prodPostphone(fake,waitFake);
                }
            };

            var myTimeout = $timeout($scope.timeout, 1000);
        }

		$scope.prodPostphone = function(fake, waitFake,problemId) {
			console.log("plano atidejimas");
			if (fake && waitFake) {
				$scope.waitForFakePostphone = true;
				$scope.lockScreen(true);
				return;
			} else {
				$scope.waitForFakePostphone = false;
			}
			$scope.stateLoading = true;
			var params = {};
			if (fake) {
				params.dateTime = $scope.currTime.toISOString();
				params.fake = 1;
			} else {
                params.startEndProductionDate = $scope.startEndProductionDate;
				params.dateTime = $scope.orderEndTime;
			}
            if(problemId) {
                params.problemId = problemId;
            }

			$http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'postphone-prod', {params: params}).success(function(data) {
				$scope.stateLoading = false;
				modals.endPlan.hide();
				inDialog = false;
				updateStats();
			}).error(function() {
				$scope.stateLoading = false;
				modals.endPlan.hide();
				inDialog = false;
				updateStats();
			});
            $scope.orderPostphoneCountdown = 0;
		};
		$scope.prodComplete = function() {
            $scope.orderEndCountdown = 0;
            $scope.orderEndVar = 0;
			if ($scope.lossTypes.length || $scope.sensor.time_interval_declare_count_on_finish) {
				$scope.stopProdState = 2;
			} else {
				$scope.stopProdState = 3;
			}
		};
		
		
		$scope.nextProdComplete = function(partialQuantityFinish) {
            $scope.partialQuantityFinish = partialQuantityFinish;
			if ($scope.endDlg.quickSwap) {
				$scope.sendProdComplete(1, false);
			} else {
				$scope.stopProdState = 3;
			}
		};
		$scope.sendProdComplete = function(problemId, promptProductionWindow) {
			$scope.promptProductionWindow = promptProductionWindow;
			if(problemId == 1 && parseInt($scope.transitionProblemTypes.length) > 0){
            	$scope.callProdComplete = true;
            	$scope.prodRegisterIssue($scope.transitionProblemTypes);
                return;
            }
			$scope.stateLoading = true; 
			var params = {dateTime: $scope.serverTime};
            params.startEndProductionDate = $scope.serverTime;
			//var params = {dateTime: $scope.event.dateTime};
			if (problemId) params['problemId'] = problemId;
			if ($scope.endDlg.quickSwap) params['quickSwap'] = 1;
			if($scope.partialQuantityFinish) {
				params['declaredQuantity'] = $scope.partialQuantityFinish;
			}
			if($scope.partialQuantityPiecesFinish) {
				params['declaredQuantityPieces'] = $scope.partialQuantityPiecesFinish;
			}
			if ($scope.lossTypes) {
				for (var k in $scope.lossTypes) {
					params['lt_' + $scope.lossTypes[k].LossType.id] = $scope.lossTypes[k].LossType.value;
				}
			}
			$http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'complete-prod', {params: params}).success(function(data) { 
				$scope.partialQuantityFinish = '';
				//$scope.stateLoading = false;
				$scope.callProdComplete = false;
				modals.endPlan.hide(); // IMPORTANT: uncommented
				inDialog = false;
				updateStats();
				if($scope.promptProductionWindow){
					$scope.event.dateTime = $scope.serverTime;
					$scope.startProductionProc();
				}
			}).error(function() {
				$scope.stateLoading = false; 
				modals.endPlan.hide();
				inDialog = false;
				updateStats();
			});
			$scope.stopProdState = 0;
		};
		$scope.prodRegisterIssue = function(subProblems) {
            if(typeof subProblems === 'undefined') {
            	$scope.parentProblemTypes = []; 
            	$scope.downtimeCutType=$scope.downtimeCutDuration=0;
                $scope.workingProblemTypes = $scope.problemTypes;
            } else {
                $scope.workingProblemTypes = subProblems;
			}
            $(".modal-dialog").addClass("widen-modal");
			$scope.stopProdState = 1;
		};
		$scope.prodRegisterIssueAfterProdEnd = function() {
			$scope.workingProblemTypes = $scope.problemTypes;
            $scope.callProdComplete = true;
			$scope.stopProdState = 1;
			$(".modal-dialog").addClass("widen-modal");
		};
        $scope.orderEnd = function() {
            if($scope.orderEndVar != 1) {
                $scope.orderEndVar = 1;
            } else {
                $scope.orderEndVar = 0;
            }
        };
        $scope.orderEndNo = function() {
            $scope.orderPostphoneCountdown = 0;
            $scope.orderEndVar = 0;
        };
        $scope.orderEndYes = function () {
        	if($scope.verificationOnProductionEnd){
        		modals.verification.show();  
        	}
            $scope.orderEndCountdown = 1;
            var timePromise = getTime();
            timePromise.then(function () {
                $scope.startEndProductionDate = $scope.serverTime;
            });
            $scope.counter = 3;//TODO greazinti sekundes i 15
            $scope.timeout = function () {
                $scope.counter--;
                if ($scope.counter > 0) {
                    myTimeout = $timeout($scope.timeout, 1000);
                } else {
                    var timePromise = getTime();
                    timePromise.then(function () {
                        $scope.orderEndTime = $scope.serverTime;
                        $scope.prodComplete();
                    });
                }
            };
            var myTimeout = $timeout($scope.timeout, 1000);

            $scope.stop = function () {
                $scope.counter = 3;
                $timeout.cancel(myTimeout);
            }
        }; 
        $scope.orderEndCountdownStop = function() {
            $scope.orderEndVar = 0;
            $scope.stop();
        };
		$scope.calculateDowntimesAfterCut = function(minutesCount) {
			$scope.downtimeCutDuration = $scope.downtimeCutDuration.toString().replace(',','.');
			if($scope.downtimeCutDuration.substr(-1) == '.'){
				return null;
			}
			$scope.downtimeCutDuration = parseFloat($scope.downtimeCutDuration);
			if(!$scope.downtimeCutDuration){
				$scope.endDlg.event.foundProblem.oldStart = $scope.endDlg.event.foundProblem.start;
				$scope.endDlg.event.foundProblem.oldEnd = $scope.endDlg.event.foundProblem.end;
				$scope.endDlg.event.foundProblem.newStart = $scope.endDlg.event.foundProblem.newEnd = '';
				$scope.endDlg.event.foundProblem.newDuration = 0;
				return false; 
			}
			if($scope.downtimeCutType == 0){//kerpama nuo pradzios
				 var dt = new Date(new Date($scope.endDlg.event.foundProblem.start).getTime() + 1000 + $scope.downtimeCutDuration * 60000);
				 var dt2 = new Date(new Date($scope.endDlg.event.foundProblem.start).getTime() + $scope.downtimeCutDuration * 60000);
				 $scope.endDlg.event.foundProblem.oldStart = dt.getFullYear()+'-'+("0"+(dt.getMonth()+1)).slice(-2)+'-'+("0"+(dt.getDate())).slice(-2)+' '+("0"+(dt.getHours())).slice(-2)+':'+("0"+(dt.getMinutes())).slice(-2)+':'+("0"+(dt.getSeconds())).slice(-2);
				 $scope.endDlg.event.foundProblem.oldEnd = $scope.endDlg.event.foundProblem.end;
				 $scope.endDlg.event.foundProblem.newStart = $scope.endDlg.event.foundProblem.start;
				 $scope.endDlg.event.foundProblem.newEnd = dt2.getFullYear()+'-'+("0"+(dt2.getMonth()+1)).slice(-2)+'-'+("0"+(dt2.getDate())).slice(-2)+' '+("0"+(dt2.getHours())).slice(-2)+':'+("0"+(dt2.getMinutes())).slice(-2)+':'+("0"+(dt2.getSeconds())).slice(-2);
			}else{//kerpama nup pabaigos
				 var dt2 = new Date(new Date($scope.endDlg.event.foundProblem.end).getTime() - 1000 - $scope.downtimeCutDuration * 60000);
			     var dt = new Date(new Date($scope.endDlg.event.foundProblem.end).getTime() - $scope.downtimeCutDuration * 60000);
				 $scope.endDlg.event.foundProblem.newStart = dt.getFullYear()+'-'+("0"+(dt.getMonth()+1)).slice(-2)+'-'+("0"+(dt.getDate())).slice(-2)+' '+("0"+(dt.getHours())).slice(-2)+':'+("0"+(dt.getMinutes())).slice(-2)+':'+("0"+(dt.getSeconds())).slice(-2);
				 $scope.endDlg.event.foundProblem.newEnd = $scope.endDlg.event.foundProblem.end;
				 $scope.endDlg.event.foundProblem.oldStart = $scope.endDlg.event.foundProblem.start;
				 $scope.endDlg.event.foundProblem.oldEnd = dt2.getFullYear()+'-'+("0"+(dt2.getMonth()+1)).slice(-2)+'-'+("0"+(dt2.getDate())).slice(-2)+' '+("0"+(dt2.getHours())).slice(-2)+':'+("0"+(dt2.getMinutes())).slice(-2)+':'+("0"+(dt2.getSeconds())).slice(-2);
			}
			var currentDowntimeDuration = Math.round((new Date($scope.endDlg.event.foundProblem.oldEnd).getTime() - new Date($scope.endDlg.event.foundProblem.oldStart).getTime())/1000);
			if(currentDowntimeDuration < 0){
				$scope.downtimeCutDuration = $scope.downtimeCutDuration - Math.ceil(Math.abs(parseFloat(currentDowntimeDuration/60)));
				$scope.calculateDowntimesAfterCut();
			}
			if(currentDowntimeDuration > 0){
				$scope.endDlg.event.foundProblem.newDuration = ('0'+String(parseInt(currentDowntimeDuration / 3600))).slice(-2) + ':' + 
								('0'+String(parseInt(currentDowntimeDuration % 3600 / 60))).slice(-2) + ':' + 
								('0'+String(parseInt(currentDowntimeDuration % 3600 % 60))).slice(-2);
			}
		};
		$scope.sendRegisterIssue = function(problemType, promptProductionWindow) {
			$scope.promptProductionWindow = promptProductionWindow;
			if(!problemType || problemType.Problem.id != 1) {
				$scope.parentProblemTypes.push($scope.workingProblemTypes);
			}else{
				$scope.parentProblemTypes = [];
			}
            if(parseInt(problemType.children.length) > 0) {
                $scope.prodRegisterIssue(problemType.children);
                return;
            }    
            if(problemType.Problem.id == 1 && parseInt($scope.transitionProblemTypes.length) > 0){
            	$scope.prodRegisterIssue($scope.transitionProblemTypes);
                return;
            }
            //$scope.endDlg = {
            //    currentPlan: (('currentPlan' in opts) ? opts.currentPlan : $scope.currentPlan),
            //    event: (('event' in opts) ? opts.event : $scope.event),
            //    autoEvent: (opts.autoEvent ? true : false),
            //    quickSwap: (opts.quickSwap ? true : false)
            //};
            //if(!problemType.children.length && problemType.Problem.id > 0 && $scope.texts.confirmProblemChoose.length){
	        //    var conf = confirm($scope.texts.confirmProblemChoose);
	        //    if(!conf) return false;
	        //} 
	        if($scope.callProdComplete){
            	$scope.sendProdComplete(problemType.Problem.id, $scope.promptProductionWindow);
            	return false;
			}
            $(".modal-dialog").removeClass("widen-modal");
			$scope.stateLoading = true;
            var params = {
                dateTime: $scope.endDlg.event.dateTime, //$scope.event.dateTime,
                foundProblemId: $scope.endDlg.event.foundProblemId,
                problemComment: $scope.problemComment,
                downtimeCutType: $scope.downtimeCutType,
                downtimeCutDuration: $scope.downtimeCutDuration,
            };
            $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-issue/' + (problemType ? problemType.Problem.id : 0), {params: params}).success(function (data) {
                $scope.problemComment = '';
                $scope.stateLoading = false;
                modals.endPlan.hide();
                inDialog = false;
                updateStats();
                if($scope.promptProductionWindow && !$scope.endDlg.currentPlan && !$scope.endDlg.event.foundProblemId){
					$scope.event.dateTime = $scope.serverTime;
					$scope.startProductionProc();
				}
            }).error(function () {
                $scope.stateLoading = false;
                modals.endPlan.hide();
                inDialog = false;
                updateStats();
            });
            $scope.stopProdState = 0;
		};
		$scope.backProdEnd = function() {
            $(".modal-dialog").removeClass("widen-modal");
            if($scope.parentProblemTypes.length > 0){
            	$scope.prodRegisterIssue($scope.parentProblemTypes.slice(-1)[0]);
            	$scope.parentProblemTypes.splice(-1,1);
            }else{
				$scope.stopProdState = 0;
			}
		};
		$scope.idleContinue = function() {
			modals.startPlan.hide();
			inDialog = false;
            $scope.prodConfirmStartWaitingConfirmation = false;
			updateStats();
		};
		$scope.idleIssue = function() {
			modals.endPlan.hide();
			inDialog = false;
			updateStats();
		};
		$scope.forceUpdateStatus = function(selTime) {
			if (selTime) $scope.selTime = selTime;
			updateStats();
		};
		$scope.startRegisterSpeedIssue = function() {
			modals.speedProblem.show();
			//inDialog = true;
			//$scope.stateLoading = false;
		};
        $scope.sendRegisterSpeedIssue = function (problemType) {
            $scope.stateLoading = true;
            var params = {dateTime: $scope.event.dateTime};
            //var params = {dateTime: $scope.serverTime};
            $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-speed-issue/' + (problemType ? problemType.Problem.id : 0), {params: params}).success(function (data) {
                $scope.stateLoading = false;
                modals.speedProblem.hide();
                inDialog = false;
                updateStats();
            }).error(function () {
                $scope.stateLoading = false;
                modals.speedProblem.hide();
                inDialog = false;
                updateStats();
            });

        };

        $scope.shouldShow = function(wc) {
            return $scope.workCenter !== null && $scope.workCenter.id == wc;
        };

        $scope.toggleR1 = function() {
            //
        };
        $scope.toggleR1Confirm = function() {
            if($scope.toggle_r1) {
                $scope.toggle_r1Text = $scope.toggleR1Text_off;
            } else {
                $scope.toggle_r1Text = $scope.toggleR1Text_on;
            }
            $scope.toggle_r1 = !$scope.toggle_r1;
            updateStats();
        };

        $scope.declareQuantities = function() {
            modals.declareQuantities.show();
            inDialog = true;
        };
        $scope.saveDeclaredQuantities = function(foundProblemId) {
            var params = [];
            params['declaredQuantity'] = $scope.partialQuantity;
            params['declaredQuantityPieces'] = $scope.partialQuantityPieces;
            $scope.partialQuantity = '';
            $scope.partialQuantityPieces = '';
            if ($scope.lossTypes) {
                for (var k in $scope.lossTypes) {
                    params['lt_' + $scope.lossTypes[k].LossType.id] = $scope.lossTypes[k].LossType.value;
                }
            }
            if(foundProblemId){
            	params['foundProblemId'] = foundProblemId;
			}
            $scope.stateLoading = true;

            $http.get(window.baseUri + 'work-center/'+ (window.baseUriDc ? (window.baseUriDc + '/') : '') +'declareQuantities', {params: params}).success(function(data) {
                $scope.stateLoading = false;
                modals.declareQuantities.hide();
                inDialog = false;
				$scope.closeInformationModal();
                updateStats();
            }).error(function() {
                $scope.stateLoading = false;
                modals.declareQuantities.hide();
                inDialog = false;
                updateStats();
            });
        };
        $scope.hideQuantitiesModal = function() {
            modals.declareQuantities.hide();
            inDialog = false;
            updateStats();
        };
        $scope.registerVerification = function() {
            modals.verification.hide();
            $http.get(window.baseUri + 'work-center/registerVerification', {params: {}}).success(function(data) {
                if(!$scope.orderEndVar){
                	updateStats();
                	inDialog = false;
               	}
            }).error(function() {
                modals.verification.hide();
                alert('Problema su serveriu. Susisiekite su sistemos administratoriumi');
            });
        };

		// note: http interval 20s
        var getTime = function() {
			let deferred = $q.defer(); // help: defer()?
			$scope.updateInProgress = true;
            $http.get(window.baseUri + 'time.php').success(function(data) {
                $scope.serverTime = data.time; // date string
				$scope.serverUnix = data.unix; // unix timestamp
				deferred.resolve("request successful");
				
				//if($scope.selTime == null){ $scope.calendar_input_str = data.time.substr(0,16); }
            });
            return deferred.promise;
		};
		
        var updateStats = function () {
            //if(updateInProgress) {
            //    return;
            //}
            var timePromise = getTime();
            timePromise.then(function () {
                if (timeoutInst) {
                    clearTimeout(timeoutInst);
                    timeoutInst = null;
                }
                if (inDialog) return;
                var params = {};
                if (window.baseUriDc && $scope.selTime) {
                	var datestring = ($scope.selTime.getFullYear()) + "-" + ("0"+($scope.selTime.getMonth()+1)).slice(-2) + "-" + ("0" +($scope.selTime.getDate())).slice(-2) + "T" + ("0" + $scope.selTime.getHours()).slice(-2) + ":" + ("0" + $scope.selTime.getMinutes()).slice(-2) + ":" + ("0" + $scope.selTime.getSeconds()).slice(-2);
					// console.log("selected: ", datestring);
					params.time = datestring;
					
                } else {
					// console.log("current: ", $scope.serverTime);
					params.time = $scope.serverTime;
                }
                params.graphHoursCount = $scope.graphHoursCount ;
				$scope.updateInProgress = true;
				
				// Note: http interval 20s
                $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'update-status', {params: params}).success(function (data) {

					if((typeof data === 'string' || data instanceof String) && data.substr(0,23) === `<pre class="cake-error"`){
						console.error(data);
						alert("Serverio klaida");
						// document.write(data);
						$scope.updateInProgress = false;
						$scope.selTime = null;
						repeatInterval(); 

						return;
					}

					var k;
                    $scope.stateLoading = false;
					// modals.endPlan.hide(); // todo: show modal
                    if ('lost_connection' in data) {
                        $scope.lost_connection = true;
                        $scope.hours = [];
                        $scope.sensor = [];
                        //return false;
                    }else{ $scope.lost_connection = false; }
                    if ('availableSensors' in data) {
                        $scope.availableSensors = data.availableSensors;
                    }
                    if ('workcenterHoursInfoBlocks' in data) {
						// console.log(data.workcenterHoursInfoBlocks);
                        $scope.workcenterHoursInfoBlocks = data.workcenterHoursInfoBlocks;
                    }
                    if ('accessesToMethods' in data) {
                        $scope.accessesToMethods = data.accessesToMethods;
                    }
                    if ('prevShiftsOEE' in data) {
                        $scope.prevShiftsOEE = data.prevShiftsOEE;
                    }
                    if ('settings' in data) {
                        $scope.settings = data.settings;
                    }
                    if ('hours' in data) {
						$scope.hours = data.hours;
						$scope.graphHoursCount = data.hours.length; // edit: set hours for [timeArrows] to move correctly
                    }
                    if ('workCenter' in data) {
                        $scope.workCenter = data.workCenter;
                    }
                    if ('currentPlan' in data) {
						$scope.currentPlan = data.currentPlan;
                    }
                    if ('foundProblem' in data) {
                        $scope.foundProblem = data.foundProblem;
                        if ($scope.foundProblem) {
                            $scope.lockScreen(false);
                            if ($scope.waitForFakePostphone) {
                                $scope.prodPostphone(true, false);
                            }
                        }
                    }
                    if('currentShift' in data){
                    	$scope.currentShift = data.currentShift;
                    }
                    if('historyShift' in data){
                    	$scope.historyShift = data.historyShift;
                    }
                    if('texts' in data){
                    	$scope.texts = data.texts;
                    }
                    
                    if ('line' in data) {
                        $scope.line = data.line;
                    }
                    if ('event' in data) {
                        $scope.event = data.event;
                        //$scope.event.ts = (new Date()).getTime();
                        $scope.event.ts = $scope.serverUnix;
                    }
                    if ('plans' in data) {
                        $scope.plans = data.plans;
                        var lid = $scope.newPlan ? $scope.newPlan.id : null;
                        $scope.newPlan = null;
                        for (var k in $scope.plans) {
                            if (!lid || lid === $scope.plans[k].id) {
                                //$scope.newPlan = $scope.plans[k];
                                break;
                            }
                        }
                    }
                    if ('allPlans' in data) {
                        $scope.allPlans = data.allPlans;
                    }
                    if ('orderPartialQuantity' in data) {
                        $scope.orderPartialQuantity = data.orderPartialQuantity;
                        $scope.orderPartialQuantityPieces = data.orderPartialQuantityPieces;
                    }
                    if ('partialQuantity' in data) {
                        $scope.partialQuantity = data.partialQuantity;
                        $scope.partialQuantityPieces = data.partialQuantityPieces;
                    }
                    if ('orderLossesQuantities' in data) {
                        $scope.orderLossesQuantities = data.orderLossesQuantities;
                        $scope.totalOrderLossesQuantities = 0;
                        for(var i in $scope.orderLossesQuantities){
	                    	$scope.totalOrderLossesQuantities += parseFloat($scope.orderLossesQuantities[i]);
	                    }
                    }
                    if ('products' in data) {
                        $scope.products = data.products;
                    }
                    if ('problemTypes' in data) {
                        $scope.problemTypes.splice(0, $scope.problemTypes.length);
                        $scope.speedProblemTypes.splice(0, $scope.speedProblemTypes.length);
                        for (var k in data.problemTypes) {
                            if (data.problemTypes[k].Problem.for_speed) {
                                $scope.speedProblemTypes.push(data.problemTypes[k]);
                            } else {
                                $scope.problemTypes.push(data.problemTypes[k]);
                            }
                        }
                    }
                    if ('transitionProblemTypes' in data) {
                        $scope.transitionProblemTypes.splice(0, $scope.transitionProblemTypes.length);
                        for (var k in data.transitionProblemTypes) {
                             $scope.transitionProblemTypes.push(data.transitionProblemTypes[k]);
                        }
                    }
                    if ('zeroTimeTransitionProblemTypes' in data) {
                        $scope.zeroTimeTransitionProblemTypes.splice(0, $scope.zeroTimeTransitionProblemTypes.length);
                        for (var k in data.zeroTimeTransitionProblemTypes) {
                             $scope.zeroTimeTransitionProblemTypes.push(data.zeroTimeTransitionProblemTypes[k]);
                        }
                    }
                    if ('lossTypes' in data) {
                        $scope.lossTypes = data.lossTypes;
                        for (k in $scope.lossTypes) {
                            $scope.lossTypes[k].LossType.value = '';
                        }
                    }
                    if ('time' in data) {
                        $scope.currTime = new Date(data.time);
                    }
                    if (('helpUrl' in data) && data.helpUrl) {
                        $scope.helpUrl = data.helpUrl;
                    }
                    if ('OEE' in data) {
                        $scope.OEE = data.OEE;
                    }
                    if ('lastMinuteSpeed' in data) {
                        $scope.lastMinuteSpeed = data.lastMinuteSpeed;
                    }

                    if ('dziuv_plans' in data) {
                        $scope.view_r1 = data.dziuv_plans;
                    }
                    if('sensorType' in data) {
                        $scope.sensorType = data.sensorType;
                    }
                    if('sensor' in data) {
                        $scope.sensor = data.sensor;
                    }
                    if('waitingDiffCardId' in data) {
                        $scope.waitingDiffCardId = data.waitingDiffCardId;
                    }
                    if('verification' in data && data.verification != null) {
                    	$scope.verification = data.verification;
                    	$scope.verificationOnProductionEnd = data.verification.verificationOnProductionEnd;
                    	if(data.verification.promptVerification) modals.verification.show();
                    	if(data.verification.countdown){
                    		$scope.verificationCountdown = data.verification.countdown;
                    	}else{$scope.verificationCountdown = '';}
                    }
                    if('mainPluginTitle' in data) {
                        $scope.mainPluginTitle = data.mainPluginTitle;
					}

					$scope.new_records = data["newRecords"] ?? [];
					$scope.hidden_buttons = data["hiddenButtons"] ?? [];
					// for(let rec of $scope.new_records) rec.qty = Math.floor(Math.random()*100);

					$scope.oeeGaugeTitle = data.oeeGaugeTitle;
					
					// time changes
					if($scope.selTime){
						let picker = document.querySelector("#date-picker-input");
						if(picker) picker.value = $scope.formatDateStr( new Date(new Date($scope.selTime).setMinutes(60)  ) );
					}else{
						let picker = document.querySelector("#date-picker-input");
						if(picker) picker.value = $scope.formatDateStr(new Date(new Date($scope.serverTime).setMinutes(60) - $scope.graphHoursCount * 3600_000));
					}

                }).error(function () {
                    $scope.hours = [
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []},
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []},
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []}
                    ];
				}).then(function(){
					$scope.hideForeground();
					$scope.updateInProgress = false;
					$scope.drawTimeline();
					$scope.configureSpeedometer();
					$scope.verifyToolbarBtnsVisibility();
					repeatInterval();        
				});
            });
        }, timeoutInst = null, repeatInterval = function () {
            if (!timeoutInst) {
                timeoutInst = setTimeout(updateStats, 20000);
            }
        };
        updateStats();
	}]);

	app.filter('productsFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText.length == 0) {
                return items;
            }
            angular.forEach(items, function(item) {
                if(item.Product.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0 || item.Product.production_code.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }]);
    app.filter('plansFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText.length == 0) {
                return items;
            }
            angular.forEach(items, function(item) {
                if(item.Plan.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }]);
    app.filter('additionalFieldsFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText != undefined){
            	if(searchText.length == 0) {
	                return items;
	            }
	            angular.forEach(items, function(item) {
	                if(typeof item === 'string' && item.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
	                    filtered.push(item);
	                }
	            });
	            return filtered;
            }else return items;
        };
    }]);
})(angular);
setInterval(function(){location.reload();},43_200_000); // reload every 12h


// EDIT: temporary fix, remove modal widen on every modal close
$('.modal').on('hidden.bs.modal', function (e) {
	$(".modal-dialog").removeClass("widen-modal");
})