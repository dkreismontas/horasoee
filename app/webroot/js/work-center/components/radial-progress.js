// useful: 
// https://javascript.info/custom-elements
// https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_shadow_DOM
// https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/preserveAspectRatio
//------------------------------------------------------------------------
// radial-progress

class RadialProgress extends HTMLElement {

    body = () => /*html*/`
        <style>
            svg:not(:root) {
                width: 100%;
                height: 100%;
            }

            #progress{
                font-size: 30px;
                font-family: 'Montserrat', sans-serif;
                font-weight: 700;
            }

            #text{
                font-size: 16px;
                font-weight: bold;
                text-transform: uppercase;
            }
        </style>

        <svg viewBox = "0 0 100 100"> 
            <defs>
                <linearGradient id="grad" x1="0" y1="0" x2="1" y2="1">
                    <stop offset="0.3" stop-color="#000"/>
                    <stop offset="1.0" stop-color="#FFF" />
                </linearGradient>
            </defs>

            <circle
                transform="rotate(-90,50,50)"
                stroke="#7D7D7D"
                stroke-width="5"
                fill="transparent"
                r="44" cx="50" cy="50"/>

            <circle
                transform="rotate(-90,50,50)"
                class="progress-ring"
                stroke="#000000"
                stroke-width="11"
                fill="transparent"
                r="44" cx="50" cy="50"/>

            <text x="50" y="46" dominant-baseline="middle" text-anchor="middle" id="progress">000</text> 
            <text x="50" y="68" dominant-baseline="middle" text-anchor="middle" id="text">kiekis</text> 
        </svg>
    `
    
    constructor() {
        super();
    }

    static get observedAttributes() {
        return ["value", "text", "color", "t0", "t1"];
    }

    set t0(val){ this.setAttribute("t0", val); }
    set t1(val){ this.setAttribute("t1", val); }
    get t0(){ return parseFloat(this.getAttribute("t0")?? 33); }
    get t1(){ return parseFloat(this.getAttribute("t1")?? 66); }

    set value(val){ this.setAttribute("value", val); }
    get value(){ return parseFloat(this.getAttribute("value")?? 0); }

    render(){
        let ring = this.shadowRoot.querySelector('.progress-ring');
        let radius = ring.r.baseVal.value;
        let circumference = radius * 2 * Math.PI;
        let offset = circumference - this.value / 100 * circumference;

        ring.style.strokeDashoffset = offset;
        ring.style.strokeDasharray = `${circumference} ${circumference}`;
        ring.style.stroke = this.color;
        
        this.shadowRoot.querySelector('#text').innerHTML = this.text;
        this.shadowRoot.querySelector('#progress').innerHTML = this.value;
        this.shadowRoot.querySelector('#progress').setAttribute("fill",this.color);
    }

    clamp = (num, min, max) => num <= min ? min : num >= max ? max : num;

    attributeChangedCallback(name, old_val, new_val) { 
        if(this.shadowRoot == null) return;
        this.render(); 
    }

    get color(){ 
        if(this.value < this.t0) return "#c02b2b";
        if(this.value < this.t1) return "#ff9a33";
        return "#059b66";
    }
    get text(){ return this.getAttribute("text")?? ""; }

    connectedCallback() {
        this.attachShadow({mode: 'open'});
        this.shadowRoot.innerHTML = this.body();
        this.classList.add("w-100","d-flex", "justify-content-center", "align-items-center");
        this.render();
    }
}


window.customElements.define('radial-progress', RadialProgress);