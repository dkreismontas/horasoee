
class BarGraph extends HTMLElement {
    //------------------------
    // fields
    
    _data = [];
    _colors = {
        red:    "linear-gradient(#cc3333, #7f0101)",
        yellow: "linear-gradient(#fbdb0e, #fbdb0e, #ff9933)",
        green:  "linear-gradient(#009966, #66cc66)",
        neutral:"linear-gradient(#888, #444)",
    }

    //------------------------
    // attriutes

    static get observedAttributes() {
        return ["min", "max", "value"];
    }

    // attributeChangedCallback(name, old_val, new_val) { 
    //     if(this.shadowRoot == null) return;
    // }

    set t0(val){ this.setAttribute("t0", val); }
    set t1(val){ this.setAttribute("t1", val); }
    get t0(){ return parseInt(this.getAttribute("t0")?? 33); }
    get t1(){ return parseInt(this.getAttribute("t1")?? 66); }

    set min(val){ this.setAttribute("min", val); }
    set max(val){ this.setAttribute("max", val); }
    get min(){ return this.getAttribute("min")??   0; }
    get max(){ return this.getAttribute("max")?? 100; }

    set length(val){ this.setAttribute("length", val); }
    get length(){ return this.getAttribute("length")?? 30; }

    //------------------------
    // graph methods

    recalculateHeights(){
        this.max = Math.max(...this._data);
        
        let columns = this.querySelectorAll("#alive > #bar");

        columns.forEach((e, i) => {
            let val = this._data[i];
            e.style.height = `${val / this.max * 100}%`;
            // e.style.background = val < this.t0? this._colors.red : ( val < this.t1? this._colors.yellow : this._colors.green);
        });

        this.querySelector("#n0").textContent = 0
        this.querySelector("#n1").textContent = Math.floor(this.max / 4 * 1);
        this.querySelector("#n2").textContent = Math.floor(this.max / 4 * 2);
        this.querySelector("#n3").textContent = Math.floor(this.max / 4 * 3);
        this.querySelector("#n4").textContent = this.max;
    }

    async push(x, y){
        // create
        let div = this.createColumn(y, x);
        this._data.push(y);
        this.querySelector("#graph-container").appendChild(div);
        this.recalculateHeights();
        
        // animate
        div.style.width = "0px";
        await new Promise(r => setTimeout(r, 50)); // wait for element to be rendered
        div.style.width = "100%";
        
        // remove
        await this.remove(0);
    }

    async remove(){
        if(this._data.length > this.length) this._data.shift();

        let div = this.querySelector("#alive");
        div.id = "dying";
        div.style.width = "0px";
        div.style.margin = "0px";

        let text = div.querySelector("#text");
        text.innerText = "";
        
        await new Promise(r => setTimeout(r, 500));
        div.id = "dead";
        this.querySelectorAll("#dead").forEach(e => e.remove());
    }

    createColumn(val = 0, time = "-:-", color){
        let col = document.createElement("div");
        col.id = "alive";
        col.style.background = "transparent";//"#5D5D5D";
        col.style.height = "100%";
        col.style.width = "100%";

        let bar = document.createElement("div");
        bar.id = "bar";
        bar.style.height = `${val / this.max * 100}%`;
        bar.style.width = "100%";
        bar.style.minHeight = "1px";
        // bar.style.background = `hsl(${ val / this._max * 120 }, 100%, 50%)`;
        bar.style.background = color ?? (val < this.t0? this._colors.red : ( val < this.t1? this._colors.yellow : this._colors.green));
        
        let text = document.createElement("div");
        text.id = "text";
        text.style.position = "absolute";
        text.style.bottom = "calc(-0.5em - 1px - 1em - 0.5em)"; // bottom padding - border height - bottom margin
        text.innerText = time.split(":")[1] ?? "-";
        
        col.appendChild(bar);
        col.appendChild(text);

        // popover
        col.setAttribute("data-toggle", "popover");
        col.setAttribute("data-placement", "bottom");  

        // <div class="d-flex mt-1">
        //     <div class="text-nowrap"> Kiekis: ${ val }</div>
        //     <div class="w-100"></div>
        //     <div class="text-nowrap"> Laikas: ${ time }</div>
        // </div>

        let hh = isNaN(parseInt(time.split(":")[0]))? 0 : time.split(":")[0];
        let mm = isNaN(parseInt(time.split(":")[1]))? 0 : time.split(":")[1];
        let to = new Date( new Date().setHours( hh, parseInt(mm)+1 ))

        new bootstrap.Popover(col,{
            trigger:"hover",
            html: true,
            sanitize: false,
            template: /*html*/`
                <div class="popover rounded-0 text-dark" role="tooltip">
                    <div class="popover-arrow"></div>
                    <h3 class="popover-header"></h3>
                    <div class="popover-body p-2 bg-white lh-sm"></div>
                </div>
            `,
            content: /*html*/`
         
                <b>Paskutinių 30 min. duomenys</b>

                <div class="pt-1"></div>

                <div class="text-nowrap"> Kiekis: ${ val } vnt.</div>
                <div class="text-nowrap"> Laikas: ${ time }-${ to.getHours().toString().padStart(2,0) + ":" + to.getMinutes().toString().padStart(2,0) }</div>

                <div class="pt-3"></div>

                <b>Greičio spalvos</b>
                <table class="mt-1 w-100">

                    <tr class="w-100">
                        <td><div style="background: ${this._colors.red}" class="w-2-em mr-1"><wbr></div></td>
                        <td><div class="w-100">Žemas</div></td>
                        <td><div class="pl-1 text-center">${ 0 + "-" + (this.t0-1) }</div></td>
                    </tr>
                    <tr>
                        <td><div style="background: ${this._colors.yellow}" class="w-2-em mr-1"><wbr></div></td>
                        <td><div class="w-100">Patenkinamas</div></td>
                        <td><div class="pl-1 text-center">${ this.t0 + "-" + (this.t1-1) }</div></td>
                    </tr>
                    <tr>
                        <td><div style="background: ${this._colors.green}" class="w-2-em mr-1"><wbr></div></td>
                        <td><div class="w-100">Geras</div></td>
                        <td><div class="pl-1 text-center">${ this.t1 + "-" + Math.max(this.t1, this.max) }</div></td>
                    </tr>
                </table>
            `,
        })

        return col;
    }

    generateGraph(count){
        for(let i = 0; i < count; i++){
            this.querySelector("#graph-container").appendChild( this.createColumn( ) );
            this._data.push();
        }
    }

    setDataFromRecords(records){
        let container = this.querySelector("#graph-container");
        container.innerHTML = "";
        this._data = [];

        for(let i = records.length-1; i >= 0; i--){
            let r = records[i];
            // container.appendChild(this.createColumn( parseInt(r.qty), r.min ))
            this._data.push(r.qty);
        }

        this.recalculateHeights();

        for(let i = records.length-1; i >= 0; i--){
            let r = records[i];
            let c = this._colors.neutral;
            let qty = parseInt(r.qty ?? 0);

            if(r.t0 && r.t1) c = r.qty < r.t0? this._colors.red : ( qty < r.t1? this._colors.yellow : this._colors.green);
            container.appendChild(this.createColumn(r.qty, r.min, c))
        }
    }

    //------------------------
    // parent methods

    constructor() {
        super();
    }

    connectedCallback() {
        // this.attachShadow({mode: 'open'});
        this.style.display = "block";
        this.style.width = "100%";
        this.style.height = "100%";

        this.innerHTML = /*html*/`
            <style>
                #bg__table #dead, #bg__table #alive, #bg__table #dying{
                    width: 100%;
                    transition: all 300ms ease;
                    transition-property: width, height;
                    
                    display: flex;
                    align-items: flex-end;
                    justify-content: center;
                }

                #bg__table #alive{
                    border-left: 2px solid white;
                }

                #bg__table #alive:hover{
                    /* outline: 1px solid black; */
                    border: 1px solid black;
                    z-index: 2;
                }

                #bg__table{
                    line-height: 100%;
                    overflow: hidden;
                    margin-left: 2px;
                }

                #bg__table #graph-lines{ /* X & Y axes black lines */
                    height: 100%;
                    border-bottom: 1px solid black;
                    border-left: 1px solid black;
                }

                #bg__table #graph-background{
                    height: 100%;
                    width: 100%; 
                    display: flex; 
                    flex-flow: column;
                    justify-content: space-between;
                }

                #bg__table #graph-background > div:not(:last-child){
                    border-top: 0.1em dotted #5D5D5D; 
                    z-index: 1;
                }

                #bg__table #graph-container{
                    position: absolute;
                    display: flex;
                    align-items: flex-end;
                    background: transparent;
                }
            </style>

            <table style="width: 100%; height: 100%;" id="bg__table">
                <tr>
                    <td>
                        <div style="writing-mode: vertical-rl; transform:rotate(-180deg); width: 1em;"> Kiekis </div>
                    </td>
                    <td>
                        <div style="height: 100%; display: flex; flex-flow: column; align-items: flex-end; justify-content: space-between; padding: 2px">
                            <div id="n4">100</div>
                            <div id="n3"> 75</div>
                            <div id="n2"> 50</div>
                            <div id="n1"> 25</div>
                            <div id="n0">  0</div>
                        </div>
                    </td>

                    <td style="width: 100%; height: 100%;">

                        <div id="graph-lines">
             
                            <div style="position: relative; height: calc(100% - 0.5em); padding-top: 0.5em; left: 3px;">
                                <div id="graph-container" style="height: calc(100% - 0.5em); width: 100%;"></div>
                                <div id="graph-background">
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                    <div></div>
                                </div>
                            </div>
                     
                        </div>

                    </td>

                    <td></td>
                </tr>
                <tr>
                    <td style="height: 1.5em"></td>
                    <td></td>
                    <td></td>
                    <td class="text-center fw-bold">
                        <div style="padding-top: 0px; padding-left: 4px;" class="text-lowercase">
                            min
                        </div>
                    </td>
                </tr>
            </table>
        `;
      
        this.generateGraph(this.length);
    }
}

  
window.customElements.define('bar-graph', BarGraph);