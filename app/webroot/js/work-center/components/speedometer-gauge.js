//------------------------------------------------------------------------
// gauge
// https://stackoverflow.com/questions/30147879/how-to-draw-a-linear-gradient-circle-by-svg

/*
            <line x1="200" y1="99" x2="152" y2="99" stroke="#5d5d5d" stroke-width="3"/>
            <line x1="0" y1="99" x2="48" y2="99" stroke="#5d5d5d" stroke-width="3"/>

            <line id="line-t0" x1="2" y1="99" x2="46" y2="99" stroke="#5d5d5d" stroke-width="2" transform="rotate(59,100,100)"/>
            <line id="line-t1" x1="2" y1="99" x2="46" y2="99" stroke="#5d5d5d" stroke-width="2" transform="rotate(118,100,100)"/>
*/

class SpeedometerGauge extends HTMLElement {
    constructor() {
        super();
    }

    center = 280/2;
    body = () => /*html*/`
        <style>
            text{
                font-weight: bold;
                font-size: 20px;
                text-transform: uppercase;
            }

            .speed_number{
                font-size: 29px;
            }

            .speed_metric{
                font-size: 14px;
            }
        </style>
        <svg viewBox="0 0 ${this.center*2} 100" style="max-heigth: 100%; width: 100%;">
            <defs>
                <radialGradient id="grad-1">
                    <stop offset="0.4" stop-color="#810202"/>
                    <stop offset="1.0" stop-color="#c02b2b" />
                </radialGradient>

                <radialGradient id="grad-2">
                    <stop offset="0.4" stop-color="#ff9a33"/>
                    <stop offset="1.0" stop-color="#fbd511" />
                </radialGradient>

                <radialGradient id="grad-3">
                    <stop offset="0.4" stop-color="#059b66"/>
                    <stop offset="1.0" stop-color="#59c566" />
                </radialGradient>

                <radialGradient id="radial">
                    <stop offset="20%" stop-color="#EEEEEE" />
                    <stop offset="90%" stop-color="#b6b6b5" />
                </radialGradient>
            </defs>

            <circle r="85" cx="${this.center}" cy="100" fill="transparent" stroke="url(#grad-1)" stroke-width="30" id="ring-1" />
            <circle r="85" cx="${this.center}" cy="100" fill="transparent" stroke="url(#grad-2)" stroke-width="30" id="ring-2" />
            <circle r="85" cx="${this.center}" cy="100" fill="transparent" stroke="url(#grad-3)" stroke-width="30" id="ring-3" />

            <polygon 
                id="arrow"
                points=" ${this.center+12},92 ${this.center-12},92    ${this.center-1},16 ${this.center},15 ${this.center+1},16" 
                style="transition: transform 0.2s;" 
                transform="rotate(0,100,100)"
            />

            <circle
                stroke="#000000"
                fill="#FFFFFF"
                stroke-width="4"
                r="10" cx="${this.center}" cy="92"/>

            <circle r="57" cx="${this.center}" cy="100" fill="#5d5d5d" />

            <rect width="100%" height="200" y=100 fill="#FFF" />
            <line x1="0" y1="98" x2="100%" y2="98" stroke="#5d5d5d" stroke-width="4"/>


            <text x="20" y="92" dominant-baseline="auto" text-anchor="middle"  id="min">000</text>
            <text x="${this.center*2-20}" y="92" dominant-baseline="auto" text-anchor="middle" id="max">000</text> 

            <text x=${this.center} y="75" dominant-baseline="auto" text-anchor="middle" class="speed_number" fill="white" id="speed">2700</text> 
            <text x=${this.center} y="94" dominant-baseline="auto" text-anchor="middle" class="speed_metric" fill="white" id="units">units/min</text> 
        </svg>
    `

    //---------------------------------------------------------
    // attributes 

    set t0(val){ this.setAttribute("t0", val); }
    set t1(val){ this.setAttribute("t1", val); }
    get t0(){ return parseFloat(this.getAttribute("t0")?? 33); }
    get t1(){ return parseFloat(this.getAttribute("t1")?? 66); }

    set min(val){ this.setAttribute("min", val); }
    set max(val){ this.setAttribute("max", val); }
    get min(){ return parseFloat(this.getAttribute("min")?? 0); }
    get max(){ return parseFloat(this.getAttribute("max")?? 100); }

    set value(val){ this.setAttribute("value", val); }
    get value(){ return parseFloat(this.getAttribute("value")?? 0); }

    set text(val){ this.setAttribute("text", val); }
    get text(){ return this.getAttribute("text")?? null; }
    
    static get observedAttributes() {
        return ["min", "max", "value", "t0", "t1", "text"];
    }

    attributeChangedCallback(name, old_val, new_val) { 
        if(this.shadowRoot == null) return;
        this.render();
    }

    //---------------------------------------------------------
    // renderRing(ring, start = 0, range = 0){
    //     console.log(start, range);

    //     let radius = ring.r.baseVal.value;
    //     let circumference = radius * 2 * Math.PI;
    //     let offset = (range / 100 ) * circumference/2;

    //     ring.style.strokeDashoffset = 0;
    //     ring.style.strokeDasharray = `${offset} ${circumference}`;
    //     ring.setAttribute("transform", `rotate(${180 + (start/100 * 180)}, 50, 100)`) // start at the left side
    // }

    renderRing(ring, start = 0, range = 0){
        let radius = ring.r.baseVal.value;
        let circumference = radius * 2 * Math.PI;
        let offset = (range - start) * circumference/2;

        ring.style.strokeDashoffset = 0;
        ring.style.strokeDasharray = `${offset} ${circumference}`;
        ring.setAttribute("transform", `rotate(${180 + start*180}, ${this.center}, 100)`) // start at the left side
    }

    render(){
        let angle = this.normalize(this.value, this.min, this.max);
        angle = this.clamp(angle, 0, 1);
        // console.log(this.value, this.min, this.max, angle);
        
        let arrow = this.shadowRoot.querySelector("#arrow");
        arrow.setAttribute("transform", `rotate(${angle * 170 - 170/2}, ${this.center}, 100)`)

        let nt0 = this.normalize(this.t0, this.min, this.max) 
        let nt1 = this.normalize(this.t1, this.min, this.max) 
        let max = this.normalize(this.max, this.min, this.max) 

        this.renderRing(this.shadowRoot.querySelector("#ring-1"),   0, nt0);
        this.renderRing(this.shadowRoot.querySelector("#ring-2"), nt0, nt1);
        this.renderRing(this.shadowRoot.querySelector("#ring-3"), nt1, max);

        // this.shadowRoot.querySelector("#line-t0").setAttribute("transform", `rotate(${nt0 * 180}, 100, 100)`)
        // this.shadowRoot.querySelector("#line-t1").setAttribute("transform", `rotate(${nt1 * 180}, 100, 100)`)

        this.shadowRoot.querySelector("#min").textContent = parseInt(this.min);
        this.shadowRoot.querySelector("#max").textContent = parseInt(this.max);
        this.shadowRoot.querySelector("#speed").textContent = parseInt(this.value);
        this.shadowRoot.querySelector("#units").textContent = this.text ?? "";
    }

    normalize = (num, min, max) => (num - min) / (max - min);
    clamp = (num, min, max) => num <= min ? min : num >= max ? max : num;

    connectedCallback(){
        this.attachShadow({mode: 'open'});
        this.style.display = "flex";
        this.style.height = "100%";
        this.style.padding = "5px";
        this.style.paddingBottom = "10px";
        this.style.paddingTop= "10px";
        
        this.shadowRoot.innerHTML = this.body();
        
        this.render();
    }
}

window.customElements.define('speedometer-gauge', SpeedometerGauge);
