
(function(angular) {
	'use strict';
	var app = angular.module('WorkCenter', ['ngSanitize']);
	var inDialog = false, soundsLoaded = false;

	if (('createjs' in window) && 'Sound' in createjs) {
		createjs.Sound.alternateExtensions = ['ogg'];
		createjs.Sound.addEventListener('fileload', function() {
			soundsLoaded = true;
		});
		//createjs.Sound.registerSound(window.baseUri + 'sounds/notify.mp3', 'notifySound');
	}

	app.directive('graphTimeline', ['$timeout',function() {
		return {
			restrict: 'A',
			scope: {
				graphTimeline: '=graphTimeline',
				graphTimelinePlan: '=graphTimelinePlan',
				graphTimelineSensorType: '=graphTimelineSensorType'
			},
			link: function (scope, element) {
				// class="tml-gitem tml-gitem-{{(period.type === 1) ? 'green' : ((period.type === 2) ? 'red' : 'gray')}}"
				// style="left: {{calcGraphVal(period.start, true)}}; width: {{calcGraphVal(period.end - period.start, false)}};"
				// title="{{period.title}}"
				// data-ng-repeat="period in hour.periods"
				element.addClass('tml-hours');
				var renderElem = function() {
					element.empty();
					angular.element(document.body).children('.popover').remove();
					var hours = scope.graphTimeline, hour, i, n, t, x, hourElem, graphElem, period, periods, elem, color, w, gl, h = 3600.0, left, m, lastHourGraphElem = null, b;
					var marginAtRight = 0;
					for(i in scope.$parent.workcenterHoursInfoBlocks){
						marginAtRight += scope.$parent.workcenterHoursInfoBlocks[i].addSpace;
					}
					var hourMaxLenght = 0;
					for (i = 0; i < hours.length; i++) {
						hour = hours[i];
						hourElem = angular.element('<div>').appendTo(element);
						for (t = 0; t < 2; t++) {
							if (t) {
								angular.element('<div>').addClass('tml-hour-title').text(hour.value).appendTo(hourElem);
								angular.element('<div>').addClass('tml-hour-date').text(hour.date).appendTo(hourElem);
							} else {
								if(!hour.planPeriods.length){continue;}
								angular.element('<div>').addClass('tml-hour-title tml-hour-title-plan').text(hour.planValue).appendTo(hourElem);
							}
							graphElem = angular.element('<div '+(marginAtRight>0?'style="margin-right: '+marginAtRight+'px;"':'')+'>').addClass('tml-graph').appendTo(hourElem);
							if (t) lastHourGraphElem = graphElem;
							if (!t) graphElem.addClass('tml-graph-plans');
							w = Math.max(186, graphElem.width()); gl = graphElem.position().left + parseInt(graphElem.css('margin-left').substring(0, 3), 10);
							if (t === 1) {
								gl = gl < 60?70:gl;
								for(x = 0; x < 13; x++) {
									left = gl + (x*(w/h)*60*5) + ((x === 12) ? 1 : 0);
									m = angular.element('<div>').addClass('tml-marker').css({left: left, top: (i ? 0 : '-20%'), height: (i ? '100%' : '120%')}).appendTo(hourElem);
									if (!i) m.text(x * 5);
								}
							}
							if(hour.markers) {
								let markerIndex;
								for(markerIndex in hour.markers) {
									let markerInfo = hour.markers[markerIndex];
									angular.element('<span>').addClass('marker').addClass(markerInfo.class?markerInfo.class:'').text(markerInfo.text?markerInfo.text:'').css({left:  Math.ceil(markerInfo.minutes*60/h*w)+'px'}).appendTo(graphElem);
								}
							}
							periods = (t ? hour.periods : hour.planPeriods);
							for (n = 0; n < periods.length; n++) {
								period = periods[n];
                                color = "";
                                if(period.type === 1) {
                                    color = "green";
                                }else if(period.type === 2) {
                                    color = "red";
                                    //if(!period.whileWorking) {
                                    //    color = 'blue';
                                    //}
                                }else if(period.type === 3) {
                                    if(period.whileWorking) {
                                        color = "sys-error";
                                    }else if(period.json && period.json.transition_exceeded > 0){
                                		color = "yellow exceeded_transition";
                                	}else{
                                        color = "yellow";
                                    }
                                }else if(period.type === 4) {
                                    color = "red-grid";
                                }else if(period.type === 5) {//spausdinimo proceso indikatorius
                                    color = "print";
                                }else if(period.type === 6) {//tuscio laiko tarpo indikatorius
                                    color = "white";
                                }else if(period.type === 7) { //leto greicio indikatorius darbo metu
                                    color = "green-slow-speed";
                                }else if(period.type === 8) { //trumpos problemos indikatorius
                                    color = "red short-problem";
                                }else if(period.type === 9) { //prastova isimtis
                                    color = "red problem-exclusion";
                                }else if(period.type === 10) { //planas
                                    color = "planedtime";
                                }else {
                                    color = period.type;
                                }
								//color = (period.type === 1) ? 'green' : ((period.type === 2) ? 'red' : ((period.type === 3) ? (period.whileWorking ? 'red-grid' : 'yellow') : 'gray'));
								elem = angular.element('<div>').addClass('tml-gitem problemid_'+period.problemId+' tml-gitem-' + color).attr("period-type",period.title);
                                b = {
									left: Math.ceil(period.start/h*w),
									width: (Math.ceil((period.end - period.start)/h*w) + 2)
									//width: (Math.ceil((period.end - period.start)/h*100) + 2)
								};
								if ((b.left + (n ? 2 : 1) + b.width) > w && (periods.length - 1) === n) {
									b.width = w - (b.left + (n ? 2 : 1)) + 3;
								}
								elem.css({left: (b.left + 'px'), width: (b.width + 'px')});
								//if (period.shortTitle && ((period.type !== 3 && period.type !== 4) || !period.whileWorking)) {
								if (period.shortTitle) {
									angular.element('<span>').text(period.shortTitle).appendTo(elem);
								}
								//if (((period.type === 4) && t) || ((window.allowChangeProcess || window.baseUriDc) && (period.type === 2 || (period.type === 3 && period.foundProblemId != null)) && scope.$parent.currentShift.Shift.id == scope.$parent.historyShift.Shift.id) ) { //(period.type === 3 && period.whileWorking) ||
								//if ((window.allowChangeProcess || window.baseUriDc || period.type == 4) && (period.foundProblemId != null) && scope.$parent.currentShift.Shift.id == period.shiftId) {
								if ((window.allowChangeProcess || (window.baseUriDc || period.type == 4)) && (period.foundProblemId != null)) { //(period.type === 3 && period.whileWorking) ||
									(function($scope, period) {
										$(elem.get(0)).on('click', function() {
											period.data.FoundProblem.oldStart = period.data.FoundProblem.start;
											period.data.FoundProblem.oldEnd = period.data.FoundProblem.end;
											period.data.FoundProblem.newStart = '';
											period.data.FoundProblem.newEnd = '';
											var currentDowntimeDuration = (new Date(period.data.FoundProblem.end).getTime() - new Date(period.data.FoundProblem.start).getTime())/1000;
											period.data.FoundProblem.duration = ('0'+String(parseInt(currentDowntimeDuration / 3600))).slice(-2) + ':' + 
																				('0'+String(parseInt(currentDowntimeDuration % 3600 / 60))).slice(-2) + ':' + 
																				('0'+String(parseInt(currentDowntimeDuration % 3600 % 60))).slice(-2);
											$scope.$parent.endProductionProc({currentPlan: null, event: {dateTime: period.dateTime, foundProblemId: period.foundProblemId, type: period.type, problemId: period.problemId, foundProblem: period.data.FoundProblem}});
											/*Skriptas del bioko*/
											if(period.json && period.json.transition_exceeded > 0 && period.type == 4){ //jei buvo paspaustas ant intyervalo, kuris susikure kaip derinimo virsijimas (yra su nepazymeta prastova ir turi perejimo problema)
												$scope.$parent.prodRegisterIssue($scope.$parent.zeroTimeTransitionProblemTypes);
											}
											/* --- */
											$scope.$parent.$apply();
										}).css({cursor: 'pointer'}); 
									})(scope, period);
								}else if(period.title.length > 0){
									(function($scope, period) {
										$(elem.get(0)).on('click', function() {
											$scope.$parent.showInformationModal(period.title, period.data);
										}).css({cursor: 'pointer'}); 
									})(scope, period);
								}
								if (period.title) {
									$(elem.get(0)).popover({
										content: period.title,
										html: true,
										container: 'body',
										placement: i <= 1?'bottom':'right',
										trigger: 'hover'
									});
								}
								graphElem.append(elem);
							}
							hourMaxLenght = parseInt(b.left)+parseInt(b.width) > hourMaxLenght?parseInt(b.left)+parseInt(b.width):hourMaxLenght;
							var infoBlockWidthSum = hourMaxLenght+70;
							for(var infoIndex in scope.$parent.workcenterHoursInfoBlocks){
								var infoBlock = angular.element('<div>').
									css({width: scope.$parent.workcenterHoursInfoBlocks[infoIndex].addSpace+'px', left: infoBlockWidthSum+'px'}).
									addClass('tml-info-block text-center').
									text(scope.$parent.workcenterHoursInfoBlocks[infoIndex].title).
									appendTo(hourElem);
								var hourInfo = typeof(scope.$parent.workcenterHoursInfoBlocks[infoIndex].value[hour.value]) !== 'undefined'?scope.$parent.workcenterHoursInfoBlocks[infoIndex].value[hour.value]:'';
								angular.element('<span>').text(hourInfo).appendTo(infoBlock);
								infoBlockWidthSum += parseInt(scope.$parent.workcenterHoursInfoBlocks[infoIndex].addSpace);
							}
						}
					}
					if (scope.graphTimelinePlan && lastHourGraphElem) {
						var lElem = lastHourGraphElem.children('.tml-gitem');
                        //if(scope.graphTimelineSensorType == 3) {
                        //    var pallet_count = Math.floor(scope.graphTimelinePlan.ApprovedOrder.box_quantity/52);
                        //    var box_count = scope.graphTimelinePlan.ApprovedOrder.box_quantity - pallet_count*52;
                        //    var quantityBox = angular.element('<div>').addClass('tml-quantity-box').text(pallet_count + "pal. " + box_count + " dėž.").appendTo(lastHourGraphElem);
                        //} else {
                            var quantityBox = angular.element('<div>').addClass('tml-quantity-box').text(scope.graphTimelinePlan.ApprovedOrder.quantity).appendTo(lastHourGraphElem);
                        //}

						var qbTop = (lastHourGraphElem.height() - quantityBox.outerHeight(true)) / 2;
						var qbLeft = ((lElem.length > 0) ? (lElem.eq(lElem.length - 1).position().left + lElem.eq(lElem.length - 1).width() - 6) : 0);
						if ((qbLeft + quantityBox.outerWidth(true)) > lastHourGraphElem.width()) {
							qbLeft = lastHourGraphElem.width() - quantityBox.outerWidth(true) - 2;
						}
						quantityBox.css({left: qbLeft, top: qbTop});
					}
					//alert($('body').css('height'));
					//var addSpaceForHover = Math.min(Math.abs($(window).height() - $(document).height()), 200);
					$('body').css('height',(parseInt($(document).height()))+'px');
				};
				renderElem();
				scope.$watch(function() { return scope.graphTimeline; }, renderElem);
			}
		};
	}]);

	app.directive('timePager', function() {
		var calcTime = function(fromDate, hours) {
			var date = new Date(fromDate.toISOString());
            var sel = new Date(Date.parse($("#timepager-date").val()));
            if(!isNaN(sel)) {
                date = sel;
            }
			if (hours < 0) {
				var day = date.getDate() - 1;
				if (day < 1) {
					var month = date.getMonth() - 1;
					if (month < 0) {
						date.setFullYear(date.getFullYear() - 1);
					}
					date.setMonth(Math.max(month, 0));
				}
				date.setDate(Math.max(day, 1));
				date.setHours(hours + 24);
			} else {
				date.setHours(hours);
			}
			date.setMinutes(59);
			date.setSeconds(59);
			return date;
		};
		return {
			restrict: 'A',
			scope: {
				timePager: '=timePager',
				timePagerSel: '=timePagerSel',
				timePagerUip: '=timePagerUip'
			},
			link: function (scope, element, attr) {
                $('#timepager-date').datepicker({
                    format: 'yyyy-mm-dd',
                    language: currentLang,
                    todayHighlight: true,
                    firstDay:1
                });
				var ul = angular.element('<ul>').addClass('pagination pagination-sm no-margin').appendTo(element);
				var prel = angular.element('<li>').addClass('disabled').append(
						angular.element('<span>').css({cursor:'default'}).append(
							angular.element('<img>').attr({'src': window.baseUri + 'images/loaders/loader12.gif', alt: ''}).css({width: 18, height: 18})
						).css({padding: '4px 4px', fontSize: '1px', lineHeight: '1px'})
					).hide();
				var buildPages = function() {
					prel.detach();
					ul.empty();
					var h = 23, c = 0, i, ih, li, liA;
					for (i = h; i >= c; i--) {
						ih = calcTime(scope.timePager, i);
						li = angular.element('<li>');
						liA = angular.element('<a>').attr('href', '#').text(ih.getHours() + 1).appendTo(li);
						if (scope.timePagerSel
							&& ih.getFullYear() === scope.timePagerSel.getFullYear()
							&& ih.getMonth() === scope.timePagerSel.getMonth()
							&& ih.getDate() === scope.timePagerSel.getDate()
							&& ih.getHours() === scope.timePagerSel.getHours()
						) {
							li.addClass('active');
						} else if (!scope.timePagerSel && i === h) {
							li.addClass('active');
						}
						(function(scope, i) {
							li.on('click', function() {
								scope.timePagerSel = calcTime(scope.timePager, i);
								scope.$parent.forceUpdateStatus(scope.timePagerSel);
								scope.$parent.$apply();
								return false;
							});
						})(scope, i);
						ul.prepend(li);
					}
					prel.appendTo(ul);
					if (attr.timePagerTitle) {
						angular.element('<li>').addClass('disabled').append(angular.element('<a>').css({cursor:'default'}).text(attr.timePagerTitle)).prependTo(ul);
					}
				};
				buildPages();
				scope.$watch(function() { return scope.timePager; }, function() { buildPages(); });
				scope.$watch(function() { return scope.timePagerUip; }, function() {
					if (scope.timePagerUip) {
						prel.show();
					} else {
						prel.hide();
					}
				});
			}
		};
	});

	app.controller('WorkCenterCtrl', ['$scope', '$http', '$timeout','$filter','$q', function($scope, $http, $timeout,$filter,$q) {
        $scope.lost_connection=false;
        $scope.Math=Math;
		$scope.currTime = new Date();
		$scope.selTime = null;
		$scope.needTimePager = window.is_operator?false:true;
		$scope.updateInProgress = false;
		$scope.helpUrl = 'javascript:void(0)';
		$scope.hours = [];
		$scope.workCenter = null;
		$scope.currentPlan = null;
		$scope.oeeGaugeTitle = '';
		$scope.foundProblem = null;
		$scope.texts = null;
		$scope.currentShift = null;
		$scope.historyShift = null;
		$scope.line = null;
		$scope.event = null;
		$scope.plans = [];
		$scope.allPlans = [];
		$scope.products = [];
		$scope.additionalFieldsErrors = [];
        $scope.productsFilterString = "";
        $scope.plansFilterString = "";
        $scope.graphHoursCount = 0;
        $scope.createPlan = {
            paste_code: 'T001',
            production_name: 'Gaminys',
            production_code: 'Gam',
            mo_number: '000009',
            start: '2015-07-20 12:00',
            end: '2015-07-21 12:00',
            quantity: 10,
            line_quantity:1,
            step:2000,
            box_quantity:1,
            package_quantity:1,
            control:'X',
            comment:'Comment',
            weight:0,
            yield:0,
            qty_1:0
        };
        $scope.callProdComplete = false; //kai gamyba baigta ir spaudziamas perejimas jei yra perejimo tipu, realiai kvieciame sendRegisterIssue f-ja, taciau reikia kviesti callProdComplete
        $scope.promptProductionWindow = false;
        $scope.mainPluginTitle = '';
		$scope.problemTypes = [];
		$scope.transitionProblemTypes = [];
		$scope.zeroTimeTransitionProblemTypes = [];
        $scope.workingProblemTypes = [];
        $scope.parentProblemTypes = [];
		$scope.speedProblemTypes = [];
		$scope.sensor = [];
		$scope.lossTypes = [];
		$scope.OEE = 0;
		$scope.lastMinuteSpeed = 0;
		$scope.showSpeedTitle = false;
        $scope.view_r1 = false;
        $scope.sensorType = 0;
        $scope.waitingDiffCardId = 0;
        $scope.verificationOnProductionEnd = false;
        $scope.verification = null;
        $scope.informationText = 'tekstas';

        $scope.serverTime = null;
        $scope.serverUnix = null;

        $scope.toggle_r1 = false;
        $scope.toggleR1Text_off = "Sekti R1 jutiklį";
        $scope.toggleR1Text_on = "Nesekti R1 jutiklio";
        $scope.toggle_r1Text = $scope.toggleR1Text_off;

        $scope.showDeclareQuantities = false;
        $scope.partialQuantity = '';
        $scope.partialQuantityPieces = '';
        $scope.orderPartialQuantity = 0;
        $scope.orderPartialQuantityPieces = 0;
        $scope.orderLossesQuantities = 0;
        $scope.totalOrderLossesQuantities = 0;
        $scope.partialQuantityFinish = '';
        $scope.partialQuantityPiecesFinish = '';
        $scope.laukas = 0;

        $scope.orderPostphoneCountdown = 0;
		$scope.newPlan = null;
		$scope.newProduct = null;
        $scope.newType = 0;
		$scope.stateLoading = false;
		$scope.swapCurrentPlanYesPlans = false;
		$scope.stopProdState = 0;
		$scope.endDlg = {};
		$scope.waitForFakePostphone = false;
		$scope.isScreenLocked = false;
		$scope.problemComment = '';
		$scope.downtimeCutType = 0;
		$scope.downtimeCutDuration = 0;
		$scope.accessesToMethods = [];
		$scope.prevShiftsOEE = [];
		$scope.additionalPlanData = {};
		$scope.availableSensors = {};
		$scope.workcenterHoursInfoBlocks = {};
		$scope.lockScreen = function(lock) {
			if (lock && !$scope.isScreenLocked) {
				$('#lockScreen').modal('show');
			} else if (!lock && $scope.isScreenLocked) {
				$('#lockScreen').modal('hide');
			}
			$scope.isScreenLocked = lock ? true : false;
		};
        $scope.createPlanErrors = [];
        $scope.doCreatePlan = function(plan) {
            plan.line_id = $scope.line.id;
            plan.sensor_id = $scope.workCenter.id;
            //$scope.createPlan = {};
            $http.post(window.baseUri + 'work-center/createAndStartPlan/', {plan:plan}).success(function (data) {
                if('errors' in data) {
                    $scope.createPlanErrors = data.errors;
                } else {
                    if('newPlan' in data) {
                        $scope.newPlan = data.newPlan;
                        $scope.prodConfirmStart();
                    }
                }
                //$scope.stateLoading = false;
                //$('#startPlanModal').modal('hide');
                //inDialog = false;
                //updateStats();
            });
        };
		$scope.selectNewPlan = function(plan) {
			inDialog = true;
			$scope.stateLoading = false;
            $scope.newPlan = plan;
        };
		$scope.selectAdditionalField = function(item,itemTitle) {
            $scope.additionalPlanData[itemTitle] = item;
        };
        $scope.selectNewProduct = function(product) {
        	inDialog = true;
			$scope.stateLoading = false;
            $scope.newProduct = product;
        };
		$scope.$watch(function() { return $scope.event; }, function() {
			setTimeout(function() { // wait for all modals to close
				if ($scope.event) {
					if ($scope.event.type === 'started') { $scope.startProductionProc(); $scope.notifySound(); }
					if ($scope.event.type === 'stopped') { $scope.endProductionProc({autoEvent:true}); $scope.$apply(); $scope.notifySound(); }
					if ($scope.event.type === 'speed') { $scope.startRegisterSpeedIssue(); $scope.notifySound(); }
				}
			}, 1000);
		});
		// $scope.emptyIfZero = function(inputName){
		// 	if(!$scope[inputName]){
		// 		$scope[inputName] = '';
		// 	}
		// };
		$scope.initiateShiftChange = function(){
			let confirmShiftChage = confirm(text_confirm_shift_change);
			if(!confirmShiftChage){ return false; }
			$http.post(window.baseUri + 'workCenter/setShiftEnd/', {'sensor_id':$scope.sensor.id}).success(function (data) {
				updateStats();
			});
		};

		$scope.openConfirmationPage = function(){
			window.open(window.baseUri + 'approved_orders', "new");
            return false;
		};
		$scope.setGraphHoursCount = function(graphHoursCount) {
			$('body').css('height',$(window).height()+'px');
			$scope.graphHoursCount = graphHoursCount;
			updateStats();
		};
		$scope.notifySound = function() {
			if (soundsLoaded) {
				var snd = createjs.Sound.play('notifySound');
				snd.setVolume(0.2);
			}
		};
		$scope.calcGraphVal = function(val, left) {
			var w = 986;
			var h = 3600;
			var nval = Math.ceil(val/h*w);
			if (nval < 0) nval = 0;
			if (left) nval -= 0; else nval += 1;
			return nval + 'px';
		};

        $scope.confirmOrders = function() {
            window.open(window.baseUri+'approved_orders/index/sensorId:'+$scope.sensor.id, "xx");
            return false; 
        };
        $scope.swapCurrentPlan = function() {
            $("#swapCurrentPlan").modal('show');
        };
        $scope.printPage = function() {
            window.print();
        };
        $scope.swapCurrentPlanYes = function() {
            $scope.swapCurrentPlanYesPlans = true;
        };
        $scope.swapCurrentPlanNo = function() {
            $scope.swapCurrentPlanYesPlans = false;
            $scope.stateLoading = false;
            $scope.swapCurrentPlanYesPlansYes = false;
            $("#swapCurrentPlan").modal('hide');
        };
        $scope.swapCurrentPlanConfirm = function() {
            $scope.swapCurrentPlanYesPlansYes = true;
        };

        $scope.swapCurrentPlanConfirmYes = function() {
            $scope.stateLoading = true;
            var currentPlan = $scope.currentPlan.Plan;
            var options = {
                'currentPlanId':currentPlan.id,
                'sensorId':$scope.workCenter.id,
                'additionalData': $scope.additionalPlanData
            };
            if($scope.sensor.working_on_plans_or_products == 1){
            	var swapProduct = $scope.newProduct.Product;
            	if(currentPlan.product_id == swapProduct.id) {
	                $scope.swapCurrentPlanNo();
	                return;
	            }
	            options.newProductId = swapProduct.id;
			}else if($scope.sensor.working_on_plans_or_products == 0){
				if(currentPlan.id == $scope.newPlan.id) {
	                $scope.swapCurrentPlanNo();
	                return;
	            }
	            options.newPlanId = $scope.newPlan.id;
			}
            $http.get(window.baseUri + 'work-center/swap-current-plan',{params:options}).success(function() {
                //$scope.prodPostphone();
                //$scope.prodConfirmStart();
                $scope.swapCurrentPlanNo();
                inDialog = false;
                updateStats();
                $scope.stateLoading = false;
            }).error(function() {
                console.log("ERROR");
                $scope.stateLoading = false;
            });
        };
        
        

		$scope.showInformationModal = function(information, periodData) {
			let foundProblem = periodData.hasOwnProperty('FoundProblem')?periodData.FoundProblem:null;
			foundProblem = foundProblem == null && periodData.hasOwnProperty('Work')?periodData.Work:foundProblem;
			if($scope.sensor.time_interval_declare_count_button == 2) {
				$scope.totalOrderLossesQuantities = 0;
				$scope.orderLossesQuantities = new Array();
				if (foundProblem.problem_id == -1) {
					for (var i in periodData.ApprovedOrder.losses) {
						let loss = periodData.ApprovedOrder.losses[i];
						$scope.orderLossesQuantities[i] = loss.quantity;
						$scope.totalOrderLossesQuantities += parseFloat(loss.quantity);
					}
				}
				inDialog = true;
			}
			if(foundProblem != null){
				$scope.informationComment = foundProblem.comments;
				$scope.informationFoundProblemId = foundProblem.id;
			}else{
				$scope.informationComment = '';
				$scope.informationFoundProblemId = 0;
			}
			$scope.informationText = information;
			//$scope.informationText = '<div class="col-sm-6">'+information +'</div><div class="col-sm-6">'+ $('#declareQuantitiesModal').find('.modal-body').html()+'</div>';
			$scope.$apply();
			$('#informationText').html($scope.informationText);
			$('#informationModal').modal('show');
		};

		$scope.closeInformationModal = function() {
			if($scope.sensor.time_interval_declare_count_button == 2) {
				inDialog = false;
				updateStats();
			}
			$('#informationModal').modal('hide');
		};
		$scope.saveAndCloseInformationModal = function(foundProblemId) {
			if(foundProblemId > 0){
				let params = {foundProblemId: foundProblemId, problemComment: $scope.informationComment}
				$http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-issue/send_only_comment', {params: params}).success(function (data) {
					updateStats();
					$('#informationModal').modal('hide');
				});
			}
		};
		
		$scope.endProductionProc = function(opts) {
			if (typeof opts !== 'object') opts = {};
			if(opts.hasOwnProperty('event')){
				$scope.problemComment = opts.event.foundProblem.comments;
			}
			$scope.endDlg = {
				currentPlan: (('currentPlan' in opts) ? opts.currentPlan : $scope.currentPlan),
				event: (('event' in opts) ? opts.event : $scope.event),
				autoEvent: (opts.autoEvent ? true : false),
				quickSwap: (opts.quickSwap ? true : false)
			};
			$('#endPlanModal').modal('show');
			inDialog = true;
			if (opts.quickSwap) {
				$scope.stateLoading = true;
				$http.get(window.baseUri + 'work-center/store-timestamp').success(function() {
					$scope.stateLoading = false;
				}).error(function() {
					$scope.stateLoading = false;
				});
				$scope.stopProdState = 2;
			} else {
				$scope.stateLoading = false;
				$scope.stopProdState = 0;
			}
		};
		$scope.startProductionProc = function() {
			$('#startPlanModal').modal('show');
			//inDialog = true;
			//$scope.stateLoading = false;

            jQuery(".datepicker").datetimepicker({
                dateFormat: "yy-mm-dd"
            });
		};

        $scope.prodConfirmStartConfirm = function(type) {
			let printRequireText = new Array();
			for (var inputName in requiredInputTexts){
				if(!$scope.additionalPlanData.hasOwnProperty(inputName)){
					printRequireText.push(requiredInputTexts[inputName]);
				}
			}
			if($scope.newPlan == null && $scope.newProduct == null){
				printRequireText.push(text_what_you_produce);
        	}
			if(printRequireText.length){
				alert(printRequireText.join("\n"));
				return;
			}
            if(type == 0) {
                $scope.newType = 0;
                $scope.newProduct = null;
            } else {
                $scope.newType = 1;
                $scope.newPlan = null;
            }
            $scope.prodConfirmStartWaitingConfirmation = true;
        };

        $scope.prodConfirmStart = function () {
            $scope.prodConfirmStartWaitingConfirmation = false;
            if (!$scope.newProduct && !$scope.newPlan) {
                //$scope.newPlan = $scope.plans[0];
                return;
            }
            $scope.stateLoading = true;

            var idToSend = 0;
            if($scope.newType == 0) {
                idToSend = $scope.newPlan.id;
            }
            if($scope.newType == 1) {
                idToSend = $scope.newProduct.Product.id;
            }
            $http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'start-prod/'+ + idToSend, {params: {dateTime: $scope.event.dateTime, type:$scope.newType, additionalData: $scope.additionalPlanData}}).success(function (data) {
				$scope.additionalPlanData = {};
            	$('#startPlanModal').modal('hide');
                inDialog = false;
                if(data !== null && data.hasOwnProperty('errors')){
            		$scope.additionalFieldsErrors = data.errors;
                }else{
                	$scope.additionalFieldsErrors = [];
                }
                $scope.stateLoading = false;
                updateStats();
            }).error(function () {
                $scope.stateLoading = false;
                $('#startPlanModal').modal('hide');
                inDialog = false;
                updateStats();
            });
        };
		$scope.prodContinue = function() {
			$scope.stateLoading = false;
			$('#endPlanModal').modal('hide');
			inDialog = false;
			updateStats();
		};

        $scope.prodPostphoneCountdownConfirm = function() {
            $scope.orderPostphoneCountdown = 1;

        };

        $scope.prodPostphoneCountdown = function (fake, waitFake) {
            $scope.orderEndTime = $scope.serverTime;
            $scope.orderPostphoneCountdown = 3;

            $scope.orderPostphoneCountdown = 2;
            var timePromise = getTime();
            timePromise.then(function () {
                $scope.startEndProductionDate = $scope.serverTime;
            });
            $scope.counter = 3;
            $scope.timeout = function () {
                $scope.counter--;
                if ($scope.counter > 0) {
                    myTimeout = $timeout($scope.timeout, 1000);
                } else {
                    var timePromise = getTime();
                    timePromise.then(function () {
                        $scope.orderEndTime = $scope.serverTime;
                        $scope.orderPostphoneCountdown = 3;
                    });

                    //$scope.prodPostphone(fake,waitFake);
                }
            };

            var myTimeout = $timeout($scope.timeout, 1000);
        }

		$scope.prodPostphone = function(fake, waitFake,problemId) {
			if (fake && waitFake) {
				$scope.waitForFakePostphone = true;
				$scope.lockScreen(true);
				return;
			} else {
				$scope.waitForFakePostphone = false;
			}
			$scope.stateLoading = true;
			var params = {};
			if (fake) {
				params.dateTime = $scope.currTime.toISOString();
				params.fake = 1;
			} else {
                params.startEndProductionDate = $scope.startEndProductionDate;
				params.dateTime = $scope.orderEndTime;
			}
            if(problemId) {
                params.problemId = problemId;
            }

			$http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'postphone-prod', {params: params}).success(function(data) {
				$scope.stateLoading = false;
				$('#endPlanModal').modal('hide');
				inDialog = false;
				updateStats();
			}).error(function() {
				$scope.stateLoading = false;
				$('#endPlanModal').modal('hide');
				inDialog = false;
				updateStats();
			});
            $scope.orderPostphoneCountdown = 0;
		};
		$scope.prodComplete = function() {
            $scope.orderEndCountdown = 0;
            $scope.orderEndVar = 0;
			if ($scope.lossTypes.length || $scope.sensor.time_interval_declare_count_on_finish) {
				$scope.stopProdState = 2;
			} else {
				$scope.stopProdState = 3;
			}
		};
		
		
		$scope.nextProdComplete = function(partialQuantityFinish) {
            $scope.partialQuantityFinish = partialQuantityFinish;
			if ($scope.endDlg.quickSwap) {
				$scope.sendProdComplete(1, false);
			} else {
				$scope.stopProdState = 3;
			}
		};
		$scope.sendProdComplete = function(problemId, promptProductionWindow) {
			$scope.promptProductionWindow = promptProductionWindow;
			if(problemId == 1 && parseInt($scope.transitionProblemTypes.length) > 0){
            	$scope.callProdComplete = true;
            	$scope.prodRegisterIssue($scope.transitionProblemTypes);
                return;
            }
			$scope.stateLoading = true; 
			var params = {dateTime: $scope.serverTime};
            params.startEndProductionDate = $scope.serverTime;
			//var params = {dateTime: $scope.event.dateTime};
			if (problemId) params['problemId'] = problemId;
			if ($scope.endDlg.quickSwap) params['quickSwap'] = 1;
			if($scope.partialQuantityFinish) {
				params['declaredQuantity'] = $scope.partialQuantityFinish;
			}
			if($scope.partialQuantityPiecesFinish) {
				params['declaredQuantityPieces'] = $scope.partialQuantityPiecesFinish;
			}
			if ($scope.lossTypes) {
				for (var k in $scope.lossTypes) {
					params['lt_' + $scope.lossTypes[k].LossType.id] = $scope.lossTypes[k].LossType.value;
				}
			}
			$http.get(window.baseUri + 'work-center/'+(window.baseUriDc ? (window.baseUriDc + '/') : '')+'complete-prod', {params: params}).success(function(data) { 
				$scope.partialQuantityFinish = '';
				//$scope.stateLoading = false;
				$scope.callProdComplete = false;
				//$('#endPlanModal').modal('hide');
				inDialog = false;
				updateStats();
				if($scope.promptProductionWindow){
					$scope.event.dateTime = $scope.serverTime;
					$scope.startProductionProc();
				}
			}).error(function() {
				$scope.stateLoading = false; 
				$('#endPlanModal').modal('hide');
				inDialog = false;
				updateStats();
			});
			$scope.stopProdState = 0;
		};
		$scope.prodRegisterIssue = function(subProblems) {
            if(typeof subProblems === 'undefined') {
            	$scope.parentProblemTypes = []; 
            	$scope.downtimeCutType=$scope.downtimeCutDuration=0;
                $scope.workingProblemTypes = $scope.problemTypes;
            } else {
                $scope.workingProblemTypes = subProblems;
            }
            $(".modal-dialog").addClass("widen");
			$scope.stopProdState = 1;
		};
		$scope.prodRegisterIssueAfterProdEnd = function() {
			$scope.workingProblemTypes = $scope.problemTypes;
            $scope.callProdComplete = true;
			$scope.stopProdState = 1;
			$(".modal-dialog").addClass("widen");
		};
        $scope.orderEnd = function() {
            if($scope.orderEndVar != 1) {
                $scope.orderEndVar = 1;
            } else {
                $scope.orderEndVar = 0;
            }
        };
        $scope.orderEndNo = function() {
            $scope.orderPostphoneCountdown = 0;
            $scope.orderEndVar = 0;
        };
        $scope.orderEndYes = function () {
        	if($scope.verificationOnProductionEnd){
        		$('#verificationModal').modal('show');  
        	}
            $scope.orderEndCountdown = 1;
            var timePromise = getTime();
            timePromise.then(function () {
                $scope.startEndProductionDate = $scope.serverTime;
            });
            $scope.counter = 3;//TODO greazinti sekundes i 15
            $scope.timeout = function () {
                $scope.counter--;
                if ($scope.counter > 0) {
                    myTimeout = $timeout($scope.timeout, 1000);
                } else {
                    var timePromise = getTime();
                    timePromise.then(function () {
                        $scope.orderEndTime = $scope.serverTime;
                        $scope.prodComplete();
                    });
                }
            };
            var myTimeout = $timeout($scope.timeout, 1000);

            $scope.stop = function () {
                $scope.counter = 3;
                $timeout.cancel(myTimeout);
            }
        }; 
        $scope.orderEndCountdownStop = function() {
            $scope.orderEndVar = 0;
            $scope.stop();
        };
		$scope.calculateDowntimesAfterCut = function(minutesCount) {
			$scope.downtimeCutDuration = $scope.downtimeCutDuration.toString().replace(',','.');
			if($scope.downtimeCutDuration.substr(-1) == '.'){
				return null;
			}
			$scope.downtimeCutDuration = parseFloat($scope.downtimeCutDuration);
			if(!$scope.downtimeCutDuration){
				$scope.endDlg.event.foundProblem.oldStart = $scope.endDlg.event.foundProblem.start;
				$scope.endDlg.event.foundProblem.oldEnd = $scope.endDlg.event.foundProblem.end;
				$scope.endDlg.event.foundProblem.newStart = $scope.endDlg.event.foundProblem.newEnd = '';
				$scope.endDlg.event.foundProblem.newDuration = 0;
				return false; 
			}
			if($scope.downtimeCutType == 0){//kerpama nuo pradzios
				 var dt = new Date(new Date($scope.endDlg.event.foundProblem.start).getTime() + 1000 + $scope.downtimeCutDuration * 60000);
				 var dt2 = new Date(new Date($scope.endDlg.event.foundProblem.start).getTime() + $scope.downtimeCutDuration * 60000);
				 $scope.endDlg.event.foundProblem.oldStart = dt.getFullYear()+'-'+("0"+(dt.getMonth()+1)).slice(-2)+'-'+("0"+(dt.getDate())).slice(-2)+' '+("0"+(dt.getHours())).slice(-2)+':'+("0"+(dt.getMinutes())).slice(-2)+':'+("0"+(dt.getSeconds())).slice(-2);
				 $scope.endDlg.event.foundProblem.oldEnd = $scope.endDlg.event.foundProblem.end;
				 $scope.endDlg.event.foundProblem.newStart = $scope.endDlg.event.foundProblem.start;
				 $scope.endDlg.event.foundProblem.newEnd = dt2.getFullYear()+'-'+("0"+(dt2.getMonth()+1)).slice(-2)+'-'+("0"+(dt2.getDate())).slice(-2)+' '+("0"+(dt2.getHours())).slice(-2)+':'+("0"+(dt2.getMinutes())).slice(-2)+':'+("0"+(dt2.getSeconds())).slice(-2);
			}else{//kerpama nup pabaigos
				 var dt2 = new Date(new Date($scope.endDlg.event.foundProblem.end).getTime() - 1000 - $scope.downtimeCutDuration * 60000);
			     var dt = new Date(new Date($scope.endDlg.event.foundProblem.end).getTime() - $scope.downtimeCutDuration * 60000);
				 $scope.endDlg.event.foundProblem.newStart = dt.getFullYear()+'-'+("0"+(dt.getMonth()+1)).slice(-2)+'-'+("0"+(dt.getDate())).slice(-2)+' '+("0"+(dt.getHours())).slice(-2)+':'+("0"+(dt.getMinutes())).slice(-2)+':'+("0"+(dt.getSeconds())).slice(-2);
				 $scope.endDlg.event.foundProblem.newEnd = $scope.endDlg.event.foundProblem.end;
				 $scope.endDlg.event.foundProblem.oldStart = $scope.endDlg.event.foundProblem.start;
				 $scope.endDlg.event.foundProblem.oldEnd = dt2.getFullYear()+'-'+("0"+(dt2.getMonth()+1)).slice(-2)+'-'+("0"+(dt2.getDate())).slice(-2)+' '+("0"+(dt2.getHours())).slice(-2)+':'+("0"+(dt2.getMinutes())).slice(-2)+':'+("0"+(dt2.getSeconds())).slice(-2);
			}
			var currentDowntimeDuration = Math.round((new Date($scope.endDlg.event.foundProblem.oldEnd).getTime() - new Date($scope.endDlg.event.foundProblem.oldStart).getTime())/1000);
			if(currentDowntimeDuration < 0){
				$scope.downtimeCutDuration = $scope.downtimeCutDuration - Math.ceil(Math.abs(parseFloat(currentDowntimeDuration/60)));
				$scope.calculateDowntimesAfterCut();
			}
			if(currentDowntimeDuration > 0){
				$scope.endDlg.event.foundProblem.newDuration = ('0'+String(parseInt(currentDowntimeDuration / 3600))).slice(-2) + ':' + 
								('0'+String(parseInt(currentDowntimeDuration % 3600 / 60))).slice(-2) + ':' + 
								('0'+String(parseInt(currentDowntimeDuration % 3600 % 60))).slice(-2);
			}
		};
		$scope.sendRegisterIssue = function(problemType, promptProductionWindow) {
			$scope.promptProductionWindow = promptProductionWindow;
			if(!problemType || problemType.Problem.id != 1) {
				$scope.parentProblemTypes.push($scope.workingProblemTypes);
			}else{
				$scope.parentProblemTypes = [];
			}
            if(parseInt(problemType.children.length) > 0) {
                $scope.prodRegisterIssue(problemType.children);
                return;
            }    
            if(problemType.Problem.id == 1 && parseInt($scope.transitionProblemTypes.length) > 0){
            	$scope.prodRegisterIssue($scope.transitionProblemTypes);
                return;
            }
            //$scope.endDlg = {
            //    currentPlan: (('currentPlan' in opts) ? opts.currentPlan : $scope.currentPlan),
            //    event: (('event' in opts) ? opts.event : $scope.event),
            //    autoEvent: (opts.autoEvent ? true : false),
            //    quickSwap: (opts.quickSwap ? true : false)
            //};
            //if(!problemType.children.length && problemType.Problem.id > 0 && $scope.texts.confirmProblemChoose.length){
	        //    var conf = confirm($scope.texts.confirmProblemChoose);
	        //    if(!conf) return false;
	        //} 
	        if($scope.callProdComplete){
            	$scope.sendProdComplete(problemType.Problem.id, $scope.promptProductionWindow);
            	return false;
            }
            $(".modal-dialog").removeClass("widen");
			$scope.stateLoading = true;
            var params = {
                dateTime: $scope.endDlg.event.dateTime, //$scope.event.dateTime,
                foundProblemId: $scope.endDlg.event.foundProblemId,
                problemComment: $scope.problemComment,
                downtimeCutType: $scope.downtimeCutType,
                downtimeCutDuration: $scope.downtimeCutDuration,
            };
            $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-issue/' + (problemType ? problemType.Problem.id : 0), {params: params}).success(function (data) {
                $scope.problemComment = '';
                $scope.stateLoading = false;
                $('#endPlanModal').modal('hide');
                inDialog = false;
                updateStats();
                if($scope.promptProductionWindow && !$scope.endDlg.currentPlan && !$scope.endDlg.event.foundProblemId){
					$scope.event.dateTime = $scope.serverTime;
					$scope.startProductionProc();
				}
            }).error(function () {
                $scope.stateLoading = false;
                $('#endPlanModal').modal('hide');
                inDialog = false;
                updateStats();
            });
            $scope.stopProdState = 0;
		};
		$scope.backProdEnd = function() {
            $(".modal-dialog").removeClass("widen");
			//console.log($scope.parentProblemTypes)
            if($scope.parentProblemTypes.length > 0){
            	$scope.prodRegisterIssue($scope.parentProblemTypes.slice(-1)[0]);
            	$scope.parentProblemTypes.splice(-1,1);
            }else{
				$scope.stopProdState = 0;
			}
		};
		$scope.idleContinue = function() {
			$('#startPlanModal').modal('hide');
			inDialog = false;
            $scope.prodConfirmStartWaitingConfirmation = false;
			updateStats();
		};
		$scope.idleIssue = function() {
			$('#endPlanModal').modal('hide');
			inDialog = false;
			updateStats();
		};
		$scope.forceUpdateStatus = function(selTime) {
			if (selTime) $scope.selTime = selTime;
			updateStats();
		};
		$scope.startRegisterSpeedIssue = function() {
			$('#speedProblemModal').modal('show');
			//inDialog = true;
			//$scope.stateLoading = false;
		};
        $scope.sendRegisterSpeedIssue = function (problemType) {
            $scope.stateLoading = true;
            var params = {dateTime: $scope.event.dateTime};
            //var params = {dateTime: $scope.serverTime};
            $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'register-speed-issue/' + (problemType ? problemType.Problem.id : 0), {params: params}).success(function (data) {
                $scope.stateLoading = false;
                $('#speedProblemModal').modal('hide');
                inDialog = false;
                updateStats();
            }).error(function () {
                $scope.stateLoading = false;
                $('#speedProblemModal').modal('hide');
                inDialog = false;
                updateStats();
            });

        };

        $scope.shouldShow = function(wc) {
            return $scope.workCenter !== null && $scope.workCenter.id == wc;
        };

        $scope.toggleR1 = function() {
            //
        };
        $scope.toggleR1Confirm = function() {
            if($scope.toggle_r1) {
                $scope.toggle_r1Text = $scope.toggleR1Text_off;
            } else {
                $scope.toggle_r1Text = $scope.toggleR1Text_on;
            }
            $scope.toggle_r1 = !$scope.toggle_r1;
            updateStats();
        };

        $scope.declareQuantities = function() {
            $("#declareQuantitiesModal").modal('show');
            inDialog = true;
        };
        $scope.saveDeclaredQuantities = function(foundProblemId) {
            var params = [];
            params['declaredQuantity'] = $scope.partialQuantity;
            params['declaredQuantityPieces'] = $scope.partialQuantityPieces;
            $scope.partialQuantity = '';
            $scope.partialQuantityPieces = '';
            if ($scope.lossTypes) {
                for (var k in $scope.lossTypes) {
                    params['lt_' + $scope.lossTypes[k].LossType.id] = $scope.lossTypes[k].LossType.value;
                }
            }
            if(foundProblemId){
            	params['foundProblemId'] = foundProblemId;
			}
            $scope.stateLoading = true;

            $http.get(window.baseUri + 'work-center/'+ (window.baseUriDc ? (window.baseUriDc + '/') : '') +'declareQuantities', {params: params}).success(function(data) {
                $scope.stateLoading = false;
                $('#declareQuantitiesModal').modal('hide');
                inDialog = false;
				$scope.closeInformationModal();
                updateStats();
            }).error(function() {
                $scope.stateLoading = false;
                $('#declareQuantitiesModal').modal('hide');
                inDialog = false;
                updateStats();
            });
        };
        $scope.hideQuantitiesModal = function() {
            $("#declareQuantitiesModal").modal('hide');
            inDialog = false;
            updateStats();
        };
        $scope.registerVerification = function() {
            $('#verificationModal').modal('hide');
            $http.get(window.baseUri + 'work-center/registerVerification', {params: {}}).success(function(data) {
                if(!$scope.orderEndVar){
                	updateStats();
                	inDialog = false;
               	}
            }).error(function() {
                $('#verificationModal').modal('hide');
                alert('Problema su serveriu. Susisiekite su sistemos administratoriumi');
            });
        };

        var getTime = function() {
            let deferred = $q.defer();
            $http.get(window.baseUri + 'time.php').success(function(data) {
                $scope.serverTime = data.time;
                $scope.serverUnix = data.unix;
                deferred.resolve("request successful");
            });
            return deferred.promise;
        };

        var updateStats = function () {
            //if(updateInProgress) {
            //    return;
            //}
            var timePromise = getTime();
            timePromise.then(function () {
                if (timeoutInst) {
                    clearTimeout(timeoutInst);
                    timeoutInst = null;
                }
                if (inDialog) return;
                var params = {};
                if (window.baseUriDc && $scope.selTime) {
                	var datestring = ($scope.selTime.getFullYear()) + "-" + ("0"+($scope.selTime.getMonth()+1)).slice(-2) + "-" + ("0" +($scope.selTime.getDate())).slice(-2) + "T" + ("0" + $scope.selTime.getHours()).slice(-2) + ":" + ("0" + $scope.selTime.getMinutes()).slice(-2) + ":" + ("0" + $scope.selTime.getSeconds()).slice(-2);
                	//params.time = $scope.selTime.toISOString();
                	params.time = datestring;
                } else {
                    params.time = $scope.serverTime;
                }
                params.graphHoursCount = $scope.graphHoursCount;
                $scope.updateInProgress = true;
                $http.get(window.baseUri + 'work-center/' + (window.baseUriDc ? (window.baseUriDc + '/') : '') + 'update-status', {params: params}).success(function (data) {
                    var k;
                    $scope.stateLoading = false;
					$('#endPlanModal').modal('hide');
                    if ('lost_connection' in data) {
                        $scope.lost_connection = true;
                        $scope.hours = [];
                        $scope.sensor = [];
                        //return false;
                    }else{ $scope.lost_connection = false; }
                    if ('availableSensors' in data) {
                        $scope.availableSensors = data.availableSensors;
                    }
                    if ('workcenterHoursInfoBlocks' in data) {
                        $scope.workcenterHoursInfoBlocks = data.workcenterHoursInfoBlocks;
                    }
                    if ('accessesToMethods' in data) {
                        $scope.accessesToMethods = data.accessesToMethods;
                    }
                    if ('prevShiftsOEE' in data) {
                        $scope.prevShiftsOEE = data.prevShiftsOEE;
                    }
                    if ('settings' in data) {
                        $scope.settings = data.settings;
                    }
                    if ('hours' in data) {
                        $scope.hours = data.hours;
                    }
                    if ('workCenter' in data) {
                        $scope.workCenter = data.workCenter;
                    }
                    if ('currentPlan' in data) {
                        $scope.currentPlan = data.currentPlan;
                    }
                    if ('foundProblem' in data) {
                        $scope.foundProblem = data.foundProblem;
                        if ($scope.foundProblem) {
                            $scope.lockScreen(false);
                            if ($scope.waitForFakePostphone) {
                                $scope.prodPostphone(true, false);
                            }
                        }
                    }
                    if('currentShift' in data){
                    	$scope.currentShift = data.currentShift;
                    }
                    if('historyShift' in data){
                    	$scope.historyShift = data.historyShift;
                    }
                    if('texts' in data){
                    	$scope.texts = data.texts;
                    }
                    
                    if ('line' in data) {
                        $scope.line = data.line;
                    }
                    if ('event' in data) {
                        $scope.event = data.event;
                        //$scope.event.ts = (new Date()).getTime();
                        $scope.event.ts = $scope.serverUnix;
                    }
                    if ('plans' in data) {
                        $scope.plans = data.plans;
                        var lid = $scope.newPlan ? $scope.newPlan.id : null;
                        $scope.newPlan = null;
                        for (var k in $scope.plans) {
                            if (!lid || lid === $scope.plans[k].id) {
                                //$scope.newPlan = $scope.plans[k];
                                break;
                            }
                        }
                    }
                    if ('allPlans' in data) {
                        $scope.allPlans = data.allPlans;
                    }
                    if ('orderPartialQuantity' in data) {
                        $scope.orderPartialQuantity = data.orderPartialQuantity;
                        $scope.orderPartialQuantityPieces = data.orderPartialQuantityPieces;
                    }
                    if ('partialQuantity' in data) {
                        $scope.partialQuantity = data.partialQuantity;
                        $scope.partialQuantityPieces = data.partialQuantityPieces;
                    }
                    if ('orderLossesQuantities' in data) {
                        $scope.orderLossesQuantities = data.orderLossesQuantities;
                        $scope.totalOrderLossesQuantities = 0;
                        for(var i in $scope.orderLossesQuantities){
	                    	$scope.totalOrderLossesQuantities += parseFloat($scope.orderLossesQuantities[i]);
	                    }
                    }
                    if ('products' in data) {
                        $scope.products = data.products;
                    }
                    if ('problemTypes' in data) {
                        $scope.problemTypes.splice(0, $scope.problemTypes.length);
                        $scope.speedProblemTypes.splice(0, $scope.speedProblemTypes.length);
                        for (var k in data.problemTypes) {
                            if (data.problemTypes[k].Problem.for_speed) {
                                $scope.speedProblemTypes.push(data.problemTypes[k]);
                            } else {
                                $scope.problemTypes.push(data.problemTypes[k]);
                            }
                        }
                    }
                    if ('transitionProblemTypes' in data) {
                        $scope.transitionProblemTypes.splice(0, $scope.transitionProblemTypes.length);
                        for (var k in data.transitionProblemTypes) {
                             $scope.transitionProblemTypes.push(data.transitionProblemTypes[k]);
                        }
                    }
                    if ('zeroTimeTransitionProblemTypes' in data) {
                        $scope.zeroTimeTransitionProblemTypes.splice(0, $scope.zeroTimeTransitionProblemTypes.length);
                        for (var k in data.zeroTimeTransitionProblemTypes) {
                             $scope.zeroTimeTransitionProblemTypes.push(data.zeroTimeTransitionProblemTypes[k]);
                        }
                    }
                    if ('lossTypes' in data) {
                        $scope.lossTypes = data.lossTypes;
                        for (k in $scope.lossTypes) {
                            $scope.lossTypes[k].LossType.value = '';
                        }
                    }
                    if ('time' in data) {
                        $scope.currTime = new Date(data.time);
                    }
                    if (('helpUrl' in data) && data.helpUrl) {
                        $scope.helpUrl = data.helpUrl;
                    }
                    if ('OEE' in data) {
                        $scope.OEE = data.OEE;
                    }
                    if ('lastMinuteSpeed' in data) {
                        $scope.lastMinuteSpeed = data.lastMinuteSpeed;
                    }

                    if ('dziuv_plans' in data) {
                        $scope.view_r1 = data.dziuv_plans;
                    }
                    if('sensorType' in data) {
                        $scope.sensorType = data.sensorType;
                    }
                    if('sensor' in data) {
                        $scope.sensor = data.sensor;
                    }
                    if('waitingDiffCardId' in data) {
                        $scope.waitingDiffCardId = data.waitingDiffCardId;
                    }
                    if('verification' in data && data.verification != null) {
                    	$scope.verification = data.verification;
                    	$scope.verificationOnProductionEnd = data.verification.verificationOnProductionEnd;
                    	if(data.verification.promptVerification) $('#verificationModal').modal('show');
                    	if(data.verification.countdown){
                    		$scope.verificationCountdown = data.verification.countdown;
                    	}else{$scope.verificationCountdown = '';}
                    }
                    if('mainPluginTitle' in data) {
                        $scope.mainPluginTitle = data.mainPluginTitle;
                    }
					$scope.oeeGaugeTitle = data.oeeGaugeTitle;
                    $scope.updateInProgress = false;
                    repeatInterval();                  
                }).error(function () {
                    $scope.hours = [
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []},
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []},
                        {value: '', date: '', planValue: '', periods: [], planPeriods: []}
                    ];
                    $scope.updateInProgress = false;
                    repeatInterval();
                });
            });
        }, timeoutInst = null, repeatInterval = function () {
            if (!timeoutInst) {
                timeoutInst = setTimeout(updateStats, 20000);//TODO
            }
        };
        updateStats();
	}]);

	app.directive('oeeGauge', function() {
		var oeeGaugeIndex = 0;
		return {
			restrict: 'A',
			scope: { oeeGauge: '=oeeGauge', oeeGaugeTitle: '=oeeGaugeTitle', colors: '=oeeColors' },
			link: function(scope, element, attrs) {
				var gId = 'oeeGauge_' + (++oeeGaugeIndex);
				element.attr('id', gId);
				element.css({width: 250, height: 250});
				var g = null;
				scope.$watch(function() { return scope.oeeGauge; }, function() {
					var levelColors = ['#D64D49', '#D64D49', '#D64D49', '#D64D49', '#D64D49', '#D64D49', '#D64D49', '#D64D49','#D64D49','#D64D49','#D64D49','#D64D49','#D64D49','#D64D49','#D64D49', '#FDEE00', '#5AB65A','#5AB65A','#5AB65A','#5AB65A'];
					if(scope.colors){
						levelColors = new Array();
						var colorIntervals = scope.colors.split(',');
						var levelStart = 1;
						for(var i in colorIntervals){
							var valueAndColor = colorIntervals[i].split(':');
							if(valueAndColor.length == 2 && parseInt(valueAndColor[0]) > 0){
								for(var i2 = levelStart; i2 <= parseInt(valueAndColor[0]); i2++){
									levelColors.push(valueAndColor[1].trim());
								}
								var levelStart = parseInt(valueAndColor[0])+1;
							}
						}
					}
					if(g != null && g.config.title != scope.oeeGaugeTitle){
						g = null;
					}
					if(g == null){
						var g = new JustGage({
							id: gId,
							value: 30,
							min: 0,
							max: 100,
							levelColors: levelColors,
							levelColorsGradient: false,
							title: scope.oeeGaugeTitle,
							titleFontSize: "5px",
						});
					}
					g.refresh(scope.oeeGauge);
				});
			}
		};
	});
	
	app.directive('speedGauge', function() {
		return {
			restrict: 'A',
			scope: { speedGauge: '=speedGauge'},
			link: function(scope, element, attrs) {
				scope.$watch(function() { return scope.speedGauge; }, function() {
					drawSpeedGraph();
				});
				var drawSpeedGraph = function(){
					 if(!scope.$parent.currentPlan || scope.$parent.sensor.tv_speed_deviation == '') return;
					 var d = scope.$parent.currentPlan.Plan.step_unit_divide?scope.$parent.currentPlan.Plan.step_unit_divide:1;
					 //var d = scope.$parent.currentPlan.Plan.step_unit && scope.$parent.currentPlan.Plan.step_unit == 'vnt/min'?1:60;//step_unit gali pareiti tik per imones plugina rankiniu budu
					 var step = parseFloat(scope.$parent.currentPlan.Plan.step / d);
					 if(step <= 0) return;
	                 scope.$parent.showSpeedTitle = true;
	                 var speederSettings = scope.$parent.sensor.tv_speed_deviation.split(',');
	                 var minDeviationPerc,maxDeviationPerc = 30;
	                 var minYellowDeviationPerc = 0;
	                 var maxYellowDeviationPerc = 5;
	                 if(speederSettings[0]){
	                     var minMaxSetting = speederSettings[0].replace('[','').replace(']','').split('-');
	                     if(minMaxSetting.length == 2){
	                         minDeviationPerc = parseFloat(minMaxSetting[0]);
	                         maxDeviationPerc = parseFloat(minMaxSetting[1]);
	                     }
	                 }
	                 if(speederSettings[1]){
	                     var minMaxSetting = speederSettings[1].replace('[','').replace(']','').split('-');
	                     if(minMaxSetting.length == 2){
	                         minYellowDeviationPerc = parseFloat(minMaxSetting[0]);
	                         maxYellowDeviationPerc = parseFloat(minMaxSetting[1]);
	                     }
	                 }
	                 var minDeviation = parseFloat(step * (1 - minDeviationPerc/100));
	                 var maxDeviation = parseFloat(step * (1 + maxDeviationPerc/100));
	                 var minYellowDeviation = parseFloat(step * (1 - minYellowDeviationPerc/100));
	                 var maxYellowDeviation = parseFloat(step * (1 + maxYellowDeviationPerc/100));
	                 var staticZones = new Array();
	                 if(typeof scope.$parent.sensor.speed_gauge_colors == 'object' && scope.$parent.sensor.speed_gauge_colors.length >= 3){
	                 	var intervalsV = !scope.$parent.sensor.speed_gauge_intervals?[minDeviation, minYellowDeviation, maxYellowDeviation, maxDeviation]:scope.$parent.sensor.speed_gauge_intervals;
	                 	for(var i in scope.$parent.sensor.speed_gauge_colors){
	                 		staticZones.push({strokeStyle: scope.$parent.sensor.speed_gauge_colors[i], min: intervalsV[i], max: intervalsV[parseInt(i)+1]});
	                 	}
	                 }else{
	                 	staticZones = [
	                       {strokeStyle: "#F03E3E", min: minDeviation, max: minYellowDeviation}, // Red from 100 to 130
	                       {strokeStyle: "#FFDD00", min: minYellowDeviation, max: maxYellowDeviation}, // Yellow
	                       {strokeStyle: "#30B32D", min: maxYellowDeviation, max: maxDeviation}, // Green
	                    ];
	                 }
	                 var opts = {
	                  angle: 0.03, // The span of the gauge arc
	                  lineWidth: 0.26, // The line thickness
	                  radiusScale: 0.8, // Relative radius
	                  pointer: {
	                    length: 0.55, // // Relative to gauge radius
	                    strokeWidth: 0.031, // The thickness
	                    color: '#000000' // Fill color
	                  },
	                  limitMax: true,     // If false, max value increases automatically if value > maxValue
	                  limitMin: true,     // If true, the min value of the gauge will be fixed
	                  strokeColor: '#E0E0E0',  // to see which ones work best for you
	                  generateGradient: true,
	                  highDpiSupport: true,     // High resolution support
	                  staticZones: staticZones,
	                  staticLabels: {
	                      font: "18px sans-serif",  // Specifies font
	                      labels: [minDeviation, maxDeviation],  // Print labels at these values
	                      color: "#000000",  // Optional: Label text color
	                      fractionDigits: 0  // Optional: Numerical precision. 0=round off.
	                    }
	                };
	                var target = document.getElementById('speedGauge'); // your canvas element
	                var gauge = new Gauge(target).setOptions(opts); // create sexy gauge!
	                gauge.maxValue = maxDeviation; // set max gauge value
	                gauge.setMinValue(minDeviation);  // Prefer setter over gauge.minValue = 0
	                gauge.animationSpeed = 5; // set animation speed (32 is default value)
	                gauge.set(scope.speedGauge); // set actual value	
	            };
			}
		};
	});

    app.filter('productsFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText.length == 0) {
                return items;
            }
            angular.forEach(items, function(item) {
                if(item.Product.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0 || item.Product.production_code.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }]);
    app.filter('plansFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText.length == 0) {
                return items;
            }
            angular.forEach(items, function(item) {
                if(item.Plan.name.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
                    filtered.push(item);
                }
            });
            return filtered;
        };
    }]);
    app.filter('additionalFieldsFilter',[ function (searchText) {
        return function(items, searchText) {
            var filtered = [];
            if(searchText != undefined){
            	if(searchText.length == 0) {
	                return items;
	            }
	            angular.forEach(items, function(item) {
	                if(typeof item === 'string' && item.toLowerCase().indexOf(searchText.toLowerCase()) >= 0) {
	                    filtered.push(item);
	                }
	            });
	            return filtered;
            }else return items;
        };
    }]);
})(angular);
setInterval(function(){location.reload();},43200000);
