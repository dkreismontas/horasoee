

(function($) {
	
	var list = [];
	
	var self = function(e) { this.init(e); };
	self.prototype.init = function(e) {
		this.elem = e;
		this.input = this.elem.children('input[type=hidden]').eq(0);
		var lists = this.elem.children('select');
		this.leftList = lists.eq(0);
		this.rightList = lists.eq(1);
		this.moveLeftBtn = this.elem.find('button.ds_prev').eq(0);
		this.moveRightBtn = this.elem.find('button.ds_next').eq(0);
		var self = this;
		this.moveLeftBtn.on('click', function() { self.moveLeft(); });
		this.moveRightBtn.on('click', function() { self.moveRight(); });
	};
	self.prototype.moveLeft = function() {
		var self = this;
		this.rightList.children('option').each(function() {
			var o = $(this);
			if (o.is(':selected')) {
				o.detach();
				o.removeAttr('selected');
				self.leftList.append(o);
			}
		});
		this.updateInput();
	};
	self.prototype.moveRight = function() {
		var self = this;
		this.leftList.children('option').each(function() {
			var o = $(this);
			if (o.is(':selected')) {
				o.detach();
				o.removeAttr('selected');
				self.rightList.append(o);
			}
		});
		this.updateInput();
	};
	self.prototype.updateInput = function() {
		var val = '';
		this.leftList.children('option').each(function() {
			var o = $(this);
			val += (val ? ';' : '') + o.val();
		});
		this.input.val(val);
	};
	
	$.fn.initDualSelect = function() {
		list.push(new self(this));
		return this;
	};
})(jQuery);
 

function langselect(iso,obj){
	var selectedSplit = iso.split('_');
	var splitCurrentLang = currentLang.split('_');
	if(!selectedSplit[0]){ return false; }else{var selectedLangIso = selectedSplit[0];}
	if(!splitCurrentLang[0]){ return false; }else{var currentLangIso = splitCurrentLang[0];}
	$(obj).closest('div').find('li a').removeClass('langSelected');
	$(obj).addClass('langSelected');
	var inputObj = $(obj).closest('div').find('input,textarea');
	if(!inputObj.attr('modified_value')){
		inputObj.attr('modified_value', inputObj.attr('value'));
	}
	inputObj.attr('language_iso', selectedLangIso);
	//var langPartsFromInput = inputObj.attr('modified_value').match(/\[:([^\]]+)\][^\[]*/ig);
	var langPartsFromInput = inputObj.attr('modified_value').split('[:');
	for(var i in langPartsFromInput){
		if(!$.trim(langPartsFromInput[i]).length){ continue; }
		var langIso = langPartsFromInput[i].substring(0,2);//match(/\[:([^\]]+)\]/i);
		var langText = langPartsFromInput[i].substring(3);//match(/\[:[^\]]+\]((.|\n)*)/i);

		if(langIso == selectedLangIso){
			inputObj.val(langText);
			break;
		}
		inputObj.val('');
	}
	return false;
}

var fixHelper = function(e, ui) {
	ui.children().each(function() {
		jQuery(this).width(jQuery(this).width());
	});
	return ui;
};
function makeSortable(path, modelName){
	jQuery(path).sortable({
		helper: fixHelper,
		containment: "parent",
		items: "> tr",
		update: function(event, ui) {
			jQuery.post("/settings/sort_elements", { order: $(event.target).sortable('toArray', {attribute: "data-sort_item"}), model: modelName} );
		}
	});
}

(function($) {
	$(document).on('blur','input[language_iso],textarea[language_iso]', function(e){
		var langPartsFromInput = $(this).attr('modified_value').split('[:');//.match(/\[:([^\]]+)\][^\[]*/ig);
		var selectedLangIso = $(this).attr('language_iso');
		var langParts = new Array();
		var currentLangExistInString = false;
		for(var i in langPartsFromInput){
			if(!$.trim(langPartsFromInput[i]).length){ continue; }
			if(langPartsFromInput[i].substr(0,3) != selectedLangIso+']'){
				langParts.push('[:'+langPartsFromInput[i]);
			}else{
				langParts.push('[:'+selectedLangIso+']'+$(this).val());
				currentLangExistInString = true;
			}
		}
		if(!currentLangExistInString){ langParts.push('[:'+selectedLangIso+']'+$(this).val()); }
		if($(this).is('input')){
			$(this).attr('value', langParts.join('')).attr('modified_value',langParts.join(''));
		}else if($(this).is('textarea')){
			$(this).text(langParts.join('')).attr('modified_value',langParts.join(''));
		}
		$(this).closest('div').find('ul.langselect a.'+selectedLangIso).trigger('click');
	});
})(jQuery);

jQuery(document).ready(function($) {
	$('ul.langselect a.'+currentLang).trigger('click');
	$('form').bind('submit', function(e){
		$(this).find('input[language_iso],textarea[language_iso]').each(function(){
			$(this).val($(this).attr('modified_value'));
			//console.log($(this).val() + ' - '+$(this).attr('modified_value'));
		});
		//return false;
	});
	$('body').on('keypress', 'input[type="number"]', function(e){
		if(e.keyCode == 44) {
			if(parseFloat($(this).val()) - parseInt($(this).val()) == 0) {
				$(this).val(parseFloat($(this).val()) + '.');
			}
			return false;
		}
	});
});