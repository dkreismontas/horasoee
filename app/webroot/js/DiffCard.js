
(function(angular) {
	
	var app = angular.module('DiffCard', []),
		diffTypeIndex = {},
		indexDiffType = function(diffType) {
			diffTypeIndex['#' + diffType.DiffType.id] = diffType;
			diffTypeIndex['#' + diffType.DiffType.id].DiffType.subs = [];
		},
		getDiffType = function(id) {
			return (('#' + id) in diffTypeIndex) ? diffTypeIndex['#' + id] : null;
		};
	
	app.directive('datePicker', function() {
		return {
			restrict: 'A',
			scope: { datePicker: '=datePicker' },
			link: function (scope, element) { jQuery(element.get(0)).datepicker(window.DiffCard.datePickerConfig).val(new Date().toISOString().split('T')[0]); }
		};
	});
	
	app.directive('timePicker', function() {
		var idIndexer = 0;
		return {
			restrict: 'A',
			scope: { timePicker: '=timePicker' },
			link: function (scope, element) {
				element.parent().addClass('input-group bootstrap-timepicker');
				jQuery(element.get(0)).attr('id', 'dum_picker_id' + idIndexer).timepicker(window.DiffCard.timePickerConfig);
				idIndexer++;
			}
		};
	});
	
	app.controller('DiffCardCtrl', ['$scope', function($scope) {
		var parent, idx,
			diffTypes = window.DiffCard.diffTypes,
			diffTypeId = window.DiffCard.diffTypeId,
			diffSubTypeId = window.DiffCard.diffSubTypeId,
			responsiveUserId = window.DiffCard.responsiveUserId;

        var defOption =JSON.parse('{"DiffType":{"id":"0","parent_id":null,"code":"0","name":"-- '+translations.choose+' --","subs":[],"label":"-- '+translations.choose+' --"}}');
		$scope.diffOption = defOption;
		$scope.diffSubOption = 0;
		$scope.diffOptions = [];
        $scope.diffOptions.push(defOption);
		for (idx in diffTypes) {
			indexDiffType(diffTypes[idx]);
			diffTypes[idx].DiffType.label = diffTypes[idx].DiffType.code + '. ' + diffTypes[idx].DiffType.name;
			if (!diffTypes[idx].DiffType.parent_id) {
				$scope.diffOptions.push(diffTypes[idx]);
				if (!$scope.diffOption) $scope.diffOption = diffTypes[idx];
			}
			if (diffTypeId && parseInt(diffTypes[idx].DiffType.id, 10) === diffTypeId) {
				$scope.diffOption = diffTypes[idx];
			}
			if (diffSubTypeId && parseInt(diffTypes[idx].DiffType.id, 10) === diffSubTypeId) {
				$scope.diffSubOption = diffTypes[idx];
			}
		}
		$scope.$watch(function() {return $scope.diffOption; }, function() {
            if($scope.diffOption == 0) {
                $scope.diffSubOption = 0;
                return;
            }
			if (!$scope.diffSubOption || $scope.diffSubOption.DiffType.parent_id !== $scope.diffOption.DiffType.id) {
				$scope.diffSubOption = null;
				for (idx in $scope.diffOption.DiffType.subs) {
					$scope.diffSubOption = $scope.diffOption.DiffType.subs[idx];
					break;
				}
			}
		});
		for (idx in diffTypes) {
			parent = diffTypes[idx].DiffType.parent_id ? getDiffType(diffTypes[idx].DiffType.parent_id) : null;
			if (parent) {
				parent.DiffType.subs.push(diffTypes[idx]);
			}
		}
        var defUser = JSON.parse('{"User":{"id":"0","username":"-- '+translations.choose+' --","first_name":"","last_name":"","section":"","active":true,"sensor_id":"1","line_id":"1","branch_id":"1","responsive":true,"tech_code":"","label":"-- '+translations.choose+' --"}}');
		$scope.responsiveUser = defUser;
		$scope.responsiveUsers = window.DiffCard.responsiveUsers;
		for (idx in $scope.responsiveUsers) {
			if (!$scope.responsiveUser) $scope.responsiveUser = $scope.responsiveUsers[idx];
			if (responsiveUserId && responsiveUserId === parseInt($scope.responsiveUsers[idx].User.id, 10)) $scope.responsiveUser = $scope.responsiveUsers[idx];
		}
	}]);
	
})(angular);
