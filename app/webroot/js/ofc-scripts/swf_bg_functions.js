var callback = function (e){
    if(e.success && e.ref){
        if(!start_time){
            start_time = getCurrentTime();
            //console.log("Started: " +start_time);
        }
        setTimeout(function checkSWFExistence(){
            if(typeof e.ref.PercentLoaded !== "undefined"){
                checkSWFStatus(e.ref);
            } else {
                checkSWFExistence();
            }
        }, 1000);
    }
};

function checkSWFStatus(swf){
    if(swf.PercentLoaded() !== 100){
        setTimeout(function (){
            checkSWFStatus(swf);
        }, 50);
    } else {
        after_load();
    }
}

function done(id){
        alert("Finished upload. Id:");
    }
    
function findSWF(movieName) {
    if (navigator.appName.indexOf("Microsoft")!= -1) {
        return window[movieName];
    } else {
        return document[movieName];
    }
}
OFC = {};
 
OFC.jquery = {
    name: "jQuery",
    version: function(src) { return $('#'+ src)[0].get_version() },
    rasterize: function (src, dst) { $('#'+ dst).replaceWith(OFC.jquery.image(src)) },
    image: function(src) { return "<img src='data:image/png;base64," + $('#'+src)[0].get_img_binary() + "' />"},
    popup: function(src) {
        var img_win = window.open('', 'Charts: Export as Image')
        with(img_win.document) {
            write('<html><head><title>Charts: Export as Image<\/title><\/head><body>' + OFC.jquery.image(src) + '<\/body><\/html>') }
        // stop the 'loading...' message
        img_win.document.close();
     }
}
 
// Using an object as namespaces is JS Best Practice. I like the Control.XXX style.
//if (!Control) {var Control = {}}
//if (typeof(Control == "undefined")) {var Control = {}}
if (typeof(Control == "undefined")) {var Control = {OFC: OFC.jquery}}
 
 
// By default, right-clicking on OFC and choosing "save image locally" calls this function.
// You are free to change the code in OFC and call my wrapper (Control.OFC.your_favorite_save_method)
// function save_image() { alert(1); Control.OFC.popup('my_chart') }
function save_image() {OFC.jquery.popup('my_chart') }
function moo() { alert(99); };