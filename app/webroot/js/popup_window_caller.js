function popupWindow(domId, duration, width, height, topTitle, closeWindowTitle, callback){
	// Extends the dialog widget with a new option.
    jQuery.widget( "app.dialog", jQuery.ui.dialog, {
        options: {
            iconButtons: []
        },
        _create: function() {
            // Call the default widget constructor.
            this._super();
            // The dialog titlebar is the button container.
            var $titlebar = this.uiDialog.find( ".ui-dialog-titlebar" );
            // Iterate over the "iconButtons" array, which defaults to
            // and empty array, in which case, nothing happens.
            jQuery.each( this.options.iconButtons, function( i, v ) {
                // Finds the last button added. This is actually the
                // left-most button.
                var $button = jQuery( "<a href='#' />" ).text( this.text )
                // Creates the button widget, adding it to the titlebar.
                $button.addClass( this.icon )
                       .css({float: 'right', margin: '0 10px 0 0'})
                       .click( this.click )
                       .appendTo( $titlebar );
            });
    
        }
    
    });
    closeFunction = {}
	closeFunction[closeWindowTitle] = function() {jQuery(this).dialog( "close" );}
	var dialogOpions = {
		autoOpen: false,
        show: {
            effect: "blind",
            duration: duration
        },
        hide: {
            effect: "fade",
            duration: duration
        },
        buttons: closeFunction,
        height: height,
        width: width,
        dialogClass: 'simple',
        title: topTitle,
        position: {
		   my: "center",
		   at: "center",
		   of: window
		},
        resizable: true,
        close: function(){jQuery.isFunction(callback)?callback():''}, 
        iconButtons: [
                {
                    text: "",
                    icon: "icon-fullscreen",
                    click: function( e ) {
                       //jQuery(e.target).parent().find('.ui-dialog-titlebar-close').trigger('click')
                       dialogOpions.height = jQuery(domId).parent().height()+20;
                       var copyOptions = jQuery.extend({},dialogOpions);
                       if(jQuery(e.target).closest('.full-screen').length <= 0){
	                       copyOptions.dialogClass = 'full-screen';
	                       copyOptions.width = 'auto';
	                       copyOptions.height = jQuery(window).height();
	                       copyOptions.position = {
							       my: 'left top',
							       at: "left top",
							       of: jQuery(window)
							    }
					   }
                       jQuery(domId).extractor(copyOptions);
                       jQuery(domId).dialog( "open" )
                       return false;
                    }
                }
                /*  Ga;ima prideti ir daugiau buttonu  }*/
            ]
    }
	jQuery(domId).extractor(dialogOpions);
    jQuery(domId).dialog( "open" );
    jQuery(domId).css('maxHeight',jQuery(window).height()-100)
    jQuery(domId).css('overflow','auto');
}