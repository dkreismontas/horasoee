<?php
//declare(strict_types=1);
//
//if (php_sapi_name() != "cli") {
//	//chdir("../");
//}
//
//spl_autoload_register(function ($class_name) {
//
//	$filename = str_replace('\\', DIRECTORY_SEPARATOR, $class_name) . '.php';
//	echo $filename;
//	include __dir__.'/'.$filename;
//});



pChart_Autoloader::Register();

class pChart_Autoloader
{
    /**
     * Register the Autoloader with SPL
     *
     */
    public static function Register()
    {
        if (function_exists('__autoload')) {
            //    Register any existing autoloader function with SPL, so we don't get any clashes
            spl_autoload_register('__autoload');
        }
        //    Register ourselves with SPL
        if (version_compare(PHP_VERSION, '5.3.0') >= 0) {
            return spl_autoload_register(array('pChart_Autoloader', 'Load'), true, true);
        } else {
            return spl_autoload_register(array('pChart_Autoloader', 'Load'));
        }
    }   //    function Register()


    /**
     * Autoload a class identified by name
     *
     * @param string $pClassName Name of the object to load
     */
    public static function Load($pClassName){
        if ((class_exists($pClassName, FALSE)) || (strpos($pClassName, 'pChart') !== 0)) {
            //    Either already loaded, or not a PHPExcel class request
            return FALSE;
        }
        $pClassFilePath = APP .'Vendor/'.
            str_replace('\\', DIRECTORY_SEPARATOR, $pClassName) .
            '.php';
        if ((file_exists($pClassFilePath) === FALSE) || (is_readable($pClassFilePath) === FALSE)) {
            //    Can't load
            return FALSE;
        }

        require($pClassFilePath);
    }   //    function Load()

}
