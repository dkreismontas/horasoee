## __ API authentication: ______________________________________________ ##

Type: HTTP Basic authentication
Authorization header MUST BE added to every request

# HTTP Basic authentication Python example --------------
import urllib3, base64, json

# ip       = "192.168.1.251"
ip       = "82.135.139.147"
port     = 5580
username = "horas"
password = "%Q}8d~@Uz$%3kmb6"

url  = "http://%s:%d" % (ip, port)
req_url = "/"
headers  = {'Authorization': 'Basic ' + base64.b64encode(bytes(username+':'+password, 'utf-8')).decode('utf-8') }
http = urllib3.PoolManager()

try:
	## Get data from server
	req_url = "/horas/api/v1.0/get"
	response = http.request('GET', url + req_url, headers = headers )

	# print(response.status)
	# print(response.headers)
	# print(response.data)
	print( json.loads(response.data.decode('utf-8')) ) # decode json data

except (TimeoutError):
	print("Server timeout")
except as e:
	print("Unknown error (device turned off, service stopped or wtf), e")

# End of example ----------------------------------------



## __ Requests: __________________________________________________________ ##

GET /
Returns {'status':'OK'}


GET /horas/api/v1.0/get
Gets not sent data. Marks all data as sent.
Returns json array of objects. Example:
{'periods':[
    {"i0":0,"i1":0,"i2":0,"i3":0,"i4":0,"i5":0,"i6":0,"i7":0,"i8":0,"i9":0,"id":17010,
    "sent": 0,"time_from": 1471350366,"time_to": 1471350386},
    {"i0":0,"i1":0,"i2":0,"i3":0,"i4":0,"i5":0,"i6":0,"i7":0,"i8":0,"i9":0,"id":17011,
    "sent": 0,"time_from": 1471350386,"time_to": 1471350406}
]}

Empty data example:
{'periods':[]}

i0 i1 ... i9       -> channel signal counter
sent               -> { 0 | 1 } has data been sent?
time_from, time_to -> EET timezone Unix timestamp in seconds (integer), start and end of period
id				   -> unique number of row (increasing on new data insert)


GET /horas/api/v1.0/get/sent
Gets all sent data.


GET /horas/api/v1.0/get/all
Gets all data.


GET /horas/api/v1.0/get/x
Gets 1 period by x id.


GET /horas/api/v1.0/get/last/x/days
Gets last x days data.


GET /horas/api/v1.0/get/last/x/hours
Gets last x hours data.


GET /horas/api/v1.0/get/last/x/minutes
Gets last x minutes data.


## __ Errors: _____________________________________________________________ ##

Returns {"error":"Not found"}           -> Wrong url or method.
Returns {"error":"Unauthorized access"} -> Authentication failed.

Not responding or Timeout errors 		-> No power, software not running, shuted down, no network, firewall or wtf