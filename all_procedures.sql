DROP PROCEDURE IF EXISTS ADD_FULL_RECORD;
DROP PROCEDURE IF EXISTS ADD_RECORD;
DROP PROCEDURE IF EXISTS ADD_RECORDS_FROM_QUEUE;
DROP PROCEDURE IF EXISTS add_records_from_queue_1;
DROP PROCEDURE IF EXISTS ADD_RECORD_NO_TIME_FIX;
DROP FUNCTION IF EXISTS CREATE_EXCEEDING_TRANSITION_BY_PLAN;
DROP FUNCTION IF EXISTS GET_APPROVED_ORDER_BOX_QUANTITY;
DROP FUNCTION IF EXISTS GET_UNIT_QUANTITY;
DROP PROCEDURE IF EXISTS ADD_LOG;
DROP FUNCTION IF EXISTS FIND_INTERSECT;

DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `ADD_FULL_RECORD`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` DECIMAL(12,4), IN `record_cycle` SMALLINT(6))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `prev_shift_id` INT(11) DEFAULT NULL;
	DECLARE `shift_start` DATETIME DEFAULT NULL;
	DECLARE `active_problem_id` INT(11) DEFAULT NULL;
	DECLARE `active_super_parent_problem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_count` SMALLINT(2) DEFAULT NULL;
	DECLARE `active_transproblem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_start` DATETIME DEFAULT NULL;
	DECLARE `active_fproblem_end` DATETIME DEFAULT NULL;
	DECLARE `active_fproblem_shift_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_duration` INT(11) DEFAULT NULL;
	DECLARE `product_production_time` DECIMAL(10,4) DEFAULT NULL;
	DECLARE `plan_production_time` DECIMAL(10,4) DEFAULT NULL;
	DECLARE `last_work_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `last_work_end` DATETIME DEFAULT NULL;
	DECLARE `active_order_id` INT(11) DEFAULT NULL;
	DECLARE `active_order_start` DATETIME DEFAULT NULL;
	DECLARE `active_plan_id` INT(11) DEFAULT NULL;
	DECLARE `plan_preparation_time` DECIMAL(12,4) DEFAULT NULL;	#plano pasiruosimo laikas
	DECLARE `step` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `box_quantity` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
	DECLARE `new_production_start` DATETIME DEFAULT NULL;		#data, nusakanti kada turi prasideti nauja gamyba, irasoma kai nera plano ir pareina reikiamas irasu kiekiais kiekis. 
	DECLARE `setting_prod_starts_on` DECIMAL(12,4) DEFAULT NULL;		#iraso atsiunciamas kiekis, kuris laikomas tinkamu (vyksta gamyba)
	DECLARE `quantity_records_to_start_order` INT(11) DEFAULT NULL;		#Is jutiklio nustatymo papimamas kiekis, nusakantis kiek geru kiekiu irasu turi pareiti, kad prasidetu gamyba.
	DECLARE `quantity_records_count` INT(11) DEFAULT NULL;		#Is eiles einanciu irasiu su gerais kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti. TODO uzbaigiant esama gamyba ji irgi reikia nuresetinti
	DECLARE `setting_records_count_to_start_problem` SMALLINT(5) DEFAULT 3;		#Is eiles einanciu irasiu su gerais kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti. TODO uzbaigiant esama gamyba ji irgi reikia nuresetinti
	DECLARE `setting_short_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi trumpiau nei nurodytas laikas minutėmis. Pvz.: 343_2
	DECLARE `setting_plan_relate_quantity` VARCHAR(50) DEFAULT NULL;	#nurodo ar planas susietas su record.quantity ar su record.unit_quantity
	DECLARE `setting_long_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi ilgiau nei nurodytas laikas minutėmis. Pvz.: 343_2. Pakeitimas vykdomas keiciantis pamainai
	DECLARE `setting_calculate_quality` SMALLINT(2) DEFAULT NULL;	#0 - kokybe neskaiciuojama, 1 - kokybe skaiciuojama, brokas nurenkamas pries jutikli, 2 - kokybe skaiciuojama, brokas nurenkamas uz jutiklio
	DECLARE `exceeded_transition_id` INT(11) DEFAULT NULL;	#Jeigu po užsakymo pasirinkimo yra viršijama derinimo trukmė, kuri nurodoma plano užsakyme, sistema turi sukurti prastovą. Nurodykite šios prastobos ID (jei 0, funkcionalumas neveikia)
	DECLARE `active_user_id` INT(11) DEFAULT NULL;	#Operatoriaus ID, kuris siuo metu yra prisijunges prie darbo centro
	DECLARE `prev_active_user_id` INT(11) DEFAULT NULL;	#Operatoriaus ID, kuris buvo prisijunges prie sistemos praejusio iraso atsiuntimo metu
	DECLARE `routine_exists` SMALLINT(2) DEFAULT 0;
	DECLARE `quantity_sum_tmp` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `order_losses_count` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `unit_quantity` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `sensor_type` SMALLINT(2) DEFAULT NULL;
	DECLARE `ao_quantity` DECIMAL(12,4) DEFAULT NULL;	#esamo aktyvaus uzsakymo pagamintas kiekis (unit_quantities)
	DECLARE `ao_qualityFacor` DECIMAL(12,4) DEFAULT NULL;	#esamo uzsakymo kokybes koeficientas
	DECLARE `ao_mo_number` VARCHAR(255) DEFAULT NULL;	#esamo uzsakymo kokybes koeficientas
	DECLARE `PROBLEM_UNKNOWN_ID` INT(6) DEFAULT 3;	#Nepazymetos prastovos ID
	
	/*Jei php sukuria prastova, pvz del trukmes virsijimo, gali vienu metu atsirasti kelios aktyvios prastovos. Uzdarome visas aktyvias prastovas, kad vel susikurti tik viena*/
	SELECT COUNT(`fp`.`id`)	INTO `active_fproblem_count` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 LIMIT 1;
	IF(`active_fproblem_count` > 1) THEN
		UPDATE `found_problems` AS `fp` SET `fp`.`state` = 2 WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1;
	END IF;
	
    /*SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	*/
	SELECT `s`.`branch_id`,`s`.`prod_starts_on`,`s`.`quantity_records_count`,`s`.`quantity_records_to_start_order`,`s`.`new_production_start`,`s`.`short_problem`,`s`.`long_problem`,`s`.`records_count_to_start_problem`,`s`.`plan_relate_quantity`,`s`.`type`,`s`.`calculate_quality`
		INTO `branch_id`,`setting_prod_starts_on`,`quantity_records_count`,`quantity_records_to_start_order`,`new_production_start`,`setting_short_problem`,`setting_long_problem`,`setting_records_count_to_start_problem`,`setting_plan_relate_quantity`,`sensor_type`,`setting_calculate_quality`
		FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id`,`sh`.`start` INTO `shift_id`,`shift_start` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	SELECT `shifts`.`id` INTO `prev_shift_id` FROM `shifts` WHERE `shifts`.`start` < `shift_start`  AND `shifts`.`branch_id` = `branch_id` ORDER BY `shifts`.`id` DESC LIMIT 1;
	SELECT `fp`.`id`, TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`), `fp`.`shift_id`,`fp`.`problem_id`, `fp`.`start`, `fp`.`end`, `fp`.`transition_problem_id`,`fp`.`user_id`, `fp`.`super_parent_problem_id`
		INTO `active_fproblem_id`,`active_fproblem_duration`,`active_fproblem_shift_id`, `active_problem_id`, `active_fproblem_start`, `active_fproblem_end`, `active_transproblem_id`, `prev_active_user_id`,`active_super_parent_problem_id`
		FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 LIMIT 1;
	SELECT `ao`.`id`, `ao`.`plan_id`, `plans`.`step`,`plans`.`box_quantity`, `products`.`production_time`,`plans`.`production_time`,`plans`.`preparation_time`,`ao`.`quantity`,`plans`.`mo_number`,`ao`.`created`
		INTO `active_order_id`,`active_plan_id`,`step`,`box_quantity`, `product_production_time`,`plan_production_time`, `plan_preparation_time`,`ao_quantity`,`ao_mo_number`,`active_order_start`
		FROM `approved_orders` AS `ao` LEFT JOIN `plans` ON(`ao`.`plan_id` = `plans`.`id`) LEFT JOIN `products` ON(`products`.`id` = `plans`.`product_id`) WHERE `ao`.`sensor_id` = `sensor_id` AND `ao`.`status_id` = 1 LIMIT 1;
	SELECT `s`.`value` INTO `exceeded_transition_id` FROM `settings` AS `s` WHERE `s`.`key` LIKE 'exceeded_transition_id' LIMIT 1;
	SELECT `u`.`id` INTO `active_user_id` FROM `users` AS `u` WHERE `u`.`active_sensor_id` = sensor_id LIMIT 1;
	IF(`active_user_id` = 0 OR `active_user_id` IS NULL) THEN
		SELECT `s`.`active_user_id` INTO `active_user_id` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	END IF;
	IF(`active_order_id` > 0) THEN
		SELECT SUM(`Loss`.`value`) INTO `order_losses_count` FROM `losses` AS `Loss` WHERE `Loss`.`approved_order_id` = `active_order_id` LIMIT 1;
	END IF;
	
	IF(`active_fproblem_id` > 0 AND TIMESTAMPDIFF(SECOND, `active_fproblem_end`, `date_time`) >= 300) THEN		#jei aktyvios prastovos endas yra mazesnis 5min uz atejusio iraso laika, reiskia buvo dingusi elektra ir dezute negavo duomenu uz ta laika. 
		UPDATE `found_problems` AS `fp` SET `fp`.`state` = 2 WHERE `fp`.`id` = `active_fproblem_id`;	#uzdarome aktyvia prastova, kad butu sukurta nauja nuo iraso atejimo pradzios
		SET `active_fproblem_id` = NULL;
	END IF;
	IF(`active_fproblem_id` > 0 AND `active_fproblem_shift_id` < `shift_id`) THEN	#KEICIASI PAMAINA: uzdarome buvusios pamainos problema ir atidarome nauja
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`shift_start`) - 1), '%Y-%m-%d %H:%i:%s');		#sena pamaina turi uzsidaryti 1s anksciau nei prasideda nauja
		UPDATE `found_problems` AS `fp` SET `fp`.`end` = @subdate, `fp`.`state` = 2 WHERE `fp`.`id` = `active_fproblem_id`;	#uzdarome esama problema, jos pabaiga bus: [new_shift_start - 1s]
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
			NULL, `shift_start`, `date_time`, `active_problem_id`, `active_super_parent_problem_id`, `active_transproblem_id`, `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', `active_fproblem_id`, `quantity`, `active_user_id`);
		SELECT `fp`.`id`, TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`), `fp`.`shift_id`,`fp`.`problem_id`, `fp`.`start`, `fp`.`end`, `fp`.`transition_problem_id`,`fp`.`user_id`, `fp`.`super_parent_problem_id`
			INTO `active_fproblem_id`,`active_fproblem_duration`,`active_fproblem_shift_id`, `active_problem_id`, `active_fproblem_start`, `active_fproblem_end`, `active_transproblem_id`, `prev_active_user_id`,`active_super_parent_problem_id`
			FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 LIMIT 1;
		IF (`active_order_id` IS NULL) THEN		#Jei keiciasi pamaina ir iki siol dar neparinkta gamyba, tuomet resetiname gamybos pradzia, kad gamyba esamoje pamainoje prasidetu nuo esamoje pamainu atsiustu kiekiu, t.y. jos pradzia butu esamoje pamainoje
			UPDATE `sensors` AS `s` SET `s`.`new_production_start` = '0000-00-00' WHERE `s`.`id`=`sensor_id`;
		END IF;
	END IF;
	IF(`active_fproblem_id` > 0 AND (`prev_active_user_id` <> `active_user_id`)) THEN	#KEICIASI PRISIJUNGES USERIS: uzdarome buvusios pamainos problema ir atidarome nauja
		#SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`active_fproblem_end`)+1), '%Y-%m-%d %H:%i:%s');		#nauja prastova turi prasideti 1s veliau
		UPDATE `found_problems` AS `fp` SET `fp`.`state` = 2 WHERE `fp`.`id` = `active_fproblem_id`;	#uzdarome esama problema
		#kai keiciasi useris, dirbtinai prastovos nereikia kurti, pakanka tik ja uzdaryti. Prastova susikurs savaime tolesnio skripto metu
		#INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`user_id`) VALUES (
		#	NULL, @adddate, `date_time`, `active_problem_id`, `active_super_parent_problem_id`, `active_transproblem_id`, `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', `active_fproblem_id`, `active_user_id`);
		#SET `active_fproblem_id` = LAST_INSERT_ID();
		CALL `ADD_LOG`(`sensor_id`, CONCAT('Keiciasi aktyvus vartotojas darbo centre. Uzdaroma prastova id: ', `active_fproblem_id`, ', kuriama nauja prastova.'));
		SET `active_fproblem_id` = NULL;
		SET `PROBLEM_UNKNOWN_ID` = `active_problem_id`;	#po userio pasikeitimo nauja prastova turi susikurti tokio paties tipo, kokia ejo iki tol
	END IF;
	
	SET `active_fproblem_duration` = `active_fproblem_duration` + 1;  #+1s reikia, nes problema kuriame su 19s atgal nuo pirmo problemos recordo
	SET `order_losses_count` = IFNULL(`order_losses_count`,0); 
	SET `step` = IFNULL(`step`,0); 
	SET `box_quantity` = IFNULL(`box_quantity`,0);
	SET `product_production_time` = IF(`product_production_time` = 0 OR `product_production_time` IS NULL, `plan_production_time`*60, `product_production_time`);
	SET `product_production_time` = IFNULL(`product_production_time`,0);
	SET `active_user_id` = IFNULL(`active_user_id`,0);
	SET `prev_active_user_id` = IFNULL(`prev_active_user_id`,0);
	SET `ao_quantity` = IFNULL(`ao_quantity`,0);
	SET `setting_prod_starts_on` = IF(`setting_prod_starts_on` > 0, `setting_prod_starts_on`, 0.1);
	SET `quantity_records_count` = IF(`quantity` >= `setting_prod_starts_on`, `quantity_records_count` + 1, 0);	#skaiciuojame kiek pagal nauajusius irasus yra is eiles einanciu produkciniu irasu.
	SET `active_transproblem_id` = IF(`PROBLEM_UNKNOWN_ID` = 1, `active_transproblem_id`, 0);
	
	IF (`quantity` < `setting_prod_starts_on`) THEN		#Jei atsiunciamas probleminis irasas
		IF(`active_problem_id` = -1 OR `active_fproblem_id` IS NULL) THEN	#nauja problema kuriame tik jei iki siol ejo darbas arba nebuvo problemos
			IF(`active_fproblem_id` > 0) THEN	#jei pries tai vyko gamybos problema (darbas), ja reikia uzdaryti
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;	
			END IF;
			SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
			INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
				NULL, @subdate, `date_time`, `PROBLEM_UNKNOWN_ID`, `PROBLEM_UNKNOWN_ID`, `active_transproblem_id`, `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '', NULL,`quantity`,`active_user_id`);
			SET `active_fproblem_id` = LAST_INSERT_ID();
		ELSEIF (`active_fproblem_id` > 0) THEN	#jei pries tai vyko paprasta problema, ja reikia tiesiog testi
			SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`found_problem_id` = `active_fproblem_id` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta arba nepakanka geru is eiles einanciu irasu prastovai uzsidaryti gali eiti prastova, bet tai vistiek kiekis)
			UPDATE `found_problems` AS `fp` SET `fp`.`end` = `date_time`, `fp`.`quantity` = `quantity_sum_tmp`+`quantity`, `fp`.`plan_id` = `active_plan_id` WHERE `fp`.`id` = `active_fproblem_id`;
		ELSE	#Jei aktyvios prastovos nera, kuriame nauja nepazymeta prastova
			SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
			INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
				NULL, @subdate, `date_time`, `PROBLEM_UNKNOWN_ID`, `PROBLEM_UNKNOWN_ID`, `active_transproblem_id`, `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '', NULL,`quantity`,`active_user_id`);
		END IF;
		IF(`active_plan_id` > 0 AND `active_problem_id` = 1) THEN
			IF(`exceeded_transition_id` > 0) THEN
				SET `active_fproblem_id` = `CREATE_EXCEEDING_TRANSITION_BY_PLAN`(`sensor_id`, `shift_id`, `active_plan_id`, `plan_preparation_time`, `active_fproblem_id`, `exceeded_transition_id`);
			END IF;
		END IF;
	ELSEIF (`active_fproblem_id` IS NULL AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei siuo metu nera aktyvios problemos ir atsiunciamas geras irasas,kuriame darbo problema
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
			NULL, @subdate, `date_time`, -1, -1, '0', `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '', NULL,`quantity`,`active_user_id`);
	ELSEIF (`active_fproblem_id` > 0 AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei yra problema, bet parejo kiekis			
		SET @realProblemDuration = IF(`product_production_time` > 0, `product_production_time`*2, `setting_records_count_to_start_problem` * `record_cycle`);	#suskaiciuojame kokio ilgio turi buti problema, kad ja paliktume
		SELECT MAX(`fp`.`end`) INTO `last_work_end` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = -1 LIMIT 1;				
		SET `last_work_end` = IFNULL(`last_work_end`,0); 
		SET @currproblem_to_lastwork_diff = IF(`last_work_end` > 0,UNIX_TIMESTAMP(`active_fproblem_end`) - UNIX_TIMESTAMP(`last_work_end`), UNIX_TIMESTAMP(`active_fproblem_end`) - UNIX_TIMESTAMP(`active_fproblem_start`));	#laiko skirtumas tarp esamos pamainos aktyvios prastovos pabaigos ir paskutinio darbo pabaigos	
		IF(`active_problem_id` = -1) THEN		#jei siuo metu vyksta gamybos problema (problem_id = -1), pratesiame jos laika
			UPDATE `found_problems` AS `fp` SET `fp`.`end` = `date_time`, `fp`.`quantity` = `fp`.`quantity` + `quantity`, `fp`.`plan_id` = `active_plan_id` WHERE `fp`.`id` = `active_fproblem_id`;
			SET `active_fproblem_id` = NULL;
		ELSEIF((`active_order_id` > 0 AND `active_problem_id` = 1) OR `active_order_id` IS NULL ) THEN		#jei siuo metu yra parinktas uzsakymas ir vyksta derinimas (problem_id = 1) arba nera parinkto plano, pvz vyko Nera darbo:
			IF (`quantity_records_count` >= `quantity_records_to_start_order`) THEN		#derinima uzdaryti tik jei pareina reikiamas geru irasu kiekis (kai nera plano prastova uzdaryti galima tik kai pareina reikiamas geru irasu kiekis)
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;		#uzdarome perejima
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis) arba kol buvo bandyta surinkti reikiamu irasu kieki darbui pradeti
				SET `quantity_sum_tmp` = IFNULL(`quantity_sum_tmp`,0); 
				UPDATE `records` SET `found_problem_id` = NULL WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on`;	#kol bus pasiektas reikiamas irasu kiekis perejimui uzdaryti, irasai su gerais kiekias eina su problema (nezinoma gal sekantis irasas bus probleminis ir derinimas tesis), todel cia jau galime nuo paskutiniu geru irasu nuimti problemas
				SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`active_fproblem_end`)+1), '%Y-%m-%d %H:%i:%s');
				INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`comments`,`quantity`,`user_id`) VALUES (
					NULL, @adddate, `date_time`, -1, -1, '0', `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '{tmp:1}', NULL,'',`quantity_sum_tmp`+`quantity`,`active_user_id`);
				SET `active_fproblem_id` = NULL;
			END IF;
		ELSEIF (@currproblem_to_lastwork_diff < @realProblemDuration AND `active_problem_id` = `PROBLEM_UNKNOWN_ID`) THEN		#jei esamos problemos trukme nuo paskutinio darbo mazesne nei maziausias galimas problemos laikas, problema saliname ir tesiame darba	
			SELECT MAX(`fp`.`id`) INTO `last_work_fproblem_id` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = -1 AND `fp`.`shift_id` = `shift_id` LIMIT 1;	 													
			IF (`last_work_fproblem_id` > 0) THEN		#tikriname ar esamoje pamainoje jau yra darbo irasas
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`found_problem_id` = `active_fproblem_id` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				DELETE FROM `found_problems`  WHERE `found_problems`.`id` = `active_fproblem_id`;
				UPDATE `records` SET `records`.`found_problem_id` = NULL WHERE `records`.`found_problem_id` = `active_fproblem_id`;		#nuresetiname recordamas uzdetas problemas
				UPDATE `found_problems` AS `fp` SET `fp`.`state` = 1, `fp`.`end` = `date_time`, `fp`.`quantity` = `fp`.`quantity`+`quantity_sum_tmp`+`quantity`, `fp`.`plan_id` = `active_plan_id` WHERE `fp`.`id` = `last_work_fproblem_id`;
			ELSE	/*Jei esamoje pamainoje darbo nera, tuomet gali buti, jog keiciasi pamaina ir darbas buvo pradetas praejusioje pamainoje. */
				SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`shift_start`) - 1), '%Y-%m-%d %H:%i:%s');		#sena pamaina turi uzsidaryti 1s anksciau nei prasideda nauja
				DELETE FROM `found_problems` WHERE `found_problems`.`start` > `last_work_end` AND `found_problems`.`sensor_id` = `sensor_id`;
				UPDATE `records` SET `records`.`found_problem_id` = NULL WHERE `records`.`created` > `last_work_end` AND `records`.`sensor_id` = `sensor_id`;		#nuresetiname recordamas uzdetas prastovu id
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`created` > `last_work_end` AND `records`.`sensor_id` = `sensor_id` AND `records`.`shift_id` = `prev_shift_id`;	#Surandame kiekiu suma prastovos metu nuo paskutinio darbo iki buvusios pamainos galo (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				UPDATE `found_problems` AS `fp` SET `fp`.`end` = @subdate, `fp`.`state` = 2, `fp`.`json`='{tmp:2}',`fp`.`quantity`=`fp`.`quantity`+`quantity_sum_tmp` WHERE `fp`.`end` = `last_work_end` AND `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id`=-1;	/*Uzbaigiame darba praejusioje pamainoje*/
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`shift_id` = `shift_id`;	#Surandame kiekiu suma prastovos metu nuo pamainos pradzios iki dabar (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`comments`,`quantity`,`user_id`) VALUES (
					NULL, `shift_start`, `date_time`, -1, -1, '0', `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '{tmp:3}', `active_fproblem_id`,'',`quantity_sum_tmp`+`quantity`,`active_user_id`);	/*kuriame nauja darba darba esamoje*/
			END IF;
			SET `active_fproblem_id` = NULL;
		ELSE
			IF (`quantity_records_count` >= `quantity_records_to_start_order`) THEN
				SET @short_problem_id = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',1));
				SET @short_problem_duration = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',-1));
				IF(@short_problem_id > 0 AND @short_problem_duration > 0 AND @short_problem_duration*60 >= `active_fproblem_duration` AND `active_problem_id` = `PROBLEM_UNKNOWN_ID`) THEN	#Jei yra jutiklio nustatymas trumpas sustojimas ir problema uzsidaroma su mazesniu laiku nei nustatyme, nepazymeta prastova automatiskai virsta i problema pagal nustatyma
					UPDATE `found_problems` SET `state` = 2, `problem_id` = @short_problem_id WHERE `found_problems`.`id` = `active_fproblem_id`;
				ELSE
					UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;
				END IF;
				IF(`active_order_id` > 0) THEN		#Jei patenkama i sia vieta, vadinasi buvo parsiustas kiekis, tikra problema uzsidare, vyksta gmayba, todel kuriame problemini irasa, kuris reiskia vykstanti darba (problem_id = -1)
					SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis) arba kol buvo bandyta surinkti reikiamu irasu kieki darbui pradeti
					SET `quantity_sum_tmp` = IFNULL(`quantity_sum_tmp`,0); 
					SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`active_fproblem_end`)+1), '%Y-%m-%d %H:%i:%s');					
					UPDATE `records` SET `found_problem_id` = NULL WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on`;
					INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `approved_order_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
						NULL, @adddate, `date_time`, -1, -1, '0', `sensor_id`, `shift_id`, `active_plan_id`, `active_order_id`, '1', '0', '0', '', '', NULL,`quantity_sum_tmp`+`quantity`,`active_user_id`);
				END IF;
				SET `active_fproblem_id` = NULL;
			END IF;
		END IF;
	END IF;
	
	#veiksmai, kai keiciasi pamaina (nuo pamainos pradzios nepraejo ilgesnis laikas kaip iraso ciklas)
	IF(UNIX_TIMESTAMP(`date_time`) - UNIX_TIMESTAMP(`shift_start`) <= `record_cycle`) THEN 
		#Jei jutiklis turi nustatyma keiciantis pamainai pakeisti ilgas nepazymetas prastovas i nurodytas nustatyme, tai darome cia
		SET @long_problem_id = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',1));
		SET @long_problem_duration = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',-1));
		IF(@long_problem_id > 0 AND @long_problem_duration > 0 AND `prev_shift_id` > 0) THEN
			UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = @long_problem_id WHERE TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`) >= @long_problem_duration*60 AND `fp`.`shift_id` = `prev_shift_id` AND `fp`.`problem_id` = 3 AND `fp`.`sensor_id` = `sensor_id`;
		END IF;
	END IF;
	
	#skaiciuojame is eiles einanciu geru irasu kiekius bei nustatome kada prasideda nauja gamyba
	UPDATE `sensors` AS `s` SET `s`.`quantity_records_count` = `quantity_records_count` WHERE `s`.`id`=`sensor_id`; 		
	IF(`quantity_records_count` >= `quantity_records_to_start_order` AND `active_order_id` IS NULL AND `new_production_start` <= `shift_start`) THEN
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle` * `quantity_records_count`)), '%Y-%m-%d %H:%i:%s');	#esamas momentas yra kai pasiektas reikiamas irasu kiekius gamybai pradeti. Nuo dabarties atsokame atgal per irasu kiekiu, tada atejo pirmas geras irasas
		UPDATE `sensors` AS `s` SET `s`.`new_production_start` = @subdate WHERE `s`.`id`=`sensor_id`;
	END IF;
	
	SET `unit_quantity` = `GET_UNIT_QUANTITY`(`quantity`,`box_quantity`,`active_plan_id`,`sensor_id`);
	
	#kokybes skaiciavimas
	IF(`setting_calculate_quality` > 0) THEN
		IF(`sensor_type` = 3) THEN		#linijos pabaiga. Jei jutiklis turi ta pati mo linijos pradzioje, kokybes koeficientas kopijuojamas is linijos pradzios. Jei neturi tokio mo, reiskia jutiklis nepriklauso linijai, nes i linijos gala produkcija negali nueiti pirma nepraejusi pradzios. Tokiu atveju skaiciuojame kokybe kaip linijos pradzios jutikliui
			SELECT `ApprovedOrder`.`quality_factor` INTO `ao_qualityFacor` FROM `approved_orders` AS `ApprovedOrder` LEFT JOIN `sensors` AS `Sensor` ON (`Sensor`.`id` = `ApprovedOrder`.`sensor_id`) LEFT JOIN `plans` AS `Plan` ON (`Plan`.`id` = `ApprovedOrder`.`plan_id`) WHERE `Plan`.`mo_number` = `ao_mo_number` AND `Sensor`.`type` = 1 ORDER BY `ApprovedOrder`.`id` DESC LIMIT 1;
		END IF;
		IF(`sensor_type` = 1 OR `ao_qualityFacor` IS NULL) THEN #linijos pradzia arba jutiklis yra linijos pabaigos ir nesuporuotas su pradzia
			SET @allQuantities = IF(`setting_calculate_quality` = 1, `ao_quantity`+`order_losses_count`, `ao_quantity`);
			SET @goodQuantities = IF(`setting_calculate_quality` = 1, `ao_quantity`, `ao_quantity`-`order_losses_count`);
			SET `ao_qualityFacor` = @goodQuantities / @allQuantities;
		END IF;
	END IF;
	
	#jei vykdoma gamyba sumuojame kiekius
	IF(`active_order_id` > 0) THEN
		UPDATE `approved_orders` AS `ao` SET `ao`.`end` = `date_time`, 
			`ao`.`quantity` = `ao`.`quantity` + `unit_quantity`,	
			`ao`.`box_quantity` = GET_APPROVED_ORDER_BOX_QUANTITY(`ao`.`quantity`,`box_quantity`,`active_plan_id`,`sensor_id`),
			`ao`.`quality_factor` = `ao_qualityFacor`
		WHERE `ao`.`id` = `active_order_id`;
		#UPDATE `found_problems` AS `fp` SET `plan_id` = `active_plan_id`, `approved_order_id` = `active_order_id` WHERE `fp`.`approved_order_id` IS NULL AND `fp`.`sensor_id` = `sensor_id` AND `fp`.`start` >= `active_order_start`;
	END IF;
	
	#iterpiame nauja recorda
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`, `user_id`) VALUES (`date_time`, `quantity`, `unit_quantity`, `sensor_id`, `active_plan_id`, `shift_id`, `active_order_id`, `active_fproblem_id`,`active_user_id`);
	
	SELECT COUNT(*) INTO `routine_exists` FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'HOOK_AFTER_RECORD_INSERT' LIMIT 1;
	IF(`routine_exists` > 0) THEN 
		CALL HOOK_AFTER_RECORD_INSERT(`date_time`,`sensor_id`,`branch_id`,`quantity`,`shift_id`,`active_fproblem_id`,`active_order_id`,`active_plan_id`,`record_cycle`); 
	END IF;
END$$

CREATE  PROCEDURE `ADD_RECORD`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` INT(11))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
    SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	
	SELECT `s`.`branch_id` INTO `branch_id` FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id` INTO `shift_id` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`) VALUES (`date_time`, `quantity`, `quantity`, `sensor_id`, NULL, `shift_id`, NULL, NULL);
END$$

--
-- Functions
--
CREATE  FUNCTION `CREATE_EXCEEDING_TRANSITION_BY_PLAN`(`sensor_id` INT(11), `shift_id` INT(11), `active_plan_id` INT(11), `preparation_time` INT(11), `active_fproblem_id` INT(11), `exceeded_transition_id` INT(11)) RETURNS varchar(100) CHARSET latin1 NO SQL
BEGIN
	DECLARE `total_preparation_time` int(11) DEFAULT NULL;
	DECLARE `exceeded_fproblem_id` int(11) DEFAULT NULL;
	DECLARE `active_fproblem_start` DATETIME DEFAULT NULL;
	DECLARE `last_record_created` DATETIME DEFAULT NULL;
	DECLARE `active_user_id` INT(11) DEFAULT NULL;	#Operatoriaus ID, kuris siuo metu yra prisijunges prie darbo centro
	
	SELECT `fp`.`id` INTO `exceeded_fproblem_id` FROM `found_problems` AS `fp` WHERE  `fp`.`plan_id` = `active_plan_id` AND `fp`.`problem_id` = `exceeded_transition_id` AND `fp`.`sensor_id` = `sensor_id` LIMIT 1;
	SELECT `u`.`id` INTO `active_user_id` FROM `users` AS `u` WHERE `u`.`active_sensor_id` = sensor_id LIMIT 1;
	IF(`exceeded_fproblem_id` > 0) THEN		#jei virsijimas jau buvo o dabar vel yra derinimas, derinima iskart verciame  virsijimu
		UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = `exceeded_transition_id` WHERE `fp`.`id` = `active_fproblem_id`;
		CALL `ADD_LOG`(`sensor_id`, CONCAT('Derinimas verciamas i pervirsi su problem_id: ', `exceeded_transition_id`, ', nes esamas plan_id: ', `active_plan_id`, 'jau turejo derinimo virsijima found_problem_id: ', `exceeded_fproblem_id`));
		RETURN `active_fproblem_id`;
	END IF;
	SELECT SUM(TIMESTAMPDIFF(SECOND, `fp`.`start`, `fp`.`end`)) INTO `total_preparation_time` FROM `found_problems` AS `fp` WHERE `fp`.`plan_id` = `active_plan_id` AND `fp`.`sensor_id` = `sensor_id` AND `fp`.problem_id = 1 LIMIT 1;
	IF(`total_preparation_time` > `preparation_time`*60 AND `preparation_time`>0) THEN 
		SELECT `fp`.`start` INTO `active_fproblem_start` FROM `found_problems` AS `fp` WHERE  `fp`.`id` = `active_fproblem_id`;
		SELECT MAX(`r`.`created`) INTO `last_record_created` FROM `records` AS `r` WHERE  `r`.`sensor_id` = `sensor_id` AND `r`.`found_problem_id` = `active_fproblem_id` LIMIT 1;
		SET @allowTransitionEnd = UNIX_TIMESTAMP(`active_fproblem_start`) + (`preparation_time`*60);
		SET @allowTransitionEnd = IF(UNIX_TIMESTAMP() >= @allowTransitionEnd, FROM_UNIXTIME(@allowTransitionEnd,'%Y-%m-%d %H:%i:%s'), NOW());
		UPDATE `found_problems` AS `fp` SET `fp`.`end` = @allowTransitionEnd, `fp`.`state` = 2 WHERE `fp`.`id` = `active_fproblem_id`;
		SET @newProblemStart = FROM_UNIXTIME(UNIX_TIMESTAMP(@allowTransitionEnd) + 1, '%Y-%m-%d %H:%i:%s');
		SET `last_record_created` = IFNULL(`last_record_created`,@newProblemStart);
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`,`user_id`) VALUES (
				NULL, @newProblemStart, `last_record_created`, `exceeded_transition_id`, `exceeded_transition_id`, '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL, '0', `active_user_id`);
		SET @new_active_fproblem_id = LAST_INSERT_ID();
		UPDATE `records` SET `found_problem_id` = @new_active_fproblem_id WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`found_problem_id` = `active_fproblem_id` AND `records`.`created` >= @newProblemStart;
		CALL `ADD_LOG`(`sensor_id`, CONCAT('Derinimas stabdomas ir pradedamas kurti pervirsis. Esamo derinimo prastovos id: ', `active_fproblem_id`, ' pabaiga verciama i ', @allowTransitionEnd, '. Naujos prastovos id: ', @new_active_fproblem_id, ', problem_id: ',`exceeded_transition_id`));
		RETURN @new_active_fproblem_id;
	END IF;
	RETURN `active_fproblem_id`;
END$$

CREATE  FUNCTION `GET_APPROVED_ORDER_BOX_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS decimal(12,4) NO SQL
BEGIN
	RETURN `quantity` / `box_quantity`;
END$$

CREATE  FUNCTION `GET_UNIT_QUANTITY`(`quantity` DECIMAL(12,4), `box_quantity` DECIMAL(12,4), `active_plan_id` INT(11), `sensor_id` INT(11)) RETURNS decimal(12,4) NO SQL
BEGIN
	RETURN `quantity` * `box_quantity`;
END$$

CREATE PROCEDURE `ADD_LOG`(IN `sensor_id` INT, IN `content` TEXT CHARSET utf8) NO SQL
BEGIN
	DECLARE `sensor_name` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
	IF(`content` IS NOT NULL) THEN
		SELECT `s`.`name` INTO `sensor_name` FROM `sensors` AS `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
		INSERT INTO `logs` (`id`, `source`, `user_id`, `sensor`, `content`, `group_stamp`, `created_at`) VALUES (NULL, '', '', `sensor_name`, `content`, 'PROCEDURA', NOW());
	END IF;
END$$

CREATE FUNCTION `FIND_INTERSECT` (`inputList` TEXT, `targetList` TEXT, `flag` TEXT) RETURNS VARCHAR(255) CHARSET latin1 BEGIN
  DECLARE limitCount INT DEFAULT 0 ;
  DECLARE counter INT DEFAULT 0 ;
  DECLARE res TEXT DEFAULT '';
  DECLARE temp TEXT ;
  DECLARE counterVal INT DEFAULT 0;
  SET limitCount = 1 + LENGTH(inputList) - LENGTH(REPLACE(inputList, ',', '')) ;
  SET targetList = IF(LENGTH(targetList)=0, inputList, targetList);
  simple_loop :
  LOOP
    SET counter = counter + 1 ;
    SET temp = SUBSTRING_INDEX(SUBSTRING_INDEX(inputList, ',', counter),',',- 1) ;
    IF FIND_IN_SET(temp, targetList) > 0 AND !FIND_IN_SET(temp, res)
    THEN SET res = CONCAT(res, IF(LENGTH(res)>0,',',''), temp); SET counterVal = counterVal +1;
    END IF ;
    IF counter = limitCount 
    THEN LEAVE simple_loop ;
    END IF ;
  END LOOP simple_loop ;
  IF(flag = 'COUNTER') THEN RETURN counterVal; END IF;
  RETURN res;
END$$


DELIMITER ;