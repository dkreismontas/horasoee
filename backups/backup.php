﻿<?php
class Backup{
    //php -d safe_mode=Off -d disable_functions=. /var/www/html/backups/backup.php

    private static $currentProjectAppPath = '';
    private static $dbLink = null;
    private static $dbConfigs = array();

    function __construct($initBackup = true){
        if($initBackup) {
            $this->createBackup();
        }
    }

    public function createBackup(){
        $dir = Settings::getRootPathToProjectsDir();
        $domains = scandir($dir);
        foreach($domains as $domain) {
            if($domain == '..' || $domain == '.' || !is_dir($dir.$domain)) {
                continue;
            }
            self::$currentProjectAppPath = $dir.$domain.'/app/';
            Settings::setDbConfigs($domain, array('host','prefix','login','password','database','db_backups'));
            if(empty(Settings::$dbConfigs) || !isset(Settings::$dbConfigs['db_backups']) || !Settings::$dbConfigs['db_backups']){ continue; }
            $this->deleteOldBackups($domain);
            extract(Settings::$dbConfigs);
            self::removeSessions();
            if(empty($login) || empty($password) || empty($database)) {
                continue;
            }
            //$this->checkProjectActivity($domain, $login, $password, $database, $prefix);
            //echo $domain ."\r\n".$login."\r\n".$password."\r\n".$database."\r\n-----";
            $date = date("Y-m-dH:i:s");
            if(!file_exists(Settings::getRootPathToBackups().$domain.'/')) {
                umask(0);
                mkdir(Settings::getRootPathToBackups().$domain.'/',0777);
            }
            set_time_limit(30000);
            exec('mysqldump -h '.$host.' --user='.$login.' --password='.$password.' '.$database.' > '.Settings::getRootPathToBackups().$domain.'/'.$database.'_'.$date.'.sql');
            $dbCredentials = array($login, $password, $database);
            $this->createPeriodicalBackup($domain, date('Y-m-d',strtotime("first day of this month")));
            self::dicconnectFromDb();
        }
        gc_collect_cycles();
    }

    public function deleteOldBackups($domain){ // delete old backups
        $delete_if_older = (60 * 60 * 24) * 3; // (60 * 60 * 24) = 1 day . Result = 15 days
        $dir = Settings::getRootPathToBackups().$domain.'/';
        if(!is_dir($dir)){ return null; }
        $files = scandir($dir);
        foreach($files as $f) {
            if($f == '..' || $f == '.' || is_dir($dir.$f)) {
                continue;
            }
            //echo "Full filename: ".$f."\r\n";
            $file_date = explode("_20",$f);
            $file_date = "20".$file_date[1];
            $file_date = explode(".",$file_date);
            $file_date = $file_date[0];
            //echo "File date: " . $file_date."\r\n";
            //echo "Delete if older than: " . date("Y-m-d H:i:s",(time() - $delete_if_older)) ."\r\n";
            if(strtotime($file_date) < (time() - $delete_if_older)) {
                echo "deleting: ".$dir.$f."\r\n";
                unlink($dir.$f);
            }
        }
    }

    function checkProjectActivity($domain, $login, $password, $database, $prefix){
        if(preg_match('/horasmpm\.eu|imperatum\.lt$/i', $domain)){
            self::connectToDb();
            $result = mysqli_query(self::$dbLink, 'SELECT * FROM '.$prefix.'events ORDER BY id DESC LIMIT 1');
            if($result->num_rows){
                $row = $result->fetch_object();
                if(strtotime($row->created) < time() - 5*24*3600 && strtotime($row->created) > time() - 20*24*3600 ){
                    echo "$domain\n";
                    $to      = 'v.tutkus@horasmpm.eu';
                    //$to      = 'd.kreismontas@horasmpm.eu';
                    $subject = $domain.' klientas nustojo naudotis HorasMPM sistema ilgiau kaip 5 dienas';
                    $message = $domain."<br />".'Reikia kažką daryti su šiuo klientu, nes jis nebesinaudojama Horu. Galimas daiktas, kad jam kažkas nebepatinka šioje sistemoje ir reikėtų iš naujo jį sudominti. Jei nieko nedarysim, galim nebegauti aptarnavimo ir visas darbas nueis šuniui ant uodegos.<br />'.
                        'Paskutinis įvykis sistemoje fiksuotas: '.$row->created.' '.$row->action;
                    ;
                    $headers = 'From: noreply@horasmpm.eu' . "\r\n" .
                        'Content-Type: text/html; charset=UTF-8' . "\r\n" .
                        'Reply-To: noreply@horasmpm.eu' . "\r\n" .
                        'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $message, $headers);
                }

            }
        }
    }

    public static function connectToDb(){
        if(self::$dbLink !== null || empty(Settings::$dbConfigs)){ return null; }
        extract(Settings::$dbConfigs);
        self::$dbLink = mysqli_connect($host, $login, $password, $database);
        if (!self::$dbLink) {
            die('Could not connect: ' . mysqli_error(self::$dbLink));
        }
    }
    public static function dicconnectFromDb(){
        if(self::$dbLink == null){ return null; }
        mysqli_close(self::$dbLink);
        self::$dbLink = null;
    }

    private static function removeSessions(){
        $files = glob(self::$currentProjectAppPath.'tmp/sessions/*'); //get all file names
        foreach($files as $file){
            if(is_file($file))
                unlink($file); //delete file
        }
    }

    private function createPeriodicalBackup(String $domain, String $periodDate){
        $year = date('Y',strtotime($periodDate));
        $path = Settings::getRootPathToBackups().'rackraysmb/'.$domain.'/'.$year.'/';
        if(!file_exists($path)) {
            umask(0);
            mkdir($path,0777,true);
        }
        $path .= $periodDate.'.sql';
        if(file_exists($path)){ return null; }
        set_time_limit(30000);
        extract(Settings::$dbConfigs);
        exec('mysqldump -h '.$host.' --user='.$login.' --password='.$password.' '.$database.' > '.$path);
        self::connectToDb();
        @mysqli_query(self::$dbLink, 'TRUNCATE TABLE `records_archive`');
        @mysqli_query(self::$dbLink, 'TRUNCATE TABLE `raw_records`');
    }

}
require_once('settings.php');
//jei sis failas yra includinamas is kito failo, tai jame santi klase turetu buti po namespace zenklu, klase turi struktura Namescpae\Klase. Tokiu atveju nereikia paleisti backup, failas includinamas tik del kitu duomenu, pvz domenu saraso
//if(!array_filter(get_declared_classes() ,function($class){return sizeof(explode("\\", $class)) > 1; })){
    new Backup;
//}


?>