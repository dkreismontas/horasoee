<?php
namespace Log{

    class LogFilter{

        private $mail_message = '';

        function __construct($types, $receivers){
            $dir = \Settings::getRootPathToProjectsDir();
            $domains = scandir($dir);
            foreach($domains as $domain) {
                if($domain == '..' || $domain == '.' || !is_dir($dir.$domain)) {
                    continue;
                }
                set_time_limit(60);
                \Settings::setDbConfigs($domain, array('host','prefix','login','password','database','error_log_reader'));
                if(empty(\Settings::$dbConfigs) || !isset(\Settings::$dbConfigs['error_log_reader']) || !\Settings::$dbConfigs['error_log_reader']){ continue; }
                $path = $dir . $domain . '/app/tmp/logs/error.log';
                $log = $this->findErrorMsgs($path, $types);
                if(count($log) > 0){
                    $this->mail_message .= $this->formatMessage($log, $domain);
                }
            }

            if(strlen($this->mail_message) != 0){
                print_r($this->mail_message);
                $this->sendMail($receivers);
            }
        }

        private function findErrorMsgs($path, $types){
            if(!file_exists($path) ){
                // $this->mail_message .= "Could not find file: " . $path . "\n";
                return [];
            }

            $file = fopen($path,'r+');
            $line_num = 0;
            $log_entries = [];

            while (($line = fgets($file)) !== false) {
                $line_num++;
                if(preg_match("/\[(" . implode("||", $types) . ")\]/", $line) && substr($line, 19,1) !== "!"){
                    array_push($log_entries, [
                        "line" => $line_num,
                        "date" => substr($line, 0, 19),
                        "message" => substr($line, 20),
                    ]);
                    $pointer = ftell($file);
                    // move pointer back to previous line and modify 20th symbol
                    // from: `2020-09-29 10:39:45 Error: [MissingControllerException] Controller class LoginController could not be found.`
                    // to  : `2020-09-29 10:39:45!Error: [MissingControllerException] Controller class LoginController could not be found.`
                    fseek($file, $pointer - strlen($line) + 19);
                    fwrite($file, "!");
                    fseek($file, $pointer);
                }
            }
            fclose($file);
            return $log_entries;
        }

        private function formatMessage($entries, $domain){
            $msg = $domain .":\n";
            $msg .= "| " . str_pad('LineNr', 6, ' ', STR_PAD_BOTH)  . " | " . str_pad('Date', 19, ' ', STR_PAD_BOTH) . " | message\n" ;
            foreach($entries as $entry){
                $msg .= "| " . str_pad($entry["line"], 6, ' ', STR_PAD_LEFT)  . " | " . $entry["date"] . " | " . $entry["message"];
            }
            return $msg . "\n";
        }

        private function sendMail($receivers){
            foreach($receivers as &$email){
                $headers = 'From: klaidos@horasoee.eu';
                mail($email, "Klaidos - " . date("Y-m-d H:i:s"), $this->mail_message,$headers);
            }
        }

    }

    require_once('settings.php');

    new LogFilter(
        ['Error', 'PDOException', 'ArgumentCountError', 'ParseError','Exception'],
        ["d.kreismontas@horasmpm.eu"]
    );
}
