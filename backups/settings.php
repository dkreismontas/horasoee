<?php
class Settings{

    private static $rootPathToBackups = __DIR__.'/';
    private static $rootPathToProjectsDir = '/var/www/html/';
    public static $dbConfigs = array();

    public static function getRootPathToProjectsDir(){
        return self::$rootPathToProjectsDir;
    }

    public static function getRootPathToBackups(){
        return self::$rootPathToBackups;
    }

    public static function setDbConfigs(String $domain, Array $searchParameters){
        self::$dbConfigs = array();
        $database_config_path = self::getRootPathToProjectsDir().$domain.'/app/Config/database.php';
        if(!file_exists($database_config_path) || in_array($domain, array('oeedev.horasoee.eu'))){
            return array();
        }
        $file = fopen($database_config_path,'r');
        $content = fread($file,filesize($database_config_path));
        fclose($file);
        foreach($searchParameters as $searchParameter){
            preg_match("/\'$searchParameter\'\s*=>\s*\'(.+?)\'/", $content,$param);
            if(sizeof($param) == 2) {
                self::$dbConfigs[$searchParameter] = $param[1];
            }
        }
    }
}