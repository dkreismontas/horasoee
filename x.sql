BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `prev_shift_id` INT(11) DEFAULT NULL;
	DECLARE `shift_start` DATETIME DEFAULT NULL;
	DECLARE `active_problem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_start` DATETIME DEFAULT NULL;
	DECLARE `active_fproblem_end` DATETIME DEFAULT NULL;
	DECLARE `active_fproblem_shift_id` INT(11) DEFAULT NULL;
	DECLARE `active_fproblem_duration` INT(11) DEFAULT NULL;
	DECLARE `product_production_time` INT(11) DEFAULT NULL;
	DECLARE `last_work_fproblem_id` INT(11) DEFAULT NULL;
	DECLARE `last_work_end` DATETIME DEFAULT NULL;
	DECLARE `active_order_id` INT(11) DEFAULT NULL;
	DECLARE `active_plan_id` INT(11) DEFAULT NULL;
	DECLARE `plan_preparation_time` DECIMAL(12,4) DEFAULT NULL;	#plano pasiruosimo laikas
	DECLARE `step` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `box_quantity` DECIMAL(12,4) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
	DECLARE `new_production_start` DATETIME DEFAULT NULL;		#data, nusakanti kada turi prasideti nauja gamyba, irasoma kai nera plano ir pareina reikiamas irasu kiekiais kiekis. 
	DECLARE `setting_prod_starts_on` DECIMAL(12,4) DEFAULT NULL;		#iraso atsiunciamas kiekis, kuris laikomas tinkamu (vyksta gamyba)
	DECLARE `quantity_records_to_start_order` INT(11) DEFAULT NULL;		#Is jutiklio nustatymo papimamas kiekis, nusakantis kiek geru kiekiu irasu turi pareiti, kad prasidetu gamyba.
	DECLARE `quantity_records_count` INT(11) DEFAULT NULL;		#Is eiles einanciu irasiu su gerais kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti. TODO uzbaigiant esama gamyba ji irgi reikia nuresetinti
	DECLARE `setting_records_count_to_start_problem` SMALLINT(5) DEFAULT 3;		#Is eiles einanciu irasiu su gerais kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti. TODO uzbaigiant esama gamyba ji irgi reikia nuresetinti
	DECLARE `setting_short_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi trumpiau nei nurodytas laikas minutėmis. Pvz.: 343_2
	DECLARE `setting_long_problem` VARCHAR(50) DEFAULT NULL;	#Problemos id, į kurią virsta nepažymėta prastova, trukusi ilgiau nei nurodytas laikas minutėmis. Pvz.: 343_2. Pakeitimas vykdomas keiciantis pamainai
	DECLARE `exceeded_transition_id` INT(11) DEFAULT NULL;	#Jeigu po užsakymo pasirinkimo yra viršijama derinimo trukmė, kuri nurodoma plano užsakyme, sistema turi sukurti prastovą. Nurodykite šios prastobos ID (jei 0, funkcionalumas neveikia)
	DECLARE `active_user_id` INT(11) DEFAULT NULL;	#Operatoriaus ID, kuris siuo metu yra prisijunges prie darbo centro
	DECLARE `routine_exists` VARCHAR(50) DEFAULT NULL;
	DECLARE `quantity_sum_tmp` DECIMAL(12,4) DEFAULT NULL;
	
    SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	
	SELECT `s`.`branch_id`,`s`.`prod_starts_on`,`s`.`quantity_records_count`,`s`.`quantity_records_to_start_order`,`s`.`new_production_start`,`s`.`short_problem`,`s`.`long_problem`,`s`.`records_count_to_start_problem`
		INTO `branch_id`,`setting_prod_starts_on`,`quantity_records_count`,`quantity_records_to_start_order`,`new_production_start`,`setting_short_problem`,`setting_long_problem`,`setting_records_count_to_start_problem`
		FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id`,`sh`.`start` INTO `shift_id`,`shift_start` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	SELECT `shifts`.`id` INTO `prev_shift_id` FROM `shifts` WHERE `shifts`.`start` < `shift_start`  AND `shifts`.`branch_id` = `branch_id` ORDER BY `shifts`.`id` DESC LIMIT 1;
	SELECT `fp`.`id`, TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`), `fp`.`shift_id`,`fp`.`problem_id`, `fp`.`start`, `fp`.`end`
		INTO `active_fproblem_id`,`active_fproblem_duration`,`active_fproblem_shift_id`, `active_problem_id`, `active_fproblem_start`, `active_fproblem_end`
		FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`state` = 1 LIMIT 1;
	SELECT `ao`.`id`, `ao`.`plan_id`, `plans`.`step`,`plans`.`box_quantity`, `products`.`production_time`,`plans`.`preparation_time` 
		INTO `active_order_id`,`active_plan_id`,`step`,`box_quantity`, `product_production_time`, `plan_preparation_time`
		FROM `approved_orders` AS `ao` LEFT JOIN `plans` ON(`ao`.`plan_id` = `plans`.`id`) LEFT JOIN `products` ON(`products`.`id` = `plans`.`product_id`) WHERE `ao`.`sensor_id` = `sensor_id` AND `ao`.`status_id` = 1 LIMIT 1;
	SELECT `s`.`value` INTO `exceeded_transition_id` FROM `settings` AS `s` WHERE `s`.`key` LIKE 'exceeded_transition_id' LIMIT 1;
	SELECT `u`.`id` INTO `active_user_id` FROM `users` AS `u` WHERE `u`.`active_sensor_id` = sensor_id LIMIT 1;
	
	SET `active_fproblem_duration` = `active_fproblem_duration` + 1;  #+1s reikia, nes problema kuriame su 19s atgal nuo pirmo problemos recordo
	SET `step` = IFNULL(`step`,0); 
	SET `box_quantity` = IFNULL(`box_quantity`,0);
	SET `product_production_time` = IFNULL(`product_production_time`,0);
	SET `active_user_id` = IFNULL(`active_user_id`,0);
	SET `setting_prod_starts_on` = IF(`setting_prod_starts_on` > 0, `setting_prod_starts_on`, 0.1);
	SET `quantity_records_count` = IF(`quantity` >= `setting_prod_starts_on`, `quantity_records_count` + 1, 0);	#skaiciuojame kiek pagal nauajusius irasus yra is eiles einanciu produkciniu irasu.

	IF(`active_fproblem_id` > 0 AND `active_fproblem_shift_id` != `shift_id`) THEN	#KEICIASI PAMAINA: uzdarome buvusios pamainos problema ir atidarome nauja esamai pamainai
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`shift_start`) - 1), '%Y-%m-%d %H:%i:%s');		#sena pamaina turi uzsidaryti 1s anksciau nei prasideda nauja
		UPDATE `found_problems` AS `fp` SET `fp`.`end` = @subdate, `fp`.`state` = 2 WHERE `fp`.`id` = `active_fproblem_id`;	#uzdarome esama problema, jos pabaiga bus: [new_shift_start - 1s]
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`) VALUES (
			NULL, `shift_start`, `date_time`, `active_problem_id`, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', `active_fproblem_id`,`quantity`);
		SET `active_fproblem_id` = LAST_INSERT_ID();
		IF (`active_order_id` IS NULL) THEN		#Jei keiciasi pamaina ir iki siol dar neparinkta gamyba, tuomet resetiname gamybos pradzia, kad gamyba esamoje pamainoje prasidetu nuo esamoje pamainu atsiustu kiekiu, t.y. jos pradzia butu esamoje pamainoje
			UPDATE `sensors` AS `s` SET `s`.`new_production_start` = '0000-00-00' WHERE `s`.`id`=`sensor_id`;
		END IF;
	END IF;
	
	IF (`quantity` < `setting_prod_starts_on`) THEN		#Jei atsiunciamas probleminis irasas
		IF(`active_problem_id` = -1 OR `active_fproblem_id` IS NULL) THEN	#nauja problema kuriame tik jei iki siol ejo darbas arba nebuvo problemos
			IF(`active_fproblem_id` > 0) THEN	#jei pries tai vyko gamybos problema (darbas), ja reikia uzdaryti
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;	
			END IF;
			SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
			INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`) VALUES (
				NULL, @subdate, `date_time`, '3', '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL,`quantity`);
			SET `active_fproblem_id` = LAST_INSERT_ID();
		ELSEIF (`active_fproblem_id` > 0) THEN	#jei pries tai vyko paprasta problema, ja reikia tiesiog testi
			SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`found_problem_id` = `active_fproblem_id` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta arba nepakanka geru is eiles einanciu irasu prastovai uzsidaryti gali eiti prastova, bet tai vistiek kiekis)
			UPDATE `found_problems` AS `fp` SET `fp`.`end` = `date_time`, `fp`.`quantity` = `quantity_sum_tmp`+`quantity` WHERE `fp`.`id` = `active_fproblem_id`;
		END IF;
		IF(`active_plan_id` > 0 AND `active_problem_id` = 1) THEN
			IF(`exceeded_transition_id` > 0) THEN
				SET `active_fproblem_id` = `CREATE_EXCEEDING_TRANSITION_BY_PLAN`(`sensor_id`, `shift_id`, `active_plan_id`, `plan_preparation_time`, `active_fproblem_id`, `exceeded_transition_id`);
			END IF;
		END IF;
	ELSEIF (`active_fproblem_id` IS NULL AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei siuo metu nera aktyvios problemos ir atsiunciamas geras irasas,kuriame darbo problema
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle`-1)), '%Y-%m-%d %H:%i:%s');
		INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`) VALUES (
			NULL, @subdate, `date_time`, -1, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL,`quantity`);
	ELSEIF (`active_fproblem_id` > 0 AND `quantity` >= `setting_prod_starts_on`) THEN		#Jei yra problema, bet parejo kiekis			
		SET @realProblemDuration = IF(product_production_time > 0, product_production_time*2, `setting_records_count_to_start_problem` * `record_cycle`);	#suskaiciuojame kokio ilgio turi buti problema, kad ja paliktume
		SELECT MAX(`fp`.`end`) INTO `last_work_end` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = -1 LIMIT 1;				
		SET `last_work_end` = IFNULL(`last_work_end`,0); 
		SET @currproblem_to_lastwork_diff = IF(`last_work_end` > 0,UNIX_TIMESTAMP(`active_fproblem_end`) - UNIX_TIMESTAMP(`last_work_end`), UNIX_TIMESTAMP(`active_fproblem_end`) - UNIX_TIMESTAMP(`active_fproblem_start`));	#laiko skirtumas tarp esamos pamainos aktyvios prastovos pabaigos ir paskutinio darbo pabaigos	
		IF(`active_problem_id` = -1) THEN		#jei siuo metu vyksta gamybos problema (problem_id = -1), pratesiame jos laika
			UPDATE `found_problems` AS `fp` SET `fp`.`end` = `date_time`, `fp`.`quantity` = `fp`.`quantity` + `quantity` WHERE `fp`.`id` = `active_fproblem_id`;
			SET `active_fproblem_id` = NULL;
		ELSEIF((`active_order_id` > 0 AND `active_problem_id` = 1) OR `active_order_id` IS NULL ) THEN		#jei siuo metu yra parinktas uzsakymas ir vyksta derinimas (problem_id = 1) arba nera parinkto plano, pvz vyko Nera darbo:
			IF (`quantity_records_count` >= `quantity_records_to_start_order`) THEN		#derinima uzdaryti tik jei pareina reikiamas geru irasu kiekis (kai nera plano prastova uzdaryti galima tik kai pareina reikiamas geru irasu kiekis)
				UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;		#uzdarome perejima
				UPDATE `records` SET `found_problem_id` = NULL WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on`;	#kol bus pasiektas reikiamas irasu kiekis perejimui uzdaryti, irasai su gerais kiekias eina su problema (nezinoma gal sekantis irasas bus probleminis ir derinimas tesis), todel cia jau galime nuo paskutiniu geru irasu nuimti problemas
				SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`active_fproblem_end`)+1), '%Y-%m-%d %H:%i:%s');
				INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`comments`,`quantity`) VALUES (
					NULL, @adddate, `date_time`, -1, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL,'tmp1',`quantity`);
				SET `active_fproblem_id` = NULL;
			END IF;
		ELSEIF (@currproblem_to_lastwork_diff < @realProblemDuration AND `active_problem_id` = 3) THEN		#jei esamos problemos trukme nuo paskutinio darbo mazesne nei maziausias galimas problemos laikas, problema saliname ir tesiame darba	
			SELECT MAX(`fp`.`id`) INTO `last_work_fproblem_id` FROM `found_problems` AS `fp` WHERE `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id` = -1 AND `fp`.`shift_id` = `shift_id` LIMIT 1;	 													
			IF (`last_work_fproblem_id` > 0) THEN		#tikriname ar esamoje pamainoje jau yra darbo irasas
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`found_problem_id` = `active_fproblem_id` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				DELETE FROM `found_problems`  WHERE `found_problems`.`id` = `active_fproblem_id`;
				UPDATE `records` SET `records`.`found_problem_id` = NULL WHERE `records`.`found_problem_id` = `active_fproblem_id`;		#nuresetiname recordamas uzdetas problemas
				UPDATE `found_problems` AS `fp` SET `fp`.`state` = 1, `fp`.`end` = `date_time`, `fp`.`quantity` = `fp`.`quantity`+`quantity_sum_tmp`+`quantity` WHERE `fp`.`id` = `last_work_fproblem_id`;
			ELSE	/*Jei esamoje pamainoje darbo nera, tuomet gali buti, jog keiciasi pamaina ir darbas buvo pradetas praejusioje pamainoje. */
				SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`shift_start`) - 1), '%Y-%m-%d %H:%i:%s');		#sena pamaina turi uzsidaryti 1s anksciau nei prasideda nauja
				DELETE FROM `found_problems` WHERE `found_problems`.`start` > `last_work_end` AND `found_problems`.`sensor_id` = `sensor_id`;
				UPDATE `records` SET `records`.`found_problem_id` = NULL WHERE `records`.`created` > `last_work_end` AND `records`.`sensor_id` = `sensor_id`;		#nuresetiname recordamas uzdetas prastovu id
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`created` > `last_work_end` AND `records`.`sensor_id` = `sensor_id` AND `records`.`shift_id` = `prev_shift_id`;	#Surandame kiekiu suma prastovos metu nuo paskutinio darbo iki buvusios pamainos galo (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				UPDATE `found_problems` AS `fp` SET `fp`.`end` = @subdate, `fp`.`state` = 2, `fp`.`comments`='tmp2',`fp`.`quantity`=`fp`.`quantity`+`quantity_sum_tmp` WHERE `fp`.`end` = `last_work_end` AND `fp`.`sensor_id` = `sensor_id` AND `fp`.`problem_id`=-1;	/*Uzbaigiame darba praejusioje pamainoje*/
				SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`shift_id` = `shift_id`;	#Surandame kiekiu suma prastovos metu nuo pamainos pradzios iki dabar (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
				INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`comments`,`quantity`) VALUES (
					NULL, `shift_start`, `date_time`, -1, '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', `active_fproblem_id`,'tmp3',`quantity_sum_tmp`+`quantity`);	/*kuriame nauja darba darba esamoje*/
			END IF;
			SET `active_fproblem_id` = NULL;
		ELSE
			IF (`quantity_records_count` >= `quantity_records_to_start_order`) THEN
				SET @short_problem_id = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',1));
				SET @short_problem_duration = TRIM(SUBSTRING_INDEX(`setting_short_problem`,'_',-1));
				IF(@short_problem_id > 0 AND @short_problem_duration > 0 AND @short_problem_duration*60 >= `active_fproblem_duration` AND `active_problem_id` = 3) THEN	#Jei yra jutiklio nustatymas trumpas sustojimas ir problema uzsidaroma su mazesniu laiku nei nustatyme, nepazymeta prastova automatiskai virsta i problema pagal nustatyma
					UPDATE `found_problems` SET `state` = 2, `problem_id` = @short_problem_id WHERE `found_problems`.`id` = `active_fproblem_id`;
				ELSE
					UPDATE `found_problems` SET `state` = 2 WHERE `found_problems`.`id` = `active_fproblem_id`;
				END IF;
				IF(`active_order_id` > 0) THEN		#Jei patenkama i sia vieta, vadinasi buvo parsiustas kiekis, tikra problema uzsidare, vyksta gmayba, todel kuriame problemini irasa, kuris reiskia vykstanti darba (problem_id = -1)
					SELECT SUM(`records`.`quantity`) INTO `quantity_sum_tmp` FROM `records` WHERE `records`.`found_problem_id` = `active_fproblem_id` AND `records`.`created` > `active_fproblem_end` LIMIT 1;	#Surandame kiekiu suma prastovos metu (kai kiekis mazesnis nei nurodyta gali eiti prastova, bet tai vistiek kiekis)
					SET `quantity_sum_tmp` = IFNULL(`quantity_sum_tmp`,0); 
					SET @adddate = FROM_UNIXTIME((UNIX_TIMESTAMP(`active_fproblem_end`)+1), '%Y-%m-%d %H:%i:%s');					
					UPDATE `records` SET `found_problem_id` = NULL WHERE `records`.`sensor_id` = `sensor_id` AND `records`.`created` > `active_fproblem_end` AND `records`.`quantity` >= `setting_prod_starts_on`;
					INSERT INTO `found_problems` (`id`, `start`, `end`, `problem_id`, `super_parent_problem_id`, `transition_problem_id`, `sensor_id`, `shift_id`, `plan_id`, `state`, `diff_card_id`, `while_working`, `mail_inform_sended`, `json`, `prev_shift_problem_id`,`quantity`) VALUES (
						NULL, @adddate, `date_time`, '-1', '0', '0', `sensor_id`, `shift_id`, `active_plan_id`, '1', '0', '0', '', '', NULL,`quantity_sum_tmp`+`quantity`);
				END IF;
				SET `active_fproblem_id` = NULL;
			END IF;
		END IF;
	END IF;
	
	#veiksmai, kai keiciasi pamaina (nuo pamainos pradzios nepraejo ilgesnis laikas kaip iraso ciklas)
	IF(UNIX_TIMESTAMP(`date_time`) - UNIX_TIMESTAMP(`shift_start`) <= `record_cycle`) THEN 
		#Jei jutiklis turi nustatyma keiciantis pamainai pakeisti ilgas nepazymetas prastovas i nurodytas nustatyme, tai darome cia
		SET @long_problem_id = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',1));
		SET @long_problem_duration = TRIM(SUBSTRING_INDEX(`setting_long_problem`,'_',-1));
		IF(@long_problem_id > 0 AND @long_problem_duration > 0 AND `prev_shift_id` > 0) THEN
			UPDATE `found_problems` AS `fp` SET `fp`.`problem_id` = @long_problem_id WHERE TIMESTAMPDIFF(SECOND,`fp`.`start`,`fp`.`end`) >= @long_problem_duration*60 AND `fp`.`shift_id` = `prev_shift_id` AND `fp`.`problem_id` = 3 AND `fp`.`sensor_id` = `sensor_id`;
		END IF;
	END IF;
	
	#skaiciuojame is eiles einanciu geru irasu kiekius bei nustatome kada prasideda nauja gamyba
	UPDATE `sensors` AS `s` SET `s`.`quantity_records_count` = `quantity_records_count` WHERE `s`.`id`=`sensor_id`; 		
	IF(`quantity_records_count` >= `quantity_records_to_start_order` AND `active_order_id` IS NULL AND `new_production_start` <= `shift_start`) THEN
		SET @subdate = FROM_UNIXTIME((UNIX_TIMESTAMP(`date_time`) - (`record_cycle` * `quantity_records_count`)), '%Y-%m-%d %H:%i:%s');	#esamas momentas yra kai pasiektas reikiamas irasu kiekius gamybai pradeti. Nuo dabarties atsokame atgal per irasu kiekiu, tada atejo pirmas geras irasas
		UPDATE `sensors` AS `s` SET `s`.`new_production_start` = @subdate WHERE `s`.`id`=`sensor_id`;
	END IF;
	
	#jei vykdoma gamyba sumuojame kiekius
	IF(`active_order_id` > 0) THEN
		UPDATE `approved_orders` AS `ao` SET `ao`.`end` = `date_time`, `ao`.`quantity` = `ao`.`quantity` + `GET_UNIT_QUANTITY`(`quantity`,`box_quantity`,`active_plan_id`,`sensor_id`),	`ao`.`box_quantity` = GET_APPROVED_ORDER_BOX_QUANTITY(`ao`.`quantity`,`box_quantity`,`active_plan_id`,`sensor_id`) WHERE `ao`.`id` = `active_order_id`;
	END IF;
	
	#iterpiame nauja recorda
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`, `user_id`) VALUES (`date_time`, `quantity`, `GET_UNIT_QUANTITY`(`quantity`,`box_quantity`,`active_plan_id`,`sensor_id`), `sensor_id`, `active_plan_id`, `shift_id`, `active_order_id`, `active_fproblem_id`,`active_user_id`);
	
	SELECT ROUTINE_NAME INTO `routine_exists` FROM INFORMATION_SCHEMA.ROUTINES WHERE ROUTINE_NAME = 'HOOK_AFTER_RECORD_INSERT' LIMIT 1;
	IF(LENGTH(`routine_exists`) > 0) THEN 
		CALL HOOK_AFTER_RECORD_INSERT(`date_time`,`sensor_id`,`quantity`,`shift_id`,`active_fproblem_id`,`active_order_id`,`active_plan_id`); 
	END IF;
END