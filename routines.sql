DELIMITER $$
--
-- Procedures
--
CREATE  PROCEDURE `ADD_RECORD`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` INT(11))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;
	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
    SELECT CONVERT_TZ(`date_time`, '+02:00', 'Europe/Vilnius') INTO `date_time_fix`; 	
	SELECT `s`.`branch_id` INTO `branch_id` FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;
	SELECT `sh`.`id` INTO `shift_id` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`) VALUES (`date_time`, `quantity`, `quantity`, `sensor_id`, NULL, `shift_id`, NULL, NULL);
END$$

CREATE  PROCEDURE `ADD_RECORDS_FROM_QUEUE`()
    NO SQL
BEGIN
   DECLARE rec_source int;
   DECLARE rec_date datetime;
   DECLARE rec_value int;
   
   DECLARE exit_loop BOOLEAN;    
   DECLARE records_cursor CURSOR FOR
     SELECT source, date, value FROM vds_pergaleoee.SensorProfileRecords order by date;
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;
   
   DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
   DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;


   START TRANSACTION;
	   
       
       
       
	   OPEN records_cursor;
	   records_loop: LOOP
		 FETCH  records_cursor INTO rec_source, rec_date, rec_value;
         
         IF exit_loop THEN
             LEAVE records_loop;			 			 
		 END IF;
		 
         
		 CALL vds_pergaleoee.ADD_RECORD(rec_date, rec_source, rec_value);

         DELETE FROM vds_pergaleoee.SensorProfileRecords 
		 WHERE source = rec_source AND date = rec_date;
         
         
		 
	   END LOOP records_loop;
	   CLOSE records_cursor;
       
   COMMIT;
END$$

CREATE  PROCEDURE `add_records_from_queue_1`()
BEGIN
   DECLARE rec_source int;
   DECLARE rec_date datetime;
   DECLARE rec_value int;

   DECLARE exit_loop BOOLEAN;

   DECLARE records_cursor CURSOR FOR
     SELECT source, date, value FROM vds_pergaleoee.sensorprofilerecords order by date;
   DECLARE CONTINUE HANDLER FOR NOT FOUND SET exit_loop = TRUE;

   DECLARE EXIT HANDLER FOR SQLEXCEPTION ROLLBACK;
   DECLARE EXIT HANDLER FOR SQLWARNING ROLLBACK;

   START TRANSACTION;

         OPEN records_cursor;
         records_loop: LOOP

              FETCH  records_cursor INTO rec_source, rec_date, rec_value;

              IF exit_loop THEN
                LEAVE records_loop;
              END IF;


              CALL vds_pergaleoee.ADD_RECORD(rec_date, rec_source, rec_value);
              DELETE FROM vds_pergaleoee.sensorprofilerecords
              WHERE source = rec_source AND date = rec_date;


         END LOOP records_loop;
         CLOSE records_cursor;

   COMMIT;
END$$

CREATE  PROCEDURE `ADD_RECORD_NO_TIME_FIX`(IN `date_time` DATETIME, IN `sensor_id` INT(11), IN `quantity` INT(11))
BEGIN
	DECLARE `shift_id` INT(11) DEFAULT NULL;
	DECLARE `branch_id` INT(11) DEFAULT NULL;

	DECLARE `date_time_fix` DATETIME DEFAULT NULL;
	
	SELECT `s`.`branch_id` INTO `branch_id` FROM `sensors` `s` WHERE `s`.`id`=`sensor_id` LIMIT 1;

	SELECT `sh`.`id` INTO `shift_id` FROM `shifts` `sh` WHERE `sh`.`start`<=`date_time` AND `sh`.`end`>=`date_time` AND `sh`.`branch_id`=`branch_id` ORDER BY `sh`.`start` DESC LIMIT 1;
	INSERT INTO `records` (`created`, `quantity`, `unit_quantity`, `sensor_id`, `plan_id`, `shift_id`, `approved_order_id`, `found_problem_id`) VALUES (`date_time`, `quantity`, `quantity`, `sensor_id`, NULL, `shift_id`, NULL, NULL);
END$$

DELIMITER ;