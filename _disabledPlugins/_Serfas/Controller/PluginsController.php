<?php
App::uses('SerfasAppController', 'Serfas.Controller');

class PluginsController extends SerfasAppController {
	
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        die();
    }
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        foreach($sensorsListInDB as $sensorId => $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $context = stream_context_create(array (
            'http' => array (
                'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            )
        ));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id', 'pin_name')));
        $saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $data = json_decode(file_get_contents('http://195.14.160.210:5580/horas/api/v1.0/get', false, $context));
        $db = ConnectionManager::getDataSource('default');
        foreach($data->periods as $record){
            foreach($saveSensors as $sensorId => $sensorName){
                $quantity = $record->$sensorName;
                $db->query('CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ');
            }
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->moveOldRecords();
        $this->requestAction('/work-center/update-status?update_all=1');
        die();
    }

    public function moveOldRecords() {
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 100 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 100;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
    }
	
}
