<?php
class Fasa extends AppModel{
     
     public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Fasa/css/Fasa_bare.css';
     }

     public function changeUpdateStatusVariablesHook(&$fSensor, &$plans, &$currentPlan, &$quantityOutput, &$fShift, &$fTransitionProblemTypes, &$allPlans, &$fzeroTimeTransitionProblemTypes, &$dcHoursInfoBlocks,&$hours){
         $fSensor['Sensor']['tv_speed_deviation'] = ''; //nerodomas greicio grafikas
     }

     public function Tv_parametersCalculationAfterValuesSet_Hook(&$sensor){
         $sensor['client_speed_vnt_min'] = 0; //nerodomas greicio grafikas
     }
}
    