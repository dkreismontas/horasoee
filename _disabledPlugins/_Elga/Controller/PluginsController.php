<?php
App::uses('ElgaAppController', 'Elga.Controller');

class PluginsController extends ElgaAppController {
    
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        die();
    }
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        $context = stream_context_create(array (
            'http' => array (
                'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            )
        ));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id', 'pin_name')));
        $saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        $data = json_decode(file_get_contents('http://88.119.156.58:5580/data', false, $context));
        $db = ConnectionManager::getDataSource('default');
        foreach($data->periods as $record){
            foreach($saveSensors as $sensorId => $sensorName){
                $quantity = $record->$sensorName;
                $db->query('CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ');
            }
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->moveOldRecords();
        
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }

    public function moveOldRecords() {
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 120 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 100 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 100;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        die();
    }
    
}
