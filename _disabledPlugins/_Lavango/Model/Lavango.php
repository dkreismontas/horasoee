<?php
class Lavango extends AppModel{

    public function Workcenter_afterBeforeRender_Hook(&$bodyClass){
        $bodyClass[] = '/../Lavango/css/Lavango_bare.css';
    }

    public function Record_calculateOee_Hook(&$oeeParams, $data){
        $oeeParams['operational_factor'] = 1;
        $oeeParams['oee'] = round(100 * $oeeParams['exploitation_factor'] * $oeeParams['operational_factor'], 2);
        $oeeParams['oee_exclusions'] = round(100 * $oeeParams['exploitation_factor_exclusions'] * $oeeParams['operational_factor'], 2);
    }

}
    