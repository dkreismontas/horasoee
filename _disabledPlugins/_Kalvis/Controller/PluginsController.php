<?php
App::uses('KalvisAppController', 'Kalvis.Controller');

class PluginsController extends KalvisAppController {
	
    public $uses = array('Shift','Record','Sensor');
    
    public function index(){
        die();
    }
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = 'linkolnas';
        // $context = stream_context_create(array (
            // 'http' => array (
                // 'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            // )
        // ));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id', 'pin_name')));
        $saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        //$data = json_decode(file_get_contents('http://88.119.156.70:5580/data', false, $context));
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://88.119.156.70:5580/data',
        );
        $mailOfErrors = array();
        foreach($adressList as $addressPort => $adress){
            //$data = json_decode(file_get_contents($adress, false, $context));
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $sensorName){
                    if(!isset($record->$sensorName)) continue;
                    $quantity = $record->$sensorName;
                    $db->query('CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ');
                }
            }
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->moveOldRecords();
        if(!empty($mailOfErrors)){ $this->checkRecordsIsPushing($mailOfErrors); }
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }

    private function moveOldRecords() {
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        die();
    }
    
    private function checkRecordsIsPushing($message){
        $settings = $this->Settings->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList))return;
        $minutes = explode(':', $settings);
        $minutes = isset($minutes[1]) && is_numeric(trim($minutes[1]))?floatval(trim($minutes[1])):0;
        if(!$minutes) $minutes = 60;
        $markerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > $minutes*60){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, 1);
            fclose($fh);
            $headers = "Content-Type: text/plain; charset=UTF-8";
            mail(implode(',',$emailsList),__("Įrašų siuntimo problema ").Configure::read('companyTitle'),implode(";\n",$message),$headers);
        }
    }
	
}
