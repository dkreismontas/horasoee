<?php
App::uses('SystemairAppController', 'Systemair.Controller');

class PluginsController extends SystemairAppController {
        
    public $uses = array('Shift','Record','Sensor');
    
    function save_sensors_data(){
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id','branch_id')));
        $db = ConnectionManager::getDataSource('default');
        $branches = array_unique(array_values($sensorsListInDB));
        foreach($branches as $branchId){
            //jei reikia importuojame pamainas
            $currentShift = $this->Shift->findCurrent($branchId, new DateTime());
            if(!$currentShift){
                App::import('Controller', 'Cron');
                $CronController = new CronController;
                $CronController->generateShifts();
                break;
            }
        }
        //vykdome iraso iterpima i DB
        $saveSensors = array('i0','i1','i2','i3','i4','i5','i6','i7','i8','i9','i10','i11','i12','i13','i14','i15','i16','i17','i18','i19','i20');
        $username = 'horas';
        $password = '%Q}8d~@Uz$%3kmb6';
        // $context = stream_context_create(array (
            // 'http' => array (
                // 'header' => 'Authorization: Basic ' . base64_encode("$username:$password")
            // )
        // ));
        $sensorsListInDB = $this->Sensor->find('list', array('fields'=>array('id', 'pin_name')));
        $saveSensors = array_intersect($sensorsListInDB, $saveSensors);
        //$data = json_decode(file_get_contents('http://88.119.128.249:5580/data', false, $context)); 
        $db = ConnectionManager::getDataSource('default');
        $adressList = array(
            '5580'=>'http://88.119.128.249:5580/data',
        );
        $mailOfErrors = array();
        foreach($adressList as $addressPort => $adress){
            //$data = json_decode(file_get_contents($adress, false, $context));
            $ch = curl_init($adress);
            curl_setopt($ch, CURLOPT_FRESH_CONNECT, true);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Basic ' . base64_encode("$username:$password")
            ));
            $data = curl_exec($ch);
            $http = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            if(curl_errno($ch) == 0 AND $http == 200) {
                $data = json_decode($data);
            }else{
                $mailOfErrors[] = __('Negaunami duomenys iš adreso %s', $adress);
                continue;
            }
            foreach($data->periods as $record){
                foreach($saveSensors as $sensorId => $sensorName){
                    $quantity = $record->$sensorName;
                    try{
                        if(Configure::read('ADD_FULL_RECORD')){
                            $query = 'CALL ADD_FULL_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.','.Configure::read('recordsCycle').');';
                        }else{
                            $query = 'CALL ADD_RECORD(\''.date('Y-m-d H:i:s', $record->time_to).'\', '.$sensorId.', '.$quantity.'); ';  
                        }
                        $db->query($query);
                    }catch(Exception $e){
                        $this->checkRecordsIsPushing(array('Nepavyko įrašyti gautų duomenų iš jutiklių. Patikrinkite įrašymo SQL procedūrą. Vykdyta užklausa: '.$query),2);
                    }
                }
            }
        }
        $dataNotSendingMarkerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!empty($mailOfErrors)){
            $this->checkRecordsIsPushing($mailOfErrors); 
        }elseif(file_exists($dataNotSendingMarkerPath)){
            unlink($dataNotSendingMarkerPath); 
        }
        //sutvarkome atsiustu irasu duomenis per darbo centra
        $this->moveOldRecords();
        $markerPath = __dir__.'/../webroot/files/last_sensors_update.txt';
        if(!file_exists($markerPath) || time() - filemtime($markerPath) > 300 ){
            $fh = fopen($markerPath, 'w') or die("negalima irasyti failo");
            fwrite($fh, 1);
            fclose($fh);
            $this->requestAction('/work-center/update-status?update_all=1');
            unlink($markerPath);
        }
        session_destroy();
        die();
    }

    private function moveOldRecords(){
        $this->render(false);
        $path = __dir__.'/../webroot/files/last_export.txt';
        if(file_exists($path) && time() - filemtime($path) <= 300 ){
            return;
        }
        $interval = $this->Record->archiveDuration;
        $q = 'insert into records_archive select * from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500 ON DUPLICATE KEY UPDATE records_archive.quantity=records_archive.quantity;
              delete from records where created <= (NOW() - INTERVAL '.$interval.') ORDER BY id ASC LIMIT 500;';
        $db = ConnectionManager::getDataSource('default');
        $db->query($q);
        $fh = fopen($path, 'w') or die("negalima irasyti failo");
        fwrite($fh, 1);
        fclose($fh);
        die();
    }
    
    private function checkRecordsIsPushing($message,$type=1){
        pr($message);
        $markerPath = __dir__.'/../webroot/files/last_push_check.txt';
        if(!file_exists($markerPath)){
            $fh = fopen($markerPath, 'w');
            fwrite($fh, time()+300); //leisime siusti pirma maila uz 5min jei vis dar neis irasai
            fclose($fh);
            return false;
        }
        $settings = $this->Settings->getOne('warn_about_no_records');
        if(!trim($settings)) return;
        $emailsList = explode('|',current(explode(':', $settings)));
        array_walk($emailsList, function(&$data){$data = trim($data);});
        if(empty($emailsList))return;
        $minutes = explode(':', $settings);
        $minutes = isset($minutes[1]) && is_numeric(trim($minutes[1]))?floatval(trim($minutes[1])):0;
        if(!$minutes) $minutes = 60;
        switch($type){
            case 1: $subject = __("Įrašų siuntimo problema ").Configure::read('companyTitle'); break;
            case 2: $subject = (Configure::read('ADD_FULL_RECORD')?'ADD_FULL_RECORD':'ADD_RECORD').' '.__('vykdymo problema').' '.Configure::read('companyTitle'); break;
        }
        $fh = fopen($markerPath, "c+") or die("Unable to open file!");
        $time = fread($fh,filesize($markerPath));
        if(time() - $time >= 0){
            $headers = "Content-Type: text/plain; charset=UTF-8";
            mail(implode(',',$emailsList),$subject,implode(";\n",$message),$headers);
            fwrite($fh, time()+$minutes*60); //leisime siusti sekanti maila uz nustatyme nurodyto laiko kas kiek siusti mailus
        }
        fclose($fh);
    }
    
}
