<?php
SET @currOrdNr := 0, @prevAppOrder := 0, @prevQuantity := 0; 
SELECT 
	plans.mo_number, 
	COUNT(*) * 20 /3600*100 AS houre_percent, 
	COUNT(*) * 20 / 60 AS minutes, 
	DATE_FORMAT(tbl1.created, '%H') AS houre, 
	tbl1.created, 
	found_problem_id 
	FROM (
		SELECT 
			plan_id,
			CASE WHEN approved_order_id != @prevAppOrder OR found_problem_id IS NULL AND approved_order_id IS NULL AND quantity > 0 AND @prevQuantity = 0 
				THEN @currOrdNr := @currOrdNr + 1 
			END AS exp1, 
			CASE WHEN approved_order_id != @prevAppOrder 
				THEN @prevAppOrder := approved_order_id 
			END AS exp2, 
			@prevQuantity := quantity, 
			CASE WHEN found_problem_id IS NULL AND approved_order_id > 0 
					THEN CONCAT('order_',@currOrdNr) 
				WHEN found_problem_id IS NULL AND approved_order_id IS NULL AND quantity > 0 
					THEN CONCAT('unprocessed_quantities',@currOrdNr) 
				ELSE found_problem_id 
			END AS found_problem_id, 
			created 
		FROM `records` 
		WHERE 
			`created` >= '2017-05-17 10:00:00' AND 
			created < '2017-05-17 12:30'
		) AS tbl1 
	LEFT JOIN plans ON(plans.id = tbl1.plan_id) 
	GROUP BY DATE_FORMAT(tbl1.created, '%Y-%m-%d %H'),found_problem_id 
	ORDER BY tbl1.created
	
	
	
	//sql skirtingiem laiko momentam stebeti, pvz darbas, problema1, problema2, darbas ir t.t. (PAKESITI: sensor_id = 2 nurodytame intervale)
SET @currJobNr := 0, @prevProblem := -1, @prevCreated := NULL; 
SELECT 
    plans.mo_number, 
    SUM(time_diff) / 60 AS minutes, 
    tbl1.created, 
    found_problem_id,
    job_nr,
    approved_order_id,
    sum(tbl1.quantity) AS quantity,
    sum(tbl1.unit_quantity) AS unit_quantity
    FROM (
        SELECT 
                    plan_id,
                    sensor_id,
                    approved_order_id,
                    quantity,
                    unit_quantity,
                    IF(@prevCreated IS NULL, 20, TIMESTAMPDIFF(SECOND,@prevCreated,created)) as time_diff,
                    CASE WHEN found_problem_id IS NULL AND IFNULL(found_problem_id,0) != @prevProblem
                            THEN @currJobNr := @currJobNr + 1 
                    END as exp1,
                    CASE WHEN found_problem_id IS NULL THEN CONCAT('order_',@currJobNr) ELSE found_problem_id END AS job_nr,
                    @prevProblem := IFNULL(found_problem_id,0) AS exp2,
                    @prevCreated := created AS exp3,
                    found_problem_id,
                    created 
                FROM `records` 
                WHERE 
                    `created` >= '2017-07-17 12:00:00' AND 
                    created < '2017-07-17 15:30' AND sensor_id = 2
    ) AS tbl1 
    LEFT JOIN plans ON(plans.id = tbl1.plan_id) 
    GROUP BY job_nr, approved_order_id
    ORDER BY tbl1.created