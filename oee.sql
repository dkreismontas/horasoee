-- phpMyAdmin SQL Dump
-- version 4.4.15.10
-- https://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Aug 08, 2019 at 02:30 PM
-- Server version: 5.5.60-MariaDB
-- PHP Version: 7.0.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `axioma_oee`
--

-- --------------------------------------------------------

--
-- Table structure for table `after_production_problems`
--

DROP TABLE IF EXISTS `after_production_problems`;
CREATE TABLE IF NOT EXISTS `after_production_problems` (
  `id` int(11) NOT NULL,
  `problem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `approved_orders`
--

DROP TABLE IF EXISTS `approved_orders`;
CREATE TABLE IF NOT EXISTS `approved_orders` (
  `id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL COMMENT 'Sukūrimo data',
  `sensor_id` int(11) unsigned NOT NULL,
  `plan_id` int(11) unsigned NOT NULL COMMENT 'Plano id, nusakanatis kokią produkciją gaminame',
  `status_id` int(11) unsigned NOT NULL COMMENT 'Statuso id',
  `end` datetime DEFAULT NULL COMMENT 'Pabaiga',
  `quantity` decimal(12,4) unsigned NOT NULL DEFAULT '0' COMMENT 'Kiekis',
  `box_quantity` decimal(12,4) unsigned NOT NULL DEFAULT '0' COMMENT 'Dėžių kiekis',
  `confirmed_quantity` decimal(10,2) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Patvirtintas kiekis',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Ar patvirtintas',
  `swap_time` datetime DEFAULT NULL,
  `exported_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'Anks??iau eksportuotas kiekis',
  `additional_data` text COMMENT 'papildomi duomenys ivedami vartotojo pradedant uzsakyma',
  `quality_factor` decimal(8,6) NOT NULL COMMENT 'Uzsakymo kokybes koeficientas ne procentinis'
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Visi vykdyti užsakymai';

-- --------------------------------------------------------

--
-- Table structure for table `branches`
--

DROP TABLE IF EXISTS `branches`;
CREATE TABLE IF NOT EXISTS `branches` (
  `id` int(11) unsigned NOT NULL,
  `ps_id` int(11) unsigned NOT NULL COMMENT 'Numeris',
  `name` varchar(45) NOT NULL COMMENT 'Pavadinimas',
  `quality_user_id` varchar(255) DEFAULT NULL COMMENT 'Kokybės vadovu id sarasas',
  `production_user_id` int(11) unsigned NOT NULL COMMENT 'Gamybos vadovas',
  `shift_manager_email` varchar(128) DEFAULT NULL COMMENT 'Pamainos vadovo el. paštas',
  `factory_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='Padaliniai';

--
-- Dumping data for table `branches`
--

INSERT INTO `branches` (`id`, `ps_id`, `name`, `quality_user_id`, `production_user_id`, `shift_manager_email`, `factory_id`) VALUES(1, 1, 'Kaunas', '1', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `dashboards_calculations`
--

DROP TABLE IF EXISTS `dashboards_calculations`;
CREATE TABLE IF NOT EXISTS `dashboards_calculations` (
  `id` int(11) NOT NULL,
  `theory_prod_time` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Teorinis gamybos laikas (min.)',
  `fact_prod_time` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Faktinis gamybos laikas (min.)',
  `total_quantity` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Pagamintos produkcijos kiekis (vnt.)',
  `count_delay` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `shift_id` int(11) unsigned DEFAULT NULL COMMENT 'Pamaina',
  `shift_length` int(11) NOT NULL,
  `shift_length_with_exclusions` decimal(20,8) NOT NULL,
  `year` int(11) NOT NULL DEFAULT '0',
  `month` smallint(6) NOT NULL DEFAULT '0',
  `week` int(11) NOT NULL DEFAULT '0',
  `day` int(11) NOT NULL DEFAULT '0',
  `calc_for` date NOT NULL,
  `sensor_id` int(11) unsigned DEFAULT NULL COMMENT 'Jutiklis',
  `oee` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `exploitation_factor` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Išnaudojimo koefic',
  `operational_factor` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Veiklos efektyvumo koefic',
  `quality_factor` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Kokybės koefic',
  `oee_with_exclusions` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `exploitation_factor_with_exclusions` decimal(20,8) NOT NULL DEFAULT '0.00000000',
  `transition_time` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Perėjimo laikas',
  `transition_count` int(11) NOT NULL,
  `exceeded_transition_time` decimal(20,8) NOT NULL COMMENT 'viršyto derinimo probleminis laikas',
  `true_problem_time` decimal(20,8) DEFAULT '0.00000000' COMMENT 'Problemų laikas su isimtimis, kai problem_id >= 3',
  `true_problem_time_exclusions` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Problemų laikas atmetus isimtimis, kai problem_id >= 3',
  `mtt_problem_time` decimal(20,8) NOT NULL COMMENT 'Problemu trukme, kurioms pazymetas Problem.mean_time_calculations',
  `no_work_time` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Nedarbo laikas',
  `other_product_time` decimal(20,8) NOT NULL DEFAULT '0.00000000' COMMENT 'Kitas produktas laikas',
  `true_problem_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Problemų kiekis su isimtimis, kai problem_id >= 3',
  `true_problem_count_exclusions` int(11) NOT NULL COMMENT 'Problemų kiekis atmetus isimtimis, kai problem_id >= 3',
  `mtt_problem_count` int(11) NOT NULL COMMENT 'Problemu kiekis, kurioms pazymetas Problem.mean_time_calculations',
  `mttf` decimal(10,2) NOT NULL DEFAULT '0.00',
  `mttr` decimal(10,2) NOT NULL DEFAULT '0.00',
  `slow_speed_duration` decimal(10,4) unsigned NOT NULL DEFAULT '0.0000' COMMENT 'Minuciu kiekis, kuomet darbo centro greitis veike leciau uz plano step numatyta greiti'
) ENGINE=InnoDB AUTO_INCREMENT=65571 DEFAULT CHARSET=latin1 COMMENT='Pamainos lygio statistikos skai??iavimai';

-- --------------------------------------------------------

--
-- Table structure for table `dashboards_calculation_mos`
--

DROP TABLE IF EXISTS `dashboards_calculation_mos`;
CREATE TABLE IF NOT EXISTS `dashboards_calculation_mos` (
  `id` int(11) NOT NULL,
  `theory_prod_time` float NOT NULL DEFAULT '0' COMMENT 'Teorinis gamybos laikas (min.)',
  `fact_prod_time` int(11) NOT NULL DEFAULT '0' COMMENT 'Faktinis gamybos laikas (min.)',
  `total_quantity` int(11) NOT NULL DEFAULT '0' COMMENT 'Pagamintos produkcijos kiekis (vnt.)',
  `shift_id` int(11) DEFAULT NULL,
  `mo_number` int(11) DEFAULT NULL,
  `sensor_id` int(11) DEFAULT NULL,
  `oee` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `exploitation_factor` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'Išnaudojimo koefic',
  `operational_factor` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'Veiklos efektyvumo koefic',
  `quality_factor` decimal(10,4) NOT NULL DEFAULT '0.0000' COMMENT 'Kokybės koefic',
  `transition_time` int(11) NOT NULL DEFAULT '0',
  `true_problem_time` int(11) NOT NULL DEFAULT '0',
  `no_work_time` int(11) NOT NULL DEFAULT '0',
  `other_product_time` int(11) NOT NULL DEFAULT '0',
  `true_problem_count` int(11) NOT NULL DEFAULT '0',
  `mttf` decimal(10,2) NOT NULL DEFAULT '0.00',
  `mttr` decimal(10,2) NOT NULL DEFAULT '0.00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `dashboards_mailsends`
--

DROP TABLE IF EXISTS `dashboards_mailsends`;
CREATE TABLE IF NOT EXISTS `dashboards_mailsends` (
  `user_id` int(11) NOT NULL COMMENT 'Vartotojas',
  `graph_id` int(11) NOT NULL COMMENT 'Grafikas',
  `start` datetime NOT NULL COMMENT 'Siuntimo pradžia',
  `end` datetime NOT NULL COMMENT 'Siuntimo pabaiga',
  `time_groupper` text CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Informacija apie prenumeruotus skydelius';

-- --------------------------------------------------------

--
-- Table structure for table `dashboards_settings`
--

DROP TABLE IF EXISTS `dashboards_settings`;
CREATE TABLE IF NOT EXISTS `dashboards_settings` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT 'Vartotojas',
  `type` int(11) NOT NULL COMMENT 'Tipas',
  `position` int(11) DEFAULT NULL COMMENT 'Pozicija',
  `data` text CHARACTER SET utf8 NOT NULL COMMENT 'Turinys',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `subscribed_users` text CHARACTER SET utf8 COMMENT 'vartotoju id sarasas, kurie registruoja si grafika kaip prenumerata'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 COMMENT='Skydeliai';

-- --------------------------------------------------------

--
-- Table structure for table `diff_cards`
--

DROP TABLE IF EXISTS `diff_cards`;
CREATE TABLE IF NOT EXISTS `diff_cards` (
  `id` int(11) unsigned NOT NULL,
  `date` datetime NOT NULL COMMENT 'Sukūrimo data',
  `sensor_id` int(11) unsigned NOT NULL,
  `branch_id` int(11) unsigned NOT NULL,
  `line_id` int(11) unsigned NOT NULL,
  `start` datetime DEFAULT NULL COMMENT 'Pradžia',
  `end` datetime DEFAULT NULL COMMENT 'Pabaiga',
  `plan_id` int(11) unsigned DEFAULT NULL,
  `problem_id` int(11) unsigned DEFAULT NULL,
  `found_problem_id` int(11) unsigned DEFAULT NULL,
  `consequences` int(11) NOT NULL DEFAULT '0' COMMENT 'Nuostolių kiekis',
  `shift_manager_user_id` int(11) unsigned DEFAULT NULL,
  `description` text COMMENT 'Aprašymas',
  `diff_type_id` int(11) unsigned DEFAULT NULL COMMENT 'Tipas',
  `diff_sub_type_id` int(11) unsigned DEFAULT NULL,
  `responsive_section_user_id` int(11) unsigned DEFAULT NULL,
  `clarification` text COMMENT 'Patvirtinimas',
  `measures_taken` text COMMENT 'Priemonės',
  `measures_for_future` text COMMENT 'Kas bus patobulinta',
  `deadline` datetime DEFAULT NULL COMMENT 'Patobulinimo data',
  `quality_section_user_id` varchar(255) DEFAULT NULL,
  `measure_evaluation` text COMMENT 'Įvertinimas',
  `explanation_denied` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Ar nepriimtas',
  `production_manager_user_id` int(11) unsigned DEFAULT NULL,
  `production_manager_comment` text,
  `state` int(2) unsigned NOT NULL DEFAULT '0' COMMENT 'Būsena',
  `shift_id` int(11) unsigned DEFAULT NULL,
  `attachment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Neatitikties kortelės';

-- --------------------------------------------------------

--
-- Table structure for table `diff_types`
--

DROP TABLE IF EXISTS `diff_types`;
CREATE TABLE IF NOT EXISTS `diff_types` (
  `id` int(11) unsigned NOT NULL,
  `parent_id` int(11) unsigned DEFAULT NULL COMMENT 'Tėvinio neatitikties tipo id',
  `code` varchar(32) NOT NULL COMMENT 'Kodas',
  `name` varchar(127) NOT NULL COMMENT 'Pavadinimas'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Neatitikties kortelių tipai';

-- --------------------------------------------------------

--
-- Table structure for table `factories`
--

DROP TABLE IF EXISTS `factories`;
CREATE TABLE IF NOT EXISTS `factories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `found_problems`
--

DROP TABLE IF EXISTS `found_problems`;
CREATE TABLE IF NOT EXISTS `found_problems` (
  `id` int(11) unsigned NOT NULL,
  `start` datetime DEFAULT NULL COMMENT 'Problemos pradžios laikas',
  `end` datetime DEFAULT NULL COMMENT 'Problemos pabaigos laikas',
  `problem_id` int(11) NOT NULL,
  `super_parent_problem_id` int(11) NOT NULL COMMENT 'pagrindines tevines problemos id',
  `transition_problem_id` int(11) NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL,
  `shift_id` int(11) unsigned NOT NULL COMMENT 'pamainos id, pririšama išskai??iuojant įrašo atėjimo laiką ir kokia pamaina tuo metu dirba',
  `plan_id` int(11) unsigned DEFAULT NULL,
  `state` int(2) unsigned DEFAULT '0' COMMENT 'Būsena',
  `quantity` decimal(12,4) NOT NULL DEFAULT '0.0000' COMMENT 'Intervale atsiustu kiekiu suma',
  `diff_card_id` int(11) unsigned DEFAULT NULL COMMENT 'Neatitikties kortelė (jei yra)',
  `while_working` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Ar vyko dirbant',
  `mail_inform_sended` varchar(255) NOT NULL COMMENT 'mailu sarasas, kuriems buvo isiustas el. pastas apie problemos ilga testinuma. Veikia pagal sensoriaus nustatyma inform_about_problems',
  `json` text NOT NULL,
  `prev_shift_problem_id` int(11) DEFAULT NULL COMMENT 'Jei problema persikelia i nauja pamaina, sis id reprezentuoja koks buvo praejusios pamainos to pacios problemos id',
  `comments` text NOT NULL,
  `exported_to_mpm` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jei lygus 1, vadinasi jau perduotas i MPM sistema',
  `user_id` smallint(5) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2792 DEFAULT CHARSET=utf8 COMMENT='Užfiksuotos problemos';

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(0, 'Technikai', '2018-07-23 10:42:04', '2018-07-23 10:42:04');
INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(1, 'Admin', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(2, 'Moderator', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(3, 'Guest', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(4, 'Operator', '0000-00-00 00:00:00', '0000-00-00 00:00:00');
INSERT INTO `groups` (`id`, `name`, `created`, `modified`) VALUES(5, 'Darbo centrai', '2018-07-23 10:42:04', '2018-07-23 10:42:04');

-- --------------------------------------------------------

--
-- Table structure for table `lines`
--

DROP TABLE IF EXISTS `lines`;
CREATE TABLE IF NOT EXISTS `lines` (
  `id` int(11) unsigned NOT NULL,
  `ps_id` varchar(45) NOT NULL COMMENT 'Linijos identifikatorius planavimo sistemoje',
  `name` varchar(45) DEFAULT NULL COMMENT 'Linijos pavadinimas',
  `branch_id` int(11) unsigned NOT NULL,
  `industrial` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Gaminti industrines linijas pagal eiliškumą, kitur ne',
  `diff_card_auto_time` int(6) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='Linijos';

--
-- Dumping data for table `lines`
--

INSERT INTO `lines` (`id`, `ps_id`, `name`, `branch_id`, `industrial`, `diff_card_auto_time`) VALUES(1, '', 'Testinė', 1, 0, 0);
INSERT INTO `lines` (`id`, `ps_id`, `name`, `branch_id`, `industrial`, `diff_card_auto_time`) VALUES(2, '', 'Gamyba', 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `line_sensors`
--

DROP TABLE IF EXISTS `line_sensors`;
CREATE TABLE IF NOT EXISTS `line_sensors` (
  `id` int(11) unsigned NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL,
  `line_id` int(11) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COMMENT='Linijų jutikliai';

--
-- Dumping data for table `line_sensors`
--

INSERT INTO `line_sensors` (`id`, `sensor_id`, `line_id`) VALUES(1, 1, 1);
INSERT INTO `line_sensors` (`id`, `sensor_id`, `line_id`) VALUES(2, 2, 2);
INSERT INTO `line_sensors` (`id`, `sensor_id`, `line_id`) VALUES(3, 3, 2);
INSERT INTO `line_sensors` (`id`, `sensor_id`, `line_id`) VALUES(4, 4, 2);

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(11) NOT NULL,
  `source` text NOT NULL,
  `user_id` text NOT NULL,
  `sensor` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `group_stamp` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `losses`
--

DROP TABLE IF EXISTS `losses`;
CREATE TABLE IF NOT EXISTS `losses` (
  `id` int(11) unsigned NOT NULL,
  `approved_order_id` int(11) unsigned NOT NULL,
  `loss_type_id` int(11) unsigned NOT NULL,
  `value` decimal(14,3) NOT NULL DEFAULT '0.000' COMMENT 'Kiekis',
  `while_confirming` tinyint(1) NOT NULL DEFAULT '0',
  `shift_id` int(11) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT 'Sukūrimo data (auto)',
  `user_id` int(11) NOT NULL COMMENT 'Deklaracija pildancio vartotojo id'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuostoliai';

-- --------------------------------------------------------

--
-- Table structure for table `loss_types`
--

DROP TABLE IF EXISTS `loss_types`;
CREATE TABLE IF NOT EXISTS `loss_types` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(127) NOT NULL,
  `code` int(11) unsigned NOT NULL,
  `unit` varchar(12) NOT NULL,
  `line_ids` varchar(255) NOT NULL COMMENT 'liniju id sarasas',
  `sensor_types` varchar(255) NOT NULL COMMENT 'Linojos pradzia 1, vidurys 2, pabaiga 3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Nuostolių tipai';

-- --------------------------------------------------------

--
-- Table structure for table `order_calculations`
--

DROP TABLE IF EXISTS `order_calculations`;
CREATE TABLE IF NOT EXISTS `order_calculations` (
  `id` int(11) NOT NULL,
  `shift_id` int(4) unsigned DEFAULT NULL,
  `sensor_id` int(2) unsigned DEFAULT NULL,
  `mo_number` varchar(20) DEFAULT NULL COMMENT 'Uzsakymo nr',
  `step` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Gamybos greitis',
  `approved_order_id` int(12) unsigned DEFAULT NULL,
  `order_start` datetime DEFAULT NULL COMMENT 'Užsakymo pradžia',
  `order_end` datetime DEFAULT NULL COMMENT 'Užsakymo pabaiga',
  `quantity` decimal(12,4) NOT NULL DEFAULT '0' COMMENT 'Kiekis',
  `confirmed_quantity` decimal(10,2) NOT NULL COMMENT 'Patvirtintas kiekis',
  `time_full` int(10) NOT NULL DEFAULT '0' COMMENT 'Pilnas laikas (su sustojimais)',
  `time_full_with_exclusions` int(11) NOT NULL DEFAULT '0',
  `time_green` int(10) NOT NULL DEFAULT '0' COMMENT 'Gamybos laikas',
  `time_yellow` int(10) NOT NULL DEFAULT '0' COMMENT 'Perėjimo laikas',
  `exceeded_time_yellow` int(11) NOT NULL COMMENT 'virsytas derinimas',
  `time_red` int(10) NOT NULL DEFAULT '0' COMMENT 'Problemų laikas',
  `problems` text COMMENT 'Problemų sąrašas JSON',
  `problem_count` int(11) NOT NULL DEFAULT '0' COMMENT 'Problemų kiekis',
  `oee_oee` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `oee_avail` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `oee_perf` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `oee_quality` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `oee_oee_with_exclusions` double(10,4) NOT NULL,
  `oee_avail_with_exclusions` double(10,4) NOT NULL,
  `mttf` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `mttr` decimal(10,4) NOT NULL DEFAULT '0.0000',
  `user_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Prie uzsakymo daugiausiai laiko pradirbusio darbuotojo id'
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COMMENT='Užsakymo lygio statistikos skai??iavimai';

-- --------------------------------------------------------

--
-- Table structure for table `partial_quantities`
--

DROP TABLE IF EXISTS `partial_quantities`;
CREATE TABLE IF NOT EXISTS `partial_quantities` (
  `id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL,
  `shift_id` int(11) unsigned NOT NULL,
  `approved_order_id` int(11) unsigned NOT NULL,
  `quantity` double NOT NULL COMMENT 'Kiekis',
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Tarpiniai užsakymų kiekiai';

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(11) NOT NULL,
  `plugin` varchar(100) CHARACTER SET utf8 NOT NULL,
  `controllers` varchar(255) NOT NULL,
  `actions` varchar(255) NOT NULL,
  `users` varchar(255) NOT NULL,
  `description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=130 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(65, '', 'ApprovedOrders', 'index', 'a:5:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"3";i:4;s:1:"2";}', 'Peržiūrėti patvirtintus užsakymus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(66, '', 'ApprovedOrders', 'edit', 'a:5:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"3";i:4;s:1:"2";}', 'Redaguoti patvirtintus užsakymus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(67, '', 'ApprovedOrders', 'remove', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Pašalinti patvirtintus užsakymus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(68, '', 'Branches', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti padalinius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(69, '', 'Branches', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti padalinius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(70, '', 'Branches', 'remove', 'a:1:{i:0;s:1:"1";}', 'Pašalinti padalinius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(71, '', 'Dashboard', 'index', 'a:2:{i:0;s:1:"1";i:1;s:1:"3";}', 'Matyti info skydą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(72, '', 'Dashboards', 'inner', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Atverti statistikos skydelį', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(73, '', 'DiffCards', 'index', 'a:5:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"3";i:4;s:1:"2";}', 'Peržiūrėti neatitikties korteles', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(74, '', 'DiffCards', 'edit', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:4;s:1:"2";}', 'Redaguoti neatitikties korteles', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(75, '', 'DiffCards', 'genxls', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Atsisiųsti neatitikties kortelių xls ataskaitą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(76, '', 'DiffTypes', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti neatitikties tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(77, '', 'DiffTypes', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti neatitikties tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(78, '', 'DiffTypes', 'remove', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pašalinti neatitikties tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(79, '', 'Lines', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti linijas', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(80, '', 'Lines', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti linijas', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(81, '', 'Lines', 'remove', 'a:1:{i:0;s:1:"1";}', 'Pašalinti linijas', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(82, '', 'LossTypes', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti nuostolių tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(83, '', 'LossTypes', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti nuostolių tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(84, '', 'LossTypes', 'remove', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pašalinti nuostolių tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(85, '', 'Plans', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti planus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(86, '', 'Plans', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti planus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(87, '', 'Plans', 'remove', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pašalinti planus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(88, '', 'Plans', 'change_data', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pakeisti plano duomenis į alternatyvius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(89, '', 'Problems', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti problemų tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(90, '', 'Problems', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti problemų tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(91, '', 'Problems', 'remove', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pašalinti problemų tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(92, '', 'Problems', 'disable', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Įjungti/išjungti problemos tipą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(93, '', 'Products', 'import', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Importuoti produktus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(94, '', 'Products', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti produktus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(95, '', 'Products', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti produktus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(96, '', 'Products', 'remove', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pašalinti produktus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(97, '', 'Records', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti jutiklių įrašus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(98, '', 'Sensors', 'index', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Peržiūrėti jutiklius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(99, '', 'Sensors', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti jutiklius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(100, '', 'Sensors', 'remove', 'a:1:{i:0;s:1:"1";}', 'Pašalinti jutiklius', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(101, '', 'Settings', 'index', 'a:1:{i:0;s:1:"1";}', 'Peržiūrėti/redaguoti nustatymus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(102, '', 'Shifts', 'index', 'a:3:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";}', 'Peržiūrėti pamainas', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(103, '', 'Shifts', 'disableEnableShift', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Įjungti / išjungti pamainą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(104, '', 'Test', 'index', 'a:2:{i:0;s:1:"1";i:1;s:1:"5";}', 'Atidaryti testinių įrašų generatorių', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(105, '', 'Test', 'tick', 'a:2:{i:0;s:1:"1";i:1;s:1:"5";}', 'Generuoti testinius įrašus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(106, '', 'Users', 'delete', 'a:1:{i:0;s:1:"1";}', 'Pašalinti vartotoją', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(107, '', 'Users', 'change_user_group', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Pakeisti vartotojo grupę', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(108, '', 'Users', 'index', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Atverti vartotojų sąrašo puslapį', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(109, '', 'Users', 'activate_user', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Aktyvuoti/deaktyvuoti vartotoją', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(110, '', 'Users', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Redaguoti vartotoją', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(111, '', 'Users', 'give_permission', 'a:1:{i:0;s:1:"1";}', 'Priskirti vartotojui prieeigos teisę', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(112, '', 'Users', 'check_permissions', 'a:1:{i:0;s:1:"1";}', 'Atverti teisių skirstymo puslapį', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(113, '', 'Users', 'add_change_group', 'a:1:{i:0;s:1:"1";}', 'Sukurti/redaguoti vartotojų grupę', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(114, '', 'Users', 'remove_group', 'a:1:{i:0;s:1:"1";}', 'Pašalinti vartotojų grupę', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(115, '', 'WorkCenter', 'index', 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}', 'Darbo centras: atidaryti', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(116, '', 'WorkCenter', 'allowChangeProcess', 'a:4:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"4";i:3;s:1:"5";}', 'Darbo centras: Redaguoti darbo procesų tipus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(117, '', 'WorkCenter', 'changeProblemToTransition', 'a:4:{i:0;s:1:"1";i:1;s:1:"4";i:2;s:1:"2";i:3;s:1:"5";}', 'Darbo centras: Pakeisti problemas į perėjimo problemą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(118, '', 'WorkCenter', 'startProd', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"2";}', 'Darbo centras: registruoti gamybos pradžią', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(119, '', 'WorkCenter', 'postphoneProd', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"2";}', 'Darbo centras: registruoti produkcijos gamybos atidėjimą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(120, '', 'WorkCenter', 'completeProd', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"2";}', 'Darbo centras: registruoti gamybos pabaigą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(121, '', 'WorkCenter', 'registerIssue', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"2";}', 'Darbo centras: registruoti gamybos problemą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(122, '', 'WorkCenter', 'registerSpeedIssue', 'a:4:{i:0;s:1:"1";i:1;s:1:"5";i:2;s:1:"4";i:3;s:1:"2";}', 'Darbo centras: registruoti gamybos greičio problemą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(123, 'WorksList', 'WorksList', 'entries', 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}', 'Technikų registras: peržiūrėti sukurtus darbus', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(124, 'WorksList', 'WorksList', 'types', 'a:5:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";i:3;s:1:"4";i:4;s:1:"5";}', 'Technikų registras: sąrašas', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(125, 'WorksList', 'WorksList', 'create', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Technikų registras: sukurti', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(126, 'WorksList', 'WorksList', 'edit', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Technikų registras: redaguoti', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(127, 'WorksList', 'WorksList', 'delete', 'a:2:{i:0;s:1:"1";i:1;s:1:"2";}', 'Technikų registras: ištrinti', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(128, 'WorksList', 'WorksList', 'generateXLSreport', 'a:3:{i:0;s:1:"1";i:1;s:1:"2";i:2;s:1:"3";}', 'Technikų registras: generuoti xls ataskaitą', 1538397277);
INSERT INTO `permissions` (`id`, `plugin`, `controllers`, `actions`, `users`, `description`, `created`) VALUES(129, 'WorksList', 'WorksList', 'works', 'a:5:{i:0;s:1:"1";i:1;s:1:"3";i:2;s:1:"2";i:3;s:1:"4";i:4;s:1:"5";}', 'Technikų registras: atverti darbo puslapį', 1538397277);

-- --------------------------------------------------------

--
-- Table structure for table `plans`
--

DROP TABLE IF EXISTS `plans`;
CREATE TABLE IF NOT EXISTS `plans` (
  `id` int(11) unsigned NOT NULL,
  `sensor_id` int(11) unsigned NOT NULL COMMENT 'Darbo centro (jutiklio) identifikacinis numeris iš planavimo sistemos',
  `paste_code` varchar(45) DEFAULT NULL COMMENT 'Operacijos kodas',
  `production_name` varchar(45) DEFAULT NULL COMMENT 'Produkcijos pavadinimas',
  `production_code` varchar(45) NOT NULL COMMENT 'Produkcijos kodas. Kodą turi tik galutinės produkcijos jutikliai. Jei nėra produkcijos kodo, laukelis tuš??ias. ',
  `mo_number` varchar(100) NOT NULL COMMENT 'Užsakymo numeris',
  `start` datetime DEFAULT NULL COMMENT 'Numatyta pagal planą gamybos pradžia. ',
  `end` datetime DEFAULT NULL COMMENT 'Numatyta pagal planą gamybos pabaiga.',
  `position` int(11) NOT NULL,
  `position_frozen` tinyint(1) NOT NULL,
  `step` decimal(12,4) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Vienetai per valandą',
  `box_quantity` decimal(12,4) unsigned NOT NULL DEFAULT '0' COMMENT 'Gaminių kiekis dėžėje (vnt)',
  `quantity` decimal(12,4) unsigned NOT NULL DEFAULT '0.00' COMMENT 'Gaminių kiekis (vnt)',
  `preparation_time` float NOT NULL COMMENT 'Pasiruosimo laikas (min)',
  `production_time` float NOT NULL COMMENT 'Gamybos laikas (min)',
  `client_speed` float NOT NULL COMMENT 'Numatytas greitis is kliento (vnt/h)',
  `control` text,
  `line_id` int(11) NOT NULL,
  `has_problem` tinyint(1) NOT NULL,
  `product_id` int(11) NOT NULL,
  `serialized_data` text NOT NULL,
  `exported_to_mpm` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Jei 1, vadinasi planas uzbaigtas ir iseksportuotas i mpm',
  `disabled` tinyint(1) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COMMENT='Suplanuoti užsakymai vykdymui';

-- --------------------------------------------------------

--
-- Table structure for table `possibilities`
--

DROP TABLE IF EXISTS `possibilities`;
CREATE TABLE IF NOT EXISTS `possibilities` (
  `id` int(11) NOT NULL,
  `sensor_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `speed` float NOT NULL,
  `client_speed` float NOT NULL,
  `preparation_time` float NOT NULL,
  `production_time` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
CREATE TABLE IF NOT EXISTS `problems` (
  `id` int(11) NOT NULL,
  `parent_id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'Problemos pavadinimas',
  `sensor_type` int(2) unsigned NOT NULL,
  `for_speed` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `transition_problem` tinyint(1) NOT NULL COMMENT 'Jei = 1 vadinasi perejimo problema',
  `line_id` int(10) NOT NULL,
  `disabled` tinyint(1) NOT NULL,
  `transition_time` int(11) NOT NULL COMMENT 'Numatyta perėjimo trukmė minutėmis, kai problema yra perejimo tipo',
  `mean_time_calculations` tinyint(1) NOT NULL COMMENT 'Ar Įtraukti į MTTR ir MTBF skaičiavimus?',
  `level` smallint(5) unsigned DEFAULT '0' COMMENT 'Prastovos lygis prastovu medyje'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='Problemų tipai';

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`id`, `parent_id`, `name`, `sensor_type`, `for_speed`, `transition_problem`, `line_id`, `disabled`, `transition_time`, `mean_time_calculations`, `level`) VALUES(-1, 0, 'Darbas', 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `problems` (`id`, `parent_id`, `name`, `sensor_type`, `for_speed`, `transition_problem`, `line_id`, `disabled`, `transition_time`, `mean_time_calculations`, `level`) VALUES(0, 0, 'Nepažymėta prastova', 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `problems` (`id`, `parent_id`, `name`, `sensor_type`, `for_speed`, `transition_problem`, `line_id`, `disabled`, `transition_time`, `mean_time_calculations`, `level`) VALUES(1, 0, 'Perėjimas', 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `problems` (`id`, `parent_id`, `name`, `sensor_type`, `for_speed`, `transition_problem`, `line_id`, `disabled`, `transition_time`, `mean_time_calculations`, `level`) VALUES(2, 0, 'Nėra darbo', 0, 0, 0, 0, 0, 0, 0, 0);
INSERT INTO `problems` (`id`, `parent_id`, `name`, `sensor_type`, `for_speed`, `transition_problem`, `line_id`, `disabled`, `transition_time`, `mean_time_calculations`, `level`) VALUES(3, 0, 'Nepažymėta prastova', 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(11) NOT NULL,
  `production_time` int(11) NOT NULL COMMENT 'produkto gamybos laikas sekundemis',
  `name` text NOT NULL,
  `production_code` varchar(50) NOT NULL,
  `work_center` varchar(255) NOT NULL,
  `kiekis_formoje` double NOT NULL COMMENT 'vnt',
  `formu_greitis` double NOT NULL COMMENT 'formos/h',
  `preparation_time` decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'Pasiruosimo laikas (min)'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='Galimi gaminiai';

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `production_time`, `name`, `production_code`, `work_center`, `kiekis_formoje`, `formu_greitis`, `preparation_time`) VALUES(1, 0, 'Gamyba', 'GMB', '', 1, 3600, '0.00');
INSERT INTO `products` (`id`, `production_time`, `name`, `production_code`, `work_center`, `kiekis_formoje`, `formu_greitis`, `preparation_time`) VALUES(2, 0, 'Test gamyba', 'GMB TEST', '', 2, 3600, '0.00');

-- --------------------------------------------------------

--
-- Table structure for table `records`
--

DROP TABLE IF EXISTS `records`;
CREATE TABLE IF NOT EXISTS `records` (
  `id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL COMMENT 'Įrašo atėjimo laikas',
  `quantity` decimal(12,4) NOT NULL DEFAULT '0' COMMENT 'Pagamintos per 1min produkcijos kiekis',
  `unit_quantity` decimal(12,4) NOT NULL DEFAULT '0',
  `sensor_id` int(11) unsigned NOT NULL COMMENT 'Jutiklio id',
  `plan_id` int(11) unsigned DEFAULT NULL COMMENT 'Plano id, randamas iš approved_orders lentelės, kaip paskutinis ant to valdiklio gamintos produkcijos id',
  `shift_id` int(11) unsigned DEFAULT NULL COMMENT 'pamainos id, pririšama išskai??iuojant įrašo atėjimo laiką ir kokia pamaina tuo metu dirba',
  `approved_order_id` int(11) unsigned DEFAULT NULL,
  `found_problem_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=146970 DEFAULT CHARSET=utf8 COMMENT='Jutiklių įrašai';

-- --------------------------------------------------------

--
-- Table structure for table `records_archive`
--

DROP TABLE IF EXISTS `records_archive`;
CREATE TABLE IF NOT EXISTS `records_archive` (
  `id` int(11) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `quantity` int(11) NOT NULL DEFAULT '0',
  `unit_quantity` int(11) NOT NULL DEFAULT '0',
  `sensor_id` int(11) unsigned NOT NULL,
  `plan_id` int(11) unsigned DEFAULT NULL,
  `shift_id` int(11) unsigned DEFAULT NULL,
  `approved_order_id` int(11) unsigned DEFAULT NULL,
  `found_problem_id` int(11) unsigned DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Jutiklių įrašų archyvas';

-- --------------------------------------------------------

--
-- Table structure for table `sensordataimportcheckpoint`
--

DROP TABLE IF EXISTS `sensordataimportcheckpoint`;
CREATE TABLE IF NOT EXISTS `sensordataimportcheckpoint` (
  `SourceId` int(11) NOT NULL,
  `Time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sensorprofilerecords`
--

DROP TABLE IF EXISTS `sensorprofilerecords`;
CREATE TABLE IF NOT EXISTS `sensorprofilerecords` (
  `Source` int(11) NOT NULL,
  `Date` datetime NOT NULL,
  `Value` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sensors`
--

DROP TABLE IF EXISTS `sensors`;
CREATE TABLE IF NOT EXISTS `sensors` (
  `id` int(11) unsigned NOT NULL,
  `ps_id` varchar(45) NOT NULL COMMENT 'Identifikatorius pagal planavimo sistemą',
  `pin_name` varchar(255) NOT NULL COMMENT 'Valdiklio pino pavadinimas, pagal atpazistamas valdiklio kontaktas. kontakto nr naudojamas duomenims atsiusti i DB',
  `port` varchar(10) NOT NULL COMMENT 'Portas, kurima priklauso jutiklio dezute',
  `name` varchar(45) DEFAULT NULL,
  `branch_id` int(11) unsigned NOT NULL,
  `type` int(2) unsigned NOT NULL DEFAULT '0' COMMENT 'Darbo centro tipas (maišymas, pjaustymas, pakavimas)',
  `image` varchar(100) NOT NULL COMMENT 'Įrengimo SPRITE paveiksliukas 1099x161 (jpeg,jpg,png)',
  `quantity_records_count` smallint(5) unsigned NOT NULL COMMENT 'Is eiles einanciu irasiu su kiekiais skaicius. Atejes probleminis irasas nuresetina vel iki 0. Naudojamas gamybai pradziai nusakyti.',
  `new_production_start` datetime NOT NULL COMMENT 'Data, kai turi prasideti gamyba. Irasoma kai nevyksta gamyba ir pareina reikiamas geru irasu kiekis, Nuresetinama sukuris approved_orders irasa.',
  `is_partial` tinyint(1) NOT NULL DEFAULT '0',
  `is_virtual` tinyint(1) NOT NULL DEFAULT '0',
  `full_sensor_id` int(4) NOT NULL,
  `marked` tinyint(1) NOT NULL COMMENT 'jei lygus 1 reiskia gali buti naudojamas statistikos isvedimuose kaip mashines_status',
  `user_update_time` datetime NOT NULL COMMENT 'prisijungusio per narsykle darbo centro paskutinio atnaujinimo laikas',
  `last_norecordsmailsend_time` datetime NOT NULL,
  `tv_speed_deviation` varchar(100) NOT NULL DEFAULT '[30-30],[0-5]' COMMENT 'tv ekranu greicio matuoklio ribos. Pirmi skliaustai reiskia bendra nuokrypi nuo normos i kaire ir desine puses, antri - geltonas ribas',
  `show_count_marker` tinyint(1) NOT NULL COMMENT 'Ar atvaizduoti kieki jutiklio valandiniame grafike?',
  `speed_detect_interval` mediumint(255) NOT NULL DEFAULT '0' COMMENT 'Greičio tikrinimo intervalas minutėmis',
  `ok_speed_threshold` mediumint(9) NOT NULL DEFAULT '0' COMMENT 'Gero greičio slenkstis procentais',
  `quantity_records_to_start_order` smallint(5) unsigned NOT NULL DEFAULT '1' COMMENT 'Įrašų kiekis užsakymui pradėti',
  `working_on_plans_or_products` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Sistemoje vykdomi užsakymai ar gaminiai? 0 - užsakymai | 1 - gaminiai',
  `show_plans_count_on_new_production` smallint(6) NOT NULL DEFAULT '4' COMMENT 'Kai pradedama gamyba ir vartotojui išmetamas užklausimas "kas dabar gaminama?" atvaizduojamų užsakymų kiekis',
  `time_interval_diff_card_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Nauja neatitikties kortelė" mygtuką? 1 - Taip | 0 - Ne',
  `time_interval_fill_nk_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Pildyti NK" mygtuką? 1 - Taip | 0 - Ne',
  `time_interval_technicians_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Technikai" mygtuką? 1 - Taip | 0 - Ne',
  `time_interval_register_event_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Registruoti įvykį" mygtuką? 1 - Taip | 0 - Ne',
  `time_interval_declare_count_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Deklaruoti kiekius" mygtuką? 1 - Taip | 0 - Ne',
  `time_interval_change_order_button` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar laiko intervalų grafike atvaizduoti "Deklaruoti kiekius" mygtuką? 1 - Taip | 0 - Ne',
  `auto_generate_diff_cards` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar neatitikties kortelės turi būti kuriamos automatiškai? 1 - Taip | 0 - Ne',
  `show_oee_or_other_in_wc` smallint(6) NOT NULL DEFAULT '0' COMMENT 'Darbo centre atvaizduojamas OEE ar prieinamumas? 0 - OEE | 1 - Prieinamumas',
  `inform_about_problems` text NOT NULL COMMENT 'siunciami problemu virsijimai i el. pastus.',
  `units` varchar(100) NOT NULL COMMENT 'matavimo vieneto pavadinimas',
  `time_interval_declare_count_on_finish` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar duoti operatoriui gamybos pabaigoje deklaruoti pagamintus kiekius. ',
  `prod_starts_on` int(11) NOT NULL COMMENT 'Mažiausia įrašo vertė, su kuria būtų skaičiuojamas iš eilės einančių įrašų kiekis užsakymui pradėti',
  `short_problem` varchar(50) NOT NULL COMMENT 'Problemos id, į kurią virsta nepažymėta prastova, trukusi trumpiau nei nurodytas laikas minutėmis. Pvz.: 343_2',
  `long_problem` varchar(50) NOT NULL COMMENT 'Problemos id, į kurią virsta nepažymėta prastova keiciantis pamainai, trukusi ilgiau nei nurodytas laikas minutėmis. Pvz.: 344_60',
  `choose_plan_after_transition` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ar planas perenkamas iskart po derinimo pasirinkimo',
  `plan_relate_quantity` varchar(30) NOT NULL DEFAULT 'quantity' COMMENT 'Planas susietas su jutiklio užfiksuotais kiekiais (quantity) ar su į vieną fiksaciją patenkančiais vienetais (unit_quantity)?',
  `allow_transition_during_work` tinyint(1) NOT NULL DEFAULT '1' COMMENT 'Ar gamybos metu leisti operatoriui fiksuoti perėjimo problemą?',
  `allow_nowork_during_work` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar gamybos metu leisti operatoriui fiksuoti "nėra darbo" prastovą per "Registruoti įvykį" mygtuką?',
  `show_plan_workcenter_intervals` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar darbo centro intervalų grafike atvaizduoti plano intervalus šalia faktinių laikų?',
  `show_plans_count_on_table` int(11) NOT NULL DEFAULT '4' COMMENT 'Operatoriaus darbo centro lange esančioje informacinėje lentelėje atvaizduojamų užsakymų skaičius',
  `new_fproblem_when_current_exceeds` varchar(255) NOT NULL COMMENT 'Pamainos mastu ilgiau nei nurodytas laiko intervalas trunkanti prastova virsta į kitą, nurodytą prastovą',
  `new_fproblem_when_current_exceeds2` varchar(255) NOT NULL COMMENT 'Ilgiau nei nurodytas laiko intervalas trunkanti prastova virsta į kitą, nurodytą prastovą',
  `records_count_to_start_problem` smallint(5) unsigned NOT NULL DEFAULT '3' COMMENT 'Kai vykdomi planai irasu kiekis prastovai pradeti',
  `half_donut_colors` varchar(255) NOT NULL COMMENT 'Half donut rodiklio intervalų spalvos, pvz.: 75:#D64D49, 80:#FDEE00, 100:#5AB65A',
  `calculate_quality` smallint(2) NOT NULL COMMENT 'Ar darbo centrui skaiciuoti kokybes koeficienta',
  `confirm_orders` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Tvirtinti uzsakymus mygtuko isvedimas darbo centro lange kai verte lygi 1',
  `has_mpm_attribute` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Ar turi atitikmeni mpm sistemoje?'
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COMMENT='Jutikliai';

--
-- Dumping data for table `sensors`
--

INSERT INTO `sensors` (`id`, `ps_id`, `pin_name`, `port`, `name`, `branch_id`, `type`, `image`, `quantity_records_count`, `new_production_start`, `is_partial`, `is_virtual`, `full_sensor_id`, `marked`, `user_update_time`, `last_norecordsmailsend_time`, `tv_speed_deviation`, `show_count_marker`, `speed_detect_interval`, `ok_speed_threshold`, `quantity_records_to_start_order`, `working_on_plans_or_products`, `show_plans_count_on_new_production`, `time_interval_diff_card_button`, `time_interval_fill_nk_button`, `time_interval_technicians_button`, `time_interval_register_event_button`, `time_interval_declare_count_button`, `time_interval_change_order_button`, `auto_generate_diff_cards`, `show_oee_or_other_in_wc`, `inform_about_problems`, `units`, `time_interval_declare_count_on_finish`, `prod_starts_on`, `short_problem`, `long_problem`, `choose_plan_after_transition`, `plan_relate_quantity`, `allow_transition_during_work`, `allow_nowork_during_work`, `show_plan_workcenter_intervals`, `show_plans_count_on_table`, `new_fproblem_when_current_exceeds`, `new_fproblem_when_current_exceeds2`, `records_count_to_start_problem`, `half_donut_colors`, `calculate_quality`, `confirm_orders`, `has_mpm_attribute`) VALUES(1, '', '', '', 'Testinis', 1, 3, '', 0, '0000-00-00 00:00:00', 0, 0, 1, 0, '2018-08-06 08:43:05', '0000-00-00 00:00:00', '[30-30],[0-5]', 0, 0, 0, 1, 1, 20, 1, 1, 1, 1, 1, 1, 0, 1, '', '', 1, 1, '', '', 1, 'quantity', 1, 0, 0, 20, '', '', 3, '', 0, 0, 0);
INSERT INTO `sensors` (`id`, `ps_id`, `pin_name`, `port`, `name`, `branch_id`, `type`, `image`, `quantity_records_count`, `new_production_start`, `is_partial`, `is_virtual`, `full_sensor_id`, `marked`, `user_update_time`, `last_norecordsmailsend_time`, `tv_speed_deviation`, `show_count_marker`, `speed_detect_interval`, `ok_speed_threshold`, `quantity_records_to_start_order`, `working_on_plans_or_products`, `show_plans_count_on_new_production`, `time_interval_diff_card_button`, `time_interval_fill_nk_button`, `time_interval_technicians_button`, `time_interval_register_event_button`, `time_interval_declare_count_button`, `time_interval_change_order_button`, `auto_generate_diff_cards`, `show_oee_or_other_in_wc`, `inform_about_problems`, `units`, `time_interval_declare_count_on_finish`, `prod_starts_on`, `short_problem`, `long_problem`, `choose_plan_after_transition`, `plan_relate_quantity`, `allow_transition_during_work`, `allow_nowork_during_work`, `show_plan_workcenter_intervals`, `show_plans_count_on_table`, `new_fproblem_when_current_exceeds`, `new_fproblem_when_current_exceeds2`, `records_count_to_start_problem`, `half_donut_colors`, `calculate_quality`, `confirm_orders`, `has_mpm_attribute`) VALUES(2, '', '', '', 'Darbo centras 1', 1, 3, '', 0, '0000-00-00 00:00:00', 0, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[30 - 30], [0 - 5]', 0, 0, 0, 1, 1, 4, 0, 0, 0, 1, 0, 0, 0, 1, '', '', 0, 0, '', '', 1, 'quantity', 0, 0, 0, 4, '', '', 3, '', 0, 0, 0);
INSERT INTO `sensors` (`id`, `ps_id`, `pin_name`, `port`, `name`, `branch_id`, `type`, `image`, `quantity_records_count`, `new_production_start`, `is_partial`, `is_virtual`, `full_sensor_id`, `marked`, `user_update_time`, `last_norecordsmailsend_time`, `tv_speed_deviation`, `show_count_marker`, `speed_detect_interval`, `ok_speed_threshold`, `quantity_records_to_start_order`, `working_on_plans_or_products`, `show_plans_count_on_new_production`, `time_interval_diff_card_button`, `time_interval_fill_nk_button`, `time_interval_technicians_button`, `time_interval_register_event_button`, `time_interval_declare_count_button`, `time_interval_change_order_button`, `auto_generate_diff_cards`, `show_oee_or_other_in_wc`, `inform_about_problems`, `units`, `time_interval_declare_count_on_finish`, `prod_starts_on`, `short_problem`, `long_problem`, `choose_plan_after_transition`, `plan_relate_quantity`, `allow_transition_during_work`, `allow_nowork_during_work`, `show_plan_workcenter_intervals`, `show_plans_count_on_table`, `new_fproblem_when_current_exceeds`, `new_fproblem_when_current_exceeds2`, `records_count_to_start_problem`, `half_donut_colors`, `calculate_quality`, `confirm_orders`, `has_mpm_attribute`) VALUES(3, '', '', '', 'Darbo centras 2', 1, 3, '', 0, '0000-00-00 00:00:00', 0, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[30 - 30], [0 - 5]', 0, 0, 0, 1, 1, 4, 0, 0, 0, 1, 0, 0, 0, 1, '', '', 0, 0, '', '', 1, 'quantity', 0, 0, 0, 4, '', '', 3, '', 0, 0, 0);
INSERT INTO `sensors` (`id`, `ps_id`, `pin_name`, `port`, `name`, `branch_id`, `type`, `image`, `quantity_records_count`, `new_production_start`, `is_partial`, `is_virtual`, `full_sensor_id`, `marked`, `user_update_time`, `last_norecordsmailsend_time`, `tv_speed_deviation`, `show_count_marker`, `speed_detect_interval`, `ok_speed_threshold`, `quantity_records_to_start_order`, `working_on_plans_or_products`, `show_plans_count_on_new_production`, `time_interval_diff_card_button`, `time_interval_fill_nk_button`, `time_interval_technicians_button`, `time_interval_register_event_button`, `time_interval_declare_count_button`, `time_interval_change_order_button`, `auto_generate_diff_cards`, `show_oee_or_other_in_wc`, `inform_about_problems`, `units`, `time_interval_declare_count_on_finish`, `prod_starts_on`, `short_problem`, `long_problem`, `choose_plan_after_transition`, `plan_relate_quantity`, `allow_transition_during_work`, `allow_nowork_during_work`, `show_plan_workcenter_intervals`, `show_plans_count_on_table`, `new_fproblem_when_current_exceeds`, `new_fproblem_when_current_exceeds2`, `records_count_to_start_problem`, `half_donut_colors`, `calculate_quality`, `confirm_orders`, `has_mpm_attribute`) VALUES(4, '', '', '', 'Darbo centras 3', 1, 3, '', 0, '0000-00-00 00:00:00', 0, 0, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[30 - 30], [0 - 5]', 0, 0, 0, 1, 1, 4, 0, 0, 0, 1, 0, 0, 0, 1, '', '', 0, 0, '', '', 1, 'quantity', 0, 0, 0, 4, '', '', 3, '', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) unsigned NOT NULL,
  `key` varchar(45) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `name` text NOT NULL,
  `description` text
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8 COMMENT='Sistemos nustatymai';

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(1, 'system_email', 'horasoee@gmail.com', 'Sistemos el.paštas', 'El.pašto adresas iš kurio sistema siųs laiškus');
INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(2, 'system_email_name', 'HorasOEE', 'Sistemos trumpas pavadinimas', 'Šis pavadinimas bus naudojamas el.laiškuose kaip sistemos pavadinimas');
INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(3, 'record_time_offset', '0', 'Įrašų laiko korekcija (sekundemis)', 'Kai įrašai atsilieka, ');
INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(4, 'warn_about_no_records', 'support@horasmpm.eu : 60', 'Įrašų siuntimo sutrikimo pranešimai', 'Informuoti vartotojus apie galimą sensorių duomenų siuntimo problemą, kai paskutinis įrašas tampa senesnis kaip nurodytas laikas minutėmis. Aprašymo forma: elpatas@pastas.lt | kitaspatsas@pastas.lt | ...@pastas.lt:60');
INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(5, 'frozing_orders_type', '1', 'Užsakymų šaldymas', 'Nurodo kokiu būdu užsakymų sąraše šaldomi užsakymai (žvaigždutė) 0 - Nuo pirmo iki pažymėto užsakymo | 1 - Kiekvienas užsakymas atskirai');
INSERT INTO `settings` (`id`, `key`, `value`, `name`, `description`) VALUES(6, 'exceeded_transition_id', '0', 'Derinimo viršijimas', 'Jeigu po užsakymo pasirinkimo yra viršijama derinimo trukmė, kuri nurodoma plano užsakyme, sistema turi sukurti prastovą. Nurodykite šios prastobos ID (jei 0, funkcionalumas neveikia)');

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

DROP TABLE IF EXISTS `shifts`;
CREATE TABLE IF NOT EXISTS `shifts` (
  `id` int(11) unsigned NOT NULL COMMENT 'Pamainų darbo laikai, gaunami iš planavimo sistemos importuojant',
  `ps_id` varchar(45) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `branch_id` int(11) unsigned NOT NULL,
  `type` varchar(30) NOT NULL,
  `disabled` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=188 DEFAULT CHARSET=utf8 COMMENT='Darbo pamainos';

-- --------------------------------------------------------

--
-- Table structure for table `shift_time_problem_exclusions`
--

DROP TABLE IF EXISTS `shift_time_problem_exclusions`;
CREATE TABLE IF NOT EXISTS `shift_time_problem_exclusions` (
  `id` int(11) NOT NULL,
  `problem_id` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift_time_problem_exclusions`
--

INSERT INTO `shift_time_problem_exclusions` (`id`, `problem_id`) VALUES(1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `slides_collections`
--

DROP TABLE IF EXISTS `slides_collections`;
CREATE TABLE IF NOT EXISTS `slides_collections` (
  `id` int(11) NOT NULL,
  `title` varchar(100) CHARACTER SET utf8 NOT NULL,
  `transition_interval` smallint(5) unsigned NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `slides_json` text CHARACTER SET utf8 NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `statuses`
--

DROP TABLE IF EXISTS `statuses`;
CREATE TABLE IF NOT EXISTS `statuses` (
  `id` int(11) unsigned NOT NULL,
  `name` varchar(45) NOT NULL COMMENT 'Statuso pavadinimas'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COMMENT='Vykdomų užsakymų statusai';

--
-- Dumping data for table `statuses`
--

INSERT INTO `statuses` (`id`, `name`) VALUES(1, 'Vykdomas');
INSERT INTO `statuses` (`id`, `name`) VALUES(2, 'Baigtas');
INSERT INTO `statuses` (`id`, `name`) VALUES(3, 'Atšauktas');
INSERT INTO `statuses` (`id`, `name`) VALUES(4, 'Atidėtas');
INSERT INTO `statuses` (`id`, `name`) VALUES(5, 'Vyksta kita gamyba');
INSERT INTO `statuses` (`id`, `name`) VALUES(6, 'Baigtas iš dalies');
INSERT INTO `statuses` (`id`, `name`) VALUES(7, 'Laukiama duomenų');

-- --------------------------------------------------------

--
-- Table structure for table `updates`
--

DROP TABLE IF EXISTS `updates`;
CREATE TABLE IF NOT EXISTS `updates` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `update_created` varchar(20) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL COMMENT 'vartotoju grupes id',
  `username` varchar(45) NOT NULL,
  `email` varchar(127) NOT NULL,
  `password` varchar(64) NOT NULL,
  `first_name` varchar(127) DEFAULT NULL,
  `last_name` varchar(127) DEFAULT NULL,
  `section` varchar(127) DEFAULT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `sensor_id` varchar(255) DEFAULT NULL,
  `active_sensor_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Jei operatorius gali dirbti prie keliu jutikliu, tai jis jungiamos prie cia nurodyto',
  `line_id` int(11) unsigned DEFAULT NULL,
  `branch_id` int(11) unsigned DEFAULT NULL,
  `responsive` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `tech_code` varchar(20) DEFAULT NULL,
  `additional_emails` text NOT NULL,
  `shift_title` varchar(100) NOT NULL,
  `taken_over_user_id` mediumint(8) unsigned NOT NULL,
  `language` varchar(20) NOT NULL,
  `operator` tinyint(1) NOT NULL,
  `default_start_path` varchar(255) NOT NULL,
  `selected_sensors` tinytext NOT NULL,
  `unique_number` varchar(100) NOT NULL COMMENT 'unikalus darbuotojo tabelio numeris'
) ENGINE=InnoDB AUTO_INCREMENT=156 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(1, 1, 'admin', '', '1d757a489eba133bfb397cf6acc3f5c03cc6cec0', '', '', 'HorasOEE', 1, '', 0, NULL, NULL, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');
INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(2, 1, 'support', 'support@horasmpm.eu', 'd7d0121dc8daf083ec9254bee5a6f1cc784e2e9e', 'Sistemos', 'Palaikymas', 'HorasOEE', 1, '', 0, NULL, NULL, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');
INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(3, 5, 'dc1', '', '83fb674f9b34afbac01f2c205fb3ef38db801950', 'Darbo', 'Centras 1', 'Gamyba', 1, '3', 0, 2, 1, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');
INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(4, 5, 'dc2', '', '7db157923b9118712a450abca38fa2e05e3ecec6', 'Darbo', 'Centras 2', 'Gamyba', 1, '4', 0, 2, 1, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');
INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(5, 5, 'dc3', '', '3354cfd018950fd96a0fbfe5b8286dba337efb5b', 'Darbo', 'Centras 3', 'Gamyba', 1, '5', 0, 2, 1, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');
INSERT INTO `users` (`id`, `group_id`, `username`, `email`, `password`, `first_name`, `last_name`, `section`, `active`, `sensor_id`, `active_sensor_id`, `line_id`, `branch_id`, `responsive`, `tech_code`, `additional_emails`, `shift_title`, `taken_over_user_id`, `language`, `operator`, `default_start_path`, `selected_sensors`, `unique_number`) VALUES(-1, 1, 'statistika', '', 'f58127c05486727024cd2b4e760dad96c2c9c45e', 'statistika', 'statistika', '', 1, '', 0, NULL, NULL, 0, '', '', '', 0, 'lt_LT', 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `verifications`
--

DROP TABLE IF EXISTS `verifications`;
CREATE TABLE IF NOT EXISTS `verifications` (
  `id` int(11) NOT NULL,
  `approved_order_id` int(11) NOT NULL,
  `plan_id` int(11) NOT NULL,
  `current_count` int(11) NOT NULL COMMENT 'esamu metu patikrintu detaliu kiekis',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `work_entries`
--

DROP TABLE IF EXISTS `work_entries`;
CREATE TABLE IF NOT EXISTS `work_entries` (
  `id` int(11) NOT NULL,
  `work_type_id` int(11) NOT NULL,
  `user_id` int(11) unsigned DEFAULT NULL,
  `sensor_id` int(11) unsigned NOT NULL DEFAULT '0',
  `plan_id` int(11) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `started_at` datetime DEFAULT NULL,
  `ended_at` datetime DEFAULT NULL,
  `comment` tinytext CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `work_types`
--

DROP TABLE IF EXISTS `work_types`;
CREATE TABLE IF NOT EXISTS `work_types` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `line_id` int(11) NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_lithuanian_ci NOT NULL,
  `sensor_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `after_production_problems`
--
ALTER TABLE `after_production_problems`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `problem_id` (`problem_id`);

--
-- Indexes for table `approved_orders`
--
ALTER TABLE `approved_orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `created` (`created`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `status_id` (`status_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `branches`
--
ALTER TABLE `branches`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `ps_id` (`ps_id`);

--
-- Indexes for table `dashboards_calculations`
--
ALTER TABLE `dashboards_calculations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `sensor_id` (`sensor_id`);

--
-- Indexes for table `dashboards_calculation_mos`
--
ALTER TABLE `dashboards_calculation_mos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `dashboards_settings`
--
ALTER TABLE `dashboards_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `diff_cards`
--
ALTER TABLE `diff_cards`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `line_id` (`line_id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `problem_id` (`problem_id`),
  ADD KEY `shift_manager_user_id` (`shift_manager_user_id`),
  ADD KEY `shift_manager_user_id_2` (`shift_manager_user_id`),
  ADD KEY `diff_type_id` (`diff_type_id`),
  ADD KEY `diff_sub_type_id` (`diff_sub_type_id`),
  ADD KEY `responsive_section_user_id` (`responsive_section_user_id`),
  ADD KEY `quality_section_user_id` (`quality_section_user_id`),
  ADD KEY `production_manager_user_id` (`production_manager_user_id`),
  ADD KEY `found_problem_id` (`found_problem_id`);

--
-- Indexes for table `diff_types`
--
ALTER TABLE `diff_types`
  ADD PRIMARY KEY (`id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `factories`
--
ALTER TABLE `factories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `found_problems`
--
ALTER TABLE `found_problems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `start` (`start`),
  ADD KEY `end` (`end`),
  ADD KEY `problem_id` (`problem_id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `state` (`state`),
  ADD KEY `diff_card_id` (`diff_card_id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `while_working` (`while_working`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lines`
--
ALTER TABLE `lines`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ps_id` (`ps_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `line_sensors`
--
ALTER TABLE `line_sensors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `line_id` (`line_id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `losses`
--
ALTER TABLE `losses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `approved_order_id` (`approved_order_id`),
  ADD KEY `loss_type_id` (`loss_type_id`);

--
-- Indexes for table `loss_types`
--
ALTER TABLE `loss_types`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_calculations`
--
ALTER TABLE `order_calculations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `shift_approved_order` (`shift_id`,`approved_order_id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `approved_order_id` (`approved_order_id`);

--
-- Indexes for table `partial_quantities`
--
ALTER TABLE `partial_quantities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `shift_id` (`shift_id`),
  ADD KEY `approved_order_id` (`approved_order_id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `plans`
--
ALTER TABLE `plans`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `unique_plans` (`sensor_id`,`mo_number`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `mo_number` (`mo_number`),
  ADD KEY `start` (`start`),
  ADD KEY `end` (`end`),
  ADD KEY `production_code` (`production_code`);

--
-- Indexes for table `possibilities`
--
ALTER TABLE `possibilities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `problems`
--
ALTER TABLE `problems`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_type` (`sensor_type`),
  ADD KEY `for_speed` (`for_speed`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `production_code` (`production_code`,`work_center`);

--
-- Indexes for table `records`
--
ALTER TABLE `records`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `approved_order_id` (`approved_order_id`),
  ADD KEY `found_problem_id` (`found_problem_id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `created` (`created`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `records_archive`
--
ALTER TABLE `records_archive`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `approved_order_id` (`approved_order_id`),
  ADD KEY `found_problem_id` (`found_problem_id`),
  ADD KEY `plan_id` (`plan_id`),
  ADD KEY `created` (`created`),
  ADD KEY `shift_id` (`shift_id`);

--
-- Indexes for table `sensordataimportcheckpoint`
--
ALTER TABLE `sensordataimportcheckpoint`
  ADD PRIMARY KEY (`SourceId`);

--
-- Indexes for table `sensorprofilerecords`
--
ALTER TABLE `sensorprofilerecords`
  ADD PRIMARY KEY (`Source`,`Date`);

--
-- Indexes for table `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ps_id_idx` (`ps_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `key_idx` (`key`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ps_id_idx` (`ps_id`),
  ADD KEY `start_idx` (`start`),
  ADD KEY `end_idx` (`end`),
  ADD KEY `branch_id_idx` (`branch_id`);

--
-- Indexes for table `shift_time_problem_exclusions`
--
ALTER TABLE `shift_time_problem_exclusions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `problem_id` (`problem_id`);

--
-- Indexes for table `slides_collections`
--
ALTER TABLE `slides_collections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `statuses`
--
ALTER TABLE `statuses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `updates`
--
ALTER TABLE `updates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name_idx` (`username`),
  ADD KEY `email_idx` (`email`),
  ADD KEY `password_idx` (`password`),
  ADD KEY `active_idx` (`active`),
  ADD KEY `sensor_id_idx` (`sensor_id`),
  ADD KEY `branch_id_idx` (`branch_id`),
  ADD KEY `responsive_idx` (`responsive`),
  ADD KEY `line_id_idx` (`line_id`);

--
-- Indexes for table `verifications`
--
ALTER TABLE `verifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_entries`
--
ALTER TABLE `work_entries`
  ADD PRIMARY KEY (`id`),
  ADD KEY `work_type_id` (`work_type_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `sensor_id` (`sensor_id`),
  ADD KEY `plan_id` (`plan_id`);

--
-- Indexes for table `work_types`
--
ALTER TABLE `work_types`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `after_production_problems`
--
ALTER TABLE `after_production_problems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `approved_orders`
--
ALTER TABLE `approved_orders`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `branches`
--
ALTER TABLE `branches`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `dashboards_calculations`
--
ALTER TABLE `dashboards_calculations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `dashboards_calculation_mos`
--
ALTER TABLE `dashboards_calculation_mos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `dashboards_settings`
--
ALTER TABLE `dashboards_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `diff_cards`
--
ALTER TABLE `diff_cards`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `diff_types`
--
ALTER TABLE `diff_types`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `factories`
--
ALTER TABLE `factories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `found_problems`
--
ALTER TABLE `found_problems`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `lines`
--
ALTER TABLE `lines`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `line_sensors`
--
ALTER TABLE `line_sensors`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `losses`
--
ALTER TABLE `losses`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `loss_types`
--
ALTER TABLE `loss_types`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_calculations`
--
ALTER TABLE `order_calculations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `partial_quantities`
--
ALTER TABLE `partial_quantities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=130;
--
-- AUTO_INCREMENT for table `plans`
--
ALTER TABLE `plans`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `possibilities`
--
ALTER TABLE `possibilities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `problems`
--
ALTER TABLE `problems`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `records`
--
ALTER TABLE `records`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Pamainų darbo laikai, gaunami iš planavimo sistemos importuojant',AUTO_INCREMENT=1;
--
-- AUTO_INCREMENT for table `shift_time_problem_exclusions`
--
ALTER TABLE `shift_time_problem_exclusions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `slides_collections`
--
ALTER TABLE `slides_collections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `statuses`
--
ALTER TABLE `statuses`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `updates`
--
ALTER TABLE `updates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `verifications`
--
ALTER TABLE `verifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `work_entries`
--
ALTER TABLE `work_entries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `work_types`
--
ALTER TABLE `work_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
